VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmJobMedia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Media Job Details"
   ClientHeight    =   15705
   ClientLeft      =   2040
   ClientTop       =   3810
   ClientWidth     =   24270
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmJobMedia.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15705
   ScaleWidth      =   24270
   Begin TabDlg.SSTab tabMediaSpecs 
      Height          =   10395
      Left            =   120
      TabIndex        =   57
      Top             =   3000
      Width           =   21375
      _ExtentX        =   37703
      _ExtentY        =   18336
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Job Details"
      TabPicture(0)   =   "frmJobMedia.frx":08CA
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdJobDetail"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ddnFormat"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "ddnInclusive"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "ddnOthers"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Media Actions"
      TabPicture(1)   =   "frmJobMedia.frx":08E6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdSaveAction"
      Tab(1).Control(1)=   "cmdClearAction"
      Tab(1).Control(2)=   "cmdSaveAsTemplate"
      Tab(1).Control(3)=   "ddnActions"
      Tab(1).Control(4)=   "cmbMediaSpec"
      Tab(1).Control(5)=   "grdMediaActions"
      Tab(1).Control(6)=   "picSpecs"
      Tab(1).Control(7)=   "lblMediaSpecID"
      Tab(1).Control(8)=   "lblCaption(51)"
      Tab(1).Control(9)=   "lblJobMediaSpecID"
      Tab(1).ControlCount=   10
      TabCaption(2)   =   "Notes"
      TabPicture(2)   =   "frmJobMedia.frx":0902
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "picNotes"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Media Files"
      TabPicture(3)   =   "frmJobMedia.frx":091E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "picAssignment"
      Tab(3).Control(1)=   "grdMediaFiles"
      Tab(3).Control(2)=   "lblCaption(53)"
      Tab(3).Control(3)=   "lblTotalMediaSize"
      Tab(3).ControlCount=   4
      Begin VB.PictureBox picAssignment 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   2175
         Left            =   -74820
         ScaleHeight     =   2145
         ScaleWidth      =   11925
         TabIndex        =   161
         Top             =   2640
         Width           =   11955
      End
      Begin VB.PictureBox picNotes 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   5235
         Left            =   -74940
         ScaleHeight     =   5235
         ScaleWidth      =   14355
         TabIndex        =   135
         Top             =   540
         Width           =   14355
         Begin VB.CommandButton cmdReplaceProducerNotes 
            Caption         =   "<< Replace"
            Height          =   255
            Left            =   8880
            TabIndex        =   155
            Top             =   4560
            Width           =   1035
         End
         Begin VB.TextBox txtProducerNotes 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFC0FF&
            Height          =   975
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   154
            Top             =   4260
            Width           =   7695
         End
         Begin VB.CommandButton cmdAddProducerNotes 
            Caption         =   "<< Insert"
            Height          =   255
            Left            =   8880
            TabIndex        =   153
            Top             =   4260
            Width           =   1035
         End
         Begin VB.CheckBox chkClearAfterAdd 
            Alignment       =   1  'Right Justify
            Caption         =   "Clear after insert"
            Height          =   195
            Left            =   12660
            TabIndex        =   146
            Top             =   0
            Value           =   1  'Checked
            Width           =   1575
         End
         Begin VB.CommandButton cmdAddDespatchNotes 
            Caption         =   "<< Insert"
            Height          =   255
            Left            =   8880
            TabIndex        =   145
            Top             =   3060
            Width           =   1035
         End
         Begin VB.CommandButton cmdAddOperatorNotes 
            Caption         =   "<< Insert"
            Height          =   255
            Left            =   8880
            TabIndex        =   144
            Top             =   1680
            Width           =   1035
         End
         Begin VB.CommandButton cmdAddOffice 
            Caption         =   "<< Insert"
            Height          =   255
            Left            =   8880
            TabIndex        =   143
            Top             =   0
            Width           =   1035
         End
         Begin VB.TextBox txtInsertNotes 
            Appearance      =   0  'Flat
            Height          =   1995
            Left            =   10140
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   142
            Top             =   240
            Width           =   4095
         End
         Begin VB.TextBox txtDespatch 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFC0&
            Height          =   915
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   141
            Top             =   3180
            Width           =   7695
         End
         Begin VB.TextBox txtOfficeClient 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFC0C0&
            Height          =   1575
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   140
            Top             =   0
            Width           =   7695
         End
         Begin VB.TextBox txtOperator 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0E0FF&
            Height          =   1155
            Left            =   1080
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   139
            Top             =   1800
            Width           =   7695
         End
         Begin VB.CommandButton cmdReplaceOffice 
            Caption         =   "<< Replace"
            Height          =   255
            Left            =   8880
            TabIndex        =   138
            Top             =   300
            Width           =   1035
         End
         Begin VB.CommandButton cmdReplaceOperator 
            Caption         =   "<< Replace"
            Height          =   255
            Left            =   8880
            TabIndex        =   137
            Top             =   1980
            Width           =   1035
         End
         Begin VB.CommandButton cmdReplaceDespatch 
            Caption         =   "<< Replace"
            Height          =   255
            Left            =   8880
            TabIndex        =   136
            Top             =   3360
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Producer Notes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   11
            Left            =   0
            TabIndex        =   156
            Top             =   4260
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Before using operator notes, please make sure your job details are as descriptive as possible - especially for dubbings!"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   88
            Left            =   1080
            TabIndex        =   152
            Top             =   1620
            Width           =   7695
         End
         Begin VB.Label lblCaption 
            Caption         =   "Before using despatch notes, be sure to create despatch records using the 'Delivery Notes' button at the bottom of this form."
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   84
            Left            =   1080
            TabIndex        =   151
            Top             =   3000
            Width           =   7695
         End
         Begin VB.Line Line5 
            BorderColor     =   &H80000010&
            X1              =   10020
            X2              =   10020
            Y1              =   0
            Y2              =   5220
         End
         Begin VB.Label lblCaption 
            Caption         =   "Notes To Insert"
            Height          =   255
            Index           =   23
            Left            =   10140
            TabIndex        =   150
            Top             =   0
            Width           =   1635
         End
         Begin VB.Label lblCaption 
            Caption         =   "Despatch Notes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Index           =   21
            Left            =   0
            TabIndex        =   149
            Top             =   3000
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "General Notes / Work to be done"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1035
            Index           =   20
            Left            =   0
            TabIndex        =   148
            Top             =   0
            Width           =   1035
         End
         Begin VB.Label lblCaption 
            Caption         =   "Operator / Technician Notes"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   555
            Index           =   19
            Left            =   0
            TabIndex        =   147
            Top             =   1620
            Width           =   1035
         End
      End
      Begin VB.CommandButton cmdSaveAction 
         Caption         =   "Save Media Action Details"
         Height          =   495
         Left            =   -74220
         TabIndex        =   64
         Top             =   5040
         Width           =   2595
      End
      Begin VB.CommandButton cmdClearAction 
         Caption         =   "Clear Media Action Details"
         Height          =   495
         Left            =   -74220
         TabIndex        =   63
         Top             =   5640
         Width           =   2595
      End
      Begin VB.CommandButton cmdSaveAsTemplate 
         Caption         =   "Save Action as Media Spec"
         Height          =   495
         Left            =   -74220
         TabIndex        =   62
         Top             =   6240
         Width           =   2595
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMediaFiles 
         Bindings        =   "frmJobMedia.frx":093A
         Height          =   1515
         Left            =   -74880
         TabIndex        =   58
         Top             =   780
         Width           =   21120
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   14
         Columns(0).Width=   1482
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1402
         Columns(1).Caption=   "Series"
         Columns(1).Name =   "evenseries"
         Columns(1).DataField=   "eventseries"
         Columns(1).FieldLen=   256
         Columns(2).Width=   1402
         Columns(2).Caption=   "Episode"
         Columns(2).Name =   "eventepisode"
         Columns(2).DataField=   "eventepisode"
         Columns(2).FieldLen=   256
         Columns(3).Width=   5292
         Columns(3).Caption=   "Title"
         Columns(3).Name =   "Title"
         Columns(3).DataField=   "eventtitle"
         Columns(3).FieldLen=   256
         Columns(4).Width=   5292
         Columns(4).Caption=   "Sub Title"
         Columns(4).Name =   "Sub Title"
         Columns(4).DataField=   "eventsubtitle"
         Columns(4).FieldLen=   256
         Columns(5).Width=   2646
         Columns(5).Caption=   "Format"
         Columns(5).Name =   "Format"
         Columns(5).DataField=   "clipformat"
         Columns(5).FieldLen=   256
         Columns(6).Width=   1402
         Columns(6).Caption=   "Horiz."
         Columns(6).Name =   "Horiz."
         Columns(6).Alignment=   2
         Columns(6).DataField=   "cliphorizontalpixels"
         Columns(6).FieldLen=   256
         Columns(7).Width=   1402
         Columns(7).Caption=   "Vert."
         Columns(7).Name =   "Vert."
         Columns(7).Alignment=   2
         Columns(7).DataField=   "clipverticalpixels"
         Columns(7).FieldLen=   256
         Columns(8).Width=   1640
         Columns(8).Caption=   "Bit Rate"
         Columns(8).Name =   "Bit Rate"
         Columns(8).DataField=   "clipbitrate"
         Columns(8).FieldLen=   256
         Columns(9).Width=   5292
         Columns(9).Caption=   "File Name"
         Columns(9).Name =   "File Name"
         Columns(9).DataField=   "clipfilename"
         Columns(9).FieldLen=   256
         Columns(10).Width=   2646
         Columns(10).Caption=   "Stored On"
         Columns(10).Name=   "clipstore"
         Columns(10).FieldLen=   256
         Columns(11).Width=   1879
         Columns(11).Caption=   "File Size (GB)"
         Columns(11).Name=   "filesize"
         Columns(11).Alignment=   2
         Columns(11).FieldLen=   256
         Columns(12).Width=   4419
         Columns(12).Caption=   "Owned By"
         Columns(12).Name=   "Owned By"
         Columns(12).DataField=   "companyname"
         Columns(12).FieldLen=   256
         Columns(13).Width=   3200
         Columns(13).Visible=   0   'False
         Columns(13).Caption=   "libraryID"
         Columns(13).Name=   "libraryID"
         Columns(13).DataField=   "libraryID"
         Columns(13).FieldLen=   256
         _ExtentX        =   37245
         _ExtentY        =   2672
         _StockProps     =   79
         Caption         =   "Media Files created by this Job"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnActions 
         Height          =   1755
         Left            =   -74640
         TabIndex        =   61
         Top             =   1200
         Width           =   2955
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   5715
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   5212
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpec 
         Height          =   315
         Left            =   -68940
         TabIndex        =   131
         ToolTipText     =   "The company this tape belongs to"
         Top             =   420
         Width           =   8115
         DataFieldList   =   "mediaspecname"
         MaxDropDownItems=   24
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "companyID"
         Columns(0).Name =   "mediaspecID"
         Columns(0).DataField=   "mediaspecID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   7938
         Columns(1).Caption=   "mediaspecname"
         Columns(1).Name =   "mediaspecname"
         Columns(1).DataField=   "mediaspecname"
         Columns(1).FieldLen=   256
         _ExtentX        =   14314
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   12632319
         DataFieldToDisplay=   "mediaspecname"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMediaActions 
         Bindings        =   "frmJobMedia.frx":0951
         Height          =   4395
         Left            =   -74820
         TabIndex        =   65
         Top             =   480
         Width           =   3795
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "jobmediaspecID"
         Columns(0).Name =   "jobmediaspecID"
         Columns(0).DataField=   "jobmediaspecID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "jobID"
         Columns(1).Name =   "jobID"
         Columns(1).DataField=   "jobID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   4445
         Columns(2).Caption=   "Action "
         Columns(2).Name =   "action"
         Columns(2).DataField=   "action"
         Columns(2).FieldLen=   256
         Columns(3).Width=   1085
         Columns(3).Caption=   "Order"
         Columns(3).Name =   "fd_order"
         Columns(3).DataField=   "fd_order"
         Columns(3).FieldLen=   256
         _ExtentX        =   6694
         _ExtentY        =   7752
         _StockProps     =   79
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnOthers 
         Height          =   1755
         Left            =   1860
         TabIndex        =   157
         Top             =   1800
         Width           =   6675
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8819
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "Column 1"
         Columns(1).FieldLen=   256
         _ExtentX        =   11774
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnInclusive 
         Bindings        =   "frmJobMedia.frx":096F
         Height          =   1755
         Left            =   1860
         TabIndex        =   158
         Top             =   4020
         Width           =   6675
         DataFieldList   =   "description"
         ListAutoValidate=   0   'False
         MaxDropDownItems=   20
         _Version        =   196617
         DefColWidth     =   8819
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   9710
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3519
         Columns(1).Caption=   "format"
         Columns(1).Name =   "format"
         Columns(1).DataField=   "format"
         Columns(1).FieldLen=   256
         _ExtentX        =   11774
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnFormat 
         Height          =   1755
         Left            =   540
         TabIndex        =   159
         Top             =   1380
         Width           =   1035
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "Description"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   1826
         _ExtentY        =   3096
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdJobDetail 
         Bindings        =   "frmJobMedia.frx":098A
         Height          =   7335
         Left            =   120
         TabIndex        =   160
         TabStop         =   0   'False
         Top             =   480
         Width           =   13215
         _Version        =   196617
         stylesets.count =   1
         stylesets(0).Name=   "COMPLETED"
         stylesets(0).BackColor=   7011261
         stylesets(0).HasFont=   -1  'True
         BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(0).Picture=   "frmJobMedia.frx":09A5
         BeveColorScheme =   1
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16051436
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   12
         Columns(0).Width=   370
         Columns(0).Name =   "copytype"
         Columns(0).DataField=   "copytype"
         Columns(0).Case =   2
         Columns(0).FieldLen=   1
         Columns(1).Width=   7541
         Columns(1).Caption=   "Description"
         Columns(1).Name =   "description"
         Columns(1).DataField=   "description"
         Columns(1).FieldLen=   256
         Columns(2).Width=   7541
         Columns(2).Caption=   "Work to be done"
         Columns(2).Name =   "workflowdescriptiom"
         Columns(2).DataField=   "workflowdescriptiom"
         Columns(2).FieldLen=   256
         Columns(3).Width=   714
         Columns(3).Caption=   "Qty"
         Columns(3).Name =   "quantity"
         Columns(3).DataField=   "quantity"
         Columns(3).FieldLen=   256
         Columns(4).Width=   2646
         Columns(4).Caption=   "Format"
         Columns(4).Name =   "format"
         Columns(4).DataField=   "format"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   979
         Columns(5).Caption=   "R/T"
         Columns(5).Name =   "runningtime"
         Columns(5).DataField=   "runningtime"
         Columns(5).FieldLen=   256
         Columns(6).Width=   1482
         Columns(6).Caption=   "real mins?"
         Columns(6).Name =   "chargemins"
         Columns(6).DataField=   "chargerealminutes"
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "jobID"
         Columns(7).Name =   "jobID"
         Columns(7).DataField=   "jobID"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "jobdetailID"
         Columns(8).Name =   "jobdetailID"
         Columns(8).DataField=   "jobdetailID"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "Completed Date"
         Columns(9).Name =   "completeddate"
         Columns(9).DataField=   "completeddate"
         Columns(9).DataType=   7
         Columns(9).FieldLen=   256
         Columns(9).Style=   1
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "By"
         Columns(10).Name=   "completeduser"
         Columns(10).DataField=   "completeduser"
         Columns(10).FieldLen=   256
         Columns(11).Width=   873
         Columns(11).Caption=   "Order"
         Columns(11).Name=   "fd_orderby"
         Columns(11).DataField=   "fd_orderby"
         Columns(11).FieldLen=   256
         _ExtentX        =   23310
         _ExtentY        =   12938
         _StockProps     =   79
         Caption         =   "Job Details"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox picSpecs 
         BorderStyle     =   0  'None
         Height          =   6255
         Left            =   -70740
         ScaleHeight     =   6255
         ScaleWidth      =   9975
         TabIndex        =   66
         Top             =   780
         Width           =   9975
         Begin VB.TextBox txtSecondAudioTrack 
            Height          =   315
            Left            =   5880
            TabIndex        =   164
            ToolTipText     =   "The clip bitrate"
            Top             =   1800
            Width           =   1935
         End
         Begin VB.TextBox txtFirstAudioTrack 
            Height          =   315
            Left            =   5880
            TabIndex        =   162
            ToolTipText     =   "The clip bitrate"
            Top             =   1440
            Width           =   1935
         End
         Begin VB.TextBox txtDestinationPassword 
            Height          =   315
            Left            =   5880
            TabIndex        =   98
            ToolTipText     =   "The clip bitrate"
            Top             =   5040
            Width           =   1935
         End
         Begin VB.TextBox txtDestinationLogin 
            Height          =   315
            Left            =   5880
            TabIndex        =   97
            ToolTipText     =   "The clip bitrate"
            Top             =   4680
            Width           =   1935
         End
         Begin VB.TextBox txtDestinationAddress 
            Height          =   315
            Left            =   5880
            TabIndex        =   96
            ToolTipText     =   "The clip filename"
            Top             =   4320
            Width           =   4035
         End
         Begin VB.ComboBox cmbDestination 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   5880
            TabIndex        =   95
            ToolTipText     =   "The Clip Codec"
            Top             =   3960
            Width           =   1935
         End
         Begin VB.TextBox txtSourcePassword 
            Height          =   315
            Left            =   5880
            TabIndex        =   94
            ToolTipText     =   "The clip bitrate"
            Top             =   3600
            Width           =   1935
         End
         Begin VB.TextBox txtSourceLogin 
            Height          =   315
            Left            =   5880
            TabIndex        =   93
            ToolTipText     =   "The clip bitrate"
            Top             =   3240
            Width           =   1935
         End
         Begin VB.TextBox txtSourceAddress 
            Height          =   315
            Left            =   5880
            TabIndex        =   92
            ToolTipText     =   "The clip filename"
            Top             =   2880
            Width           =   4035
         End
         Begin VB.ComboBox cmbSource 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   5880
            TabIndex        =   91
            ToolTipText     =   "The Clip Codec"
            Top             =   2520
            Width           =   1935
         End
         Begin VB.ComboBox cmbStreamType 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1800
            TabIndex        =   90
            ToolTipText     =   "The Clip Codec"
            Top             =   1440
            Width           =   1935
         End
         Begin VB.ComboBox cmbInterlace 
            Height          =   315
            Left            =   1800
            TabIndex        =   89
            ToolTipText     =   "The Clip Codec"
            Top             =   3600
            Width           =   1935
         End
         Begin VB.ComboBox cmbCbrVbr 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1800
            TabIndex        =   88
            ToolTipText     =   "The Clip Codec"
            Top             =   2160
            Width           =   1935
         End
         Begin VB.ComboBox cmbPasses 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1800
            TabIndex        =   87
            ToolTipText     =   "The Clip Codec"
            Top             =   1800
            Width           =   1935
         End
         Begin VB.TextBox txtSortOrder 
            Height          =   315
            Left            =   5880
            TabIndex        =   86
            ToolTipText     =   "The clip bitrate"
            Top             =   2160
            Width           =   1935
         End
         Begin VB.TextBox txtAltLocation 
            Height          =   315
            Left            =   5880
            TabIndex        =   85
            ToolTipText     =   "The clip filename"
            Top             =   0
            Width           =   4035
         End
         Begin VB.ComboBox cmbPurpose 
            Height          =   315
            Left            =   1800
            TabIndex        =   84
            ToolTipText     =   "The Clip Codec"
            Top             =   360
            Width           =   1935
         End
         Begin VB.ComboBox cmbDelivery 
            Height          =   315
            Left            =   1800
            TabIndex        =   83
            ToolTipText     =   "The Clip Codec"
            Top             =   0
            Width           =   1935
         End
         Begin VB.TextBox txtOtherSpecs 
            Height          =   1035
            Left            =   5880
            MultiLine       =   -1  'True
            TabIndex        =   82
            ToolTipText     =   "Notes for the clip"
            Top             =   5760
            Width           =   4095
         End
         Begin VB.ComboBox cmbGraphicOverlay 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   5880
            TabIndex        =   81
            ToolTipText     =   "The Clip Codec"
            Top             =   360
            Width           =   1935
         End
         Begin VB.TextBox txtGraphicOverlayOpacity 
            Height          =   315
            Left            =   5880
            TabIndex        =   80
            ToolTipText     =   "The clip bitrate"
            Top             =   720
            Visible         =   0   'False
            Width           =   1935
         End
         Begin VB.TextBox txtgraphicOverlayFilename 
            Height          =   315
            Left            =   5880
            TabIndex        =   79
            ToolTipText     =   "The clip filename"
            Top             =   1080
            Width           =   4035
         End
         Begin VB.ComboBox cmbGeometry 
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1800
            TabIndex        =   78
            ToolTipText     =   "The Clip Codec"
            Top             =   4320
            Width           =   1935
         End
         Begin VB.ComboBox cmbAspect 
            Height          =   315
            Left            =   1800
            TabIndex        =   77
            ToolTipText     =   "The Clip Codec"
            Top             =   3960
            Width           =   1935
         End
         Begin VB.TextBox txtAudioBitrate 
            Height          =   315
            Left            =   1800
            TabIndex        =   76
            ToolTipText     =   "The clip bitrate"
            Top             =   5400
            Width           =   1935
         End
         Begin VB.TextBox txtVideoBitrate 
            Height          =   315
            Left            =   1800
            TabIndex        =   75
            ToolTipText     =   "The clip bitrate"
            Top             =   4680
            Width           =   1935
         End
         Begin VB.TextBox txtBitrate 
            Height          =   315
            Left            =   1800
            TabIndex        =   74
            ToolTipText     =   "The clip bitrate"
            Top             =   5760
            Width           =   1935
         End
         Begin VB.ComboBox cmbFrameRate 
            Height          =   315
            Left            =   1800
            TabIndex        =   73
            ToolTipText     =   "The Clip Codec"
            Top             =   3240
            Width           =   1935
         End
         Begin VB.ComboBox cmbAudioCodec 
            Height          =   315
            Left            =   1800
            TabIndex        =   72
            ToolTipText     =   "The Clip Codec"
            Top             =   5040
            Width           =   1935
         End
         Begin VB.TextBox txtVertpixels 
            Height          =   315
            Left            =   1800
            TabIndex        =   71
            ToolTipText     =   "The number of vertical pixels"
            Top             =   2880
            Width           =   1935
         End
         Begin VB.TextBox txtHorizpixels 
            Height          =   315
            Left            =   1800
            TabIndex        =   70
            ToolTipText     =   "The number of horizontal pixels"
            Top             =   2520
            Width           =   1935
         End
         Begin VB.ComboBox cmbClipcodec 
            Height          =   315
            Left            =   1800
            TabIndex        =   69
            ToolTipText     =   "The Clip Codec"
            Top             =   1080
            Width           =   1935
         End
         Begin VB.ComboBox cmbClipformat 
            Height          =   315
            Left            =   1800
            TabIndex        =   68
            ToolTipText     =   "The Clip Format"
            Top             =   720
            Width           =   1935
         End
         Begin VB.ComboBox cmbVodPlatform 
            Height          =   315
            Left            =   5880
            TabIndex        =   67
            ToolTipText     =   "The Clip Codec"
            Top             =   5400
            Width           =   1935
         End
         Begin VB.Label lblCaption 
            Caption         =   "Second Audio Track"
            Height          =   255
            Index           =   57
            Left            =   4080
            TabIndex        =   165
            Top             =   1860
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "First Audio Track"
            Height          =   255
            Index           =   56
            Left            =   4080
            TabIndex        =   163
            Top             =   1500
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Delivery Password"
            Height          =   255
            Index           =   31
            Left            =   4080
            TabIndex        =   130
            Top             =   5100
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Delivery Login"
            Height          =   255
            Index           =   30
            Left            =   4080
            TabIndex        =   129
            Top             =   4740
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Delivery Address"
            Height          =   255
            Index           =   29
            Left            =   4080
            TabIndex        =   128
            Top             =   4380
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Eventual Media Delivery"
            Height          =   255
            Index           =   28
            Left            =   4080
            TabIndex        =   127
            Top             =   4020
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Source Password"
            Height          =   255
            Index           =   27
            Left            =   4080
            TabIndex        =   126
            Top             =   3660
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Source Login"
            Height          =   255
            Index           =   26
            Left            =   4080
            TabIndex        =   125
            Top             =   3300
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Source Address"
            Height          =   255
            Index           =   25
            Left            =   4080
            TabIndex        =   124
            Top             =   2940
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Original Media Source"
            Height          =   255
            Index           =   24
            Left            =   4080
            TabIndex        =   123
            Top             =   2580
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Stream Type"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   122
            Top             =   1500
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Interlace/Progressive"
            Height          =   255
            Index           =   22
            Left            =   0
            TabIndex        =   121
            Top             =   3660
            Width           =   1695
         End
         Begin VB.Label lblCaption 
            Caption         =   "CBR/VBR?"
            Height          =   255
            Index           =   4
            Left            =   0
            TabIndex        =   120
            Top             =   2220
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Number of Passes"
            Height          =   255
            Index           =   7
            Left            =   0
            TabIndex        =   119
            Top             =   1860
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "List Sorting Order"
            Height          =   255
            Index           =   12
            Left            =   4080
            TabIndex        =   118
            Top             =   2220
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Usual Alt. Location"
            Height          =   255
            Index           =   18
            Left            =   4080
            TabIndex        =   117
            Top             =   60
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Clip Purpose"
            Height          =   255
            Index           =   17
            Left            =   0
            TabIndex        =   116
            Top             =   420
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Delivery Method"
            Height          =   255
            Index           =   13
            Left            =   0
            TabIndex        =   115
            Top             =   60
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Other Specs"
            Height          =   255
            Index           =   15
            Left            =   4080
            TabIndex        =   114
            Top             =   5820
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Graphic Overlay?"
            Height          =   255
            Index           =   14
            Left            =   4080
            TabIndex        =   113
            Top             =   420
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Overlay Opacity (%)"
            Height          =   255
            Index           =   32
            Left            =   4080
            TabIndex        =   112
            Top             =   780
            Visible         =   0   'False
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Graphic Overlay File"
            Height          =   255
            Index           =   36
            Left            =   4080
            TabIndex        =   111
            Top             =   1140
            Width           =   1815
         End
         Begin VB.Label lblCaption 
            Caption         =   "Geometry"
            Height          =   255
            Index           =   37
            Left            =   0
            TabIndex        =   110
            Top             =   4380
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Aspect Ratio"
            Height          =   255
            Index           =   38
            Left            =   0
            TabIndex        =   109
            Top             =   4020
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Bitrate"
            Height          =   255
            Index           =   39
            Left            =   0
            TabIndex        =   108
            Top             =   5460
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Video Bitrate"
            Height          =   255
            Index           =   40
            Left            =   0
            TabIndex        =   107
            Top             =   4740
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Total Bitrate"
            Height          =   255
            Index           =   41
            Left            =   0
            TabIndex        =   106
            Top             =   5820
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Frame Rate"
            Height          =   255
            Index           =   42
            Left            =   0
            TabIndex        =   105
            Top             =   3300
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Audio Codec/Contents"
            Height          =   255
            Index           =   43
            Left            =   0
            TabIndex        =   104
            Top             =   5100
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Vert Pixels"
            Height          =   255
            Index           =   44
            Left            =   0
            TabIndex        =   103
            Top             =   2940
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Horiz Pixels"
            Height          =   255
            Index           =   45
            Left            =   0
            TabIndex        =   102
            Top             =   2580
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "Video Codec/Contents"
            Height          =   255
            Index           =   48
            Left            =   0
            TabIndex        =   101
            Top             =   1140
            Width           =   1755
         End
         Begin VB.Label lblCaption 
            Caption         =   "Media Format"
            Height          =   255
            Index           =   50
            Left            =   0
            TabIndex        =   100
            Top             =   780
            Width           =   1455
         End
         Begin VB.Label lblCaption 
            Caption         =   "VOD Platform"
            Height          =   255
            Index           =   55
            Left            =   4080
            TabIndex        =   99
            Top             =   5460
            Width           =   1455
         End
      End
      Begin VB.Label lblMediaSpecID 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74760
         TabIndex        =   133
         Tag             =   "CLEARFIELDS"
         Top             =   6780
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Choose Media Spec"
         Height          =   255
         Index           =   51
         Left            =   -70740
         TabIndex        =   134
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label lblJobMediaSpecID 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -72960
         TabIndex        =   132
         Tag             =   "CLEARFIELDS"
         Top             =   6780
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label lblCaption 
         Caption         =   "Total Filesize of Job Media (GB)"
         Height          =   255
         Index           =   53
         Left            =   -74820
         TabIndex        =   60
         Top             =   420
         Width           =   2415
      End
      Begin VB.Label lblTotalMediaSize 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -72360
         TabIndex        =   59
         Tag             =   "CLEARFIELDS"
         Top             =   420
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   330
      Index           =   5
      Left            =   14040
      MaskColor       =   &H00FFFFFF&
      Picture         =   "frmJobMedia.frx":09C1
      Style           =   1  'Graphical
      TabIndex        =   56
      ToolTipText     =   "Change this jobs status"
      Top             =   2400
      UseMaskColor    =   -1  'True
      Width           =   345
   End
   Begin VB.ComboBox cmbDeadlineTime 
      Height          =   315
      Left            =   8040
      TabIndex        =   53
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2400
      Width           =   1035
   End
   Begin VB.TextBox txtStatus 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   11040
      Locked          =   -1  'True
      TabIndex        =   51
      ToolTipText     =   "Current status of selected job"
      Top             =   2400
      Width           =   2895
   End
   Begin VB.Frame fraJobOptions 
      Caption         =   "Additional Job Options"
      Height          =   915
      Left            =   9660
      TabIndex        =   44
      Top             =   1440
      Visible         =   0   'False
      Width           =   4695
      Begin VB.CheckBox chkUrgent 
         Caption         =   "Urgent"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   3600
         TabIndex        =   22
         ToolTipText     =   "Is Job on Urgent?"
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox chkJobDetailOnInvoice 
         Caption         =   "Job Detail on Invoice"
         Height          =   255
         Left            =   180
         TabIndex        =   18
         ToolTipText     =   "Use the job detail descriptions rather than the rate card descriptions"
         Top             =   240
         Width           =   1935
      End
      Begin VB.CheckBox chkSendEmailOnCompletion 
         Caption         =   "Email on complete"
         Height          =   255
         Left            =   180
         TabIndex        =   20
         ToolTipText     =   "Create an email in Microsoft Outlook when this job has been completed"
         Top             =   540
         Width           =   1635
      End
      Begin VB.CheckBox chkQuote 
         Caption         =   "Quotation"
         Height          =   255
         Left            =   2160
         TabIndex        =   19
         ToolTipText     =   "If this is a Quotation"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CheckBox chkOnHold 
         Caption         =   "On Hold"
         Height          =   255
         Left            =   2160
         TabIndex        =   21
         ToolTipText     =   "Is Job on Hold?"
         Top             =   540
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdAddOrderReference 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9180
      Style           =   1  'Graphical
      TabIndex        =   15
      ToolTipText     =   "Apply a new order reference"
      Top             =   1980
      Width           =   330
   End
   Begin VB.TextBox txtCreated 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000012&
      Height          =   255
      Left            =   11040
      Locked          =   -1  'True
      TabIndex        =   47
      ToolTipText     =   "The job specific fax number"
      Top             =   60
      Width           =   2895
   End
   Begin VB.TextBox txtWorkOrderNumber 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000011&
      Height          =   255
      Left            =   3300
      TabIndex        =   1
      ToolTipText     =   "The unique identifier for this job. In the previous version of CETA, this was known as the 'Booking Number'."
      Top             =   420
      Width           =   915
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   0
      Left            =   9180
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Send email to this contact"
      Top             =   780
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin VB.TextBox txtClientStatus 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   8340
      Locked          =   -1  'True
      TabIndex        =   17
      ToolTipText     =   "Current client's status (On hold, cash, ok etc)"
      Top             =   60
      Width           =   1155
   End
   Begin VB.CommandButton cmdLaunch 
      Appearance      =   0  'Flat
      Height          =   330
      Index           =   1
      Left            =   9180
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Open this company's details in the company editor"
      Top             =   420
      UseMaskColor    =   -1  'True
      Width           =   330
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbJobType 
      Height          =   285
      Left            =   1380
      TabIndex        =   16
      ToolTipText     =   "Select what the main type of work is going to happen on this job"
      Top             =   60
      Width           =   2895
      DataFieldList   =   "Column 0"
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      BevelColorFrame =   -2147483644
      CheckBox3D      =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.TextBox txtFax 
      Height          =   285
      Left            =   6180
      TabIndex        =   13
      ToolTipText     =   "The job specific fax number"
      Top             =   1500
      Width           =   2895
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      Height          =   315
      Left            =   300
      ScaleHeight     =   315
      ScaleWidth      =   6615
      TabIndex        =   36
      Top             =   13500
      Width           =   6615
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   315
         Left            =   4140
         TabIndex        =   25
         ToolTipText     =   "Save changes to this job"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   5400
         TabIndex        =   26
         ToolTipText     =   "Close this form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   1620
         TabIndex        =   23
         ToolTipText     =   "Clear the form"
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   2880
         TabIndex        =   24
         ToolTipText     =   "Print this job"
         Top             =   0
         Width           =   1155
      End
   End
   Begin VB.TextBox txtSubTitle 
      Height          =   285
      Left            =   1380
      TabIndex        =   6
      ToolTipText     =   "The sub title (if known)"
      Top             =   2220
      Width           =   3315
   End
   Begin VB.TextBox txtTitle 
      Height          =   285
      Left            =   1380
      TabIndex        =   5
      ToolTipText     =   "The title for the job"
      Top             =   1860
      Width           =   3315
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   285
      Left            =   6180
      TabIndex        =   8
      ToolTipText     =   "The company this job is for"
      Top             =   420
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   8
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "cetaclientcode"
      Columns(4).Name =   "cetaclientcode"
      Columns(4).DataField=   "cetaclientcode"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "accountstatus"
      Columns(5).Name =   "accountstatus"
      Columns(5).DataField=   "accountstatus"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "fax"
      Columns(6).Name =   "fax"
      Columns(6).DataField=   "fax"
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "email"
      Columns(7).Name =   "email"
      Columns(7).DataField=   "email"
      Columns(7).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.TextBox txtOrderReference 
      Height          =   285
      Left            =   6180
      TabIndex        =   14
      ToolTipText     =   "The clients purchase order reference number"
      Top             =   1980
      Width           =   2895
   End
   Begin VB.TextBox txtTelephone 
      Height          =   285
      Left            =   6180
      TabIndex        =   12
      ToolTipText     =   "The job specific telephone number"
      Top             =   1140
      Width           =   2895
   End
   Begin VB.TextBox txtAccountCode 
      BackColor       =   &H00FFC0FF&
      Height          =   285
      Left            =   6180
      TabIndex        =   7
      ToolTipText     =   "The company account code"
      Top             =   60
      Width           =   975
   End
   Begin VB.TextBox txtJobID 
      BackColor       =   &H80000004&
      Height          =   285
      Left            =   1380
      TabIndex        =   0
      ToolTipText     =   "The current job ID. You can load an existing job by typing it's ID in to this box then pressing return"
      Top             =   420
      Width           =   1035
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbContact 
      Height          =   285
      Left            =   6180
      TabIndex        =   10
      ToolTipText     =   "The contact for this job"
      Top             =   780
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   5424
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "telephone"
      Columns(1).Name =   "telephone"
      Columns(1).DataField=   "telephone"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "contactID"
      Columns(2).Name =   "contactID"
      Columns(2).DataField=   "contactID"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "email"
      Columns(3).Name =   "email"
      Columns(3).DataField=   "email"
      Columns(3).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoJobDetail 
      Height          =   330
      Left            =   300
      Top             =   13920
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoJobDetail"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoRateCard 
      Height          =   330
      Left            =   300
      Top             =   14340
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   8
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoRateCard"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProduct 
      Height          =   285
      Left            =   1380
      TabIndex        =   4
      ToolTipText     =   "The product for this job. Normally associated with a project."
      Top             =   1500
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   8438015
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "productID"
      Columns(0).Name =   "productID"
      Columns(0).DataField=   "productID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5212
      Columns(1).Caption=   "Name"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "name"
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   8438015
      DataFieldToDisplay=   "name"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbAllocation 
      Height          =   285
      Left            =   1380
      TabIndex        =   2
      ToolTipText     =   "Select the job allocation. ie. Is this part of a project, or a regular (one off) job?"
      Top             =   780
      Width           =   2895
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   196617
      DataMode        =   2
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      Columns(0).Width=   4868
      Columns(0).Caption=   "Description"
      Columns(0).Name =   "description"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 0"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbProject 
      Height          =   285
      Left            =   1380
      TabIndex        =   3
      ToolTipText     =   "The referencce number of this project. In the previous version of CETA, this was known as the 'Special Job Number'"
      Top             =   1140
      Visible         =   0   'False
      Width           =   2895
      DataFieldList   =   "projectnumber"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      CheckBox3D      =   0   'False
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   12648447
      RowHeight       =   423
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "projectID"
      Columns(0).Name =   "projectID"
      Columns(0).DataField=   "projectID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2566
      Columns(1).Caption=   "Project Number"
      Columns(1).Name =   "name"
      Columns(1).DataField=   "projectnumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3519
      Columns(2).Caption=   "Product"
      Columns(2).Name =   "productname"
      Columns(2).DataField=   "productname"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Title"
      Columns(3).Name =   "Title"
      Columns(3).DataField=   "title"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3757
      Columns(4).Caption=   "Company"
      Columns(4).Name =   "companyname"
      Columns(4).DataField=   "companyname"
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Contact"
      Columns(5).Name =   "contactname"
      Columns(5).DataField=   "contactname"
      Columns(5).FieldLen=   256
      Columns(6).Width=   3200
      Columns(6).Caption=   "Our Contact"
      Columns(6).Name =   "ourcontact"
      Columns(6).DataField=   "ourcontact"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   3200
      Columns(7).Caption=   "Status"
      Columns(7).Name =   "status"
      Columns(7).DataField=   "fd_status"
      Columns(7).FieldLen=   256
      Columns(8).Width=   3200
      Columns(8).Visible=   0   'False
      Columns(8).Caption=   "productID"
      Columns(8).Name =   "productID"
      Columns(8).DataField=   "productID"
      Columns(8).FieldLen=   256
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "companyID"
      Columns(9).Name =   "companyID"
      Columns(9).DataField=   "companyID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "contactID"
      Columns(10).Name=   "contactID"
      Columns(10).DataField=   "contactID"
      Columns(10).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   12648447
      DataFieldToDisplay=   "projectnumber"
   End
   Begin MSAdodcLib.Adodc adoHistory 
      Height          =   330
      Left            =   2640
      Top             =   14340
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoHistory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoInclusive 
      Height          =   330
      Left            =   5040
      Top             =   13920
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoInclusive"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComCtl2.DTPicker datDeadlineDate 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   2057
         SubFormatType   =   3
      EndProperty
      Height          =   315
      Left            =   6180
      TabIndex        =   54
      ToolTipText     =   "The default despatch deadline. Remember that each delivery note also has its own despatch deadline!"
      Top             =   2400
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      DateIsNull      =   -1  'True
      Format          =   161021953
      CurrentDate     =   37870
   End
   Begin MSAdodcLib.Adodc adoMediaActions 
      Height          =   330
      Left            =   2640
      Top             =   13920
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMediaActions"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoMedia 
      Height          =   330
      Left            =   5040
      Top             =   14340
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMedia"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label Des 
      Caption         =   "VT Deadline for Despatch"
      Height          =   375
      Index           =   6
      Left            =   4920
      TabIndex        =   55
      Top             =   2340
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Status"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   47
      Left            =   9720
      TabIndex        =   52
      Top             =   2400
      Width           =   1035
   End
   Begin VB.Line Line7 
      BorderColor     =   &H80000010&
      Index           =   2
      X1              =   4860
      X2              =   9420
      Y1              =   1860
      Y2              =   1860
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client P/O #"
      Height          =   255
      Index           =   54
      Left            =   4920
      TabIndex        =   50
      Top             =   1980
      Width           =   1035
   End
   Begin VB.Label lblJobID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7500
      TabIndex        =   49
      Tag             =   "CLEARFIELDS"
      Top             =   13860
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Line Line4 
      BorderColor     =   &H80000010&
      X1              =   9600
      X2              =   9600
      Y1              =   0
      Y2              =   2460
   End
   Begin VB.Line Line3 
      BorderColor     =   &H80000010&
      X1              =   4800
      X2              =   4800
      Y1              =   60
      Y2              =   2460
   End
   Begin VB.Label lblCaption 
      Caption         =   "Created / By"
      Height          =   255
      Index           =   6
      Left            =   9720
      TabIndex        =   48
      Top             =   60
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Old J/N"
      ForeColor       =   &H80000011&
      Height          =   255
      Index           =   52
      Left            =   2520
      TabIndex        =   46
      Top             =   420
      Width           =   615
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client Status"
      Height          =   255
      Index           =   10
      Left            =   7260
      TabIndex        =   45
      Top             =   60
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   49
      Left            =   120
      TabIndex        =   43
      Top             =   780
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project #"
      Height          =   255
      Index           =   33
      Left            =   120
      TabIndex        =   42
      Top             =   1140
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Product"
      Height          =   255
      Index           =   46
      Left            =   120
      TabIndex        =   41
      Top             =   1500
      Width           =   1035
   End
   Begin VB.Label lblProjectID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2220
      TabIndex        =   40
      Tag             =   "CLEARFIELDS"
      Top             =   15360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Fax"
      Height          =   255
      Index           =   35
      Left            =   4920
      TabIndex        =   39
      Top             =   1500
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   34
      Left            =   120
      TabIndex        =   38
      Top             =   60
      Width           =   1035
   End
   Begin VB.Label lblProductID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   2280
      TabIndex        =   37
      Tag             =   "CLEARFIELDS"
      Top             =   15000
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Job ID"
      Height          =   255
      Index           =   16
      Left            =   120
      TabIndex        =   35
      Top             =   420
      Width           =   1035
   End
   Begin VB.Label lblContactID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1200
      TabIndex        =   34
      Tag             =   "CLEARFIELDS"
      Top             =   15000
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCompanyID 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1200
      TabIndex        =   33
      Tag             =   "CLEARFIELDS"
      Top             =   15360
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   9
      Left            =   120
      TabIndex        =   32
      Top             =   2220
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   8
      Left            =   120
      TabIndex        =   31
      Top             =   1860
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Contact"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   4920
      TabIndex        =   30
      Top             =   780
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Telephone"
      Height          =   255
      Index           =   3
      Left            =   4920
      TabIndex        =   29
      Top             =   1140
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4920
      TabIndex        =   28
      Top             =   420
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Client A/C #"
      Height          =   255
      Index           =   1
      Left            =   4920
      TabIndex        =   27
      Top             =   60
      Width           =   1035
   End
End
Attribute VB_Name = "frmJobMedia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub adoMediaActions_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

On Error Resume Next

If Not adoMediaActions.Recordset.EOF And adoMediaActions.Recordset("jobmediaspecID") <> 0 Then
    ShowClipSpec "jobmediaspec", frmJobMedia, adoMediaActions.Recordset("jobmediaspecID"), True, False
    lblJobMediaSpecID.Caption = adoMediaActions.Recordset("jobmediaspecID")
Else
    ShowClipSpec "jobmediaspec", frmJobMedia, 0, True, False
    lblJobMediaSpecID.Caption = ""
End If

End Sub

Private Sub chkQuote_Click()

If chkQuote.Value = 1 Then
    txtJobID.BackColor = vbLightGreen
Else
    If cmdSave.Enabled = True Then
        txtJobID.BackColor = vbLightBlue
    Else
        txtJobID.BackColor = vbLightRed
    End If
End If

End Sub

Public Sub cmbAllocation_Click()

If LCase(GetData("allocation", "allocationtype", "name", cmbAllocation.Text)) = "automatic" Then
    cmbProject.Enabled = False
Else
    cmbProject.Enabled = True
End If

End Sub

Private Sub cmbAllocation_DropDown()
PopulateCombo "allocation", cmbAllocation
End Sub

Private Sub cmbCompany_CloseUp()

txtAccountCode.Text = cmbCompany.Columns("accountcode").Text
txtTelephone.Text = cmbCompany.Columns("telephone").Text
txtFax.Text = cmbCompany.Columns("fax").Text
lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
txtClientStatus.Text = Trim(" " & cmbCompany.Columns("accountstatus").Text)

End Sub


Private Sub cmbCompany_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus, email FROM company WHERE name LIKE '" & QuoteSanitise(cmbCompany.Text) & "%' AND (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmbContact_Click()

If cmbContact.Columns("telephone").Text <> "" Then txtTelephone.Text = cmbContact.Columns("telephone").Text

lblContactID.Caption = cmbContact.Columns("contactID").Text

End Sub

Private Sub cmbContact_DropDown()

If lblCompanyID.Caption = "" Then
    NoCompanySelectedMessage
    Exit Sub
End If

Dim l_strSQL As String

l_strSQL = "SELECT company.companyID, contact.contactID, contact.name, contact.telephone, employee.jobtitle, contact.email FROM contact INNER JOIN (company INNER JOIN employee ON company.companyID = employee.companyID) ON contact.contactID = employee.contactID WHERE (((company.companyID)=" & lblCompanyID.Caption & ")) AND contact.system_active = 1 ORDER BY contact.name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbContact.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub cmbDestination_GotFocus()

PopulateCombo "clipdelivery", cmbDestination
HighLite cmbDestination

End Sub

Private Sub cmbJobType_DropDown()
PopulateCombo "jobtype", cmbJobType
HighLite cmbJobType
End Sub

Private Sub cmbMediaSpec_CloseUp()

ShowClipSpec "mediaspec", frmJobMedia, cmbMediaSpec.Columns("mediaspecID").Text, False, False

End Sub

Private Sub cmbProject_Click()

If cmbProject.Rows = 0 Then Exit Sub

lblProjectID.Caption = cmbProject.Columns("projectID").Text
txtTitle.Text = GetData("project", "subtitle", "projectID", Val(lblProjectID.Caption))


cmbCompany.Text = cmbProject.Columns("companyname").Text
cmbContact.Text = cmbProject.Columns("contactname").Text
cmbProduct.Text = cmbProject.Columns("productname").Text
lblCompanyID.Caption = cmbProject.Columns("companyID").Text
lblProductID.Caption = cmbProject.Columns("productID").Text
lblContactID.Caption = cmbProject.Columns("contactID").Text

txtAccountCode.Text = GetData("company", "accountcode", "companyID", lblCompanyID.Caption)
txtClientStatus.Text = GetData("company", "accountstatus", "companyID", lblCompanyID.Caption)

txtTelephone.Text = GetData("project", "telephone", "projectID", lblProjectID.Caption)
txtFax.Text = GetData("project", "fax", "projectID", lblProjectID.Caption)
    
cmbAllocation.Text = GetData("project", "allocation", "projectID", lblProjectID.Caption)

End Sub

Private Sub cmbProject_DropDown()

If cmbProject.Tag = "STOP" Then Exit Sub

Dim l_strSQL As String
l_strSQL = "SELECT project.projectnumber, project.title, project.companyname, project.contactname, project.ourcontact, project.fd_status, product.name as productname, product.productID, project.companyID, project.contactID, project.projectID  FROM project LEFT JOIN product ON project.productID = product.productID WHERE (allocation = '" & cmbAllocation.Text & "') AND (projectnumber LIKE '" & QuoteSanitise(cmbProject.Text) & "%') AND (project.fd_status IS NULL OR (project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCELLED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'INVOICED' AND project.fd_status <> 'Sent To Accounts')) ORDER BY projectnumber;"
Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProject.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Public Sub cmbJobType_Click()

On Error GoTo cmbJobType_Error

If cmbJobType.Text = "" Then cmbJobType.Text = g_strDefaultJobType

Exit Sub

cmbJobType_Error:
    
    Resume

'    tab1.Tab = m_intTabSchedule
'    tabReplacementTabs.Tab = GetReplacementTab(m_intTabSchedule)
'End If
End Sub

Private Sub cmbProduct_Click()

lblProductID.Caption = cmbProduct.Columns("productID").Text

'check if projects are live for this product
If Val(lblProjectID.Caption) = 0 And IsProjectLiveForProduct(cmbProduct.Text) = True Then
    Dim l_intMsg As Integer
    l_intMsg = MsgBox("The product you have selected is in use on one or more live projects. Do you want to list them now?", vbQuestion + vbYesNo)
    If l_intMsg = vbYes Then
        
        cmbProduct.DroppedDown = False
            
        Dim l_strSQL As String
        l_strSQL = "SELECT project.projectnumber, project.title, project.companyname, project.contactname, project.fd_status, product.name as productname, project.ourcontact, product.productID, project.companyID, project.contactID, project.projectID  FROM project LEFT JOIN product ON project.productID = product.productID WHERE product.name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' AND project.fd_status <> 'COMPLETED' AND project.fd_status <> 'CANCEL' AND project.fd_status <> 'CANCELLED' AND project.fd_status <> 'INVOICED' AND project.fd_status <> 'SENT TO ACCOUNTS' ORDER BY title;"
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch As ADODB.Recordset
        
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        With l_rstSearch
             .CursorLocation = adUseClient
             .LockType = adLockBatchOptimistic
             .CursorType = adOpenDynamic
             .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch.ActiveConnection = Nothing
        
        Set cmbProject.DataSourceList = l_rstSearch
        
        cmbProject.Enabled = True

        l_conSearch.Close
        Set l_conSearch = Nothing
        
        'open the drop down
        'first stop it from populating with its own details
        cmbProject.Tag = "STOP"
        cmbProject.DroppedDown = True
        cmbProject.Tag = ""
    End If
End If

End Sub

Private Sub cmbProduct_DropDown()

Dim l_strSQL As String
l_strSQL = "SELECT name, productID FROM product WHERE name LIKE '" & QuoteSanitise(cmbProduct.Text) & "%' AND fd_status <> 'Cancelled' AND fd_status <> 'Hidden' AND fd_status <> 'Completed' ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbProduct.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing
End Sub

Private Sub cmbSource_GotFocus()

PopulateCombo "clipdelivery", cmbSource
HighLite cmbSource

End Sub

Private Sub cmdAddDespatchNotes_Click()
If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "notes3", txtInsertNotes.Text
txtDespatch.Text = GetData("job", "notes3", "jobID", Val(txtJobID.Text))

End Sub

Private Sub cmdAddOrderReference_Click()
If Not CheckAccess("/updateorderreference") Then Exit Sub

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

Dim l_strInputOrder As String

l_strInputOrder = InputBox("Please enter the new order referenece", "Update Order Reference", txtOrderReference.Text)

If l_strInputOrder <> txtOrderReference.Text And l_strInputOrder <> "" Then
    SetData "job", "orderreference", "jobID", txtJobID.Text, UCase(l_strInputOrder)
    AddJobHistory txtJobID.Text, "Updated Order Reference from " & txtOrderReference.Text & " to " & UCase(l_strInputOrder)
    txtOrderReference.Text = UCase(l_strInputOrder)
    
    RefreshJobHistory txtJobID.Text
End If

End Sub

Private Sub cmdAddProducerNotes_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If

InsertNote Val(txtJobID.Text), "notes5", txtInsertNotes.Text
txtProducerNotes.Text = GetData("job", "notes5", "jobID", Val(txtJobID.Text))

End Sub

Private Sub cmdClear_Click()

ShowMediaJob -1
cmbJobType.Text = g_strDefaultJobType
cmbAllocation.Text = g_strDefaultJobAllocation

End Sub

Private Sub cmdClearAction_Click()

ShowClipSpec "mediaspec", frmJobMedia, 0, True, False

End Sub

Private Sub cmdLaunch_Click(Index As Integer)

Dim l_strSearch As String

Select Case Index
Case 0 ' send email to contact
    
    If Val(lblContactID.Caption) <> 0 Then EmailContact lblContactID.Caption
    
Case 1 ' view company website
    'Dim l_strWebsite As String
    'l_strWebsite = GetData("company", "website", "companyID", lblCompanyID.Caption)
    'OpenBrowser l_strWebsite
    
    ShowCompany Val(lblCompanyID.Caption)
    
Case 5 ' change status
    
    If Val(txtJobID.Text) = 0 Then
        NoJobSelectedMessage
        Exit Sub
    End If
    
    'save the job first
    cmdSave.Value = True
    
    txtStatus.Text = ChangeJobStatus(Val(txtJobID.Text))
    ShowJob Val(txtJobID.Text), g_intDefaultTab, False
'    ShowJob Val(txtJobID.Text), g_intDefaultTab, False
    
End Select
End Sub


Private Sub cmdPrint_Click()

If Val(txtJobID.Text) > 0 Then
    
    Dim l_strReportToPrint As String
    
    l_strReportToPrint = g_strLocationOfCrystalReportFiles & "jobsheetmedia.rpt"
    
    PrintCrystalReport l_strReportToPrint, "{job.jobID} = " & txtJobID.Text, g_blnPreviewReport
    
Else
    NoJobSelectedMessage
End If


End Sub

Private Sub cmdReplaceDespatch_Click()

Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes3", "jobID", Val(txtJobID.Text), ""
cmdAddDespatchNotes.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Despatch Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub
Private Sub cmdAddOffice_Click()

If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "notes1", txtInsertNotes.Text
txtOfficeClient.Text = GetData("job", "notes1", "jobID", Val(txtJobID.Text))

End Sub
Private Sub cmdReplaceOffice_Click()
Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes1", "jobID", Val(txtJobID.Text), ""
cmdAddOffice.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Office Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub

Private Sub cmdAddOperatorNotes_Click()
If Val(txtJobID.Text) = 0 Then
    NoJobSelectedMessage
    Exit Sub
End If
InsertNote Val(txtJobID.Text), "notes2", txtInsertNotes.Text
If g_optAlsoInsertOpNotesToAccounts Then InsertNote Val(txtJobID.Text), "notes4", "OP: " & txtInsertNotes.Text
txtOperator.Text = GetData("job", "notes2", "jobID", Val(txtJobID.Text))

End Sub
Private Sub cmdReplaceOperator_Click()
Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes2", "jobID", Val(txtJobID.Text), ""
cmdAddOperatorNotes.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Operator Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub

Private Sub cmdClose_Click()
Me.Hide
DoEvents
End Sub

Private Sub cmdReplaceProducerNotes_Click()

Dim l_intQuestion As Integer
l_intQuestion = MsgBox("Are you sure you want to replace these notes?", vbQuestion + vbYesNo)
If l_intQuestion = vbNo Then Exit Sub
SetData "job", "notes5", "jobID", Val(txtJobID.Text), ""
cmdAddProducerNotes.Value = True
AddJobHistory Val(txtJobID.Text), "Replaced Producer Notes"
RefreshJobHistory Val(txtJobID.Text)
End Sub

Private Sub cmdSave_Click()

If Val(lblCompanyID.Caption) = 0 Then
    NoCompanySelectedMessage
    cmbCompany.SetFocus
    Exit Sub
End If

If lblJobID.Caption <> txtJobID.Text Then
    txtJobID.Text = lblJobID.Caption
    MsgBox "You have tried to change this job's 'Job ID'. You can not do this!" & vbCrLf & vbCrLf & "The job you are viewing should be job ID " & lblJobID.Caption & " so the jobID textbox has now be reverted back to it. You can then either re-save if required, or load the new job ID by pressing return while in the job ID box.", vbExclamation
    Exit Sub
End If

Dim l_intMsg As Integer

'check if a title exists
If txtTitle.Text = "" Then
    l_intMsg = MsgBox("You have not entered a title." & vbCrLf & vbCrLf & "Are you sure you want to save without one?", vbExclamation + vbYesNo)
    If l_intMsg = vbYes Then
        txtTitle.Text = "TBA"
    Else
        Exit Sub
    End If
End If

If g_intDisableProjects = 1 Then cmbAllocation.Text = "Regular"

If cmbAllocation.Text = "" And g_intDisableProjects <> 1 Then
    MsgBox "You need to specify this job's project type. Please correct and try again", vbExclamation
    Exit Sub
End If

If Val(lblContactID.Caption) = 0 Then

    If cmbContact.Text <> "" Then
        frmAddSelectContact.txtContactName(0).Text = cmbContact.Text
        If InStr(cmbContact.Text, " ") <> 0 Then
            frmAddSelectContact.txtContactName(1).Text = Left(cmbContact.Text, InStr(cmbContact.Text, " ") - 1)
            frmAddSelectContact.txtContactName(2).Text = Right(cmbContact.Text, Len(cmbContact.Text) - InStr(cmbContact.Text, " "))
        End If
        frmAddSelectContact.txtContactName(3).Text = txtTelephone.Text
    End If
    
    frmAddSelectContact.lblCompanyID.Caption = lblCompanyID.Caption
    frmAddSelectContact.cmdLoadContacts.Value = True
    frmAddSelectContact.Show vbModal
    
    Dim l_lngContactID As Long
    l_lngContactID = frmAddSelectContact.Tag
    
    'if the user has not selected a valid contact...
    If l_lngContactID = "0" Then
        NoContactSelectedMessage
        cmbContact.SetFocus
        Exit Sub
    Else
        lblContactID.Caption = l_lngContactID
        cmbContact.Text = GetData("contact", "name", "contactID", l_lngContactID)
    End If
    
End If

If cmbJobType.Text = "" Then
    MsgBox "You must select a job type before saving", vbExclamation
    cmbJobType.SetFocus
    Exit Sub
End If


If cmbAllocation.Text = "" Then
    MsgBox "You must select a project type before saving", vbExclamation
    cmbAllocation.SetFocus
    Exit Sub
End If

SaveMediaJob

If txtClientStatus.Text <> "" And UCase(txtClientStatus.Text) <> "OK" Then
    MsgBox "WARNING: There is a problem with this customers account." & vbCrLf & vbCrLf & "Please check with your accounts department to find out why this customers account status is set to " & txtClientStatus.Text & "." & vbCrLf & vbCrLf & "The system has logged that you have read this message.", vbOKCancel + vbCritical
    AddJobHistory Val(txtJobID.Text), "Account Problem (" & txtClientStatus.Text & ")"
End If

ShowMediaJob Val(txtJobID.Text)

End Sub

Private Sub cmdSaveAction_Click()

If Val(lblJobMediaSpecID.Caption) <> 0 Then
    SaveExistingMediaSpec "jobmediaspec", frmJobMedia, Val(lblJobMediaSpecID.Caption), False
End If

End Sub

Private Sub cmdSaveAsTemplate_Click()

If MsgBox("Are you Sure?", vbYesNo, "This will save these details as a Media Specification") = vbNo Then Exit Sub

Dim l_lngMediaSpecID As Long
Dim l_strSQL As String

'Existing Specification - Check whether the Name has changed, and if it has check whether this is an edit or a new Spec after all

l_lngMediaSpecID = GetData("mediaspec", "mediaspecID", "mediaspecname", cmbMediaSpec.Text)
If l_lngMediaSpecID <> 0 Then

    If MsgBox("Is this a New Specification?", vbYesNo, "A Specification with this name alreasy exists.") = vbYes Then GoTo NEW_CLIP_SAVE
    SaveExistingMediaSpec "mediaspec", frmJobMedia, l_lngMediaSpecID, False

Else

NEW_CLIP_SAVE:

    'New Specification
    l_strSQL = "INSERT INTO mediaspec (mediaspecname, mediaformat, videocodec, "
    l_strSQL = l_strSQL & "audiocodec, purpose, horiz, vert, framerate, videobitrate, audiobitrate, "
    l_strSQL = l_strSQL & "totalbitrate, aspectratio, geometry, delivery, altlocation, "
    l_strSQL = l_strSQL & "graphicoverlay, overlayfilename, overlayopacity, fd_order, encodepasses, cbrvbr, interlace, "
    l_strSQL = l_strSQL & "mediasource, sourceaddress, sourcelogin, sourcepassword, mediadelivery, deliveryaddress, deliverylogin, deliverypassword, "
    l_strSQL = l_strSQL & "otherspecs) VALUES ("
    
    l_strSQL = l_strSQL & "'" & cmbMediaSpec.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbClipformat.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbClipcodec.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbAudioCodec.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbPurpose.Text & "', "
    l_strSQL = l_strSQL & "'" & txtHorizpixels.Text & "', "
    l_strSQL = l_strSQL & "'" & txtVertpixels.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbFrameRate.Text & "', "
    l_strSQL = l_strSQL & "'" & txtVideoBitrate.Text & "', "
    l_strSQL = l_strSQL & "'" & txtAudioBitrate.Text & "', "
    l_strSQL = l_strSQL & "'" & txtBitrate.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbAspect.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbGeometry.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbDelivery.Text & "', "
    l_strSQL = l_strSQL & "'" & txtAltLocation.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbGraphicOverlay.Text & "', "
    l_strSQL = l_strSQL & "'" & txtgraphicOverlayFilename.Text & "', "
    l_strSQL = l_strSQL & "'" & txtGraphicOverlayOpacity.Text & "', "
    l_strSQL = l_strSQL & "'0', "
    l_strSQL = l_strSQL & "'" & cmbPasses.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbCbrVbr.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbInterlace.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbSource.Text & "', "
    l_strSQL = l_strSQL & "'" & txtSourceAddress.Text & "', "
    l_strSQL = l_strSQL & "'" & txtSourceLogin.Text & "', "
    l_strSQL = l_strSQL & "'" & txtSourcePassword.Text & "', "
    l_strSQL = l_strSQL & "'" & cmbDestination.Text & "', "
    l_strSQL = l_strSQL & "'" & txtDestinationAddress.Text & "', "
    l_strSQL = l_strSQL & "'" & txtDestinationLogin.Text & "', "
    l_strSQL = l_strSQL & "'" & txtDestinationPassword.Text & "', "
    l_strSQL = l_strSQL & "'" & txtOtherSpecs.Text & "'); "
        
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngMediaSpecID = g_lngLastID
    
        
End If

End Sub

Private Sub ddnInclusive_CloseUp()

grdJobDetail.Columns("format").Text = ddnInclusive.Columns("format").Text
If grdJobDetail.Columns("description").Text = "" Then grdJobDetail.Columns("description").Text = ddnInclusive.Columns("description").Text

End Sub


Private Sub ddnOthers_ValidateList(Text As String, RtnPassed As Integer)
    
    If Text = "" Then Exit Sub

    Dim l_strSQL As String
    l_strSQL = "SELECT description, format FROM xref WHERE category = 'other' AND (description LIKE '" & Text & "%' OR descriptionalias LIKE '" & Text & "%') ORDER BY description"
    
    Dim l_rstXRefRatio As ADODB.Recordset
    Set l_rstXRefRatio = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError

    If l_rstXRefRatio.EOF Then
        'Code note found
        MsgBox "Cannot find this description", 64
        RtnPassed = False
    Else
        l_rstXRefRatio.MoveFirst
        RtnPassed = True
        Text = l_rstXRefRatio("description")
        If MsgBox("Overwrite Description?", vbYesNo) = vbYes Then
            grdJobDetail.Columns("description").Text = Text
        End If
        grdJobDetail.Columns("format").Text = Trim(" " & l_rstXRefRatio("format"))
    End If
    
    l_rstXRefRatio.Close
    Set l_rstXRefRatio = Nothing
    
    If grdJobDetail.Columns("quantity").Text = "" Then grdJobDetail.Columns("quantity").Text = "1"

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

'populate all the other combos, including ones bound to the grid

If g_optRequireProductBeforeSaving <> 0 Then lblCaption(46).FontBold = True
If g_optRequireOurContactBeforeSaving <> 0 Then lblCaption(12).FontBold = True

l_strSQL = "SELECT name, accountcode, telephone, companyID, fax, accountstatus, email FROM company WHERE (issupplier = 1) ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing
Set cmbCompany.DataSourceList = l_rstSearch

l_strSQL = "SELECT mediaspecID, mediaspecname FROM mediaspec WHERE companyID = 0 ORDER BY fd_order, mediaspecname;"

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set cmbMediaSpec.DataSourceList = l_rstSearch2

l_conSearch.Close
Set l_conSearch = Nothing

PopulateCombo "dformat", ddnFormat
PopulateCombo "other", ddnOthers
PopulateCombo "clipactions", ddnActions
grdMediaActions.Columns("action").DropDownHwnd = ddnActions.hWnd

l_strSQL = "SELECT description, format FROM xref WHERE category = 'inclusive' ORDER BY forder, format"
adoInclusive.ConnectionString = g_strConnection
adoInclusive.RecordSource = l_strSQL
adoInclusive.Refresh

cmbAllocation_Click

End Sub

Private Sub Form_Resize()

On Error Resume Next

If Me.WindowState = vbMinimized Then Exit Sub

If Me.Height < 12735 Then
    Me.Height = 12735
End If

If Me.Width < 24060 Then
    Me.Width = 24060
End If

picFooter.Top = Me.ScaleHeight - picFooter.Height - 120
picFooter.Left = Me.ScaleWidth - picFooter.Width - 120

tabMediaSpecs.Height = picFooter.Top - tabMediaSpecs.Top - 120
tabMediaSpecs.Width = Me.ScaleWidth - tabMediaSpecs.Left - 120
If tabMediaSpecs.Width > 21300 Then tabMediaSpecs.Width = 21300
'picSpecs.Width = tabMediaSpecs.Width - picSpecs.Left - 60

grdJobDetail.Height = tabMediaSpecs.Height - grdJobDetail.Top - 120
grdMediaFiles.Width = tabMediaSpecs.Width - 240
grdMediaFiles.Height = tabMediaSpecs.Height / 2 - grdMediaFiles.Top - 120

grdJobDetail.Width = grdMediaFiles.Width
DoEvents

End Sub

Private Sub Form_Unload(Cancel As Integer)

'Me.Hide
'DoEvents

'Cancel = True
cmdClose_Click

End Sub

Private Sub grdJobDetail_BeforeUpdate(Cancel As Integer)

On Error GoTo grdJobDetail_BeforeUpdate_Error

grdJobDetail.Columns("jobID").Text = txtJobID.Text

If grdJobDetail.Columns("copytype").Text = "" Then
    MsgBox "You must specify a copy type (first column M, C, O etc)", vbExclamation
    Cancel = True
    grdJobDetail.SetFocus
    Exit Sub
End If


'for master and copy lines
If (UCase(grdJobDetail.Columns("copytype").Text) = "C" Or UCase(grdJobDetail.Columns("copytype").Text) = "M") Then
    
    If UCase(grdJobDetail.Columns("copytype").Text) = "C" And grdJobDetail.Columns("format").Text = "" Then
        MsgBox "You must specify a format for ALL COPY lines!", vbExclamation
        Cancel = True
        grdJobDetail.SetFocus
        Exit Sub
    End If
    
    If g_optHarshCheckingOnDubbingEntry = 1 Then
        
        If UCase(grdJobDetail.Columns("copytype").Text) = "C" And grdJobDetail.Columns("ratio").Text = "" Then
            MsgBox "You must specify an aspect ratio for ALL COPY lines!", vbExclamation
            Cancel = True
            grdJobDetail.SetFocus
            Exit Sub
        End If
        
        If grdJobDetail.Columns("standard").Text = "" Then
            MsgBox "You must specify a video standard for ALL lines!", vbExclamation
            Cancel = True
            grdJobDetail.SetFocus
            Exit Sub
        End If
        
        If Val(grdJobDetail.Columns("quantity").Text) = 0 Then
            MsgBox "You must specify a quantity for ALL lines!", vbExclamation
            Cancel = True
            grdJobDetail.SetFocus
            Exit Sub
        End If
        
    End If
    
End If

If grdJobDetail.Columns("fd_orderby").Text = "" Then

    Dim l_lngOrderby As Long
    l_lngOrderby = GetCount("SELECT COUNT(jobdetailID) FROM jobdetail WHERE jobID = '" & txtJobID.Text & "';")
        
    grdJobDetail.Columns("fd_orderby").Text = l_lngOrderby
End If

Exit Sub

grdJobDetail_BeforeUpdate_Error:

MsgBox Err.Number & " " & Err.Description, vbExclamation
Exit Sub

End Sub

Private Sub grdJobDetail_GotFocus()

'let the user know they cant do this without a job selected
If Not IsNumeric(txtJobID.Text) Then
    NoJobSelectedMessage
    txtJobID.SetFocus
End If

Dim l_intMsg As Integer

If Not IsDate(GetData("job", "deadlinedate", "jobID", txtJobID.Text)) Then
    'l_intMsg = MsgBox("You must enter and save a valid deadline for dubbing jobs." & vbCrLf & vbCrLf & "Do you want to save now?", vbQuestion + vbOK)
    
    MsgBox "You must enter and SAVE a valid deadline for dubbing jobs", vbExclamation
    
    'If l_intMsg = vbCancel Then
        If datDeadlineDate.Enabled = True Then datDeadlineDate.SetFocus
        Exit Sub
    'Else
    '    cmdSave.Value = True
    'End If
    
End If


End Sub

Private Sub grdJobDetail_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If g_optUpperCaseDubbingDetails = 1 Then
    grdJobDetail.Columns("copytype").Text = UCase(grdJobDetail.Columns("copytype").Text)
    grdJobDetail.Columns("description").Text = UCase(grdJobDetail.Columns("description").Text)
End If

'only show drop downs for relevant copy types
grdJobDetail.Columns("description").DropDownHwnd = 0

grdJobDetail.Columns("format").DropDownHwnd = 0

Dim l_strTitle As String

'what copy type (master copy etc)
Select Case UCase(grdJobDetail.Columns("copytype").Text)
Case "M"
    
    l_strTitle = txtTitle.Text
    
    If g_optUpperCaseDubbingDetails = 1 Then l_strTitle = UCase(l_strTitle)
    
    If LastCol = 0 And g_optUseTitleInMaster <> 0 Then
        If grdJobDetail.Columns("description").Text = "" Then grdJobDetail.Columns("description").Text = l_strTitle
    End If
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd

Case "A"
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd

Case "C"
    
    l_strTitle = txtTitle.Text
    
    If g_optUpperCaseDubbingDetails = 1 Then l_strTitle = UCase(l_strTitle)
    
    If LastCol = 0 Then
        If g_optUseTitleInCopy <> 0 Then
            If grdJobDetail.Columns("description").Text = "" Then grdJobDetail.Columns("description").Text = l_strTitle
        Else
            If grdJobDetail.Columns("description").Text = "" Then grdJobDetail.Columns("description").Text = "Please make copies"
        End If
    End If
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd

Case "O"
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnOthers.hWnd

Case "I"
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnInclusive.hWnd

Case "S"
    
    If LastCol = 0 Then
        If grdJobDetail.Columns("description").Text = "" Then grdJobDetail.Columns("description").Text = "SOURCE SUPPLIED BY CLIENT"
    End If
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd

Case "D"
    
    l_strTitle = txtTitle.Text
    
    If g_optUpperCaseDubbingDetails = 1 Then l_strTitle = UCase(l_strTitle)
    
    If LastCol = 0 And g_optUseTitleInMaster <> 0 Then
        If grdJobDetail.Columns("description").Text = "" Then grdJobDetail.Columns("description").Text = l_strTitle
    End If
    
    grdJobDetail.Columns("format").DropDownHwnd = ddnFormat.hWnd
End Select

End Sub

Private Sub grdMediaActions_BeforeUpdate(Cancel As Integer)

grdMediaActions.Columns("jobID").Text = txtJobID.Text

If grdMediaActions.Columns("fd_order").Text = "" Then

    Dim l_lngOrderby As Long
    l_lngOrderby = GetCount("SELECT COUNT(jobmediaspecID) FROM jobmediaspec WHERE jobID = '" & txtJobID.Text & "';")
        
    grdMediaActions.Columns("fd_order").Text = l_lngOrderby
    
End If

End Sub

Private Sub grdMediaFiles_DblClick()
If grdMediaFiles.Rows > 0 Then ShowClipControl grdMediaFiles.Columns("eventID").Text
End Sub

Private Sub txtAccountCode_GotFocus()
HighLite txtAccountCode
End Sub

Private Sub txtAccountCode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    
    Dim l_strCompanyName As String
    l_strCompanyName = GetData("company", "name", "accountcode", txtAccountCode.Text)
    If l_strCompanyName <> "" Then
       
        cmbCompany.Text = l_strCompanyName
       
        Dim l_strSQL As String
        Dim l_lngCompanyID As Long
        
        l_lngCompanyID = GetData("company", "companyID", "accountcode", txtAccountCode.Text)
        lblCompanyID.Caption = l_lngCompanyID
        txtClientStatus.Text = GetData("company", "accountstatus", "companyID", l_lngCompanyID)
        
        If UCase(txtClientStatus.Text) = "HOLD" Or UCase(txtClientStatus.Text) = "CASH" Then
            txtClientStatus.BackColor = vbRed
        Else
            txtClientStatus.BackColor = vbWindowBackground
        End If
     
    End If
    
End If

End Sub


Private Sub txtDespatch_GotFocus()
HighLite txtDespatch
End Sub

Private Sub txtInsertNotes_GotFocus()
HighLite txtInsertNotes
End Sub


Private Sub txtJobID_GotFocus()
HighLite txtJobID
End Sub

Private Sub txtJobID_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtJobID.Text) Then
        ShowMediaJob Val(txtJobID.Text)
    End If
    HighLite txtJobID
End If

End Sub

Private Sub txtOfficeClient_GotFocus()
HighLite txtOfficeClient
End Sub

Private Sub txtOperator_GotFocus()
HighLite txtOperator
End Sub

Private Sub txtOrderReference_GotFocus()
HighLite txtOrderReference
End Sub

Private Sub txtSubTitle_GotFocus()
HighLite txtSubTitle
End Sub

Private Sub txtTelephone_GotFocus()
HighLite txtTelephone
End Sub

Private Sub txtTitle_GotFocus()
HighLite txtTitle
End Sub

Private Sub txtWorkOrderNumber_GotFocus()
HighLite txtWorkOrderNumber
End Sub

Private Sub txtWorkOrderNumber_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    KeyAscii = 0
    If IsNumeric(txtWorkOrderNumber.Text) Then
        ShowJob GetData("job", "jobID", "workordernumber", txtWorkOrderNumber.Text), g_intDefaultTab, True
'        ShowJob GetData("job", "jobID", "workordernumber", txtWorkOrderNumber.Text), g_intDefaultTab, True
    End If
    HighLite txtWorkOrderNumber
End If

End Sub

Private Sub cmbAspect_GotFocus()
PopulateCombo "aspectratio", cmbAspect
HighLite cmbAspect
End Sub

Private Sub cmbAudioCodec_GotFocus()
PopulateCombo "audiocodec", cmbAudioCodec
HighLite cmbAudioCodec
End Sub

Private Sub cmbClipcodec_GotFocus()
PopulateCombo "clipcodec", cmbClipcodec
HighLite cmbClipcodec
End Sub

Private Sub cmbClipformat_GotFocus()
PopulateCombo "clipformat", cmbClipformat
HighLite cmbClipformat
End Sub

Private Sub cmbDelivery_GotFocus()
PopulateCombo "clipdelivery", cmbDelivery
HighLite cmbDelivery
End Sub

Private Sub cmbFrameRate_GotFocus()
PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate
End Sub

Private Sub cmbGeometry_GotFocus()
PopulateCombo "geometry", cmbGeometry
HighLite cmbGeometry
End Sub

Private Sub cmbGraphicOverlay_GotFocus()
PopulateCombo "yes-no", cmbGraphicOverlay
HighLite cmbGraphicOverlay
End Sub

Private Sub cmbInterlace_GotFocus()
PopulateCombo "interlace", cmbInterlace
End Sub


Private Sub cmbPasses_GotFocus()
PopulateCombo "encodepasses", cmbPasses
HighLite cmbPasses
End Sub

Private Sub cmbPurpose_GotFocus()
PopulateCombo "clippurpose", cmbPurpose
HighLite cmbPurpose
End Sub

Private Sub cmbCbrVbr_GotFocus()
PopulateCombo "cbrvbr", cmbCbrVbr
HighLite cmbCbrVbr

End Sub

Private Sub cmbStreamType_GotFocus()

PopulateCombo "clipstreamtype", cmbStreamType
HighLite cmbStreamType

End Sub

