VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmEditATSProfiles 
   Caption         =   "Edit ATS Profiles"
   ClientHeight    =   9330
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   25410
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   9330
   ScaleWidth      =   25410
   WindowState     =   2  'Maximized
   Begin RichTextLib.RichTextBox rtxtEditXML 
      Height          =   5115
      Left            =   6540
      TabIndex        =   3
      Top             =   120
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   9022
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"frmEditATSProfiles.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoATSProfiles 
      Height          =   330
      Left            =   540
      Top             =   8520
      Visible         =   0   'False
      Width           =   1905
      _ExtentX        =   3360
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoATSProfiles"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   12960
      TabIndex        =   0
      Top             =   8400
      Width           =   5835
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save Text Back to Profile"
         Height          =   315
         Left            =   2100
         TabIndex        =   4
         Top             =   0
         Width           =   2415
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   4620
         TabIndex        =   1
         Top             =   0
         Width           =   1215
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdProfiles 
      Bindings        =   "frmEditATSProfiles.frx":0080
      Height          =   6075
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   6045
      ScrollBars      =   3
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12648447
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Caption=   "ATS_Profile_ID"
      Columns(0).Name =   "ATS_Profile_ID"
      Columns(0).DataField=   "ATS_Profile_ID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "ATS_Profile_Name"
      Columns(1).Name =   "ATS_Profile_Name"
      Columns(1).DataField=   "ATS_Profile_Name"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Edit"
      Columns(2).Name =   "Edit"
      Columns(2).DataField=   "Column 3"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Style=   4
      _ExtentX        =   10663
      _ExtentY        =   10716
      _StockProps     =   79
      Caption         =   "ATS Profiles"
      BackColor       =   -2147483643
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmEditATSProfiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_blnTabStop() As Boolean

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdSave_Click()

Dim SQL As String

If MsgBox("Are you sure you wish to save this teat back to profile: " & grdProfiles.Columns("ATS_Profile_ID").Text & "Name: " & grdProfiles.Columns("ATS_Profile_Name").Text, vbYesNo + vbDefaultButton2) = vbYes Then

    SQL = "UPDATE ATS_Profile SET ATS_Profile_XML_STring = '" & QuoteSanitise(rtxtEditXML.Text) & "' WHERE ATS_Profile_ID = " & grdProfiles.Columns("ATS_Profile_ID").Text
    Debug.Print SQL
    ExecuteSQL SQL, g_strExecuteError
    CheckForSQLError
    MsgBox "Done"
    
End If

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM ATS_Profile ORDER BY ATS_Profile_Name"
adoATSProfiles.ConnectionString = g_strConnection
adoATSProfiles.RecordSource = l_strSQL
adoATSProfiles.Refresh

End Sub

Private Sub Form_Resize()

On Error Resume Next
fraButtons.Top = Me.ScaleHeight - fraButtons.Height - 120
fraButtons.Left = Me.ScaleWidth - fraButtons.Width - 120
grdProfiles.Height = Me.ScaleHeight - fraButtons.Height - 120 - grdProfiles.Top - 120
rtxtEditXML.Width = Me.ScaleWidth - rtxtEditXML.Left - 120
rtxtEditXML.Height = grdProfiles.Height

End Sub

Private Sub grdProfiles_BtnClick()

rtxtEditXML.Text = GetData("ATS_Profile", "ATS_Profile_XML_String", "ATS_Profile_ID", grdProfiles.Columns("ATS_Profile_ID").Text)

End Sub

Private Sub rtxtEditXML_GotFocus()

'Store the TabStop property for each control on the
'form and then set the TabStop property of each
'control to False
Dim i As Long
ReDim m_blnTabStop(0 To Controls.Count - 1) As Boolean
On Error Resume Next
For i = 0 To Controls.Count - 1
   m_blnTabStop(i) = Controls(i).TabStop
   Controls(i).TabStop = False
Next

End Sub

Private Sub rtxtEditXML_LostFocus()

Dim i As Long
'Restore the Tabstop property for each control on the form
On Error Resume Next
For i = 0 To Controls.Count - 1
   Controls(i).TabStop = m_blnTabStop(i)
Next

End Sub
