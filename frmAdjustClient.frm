VERSION 5.00
Begin VB.Form frmAdjustClient 
   Caption         =   "Adjust Client Details"
   ClientHeight    =   3375
   ClientLeft      =   4185
   ClientTop       =   5295
   ClientWidth     =   8010
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmAdjustClient.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3375
   ScaleWidth      =   8010
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   6780
      TabIndex        =   5
      ToolTipText     =   "Close this form"
      Top             =   3000
      Width           =   1155
   End
   Begin VB.TextBox txtNotes 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   1875
      Left            =   4620
      MultiLine       =   -1  'True
      TabIndex        =   4
      ToolTipText     =   "Extra Notes for the Invoice"
      Top             =   1020
      Width           =   3315
   End
   Begin VB.TextBox txtCompanyName 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   0
      ToolTipText     =   "Client Name"
      Top             =   720
      Width           =   3315
   End
   Begin VB.TextBox txtAddress 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   1275
      Left            =   1200
      MultiLine       =   -1  'True
      TabIndex        =   1
      ToolTipText     =   "Client Address"
      Top             =   1020
      Width           =   3315
   End
   Begin VB.TextBox txtPostCode 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   2
      ToolTipText     =   "Client Postcode"
      Top             =   2340
      Width           =   3315
   End
   Begin VB.TextBox txtCountry 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   1200
      TabIndex        =   3
      ToolTipText     =   "Client Country"
      Top             =   2640
      Width           =   3315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Extra Notes"
      Height          =   255
      Index           =   0
      Left            =   4620
      TabIndex        =   11
      Top             =   720
      Width           =   1035
   End
   Begin VB.Label Label1 
      Caption         =   $"frmAdjustClient.frx":08CA
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   60
      TabIndex        =   10
      Top             =   60
      Width           =   7755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Name"
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   9
      Top             =   720
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Address"
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   8
      Top             =   1020
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Post Code"
      Height          =   255
      Index           =   3
      Left            =   60
      TabIndex        =   7
      Top             =   2340
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Country"
      Height          =   255
      Index           =   4
      Left            =   60
      TabIndex        =   6
      Top             =   2640
      Width           =   1035
   End
End
Attribute VB_Name = "frmAdjustClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()
Me.Hide
End Sub

Private Sub Form_Load()
MakeLookLikeOffice Me
CenterForm Me
End Sub

