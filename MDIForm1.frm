VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "CETA Facilities Management"
   ClientHeight    =   12075
   ClientLeft      =   225
   ClientTop       =   570
   ClientWidth     =   15960
   Icon            =   "MDIForm1.frx":0000
   LinkTopic       =   "MDIForm1"
   ScrollBars      =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Timer tmrStateMonitor 
      Interval        =   1
      Left            =   1500
      Top             =   3240
   End
   Begin VB.Timer tmrPeriod 
      Interval        =   1
      Left            =   1560
      Top             =   2400
   End
   Begin VB.Timer timShutDown 
      Interval        =   60000
      Left            =   240
      Top             =   2220
   End
   Begin MSComDlg.CommonDialog dlgMain 
      Left            =   180
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.Toolbar barHeader 
      Align           =   1  'Align Top
      Height          =   810
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   15960
      _ExtentX        =   28152
      _ExtentY        =   1429
      ButtonWidth     =   1746
      ButtonHeight    =   1429
      Style           =   1
      ImageList       =   "ImageList2"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   16
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Job"
            Key             =   "jobdetail"
            Object.ToolTipText     =   "Show the job detail form or create a new job"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Job List"
            Key             =   "joblisting"
            Object.ToolTipText     =   "Show the job listing form (also dubbing control)"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Find Job"
            Key             =   "jobsearch"
            Object.ToolTipText     =   "Perform a custom search through job in the system"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Library"
            Key             =   "library"
            Object.ToolTipText     =   "Show library info form or create new record"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Find Tape"
            Key             =   "librarysearch"
            Object.ToolTipText     =   "Search through the library"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Media"
            Key             =   "clip"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Find Media"
            Key             =   "clipsearch"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Requests"
            Key             =   "requests"
            Object.ToolTipText     =   "View the Transcode and File Requests"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Despatch"
            Key             =   "createdespatch"
            Object.ToolTipText     =   "Create or load a despatch note"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Desp List"
            Key             =   "despatch"
            Object.ToolTipText     =   "Show a list of incoming / outgoing deliveries"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Find Desp"
            Key             =   "despatchsearch"
            Object.ToolTipText     =   "Search through existing despatch records"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Company"
            Key             =   "company"
            Object.ToolTipText     =   "Show the company address book"
            ImageIndex      =   11
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Issues"
            Key             =   "complaint"
            Object.ToolTipText     =   "Open the Complaints Screen"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Sundry"
            Key             =   "sundry"
            Object.ToolTipText     =   "Create or view sundry costing information"
            ImageIndex      =   16
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Print"
            Key             =   "print"
            Object.ToolTipText     =   "Print the currently open document "
            ImageIndex      =   12
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Log Out"
            Key             =   "logout"
            Object.ToolTipText     =   "Log out of CETA and show the logon form"
            ImageIndex      =   10
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList2 
      Left            =   180
      Top             =   2820
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   -2147483628
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   17
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":0E42
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":13DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":19CC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":1FB3
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":25C2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":2C84
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":3363
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":3A01
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":40B9
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":477F
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":4D81
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":531B
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":5914
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":5EBE
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":645A
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":6A39
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MDIForm1.frx":70A8
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFile_New 
         Caption         =   "&New"
         Begin VB.Menu mnuTools_JobDetail 
            Caption         =   "Job"
            Shortcut        =   ^{F3}
         End
         Begin VB.Menu mnuPurchase_New 
            Caption         =   "Purchase Order"
            Shortcut        =   ^{F4}
         End
         Begin VB.Menu mnuLibrary_New 
            Caption         =   "Library"
            Shortcut        =   ^{F5}
         End
         Begin VB.Menu mnuMediaNew 
            Caption         =   "Media (Clip) Item"
         End
         Begin VB.Menu mnuDespatch_New 
            Caption         =   "Despatch"
            Shortcut        =   ^{F6}
         End
      End
      Begin VB.Menu mnuFile_Save 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFile_Clear 
         Caption         =   "&Clear"
      End
      Begin VB.Menu mnuFile_Bar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFile_Print 
         Caption         =   "&Print"
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuFile_Preview 
         Caption         =   "P&review"
      End
      Begin VB.Menu mnuFile_PrinterSetup 
         Caption         =   "Printer Set&up"
      End
      Begin VB.Menu mnuFile_Bar_2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFile_UserComments 
         Caption         =   "Show User Comments"
         Shortcut        =   +{F1}
      End
      Begin VB.Menu mnuFile_Bar_3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileChangePassword 
         Caption         =   "Change Password"
      End
      Begin VB.Menu mnuFile_Bar10 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFile_Close 
         Caption         =   "Close"
      End
      Begin VB.Menu mnuFile_Bar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFile_Exit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   "&Tools"
      Begin VB.Menu mnuTools_Search 
         Caption         =   "Search"
         Begin VB.Menu mnuTools_JobSearch 
            Caption         =   "Job"
            Shortcut        =   +{F3}
         End
         Begin VB.Menu mnuPurchase_Search 
            Caption         =   "Purchase Orders"
            Shortcut        =   +{F4}
         End
         Begin VB.Menu mnuLibrary_Search 
            Caption         =   "Library"
            Shortcut        =   +{F5}
         End
         Begin VB.Menu mnuClipSearch 
            Caption         =   "Media"
         End
         Begin VB.Menu mnuDespatch_Search 
            Caption         =   "Despatch"
            Shortcut        =   +{F6}
         End
      End
      Begin VB.Menu mnuToolsContactDetails 
         Caption         =   "Contact Details..."
         Begin VB.Menu mnuTools_CompanyDatabase 
            Caption         =   "Company Database"
         End
         Begin VB.Menu mnuTools_ContactDatabase 
            Caption         =   "Contact Database"
         End
      End
      Begin VB.Menu mnuTools_JobList 
         Caption         =   "Job Listing..."
         Begin VB.Menu mnuJobListing_OutStanding 
            Caption         =   "&Outstanding"
         End
         Begin VB.Menu mnuJobListing_Completed 
            Caption         =   "&Completed"
         End
         Begin VB.Menu mnuJobListing_Invoiced 
            Caption         =   "&Invoiced"
         End
         Begin VB.Menu mnuJobListing_SentToAccounts 
            Caption         =   "&Sent To Accounts"
         End
         Begin VB.Menu mnuJobListing_All 
            Caption         =   "&All"
         End
         Begin VB.Menu mnuJobListing_Quotes 
            Caption         =   "&Quotes"
         End
         Begin VB.Menu mnuJobListing_Abandoned 
            Caption         =   "Aba&ndoned"
         End
      End
      Begin VB.Menu mnuDespatch_List 
         Caption         =   "Despatch Listing..."
         Begin VB.Menu mnuDeliveryIncomplete 
            Caption         =   "Incomplete"
         End
         Begin VB.Menu mnuDelivery_Complete 
            Caption         =   "Complete"
         End
         Begin VB.Menu mnuDelivery_All 
            Caption         =   "All"
         End
      End
      Begin VB.Menu mnuDataEntry 
         Caption         =   "Data Entry"
         Begin VB.Menu mnuDataEntry_Job 
            Caption         =   "Job"
         End
         Begin VB.Menu mnuDataEntry_Library 
            Caption         =   "Library"
         End
         Begin VB.Menu mnuDataEntry_Media 
            Caption         =   "Media"
         End
         Begin VB.Menu mnuDataEntry_Despatch 
            Caption         =   "Despatch Note"
         End
      End
      Begin VB.Menu mnuTools_Bar_02 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTools_InternalMovement 
         Caption         =   "Library Internal Movements"
         Shortcut        =   +^{F2}
      End
      Begin VB.Menu mnuTools_ShelfAudit 
         Caption         =   "Library Shelf Audit"
      End
      Begin VB.Menu mnuTools_MakeADub 
         Caption         =   "Library Event Transfer"
      End
      Begin VB.Menu mnuTools_PrepareBarcodeLabels 
         Caption         =   "Prepare Barcode Labels for Blank Stock"
      End
      Begin VB.Menu mnuTools_Bar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTools_EquipmentManage 
         Caption         =   "Equipment Management"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTools_Bar4 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuOffAirs 
         Caption         =   "Off Air Schedule"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu mnuOffAirPortal 
         Caption         =   "Off Air Portal"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu mnuExcelRead 
         Caption         =   "Read Excel Sheets"
      End
      Begin VB.Menu mnuTools_EditMAsterSeriesIDs 
         Caption         =   "Edit Master Series IDs"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTools_BBCAlias 
         Caption         =   "Edit BBC Alias Lists"
      End
      Begin VB.Menu mnuTools_BBCTapeRenumberLookup 
         Caption         =   "Edit BBC Tape Renumber Lookups"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDespatch_AddressAdmin 
         Caption         =   "Edit Delivery Addresses / Skibbly Customers"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuDigitalDestinations 
         Caption         =   "Edit Digital Destinations"
      End
      Begin VB.Menu mnuDigitalDeliveryDestinations 
         Caption         =   "Edit Digital Delivery Destinations"
         Visible         =   0   'False
      End
      Begin VB.Menu mnumediaSpecNew 
         Caption         =   "Edit Media Specifications"
      End
      Begin VB.Menu mnuEditATSProfiles 
         Caption         =   "Edit ATS Profiles"
      End
      Begin VB.Menu mnumediaTranscode 
         Caption         =   "Transcoding Control"
      End
      Begin VB.Menu mnuShowPortalForm 
         Caption         =   "Media Portal Control"
      End
      Begin VB.Menu mnuShowItunesPackage 
         Caption         =   "Platform Package Preparation"
      End
      Begin VB.Menu mnuCetaUserMessages 
         Caption         =   "Ceta User Messages"
      End
      Begin VB.Menu mnuComplaints 
         Caption         =   "Complaints"
      End
      Begin VB.Menu mnuProfitCentre5 
         Caption         =   "Profit Centre 5"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTools_Bar8 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTools_BudgetAllocation 
         Caption         =   "Purchase Order Budget Re-Allocation"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuTools_Bar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTools_Toolbar 
         Caption         =   "Toolbar..."
         Begin VB.Menu mnuToolbar_Top 
            Caption         =   "Top"
            Checked         =   -1  'True
         End
         Begin VB.Menu mnuToolbar_Bottom 
            Caption         =   "Bottom"
         End
         Begin VB.Menu mnuToolbar_Left 
            Caption         =   "Left"
         End
         Begin VB.Menu mnuToolBar_Right 
            Caption         =   "Right"
         End
         Begin VB.Menu mnuToolbar_Hidden 
            Caption         =   "Hidden"
         End
         Begin VB.Menu mnuToolBar_AlignText 
            Caption         =   "Align Text..."
            Begin VB.Menu mnuToolBar_AlignBottom 
               Caption         =   "Bottom"
               Checked         =   -1  'True
            End
            Begin VB.Menu mnuToolBar_AlignRight 
               Caption         =   "Right"
            End
         End
      End
      Begin VB.Menu mnuTools_Supervisor 
         Caption         =   "Supervisor..."
         Begin VB.Menu mnuTools_Purchase_Budgets 
            Caption         =   "Budget Administration"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuTools_Supervisor_Bar3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSupervisor_SendToAccounts 
            Caption         =   "Posting to Accounts and Customers"
         End
         Begin VB.Menu mnuSettingsBar4 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSupervisor_RateCard 
            Caption         =   "Rate Card"
            Shortcut        =   ^{F12}
         End
         Begin VB.Menu mnuSupervisor_ResourceList 
            Caption         =   "&Resource List"
         End
         Begin VB.Menu mnuSupervisor_EditViews 
            Caption         =   "Edit Views"
         End
         Begin VB.Menu mnuSettingsBar3 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSupervisor_FormatStockCodes 
            Caption         =   "Formats && Stock Codes"
         End
         Begin VB.Menu mnuSupervisor_ComboBoxOptions 
            Caption         =   "Combo Box Options"
         End
         Begin VB.Menu mnuSupervisor_UserAdmin 
            Caption         =   "User Administration"
         End
         Begin VB.Menu mnuSupervisor_PublishNotice 
            Caption         =   "Publish a CETA Notice"
         End
         Begin VB.Menu mnuSettings_Bar1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuSupervisor_Settings 
            Caption         =   "&Settings"
         End
         Begin VB.Menu mnuMachineName 
            Caption         =   "What Machine Is This"
         End
      End
      Begin VB.Menu mnuTools_Reports 
         Caption         =   "Reports"
         Begin VB.Menu mnuReports_ProjectOverview 
            Caption         =   "Project Overview"
            Shortcut        =   +^{F11}
         End
         Begin VB.Menu mnuTools_reports_Bar1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuTools_Reports_LateDeliveries 
            Caption         =   "Late Deliveries"
         End
         Begin VB.Menu mnuRepors_CompanyList 
            Caption         =   "Company List"
         End
         Begin VB.Menu mnuReports_CompaniesOnHold 
            Caption         =   "Companies on hold"
         End
         Begin VB.Menu mnuReportsBBCMGMissingMetadata 
            Caption         =   "BBCMG Missing Metadata Report"
         End
         Begin VB.Menu mnuReports_DailyLog 
            Caption         =   "Daily Activity Log"
         End
         Begin VB.Menu mnuReports_MonthlyLog 
            Caption         =   "Monthly Activity Log"
         End
         Begin VB.Menu mnuReports_YTDLog 
            Caption         =   "Year to Date Activity Log"
         End
         Begin VB.Menu mnuDisney_Masterfiles_for_Purge 
            Caption         =   "Disney Masterfiles For Purge"
         End
         Begin VB.Menu mnuReports_BBCCompletions 
            Caption         =   "BBC Completions Report"
         End
         Begin VB.Menu mnuReports_PatheReport 
            Caption         =   "British Pathe Invoice Report"
         End
         Begin VB.Menu mnuReports_BikeReport 
            Caption         =   "Bike Report"
         End
         Begin VB.Menu mnuReports_OutOfLibrary 
            Caption         =   "Out of Library Report"
         End
         Begin VB.Menu mnuReports_Protape 
            Caption         =   "Protape Monthly Stock Order Report"
         End
         Begin VB.Menu mnuReports_TrackerCompleted 
            Caption         =   "Tracker Completed Report"
         End
         Begin VB.Menu mnuReports_Worklist 
            Caption         =   "Work List"
         End
         Begin VB.Menu mnuReports_OutstandingWork 
            Caption         =   "Outstanging Work"
         End
         Begin VB.Menu mnuReports_UninvoicedWork 
            Caption         =   "Uninvoiced Work from Month"
         End
         Begin VB.Menu mnuReports_UninvoicedWorkAll 
            Caption         =   "Uninvoiced Work (All)"
         End
         Begin VB.Menu mnuReports_MotionGallery 
            Caption         =   "Motion Gallery Work Report"
         End
         Begin VB.Menu mnuReports_MediaServicesKPI 
            Caption         =   "Media Services KPI Report"
         End
         Begin VB.Menu mnuReports_EntsRights 
            Caption         =   "Ents Rights Report"
         End
         Begin VB.Menu mnuReports_Weblogins 
            Caption         =   "Online Web Logins"
         End
         Begin VB.Menu mnuReports_WebPortalLogins 
            Caption         =   "Online Web Portal Logins"
         End
         Begin VB.Menu mnuReports_WebDeliveries 
            Caption         =   "Online Web Deliveries"
         End
         Begin VB.Menu mnuReports_WebPortalDeliveries 
            Caption         =   "Online Portal Web Deliveries"
         End
         Begin VB.Menu mnuReports_WebUserIDs 
            Caption         =   "Online Web UserIDs"
         End
         Begin VB.Menu mnuReports_portalUserIDs 
            Caption         =   "Portal Web UserIDs"
         End
         Begin VB.Menu mnuReports_Ratecard 
            Caption         =   "Rate Card"
         End
         Begin VB.Menu mnuTools_Reports_UserRequests 
            Caption         =   "User request list (F1)"
         End
         Begin VB.Menu mnuReports_SessionReport 
            Caption         =   "Login Session report"
         End
         Begin VB.Menu mnuReports_ReprintAccountsBatch 
            Caption         =   "Reprint Accounts Batch"
         End
         Begin VB.Menu mnu_Reports_ReprintSprocketsBatch 
            Caption         =   "Reprint Sprockets Batch"
         End
         Begin VB.Menu mnu_Reports_ReprintBBCBatch 
            Caption         =   "Reprint BBC Batch"
         End
      End
      Begin VB.Menu mnuTools_Bar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTools_Refresh 
         Caption         =   "Refresh"
         Shortcut        =   {F5}
      End
   End
   Begin VB.Menu mnuTrackers 
      Caption         =   "Trackers"
      Begin VB.Menu mnuDADCTracker 
         Caption         =   "DADC Tracker"
      End
      Begin VB.Menu mnuGenericTracker 
         Caption         =   "Generic Tracker"
      End
      Begin VB.Menu mnuGoogleTracker 
         Caption         =   "Google Submissions Tracker"
      End
      Begin VB.Menu mnuiTunesTracker 
         Caption         =   "Platforms Tracker"
      End
      Begin VB.Menu mnuDisneyTracker 
         Caption         =   "Disney Tracker"
      End
      Begin VB.Menu mnuHDDTracker 
         Caption         =   "HDD Tracker"
      End
      Begin VB.Menu mnuMediaServicesTracker 
         Caption         =   "Media Services Tracker"
      End
   End
   Begin VB.Menu mnuWindow 
      Caption         =   "&Window"
      NegotiatePosition=   2  'Middle
      Begin VB.Menu mnuWindow_Cascade 
         Caption         =   "Cascade"
      End
      Begin VB.Menu mnuWindow_Tile 
         Caption         =   "&Tile"
      End
      Begin VB.Menu mnuWindow_ArrangeIcons 
         Caption         =   "Arrange Icons"
      End
      Begin VB.Menu mnuWindowBar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuNewBookingFormToFront 
         Caption         =   "New Booking Form To Front"
      End
      Begin VB.Menu mnuWindowBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOpenWindows 
         Caption         =   "Open Windows..."
         Begin VB.Menu mnuWindowListItem 
            Caption         =   "?"
            Index           =   0
         End
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About"
      End
      Begin VB.Menu mnuHelp_OnLineGuide 
         Caption         =   "On Line Guide"
      End
      Begin VB.Menu mnuHelp_Bar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelp_UserComment 
         Caption         =   "&User Comment"
         Shortcut        =   {F1}
      End
   End
   Begin VB.Menu mnuHiddenEventsOptions 
      Caption         =   "mnuHiddenEventsOptions"
      Visible         =   0   'False
      Begin VB.Menu mnuHiddenEvent_AddToJobAsMaster 
         Caption         =   "Add to job as master"
      End
      Begin VB.Menu mnuHiddenEvent_Bar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHiddenEvent_Copy 
         Caption         =   "Copy"
      End
      Begin VB.Menu mnuHiddenEvent_Paste 
         Caption         =   "Paste"
      End
      Begin VB.Menu mnuHiddenEvent_Bar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHiddenEvent_Delete 
         Caption         =   "Delete"
      End
   End
   Begin VB.Menu mnuHiddenActionItems 
      Caption         =   "mnuHiddenActionItems"
      Visible         =   0   'False
      Begin VB.Menu mnuActionItems_Properties 
         Caption         =   "Properties"
      End
      Begin VB.Menu mnuHiddenActions_Bar 
         Caption         =   "-"
      End
      Begin VB.Menu mnuActionItems_DNotes 
         Caption         =   "Delivery Notes"
      End
      Begin VB.Menu mnuActionItems_Bar0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuActionItems_Action 
         Caption         =   "Action"
         Begin VB.Menu mnuHiddenActionItems_ActionItem 
            Caption         =   "Move"
            Enabled         =   0   'False
            Index           =   0
            Visible         =   0   'False
         End
         Begin VB.Menu mnuHiddenActionItems_ActionItem 
            Caption         =   "Cancel Job"
            Index           =   1
         End
      End
      Begin VB.Menu mnuActionItems_Bar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuActionItems_Print 
         Caption         =   "Print"
         Begin VB.Menu mnuHiddenActionItems_PrintMenuItem 
            Caption         =   "Job Sheet"
            Index           =   0
         End
         Begin VB.Menu mnuHiddenActionItems_PrintMenuItem 
            Caption         =   "Fax Confirmation"
            Enabled         =   0   'False
            Index           =   1
         End
         Begin VB.Menu mnuHiddenActionItems_PrintMenuItem 
            Caption         =   "Costing Breakdown"
            Index           =   2
         End
         Begin VB.Menu mnuHiddenActionItems_PrintMenuItem 
            Caption         =   "Picking List"
            Index           =   3
         End
         Begin VB.Menu mnuHiddenActionItems_PrintMenuItem 
            Caption         =   "Dub Job Sheet"
            Index           =   4
         End
      End
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type POINTAPI
    X As Long
    Y As Long
End Type
    'API function to get the cursor position
    '


Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
    'API function to check the state of the mouse buttons
    'as well as the keyboard.


Private Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer
    'set the length of time the computer must idle, before
    'the so-called "idle-state" is reached.
    '     unit: seconds
    'You'd probably want to change this value!!!
Private Const INTERVAL As Long = 300
Dim IsIdle As Boolean 'True when idling or while in idle-state
Dim MousePos As POINTAPI 'holds mouse position
    'holds time (in seconds) when the idle started
    'used to calculate if the computer has been idle for INTERVAL
Dim startOfIdle As Long

Private Sub barHeader_ButtonClick(ByVal Button As MSComctlLib.Button)

Select Case Button.Key

Case "jobdetail"
    ShowJob 0, g_intDefaultTab, True
'    ShowJob 0, g_intDefaultTab, True
'Case "jobmediadetail"
'    ShowMediaJob 0
Case "joblisting"

    g_strDefaultJobType = GetData("cetauser", "defaultjobtype", "cetauserID", g_lngUserID)
    
    
    If frmJobListing.tabFilter.Visible = True Then
        frmJobListing.tabFilter_Click 0
        
    Else
    
        If Left(LCase(g_strDefaultJobType), 3) = "dub" Then
            ShowJobListing "dubbing control"
        Else
        
            frmJobListing.chkOnlyJobsWithDubs.Value = vbUnchecked
            frmJobListing.tabReplacement.Tab = 3
        End If
        'ShowJobListing "completed / returned"
    End If

    frmJobListing.ZOrder 0
Case "jobsearch"
    ShowJobSearch
Case "library"
    ShowLibrary 0
Case "librarysearch"
    ShowLibrarySearch
Case "clip"
    ShowClipControl 0
Case "clipsearch"
    ShowClipSearch
Case "createdespatch"
    ShowDespatchForJob 0
Case "despatch"
    ShowDespatch "mdibuttonclick"
Case "despatchsearch"
    ShowDespatchSearch
Case "company"
    ShowCompany 0
Case "complaint"
    ShowComplaints
Case "requests"
    frmTranscoding.Show
    frmTranscoding.ZOrder 0
Case "sundry"
    ShowSundry
Case "print"
    On Error Resume Next
    MDIForm1.ActiveForm.cmdPrint.Value = True
Case "logout"
    Logout
End Select

End Sub
 
Private Sub MDIForm_Load()
'Me.Height = 768 * 15
'Me.Width = 1024 * 15

'disable controls when told to!

If g_intEnableLibraryClipManagement = 0 Then
    mnuMediaNew.Visible = False
    mnuClipSearch.Visible = False
    barHeader.Buttons(7).Visible = False
    barHeader.Buttons(8).Visible = False
End If

If g_intDisableEditViews = 1 Then mnuSupervisor_EditViews.Visible = False
If g_intDisableEquipmentManagement = 1 Then mnuTools_EquipmentManage.Visible = False
If g_intDisableLibraryTransfer = 1 Then mnuTools_MakeADub.Visible = False

If g_intDisablePurchaseOrders = 1 Then
    mnuPurchase_New.Visible = False
    mnuPurchase_Search.Visible = False
    barHeader.Buttons("purchase").Visible = False
    mnuTools_Purchase_Budgets.Visible = False
    mnuTools_BudgetAllocation.Visible = False
End If

If g_intDisableResourceList = 1 Then
    mnuSupervisor_ResourceList.Visible = False
End If

If g_intDisableSundry = 1 Then
    barHeader.Buttons("sundry").Visible = False
End If

'If CheckAccess("ShowOffair", 1) = False Then
'    mnuOffAirs.Visible = False
'    mnuOffAirPortal.Visible = False
'Else
'    mnuOffAirs.Visible = True
'    mnuOffAirPortal.Visible = True
'End If

End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Cancel = Not ExitCETA(g_optCloseSystemDown)
End Sub

Private Sub MDIForm_Resize()

If MDIForm1.WindowState = vbMinimized Then Exit Sub

If Me.Height < 12000 Then
    MDIForm1.barHeader.Visible = False
    MDIForm1.mnuToolbar_Hidden.Checked = True
Else
    MDIForm1.barHeader.Visible = True
    MDIForm1.mnuToolbar_Hidden.Checked = False
End If

DoEvents

End Sub

Private Sub mnu_Reports_ReprintBBCBatch_Click()

Dim l_Batchnumber As String
l_Batchnumber = InputBox("Please give the Batch Number", "Reprint Accounts Batch")
If l_Batchnumber <> "" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "BBCBatchreport.rpt", "{job.senttobbcbatchnumber} = " & l_Batchnumber, True
End If

End Sub

Private Sub mnu_Reports_ReprintSprocketsBatch_Click()

Dim l_lngBatchNumber As String
l_lngBatchNumber = InputBox("Please give the Batch Number", "Reprint Sprockets Batch")
If l_lngBatchNumber <> "" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "batchreportsprockets.rpt", "{batchsprockets.batchnumber} = " & l_lngBatchNumber, True
End If

End Sub

Private Sub mnuActionItems_DNotes_Click()


Dim l_lngJobID As Long

Select Case MDIForm1.ActiveForm.Name

Case "frmJobListing"
    l_lngJobID = frmJobListing.grdJobListing.Columns("jobID").Text
    
End Select

ShowDespatchForJob l_lngJobID

End Sub

Private Sub mnuActionItems_Properties_Click()

Dim l_lngJobID As Long

Select Case MDIForm1.ActiveForm.Name

Case "frmJobListing"
    l_lngJobID = frmJobListing.grdJobListing.Columns("jobID").Text
    
End Select

ShowJob l_lngJobID, g_intDefaultTab, True
'ShowJob l_lngJobID, g_intDefaultTab, True
End Sub



Private Sub mnuCetaUserMessages_Click()
frmCetaUserMessages.Show
End Sub

Private Sub mnuClipSearch_Click()
ShowClipSearch
End Sub

Private Sub mnuComplaints_Click()
ShowComplaints
End Sub

Private Sub mnuDADCTracker_Click()

frmTrackerDADC.Show
frmTrackerDADC.ZOrder 0

End Sub

Private Sub mnuDataEntry_Despatch_Click()
ShowDespatchForJob 0
End Sub

Private Sub mnuDataEntry_Job_Click()
ShowJob 0, g_intDefaultTab, True
'ShowJob 0, g_intDefaultTab, True
End Sub

Private Sub mnuDataEntry_Library_Click()
ShowLibrary 0
End Sub

Private Sub mnuDataEntry_Media_Click()
ShowClipControl 0
End Sub

Private Sub mnuDelivery_All_Click()
ShowDespatch "all"
End Sub

Private Sub mnuDelivery_Complete_Click()
ShowDespatch "complete"
End Sub
Private Sub mnuDeliveryIncomplete_Click()
ShowDespatch "incomplete"
End Sub

Private Sub mnuDespatch_AddressAdmin_Click()

frmDeliveryAddress.Show

End Sub

Private Sub mnuDespatch_New_Click()
ShowDespatchForJob 0

End Sub

Private Sub mnuDespatch_Search_Click()
ShowDespatchSearch
End Sub

Private Sub mnuDigitalDeliveryDestinations_Click()

If Val(frmDigitalDeliveryDestination.lblDigitalDeliveryDestinationID.Caption) = 0 Then
    ShowDigitalDeliveryDestination 0, False
Else
    frmClipSpec.ZOrder 0
End If

End Sub

Private Sub mnuDigitalDestinations_Click()
frmDeliveryDestinations.Show
frmDeliveryDestinations.ZOrder 0
End Sub

Private Sub mnuDisney_Masterfiles_for_Purge_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Disney_Masterfiles_for_Purge.rpt", "", True
End Sub

Private Sub mnuDisneyTracker_Click()
frmDisneyTracker.Show
frmDisneyTracker.ZOrder 0
End Sub

Private Sub mnuEditATSProfiles_Click()
frmEditATSProfiles.Show
End Sub

Private Sub mnuExcelRead_Click()
frmExcelRead.Show
End Sub

Private Sub mnuFile_Clear_Click()
On Error Resume Next
MDIForm1.ActiveForm.cmdClear.Value = True
End Sub

Private Sub mnuFile_Close_Click()
On Error Resume Next
Unload MDIForm1.ActiveForm
End Sub

Private Sub mnuFile_Exit_Click()
ExitCETA
End Sub

Private Sub mnuFile_Preview_Click()

Dim l_blnErrorInPreview As Boolean

On Error GoTo Proc_Error

MDIForm1.ActiveForm.cmdPreview.Value = True

Proc_TryPrintButton:

If l_blnErrorInPreview = True Then
    g_blnPreviewReport = True
    MDIForm1.ActiveForm.cmdPrint.Value = True
End If

g_blnPreviewReport = False

Exit Sub

Proc_Error:

If l_blnErrorInPreview = True Then
    Resume Proc_Error2
Else
    l_blnErrorInPreview = True
    Resume Proc_TryPrintButton
End If

Exit Sub

Proc_Error2:

Exit Sub

End Sub

Private Sub mnuFile_Print_Click()
On Error Resume Next
MDIForm1.ActiveForm.cmdPrint.Value = True
End Sub

Private Sub mnuFile_PrinterSetup_Click()
ShowPrinterSetup
End Sub

Private Sub mnuFile_Save_Click()
On Error Resume Next
MDIForm1.ActiveForm.cmdSave.Value = True

End Sub


Private Sub mnuFile_UserComments_Click()
frmUserRequest.Show
End Sub

Private Sub mnuFileChangePassword_Click()

Dim l_intResult As Boolean
l_intResult = ChangeMyPassword()

If l_intResult = True Then
    MsgBox "Password changed successfully", vbInformation
Else
    MsgBox "Password not changed", vbExclamation
End If

End Sub

Private Sub mnuGenericTracker_Click()

frmTracker.Show
frmTracker.ZOrder 0

End Sub

Private Sub mnuGoogleTracker_Click()
frmTrackerGoogle.Show
frmTrackerGoogle.ZOrder 0
End Sub

Private Sub mnuHDDTracker_Click()
frmTrackerHDD.Show
frmTrackerHDD.ZOrder 0
End Sub

Private Sub mnuiTunesTracker_Click()

frmTrackeriTunes.Show
frmTrackeriTunes.ZOrder 0

End Sub

Private Sub mnuMachineName_Click()

MsgBox "You are on " & CurrentMachineName

End Sub

Private Sub mnuMediaServicesTracker_Click()

frmTrackerMediaServices.Show
frmTrackerMediaServices.ZOrder 0

End Sub

Private Sub mnuProfitCentre5_Click()

frmProfitCentre5.Show
frmProfitCentre5.ZOrder 0

End Sub

Private Sub mnuReports_MediaServicesKPI_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "MediaServicesKPIReport.rpt", "", True
End Sub

Private Sub mnuReports_UninvoicedWorkAll_Click()
PrintUninvoicedWorkreportAll
End Sub

Private Sub mnuReports_YTDLog_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "YTDActivity.rpt", "", True
End Sub

Private Sub mnuReportsBBCMGMissingMetadata_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "BBCMGMissingMetadata.rpt", "", True
End Sub

Private Sub mnuHelp_OnLineGuide_Click()
    OpenBrowser "http://www.cetasoft.net/mediawiki"
End Sub

Private Sub mnuHelp_UserComment_Click()

Dim l_strMsg As String
Dim l_strSeverity As String
Dim l_strArea As String
Dim l_strType As String

'l_strMsg = InputBox("Please enter your comment", "Save comment")

keybd_event vbKeySnapshot, 0, 0, 0

frmUserComment.Show vbModal

l_strMsg = frmUserComment.txtComment.Text
l_strSeverity = frmUserComment.cmbSeverity.Text
l_strArea = frmUserComment.cmbArea.Text
l_strType = frmUserComment.cmbType.Text

'did the user cancel?
If frmUserComment.Tag = "CANCELLED" Then l_strMsg = ""

If l_strMsg <> "" Then
    Dim l_strSQL As String
    l_strSQL = "INSERT INTO usercomment (cuser, cdate, comment, commenttype, area, severity, versionadded) VALUES "
    l_strSQL = l_strSQL & "('" & g_strUserInitials & "', '" & FormatSQLDate(Now()) & "', '" & QuoteSanitise(l_strMsg) & "', '" & QuoteSanitise(l_strType) & "', '" & QuoteSanitise(l_strArea) & "','" & QuoteSanitise(l_strSeverity) & "','" & App.Major & "." & App.Minor & "." & App.Revision & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError

    
    Dim l_strbody As String
    l_strbody = "Type        : " & l_strType
    l_strbody = l_strbody & vbCrLf & "Area        : " & l_strArea
    l_strbody = l_strbody & vbCrLf & "Severity    : " & l_strSeverity
    l_strbody = l_strbody & vbCrLf & "Message     : " & l_strMsg
    l_strbody = l_strbody & vbCrLf & "User        : " & g_strFullUserName
    l_strbody = l_strbody & vbCrLf & "Telephone   : " & GetData("cetauser", "telephone", "cetauserID", g_lngUserID)
    l_strbody = l_strbody & vbCrLf & "-------------"
    l_strbody = l_strbody & vbCrLf & "CFM Version : " & App.Major & "." & App.Minor & "." & App.Revision
    l_strbody = l_strbody & vbCrLf & "Date/Time   : " & Format(Now, "dd/mm/yyyy HH:nn")
    l_strbody = l_strbody & vbCrLf & "PC Name     : " & g_strWorkstation
    
    Dim l_strAttachment As String
    
    l_strAttachment = App.Path & "\screenshots\" & g_strUserName & ".bmp"
    
    If Dir(l_strAttachment) <> g_strUserName & ".bmp" Then
        l_strAttachment = ""
    End If
    
    'send CETA an email.....
'    SendSMTPMail "support@cetasoft.net", "CETA Support", "New CETA Task :: " & g_lngLastID & " - " & l_strType, l_strAttachment, l_strbody, True, g_strUserEmailAddress, g_strFullUserName
    
    If g_strCETAAdministratorEmail <> "" Then
        SendSMTPMail g_strCETAAdministratorEmail, "CETA Administrator", "New CETA Task :: " & g_lngLastID & " - " & l_strType, l_strAttachment, l_strbody, True, "", ""
    End If


End If

Unload frmUserComment
Set frmUserComment = Nothing

On Error Resume Next
Kill App.Path & "\screenshots\" & g_strUserName & ".bmp"

End Sub

Private Sub mnuHelpAbout_Click()
frmAbout.Show vbModal
End Sub

Private Sub mnuHiddenActionItems_ActionItem_Click(Index As Integer)

Dim l_lngJobID As Long

Select Case MDIForm1.ActiveForm.Name

Case "frmJobListing"
    l_lngJobID = frmJobListing.grdJobListing.Columns("jobID").Text
    
End Select


Select Case Index
Case 0 'move

Case 1 'cancel
    
    CancelJob l_lngJobID
    
End Select

End Sub

Private Sub mnuHiddenActionItems_PrintMenuItem_Click(Index As Integer)


Dim l_lngJobID As Long

Select Case MDIForm1.ActiveForm.Name

Case "frmJobListing"
    l_lngJobID = frmJobListing.grdJobListing.Columns("jobID").Text
    
End Select



Select Case Index
Case 0 'job sheet
    PrintJobSheet l_lngJobID
Case 1 'fax
    MsgBox "This is not available.", vbExclamation
Case 2 'costing
    PrintJobCostingPage l_lngJobID
Case 3 'picking list
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "pickinglist.rpt", "{job.jobID} = " & l_lngJobID, False

Case 4 'dub job sheet
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "jobsheetdub.rpt", "{job.jobID} = " & l_lngJobID, False

End Select

End Sub

Private Sub mnuHiddenEvent_AddToJobAsMaster_Click()

'chris tate-davies - task 1.8 for sammy

AddEventToJobAsMaster


End Sub

Private Sub mnuHiddenEvent_Copy_Click()
g_lngEventID = MDIForm1.ActiveForm.grdEvents.Columns("eventID").Text

End Sub

Private Sub mnuHiddenEvent_Delete_Click()

MDIForm1.ActiveForm.grdEvents.DeleteSelected

End Sub

Private Sub mnuHiddenEvent_Paste_Click()

If g_lngEventID = 0 Then Exit Sub

CopyEventToLibraryID g_lngEventID, MDIForm1.ActiveForm.lblLibraryID.Caption

MDIForm1.ActiveForm.cmdRefreshEvents.Value = True


End Sub

Private Sub mnuJobListing_Abandoned_Click()
ShowJobListing "abandoned"
End Sub

Private Sub mnuJobListing_All_Click()
ShowJobListing "all"
End Sub

Private Sub mnuJobListing_Completed_Click()
ShowJobListing "completed"
End Sub

Private Sub mnuJobListing_Invoiced_Click()
ShowJobListing "invoiced"
End Sub

Private Sub mnuJobListing_OutStanding_Click()
ShowJobListing "outstanding"
End Sub

Private Sub mnuJobListing_Quotes_Click()
ShowJobListing "quote"
End Sub

Private Sub mnuJobListing_SentToAccounts_Click()
ShowJobListing "senttoaccounts"
End Sub

Private Sub mnuLibrary_New_Click()
ShowLibrary 0
End Sub

Private Sub mnuLibrary_Search_Click()
ShowLibrarySearch
End Sub

Private Sub mnuMediaNew_Click()
ShowClipControl 0
End Sub

Private Sub mnumediaSpecNew_Click()

If Val(frmClipSpec.lblMediaSpecID.Caption) = 0 Then
    ShowClipSpec "mediaspec", frmClipSpec, 0, True, True
Else
    frmClipSpec.ZOrder 0
End If

End Sub

Private Sub mnumediaTranscode_Click()

frmTranscoding.Show
frmTranscoding.ZOrder 0

End Sub

Private Sub mnuOffAirPortal_Click()

frmOffAirPortal.Show
frmOffAirPortal.ZOrder 0

End Sub

Private Sub mnuOffAirs_Click()

frmOffAirs.Show

End Sub

Private Sub mnuPurchase_New_Click()
ShowPurchaseOrder 0
End Sub

Private Sub mnuPurchase_Search_Click()

frmPurchaseSearch.Show

End Sub

Private Sub mnuRepors_CompanyList_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "companylist.rpt", "", True
End Sub

Private Sub mnuReports_BBCCompletions_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "BBC_Completions.rpt", "", True
End Sub

Private Sub mnuReports_BikeReport_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "BikeReport.rpt", "", True
End Sub

Private Sub mnuReports_CompaniesOnHold_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "companylist.rpt", "{company.accountstatus} = 'hold'", True
End Sub

Private Sub mnuReports_DailyLog_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "DailyAct.rpt", "", True
End Sub

Private Sub mnuReports_EntsRights_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Entrimon.rpt", "", True
End Sub

Private Sub mnuReports_MonthlyLog_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "MonthlyAct.rpt", "", True
End Sub

Private Sub mnuReports_MotionGallery_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "MotionGallery.rpt", "", True
End Sub

Private Sub mnuReports_OutOfLibrary_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "outoflibrary.rpt", "", True
End Sub

Private Sub mnuReports_OutstandingWork_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "outstandingworklist.rpt", "", True
End Sub

Private Sub mnuReports_PatheReport_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Pathe.rpt", "", True
End Sub

Private Sub mnuReports_portalUserIDs_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Portalusers.rpt", "", True
End Sub

Private Sub mnuReports_ProjectOverview_Click()

If Not CheckAccess("/projectoverviewreport") Then Exit Sub

Dim l_lngProjectNumber As Long
l_lngProjectNumber = Val(InputBox("Please enter the project number", "Project Overview Report"))

If l_lngProjectNumber = 0 Then Exit Sub

PrintCrystalReport g_strLocationOfCrystalReportFiles & "management\projectoverview.rpt", "{job.projectnumber} = " & l_lngProjectNumber, True

End Sub

Private Sub mnuReports_Protape_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "purchaseorder_monthlyreport_protape.rpt", "", True
End Sub

Private Sub mnuReports_Ratecard_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "ratecard.rpt", "", True
End Sub

Private Sub mnuReports_ReprintAccountsBatch_Click()

Dim l_Batchnumber As String
l_Batchnumber = InputBox("Please give the Batch Number", "Reprint Accounts Batch")
If l_Batchnumber <> "" Then
    PrintCrystalReport g_strLocationOfCrystalReportFiles & "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_Batchnumber & "'", True
    'ExportCrystalReport g_strLocationOfCrystalReportFiles & "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_Batchnumber & "'", "ExportFile.xls"
End If

End Sub

Private Sub mnuReports_SessionReport_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "sessionreport.rpt", "", True
End Sub

Private Sub mnuReports_TrackerCompleted_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "TrackerCompleteReport.rpt", "", True
End Sub

Private Sub mnuReports_UninvoicedWork_Click()
PrintUninvoicedWorkReport
End Sub

Private Sub mnuReports_WebDeliveries_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Webdeliveries.rpt", "", True
End Sub

Private Sub mnuReports_Weblogins_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Weblogins.rpt", "", True
End Sub

Private Sub mnuReports_WebPortalDeliveries_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "WebPortal_deliveries_simple.rpt", "", True
End Sub

Private Sub mnuReports_WebPortalLogins_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "Webportal_logins.rpt", "", True

End Sub

Private Sub mnuReports_WebUserIDs_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "webuserids.rpt", "", True
End Sub

Private Sub mnuReports_Worklist_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "worklist.rpt", "", True
End Sub

Private Sub mnuShowItunesPackage_Click()

frmiTunesPackage.Show
frmiTunesPackage.ZOrder 0

End Sub

Private Sub mnuShowPortalForm_Click()
frmClipPortalUsers.Show
End Sub

Private Sub mnuSupervisor_ComboBoxOptions_Click()
ShowComboBoxOptions
End Sub

Private Sub mnuSupervisor_EditViews_Click()

If Not CheckAccess("/editviews") Then Exit Sub
frmViewEditor.Show vbModal

End Sub

Private Sub mnuSupervisor_FormatStockCodes_Click()

If Not CheckAccess("/stockeditor") Then Exit Sub
frmStockEditor.Show

End Sub

Private Sub mnuSupervisor_PublishNotice_Click()

Dim l_strNoticeText As String, l_strSQL As String

frmTextEdit.Caption = "Please enter the text of the notice"
frmTextEdit.cmdSave.Caption = "Publish"
frmTextEdit.Show vbModal

If frmTextEdit.txtTextToEdit.Text <> "" Then
    l_strNoticeText = frmTextEdit.txtTextToEdit.Text
    If MsgBox("About to publish notice:" & vbCrLf & l_strNoticeText, vbYesNo, "Please Confirm Notice Publication") = vbYes Then
        l_strSQL = "INSERT INTO cetanotice (datecreated, notice) VALUES ("
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNoticeText) & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
End If
Unload frmTextEdit

End Sub

Private Sub mnuSupervisor_RateCard_Click()
ShowRateCard g_strRateCardType, 0

End Sub

Private Sub mnuSupervisor_ResourceList_Click()
If Not CheckAccess("/showresourcelist") Then Exit Sub
frmResourceList.Show
End Sub

Private Sub mnuSupervisor_SendToAccounts_Click()
If Not CheckAccess("/sendtoaccounts") Then Exit Sub
frmSendToAccounts.Show

End Sub

Private Sub mnuSupervisor_Settings_Click()
ShowSettings

End Sub

Private Sub mnuSupervisor_UserAdmin_Click()
ShowUserInformation
End Sub

Private Sub mnuToolBar_AlignBottom_Click()
SetToolBar "textbottom"
End Sub

Private Sub mnuToolBar_AlignRight_Click()
SetToolBar "textright"
End Sub

Private Sub mnuToolbar_Bottom_Click()
SetToolBar "bottom"
End Sub

Private Sub mnuToolbar_Hidden_Click()
SetToolBar "hidden"
End Sub

Private Sub mnuToolbar_Left_Click()
SetToolBar "left"
End Sub

Private Sub mnuToolBar_Right_Click()
SetToolBar "right"
End Sub

Private Sub mnuToolbar_Top_Click()
SetToolBar "top"
End Sub

Private Sub mnuTools_BBCAlias_Click()
ShowBBCAliasList
End Sub

Private Sub mnuTools_BBCTapeRenumberLookup_Click()
ShowBBCTapeRenumberLookup
End Sub

Private Sub mnuTools_BudgetAllocation_Click()
ShowPurchaseOrderAllocation
End Sub

Private Sub mnuTools_CompanyDatabase_Click()
ShowCompany 0
End Sub

Private Sub mnuTools_ContactDatabase_Click()
ShowContact 0
End Sub

Private Sub mnuTools_EditMAsterSeriesIDs_Click()
frmEditMasterSeriesIDs.Show
End Sub

Private Sub mnuTools_EquipmentManage_Click()
ShowEquipmentManagement
End Sub

Private Sub mnuTools_InternalMovement_Click()
ShowLibraryMovement
End Sub

Private Sub mnuTools_JobDetail_Click()
ShowJob 0, 0, True
'ShowJob 0, 0, True
End Sub

Private Sub mnuTools_JobSearch_Click()
ShowJobSearch
End Sub

Private Sub mnuTools_MakeADub_Click()

If Not CheckAccess("/makeadub") Then Exit Sub
frmMakeADub.Show

End Sub

Private Sub mnuTools_PrepareBarcodeLabels_Click()
PrintBarcodeLabels
End Sub

Private Sub mnuTools_Purchase_Budgets_Click()

If Not CheckAccess("/editbudgets") Then Exit Sub

frmBudgets.Show

End Sub

Private Sub mnuTools_Refresh_Click()
On Error Resume Next
MDIForm1.ActiveForm.cmdSearch.Value = True
MDIForm1.ActiveForm.cmdRefresh.Value = True

End Sub

Private Sub mnuTools_Reports_LateDeliveries_Click()

Dim l_datFromDate As Date
Dim l_datToDate As Date

MsgBox "Please select the start of your date range", vbInformation

l_datFromDate = GetDate(Now)

MsgBox "Please select the end of your date range", vbInformation
l_datToDate = GetDate(Now)

If Not IsDate(l_datFromDate) Then Exit Sub
If Not IsDate(l_datToDate) Then Exit Sub

Dim l_strFormula As String

l_strFormula = ""

l_strFormula = l_strFormula & " DateDiff (""n"", {despatch.deadlinedate}, {despatch.despatcheddate}) > 1"
l_strFormula = l_strFormula & " AND"
l_strFormula = l_strFormula & " {despatch.deadlinedate} IN cDate(" & Format(l_datFromDate, "YYYY,MM,DD") & ") to cdatetime(" & Format(l_datToDate, "YYYY,MM,DD") & ", 23,59,00)"

PrintCrystalReport g_strLocationOfCrystalReportFiles & "latedeliveries.rpt", l_strFormula, True


End Sub

Private Sub mnuTools_Reports_UserRequests_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "requestlist.rpt", "IsNull({usercomment.completeddate})", True

End Sub

Private Sub mnuTools_ShelfAudit_Click()
ShowShelfAudit
End Sub

Private Sub mnuWindow_ArrangeIcons_Click()
MDIForm1.Arrange vbArrangeIcons
End Sub

Private Sub mnuWindow_Cascade_Click()
MDIForm1.Arrange vbCascade
End Sub

Private Sub mnuWindow_Click()


Dim l_intForm As Integer

For l_intForm = 1 To mnuWindowListItem.Count - 1
    mnuWindowListItem(l_intForm).Visible = False
    Unload mnuWindowListItem(l_intForm)
Next


'actually loop through every loaded form
For l_intForm = 0 To Forms.Count - 1
            
        
    If l_intForm > 0 Then
        Load mnuWindowListItem(l_intForm)
    End If
    
    mnuWindowListItem(l_intForm).Caption = Forms(l_intForm).Caption
    mnuWindowListItem(l_intForm).Enabled = True
    
Next

If mnuWindowListItem(0).Caption Like "CETA Facilities Management*" Then
    mnuWindowListItem(0).Enabled = False
Else
    mnuWindowListItem(0).Enabled = True
End If

End Sub

Private Sub mnuWindow_Tile_Click()
MDIForm1.Arrange vbTileHorizontal
End Sub

Private Sub mnuWindowListItem_Click(Index As Integer)

Dim l_intForm As Integer

For l_intForm = 0 To Forms.Count - 1
    
    If Forms(l_intForm).Caption = mnuWindowListItem(Index).Caption Then
        
        Forms(l_intForm).Show
        
        Forms(l_intForm).WindowState = vbMaximized
        
        Forms(l_intForm).ZOrder 0
    End If
    
Next


End Sub

Private Sub timShutDown_Timer()

Dim l_strConnectionDisplay As String, Count As Long

Count = InStr(g_strConnection, "SERVER")
l_strConnectionDisplay = Mid(g_strConnection, Count)
Count = InStr(l_strConnectionDisplay, "DATABASE")
l_strConnectionDisplay = Left(l_strConnectionDisplay, Count - 1)

If g_optOverRide = 1 Then Exit Sub

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))
g_optLockSystem = Val(Trim(" " & GetData("setting", "value", "name", "LockSystem")))

If g_optCloseSystemDown <> False And (g_optLockSystem And 1) = 1 Then
    Beep
    If frmLogin.Visible = True Then
        frmLogin.Hide
        'ExitCETA True
    End If
    frmShutdownWarning.lblShutdownWarning.Caption = "The System Will Shutdown In " & g_intMinutesToCloseDown & " MINUTES"
    frmShutdownWarning.Show
    frmShutdownWarning.ZOrder 0
    DoEvents
    MDIForm1.Caption = "SYSTEM IS CLOSING DOWN IN " & g_intMinutesToCloseDown & " MINUTES: PLEASE QUIT!!!"
    If g_intMinutesToCloseDown <= 0 Then
        ExitCETA True
    End If
    g_intMinutesToCloseDown = g_intMinutesToCloseDown - 1
Else
    g_intMinutesToCloseDown = 5
    MDIForm1.Caption = "CETA Facilities Management " & App.Major & "." & App.Minor & "." & App.Revision & " - [Logged in as: " & g_strFullUserName & "]" & " " & l_strConnectionDisplay
End If

End Sub

Private Sub tmrStateMonitor_Timer()
    'holds the state of the key that is bein
    '     g monitored
    Dim state As Integer
    'holds the CURRENT mouse position.
    'It's to compare the current position wi
    '     th the previous
    'position
    Dim tmpPos As POINTAPI
    Dim Ret As Long 'simply holds the return value of the API
    'this checks if a key/button is pressed,
    '     or
    'if the mouse has moved.
    Dim IdleFound As Boolean
    Dim i As Integer 'the counter uses by the For loop
    IdleFound = False
    'Here I'm not sure about myself:
    'I don't know to what to set the value
    '256 to. It works as is, though!
    'And, what it does, is retrieve the stat
    '     e of each
    'individual key.


    For i = 1 To 256
        'call the API
        state = GetAsyncKeyState(i)
        'state will = -32767 if the 'i' key/butt
        '     on is
        'currently being pressed:


        If state = -32767 Then
            'if it is pressed, then this is the end
            '     of any idles
            IdleFound = True 'means that something is withholding the computer of idling
            IsIdle = False 'thus, it is not idling, so set the value
        End If
    Next
    'get the position of the mouse cursor
    Ret = GetCursorPos(tmpPos)
    'if the coordinates of the mouse are dif
    '     ferent than
    'last time or when the idle started, the
    '     n the system
    'is not idling:


    If tmpPos.X <> MousePos.X Or tmpPos.Y <> MousePos.Y Then
        IsIdle = False 'set the...
        IdleFound = True 'values
        'store the current coordinates so that w
        '     e
        'can compare next time round
        MousePos.X = tmpPos.X
        MousePos.Y = tmpPos.Y
    End If
    'if something did not withhold the idle
    '     then...


    If Not IdleFound Then
        'if isIdle not equals false, then don't
        '     reset the
        'startOfIdle!!


        If Not IsIdle Then
            'if it is false, then the idle is beginn
            '     ing
            IsIdle = True
            startOfIdle = Timer
        End If
    End If
End Sub

Private Sub tmrPeriod_Timer()
    'this timer continuesly monitors the
    'value of IsIdle to see if the system ha
    '     s been
    'idle for INTERVAL

    If IsIdle Then
        'if the difference between now (timer) a
        '     nd the
        'time the idle started, is => INTERVAL,
        '     then
        'the 'idle state' has been reached

        If Timer - startOfIdle >= INTERVAL Then
            'call the sub that will handle any code
            '     at this stage
            'this is merely to seperate the idle che
            '     ck code
            'from your own code
            'NOTE: I advise you to perform some sort
            '     of
            'check here to see if the idle state has
            '     been
            'reached for the first time, or if the s
            '     ystem
            'has just been idling ever since the idl
            '     e state
            'was reached
            Call IdleStateEngaged(Timer)
            'important: set the values
            startOfIdle = Timer
            IsIdle = True
        End If
    Else ' not idling, or the idlestate has been left
        'call the sub
        'NOTE: I advise you to perform some sort
        '     of
        'check here to see if the system was in
        '     the
        'idle state, or if the system
        'has not been idling anyway
        Call IdleStateDisengaged(Timer)
    End If
End Sub

Public Sub IdleStateEngaged(ByVal IdleStartTime As Long)
    
'System has been idle for 300 second (5 minutes)
'Check whether this is a machine that should autologout, and do it if it is.
If InStr(GetData("xref", "descriptionalias", "description", CurrentMachineName()), "/autologout") > 0 Then
    Logout
End If

End Sub

Public Sub IdleStateDisengaged(ByVal IdleStopTime As Long)

'Nothing significant needs to happen here

End Sub

