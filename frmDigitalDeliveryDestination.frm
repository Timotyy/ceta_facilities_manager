VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmDigitalDeliveryDestination 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Digital Delivery Destinations"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7650
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   7650
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtSmartjogEmailNotifications 
      Height          =   795
      Left            =   1920
      MultiLine       =   -1  'True
      TabIndex        =   24
      ToolTipText     =   "Smartjog Account Code (if applicable)"
      Top             =   3120
      Width           =   5595
   End
   Begin VB.TextBox txtPortNumber 
      Height          =   315
      Left            =   1920
      TabIndex        =   22
      ToolTipText     =   "The Site Address (if applicable)"
      Top             =   1680
      Width           =   3975
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      Left            =   1920
      TabIndex        =   16
      ToolTipText     =   "Password"
      Top             =   2400
      Width           =   3975
   End
   Begin VB.TextBox txtLoginName 
      Height          =   315
      Left            =   1920
      TabIndex        =   15
      ToolTipText     =   "Login Name"
      Top             =   2040
      Width           =   3975
   End
   Begin VB.TextBox txtSmartjogAccountcode 
      Height          =   315
      Left            =   1920
      TabIndex        =   14
      ToolTipText     =   "Smartjog Account Code (if applicable)"
      Top             =   2760
      Width           =   3975
   End
   Begin VB.TextBox txtSiteAddress 
      Height          =   315
      Left            =   1920
      TabIndex        =   13
      ToolTipText     =   "The Site Address (if applicable)"
      Top             =   1320
      Width           =   3975
   End
   Begin VB.TextBox txtMethodName 
      Height          =   315
      Left            =   1920
      TabIndex        =   12
      ToolTipText     =   "The Method Name"
      Top             =   960
      Width           =   3975
   End
   Begin VB.CommandButton cmdClearCompany 
      Caption         =   "Clear"
      Height          =   315
      Left            =   6780
      TabIndex        =   11
      Top             =   540
      Width           =   735
   End
   Begin MSAdodcLib.Adodc adoDestinationName 
      Height          =   330
      Left            =   2340
      Top             =   240
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoDestinationName"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   4080
      Width           =   6015
      Begin VB.CommandButton cmdDeleteSpec 
         Caption         =   "Delete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2400
         TabIndex        =   7
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         TabIndex        =   6
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   4860
         TabIndex        =   4
         Top             =   0
         Width           =   1155
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3660
         TabIndex        =   3
         Top             =   0
         Width           =   1155
      End
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDestinationName 
      Bindings        =   "frmDigitalDeliveryDestination.frx":0000
      DataField       =   "DestinationName"
      Height          =   315
      Left            =   1920
      TabIndex        =   5
      ToolTipText     =   "Digital Delivery Destination Name"
      Top             =   120
      Width           =   4035
      DataFieldList   =   "DestinationName"
      MaxDropDownItems=   24
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      RowHeight       =   423
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "mediaspecID"
      Columns(0).Name =   "DigitalDeliveryDestinationID"
      Columns(0).DataField=   "DigitalDeliveryDestinationID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   5292
      Columns(1).Caption=   "mediaspecname"
      Columns(1).Name =   "DestinationName"
      Columns(1).DataField=   "DestinationName"
      Columns(1).FieldLen=   256
      Columns(2).Width=   5292
      Columns(2).Caption=   "MethodName"
      Columns(2).Name =   "MethodName"
      Columns(2).DataField=   "MethodName"
      Columns(2).FieldLen=   256
      _ExtentX        =   7117
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1920
      TabIndex        =   8
      ToolTipText     =   "The company name"
      Top             =   540
      Width           =   4035
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   7117
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Smartjog Email Notifications"
      Height          =   555
      Index           =   7
      Left            =   120
      TabIndex        =   25
      Top             =   3180
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Port # (Aspera)"
      Height          =   195
      Index           =   6
      Left            =   120
      TabIndex        =   23
      Top             =   1740
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Password"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   21
      Top             =   2460
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Login Name"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   20
      Top             =   2100
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Smartjog Account Code"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   19
      Top             =   2820
      Width           =   1755
   End
   Begin VB.Label lblCaption 
      Caption         =   "Site Address"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   18
      Top             =   1380
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Delivery Method Name"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   17
      Top             =   1020
      Width           =   1755
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   6000
      TabIndex        =   10
      Top             =   540
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "JCA Customer"
      Height          =   195
      Index           =   32
      Left            =   120
      TabIndex        =   9
      Top             =   600
      Width           =   1275
   End
   Begin VB.Label lblDigitalDeliveryDestinationID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   6000
      TabIndex        =   1
      Tag             =   "CLEARFIELDS"
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Destination Name"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   180
      Width           =   1455
   End
End
Attribute VB_Name = "frmDigitalDeliveryDestination"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmbCompany_CloseUp()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

Dim l_strSQL As String

l_strSQL = "SELECT * FROM DigitalDeliveryDestination WHERE companyID = " & lblCompanyID.Caption & " ORDER BY DestinationName;"

adoDestinationName.ConnectionString = g_strConnection
adoDestinationName.RecordSource = l_strSQL
adoDestinationName.Refresh

End Sub

Private Sub cmdClear_Click()
ClearFields Me
End Sub

Private Sub cmbDestinationName_Click()

ShowDigitalDeliveryDestination cmbDestinationName.Columns("DigitalDeliveryDestinationID").Text, True, True

End Sub

Private Sub cmbDestinationName_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then

    If Val(lblDigitalDeliveryDestinationID.Caption) <> 0 Then
        ShowDigitalDeliveryDestination Val(lblDigitalDeliveryDestinationID.Caption)
    End If
    KeyAscii = 0
End If

End Sub

Private Sub cmdClearCompany_Click()

cmbCompany.Text = ""
lblCompanyID.Caption = "0"

Dim l_strSQL As String

l_strSQL = "SELECT * FROM DigitalDeliveryDestination WHERE companyID = " & lblCompanyID.Caption & " ORDER BY DestinationName;"

adoDestinationName.ConnectionString = g_strConnection
adoDestinationName.RecordSource = l_strSQL
adoDestinationName.Refresh

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdDeleteSpec_Click()

DeleteDigitalDestination

End Sub

Private Sub cmdSave_Click()

SaveDigitalDeliveryDestination

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM DigitalDeliveryDestination WHERE companyID = 0 ORDER BY DestinationName;"

adoDestinationName.ConnectionString = g_strConnection
adoDestinationName.RecordSource = l_strSQL
adoDestinationName.Refresh

l_strSQL = "SELECT name, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

End Sub

Private Sub Form_Resize()

'frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
'frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

End Sub
