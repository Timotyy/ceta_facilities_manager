VERSION 5.00
Begin VB.Form frmExecuteSQL 
   Caption         =   "Execute an SQL Statement"
   ClientHeight    =   2625
   ClientLeft      =   4920
   ClientTop       =   4725
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   ScaleHeight     =   2625
   ScaleWidth      =   7575
   Begin VB.CommandButton cmdExecute 
      Caption         =   "Execute"
      Height          =   375
      Left            =   4560
      TabIndex        =   1
      Top             =   1860
      Width           =   1155
   End
   Begin VB.TextBox txtExecute 
      Height          =   1335
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   7215
   End
End
Attribute VB_Name = "frmExecuteSQL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExecute_Click()

On Error GoTo ERROR_PROC

Dim l_lngRecords As Long

Dim l_con As New ADODB.Connection
l_con.Open g_strConnection

Screen.MousePointer = vbHourglass

l_con.Execute CStr(txtExecute.Text), l_lngRecords

Me.Caption = CStr(l_lngRecords) & " records updated"



EXIT_PROC:

l_con.Close
Set l_con = Nothing


Screen.MousePointer = vbDefault

MsgBox "Completed", vbExclamation

Exit Sub

ERROR_PROC:

MsgBox Err.Description

Resume EXIT_PROC

End Sub


