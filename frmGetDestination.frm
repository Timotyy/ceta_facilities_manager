VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Begin VB.Form frmGetDestination 
   Caption         =   "Digital Send Destination"
   ClientHeight    =   1170
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7980
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1170
   ScaleWidth      =   7980
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmbOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   2040
      TabIndex        =   2
      ToolTipText     =   "Accept the offered Barcode"
      Top             =   720
      Width           =   1275
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbDestination 
      Height          =   315
      Left            =   2040
      TabIndex        =   0
      Top             =   180
      Width           =   5235
      DataFieldList   =   "DestinationName"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   3
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "DigitalDestinationID"
      Columns(0).Name =   "DigitalDestinationID"
      Columns(0).DataField=   "DigitalDestinationID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3201
      Columns(1).Caption=   "DestinationType"
      Columns(1).Name =   "DestinationType"
      Columns(1).DataField=   "DestinationType"
      Columns(1).FieldLen=   256
      Columns(2).Width=   9578
      Columns(2).Caption=   "DestinationName"
      Columns(2).Name =   "DestinationName"
      Columns(2).DataField=   "DestinationName"
      Columns(2).FieldLen=   256
      _ExtentX        =   9234
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "DestinationName"
   End
   Begin VB.Label lblDestinationType 
      Height          =   195
      Left            =   3600
      TabIndex        =   3
      Top             =   780
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Choose Destination"
      Height          =   255
      Left            =   300
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
End
Attribute VB_Name = "frmGetDestination"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbOK_Click()
Me.Hide
End Sub

Private Sub Form_Activate()

Dim l_strSQL As String
'l_strSQL = "SELECT DigitalDestinationID, DestinationType, DestinationName, DestinationDropfolder FROM DigitalDestination WHERE DestinationType  = '' OR DestinationType Like '" & lblDestinationType.Caption & "' ORDER BY DestinationType, DestinationName;"
l_strSQL = "SELECT DigitalDestinationID, DestinationType, DestinationName, DestinationDropfolder FROM DigitalDestination WHERE DestinationType <> 'SmartJog' ORDER BY DestinationName;;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbDestination.DataSourceList = l_rstSearch1
cmbDestination.Text = ""

End Sub

Private Sub Form_Load()

DoEvents

End Sub
