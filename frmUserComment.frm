VERSION 5.00
Begin VB.Form frmUserComment 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "User Comment"
   ClientHeight    =   6240
   ClientLeft      =   5280
   ClientTop       =   3555
   ClientWidth     =   13560
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6240
   ScaleWidth      =   13560
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox picScreenCapture 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      Height          =   855
      Left            =   10620
      ScaleHeight     =   795
      ScaleWidth      =   1155
      TabIndex        =   16
      Top             =   5280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdGetImage 
      Caption         =   "Include Screenshot BIG FILE (Uses clipboard)"
      Height          =   315
      Left            =   1140
      TabIndex        =   15
      Top             =   5820
      Width           =   3435
   End
   Begin VB.ComboBox cmbType 
      BackColor       =   &H00C0FFC0&
      Height          =   315
      Left            =   1140
      TabIndex        =   5
      Top             =   120
      Width           =   2175
   End
   Begin VB.ComboBox cmbArea 
      BackColor       =   &H00C0E0FF&
      Height          =   315
      Left            =   1140
      TabIndex        =   4
      Top             =   540
      Width           =   5835
   End
   Begin VB.ComboBox cmbSeverity 
      BackColor       =   &H00C0FFC0&
      Height          =   315
      Left            =   4200
      TabIndex        =   3
      Top             =   120
      Width           =   2775
   End
   Begin VB.TextBox txtComment 
      BackColor       =   &H00C0E0FF&
      Height          =   2475
      Left            =   1140
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   960
      Width           =   5835
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   5940
      TabIndex        =   1
      Top             =   5820
      Width           =   1155
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   315
      Left            =   4680
      TabIndex        =   0
      Top             =   5820
      Width           =   1155
   End
   Begin VB.Image ScreenCapture 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   5055
      Left            =   7080
      Stretch         =   -1  'True
      Top             =   120
      Width           =   6375
   End
   Begin VB.Label lblCaption 
      Caption         =   $"frmUserComment.frx":0000
      Height          =   435
      Index           =   12
      Left            =   1140
      TabIndex        =   14
      Top             =   5220
      Width           =   5955
   End
   Begin VB.Label lblCaption 
      Caption         =   "The more information you send, the more likely we will be able to repeat the fault and fix it - which means it wont happen again."
      Height          =   435
      Index           =   11
      Left            =   1140
      TabIndex        =   13
      Top             =   4680
      Width           =   5895
   End
   Begin VB.Label lblCaption 
      Caption         =   "If you received an error, please send us the exact error and what you were doing at the time and just before."
      Height          =   435
      Index           =   10
      Left            =   1140
      TabIndex        =   12
      Top             =   4200
      Width           =   5835
   End
   Begin VB.Label lblCaption 
      Caption         =   "Maybe a specific company name or resource, P/O number or date."
      Height          =   255
      Index           =   9
      Left            =   1140
      TabIndex        =   11
      Top             =   3900
      Width           =   5955
   End
   Begin VB.Label lblCaption 
      Caption         =   "Please try to include useful information, such as the job ID or project number."
      Height          =   255
      Index           =   8
      Left            =   1140
      TabIndex        =   10
      Top             =   3600
      Width           =   5835
   End
   Begin VB.Label lblCaption 
      Caption         =   "Type"
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Area"
      Height          =   315
      Index           =   1
      Left            =   120
      TabIndex        =   8
      Top             =   540
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Severity"
      Height          =   315
      Index           =   2
      Left            =   3420
      TabIndex        =   7
      Top             =   120
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Comment"
      Height          =   315
      Index           =   3
      Left            =   120
      TabIndex        =   6
      Top             =   960
      Width           =   795
   End
End
Attribute VB_Name = "frmUserComment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
Me.Tag = "CANCELLED"
Me.Hide
End Sub

Private Sub cmdGetImage_Click()

On Error GoTo PROC_GetImageError

' paste the clipboard contents into the picture box
ScreenCapture.Picture = Clipboard.GetData(vbCFBitmap)
DoEvents

picScreenCapture.Picture = Clipboard.GetData(vbCFBitmap)
DoEvents

' save the image to a file using the application path
SavePicture ScreenCapture.Picture, App.Path & "\screenshots\" & g_strUserName & ".bmp"

Exit Sub

PROC_GetImageError:
Select Case Err.Number
    Case 76 'path not found
        MkDir App.Path & "\screenshots"
        Resume
    
    Case 70 'permission denied
    
        MsgBox "The system could not store a screenshot BMP file as you do not have the correct file-permissions to save to disk." & vbCrLf & vbCrLf & "Please contact your CETA Administrator and ask them to set up the screenshots folder.", vbExclamation + vbOKOnly
        picScreenCapture.Cls
        Exit Sub
        
    Case Else
        MsgBox Err.Description, vbExclamation
        Exit Sub
End Select



End Sub

Private Sub Form_Load()

With cmbType
    .AddItem "Bug"
    .AddItem "Enhancement"
    .AddItem "Question"
    .AddItem "New Function"
End With

PopulateCombo "programareas", cmbArea

With cmbSeverity
    .AddItem "1 - Severe"
    .AddItem "2 - High"
    .AddItem "3 - Moderate"
    .AddItem "4 - Low"
    .AddItem "5 - Very Low"
End With

CenterForm Me

End Sub


Private Sub OKButton_Click()

If cmbType.Text = "" Then
    MsgBox "Please select a valid comment type", vbExclamation
    Exit Sub
End If

If cmbArea.Text = "" Then
    MsgBox "Please select a valid area", vbExclamation
    Exit Sub
End If

If cmbSeverity.Text = "" Then
    MsgBox "Please select a valid severity", vbExclamation
    Exit Sub
End If

If txtComment.Text = "" Then
    MsgBox "Please enter a comment!", vbExclamation
    Exit Sub
End If



Me.Hide

End Sub


