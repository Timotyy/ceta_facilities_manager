VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmDRGTracker 
   Caption         =   "Materials Tracker"
   ClientHeight    =   14670
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   26010
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDRGTracker.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   14670
   ScaleWidth      =   26010
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cmbHeadertext5 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   7020
      TabIndex        =   38
      ToolTipText     =   "The Clip Codec"
      Top             =   1620
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.ComboBox cmbHeadertext4 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   7020
      TabIndex        =   37
      ToolTipText     =   "The Clip Codec"
      Top             =   1260
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.ComboBox cmbHeadertext3 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   7020
      TabIndex        =   36
      ToolTipText     =   "The Clip Codec"
      Top             =   900
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.ComboBox cmbHeadertext2 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   7020
      TabIndex        =   35
      ToolTipText     =   "The Clip Codec"
      Top             =   540
      Visible         =   0   'False
      Width           =   4515
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnLanguage 
      Height          =   855
      Left            =   3900
      TabIndex        =   34
      Top             =   3420
      Width           =   3075
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns(0).Width=   4789
      Columns(0).Caption=   "Language"
      Columns(0).Name =   "Language"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   5424
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   2100
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoItems"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnMasterSeriesTitle 
      Bindings        =   "frmDRGTracker.frx":08CA
      Height          =   855
      Left            =   720
      TabIndex        =   33
      Top             =   2880
      Width           =   2295
      DataFieldList   =   "Title"
      ScrollBars      =   0
      _Version        =   196617
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   5292
      Columns(0).Caption=   "value"
      Columns(0).Name =   "Title"
      Columns(0).DataField=   "Title"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2117
      Columns(1).Caption=   "SeriesID"
      Columns(1).Name =   "SeriesID"
      Columns(1).DataField=   "SeriesID"
      Columns(1).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "Title"
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmDRGTracker.frx":08EA
      Height          =   6855
      Left            =   120
      TabIndex        =   3
      Top             =   2100
      Width           =   25545
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDRGTracker.frx":0901
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmDRGTracker.frx":091D
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmDRGTracker.frx":0939
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   79
      Columns.Count   =   38
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "MasterSeriesTrackerID"
      Columns(0).DataField=   "MasterSeriesTrackerID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   9181
      Columns(2).Caption=   "Title"
      Columns(2).Name =   "Title"
      Columns(2).DataField=   "Title"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   1773
      Columns(3).Caption=   "SeriesID"
      Columns(3).Name =   "SeriesID"
      Columns(3).DataField=   "SeriesID"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   3519
      Columns(4).Caption=   "headertext2"
      Columns(4).Name =   "headertext2"
      Columns(4).DataField=   "headertext2"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   3519
      Columns(5).Caption=   "headertext3"
      Columns(5).Name =   "headertext3"
      Columns(5).DataField=   "headertext3"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   3519
      Columns(6).Caption=   "headertext4"
      Columns(6).Name =   "headertext4"
      Columns(6).DataField=   "headertext4"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   3519
      Columns(7).Caption=   "headertext5"
      Columns(7).Name =   "headertext5"
      Columns(7).DataField=   "headertext5"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   2646
      Columns(8).Caption=   "headerint2"
      Columns(8).Name =   "headerint2"
      Columns(8).DataField=   "headerint2"
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   1773
      Columns(9).Caption=   "headerint3"
      Columns(9).Name =   "headerint3"
      Columns(9).DataField=   "headerint3"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   1773
      Columns(10).Caption=   "headerint4"
      Columns(10).Name=   "headerint4"
      Columns(10).DataField=   "headerint4"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1773
      Columns(11).Caption=   "headerint5"
      Columns(11).Name=   "headerint5"
      Columns(11).DataField=   "headerint5"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   2117
      Columns(12).Caption=   "stagefield1"
      Columns(12).Name=   "stagefield1"
      Columns(12).DataField=   "stagefield1"
      Columns(12).FieldLen=   256
      Columns(12).StyleSet=   "stagefield"
      Columns(13).Width=   2117
      Columns(13).Caption=   "stagefield2"
      Columns(13).Name=   "stagefield2"
      Columns(13).DataField=   "stagefield2"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "stagefield"
      Columns(14).Width=   2117
      Columns(14).Caption=   "stagefield3"
      Columns(14).Name=   "stagefield3"
      Columns(14).DataField=   "stagefield3"
      Columns(14).FieldLen=   256
      Columns(14).StyleSet=   "stagefield"
      Columns(15).Width=   2117
      Columns(15).Caption=   "stagefield4"
      Columns(15).Name=   "stagefield4"
      Columns(15).DataField=   "stagefield4"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "stagefield"
      Columns(16).Width=   2117
      Columns(16).Caption=   "stagefield5"
      Columns(16).Name=   "stagefield5"
      Columns(16).DataField=   "stagefield5"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "stagefield"
      Columns(17).Width=   2117
      Columns(17).Caption=   "stagefield6"
      Columns(17).Name=   "stagefield6"
      Columns(17).DataField=   "stagefield6"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   2117
      Columns(18).Caption=   "stagefield7"
      Columns(18).Name=   "stagefield7"
      Columns(18).DataField=   "stagefield7"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "stagefield"
      Columns(19).Width=   2117
      Columns(19).Caption=   "stagefield8"
      Columns(19).Name=   "stagefield8"
      Columns(19).DataField=   "stagefield8"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "stagefield"
      Columns(20).Width=   2117
      Columns(20).Caption=   "stagefield9"
      Columns(20).Name=   "stagefield9"
      Columns(20).DataField=   "stagefield9"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "stagefield"
      Columns(21).Width=   2117
      Columns(21).Caption=   "stagefield10"
      Columns(21).Name=   "stagefield10"
      Columns(21).DataField=   "stagefield10"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "stagefield"
      Columns(22).Width=   2117
      Columns(22).Caption=   "stagefield11"
      Columns(22).Name=   "stagefield11"
      Columns(22).DataField=   "stagefield11"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "stagefield"
      Columns(23).Width=   2117
      Columns(23).Caption=   "stagefield12"
      Columns(23).Name=   "stagefield12"
      Columns(23).DataField=   "stagefield12"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "stagefield"
      Columns(24).Width=   2117
      Columns(24).Caption=   "stagefield13"
      Columns(24).Name=   "stagefield13"
      Columns(24).DataField=   "stagefield13"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "stagefield"
      Columns(25).Width=   2117
      Columns(25).Caption=   "stagefield14"
      Columns(25).Name=   "stagefield14"
      Columns(25).DataField=   "stagefield14"
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "stagefield"
      Columns(26).Width=   2117
      Columns(26).Caption=   "stagefield15"
      Columns(26).Name=   "stagefield15"
      Columns(26).DataField=   "stagefield15"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "stagefield"
      Columns(27).Width=   2117
      Columns(27).Caption=   "stagefield16"
      Columns(27).Name=   "stagefield16"
      Columns(27).DataField=   "stagefield16"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "stagefield"
      Columns(28).Width=   2117
      Columns(28).Caption=   "stagefield17"
      Columns(28).Name=   "stagefield17"
      Columns(28).DataField=   "stagefield17"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "stagefield"
      Columns(29).Width=   2117
      Columns(29).Caption=   "stagefield18"
      Columns(29).Name=   "stagefield18"
      Columns(29).DataField=   "stagefield18"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "stagefield"
      Columns(30).Width=   2117
      Columns(30).Caption=   "stagefield19"
      Columns(30).Name=   "stagefield19"
      Columns(30).DataField=   "stagefield19"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "stagefield"
      Columns(31).Width=   2117
      Columns(31).Caption=   "stagefield20"
      Columns(31).Name=   "stagefield20"
      Columns(31).DataField=   "stagefield20"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "stagefield"
      Columns(32).Width=   2117
      Columns(32).Caption=   "stagefield21"
      Columns(32).Name=   "stagefield21"
      Columns(32).DataField=   "stagefield21"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "stagefield"
      Columns(33).Width=   2117
      Columns(33).Caption=   "stagefield22"
      Columns(33).Name=   "stagefield22"
      Columns(33).DataField=   "stagefield22"
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "stagefield"
      Columns(34).Width=   2117
      Columns(34).Caption=   "stagefield23"
      Columns(34).Name=   "stagefield23"
      Columns(34).DataField=   "stagefield23"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "stagefield"
      Columns(35).Width=   2117
      Columns(35).Caption=   "stagefield24"
      Columns(35).Name=   "stagefield24"
      Columns(35).DataField=   "stagefield24"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "stagefield"
      Columns(36).Width=   2117
      Columns(36).Caption=   "stagefield25"
      Columns(36).Name=   "stagefield25"
      Columns(36).DataField=   "stagefield25"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "stagefield"
      Columns(37).Width=   3200
      Columns(37).Caption=   "Date Added"
      Columns(37).Name=   "cdate"
      Columns(37).DataField=   "cdate"
      Columns(37).DataType=   7
      Columns(37).FieldLen=   256
      Columns(37).StyleSet=   "headerfield"
      _ExtentX        =   45059
      _ExtentY        =   12091
      _StockProps     =   79
      Caption         =   "Series Items"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdMakeEpisodes 
      Caption         =   "Make Episodes"
      Height          =   315
      Left            =   180
      TabIndex        =   31
      Top             =   1620
      Width           =   1815
   End
   Begin VB.TextBox txtheaderint5 
      Height          =   315
      Left            =   13920
      TabIndex        =   28
      Top             =   1620
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint4 
      Height          =   315
      Left            =   13920
      TabIndex        =   27
      Top             =   1260
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheaderint3 
      Height          =   315
      Left            =   13920
      TabIndex        =   26
      Top             =   900
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtheadertext5 
      Height          =   315
      Left            =   7020
      TabIndex        =   21
      Top             =   1620
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheadertext4 
      Height          =   315
      Left            =   7020
      TabIndex        =   20
      Top             =   1260
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheadertext3 
      Height          =   315
      Left            =   7020
      TabIndex        =   19
      Top             =   900
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheaderint2 
      Height          =   315
      Left            =   13920
      TabIndex        =   18
      Top             =   540
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.TextBox txtSeriesID 
      Height          =   315
      Left            =   13920
      TabIndex        =   15
      Top             =   180
      Width           =   1275
   End
   Begin VB.TextBox txtheadertext2 
      Height          =   315
      Left            =   7020
      TabIndex        =   14
      Top             =   540
      Visible         =   0   'False
      Width           =   4515
   End
   Begin VB.TextBox txtheadertext1 
      Height          =   315
      Left            =   7020
      TabIndex        =   11
      Top             =   180
      Width           =   4515
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7800
      TabIndex        =   4
      Top             =   14100
      Width           =   17835
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   16560
         TabIndex        =   8
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   15300
         TabIndex        =   7
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   14040
         TabIndex        =   6
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   12780
         TabIndex        =   5
         Top             =   0
         Visible         =   0   'False
         Width           =   1215
      End
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   180
      TabIndex        =   0
      Tag             =   "NOCLEAR"
      Top             =   600
      Value           =   1  'Checked
      Width           =   1755
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   1740
      TabIndex        =   1
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   180
      Width           =   2895
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   5106
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   1920
      Top             =   13260
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoComments"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmDRGTracker.frx":0955
      Height          =   4875
      Left            =   120
      TabIndex        =   32
      Top             =   9000
      Width           =   25545
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmDRGTracker.frx":096F
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmDRGTracker.frx":098B
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmDRGTracker.frx":09A7
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   39
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "MasterSeriesTrackerID"
      Columns(0).DataField=   "MasterSeriesTrackerID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   9181
      Columns(2).Caption=   "Title"
      Columns(2).Name =   "Title"
      Columns(2).DataField=   "Title"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   1402
      Columns(3).Caption=   "SeriesID"
      Columns(3).Name =   "SeriesID"
      Columns(3).DataField=   "SeriesID"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   3175
      Columns(4).Caption=   "headertext2"
      Columns(4).Name =   "headertext2"
      Columns(4).DataField=   "headertext2"
      Columns(4).FieldLen=   256
      Columns(4).StyleSet=   "headerfield"
      Columns(5).Width=   3175
      Columns(5).Caption=   "headertext3"
      Columns(5).Name =   "headertext3"
      Columns(5).DataField=   "headertext3"
      Columns(5).FieldLen=   256
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   3175
      Columns(6).Caption=   "headertext4"
      Columns(6).Name =   "headertext4"
      Columns(6).DataField=   "headertext4"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   3175
      Columns(7).Caption=   "headertext5"
      Columns(7).Name =   "headertext5"
      Columns(7).DataField=   "headertext5"
      Columns(7).FieldLen=   256
      Columns(7).StyleSet=   "headerfield"
      Columns(8).Width=   1773
      Columns(8).Caption=   "headerint2"
      Columns(8).Name =   "headerint2"
      Columns(8).DataField=   "headerint2"
      Columns(8).FieldLen=   256
      Columns(8).StyleSet=   "headerfield"
      Columns(9).Width=   1773
      Columns(9).Caption=   "headerint3"
      Columns(9).Name =   "headerint3"
      Columns(9).DataField=   "headerint3"
      Columns(9).FieldLen=   256
      Columns(9).StyleSet=   "headerfield"
      Columns(10).Width=   1773
      Columns(10).Caption=   "headerint4"
      Columns(10).Name=   "headerint4"
      Columns(10).DataField=   "headerint4"
      Columns(10).FieldLen=   256
      Columns(10).StyleSet=   "headerfield"
      Columns(11).Width=   1773
      Columns(11).Caption=   "headerint5"
      Columns(11).Name=   "headerint5"
      Columns(11).DataField=   "headerint5"
      Columns(11).FieldLen=   256
      Columns(11).StyleSet=   "headerfield"
      Columns(12).Width=   1244
      Columns(12).Caption=   "Episode"
      Columns(12).Name=   "Episode"
      Columns(12).DataField=   "Episode"
      Columns(12).FieldLen=   256
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   2117
      Columns(13).Caption=   "stagefield1"
      Columns(13).Name=   "stagefield1"
      Columns(13).DataField=   "stagefield1"
      Columns(13).FieldLen=   256
      Columns(13).StyleSet=   "stagefield"
      Columns(14).Width=   2117
      Columns(14).Caption=   "stagefield2"
      Columns(14).Name=   "stagefield2"
      Columns(14).DataField=   "stagefield2"
      Columns(14).FieldLen=   256
      Columns(14).StyleSet=   "stagefield"
      Columns(15).Width=   2117
      Columns(15).Caption=   "stagefield3"
      Columns(15).Name=   "stagefield3"
      Columns(15).DataField=   "stagefield3"
      Columns(15).FieldLen=   256
      Columns(15).StyleSet=   "stagefield"
      Columns(16).Width=   2117
      Columns(16).Caption=   "stagefield4"
      Columns(16).Name=   "stagefield4"
      Columns(16).DataField=   "stagefield4"
      Columns(16).FieldLen=   256
      Columns(16).StyleSet=   "stagefield"
      Columns(17).Width=   2117
      Columns(17).Caption=   "stagefield5"
      Columns(17).Name=   "stagefield5"
      Columns(17).DataField=   "stagefield5"
      Columns(17).FieldLen=   256
      Columns(17).StyleSet=   "stagefield"
      Columns(18).Width=   2117
      Columns(18).Caption=   "stagefield6"
      Columns(18).Name=   "stagefield6"
      Columns(18).DataField=   "stagefield6"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "stagefield"
      Columns(19).Width=   2117
      Columns(19).Caption=   "stagefield7"
      Columns(19).Name=   "stagefield7"
      Columns(19).DataField=   "stagefield7"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "stagefield"
      Columns(20).Width=   2117
      Columns(20).Caption=   "stagefield8"
      Columns(20).Name=   "stagefield8"
      Columns(20).DataField=   "stagefield8"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "stagefield"
      Columns(21).Width=   2117
      Columns(21).Caption=   "stagefield9"
      Columns(21).Name=   "stagefield9"
      Columns(21).DataField=   "stagefield9"
      Columns(21).FieldLen=   256
      Columns(21).StyleSet=   "stagefield"
      Columns(22).Width=   2117
      Columns(22).Caption=   "stagefield10"
      Columns(22).Name=   "stagefield10"
      Columns(22).DataField=   "stagefield10"
      Columns(22).FieldLen=   256
      Columns(22).StyleSet=   "stagefield"
      Columns(23).Width=   2117
      Columns(23).Caption=   "stagefield11"
      Columns(23).Name=   "stagefield11"
      Columns(23).DataField=   "stagefield11"
      Columns(23).FieldLen=   256
      Columns(23).StyleSet=   "stagefield"
      Columns(24).Width=   2117
      Columns(24).Caption=   "stagefield12"
      Columns(24).Name=   "stagefield12"
      Columns(24).DataField=   "stagefield12"
      Columns(24).FieldLen=   256
      Columns(24).StyleSet=   "stagefield"
      Columns(25).Width=   2117
      Columns(25).Caption=   "stagefield13"
      Columns(25).Name=   "stagefield13"
      Columns(25).DataField=   "stagefield13"
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "stagefield"
      Columns(26).Width=   2117
      Columns(26).Caption=   "stagefield14"
      Columns(26).Name=   "stagefield14"
      Columns(26).DataField=   "stagefield14"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "stagefield"
      Columns(27).Width=   2117
      Columns(27).Caption=   "stagefield15"
      Columns(27).Name=   "stagefield15"
      Columns(27).DataField=   "stagefield15"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "stagefield"
      Columns(28).Width=   2117
      Columns(28).Caption=   "stagefield16"
      Columns(28).Name=   "stagefield16"
      Columns(28).DataField=   "stagefield16"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "stagefield"
      Columns(29).Width=   2117
      Columns(29).Caption=   "stagefield17"
      Columns(29).Name=   "stagefield17"
      Columns(29).DataField=   "stagefield17"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "stagefield"
      Columns(30).Width=   2117
      Columns(30).Caption=   "stagefield18"
      Columns(30).Name=   "stagefield18"
      Columns(30).DataField=   "stagefield18"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "stagefield"
      Columns(31).Width=   2117
      Columns(31).Caption=   "stagefield19"
      Columns(31).Name=   "stagefield19"
      Columns(31).DataField=   "stagefield19"
      Columns(31).FieldLen=   256
      Columns(31).StyleSet=   "stagefield"
      Columns(32).Width=   2117
      Columns(32).Caption=   "stagefield20"
      Columns(32).Name=   "stagefield20"
      Columns(32).DataField=   "stagefield20"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "stagefield"
      Columns(33).Width=   2117
      Columns(33).Caption=   "stagefield21"
      Columns(33).Name=   "stagefield21"
      Columns(33).DataField=   "stagefield21"
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "stagefield"
      Columns(34).Width=   2117
      Columns(34).Caption=   "stagefield22"
      Columns(34).Name=   "stagefield22"
      Columns(34).DataField=   "stagefield22"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "stagefield"
      Columns(35).Width=   2117
      Columns(35).Caption=   "stagefield23"
      Columns(35).Name=   "stagefield23"
      Columns(35).DataField=   "stagefield23"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "stagefield"
      Columns(36).Width=   2117
      Columns(36).Caption=   "stagefield24"
      Columns(36).Name=   "stagefield24"
      Columns(36).DataField=   "stagefield24"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "stagefield"
      Columns(37).Width=   2117
      Columns(37).Caption=   "stagefield25"
      Columns(37).Name=   "stagefield25"
      Columns(37).DataField=   "stagefield25"
      Columns(37).FieldLen=   256
      Columns(37).StyleSet=   "stagefield"
      Columns(38).Width=   3200
      Columns(38).Caption=   "Date Added"
      Columns(38).Name=   "cdate"
      Columns(38).DataField=   "cdate"
      Columns(38).DataType=   7
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      _ExtentX        =   45059
      _ExtentY        =   8599
      _StockProps     =   79
      Caption         =   "EpisodeItems"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoMasterSeriesID 
      Height          =   330
      Left            =   19260
      Top             =   660
      Visible         =   0   'False
      Width           =   2565
      _ExtentX        =   4524
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   "cetasoft"
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMasterSeriesID"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   9
      Left            =   11820
      TabIndex        =   30
      Top             =   1680
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   11820
      TabIndex        =   29
      Top             =   1320
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   11820
      TabIndex        =   25
      Top             =   960
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   11820
      TabIndex        =   24
      Top             =   600
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "SeriesID"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   11820
      TabIndex        =   23
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   4800
      TabIndex        =   22
      Top             =   1680
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4800
      TabIndex        =   17
      Top             =   1320
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   4800
      TabIndex        =   16
      Top             =   960
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   4800
      TabIndex        =   13
      Top             =   600
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   4800
      TabIndex        =   12
      Top             =   240
      Width           =   2055
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   2100
      TabIndex        =   10
      Top             =   540
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   255
      Left            =   3360
      TabIndex        =   9
      Top             =   600
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   180
      TabIndex        =   2
      Top             =   240
      Width           =   915
   End
End
Attribute VB_Name = "frmDRGTracker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String

Private Sub HideAllColumns()

DoEvents

grdItems.Columns("stagefield1").Visible = False
grdItems.Columns("stagefield2").Visible = False
grdItems.Columns("stagefield3").Visible = False
grdItems.Columns("stagefield4").Visible = False
grdItems.Columns("stagefield5").Visible = False
grdItems.Columns("stagefield6").Visible = False
grdItems.Columns("stagefield7").Visible = False
grdItems.Columns("stagefield8").Visible = False
grdItems.Columns("stagefield9").Visible = False
grdItems.Columns("stagefield10").Visible = False
grdItems.Columns("stagefield11").Visible = False
grdItems.Columns("stagefield12").Visible = False
grdItems.Columns("stagefield13").Visible = False
grdItems.Columns("stagefield14").Visible = False
grdItems.Columns("stagefield15").Visible = False
grdItems.Columns("stagefield16").Visible = False
grdItems.Columns("stagefield17").Visible = False
grdItems.Columns("stagefield18").Visible = False
grdItems.Columns("stagefield19").Visible = False
grdItems.Columns("stagefield20").Visible = False
grdItems.Columns("stagefield21").Visible = False
grdItems.Columns("stagefield22").Visible = False
grdItems.Columns("stagefield23").Visible = False
grdItems.Columns("stagefield24").Visible = False
grdItems.Columns("stagefield25").Visible = False
grdItems.Columns("headertext2").Visible = False
grdItems.Columns("headertext3").Visible = False
grdItems.Columns("headertext4").Visible = False
grdItems.Columns("headertext5").Visible = False
grdItems.Columns("headerint2").Visible = False
grdItems.Columns("headerint3").Visible = False
grdItems.Columns("headerint4").Visible = False
grdItems.Columns("headerint5").Visible = False
grdComments.Columns("stagefield1").Visible = False
grdComments.Columns("stagefield2").Visible = False
grdComments.Columns("stagefield3").Visible = False
grdComments.Columns("stagefield4").Visible = False
grdComments.Columns("stagefield5").Visible = False
grdComments.Columns("stagefield6").Visible = False
grdComments.Columns("stagefield7").Visible = False
grdComments.Columns("stagefield8").Visible = False
grdComments.Columns("stagefield9").Visible = False
grdComments.Columns("stagefield10").Visible = False
grdComments.Columns("stagefield11").Visible = False
grdComments.Columns("stagefield12").Visible = False
grdComments.Columns("stagefield13").Visible = False
grdComments.Columns("stagefield14").Visible = False
grdComments.Columns("stagefield15").Visible = False
grdComments.Columns("stagefield16").Visible = False
grdComments.Columns("stagefield17").Visible = False
grdComments.Columns("stagefield18").Visible = False
grdComments.Columns("stagefield19").Visible = False
grdComments.Columns("stagefield20").Visible = False
grdComments.Columns("stagefield21").Visible = False
grdComments.Columns("stagefield22").Visible = False
grdComments.Columns("stagefield23").Visible = False
grdComments.Columns("stagefield24").Visible = False
grdComments.Columns("stagefield25").Visible = False
grdComments.Columns("headertext2").Visible = False
grdComments.Columns("headertext3").Visible = False
grdComments.Columns("headertext4").Visible = False
grdComments.Columns("headertext5").Visible = False
grdComments.Columns("headerint2").Visible = False
grdComments.Columns("headerint3").Visible = False
grdComments.Columns("headerint4").Visible = False
grdComments.Columns("headerint5").Visible = False
lblCaption(1).Visible = False
lblCaption(2).Visible = False
lblCaption(3).Visible = False
lblCaption(4).Visible = False
lblCaption(6).Visible = False
lblCaption(7).Visible = False
lblCaption(8).Visible = False
lblCaption(9).Visible = False
txtheadertext2.Visible = False
txtheadertext3.Visible = False
txtheadertext4.Visible = False
txtheadertext5.Visible = False
txtheaderint2.Visible = False
txtheaderint3.Visible = False
txtheaderint4.Visible = False
txtheaderint5.Visible = False

DoEvents

End Sub

Private Sub adoItems_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_strSQL As String

If adoItems.Recordset.EOF = True Then
    lblTrackeritemID.Caption = ""
    l_strSQL = "SELECT * FROM MasterEpisodeTracker WHERE MasterSeriesTrackerID = -1;"
    adoComments.RecordSource = l_strSQL
    adoComments.ConnectionString = g_strConnection
    adoComments.Refresh
    Exit Sub
Else
    If adoItems.Recordset("MasterSeriesTrackerID") = "" Then
        lblTrackeritemID.Caption = ""
        l_strSQL = "SELECT * FROM MasterEpisodeTracker WHERE MasterSeriesTrackerID = -1;"
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
        Exit Sub
    End If
End If

lblTrackeritemID.Caption = adoItems.Recordset("MasterSeriesTrackerID")

l_strSQL = "SELECT * FROM MasterEpisodeTracker WHERE MasterSeriesTrackerID = " & adoItems.Recordset("MasterSeriesTrackerID") & ";"

adoComments.RecordSource = l_strSQL
adoComments.ConnectionString = g_strConnection
adoComments.Refresh

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    m_strSearch = "WHERE cetaclientcode like '%material%' and companyID > 100 "
    m_strOrderby = "ORDER BY name"
    l_strSQL = "SELECT name, companyID FROM company " & m_strSearch & m_strOrderby & ";"
Else
    m_strSearch = "WHERE cetaclientcode like '%material%'  "
    m_strOrderby = "ORDER BY name"
    l_strSQL = "SELECT name, companyID FROM company " & m_strSearch & m_strOrderby & ";"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

Dim l_rstChoices As ADODB.Recordset

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

adoMasterSeriesID.RecordSource = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"
adoMasterSeriesID.ConnectionString = g_strConnection
adoMasterSeriesID.Refresh

grdItems.Columns("Title").DropDownHwnd = ddnMasterSeriesTitle.hWnd

Set l_rstChoices = ExecuteSQL("SELECT * FROM Master_tracker_itemchoice WHERE companyID = " & Val(lblCompanyID.Caption) & ";", g_strExecuteError)
CheckForSQLError

If l_rstChoices.RecordCount > 0 Then
    l_rstChoices.MoveFirst
    HideAllColumns
    Do While Not l_rstChoices.EOF
        Select Case l_rstChoices("itemfield")
            Case "stagefield1"
                grdItems.Columns("stagefield1").Visible = True
                grdItems.Columns("stagefield1").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield1").Visible = True
                grdComments.Columns("stagefield1").Caption = l_rstChoices("itemheading")
                
            Case "stagefield2"
                grdItems.Columns("stagefield2").Visible = True
                grdItems.Columns("stagefield2").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield2").Visible = True
                grdComments.Columns("stagefield2").Caption = l_rstChoices("itemheading")
                
            Case "stagefield3"
                grdItems.Columns("stagefield3").Visible = True
                grdItems.Columns("stagefield3").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield3").Visible = True
                grdComments.Columns("stagefield3").Caption = l_rstChoices("itemheading")
                
            Case "stagefield4"
                grdItems.Columns("stagefield4").Visible = True
                grdItems.Columns("stagefield4").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield4").Visible = True
                grdComments.Columns("stagefield4").Caption = l_rstChoices("itemheading")
                
            Case "stagefield5"
                grdItems.Columns("stagefield5").Visible = True
                grdItems.Columns("stagefield5").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield5").Visible = True
                grdComments.Columns("stagefield5").Caption = l_rstChoices("itemheading")
                
            Case "stagefield6"
                grdItems.Columns("stagefield6").Visible = True
                grdItems.Columns("stagefield6").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield6").Visible = True
                grdComments.Columns("stagefield6").Caption = l_rstChoices("itemheading")
                
            Case "stagefield7"
                grdItems.Columns("stagefield7").Visible = True
                grdItems.Columns("stagefield7").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield7").Visible = True
                grdComments.Columns("stagefield7").Caption = l_rstChoices("itemheading")
                
            Case "stagefield8"
                grdItems.Columns("stagefield8").Visible = True
                grdItems.Columns("stagefield8").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield8").Visible = True
                grdComments.Columns("stagefield8").Caption = l_rstChoices("itemheading")
                
            Case "stagefield9"
                grdItems.Columns("stagefield9").Visible = True
                grdItems.Columns("stagefield9").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield9").Visible = True
                grdComments.Columns("stagefield9").Caption = l_rstChoices("itemheading")
                
            Case "stagefield10"
                grdItems.Columns("stagefield10").Visible = True
                grdItems.Columns("stagefield10").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield10").Visible = True
                grdComments.Columns("stagefield10").Caption = l_rstChoices("itemheading")
                
            Case "stagefield11"
                grdItems.Columns("stagefield11").Visible = True
                grdItems.Columns("stagefield11").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield11").Visible = True
                grdComments.Columns("stagefield11").Caption = l_rstChoices("itemheading")
                
            Case "stagefield12"
                grdItems.Columns("stagefield12").Visible = True
                grdItems.Columns("stagefield12").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield12").Visible = True
                grdComments.Columns("stagefield12").Caption = l_rstChoices("itemheading")
                
            Case "stagefield13"
                grdItems.Columns("stagefield13").Visible = True
                grdItems.Columns("stagefield13").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield13").Visible = True
                grdComments.Columns("stagefield13").Caption = l_rstChoices("itemheading")
                
            Case "stagefield14"
                grdItems.Columns("stagefield14").Visible = True
                grdItems.Columns("stagefield14").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield14").Visible = True
                grdComments.Columns("stagefield14").Caption = l_rstChoices("itemheading")
                
            Case "stagefield15"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield16"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield17"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield18"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield19"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield20"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield21"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield22"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield23"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield24"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "stagefield25"
                grdItems.Columns("stagefield15").Visible = True
                grdItems.Columns("stagefield15").Caption = l_rstChoices("itemheading")
                grdComments.Columns("stagefield15").Visible = True
                grdComments.Columns("stagefield15").Caption = l_rstChoices("itemheading")
        
            Case "headertext2"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headertext2").Visible = True
                    grdItems.Columns("headertext2").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdItems.Columns("headertext2").DropDownHwnd = ddnLanguage.hWnd
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headertext2").Visible = True
                    grdComments.Columns("headertext2").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdComments.Columns("headertext2").DropDownHwnd = ddnLanguage.hWnd
                End If
                lblCaption(1).Visible = True
                lblCaption(1).Caption = "Search " & l_rstChoices("itemheading")
                If l_rstChoices("itemheading") = "Language" Then
                    txtheadertext2.Visible = False
                    cmbHeadertext2.Visible = True
                    PopulateCombo "language", cmbHeadertext2
                Else
                    txtheadertext2.Visible = True
                    cmbHeadertext2.Visible = False
                End If
            
            Case "headertext3"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headertext3").Visible = True
                    grdItems.Columns("headertext3").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdItems.Columns("headertext3").DropDownHwnd = ddnLanguage.hWnd
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headertext3").Visible = True
                    grdComments.Columns("headertext3").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdComments.Columns("headertext3").DropDownHwnd = ddnLanguage.hWnd
                End If
                lblCaption(2).Visible = True
                lblCaption(2).Caption = "Search " & l_rstChoices("itemheading")
                If l_rstChoices("itemheading") = "Language" Then
                    txtheadertext3.Visible = False
                    cmbHeadertext3.Visible = True
                    PopulateCombo "language", cmbHeadertext3
                Else
                    txtheadertext3.Visible = True
                    cmbHeadertext3.Visible = False
                End If
            
            Case "headertext4"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headertext4").Visible = True
                    grdItems.Columns("headertext4").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdItems.Columns("headertext4").DropDownHwnd = ddnLanguage.hWnd
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headertext4").Visible = True
                    grdComments.Columns("headertext4").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdComments.Columns("headertext4").DropDownHwnd = ddnLanguage.hWnd
                End If
                lblCaption(3).Visible = True
                lblCaption(3).Caption = "Search " & l_rstChoices("itemheading")
                If l_rstChoices("itemheading") = "Language" Then
                    txtheadertext4.Visible = False
                    cmbHeadertext4.Visible = True
                    PopulateCombo "language", cmbHeadertext4
                Else
                    txtheadertext4.Visible = True
                    cmbHeadertext4.Visible = False
                End If
            
            Case "headertext5"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headertext5").Visible = True
                    grdItems.Columns("headertext5").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdItems.Columns("headertext5").DropDownHwnd = ddnLanguage.hWnd
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headertext5").Visible = True
                    grdComments.Columns("headertext5").Caption = l_rstChoices("itemheading")
                    If l_rstChoices("itemheading") = "Language" Then grdComments.Columns("headertext5").DropDownHwnd = ddnLanguage.hWnd
                End If
                lblCaption(4).Visible = True
                lblCaption(4).Caption = "Search " & l_rstChoices("itemheading")
                If l_rstChoices("itemheading") = "Language" Then
                    txtheadertext5.Visible = False
                    cmbHeadertext5.Visible = True
                    PopulateCombo "language", cmbHeadertext5
                Else
                    txtheadertext5.Visible = True
                    cmbHeadertext5.Visible = False
                End If
            
            Case "headerint2"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headerint2").Visible = True
                    grdItems.Columns("headerint2").Caption = l_rstChoices("itemheading")
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headerint2").Visible = True
                    grdComments.Columns("headerint2").Caption = l_rstChoices("itemheading")
                End If
                lblCaption(6).Visible = True
                lblCaption(6).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint2.Visible = True
            
            Case "headerint3"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headerint3").Visible = True
                    grdItems.Columns("headerint3").Caption = l_rstChoices("itemheading")
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headerint3").Visible = True
                    grdComments.Columns("headerint3").Caption = l_rstChoices("itemheading")
                End If
                lblCaption(7).Visible = True
                lblCaption(7).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint3.Visible = True
            
            Case "headerint4"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headerint4").Visible = True
                    grdItems.Columns("headerint4").Caption = l_rstChoices("itemheading")
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headerint4").Visible = True
                    grdComments.Columns("headerint4").Caption = l_rstChoices("itemheading")
                End If
                lblCaption(8).Visible = True
                lblCaption(8).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint4.Visible = True
            
            Case "headerint5"
                If l_rstChoices("itemspecific") <> "E" Then
                    grdItems.Columns("headerint5").Visible = True
                    grdItems.Columns("headerint5").Caption = l_rstChoices("itemheading")
                End If
                If l_rstChoices("itemspecific") <> "S" Then
                    grdComments.Columns("headerint5").Visible = True
                    grdComments.Columns("headerint5").Caption = l_rstChoices("itemheading")
                End If
                lblCaption(9).Visible = True
                lblCaption(9).Caption = "Search " & l_rstChoices("itemheading")
                txtheaderint5.Visible = True
            
        End Select
        
        l_rstChoices.MoveNext
    Loop
End If

m_strSearch = " WHERE companyID = " & lblCompanyID.Caption
m_strOrderby = " ORDER BY Title, SeriesID, headerint2, headerint3, headerint4, headerint5"

cmdSearch.Value = True

End Sub

Private Sub cmdClear_Click()

ClearFields Me

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub


Private Sub cmdMakeEpisodes_Click()

If lblTrackeritemID.Caption = "" Then Exit Sub
If lblCompanyID.Caption = "" Then Exit Sub

If MsgBox("Are you sure you wish to make new Episode Items for this Series Item?", vbYesNo, "Making Episode Items") = vbNo Then Exit Sub

Dim l_lngCount1 As Long, l_lngCount2 As Long, i As Long, l_strSQL As String

l_lngCount1 = Val(InputBox("Starting Episode Number"))
l_lngCount2 = Val(InputBox("Ending Episode Number"))
If l_lngCount2 = 0 Then l_lngCount2 = l_lngCount1

If l_lngCount1 > 0 And l_lngCount2 > 0 And l_lngCount2 >= l_lngCount1 Then
    
    For i = l_lngCount1 To l_lngCount2
        
        l_strSQL = "INSERT INTO MasterEpisodeTracker (companyID, MasterSeriesTrackerID, Episode, Title, headertext2, headertext3, headertext4, headertext5, SeriesID, headerint2, headerint3, headerint4, headerint5, "
        l_strSQL = l_strSQL & "stagefield1, stagefield2, stagefield3, stagefield4, stagefield5, stagefield6, stagefield7, "
        l_strSQL = l_strSQL & "stagefield8, stagefield9, stagefield10, stagefield11, stagefield12, stagefield13, stagefield14, stagefield15, "
        l_strSQL = l_strSQL & "stagefield16, stagefield17, stagefield18, stagefield19, stagefield20, stagefield21, stagefield22, stagefield23, stagefield24, stagefield25"
        l_strSQL = l_strSQL & ") VALUES ("
        l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
        l_strSQL = l_strSQL & grdItems.Columns("MasterSeriesTrackerID").Text & ", "
        l_strSQL = l_strSQL & i & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("Title").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext2").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext3").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext4").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext5").Text) & "', "
        If grdItems.Columns("SeriesID").Text <> "" Then
            l_strSQL = l_strSQL & "'" & grdItems.Columns("SeriesID").Text & "', "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If grdItems.Columns("headerint2").Text <> "" Then
            l_strSQL = l_strSQL & grdItems.Columns("headerint2").Text & ", "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If grdItems.Columns("headerint3").Text <> "" Then
            l_strSQL = l_strSQL & grdItems.Columns("headerint3").Text & ", "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If grdItems.Columns("headerint4").Text <> "" Then
            l_strSQL = l_strSQL & grdItems.Columns("headerint4").Text & ", "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        If grdItems.Columns("headerint5").Text <> "" Then
            l_strSQL = l_strSQL & grdItems.Columns("headerint5").Text & ", "
        Else
            l_strSQL = l_strSQL & "NULL, "
        End If
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield1").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield2").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield3").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield4").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield5").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield6").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield7").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield8").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield9").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield10").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield11").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield12").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield13").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield14").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield15").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield16").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield17").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield18").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield19").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield20").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield21").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield22").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield23").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield24").Text) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield25").Text) & "'); "
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
    Next
    
Else
    l_strSQL = "INSERT INTO MasterEpisodeTracker (companyID, MasterSeriesTrackerID, Title, headertext2, headertext3, headertext4, headertext5, SeriesID, headerint2, headerint3, headerint4, headerint5, "
    l_strSQL = l_strSQL & "stagefield1, stagefield2, stagefield3, stagefield4, stagefield5, stagefield6, stagefield7, "
    l_strSQL = l_strSQL & "stagefield8, stagefield9, stagefield10, stagefield11, stagefield12, stagefield13, stagefield14, stagefield15) VALUES ("
    l_strSQL = l_strSQL & lblCompanyID.Caption & ", "
    l_strSQL = l_strSQL & grdItems.Columns("MasterSeriesTrackerID").Text & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("Title").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext2").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext3").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext4").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("headertext5").Text) & "', "
    If grdItems.Columns("SeriesID").Text <> "" Then
        l_strSQL = l_strSQL & "'" & grdItems.Columns("SeriesID").Text & "', "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    If grdItems.Columns("headerint2").Text <> "" Then
        l_strSQL = l_strSQL & grdItems.Columns("headerint2").Text & ", "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    l_strSQL = l_strSQL & "NULL, "
    If grdItems.Columns("headerint4").Text <> "" Then
        l_strSQL = l_strSQL & grdItems.Columns("headerint4").Text & ", "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    If grdItems.Columns("headerint5").Text <> "" Then
        l_strSQL = l_strSQL & grdItems.Columns("headerint5").Text & ", "
    Else
        l_strSQL = l_strSQL & "NULL, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield1").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield2").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield3").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield4").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield5").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield6").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield7").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield8").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield9").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield10").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield11").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield12").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield13").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield14").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield15").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield16").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield17").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield18").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield19").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield20").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield21").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield22").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield23").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield24").Text) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(grdItems.Columns("stagefield25").Text) & "'); "
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
End If

adoComments.Refresh

End Sub

Private Sub cmdSearch_Click()

Dim l_rstTotal As ADODB.Recordset, l_lngVideoTotal As Long
Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then

    If txtheadertext1.Text <> "" Then
        l_strSQL = l_strSQL & " AND title LIKE '" & QuoteSanitise(txtheadertext1.Text) & "%' "
    End If
    If txtheadertext2.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext2 LIKE '" & QuoteSanitise(txtheadertext2.Text) & "%' "
    End If
    If cmbHeadertext2.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext2 LIKE '" & QuoteSanitise(cmbHeadertext2.Text) & "%' "
    End If
    If txtheadertext3.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext3 LIKE '" & QuoteSanitise(txtheadertext3.Text) & "%' "
    End If
    If cmbHeadertext3.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext3 LIKE '" & QuoteSanitise(cmbHeadertext3.Text) & "%' "
    End If
    If txtheadertext4.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext4 LIKE '" & QuoteSanitise(txtheadertext4.Text) & "%' "
    End If
    If cmbHeadertext4.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext4 LIKE '" & QuoteSanitise(cmbHeadertext4.Text) & "%' "
    End If
    If txtheadertext5.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext5 LIKE '" & QuoteSanitise(txtheadertext5.Text) & "%' "
    End If
    If cmbHeadertext5.Text <> "" Then
        l_strSQL = l_strSQL & " AND headertext5 LIKE '" & QuoteSanitise(cmbHeadertext5.Text) & "%' "
    End If
    If txtSeriesID.Text <> "" Then
        l_strSQL = l_strSQL & " AND SeriesID = '" & txtSeriesID.Text & "' "
    End If
    If Val(txtheaderint2.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint2 = " & Val(txtheaderint2.Text) & " "
    End If
    If Val(txtheaderint3.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint3 = " & Val(txtheaderint3.Text) & " "
    End If
    If Val(txtheaderint4.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint4 = " & Val(txtheaderint4.Text) & " "
    End If
    If Val(txtheaderint5.Text) <> 0 Then
        l_strSQL = l_strSQL & " AND headerint5 = " & Val(txtheaderint5.Text) & " "
    End If
    
    adoItems.ConnectionString = g_strConnection
    adoItems.RecordSource = "SELECT * FROM MasterSeriesTracker " & m_strSearch & l_strSQL & m_strOrderby & ";"
    adoItems.Refresh
    
    adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"

End If

End Sub

Private Sub ddnMasterSeriesTitle_CloseUp()

grdItems.Columns("seriesID").Text = ddnMasterSeriesTitle.Columns("seriesID").Text

End Sub

Private Sub Form_Activate()

'HideAllColumns

DoEvents

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdComments.StyleSets("headerfield").BackColor = &HE7FFE7
grdComments.StyleSets("stagefield").BackColor = &HE7FFFF
grdComments.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdComments.StyleSets.Add "Error"
grdComments.StyleSets("Error").BackColor = &HA0A0FF

If chkHideDemo.Value <> 0 Then
    m_strSearch = "WHERE cetaclientcode like '%material%' and companyID > 100 AND system_active = 1 "
    m_strOrderby = "ORDER BY name"
    l_strSQL = "SELECT name, companyID FROM company " & m_strSearch & m_strOrderby & ";"
Else
    m_strSearch = "WHERE cetaclientcode like '%material%' AND system_active = 1  "
    m_strOrderby = "ORDER BY name"
    l_strSQL = "SELECT name, companyID FROM company " & m_strSearch & m_strOrderby & ";"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

PopulateCombo "language", ddnLanguage

DoEvents

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) * 0.7 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120
grdComments.Top = grdItems.Top + grdItems.Height + 120
grdComments.Height = frmButtons.Top - grdComments.Top - 120
grdComments.Width = grdItems.Width

End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

grdComments.Columns("MasterSeriesTrackerID").Text = lblTrackeritemID.Caption
grdComments.Columns("companyID").Text = lblCompanyID.Caption

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

grdItems.Columns("companyID").Text = lblCompanyID.Caption

End Sub

Private Sub grdItems_HeadClick(ByVal ColIndex As Integer)

m_strOrderby = " ORDER BY " & grdItems.Columns(ColIndex).Name
adoItems.RecordSource = "SELECT * FROM MasterSeriesTracker " & m_strSearch & m_strOrderby & ";"
adoItems.Refresh

End Sub
