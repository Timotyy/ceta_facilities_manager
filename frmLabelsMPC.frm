VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmLabelsMPC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Label Generation"
   ClientHeight    =   8610
   ClientLeft      =   4605
   ClientTop       =   3945
   ClientWidth     =   14025
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8610
   ScaleWidth      =   14025
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdPrintAndLoop 
      Caption         =   "Print"
      Height          =   315
      Left            =   1980
      TabIndex        =   40
      Top             =   7200
      Width           =   1155
   End
   Begin VB.ComboBox cmbSelectPrinter 
      Height          =   315
      Left            =   720
      TabIndex        =   38
      Tag             =   "NOCLEAR"
      Top             =   5820
      Width           =   3675
   End
   Begin VB.CommandButton cmdStandardText 
      Caption         =   "Standard Text (Show / Hide)"
      Height          =   315
      Left            =   1980
      TabIndex        =   37
      ToolTipText     =   "Print MPC labels"
      Top             =   4500
      Width           =   2415
   End
   Begin VB.PictureBox picStandardtext 
      Appearance      =   0  'Flat
      BackColor       =   &H80000003&
      ForeColor       =   &H80000008&
      Height          =   3975
      Left            =   4560
      ScaleHeight     =   3945
      ScaleWidth      =   3045
      TabIndex        =   35
      Top             =   180
      Visible         =   0   'False
      Width           =   3075
      Begin VB.ListBox lstStandardText 
         Height          =   3765
         Left            =   60
         TabIndex        =   36
         Top             =   60
         Width           =   2895
      End
   End
   Begin VB.CommandButton cmdPrintNew 
      Caption         =   "Print"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1980
      TabIndex        =   34
      Top             =   5100
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      Height          =   8415
      Left            =   4500
      ScaleHeight     =   8355
      ScaleWidth      =   9315
      TabIndex        =   33
      Top             =   120
      Width           =   9375
   End
   Begin VB.CommandButton cmdPreview 
      Caption         =   "Preview"
      Height          =   315
      Left            =   720
      TabIndex        =   32
      Top             =   7200
      Width           =   1155
   End
   Begin VB.TextBox txtYOffset 
      Height          =   285
      Left            =   1920
      TabIndex        =   29
      Tag             =   "NOCLEAR"
      Top             =   6600
      Width           =   675
   End
   Begin VB.TextBox txtXOffset 
      Height          =   285
      Left            =   720
      TabIndex        =   28
      Tag             =   "NOCLEAR"
      Top             =   6600
      Width           =   675
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   315
      Left            =   720
      TabIndex        =   27
      ToolTipText     =   "Print MPC labels"
      Top             =   4500
      Width           =   1155
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   11
      Left            =   3600
      TabIndex        =   11
      Tag             =   "NOCLEAR"
      Text            =   "1"
      ToolTipText     =   "Main "
      Top             =   6600
      Width           =   420
   End
   Begin MSComCtl2.UpDown UpDown1 
      Height          =   285
      Left            =   4080
      TabIndex        =   26
      Top             =   6600
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      BuddyControl    =   "txtLabel(11)"
      BuddyDispid     =   196620
      BuddyIndex      =   11
      OrigLeft        =   1500
      OrigTop         =   4260
      OrigRight       =   1755
      OrigBottom      =   4575
      SyncBuddy       =   -1  'True
      BuddyProperty   =   0
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   10
      Left            =   720
      TabIndex        =   10
      ToolTipText     =   "Text line 3"
      Top             =   3900
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   9
      Left            =   720
      TabIndex        =   9
      ToolTipText     =   "Text line 4"
      Top             =   3360
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   8
      Left            =   720
      TabIndex        =   8
      ToolTipText     =   "Text line 5"
      Top             =   3000
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   7
      Left            =   720
      TabIndex        =   7
      ToolTipText     =   "Text line 6"
      Top             =   2640
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   6
      Left            =   720
      TabIndex        =   6
      ToolTipText     =   "Main "
      Top             =   2280
      Width           =   3675
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   3240
      TabIndex        =   13
      ToolTipText     =   "Cancel this form"
      Top             =   7200
      Width           =   1155
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print"
      Height          =   315
      Left            =   720
      TabIndex        =   12
      ToolTipText     =   "Print MPC labels"
      Top             =   5100
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   5
      Left            =   720
      TabIndex        =   5
      ToolTipText     =   "Text line 6"
      Top             =   1920
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   4
      Left            =   720
      TabIndex        =   4
      ToolTipText     =   "Text line 5"
      Top             =   1560
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   3
      Left            =   720
      TabIndex        =   3
      ToolTipText     =   "Text line 4"
      Top             =   1200
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   2
      Left            =   720
      TabIndex        =   2
      ToolTipText     =   "Text line 3"
      Top             =   840
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   1
      Left            =   720
      TabIndex        =   1
      ToolTipText     =   "Text line 2"
      Top             =   480
      Width           =   3675
   End
   Begin VB.TextBox txtLabel 
      Height          =   285
      Index           =   0
      Left            =   720
      TabIndex        =   0
      ToolTipText     =   "Text line 1"
      Top             =   120
      Width           =   3675
   End
   Begin VB.Label lblCaption 
      Caption         =   "Printer"
      Height          =   195
      Index           =   12
      Left            =   120
      TabIndex        =   39
      Top             =   5820
      Width           =   660
   End
   Begin VB.Label lblCaption 
      Caption         =   "Y"
      Height          =   195
      Index           =   14
      Left            =   1515
      TabIndex        =   31
      Top             =   6600
      Width           =   660
   End
   Begin VB.Label lblCaption 
      Caption         =   "X"
      Height          =   195
      Index           =   13
      Left            =   135
      TabIndex        =   30
      Top             =   6600
      Width           =   660
   End
   Begin VB.Label lblCaption 
      Caption         =   "Copies"
      Height          =   195
      Index           =   11
      Left            =   2820
      TabIndex        =   25
      Top             =   6600
      Width           =   660
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 7"
      Height          =   195
      Index           =   10
      Left            =   120
      TabIndex        =   24
      Top             =   2280
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 8"
      Height          =   195
      Index           =   9
      Left            =   120
      TabIndex        =   23
      Top             =   2640
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 9"
      Height          =   195
      Index           =   8
      Left            =   120
      TabIndex        =   22
      Top             =   3000
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 10"
      Height          =   195
      Index           =   7
      Left            =   120
      TabIndex        =   21
      Top             =   3360
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Main"
      Height          =   195
      Index           =   6
      Left            =   120
      TabIndex        =   20
      Top             =   3900
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 6"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   19
      Top             =   1920
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 5"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   18
      Top             =   1560
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 4"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   17
      Top             =   1200
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 3"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   16
      Top             =   840
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 2"
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   15
      Top             =   480
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Line 1"
      Height          =   195
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   120
      Width           =   795
   End
End
Attribute VB_Name = "frmLabelsMPC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim m_intCurrentBox As Integer

Dim m_intStillPrinting As Integer

Private Sub cmdCancel_Click()
Unload Me
End Sub

Private Sub cmdClear_Click()
ClearFields Me
Picture1.Cls

End Sub

Private Sub cmdPreview_Click()

    Dim txt1$
    
    Dim l_intLoop               As Integer
    Dim l_intNumberOfLines      As Integer
    Dim l_dblLabelYPosition     As Double
    Dim l_intLabelLoop          As Integer
    Dim l_sngFontSize           As Single
    Dim l_sngLabelHeight        As Single
    Dim l_sngLongestText        As Single
    Dim l_sngLongestTextWidth   As Single
    Dim l_sngXOffset            As Single
    Dim l_sngYOffset            As Single
    
    'pick up the offsets
    l_sngXOffset = Val(txtXOffset.Text)
    l_sngYOffset = Val(txtYOffset.Text)
    
    'check which box has the longest text
    For l_intLoop = 0 To 10
       If txtLabel(l_intLoop).Text <> "" Then l_intNumberOfLines = l_intNumberOfLines + 1
       
       If Picture1.TextWidth(txtLabel(l_intLoop).Text) > l_sngLongestText Then
            l_sngLongestText = Picture1.TextWidth(txtLabel(l_intLoop).Text)
            l_sngLongestTextWidth = l_intLoop
       End If

    Next
    
    'clear the box and set the default measuring scalemode
    Picture1.Cls
    Picture1.ScaleMode = 7
    Printer.ScaleMode = 7

    'set the picture to be the same size etc as the printer
    Picture1.Height = Printer.Height
    Picture1.Width = Printer.Width

    'set the fonts used
    
    l_sngFontSize = 16 - l_intNumberOfLines
    Picture1.FontName = "Arial"
    Picture1.FontSize = 16 - l_intNumberOfLines
    Picture1.FontBold = True

    'do the four main biggest labels first
    For l_intLabelLoop = 1 To 4
    
        l_dblLabelYPosition = Choose(l_intLabelLoop, 1.5, 5.1, 11.3, 14.9)
       
        Picture1.Line (1.5 + l_sngXOffset, l_dblLabelYPosition + l_sngYOffset)-(8 + l_sngXOffset, l_dblLabelYPosition + 3 + l_sngYOffset), 255, B
       
        Picture1.CurrentY = l_dblLabelYPosition + ((3 - (Picture1.TextHeight("|") * l_intNumberOfLines)) / 2) + l_sngYOffset
    
        Do Until Picture1.TextWidth(txtLabel(l_sngLongestTextWidth).Text) < 5.5
            l_sngFontSize = l_sngFontSize - 0.5
            If l_sngFontSize < 5 Then Exit Do
            Picture1.FontSize = l_sngFontSize
        Loop
    
        For l = 0 To 9
            If txtLabel(l).Text <> "" Then
                Picture1.CurrentX = 1.5 + ((6.5 - Picture1.TextWidth(txtLabel(l).Text)) / 2) + l_sngXOffset
                Picture1.Print txtLabel(l).Text
            End If
        Next

    Next

    'do the two smaller lines, with big captions
    For l_intLabelLoop = 1 To 2
        
        Picture1.FontSize = 36
        l_sngFontSize = 36
        
        Do Until Picture1.TextWidth(txtLabel(10).Text) < 5.5
            l_sngFontSize = l_sngFontSize - 0.5
            If l_sngFontSize < 5 Then Exit Do
            Picture1.FontSize = l_sngFontSize
        Loop

        l_dblLabelYPosition = Choose(l_intLabelLoop, 8.8, 18.6)

        Picture1.Line (1.5 + l_sngXOffset, l_dblLabelYPosition + l_sngYOffset)-(8 + l_sngXOffset, l_dblLabelYPosition + 1.4 + l_sngYOffset), 255, B
       
        Picture1.CurrentY = l_dblLabelYPosition + ((1.4 - (Picture1.TextHeight("|") * 1)) / 2) + l_sngYOffset
        
        If txtLabel(10).Text <> "" Then
            Picture1.CurrentX = 1.5 + ((6.5 - Picture1.TextWidth(txtLabel(10).Text)) / 2) + l_sngXOffset
            Picture1.Print txtLabel(10).Text
        End If
      
    Next
    
    'set font size
    Picture1.FontSize = 16 - l_intNumberOfLines
    Picture1.FontBold = True
    
    'get the text length
    For l = 0 To 10
        If txtLabel(l).Text <> "" Then l_intNumberOfLines = l_intNumberOfLines + 1
        If Picture1.TextWidth(txtLabel(l).Text) > l_sngLongestText Then
            l_sngLongestText = Picture1.TextWidth(txtLabel(l).Text)
            l_sngLongestTextWidth = l
        End If

    Next
    
    'do the 6 labels on the right
    For l_intLabelLoop = 1 To 6
        
        'pick up the top position using choose
        l_dblLabelYPosition = Choose(l_intLabelLoop, 1.5, 5.1, 8.7, 11.3, 14.9, 18.5)
        
        'is this one of the smaller labels?
        If l_intLabelLoop = 3 Or l_intLabelLoop = 6 Then
            l_sngLabelHeight = 2
        Else
            l_sngLabelHeight = 3
        End If
        
        'set the font size to be bigger than required
        Picture1.FontSize = 36
        l_sngFontSize = 36
        
        'then loop around and make it smaller...
        Do Until (Picture1.TextHeight("|") * l_intNumberOfLines) < l_sngLabelHeight And Picture1.TextWidth(txtLabel(l_sngLongestTextWidth).Text) < 5
            l_sngFontSize = l_sngFontSize - 0.5
            If l_sngFontSize < 5 Then Exit Do
            Picture1.FontSize = l_sngFontSize
        Loop
        
        'print on the lines, as this is a preview
        Picture1.Line (10 + l_sngXOffset, l_dblLabelYPosition + l_sngYOffset)-(13 + l_sngXOffset, l_dblLabelYPosition + l_sngLabelHeight + l_sngYOffset), 255, B
       
        'set the y position including the offset etc
        Picture1.CurrentY = (l_dblLabelYPosition + l_sngYOffset) + 0.15  ' - ((Picture1.TextHeight("|") * l_intNumberOfLines) / 2)
    
        'print the text
        For l = 0 To 9
            If txtLabel(l).Text <> "" Then
                Picture1.CurrentX = 10.2 + l_sngXOffset
                Picture1.Print txtLabel(l).Text
            End If
        Next
        
    Next

End Sub

Private Sub cmdPrintAndLoop_Click()

Dim l_intLoop As Integer



'print as many copies as required by looping
For l_intLoop = 1 To Val(txtLabel(11).Text)
    cmdPrintNew.Value = True
    DoEvents
Next

End Sub

Private Sub cmdPrintNew_Click()

On Error GoTo cmdPrintNew_Error
Dim l_strCode As String

l_strCode = "Setting printer"
Set Printer = Printers(cmbSelectPrinter.ListIndex)

Dim txt1$

Dim l_intCopies             As Integer
Dim l_intLoop               As Integer
Dim l_intNumberOfLines      As Integer
Dim l_dblLabelYPosition     As Double
Dim l_intLabelLoop          As Integer
Dim l_sngFontSize           As Single
Dim l_sngLabelHeight        As Single
Dim l_sngLongestText        As Single
Dim l_sngLongestTextWidth   As Single
Dim l_sngXOffset            As Single
Dim l_sngYOffset            As Single


Do Until m_intStillPrinting = False

    DoEvents
    
Loop

m_intStillPrinting = True

l_strCode = "Getting offsets"

'pick up the offsets
l_sngXOffset = Val(txtXOffset.Text)
l_sngYOffset = Val(txtYOffset.Text)
    
l_strCode = "Starting loop for number of copies"
'For l_intCopies = 1 To Val(txtLabel(11).Text)
    
    'check which box has the longest text
    For l_intLoop = 0 To 10
       If txtLabel(l_intLoop).Text <> "" Then l_intNumberOfLines = l_intNumberOfLines + 1
       
       If Printer.TextWidth(txtLabel(l_intLoop).Text) > l_sngLongestText Then
            l_sngLongestText = Printer.TextWidth(txtLabel(l_intLoop).Text)
            l_sngLongestTextWidth = l_intLoop
       End If

    Next
    
    'clear the box and set the default measuring scalemode
    
    Printer.ScaleMode = 7
    Printer.ScaleMode = 7

    'set the picture to be the same size etc as the printer
    Printer.Height = Printer.Height
    Printer.Width = Printer.Width

    'set the fonts used
    
    l_sngFontSize = 16 - l_intNumberOfLines
    Printer.FontName = "Arial"
    Printer.FontSize = 16 - l_intNumberOfLines
    Printer.FontBold = True

    'do the four main biggest labels first
    For l_intLabelLoop = 1 To 4
    
        l_dblLabelYPosition = Choose(l_intLabelLoop, 1.5, 5.1, 11.3, 14.9)
       
        'Printer.Line (1.5 + l_sngXOffset, l_dblLabelYPosition + l_sngYOffset)-(8 + l_sngXOffset, l_dblLabelYPosition + 3 + l_sngYOffset), 255, B
       
        Printer.CurrentY = l_dblLabelYPosition + ((3 - (Printer.TextHeight("|") * l_intNumberOfLines)) / 2) + l_sngYOffset
    
        Do Until Printer.TextWidth(txtLabel(l_sngLongestTextWidth).Text) < 5.5
            l_sngFontSize = l_sngFontSize - 0.5
            If l_sngFontSize < 5 Then Exit Do
            Printer.FontSize = l_sngFontSize
        Loop
    
        For l = 0 To 9
            If txtLabel(l).Text <> "" Then
                Printer.CurrentX = 1.5 + ((6.5 - Printer.TextWidth(txtLabel(l).Text)) / 2) + l_sngXOffset
                Printer.Print txtLabel(l).Text
            End If
        Next

    Next

    'do the two smaller lines, with big captions
    For l_intLabelLoop = 1 To 2
        
        Printer.FontSize = 36
        l_sngFontSize = 36
        
        Do Until Printer.TextWidth(txtLabel(10).Text) < 5.5
            l_sngFontSize = l_sngFontSize - 0.5
            If l_sngFontSize < 5 Then Exit Do
            Printer.FontSize = l_sngFontSize
        Loop

        l_dblLabelYPosition = Choose(l_intLabelLoop, 8.8, 18.6)

        'Printer.Line (1.5 + l_sngXOffset, l_dblLabelYPosition + l_sngYOffset)-(8 + l_sngXOffset, l_dblLabelYPosition + 1.4 + l_sngYOffset), 255, B
       
        Printer.CurrentY = l_dblLabelYPosition + ((1.4 - (Printer.TextHeight("|") * 1)) / 2) + l_sngYOffset
        
        If txtLabel(10).Text <> "" Then
            Printer.CurrentX = 1.5 + ((6.5 - Printer.TextWidth(txtLabel(10).Text)) / 2) + l_sngXOffset
            Printer.Print txtLabel(10).Text
        End If
      
    Next
    
    'set font size
    Printer.FontSize = 16 - l_intNumberOfLines
    Printer.FontBold = True
    
    'get the text length
    For l = 0 To 10
        If txtLabel(l).Text <> "" Then l_intNumberOfLines = l_intNumberOfLines + 1
        If Printer.TextWidth(txtLabel(l).Text) > l_sngLongestText Then
            l_sngLongestText = Printer.TextWidth(txtLabel(l).Text)
            l_sngLongestTextWidth = l
        End If

    Next
    
    'do the 6 labels on the right
    For l_intLabelLoop = 1 To 6
        
        'pick up the top position using choose
        l_dblLabelYPosition = Choose(l_intLabelLoop, 1.5, 5.1, 8.7, 11.3, 14.9, 18.5)
        
        'is this one of the smaller labels?
        If l_intLabelLoop = 3 Or l_intLabelLoop = 6 Then
            l_sngLabelHeight = 2
        Else
            l_sngLabelHeight = 3
        End If
        
        'set the font size to be bigger than required
        Printer.FontSize = 36
        l_sngFontSize = 36
        
        'then loop around and make it smaller...
        Do Until (Printer.TextHeight("|") * l_intNumberOfLines) < l_sngLabelHeight And Printer.TextWidth(txtLabel(l_sngLongestTextWidth).Text) < 5
            l_sngFontSize = l_sngFontSize - 0.5
            If l_sngFontSize < 5 Then Exit Do
            Printer.FontSize = l_sngFontSize
        Loop
        
        'print on the lines, as this is a preview
        'Printer.Line (10 + l_sngXOffset, l_dblLabelYPosition + l_sngYOffset)-(13 + l_sngXOffset, l_dblLabelYPosition + l_sngLabelHeight + l_sngYOffset), 255, B
       
        'set the y position including the offset etc
        Printer.CurrentY = (l_dblLabelYPosition + l_sngYOffset) + 0.15  ' - ((Printer.TextHeight("|") * l_intNumberOfLines) / 2)
    
        'print the text
        For l = 0 To 9
            If txtLabel(l).Text <> "" Then
                Printer.CurrentX = 10.2 + l_sngXOffset
                Printer.Print txtLabel(l).Text
            End If
        Next
        
        
        
    Next
   
   Printer.EndDoc
   
'Next

m_intStillPrinting = False

Exit Sub
cmdPrintNew_Error:
MsgBox Err.Description & vbCrLf & vbCrLf & l_strCode, vbExclamation
Printer.EndDoc
Screen.MousePointer = vbDefault
Exit Sub

End Sub


Private Sub cmdStandardText_Click()
picStandardtext.Visible = Not picStandardtext.Visible
End Sub

Private Sub Form_Load()

Dim l_strLabelPrinter As String
Dim a As String
Dim x As Printer
Dim prt As Printer


'txtXOffset.Text = GetSetting("CETA Facilities Manager", "LabelPrinting", "XOffset", Val(txtXOffset.Text))
'txtYOffset.Text = GetSetting("CETA Facilities Manager", "LabelPrinting", "YOffset", Val(txtYOffset.Text))

'l_strLabelPrinter = GetSetting("CETA Facilities Manager", "LabelPrinting", "LabelPrinter", "")

On Error Resume Next

Open "c:\labels.txt" For Input As 1
Line Input #1, a
txtXOffset.Text = a
Line Input #1, a
txtYOffset.Text = a
Line Input #1, a
l_strLabelPrinter = a
Close 1

cmbSelectPrinter.Clear
'Load the combo with all available printers
For Each x In Printers
   cmbSelectPrinter.AddItem x.DeviceName
   If Printer.DeviceName = x.DeviceName Then 'Current default
      cmbSelectPrinter.Text = x.DeviceName
   End If
Next

'Set the number of copies to 1
cmbSelectPrinter.ListIndex = GetListIndex(cmbSelectPrinter, l_strLabelPrinter)

'This code would probably go in the Form_Load of your printer selection form

'Load Printers
For Each prt In Printers
     cboSelectPrinter.AddItem prt.DeviceName
Next


'This code would go in the function that gets called
'CenterForm Me

PopulateCombo "standardlabeltext", lstStandardText

lstStandardText.AddItem Format(Date, "ddd dd mmm yyyy")
lstStandardText.AddItem Format(Date, "dd/mm/yyyy")
lstStandardText.AddItem Format(Date, "dd/mm/yy")


m_intCurrentBox = 0

CenterForm Me

End Sub

Private Sub Form_Unload(Cancel As Integer)

On Error GoTo Unload_Error

Open "c:\labels.txt" For Output As 1
Print #1, txtXOffset.Text
Print #1, txtYOffset.Text
Print #1, cmbSelectPrinter.Text
Close 1

'SaveSetting "CETA Facilities Manager", "LabelPrinting", "LabelPrinter", cmbSelectPrinter.Text

Exit Sub

Unload_Error:

MsgBox Err.Description, vbExclamation
Exit Sub

End Sub


Private Sub lstStandardText_Click()

Clipboard.SetText lstStandardText.List(lstStandardText.ListIndex)

End Sub

Private Sub lstStandardText_DblClick()

txtLabel(m_intCurrentBox).Text = txtLabel(m_intCurrentBox).Text & " " & lstStandardText.List(lstStandardText.ListIndex)

End Sub


Private Sub txtLabel_GotFocus(Index As Integer)

m_intCurrentBox = Index

End Sub


