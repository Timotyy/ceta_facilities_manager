VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3480
   ClientLeft      =   5715
   ClientTop       =   5985
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   ScaleHeight     =   3480
   ScaleWidth      =   6585
   Begin VB.CommandButton cmdStart 
      Caption         =   "Start"
      Height          =   555
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1755
   End
   Begin VB.Label lblCostingID 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   555
      Left            =   2460
      TabIndex        =   1
      Top             =   240
      Width           =   1635
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdStart_Click()


Dim dbLive As New ADODB.Connection
Dim dbOld As New ADODB.Connection


dbLive.Open "DSN=narsil"
dbOld.Open g_strConnection


Dim l_strSQL As String

l_strSQL = "SELECT costingID FROM costing ORDER BY costingID"

Dim rstLive As New ADODB.Recordset
Dim rstOld As New ADODB.Recordset
Dim rstRecord As New ADODB.Recordset

'open the costing table
rstOld.Open l_strSQL, dbOld, adOpenForwardOnly, adLockReadOnly
rstOld.MoveFirst


Dim l_lngCostingID As Long

'loop through every costing record and check if it is in the live system
'if it isnt then add it
Do While Not rstOld.EOF
    l_lngCostingID = rstOld("costingID")
    
    lblCostingID.Caption = l_lngCostingID

    DoEvents

    l_strSQL = "SELECT costingID FROM costing WHERE costingID = " & l_lngCostingID
    
    rstLive.Open l_strSQL, dbLive, adOpenDynamic, adLockOptimistic
    
    If rstLive.EOF Then
        'add this line as it doesnt exist
        
        rstRecord.Open "SELECT * FROM costing WHERE costingID = " & l_lngCostingID, dbOld, adOpenStatic, adLockReadOnly
        
        
        l_strSQL = "INSERT INTO costing ("
        l_strSQL = l_strSQL & "costingID, "
        l_strSQL = l_strSQL & "costingsheetID, "
        l_strSQL = l_strSQL & "creditnotenumber, "
        l_strSQL = l_strSQL & "invoicenumber, "
        l_strSQL = l_strSQL & "jobID, "
        l_strSQL = l_strSQL & "copytype, "
        l_strSQL = l_strSQL & "accountcode, "
        l_strSQL = l_strSQL & "costcode, "
        l_strSQL = l_strSQL & "description, "
        l_strSQL = l_strSQL & "quantity, "
        l_strSQL = l_strSQL & "fd_time, "
        l_strSQL = l_strSQL & "unitcharge, "
        l_strSQL = l_strSQL & "discount, "
        l_strSQL = l_strSQL & "total, "
        l_strSQL = l_strSQL & "unit, "
        l_strSQL = l_strSQL & "vat, "
        l_strSQL = l_strSQL & "totalincludingvat, "
        l_strSQL = l_strSQL & "category, "
        l_strSQL = l_strSQL & "fd_order, "
        l_strSQL = l_strSQL & "includedindealprice, "
        l_strSQL = l_strSQL & "ratecardprice, "
        l_strSQL = l_strSQL & "ratecardtotal, "
        l_strSQL = l_strSQL & "ratecardcategory, "
        l_strSQL = l_strSQL & "ratecardorderby) VALUES ("
        
        l_strSQL = l_strSQL & "'" & rstRecord("costingID") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("costingsheetID") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("creditnotenumber") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("invoicenumber") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("jobID") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("copytype") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(rstRecord("accountcode")) & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("costcode") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(rstRecord("description")) & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("quantity") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("fd_time") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("unitcharge") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("discount") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("total") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("unit") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("vat") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("totalincludingvat") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(rstRecord("category")) & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("fd_order") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("includedindealprice") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("ratecardprice") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("ratecardtotal") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("ratecardcategory") & "', "
        l_strSQL = l_strSQL & "'" & rstRecord("ratecardorderby") & "');"
        
        dbLive.Execute l_strSQL
        
        rstRecord.Close
        
    End If
    
    rstLive.Close

    rstOld.MoveNext
    
    'DBEngine.Idle
    'DoEvents

    If rstOld.EOF Then Exit Do

Loop

rstOld.Close
Set rstOld = Nothing

Set rstRecord = Nothing


'close the databases
dbOld.Close
Set dbOld = Nothing

dbLive.Close
Set dbLive = Nothing



End Sub
