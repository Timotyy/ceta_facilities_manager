VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmClipSegment 
   Caption         =   "Segment File"
   ClientHeight    =   8880
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   14985
   LinkTopic       =   "Excel Orders"
   ScaleHeight     =   8880
   ScaleWidth      =   14985
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   675
      Left            =   11700
      TabIndex        =   19
      Top             =   3540
      Width           =   2355
      Begin VB.OptionButton optHidden 
         Caption         =   "Hidden"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   21
         Top             =   240
         Width           =   1035
      End
      Begin VB.OptionButton optHidden 
         Caption         =   "Visible"
         Height          =   255
         Index           =   1
         Left            =   1380
         TabIndex        =   20
         Top             =   240
         Width           =   795
      End
   End
   Begin VB.CheckBox chkOverWrite 
      Caption         =   "Overwrite if already exists"
      Height          =   255
      Left            =   11820
      TabIndex        =   18
      Top             =   3300
      Width           =   2295
   End
   Begin VB.CheckBox chkAudioOnly 
      Caption         =   "Audio Only"
      Height          =   255
      Left            =   11820
      TabIndex        =   15
      Top             =   3000
      Width           =   1515
   End
   Begin VB.CheckBox chkVideoOnly 
      Caption         =   "Video Only"
      Height          =   255
      Left            =   11820
      TabIndex        =   14
      Top             =   2700
      Width           =   1515
   End
   Begin VB.Frame frmTimecode 
      Caption         =   "Select Timecode Type"
      Height          =   1515
      Left            =   11640
      TabIndex        =   9
      Top             =   1140
      Width           =   3075
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "24 fps"
         Height          =   255
         Index           =   3
         Left            =   180
         TabIndex        =   13
         Top             =   300
         Width           =   1995
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "30 fps"
         Height          =   255
         Index           =   2
         Left            =   180
         TabIndex        =   12
         Top             =   1200
         Width           =   1995
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "29.97 fps"
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   11
         Top             =   900
         Width           =   1995
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "25 fps"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   10
         Top             =   600
         Value           =   -1  'True
         Width           =   1995
      End
   End
   Begin MSAdodcLib.Adodc adoSegment 
      Height          =   330
      Left            =   1740
      Top             =   1140
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSegment"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSegments 
      Bindings        =   "frmClipSegment.frx":0000
      Height          =   7575
      Left            =   1740
      TabIndex        =   8
      Top             =   1140
      Width           =   9495
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "eventID"
      Columns(0).Name =   "eventID"
      Columns(0).DataField=   "eventID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   6085
      Columns(1).Caption=   "Segment Reference"
      Columns(1).Name =   "segmentreference"
      Columns(1).DataField=   "segmentreference"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Start"
      Columns(2).Name =   "timecodestart"
      Columns(2).DataField=   "timecodestart"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "End"
      Columns(3).Name =   "timecodestop"
      Columns(3).DataField=   "timecodestop"
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "customfield1"
      Columns(4).Name =   "customfield1"
      Columns(4).DataField=   "customfield1"
      Columns(4).FieldLen=   256
      _ExtentX        =   16748
      _ExtentY        =   13361
      _StockProps     =   79
      Caption         =   "Segments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Timer timFormClose 
      Interval        =   20000
      Left            =   11520
      Top             =   4440
   End
   Begin VB.CommandButton cmdWatchFolder 
      Caption         =   "Select"
      Height          =   315
      Left            =   11640
      TabIndex        =   6
      Top             =   600
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtWatchFolder 
      Height          =   315
      Left            =   12540
      TabIndex        =   5
      Top             =   600
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdSubmit 
      Caption         =   "Submit"
      Height          =   315
      Left            =   12000
      TabIndex        =   4
      Top             =   4500
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   13380
      TabIndex        =   3
      Top             =   4500
      Width           =   1335
   End
   Begin VB.CommandButton cmdGetVideoFilename 
      Caption         =   "Select"
      Height          =   315
      Left            =   11640
      TabIndex        =   2
      Top             =   240
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtVideoFilename 
      Height          =   315
      Left            =   12540
      TabIndex        =   0
      Top             =   240
      Visible         =   0   'False
      Width           =   1995
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTranscodeSpec 
      Bindings        =   "frmClipSegment.frx":0019
      Height          =   315
      Left            =   1740
      TabIndex        =   17
      Top             =   600
      Width           =   6255
      DataFieldList   =   "transcodename"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Transcode Spec ID"
      Columns(0).Name =   "transcodespecID"
      Columns(0).DataField=   "transcodespecID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   7938
      Columns(1).Caption=   "Transcode Name"
      Columns(1).Name =   "transcodename"
      Columns(1).DataField=   "transcodename"
      Columns(1).FieldLen=   256
      _ExtentX        =   11033
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "transcodename"
   End
   Begin MSAdodcLib.Adodc adoTranscodeSpecs 
      Height          =   330
      Left            =   8100
      Top             =   600
      Visible         =   0   'False
      Width           =   2385
      _ExtentX        =   4207
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoTranscodeSpecs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblClipID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1740
      TabIndex        =   16
      Top             =   240
      Width           =   3075
   End
   Begin VB.Label lblCaption 
      Caption         =   "Trancode Spec"
      Height          =   255
      Index           =   2
      Left            =   300
      TabIndex        =   7
      Top             =   660
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source ClipID"
      Height          =   255
      Index           =   0
      Left            =   300
      TabIndex        =   1
      Top             =   300
      Width           =   1275
   End
End
Attribute VB_Name = "frmClipSegment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim m_strVideoFilename As String
Dim m_lngSrcClipID As Long

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdGetVideoFilename_Click()

'Use MDIform's common dialog to get the video filename

Dim l_strCurrentPath As String

If frmClipControl.lblLibraryID.Caption <> "" Then
    l_strCurrentPath = GetData("library", "subtitle", "libraryID", frmClipControl.lblLibraryID.Caption)
Else
    l_strCurrentPath = ""
End If

If frmClipControl.lblCompanyID.Caption <> "" Then
    If frmClipControl.txtAltFolder.Text <> "" Then
        l_strCurrentPath = l_strCurrentPath & "\" & frmClipControl.txtAltFolder.Text & "\"
    Else
        l_strCurrentPath = l_strCurrentPath & "\" & frmClipControl.lblCompanyID.Caption & "\"
    End If
Else
    l_strCurrentPath = l_strCurrentPath & "\"
End If

MDIForm1.dlgMain.InitDir = l_strCurrentPath
MDIForm1.dlgMain.Filter = "All Files |*.*"
If txtVideoFilename.Text <> "" Then
    MDIForm1.dlgMain.Filename = txtVideoFilename.Text
Else
    MDIForm1.dlgMain.Filename = l_strCurrentPath & "*.*"
End If

MDIForm1.dlgMain.ShowOpen

If Left(MDIForm1.dlgMain.Filename, 2) <> "\\" Then
    MsgBox "Please always use UNC network addressing for transcode jobs" & vbCrLf & "rather than drive letters", vbCritical, "Problem..."
    Exit Sub
End If

If Right(MDIForm1.dlgMain.Filename, 3) <> "*.*" Then
    txtVideoFilename.Text = MDIForm1.dlgMain.Filename
    m_strVideoFilename = MDIForm1.dlgMain.FileTitle
End If

End Sub

Private Sub cmdSubmit_Click()

Dim l_lngClipID As Long, l_strFPS As String, Framerate As Integer, SQL As String

If optTimecodeType(0).Value = True Then
    l_strFPS = "25"
    Framerate = TC_25
ElseIf optTimecodeType(1).Value = True Then
    l_strFPS = "29.97"
    Framerate = TC_29
ElseIf optTimecodeType(2).Value = True Then
    l_strFPS = "30"
    Framerate = TC_30
ElseIf optTimecodeType(3).Value = True Then
    l_strFPS = "24"
    Framerate = TC_24
End If

If cmbTranscodeSpec.Text <> "" And Not (chkVideoOnly.Value <> 0 And chkAudioOnly.Value <> 0) Then

    If adoSegment.Recordset.RecordCount > 0 Then
        adoSegment.Recordset.MoveFirst
        Do While Not adoSegment.Recordset.EOF
                    
            'Routines to submit Segment transcode requests.
            
            SQL = "INSERT INTO transcoderequest (sourceclipID, sourceaudio1clipID, transcodespecID, transcodesystem, videoonly, audioonly, leavehidden, overwriteifexisting, status, savedate, segmentneeded, segmentreference, fps, duration, timecodestart, timecodestop) VALUES ("
            SQL = SQL & lblClipID.Caption & ", "
            SQL = SQL & lblClipID.Caption & ", "
            SQL = SQL & cmbTranscodeSpec.Columns("transcodespecID").Text & ", "
            SQL = SQL & "'" & GetData("transcodespec", "transcodesystem", "transcodespecID", cmbTranscodeSpec.Columns("transcodespecID").Text) & "', "
            If chkAudioOnly.Value = True Then
                SQL = SQL & "0, 1, "
            ElseIf chkVideoOnly.Value = True Then
                SQL = SQL & "1, 0, "
            Else
                SQL = SQL & "0, 0, "
            End If
            If optHidden(0).Value = True Then
                SQL = SQL & "1, "
            Else
                SQL = SQL & "0, "
            End If
            
            If chkOverWrite.Value <> 0 Then
                SQL = SQL & "1, "
            Else
                SQL = SQL & "0, "
            End If
            
            SQL = SQL & GetData("transcodestatus", "transcodestatusID", "description", "Requested") & ", getdate(), "
            SQL = SQL & "1, '" & Trim(adoSegment.Recordset("segmentreference")) & "', "
            SQL = SQL & "'" & l_strFPS & "', "
            If g_blnFrameAccurateClipDurations = True Then
                SQL = SQL & "'" & Timecode_Subtract(adoSegment.Recordset("timecodestop"), adoSegment.Recordset("timecodestart"), Framerate) & "', "
            Else
                SQL = SQL & "'" & Duration_From_Timecode(Timecode_Subtract(adoSegment.Recordset("timecodestop"), adoSegment.Recordset("timecodestart"), Framerate), Framerate) & "', "
            End If
            SQL = SQL & "'" & adoSegment.Recordset("timecodestart") & "', "
            SQL = SQL & "'" & adoSegment.Recordset("timecodestop") & "' "
            SQL = SQL & ");"
            
            Debug.Print SQL
            ExecuteSQL SQL, g_strExecuteError
            CheckForSQLError
            
            'Record the clip usage
            SQL = "INSERT INTO eventusage ("
            SQL = SQL & "eventID, dateused) VALUES ("
            SQL = SQL & "'" & lblClipID.Caption & "', "
            SQL = SQL & "'" & FormatSQLDate(Now) & "'"
            SQL = SQL & ");"
            
            ExecuteSQL SQL, g_strExecuteError
            CheckForSQLError
            
            adoSegment.Recordset.MoveNext
        Loop
    End If

    Me.Hide

End If

End Sub

Private Sub cmdWatchFolder_Click()

'Use MDIform's common dialog to get the video filename
Dim l_strTemp As String, l_lngCount As Long

If m_strVideoFilename = "" Then
    MsgBox "Have to have at least a video source for a transcode", vbCritical, "Error..."
    Exit Sub
End If

MDIForm1.dlgMain.InitDir = g_strLocationOfWatchfolders & "\WatchFolders"
MDIForm1.dlgMain.Filter = "XML Files |*.xml*"
MDIForm1.dlgMain.Filename = g_strLocationOfWatchfolders & "\WatchFolders\" & frmClipControl.txtReference.Text & ".xml"

MDIForm1.dlgMain.ShowOpen

If Left(MDIForm1.dlgMain.Filename, 2) <> "\\" Then
    MsgBox "Please always use UNC network addressing for transcode jobs" & vbCrLf & "rather than drive letters.", vbCritical, "Problem..."
    Exit Sub
End If

l_strTemp = MDIForm1.dlgMain.Filename
l_lngCount = InStr(l_strTemp, MDIForm1.dlgMain.FileTitle)
If l_lngCount > 2 Then
    txtWatchfolder.Text = Left(l_strTemp, l_lngCount - 2)
Else
    txtWatchfolder.Text = ""
End If

End Sub

Private Sub Form_Load()

Me.Icon = frmClipControl.Icon

m_strVideoFilename = ""

m_lngSrcClipID = Val(frmClipControl.txtClipID.Text)
optHidden(0).Value = True

adoSegment.RecordSource = "SELECT * FROM eventsegment WHERE eventID = " & m_lngSrcClipID & " ORDER BY timecodestart;"
adoSegment.ConnectionString = g_strConnection
adoSegment.Refresh
adoSegment.Caption = adoSegment.Recordset.RecordCount & " Segments"

adoTranscodeSpecs.ConnectionString = g_strConnection
adoTranscodeSpecs.RecordSource = "SELECT transcodespecID, transcodename, transcodesystem, skiplogging, NexGuard, CASE WHEN companyID = 501 THEN 1 ELSE 2 END companyorder FROM transcodespec WHERE enabled <> 0 ORDER by forder, transcodename;"
adoTranscodeSpecs.Refresh

End Sub

Private Sub grdSegments_AfterUpdate(RtnDispErrMsg As Integer)
adoSegment.Caption = adoSegment.Recordset.RecordCount & " Segments"
End Sub

Private Sub grdSegments_BeforeUpdate(Cancel As Integer)

grdSegments.Columns("eventID").Text = m_lngSrcClipID
grdSegments.Columns("segmentreference").Text = Trim(grdSegments.Columns("segmentreference").Text)

If grdSegments.Columns("timecodestart").Text = "" Then
    If optTimecodeType(1).Value = True Then
        grdSegments.Columns("timecodestart").Text = "00:00:00;00"
    Else
        grdSegments.Columns("timecodestart").Text = "00:00:00:00"
    End If
End If

If grdSegments.Columns("timecodestop").Text = "" Then
    If optTimecodeType(1).Value = True Then
        grdSegments.Columns("timecodestop").Text = "00:00:00;00"
    Else
        grdSegments.Columns("timecodestop").Text = "00:00:00:00"
    End If
End If

End Sub

Private Sub grdSegments_KeyPress(KeyAscii As Integer)

If g_optUseFormattedTimeCodesInEvents = 1 Then
    If grdSegments.Columns(grdSegments.Col).Caption = "Start" Or grdSegments.Columns(grdSegments.Col).Caption = "End" Then
        If Len(grdSegments.ActiveCell.Text) < 2 Then
            If optTimecodeType(1).Value = True Then
                grdSegments.ActiveCell.Text = "00:00:00;00"
            Else
                grdSegments.ActiveCell.Text = "00:00:00:00"
            End If
            grdSegments.ActiveCell.SelStart = 0
            grdSegments.ActiveCell.SelLength = 1
        Else
            If optTimecodeType(0).Value = True Then
                Timecode_Check_Grid grdSegments, KeyAscii, TC_25
            ElseIf optTimecodeType(1).Value = True Then
                Timecode_Check_Grid grdSegments, KeyAscii, TC_29
            ElseIf optTimecodeType(1).Value = True Then
                Timecode_Check_Grid grdSegments, KeyAscii, TC_30
            Else
                Timecode_Check_Grid grdSegments, KeyAscii, TC_24
            End If
        End If
    End If
End If

End Sub

Private Sub timFormClose_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Unload Me

End Sub
