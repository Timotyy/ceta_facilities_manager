VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmOffAirs 
   Caption         =   "Off Air Schedule"
   ClientHeight    =   8730
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   16065
   Icon            =   "frmOffAirs.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8730
   ScaleWidth      =   16065
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdPrintReport 
      Caption         =   "Schedule Report"
      Height          =   315
      Left            =   8580
      TabIndex        =   2
      ToolTipText     =   "Close this form"
      Top             =   180
      Width           =   1575
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdOffAirs 
      Bindings        =   "frmOffAirs.frx":08CA
      Height          =   7935
      Left            =   180
      TabIndex        =   1
      Top             =   660
      Width           =   14145
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowHeight       =   423
      Columns.Count   =   9
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "offairscheduleID"
      Columns(0).Name =   "offairscheduleID"
      Columns(0).DataField=   "offairscheduleID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2831
      Columns(1).Caption=   "Order Number"
      Columns(1).Name =   "starordernumber"
      Columns(1).DataField=   "starordernumber"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3387
      Columns(2).Caption=   "TX Date & Time"
      Columns(2).Name =   "offairdate"
      Columns(2).DataField=   "offairdate"
      Columns(2).FieldLen=   256
      Columns(3).Width=   1614
      Columns(3).Caption=   "Channel"
      Columns(3).Name =   "offairchannel"
      Columns(3).DataField=   "offairchannel"
      Columns(3).FieldLen=   256
      Columns(4).Width=   9790
      Columns(4).Caption=   "Title"
      Columns(4).Name =   "progtitle"
      Columns(4).DataField=   "progtitle"
      Columns(4).FieldLen=   256
      Columns(5).Width=   1482
      Columns(5).Caption=   "Duration"
      Columns(5).Name =   "duration"
      Columns(5).DataField=   "duration"
      Columns(5).FieldLen=   256
      Columns(6).Width=   1455
      Columns(6).Caption=   "DVD PAL"
      Columns(6).Name =   "dvdpal"
      Columns(6).DataField=   "dvdpal"
      Columns(6).FieldLen=   256
      Columns(7).Width=   1693
      Columns(7).Caption=   "OLC FIle"
      Columns(7).Name =   "dvdntsc"
      Columns(7).DataField=   "dvdntsc"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1482
      Columns(8).Caption=   "DVD JCA"
      Columns(8).Name =   "dvdjca"
      Columns(8).DataField=   "dvdjca"
      Columns(8).FieldLen=   256
      _ExtentX        =   24950
      _ExtentY        =   13996
      _StockProps     =   79
      Caption         =   "Off Air Schedule"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   10260
      TabIndex        =   0
      ToolTipText     =   "Close this form"
      Top             =   180
      Width           =   1335
   End
   Begin MSAdodcLib.Adodc adoOffAirs 
      Height          =   330
      Left            =   180
      Top             =   360
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Off Airs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
End
Attribute VB_Name = "frmOffAirs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdPrintReport_Click()
PrintCrystalReport g_strLocationOfCrystalReportFiles & "OffAirs.rpt", "", True
End Sub

Private Sub Form_Load()

Dim l_strSQL As String

l_strSQL = "SELECT * FROM offairschedule ORDER BY offairdate, offairchannel;"
adoOffAirs.RecordSource = l_strSQL
adoOffAirs.ConnectionString = g_strConnection
adoOffAirs.Refresh

adoOffAirs.Caption = adoOffAirs.Recordset.RecordCount & " Items"

End Sub

Private Sub Form_Resize()

grdOffAirs.Height = Me.ScaleHeight - grdOffAirs.Top - 240

End Sub

Private Sub grdOffAirs_AfterUpdate(RtnDispErrMsg As Integer)

adoOffAirs.Caption = adoOffAirs.Recordset.RecordCount & " Items"

End Sub

