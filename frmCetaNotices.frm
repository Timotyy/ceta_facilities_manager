VERSION 5.00
Begin VB.Form frmCetaNotices 
   Caption         =   "Ceta Notices"
   ClientHeight    =   3840
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10440
   LinkTopic       =   "Form1"
   ScaleHeight     =   3840
   ScaleWidth      =   10440
   Begin VB.CommandButton cmdAcknowledge 
      Caption         =   "I have read and understood this notice"
      Height          =   735
      Left            =   8340
      TabIndex        =   1
      Top             =   480
      Width           =   1995
   End
   Begin VB.TextBox txtNotice 
      Height          =   3075
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   360
      Width           =   7815
   End
   Begin VB.Label lblcetanoticeID 
      Height          =   375
      Left            =   8700
      TabIndex        =   2
      Top             =   1440
      Visible         =   0   'False
      Width           =   1275
   End
End
Attribute VB_Name = "frmCetaNotices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdAcknowledge_Click()

Dim SQL As String

If lblcetanoticeID.Caption <> "" Then
    
    SQL = "INSERT INTO cetanoticeread (cetanoticeID, cetauserID, cetausername, dateread) VALUES ("
    SQL = SQL & lblcetanoticeID.Caption & ", " & g_lngUserID & ", "
    SQL = SQL & "'" & g_strFullUserName & "', "
    SQL = SQL & "'" & FormatSQLDate(Now) & "');"
    
    ExecuteSQL SQL, g_strExecuteError
    CheckForSQLError
    Me.Hide

End If

End Sub

Private Sub Form_Load()

CenterForm Me

End Sub
