VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmClipControl 
   Caption         =   "Media File Details"
   ClientHeight    =   13080
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   22935
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmClipcontrol.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   13080
   ScaleWidth      =   22935
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdEditSize 
      Caption         =   "Edit"
      Height          =   315
      Left            =   16260
      TabIndex        =   307
      Top             =   1860
      Width           =   435
   End
   Begin VB.TextBox txtOldReference 
      Height          =   315
      Left            =   11820
      TabIndex        =   232
      ToolTipText     =   "A reference for the clip (readers digest)"
      Top             =   11940
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.TextBox txtChromaSubsampling 
      Height          =   315
      Left            =   1740
      TabIndex        =   225
      ToolTipText     =   "The number of horizontal pixels"
      Top             =   5100
      Width           =   1575
   End
   Begin VB.TextBox txtColorSpace 
      Height          =   315
      Left            =   1740
      TabIndex        =   224
      ToolTipText     =   "The number of horizontal pixels"
      Top             =   4740
      Width           =   1575
   End
   Begin VB.TextBox txtJobDetailID 
      Height          =   315
      Left            =   1740
      TabIndex        =   220
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   2220
      Width           =   1575
   End
   Begin VB.TextBox txtSerialNumber 
      Height          =   315
      Left            =   7800
      TabIndex        =   218
      ToolTipText     =   "The source Clip ID"
      Top             =   1500
      Width           =   1455
   End
   Begin VB.TextBox txtAudioChannelCount 
      Height          =   315
      Left            =   1740
      TabIndex        =   209
      ToolTipText     =   "The clip bitrate"
      Top             =   10500
      Width           =   1575
   End
   Begin VB.TextBox txtSvenskProjectNumber 
      Height          =   315
      Left            =   1740
      TabIndex        =   204
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   2580
      Width           =   1575
   End
   Begin VB.PictureBox picKeyframe 
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2745
      Left            =   16740
      ScaleHeight     =   2685
      ScaleMode       =   0  'User
      ScaleWidth      =   4785
      TabIndex        =   169
      Top             =   0
      Width           =   4845
   End
   Begin VB.TextBox txtTrackCount 
      Height          =   315
      Left            =   1740
      TabIndex        =   155
      ToolTipText     =   "The clip bitrate"
      Top             =   10140
      Width           =   1575
   End
   Begin VB.TextBox txtSeriesID 
      Height          =   315
      Left            =   4680
      TabIndex        =   151
      ToolTipText     =   "The source Clip ID"
      Top             =   1500
      Width           =   1635
   End
   Begin TabDlg.SSTab tabExtras 
      Height          =   7335
      Left            =   10200
      TabIndex        =   77
      Top             =   4560
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   12938
      _Version        =   393216
      Tabs            =   10
      Tab             =   9
      TabsPerRow      =   5
      TabHeight       =   706
      OLEDropMode     =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Clip Keywords"
      TabPicture(0)   =   "frmClipcontrol.frx":0BC2
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "ddnKeyword"
      Tab(0).Control(1)=   "grdKeyword"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Add New Keywords"
      TabPicture(1)   =   "frmClipcontrol.frx":0BDE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdClientKeywords"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Media Window Users"
      TabPicture(2)   =   "frmClipcontrol.frx":0BFA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "ddnPortalUsers"
      Tab(2).Control(1)=   "grdPortalPermission"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Custom Fields"
      TabPicture(3)   =   "frmClipcontrol.frx":0C16
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "cmbField1"
      Tab(3).Control(1)=   "cmbField2"
      Tab(3).Control(2)=   "cmbField3"
      Tab(3).Control(3)=   "cmbField4"
      Tab(3).Control(4)=   "cmbField5"
      Tab(3).Control(5)=   "cmbField6"
      Tab(3).Control(6)=   "ddnCustomFieldLabel"
      Tab(3).Control(7)=   "grdCustomFieldDefs"
      Tab(3).Control(8)=   "lblField1"
      Tab(3).Control(9)=   "lblField2"
      Tab(3).Control(10)=   "lblField3"
      Tab(3).Control(11)=   "lblField4"
      Tab(3).Control(12)=   "lblField5"
      Tab(3).Control(13)=   "lblField6"
      Tab(3).ControlCount=   14
      TabCaption(4)   =   "Audio"
      TabPicture(4)   =   "frmClipcontrol.frx":0C32
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "cmdCopyJSONToClipboard"
      Tab(4).Control(1)=   "txtFFprobeNotes"
      Tab(4).Control(2)=   "grdFFProbe"
      Tab(4).Control(3)=   "cmbAudioLanguage8"
      Tab(4).Control(4)=   "cmbAudioContent8"
      Tab(4).Control(5)=   "cmbAudioType8"
      Tab(4).Control(6)=   "cmbAudioLanguage7"
      Tab(4).Control(7)=   "cmbAudioContent7"
      Tab(4).Control(8)=   "cmbAudioType7"
      Tab(4).Control(9)=   "cmbAudioType1"
      Tab(4).Control(10)=   "cmbAudioType2"
      Tab(4).Control(11)=   "cmbAudioType3"
      Tab(4).Control(12)=   "cmbAudioType4"
      Tab(4).Control(13)=   "cmbAudioType5"
      Tab(4).Control(14)=   "cmbAudioType6"
      Tab(4).Control(15)=   "cmbAudioContent1"
      Tab(4).Control(16)=   "cmbAudioLanguage1"
      Tab(4).Control(17)=   "cmbAudioContent2"
      Tab(4).Control(18)=   "cmbAudioContent3"
      Tab(4).Control(19)=   "cmbAudioContent4"
      Tab(4).Control(20)=   "cmbAudioContent5"
      Tab(4).Control(21)=   "cmbAudioContent6"
      Tab(4).Control(22)=   "cmbAudioLanguage2"
      Tab(4).Control(23)=   "cmbAudioLanguage3"
      Tab(4).Control(24)=   "cmbAudioLanguage4"
      Tab(4).Control(25)=   "cmbAudioLanguage5"
      Tab(4).Control(26)=   "cmbAudioLanguage6"
      Tab(4).Control(27)=   "Label5"
      Tab(4).Control(28)=   "lblCaption(71)"
      Tab(4).Control(29)=   "lblCaption(70)"
      Tab(4).Control(30)=   "lblCaption(100)"
      Tab(4).Control(31)=   "lblCaption(101)"
      Tab(4).Control(32)=   "lblCaption(102)"
      Tab(4).Control(33)=   "lblCaption(103)"
      Tab(4).Control(34)=   "lblCaption(104)"
      Tab(4).Control(35)=   "lblCaption(105)"
      Tab(4).ControlCount=   36
      TabCaption(5)   =   "History"
      TabPicture(5)   =   "frmClipcontrol.frx":0C4E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "grdEventHistory"
      Tab(5).Control(1)=   "grdChecksumHistory"
      Tab(5).ControlCount=   2
      TabCaption(6)   =   "Media Window History"
      TabPicture(6)   =   "frmClipcontrol.frx":0C6A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "grdMediaWindowHistory"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "Assigned Jobs"
      TabPicture(7)   =   "frmClipcontrol.frx":0C86
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "grdAssignedJobs"
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "Unused Tab"
      TabPicture(8)   =   "frmClipcontrol.frx":0CA2
      Tab(8).ControlEnabled=   0   'False
      Tab(8).ControlCount=   0
      TabCaption(9)   =   "File Actions"
      TabPicture(9)   =   "frmClipcontrol.frx":0CBE
      Tab(9).ControlEnabled=   -1  'True
      Tab(9).Control(0)=   "cmdESIDL3XMLFile"
      Tab(9).Control(0).Enabled=   0   'False
      Tab(9).Control(1)=   "cmdRename"
      Tab(9).Control(1).Enabled=   0   'False
      Tab(9).Control(2)=   "cmdGoogleEpisodeXML"
      Tab(9).Control(2).Enabled=   0   'False
      Tab(9).Control(3)=   "cmdGoogleSeasonXML"
      Tab(9).Control(3).Enabled=   0   'False
      Tab(9).Control(4)=   "cmdGoogleShowXML"
      Tab(9).Control(4).Enabled=   0   'False
      Tab(9).Control(5)=   "cmdDiscoveryOutput"
      Tab(9).Control(5).Enabled=   0   'False
      Tab(9).Control(6)=   "cmdMD5Request"
      Tab(9).Control(6).Enabled=   0   'False
      Tab(9).Control(7)=   "cmdSHA1Request"
      Tab(9).Control(7).Enabled=   0   'False
      Tab(9).Control(8)=   "cmdSendToSMartjog"
      Tab(9).Control(8).Enabled=   0   'False
      Tab(9).Control(9)=   "cmdMoveFile"
      Tab(9).Control(9).Enabled=   0   'False
      Tab(9).Control(10)=   "cmdCopyFile"
      Tab(9).Control(10).Enabled=   0   'False
      Tab(9).Control(11)=   "cmdDelete"
      Tab(9).Control(11).Enabled=   0   'False
      Tab(9).Control(12)=   "cmdRestoreFromWasabi"
      Tab(9).Control(12).Enabled=   0   'False
      Tab(9).Control(13)=   "cmdMoveToAutoDelete"
      Tab(9).Control(13).Enabled=   0   'False
      Tab(9).Control(14)=   "cmdMoveToPrivate"
      Tab(9).Control(14).Enabled=   0   'False
      Tab(9).Control(15)=   "cmdMoveToOps"
      Tab(9).Control(15).Enabled=   0   'False
      Tab(9).Control(16)=   "cmdMoveToVOD"
      Tab(9).Control(16).Enabled=   0   'False
      Tab(9).Control(17)=   "cmdMoveAndChangeOwner"
      Tab(9).Control(17).Enabled=   0   'False
      Tab(9).Control(18)=   "cmdCopyAndChangeOwner"
      Tab(9).Control(18).Enabled=   0   'False
      Tab(9).Control(19)=   "cmdMediaInfoRequest"
      Tab(9).Control(19).Enabled=   0   'False
      Tab(9).Control(20)=   "cmdFFProbeRequest"
      Tab(9).Control(20).Enabled=   0   'False
      Tab(9).Control(21)=   "cmdNexGuardQC"
      Tab(9).Control(21).Enabled=   0   'False
      Tab(9).Control(22)=   "cmdBatonQC"
      Tab(9).Control(22).Enabled=   0   'False
      Tab(9).Control(23)=   "cmdMoveToEdit"
      Tab(9).Control(23).Enabled=   0   'False
      Tab(9).Control(24)=   "cmdDADCOutputAudioOnly"
      Tab(9).Control(24).Enabled=   0   'False
      Tab(9).Control(25)=   "cmdDADCOutput"
      Tab(9).Control(25).Enabled=   0   'False
      Tab(9).Control(26)=   "cmdRestoreFromDIVA"
      Tab(9).Control(26).Enabled=   0   'False
      Tab(9).ControlCount=   27
      Begin VB.CommandButton cmdCopyJSONToClipboard 
         Caption         =   "Copy to Clipboard"
         Height          =   495
         Left            =   -70200
         TabIndex        =   311
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   6360
         Width           =   1275
      End
      Begin VB.TextBox txtFFprobeNotes 
         Height          =   795
         Left            =   -74940
         MultiLine       =   -1  'True
         TabIndex        =   310
         ToolTipText     =   "Notes for the clip"
         Top             =   6240
         Width           =   4575
      End
      Begin VB.CommandButton cmdRestoreFromDIVA 
         Caption         =   "Restore Fom DIVA"
         Enabled         =   0   'False
         Height          =   495
         Left            =   4500
         TabIndex        =   309
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3780
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdDADCOutput 
         Caption         =   "DADC BBC XML File"
         Height          =   495
         Left            =   180
         TabIndex        =   239
         ToolTipText     =   "Make a DADC BBC Video XML file"
         Top             =   2700
         Width           =   1395
      End
      Begin VB.CommandButton cmdDADCOutputAudioOnly 
         Caption         =   "DADC BBC Audio XML File"
         Height          =   495
         Left            =   1620
         TabIndex        =   240
         ToolTipText     =   "Make a DADC BBC Audio Only XML file"
         Top             =   2700
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveToEdit 
         Height          =   495
         Left            =   1620
         Picture         =   "frmClipcontrol.frx":0CDA
         Style           =   1  'Graphical
         TabIndex        =   291
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3240
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdBatonQC 
         Caption         =   "Make Baton QC Request"
         Height          =   495
         Left            =   4500
         TabIndex        =   303
         ToolTipText     =   "Request an MD5 Checksum be made"
         Top             =   2160
         Width           =   1395
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdFFProbe 
         Bindings        =   "frmClipcontrol.frx":12F9
         Height          =   1455
         Left            =   -74940
         TabIndex        =   302
         Top             =   4680
         Width           =   6135
         _Version        =   196617
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         RowHeight       =   423
         ExtraHeight     =   318
         Columns.Count   =   5
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "eventchannelmapID"
         Columns(0).Name =   "eventchannelmapID"
         Columns(0).DataField=   "eventchannelmapID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "eventID"
         Columns(1).Name =   "eventID"
         Columns(1).DataField=   "eventID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   7488
         Columns(2).Caption=   "JSONstring"
         Columns(2).Name =   "JSONstring"
         Columns(2).DataField=   "JSONstring"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "MovCeCommandString"
         Columns(3).Name =   "MovCeCommandString"
         Columns(3).DataField=   "MovCeCommandString"
         Columns(3).FieldLen=   256
         Columns(4).Width=   2117
         Columns(4).Caption=   "cdate"
         Columns(4).Name =   "cdate"
         Columns(4).DataField=   "cdate"
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         _ExtentX        =   10821
         _ExtentY        =   2566
         _StockProps     =   79
         Caption         =   "FF Probe Data"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.CommandButton cmdNexGuardQC 
         Caption         =   "Make NecGuard QC Request"
         Height          =   495
         Left            =   3060
         TabIndex        =   300
         ToolTipText     =   "Request an MD5 Checksum be made"
         Top             =   2700
         Width           =   1395
      End
      Begin VB.CommandButton cmdFFProbeRequest 
         Caption         =   "Make FFProbe Request"
         Height          =   495
         Left            =   1620
         TabIndex        =   299
         ToolTipText     =   "Request an MD5 Checksum be made"
         Top             =   2160
         Width           =   1395
      End
      Begin VB.CommandButton cmdMediaInfoRequest 
         Caption         =   "Make MediaInfo Request"
         Height          =   495
         Left            =   180
         TabIndex        =   298
         ToolTipText     =   "Request an MD5 Checksum be made"
         Top             =   2160
         Width           =   1395
      End
      Begin VB.CommandButton cmdCopyAndChangeOwner 
         Height          =   495
         Left            =   1620
         Picture         =   "frmClipcontrol.frx":1312
         Style           =   1  'Graphical
         TabIndex        =   295
         ToolTipText     =   "Copy the File"
         Top             =   1560
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveAndChangeOwner 
         Height          =   495
         Left            =   180
         Picture         =   "frmClipcontrol.frx":19AA
         Style           =   1  'Graphical
         TabIndex        =   294
         ToolTipText     =   "Move the File"
         Top             =   1560
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveToVOD 
         Height          =   495
         Left            =   180
         Picture         =   "frmClipcontrol.frx":204A
         Style           =   1  'Graphical
         TabIndex        =   293
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3240
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveToOps 
         Height          =   495
         Left            =   3060
         Picture         =   "frmClipcontrol.frx":26B8
         Style           =   1  'Graphical
         TabIndex        =   292
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3240
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveToPrivate 
         Height          =   495
         Left            =   4500
         Picture         =   "frmClipcontrol.frx":2CE3
         Style           =   1  'Graphical
         TabIndex        =   290
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3240
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveToAutoDelete 
         Caption         =   "Move To AutoDelete"
         Enabled         =   0   'False
         Height          =   495
         Left            =   4500
         TabIndex        =   252
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   2700
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdRestoreFromWasabi 
         Height          =   495
         Left            =   4500
         Picture         =   "frmClipcontrol.frx":3319
         Style           =   1  'Graphical
         TabIndex        =   251
         ToolTipText     =   "Restore the file from DIVA including a DIVA Transcode"
         Top             =   1560
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdDelete 
         Height          =   495
         Left            =   4500
         Picture         =   "frmClipcontrol.frx":39D8
         Style           =   1  'Graphical
         TabIndex        =   250
         ToolTipText     =   "Delete the File"
         Top             =   1020
         Width           =   1395
      End
      Begin VB.CommandButton cmdCopyFile 
         Height          =   495
         Left            =   1620
         Picture         =   "frmClipcontrol.frx":3F92
         Style           =   1  'Graphical
         TabIndex        =   249
         ToolTipText     =   "Copy the File"
         Top             =   1020
         Width           =   1395
      End
      Begin VB.CommandButton cmdMoveFile 
         Height          =   495
         Left            =   180
         Picture         =   "frmClipcontrol.frx":4541
         Style           =   1  'Graphical
         TabIndex        =   248
         ToolTipText     =   "Move the File"
         Top             =   1020
         Width           =   1395
      End
      Begin VB.CommandButton cmdSendToSMartjog 
         Caption         =   "Send to Smartjog Box"
         Height          =   495
         Left            =   180
         TabIndex        =   247
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3840
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdSHA1Request 
         Cancel          =   -1  'True
         Caption         =   "Make SHA1 Checksum"
         Height          =   495
         Left            =   4500
         TabIndex        =   246
         ToolTipText     =   "Request an SHA1 Checksum be made"
         Top             =   5340
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdMD5Request 
         Caption         =   "Make MD5 Checksum"
         Height          =   495
         Left            =   3060
         TabIndex        =   245
         ToolTipText     =   "Request an MD5 Checksum be made"
         Top             =   2160
         Width           =   1395
      End
      Begin VB.CommandButton cmdDiscoveryOutput 
         Caption         =   "Discovery On-Ramp XML"
         Height          =   495
         Left            =   180
         TabIndex        =   244
         ToolTipText     =   "Make a Discovery On-Ramp XML"
         Top             =   4380
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdGoogleShowXML 
         Caption         =   "Google Show XML"
         Height          =   495
         Left            =   3060
         TabIndex        =   243
         ToolTipText     =   "Make a Google Show XML file"
         Top             =   5340
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdGoogleSeasonXML 
         Caption         =   "Google Season XML"
         Height          =   495
         Left            =   1620
         TabIndex        =   242
         ToolTipText     =   "Make a Google Season XML file"
         Top             =   5340
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdGoogleEpisodeXML 
         Caption         =   "Google Episode XML"
         Height          =   495
         Left            =   180
         TabIndex        =   241
         ToolTipText     =   "Make a Google Episodic XML file"
         Top             =   5340
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CommandButton cmdRename 
         Height          =   495
         Left            =   3060
         Picture         =   "frmClipcontrol.frx":4AE7
         Style           =   1  'Graphical
         TabIndex        =   238
         ToolTipText     =   "Delete the File"
         Top             =   1020
         Width           =   1395
      End
      Begin VB.CommandButton cmdESIDL3XMLFile 
         Caption         =   "Make ESI DL3 XML File"
         Height          =   495
         Left            =   1620
         TabIndex        =   237
         ToolTipText     =   "Make a DADC SPE XML file"
         Top             =   4380
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.ComboBox cmbAudioLanguage8 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50B4
         Left            =   -70380
         List            =   "frmClipcontrol.frx":50B6
         TabIndex        =   200
         ToolTipText     =   "The Version for the Tape"
         Top             =   3600
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent8 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50B8
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50BA
         TabIndex        =   199
         ToolTipText     =   "The Version for the Tape"
         Top             =   3600
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType8 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50BC
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50BE
         TabIndex        =   198
         ToolTipText     =   "The Version for the Tape"
         Top             =   3600
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage7 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50C0
         Left            =   -70380
         List            =   "frmClipcontrol.frx":50C2
         TabIndex        =   197
         ToolTipText     =   "The Version for the Tape"
         Top             =   3240
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent7 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50C4
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50C6
         TabIndex        =   196
         ToolTipText     =   "The Version for the Tape"
         Top             =   3240
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType7 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50C8
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50CA
         TabIndex        =   195
         ToolTipText     =   "The Version for the Tape"
         Top             =   3240
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType1 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50CC
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50CE
         TabIndex        =   188
         ToolTipText     =   "The Version for the Tape"
         Top             =   1080
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType2 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50D0
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50D2
         TabIndex        =   187
         ToolTipText     =   "The Version for the Tape"
         Top             =   1440
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType3 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50D4
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50D6
         TabIndex        =   186
         ToolTipText     =   "The Version for the Tape"
         Top             =   1800
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType4 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50D8
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50DA
         TabIndex        =   185
         ToolTipText     =   "The Version for the Tape"
         Top             =   2160
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType5 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50DC
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50DE
         TabIndex        =   184
         ToolTipText     =   "The Version for the Tape"
         Top             =   2520
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioType6 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50E0
         Left            =   -73740
         List            =   "frmClipcontrol.frx":50E2
         TabIndex        =   183
         ToolTipText     =   "The Version for the Tape"
         Top             =   2880
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent1 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50E4
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50E6
         TabIndex        =   182
         ToolTipText     =   "The Version for the Tape"
         Top             =   1080
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage1 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50E8
         Left            =   -70380
         List            =   "frmClipcontrol.frx":50EA
         TabIndex        =   181
         ToolTipText     =   "The Version for the Tape"
         Top             =   1080
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent2 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50EC
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50EE
         TabIndex        =   180
         ToolTipText     =   "The Version for the Tape"
         Top             =   1440
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent3 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50F0
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50F2
         TabIndex        =   179
         ToolTipText     =   "The Version for the Tape"
         Top             =   1800
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent4 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50F4
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50F6
         TabIndex        =   178
         ToolTipText     =   "The Version for the Tape"
         Top             =   2160
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent5 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50F8
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50FA
         TabIndex        =   177
         ToolTipText     =   "The Version for the Tape"
         Top             =   2520
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioContent6 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":50FC
         Left            =   -72060
         List            =   "frmClipcontrol.frx":50FE
         TabIndex        =   176
         ToolTipText     =   "The Version for the Tape"
         Top             =   2880
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage2 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":5100
         Left            =   -70380
         List            =   "frmClipcontrol.frx":5102
         TabIndex        =   175
         ToolTipText     =   "The Version for the Tape"
         Top             =   1440
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage3 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":5104
         Left            =   -70380
         List            =   "frmClipcontrol.frx":5106
         TabIndex        =   174
         ToolTipText     =   "The Version for the Tape"
         Top             =   1800
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage4 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":5108
         Left            =   -70380
         List            =   "frmClipcontrol.frx":510A
         TabIndex        =   173
         ToolTipText     =   "The Version for the Tape"
         Top             =   2160
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage5 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":510C
         Left            =   -70380
         List            =   "frmClipcontrol.frx":510E
         TabIndex        =   172
         ToolTipText     =   "The Version for the Tape"
         Top             =   2520
         Width           =   1575
      End
      Begin VB.ComboBox cmbAudioLanguage6 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":5110
         Left            =   -70380
         List            =   "frmClipcontrol.frx":5112
         TabIndex        =   171
         ToolTipText     =   "The Version for the Tape"
         Top             =   2880
         Width           =   1575
      End
      Begin VB.ComboBox cmbField1 
         Height          =   315
         Left            =   -73260
         TabIndex        =   118
         Tag             =   "CLEAR"
         ToolTipText     =   "The Clip Codec"
         Top             =   900
         Width           =   4215
      End
      Begin VB.ComboBox cmbField2 
         Height          =   315
         Left            =   -73260
         TabIndex        =   117
         Tag             =   "CLEAR"
         ToolTipText     =   "The Clip Codec"
         Top             =   1260
         Width           =   4215
      End
      Begin VB.ComboBox cmbField3 
         Height          =   315
         Left            =   -73260
         TabIndex        =   116
         Tag             =   "CLEAR"
         ToolTipText     =   "The Clip Codec"
         Top             =   1620
         Width           =   4215
      End
      Begin VB.ComboBox cmbField4 
         Height          =   315
         Left            =   -73260
         TabIndex        =   115
         Tag             =   "CLEAR"
         ToolTipText     =   "The Clip Codec"
         Top             =   1980
         Width           =   4215
      End
      Begin VB.ComboBox cmbField5 
         Height          =   315
         Left            =   -73260
         TabIndex        =   114
         Tag             =   "CLEAR"
         ToolTipText     =   "The Clip Codec"
         Top             =   2340
         Width           =   4215
      End
      Begin VB.ComboBox cmbField6 
         Height          =   315
         Left            =   -73260
         TabIndex        =   113
         Tag             =   "CLEAR"
         ToolTipText     =   "The Clip Codec"
         Top             =   2700
         Width           =   4215
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCustomFieldLabel 
         Height          =   795
         Left            =   -74820
         TabIndex        =   97
         Tag             =   "CLEAR"
         Top             =   3840
         Width           =   4875
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         _Version        =   196617
         DataMode        =   1
         Rows            =   6
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   3200
         Columns(0).Caption=   "Column 0"
         Columns(0).Name =   "Column 0"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   8599
         _ExtentY        =   1402
         _StockProps     =   77
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnPortalUsers 
         Bindings        =   "frmClipcontrol.frx":5114
         Height          =   795
         Left            =   -74160
         TabIndex        =   80
         Tag             =   "CLEAR"
         Top             =   1680
         Width           =   4155
         DataFieldList   =   "fullname"
         ListAutoValidate=   0   'False
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "portaluserID"
         Columns(0).Name =   "portaluserID"
         Columns(0).DataField=   "portaluserID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   8811
         Columns(1).Caption=   "fullname"
         Columns(1).Name =   "fullname"
         Columns(1).DataField=   "fullname"
         Columns(1).FieldLen=   256
         _ExtentX        =   7329
         _ExtentY        =   1402
         _StockProps     =   77
         DataFieldToDisplay=   "fullname"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdClientKeywords 
         Bindings        =   "frmClipcontrol.frx":5131
         Height          =   5115
         Left            =   -74940
         TabIndex        =   78
         Top             =   900
         Width           =   6135
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "companyID"
         Columns(0).DataField=   "companyID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   9525
         Columns(1).Caption=   "keyword"
         Columns(1).Name =   "keywordtext"
         Columns(1).DataField=   "keywordtext"
         Columns(1).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   9022
         _StockProps     =   79
         Caption         =   "Client Keywords"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdPortalPermission 
         Bindings        =   "frmClipcontrol.frx":5151
         Height          =   5115
         Left            =   -74940
         TabIndex        =   79
         Top             =   900
         Width           =   6135
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   9
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "portalpermissionID"
         Columns(0).Name =   "portalpermissionID"
         Columns(0).DataField=   "portalpermissionID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "portaluserID"
         Columns(1).Name =   "portaluserID"
         Columns(1).DataField=   "portaluserID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "eventID"
         Columns(2).Name =   "eventID"
         Columns(2).DataField=   "eventID"
         Columns(2).FieldLen=   256
         Columns(3).Width=   9684
         Columns(3).Caption=   "Portal User Name"
         Columns(3).Name =   "fullname"
         Columns(3).DataField=   "fullname"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "clipreference"
         Columns(4).Name =   "clipreference"
         Columns(4).DataField=   "mediareference"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "dateassigned"
         Columns(5).Name =   "dateassigned"
         Columns(5).DataField=   "dateassigned"
         Columns(5).DataType=   7
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "assignedby"
         Columns(6).Name =   "assignedby"
         Columns(6).DataField=   "assignedby"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "permissionstart"
         Columns(7).Name =   "permissionstart"
         Columns(7).DataField=   "permissionstart"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "permissionend"
         Columns(8).Name =   "permissionend"
         Columns(8).DataField=   "permissionend"
         Columns(8).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   9022
         _StockProps     =   79
         Caption         =   "Portal User Access Permissions"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCustomFieldDefs 
         Bindings        =   "frmClipcontrol.frx":5173
         Height          =   1695
         Left            =   -74940
         TabIndex        =   95
         Top             =   3060
         Width           =   6135
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "customfieldxrefID"
         Columns(0).Name =   "customfieldxrefID"
         Columns(0).DataField=   "customfieldxrefID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "customfield"
         Columns(1).Name =   "customfield"
         Columns(1).DataField=   "customfield"
         Columns(1).FieldLen=   256
         Columns(2).Width=   6244
         Columns(2).Caption=   "textentry"
         Columns(2).Name =   "textentry"
         Columns(2).DataField=   "textentry"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "companyID"
         Columns(3).Name =   "companyID"
         Columns(3).DataField=   "companyID"
         Columns(3).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   2990
         _StockProps     =   79
         Caption         =   "Custom Fields"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnKeyword 
         Bindings        =   "frmClipcontrol.frx":5194
         Height          =   1275
         Left            =   -73980
         TabIndex        =   107
         Tag             =   "CLEAR"
         Top             =   1740
         Width           =   4875
         DataFieldList   =   "keywordtext"
         ListAutoValidate=   0   'False
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   7461
         Columns(0).Caption=   "Keyword"
         Columns(0).Name =   "keywordtext"
         Columns(0).DataField=   "keywordtext"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "KeywordID"
         Columns(1).Name =   "keywordID"
         Columns(1).DataField=   "keywordID"
         Columns(1).FieldLen=   256
         _ExtentX        =   8599
         _ExtentY        =   2249
         _StockProps     =   77
         DataFieldToDisplay=   "keywordtext"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdKeyword 
         Bindings        =   "frmClipcontrol.frx":51AD
         Height          =   5115
         Left            =   -74940
         TabIndex        =   106
         Top             =   900
         Width           =   6135
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   9525
         Columns(1).Caption=   "keyword"
         Columns(1).Name =   "keywordtext"
         Columns(1).DataField=   "keywordtext"
         Columns(1).FieldLen=   256
         Columns(1).Style=   3
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "clipreference"
         Columns(2).Name =   "clipreference"
         Columns(2).DataField=   "clipreference"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "companyID"
         Columns(3).Name =   "companyID"
         Columns(3).DataField=   "companyID"
         Columns(3).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   9022
         _StockProps     =   79
         Caption         =   "Clip Keywords"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdEventHistory 
         Bindings        =   "frmClipcontrol.frx":51C7
         Height          =   3975
         Left            =   -74940
         TabIndex        =   149
         Top             =   900
         Width           =   6135
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Date"
         Columns(1).Name =   "datesaved"
         Columns(1).DataField=   "datesaved"
         Columns(1).DataType=   7
         Columns(1).FieldLen=   256
         Columns(2).Width=   2937
         Columns(2).Caption=   "User"
         Columns(2).Name =   "username"
         Columns(2).DataField=   "username"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3545
         Columns(3).Caption=   "Description"
         Columns(3).Name =   "description"
         Columns(3).DataField=   "description"
         Columns(3).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   7011
         _StockProps     =   79
         Caption         =   "Clip History"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdChecksumHistory 
         Bindings        =   "frmClipcontrol.frx":51E0
         Height          =   1155
         Left            =   -74940
         TabIndex        =   234
         Top             =   4980
         Width           =   6135
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Date"
         Columns(1).Name =   "cdate"
         Columns(1).DataField=   "cdate"
         Columns(1).DataType=   7
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "md5"
         Columns(2).Name =   "md5checksum"
         Columns(2).DataField=   "md5checksum"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "sha1"
         Columns(3).Name =   "sha1checksum"
         Columns(3).DataField=   "sha1checksum"
         Columns(3).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   2037
         _StockProps     =   79
         Caption         =   "Checksum History"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMediaWindowHistory 
         Bindings        =   "frmClipcontrol.frx":5201
         Height          =   5115
         Left            =   -74880
         TabIndex        =   235
         Top             =   900
         Width           =   6135
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Caption=   "Date"
         Columns(0).Name =   "timewhen"
         Columns(0).DataField=   "timewhen"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Action"
         Columns(1).Name =   "accesstype"
         Columns(1).DataField=   "accesstype"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Name"
         Columns(2).Name =   "fullname"
         Columns(2).DataField=   "fullname"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "contactID"
         Columns(3).Name =   "contactID"
         Columns(3).DataField=   "contactID"
         Columns(3).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   9022
         _StockProps     =   79
         Caption         =   "Media Window History"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdAssignedJobs 
         Bindings        =   "frmClipcontrol.frx":5225
         Height          =   5235
         Left            =   -74940
         TabIndex        =   236
         Top             =   900
         Width           =   6135
         _Version        =   196617
         AllowUpdate     =   0   'False
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "requiredmediaID"
         Columns(0).Name =   "requiredmediaID"
         Columns(0).DataField=   "requiredmediaID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "jobID"
         Columns(1).Name =   "jobID"
         Columns(1).DataField=   "jobID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   3281
         Columns(2).Caption=   "Job Status"
         Columns(2).Name =   "fd_status"
         Columns(2).DataField=   "fd_status"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "completeddate"
         Columns(3).Name =   "completeddate"
         Columns(3).DataField=   "completeddate"
         Columns(3).FieldLen=   256
         _ExtentX        =   10821
         _ExtentY        =   9234
         _StockProps     =   79
         Caption         =   "Assigned as Master to Jobs"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label5 
         Caption         =   $"frmClipcontrol.frx":5243
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   -74880
         TabIndex        =   228
         Top             =   4020
         Width           =   6195
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 8"
         Height          =   255
         Index           =   71
         Left            =   -74880
         TabIndex        =   202
         Top             =   3660
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 7"
         Height          =   255
         Index           =   70
         Left            =   -74880
         TabIndex        =   201
         Top             =   3300
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 1"
         Height          =   255
         Index           =   100
         Left            =   -74880
         TabIndex        =   194
         Top             =   1140
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 2"
         Height          =   255
         Index           =   101
         Left            =   -74880
         TabIndex        =   193
         Top             =   1500
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 3"
         Height          =   255
         Index           =   102
         Left            =   -74880
         TabIndex        =   192
         Top             =   1860
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 4"
         Height          =   255
         Index           =   103
         Left            =   -74880
         TabIndex        =   191
         Top             =   2220
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 5"
         Height          =   255
         Index           =   104
         Left            =   -74880
         TabIndex        =   190
         Top             =   2580
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Audio Set 6"
         Height          =   255
         Index           =   105
         Left            =   -74880
         TabIndex        =   189
         Top             =   2940
         Width           =   1155
      End
      Begin VB.Label lblField1 
         Appearance      =   0  'Flat
         Caption         =   "Custom Fld1"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74880
         TabIndex        =   124
         Tag             =   "1"
         Top             =   960
         Width           =   1635
      End
      Begin VB.Label lblField2 
         Appearance      =   0  'Flat
         Caption         =   "Custom Fld2"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74880
         TabIndex        =   123
         Tag             =   "2"
         Top             =   1320
         Width           =   1635
      End
      Begin VB.Label lblField3 
         Appearance      =   0  'Flat
         Caption         =   "Custom Fld3"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74880
         TabIndex        =   122
         Tag             =   "3"
         Top             =   1680
         Width           =   1635
      End
      Begin VB.Label lblField4 
         Appearance      =   0  'Flat
         Caption         =   "Custom Fld4"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74880
         TabIndex        =   121
         Tag             =   "4"
         Top             =   2040
         Width           =   1575
      End
      Begin VB.Label lblField5 
         Appearance      =   0  'Flat
         Caption         =   "Custom Fld5"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74880
         TabIndex        =   120
         Tag             =   "5"
         Top             =   2400
         Width           =   1635
      End
      Begin VB.Label lblField6 
         Appearance      =   0  'Flat
         Caption         =   "Custom Fld6"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74880
         TabIndex        =   119
         Tag             =   "6"
         Top             =   2760
         Width           =   1575
      End
   End
   Begin TabDlg.SSTab tabTimecodes 
      Height          =   7335
      Left            =   3600
      TabIndex        =   126
      Top             =   4560
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   12938
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Main"
      TabPicture(0)   =   "frmClipcontrol.frx":5304
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCaption(19)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblCaption(20)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblCaption(21)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblTechReviewAvalable"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblCaption(1)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblCaption(92)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label3"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblCaption(43)"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "lblCaption(22)"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "lblCaption(59)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lblCaption(60)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtSHA1Checksum"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtMD5Checksum"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtTimecodestart"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "txtTimecodestop"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "txtDuration"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "cmdTechRev"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "chkHideFromWeb"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "chkfourbythreeflag"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "chkOnline"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "chkNotCheckFilenames"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "cmdPlay"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "txtTimecodeKeyframe"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "txtKeyframeFilename"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "cmdVerifyKeyframe"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "cmdMakeKeyframeLikeReference"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "cmdiTunes"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "cmdVisualQuery"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "cmdMediaInfoPreserveTimecode"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "cmdMediaInfo"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "cmdVerifyClip"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "cmdMakeKeyframeFromImage"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "txtEndCredits"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "txtEndCreditsElapsed"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "chkTextlessPresent"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "cmdffplay"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "cmdAssignToJobAsMaster"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "cmdOpenFolderInExplorer"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "chkFileLocked"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "chkIgnoreJobID"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).ControlCount=   40
      TabCaption(1)   =   "Logging"
      TabPicture(1)   =   "frmClipcontrol.frx":5320
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmbIsTextInPicture"
      Tab(1).Control(1)=   "cmbPictFormat"
      Tab(1).Control(2)=   "cmbForcedSubtitle"
      Tab(1).Control(3)=   "cmbActiveRatio"
      Tab(1).Control(4)=   "cmbTextInPictureLanguage"
      Tab(1).Control(5)=   "cmbBurnedInSubtitleLanguage"
      Tab(1).Control(6)=   "chkNoTimecodeShuffle"
      Tab(1).Control(7)=   "cmdSvenskItems"
      Tab(1).Control(8)=   "chkNoResorting"
      Tab(1).Control(9)=   "adoEventList2"
      Tab(1).Control(10)=   "ddnEvents2"
      Tab(1).Control(11)=   "adoEventsList"
      Tab(1).Control(12)=   "ddnEvents"
      Tab(1).Control(13)=   "adoLogging"
      Tab(1).Control(14)=   "grdLogging"
      Tab(1).Control(15)=   "lblCaption(58)"
      Tab(1).Control(16)=   "lblCaption(57)"
      Tab(1).Control(17)=   "lblCaption(56)"
      Tab(1).Control(18)=   "lblCaption(55)"
      Tab(1).Control(19)=   "lblCaption(54)"
      Tab(1).Control(20)=   "Label4"
      Tab(1).Control(21)=   "lblCaption(76)"
      Tab(1).Control(22)=   "Label2"
      Tab(1).ControlCount=   23
      TabCaption(2)   =   "Web Logs"
      TabPicture(2)   =   "frmClipcontrol.frx":533C
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label1"
      Tab(2).Control(1)=   "grdWebLogging"
      Tab(2).Control(2)=   "adoWebLogging"
      Tab(2).ControlCount=   3
      TabCaption(3)   =   "Pipeline"
      TabPicture(3)   =   "frmClipcontrol.frx":5358
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lblCaption(79)"
      Tab(3).Control(1)=   "lblCaption(113)"
      Tab(3).Control(2)=   "lblCaption(112)"
      Tab(3).Control(3)=   "lblCaption(111)"
      Tab(3).Control(4)=   "cmbPipelineUnit"
      Tab(3).Control(5)=   "cmbEncodeReplayDeck"
      Tab(3).Control(6)=   "cmbAudioTrackDepth"
      Tab(3).Control(7)=   "cmbAudioTracks"
      Tab(3).Control(8)=   "cmdMakePipelineSchedule"
      Tab(3).ControlCount=   9
      TabCaption(4)   =   "Comp"
      TabPicture(4)   =   "frmClipcontrol.frx":5374
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "grdCompilation"
      Tab(4).Control(1)=   "adoCompilation"
      Tab(4).ControlCount=   2
      Begin VB.CheckBox chkIgnoreJobID 
         Caption         =   "No Check JobID"
         ForeColor       =   &H00000080&
         Height          =   195
         Left            =   4800
         TabIndex        =   308
         Tag             =   "NOCLEAR"
         Top             =   2040
         Value           =   1  'Checked
         Width           =   1485
      End
      Begin VB.CheckBox chkFileLocked 
         Caption         =   "File Locked"
         Height          =   195
         Left            =   4800
         TabIndex        =   304
         Top             =   840
         Width           =   1125
      End
      Begin VB.CommandButton cmdOpenFolderInExplorer 
         Appearance      =   0  'Flat
         Caption         =   "Open in Explorer"
         Height          =   330
         Left            =   3180
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   301
         ToolTipText     =   "Attempt to Play this file"
         Top             =   1980
         UseMaskColor    =   -1  'True
         Width           =   1395
      End
      Begin VB.CommandButton cmdAssignToJobAsMaster 
         Appearance      =   0  'Flat
         Caption         =   "Assign to Job as Master"
         Height          =   330
         Left            =   4380
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   297
         ToolTipText     =   "OPen the iTunes form for this File"
         Top             =   6060
         UseMaskColor    =   -1  'True
         Width           =   1935
      End
      Begin VB.CommandButton cmdffplay 
         Appearance      =   0  'Flat
         Caption         =   "FFPlay Clip"
         Height          =   330
         Left            =   3180
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   296
         ToolTipText     =   "Attempt to Play this file"
         Top             =   1620
         UseMaskColor    =   -1  'True
         Visible         =   0   'False
         Width           =   1395
      End
      Begin VB.CheckBox chkTextlessPresent 
         Caption         =   "Textless Elements Present in file"
         Height          =   195
         Left            =   3360
         TabIndex        =   289
         Top             =   2400
         Width           =   2745
      End
      Begin VB.TextBox txtEndCreditsElapsed 
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   1920
         TabIndex        =   286
         ToolTipText     =   "End timecode"
         Top             =   1980
         Width           =   1095
      End
      Begin VB.TextBox txtEndCredits 
         Height          =   315
         Left            =   1920
         TabIndex        =   285
         ToolTipText     =   "End timecode"
         Top             =   1620
         Width           =   1095
      End
      Begin VB.CommandButton cmdMakePipelineSchedule 
         Caption         =   "Make Pipeline Schedule File"
         Height          =   315
         Left            =   -73320
         TabIndex        =   278
         Top             =   2100
         Width           =   2235
      End
      Begin VB.ComboBox cmbAudioTracks 
         Height          =   315
         Left            =   -73320
         TabIndex        =   277
         ToolTipText     =   "The Clip Codec"
         Top             =   960
         Width           =   2055
      End
      Begin VB.ComboBox cmbAudioTrackDepth 
         Height          =   315
         Left            =   -73320
         TabIndex        =   276
         ToolTipText     =   "The Clip Codec"
         Top             =   1320
         Width           =   2055
      End
      Begin VB.ComboBox cmbEncodeReplayDeck 
         Height          =   315
         Left            =   -73320
         TabIndex        =   275
         ToolTipText     =   "The Machine this Tape was recorded in"
         Top             =   1680
         Width           =   2055
      End
      Begin VB.ComboBox cmbIsTextInPicture 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   -70320
         TabIndex        =   265
         ToolTipText     =   "The Clip Codec"
         Top             =   4560
         Width           =   1575
      End
      Begin VB.ComboBox cmbPictFormat 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   -70320
         TabIndex        =   264
         ToolTipText     =   "The Clip Codec"
         Top             =   4920
         Width           =   1575
      End
      Begin VB.ComboBox cmbForcedSubtitle 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   -70320
         TabIndex        =   263
         ToolTipText     =   "The Clip Codec"
         Top             =   5640
         Width           =   1575
      End
      Begin VB.ComboBox cmbActiveRatio 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   -70320
         TabIndex        =   262
         ToolTipText     =   "The Clip Codec"
         Top             =   5280
         Width           =   1575
      End
      Begin VB.ComboBox cmbTextInPictureLanguage 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":5390
         Left            =   -70320
         List            =   "frmClipcontrol.frx":5392
         TabIndex        =   261
         ToolTipText     =   "The Version for the Tape"
         Top             =   6000
         Width           =   1575
      End
      Begin VB.ComboBox cmbBurnedInSubtitleLanguage 
         Height          =   315
         ItemData        =   "frmClipcontrol.frx":5394
         Left            =   -70320
         List            =   "frmClipcontrol.frx":5396
         TabIndex        =   260
         ToolTipText     =   "The Version for the Tape"
         Top             =   6360
         Width           =   1575
      End
      Begin VB.CheckBox chkNoTimecodeShuffle 
         Caption         =   "No Timecode Shuffling Outs to Ins"
         Height          =   315
         Left            =   -74940
         TabIndex        =   259
         Top             =   5220
         Width           =   1995
      End
      Begin VB.CommandButton cmdSvenskItems 
         Appearance      =   0  'Flat
         Caption         =   "Add Svensk Standard Items"
         Height          =   330
         Left            =   -74940
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   258
         ToolTipText     =   "Play associated clips"
         Top             =   5700
         UseMaskColor    =   -1  'True
         Width           =   2235
      End
      Begin VB.CheckBox chkNoResorting 
         Caption         =   "No Refreshing this grid after a new line is added"
         Height          =   555
         Left            =   -74940
         TabIndex        =   257
         Top             =   4500
         Width           =   2295
      End
      Begin VB.CommandButton cmdMakeKeyframeFromImage 
         Caption         =   "Keyframe From Image"
         Height          =   330
         Left            =   1380
         TabIndex        =   215
         ToolTipText     =   "Make a Keyframe (if this file is an Image FIle)"
         Top             =   4620
         Width           =   2355
      End
      Begin VB.CommandButton cmdVerifyClip 
         Appearance      =   0  'Flat
         Caption         =   "Verify File"
         Height          =   330
         Left            =   180
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   214
         ToolTipText     =   "Verify this File Record (checks file actually exists)"
         Top             =   6060
         UseMaskColor    =   -1  'True
         Width           =   1935
      End
      Begin VB.CommandButton cmdMediaInfo 
         Appearance      =   0  'Flat
         Caption         =   "MediaInfo With Timecode"
         Height          =   330
         Left            =   4380
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   213
         ToolTipText     =   "MediaInfo into this form (including Timecodes)"
         Top             =   6480
         UseMaskColor    =   -1  'True
         Width           =   1935
      End
      Begin VB.CommandButton cmdMediaInfoPreserveTimecode 
         Appearance      =   0  'Flat
         Caption         =   "MediaInfo No Timecode"
         Height          =   330
         Left            =   2280
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   212
         ToolTipText     =   "MediaInfo into this form (no Timecodes)"
         Top             =   6480
         UseMaskColor    =   -1  'True
         Width           =   1935
      End
      Begin VB.CommandButton cmdVisualQuery 
         Caption         =   "Test MediaInfo Query"
         Height          =   330
         Left            =   180
         TabIndex        =   211
         ToolTipText     =   "MediaInfo into a vewing form"
         Top             =   6480
         Width           =   1935
      End
      Begin VB.CommandButton cmdiTunes 
         Appearance      =   0  'Flat
         Caption         =   "iTunes Data"
         Height          =   330
         Left            =   2280
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   208
         ToolTipText     =   "OPen the iTunes form for this File"
         Top             =   6060
         UseMaskColor    =   -1  'True
         Width           =   1935
      End
      Begin VB.CommandButton cmdMakeKeyframeLikeReference 
         Caption         =   "Keyframe from Reference"
         Height          =   330
         Left            =   3780
         TabIndex        =   170
         ToolTipText     =   "Make a Keyframe record from the reference of this file"
         Top             =   4620
         Width           =   2415
      End
      Begin VB.CommandButton cmdVerifyKeyframe 
         Appearance      =   0  'Flat
         Caption         =   "Verify"
         Height          =   330
         Left            =   4500
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   165
         ToolTipText     =   "Verify the Keyframe and show it"
         Top             =   5040
         UseMaskColor    =   -1  'True
         Width           =   615
      End
      Begin VB.TextBox txtKeyframeFilename 
         Height          =   315
         Left            =   1320
         TabIndex        =   164
         ToolTipText     =   "The clip filename"
         Top             =   5040
         Width           =   3135
      End
      Begin VB.TextBox txtTimecodeKeyframe 
         Height          =   315
         Left            =   1320
         TabIndex        =   163
         ToolTipText     =   "Start timecode"
         Top             =   5400
         Width           =   1935
      End
      Begin VB.CommandButton cmdPlay 
         Appearance      =   0  'Flat
         Caption         =   "Play Clip"
         Height          =   330
         Left            =   3180
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   145
         ToolTipText     =   "Attempt to Play this file"
         Top             =   1260
         UseMaskColor    =   -1  'True
         Width           =   1395
      End
      Begin VB.CheckBox chkNotCheckFilenames 
         Caption         =   "No Verify"
         ForeColor       =   &H00000080&
         Height          =   195
         Left            =   4800
         TabIndex        =   144
         Top             =   1740
         Width           =   1365
      End
      Begin VB.CheckBox chkOnline 
         Caption         =   "Online?"
         Enabled         =   0   'False
         Height          =   195
         Left            =   4800
         TabIndex        =   143
         Top             =   540
         Width           =   1125
      End
      Begin VB.CheckBox chkfourbythreeflag 
         Caption         =   "4 by 3 flag"
         Height          =   195
         Left            =   4800
         TabIndex        =   142
         Top             =   1140
         Width           =   1125
      End
      Begin VB.CheckBox chkHideFromWeb 
         Caption         =   "Hide from Web"
         Height          =   195
         Left            =   4800
         TabIndex        =   141
         Top             =   1440
         Width           =   1365
      End
      Begin VB.CommandButton cmdTechRev 
         Appearance      =   0  'Flat
         Caption         =   "Tech Review"
         Height          =   330
         Left            =   3180
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   140
         ToolTipText     =   "Open the Tech Review Form for this file"
         Top             =   900
         UseMaskColor    =   -1  'True
         Width           =   1395
      End
      Begin VB.TextBox txtDuration 
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   1920
         TabIndex        =   129
         ToolTipText     =   "The duration"
         Top             =   1260
         Width           =   1095
      End
      Begin VB.TextBox txtTimecodestop 
         Height          =   315
         Left            =   1920
         TabIndex        =   128
         ToolTipText     =   "End timecode"
         Top             =   900
         Width           =   1095
      End
      Begin VB.TextBox txtTimecodestart 
         Height          =   315
         Left            =   1920
         TabIndex        =   127
         ToolTipText     =   "Start timecode"
         Top             =   540
         Width           =   1095
      End
      Begin VB.TextBox txtMD5Checksum 
         Height          =   315
         Left            =   1920
         TabIndex        =   159
         ToolTipText     =   "Alternative file location if not in client number folder"
         Top             =   3060
         Width           =   4275
      End
      Begin VB.TextBox txtSHA1Checksum 
         Height          =   315
         Left            =   1920
         TabIndex        =   160
         ToolTipText     =   "Alternative file location if not in client number folder"
         Top             =   3420
         Width           =   4275
      End
      Begin MSAdodcLib.Adodc adoEventList2 
         Height          =   330
         Left            =   -72300
         Top             =   1560
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   8
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoEventList2"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnEvents2 
         Bindings        =   "frmClipcontrol.frx":5398
         Height          =   1275
         Left            =   -72720
         TabIndex        =   253
         Top             =   1320
         Width           =   3735
         DataFieldList   =   "description"
         ListAutoValidate=   0   'False
         ListWidthAutoSize=   0   'False
         MaxDropDownItems=   16
         ListWidth       =   6720
         _Version        =   196617
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   6165
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   2249
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin MSAdodcLib.Adodc adoEventsList 
         Height          =   330
         Left            =   -73800
         Top             =   1140
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   8
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoEventList"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnEvents 
         Bindings        =   "frmClipcontrol.frx":53B4
         Height          =   1335
         Left            =   -74160
         TabIndex        =   254
         Top             =   960
         Width           =   3735
         DataFieldList   =   "description"
         ListAutoValidate=   0   'False
         ListWidthAutoSize=   0   'False
         MaxDropDownItems=   16
         ListWidth       =   6720
         _Version        =   196617
         BackColorOdd    =   16761024
         RowHeight       =   423
         ExtraHeight     =   26
         Columns(0).Width=   6165
         Columns(0).Caption=   "Description"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   2355
         _StockProps     =   77
         DataFieldToDisplay=   "description"
      End
      Begin MSAdodcLib.Adodc adoLogging 
         Height          =   330
         Left            =   -74880
         Top             =   840
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoLogging"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdLogging 
         Bindings        =   "frmClipcontrol.frx":53D0
         Height          =   3555
         Left            =   -74880
         TabIndex        =   255
         Top             =   780
         Width           =   6255
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   6
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "eventID"
         Columns(0).Name =   "eventID"
         Columns(0).DataField=   "eventID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "Logging Item"
         Columns(1).Name =   "segmentreference"
         Columns(1).DataField=   "segmentreference"
         Columns(1).FieldLen=   256
         Columns(2).Width=   2302
         Columns(2).Caption=   "Start"
         Columns(2).Name =   "timecodestart"
         Columns(2).DataField=   "timecodestart"
         Columns(2).FieldLen=   256
         Columns(3).Width=   2302
         Columns(3).Caption=   "End"
         Columns(3).Name =   "timecodestop"
         Columns(3).DataField=   "timecodestop"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "Additional Info"
         Columns(4).Name =   "note"
         Columns(4).DataField=   "note"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "eventloggingID"
         Columns(5).Name =   "eventloggingID"
         Columns(5).DataField=   "eventloggingID"
         Columns(5).FieldLen=   256
         _ExtentX        =   11033
         _ExtentY        =   6271
         _StockProps     =   79
         Caption         =   "Logging Items"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSAdodcLib.Adodc adoWebLogging 
         Height          =   330
         Left            =   -74880
         Top             =   780
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoWebLogging"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbPipelineUnit 
         Height          =   315
         Left            =   -73320
         TabIndex        =   279
         ToolTipText     =   "The company this tape belongs to"
         Top             =   600
         Width           =   2715
         DataFieldList   =   "description"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2805
         Columns(0).Caption=   "Pipeline Unit"
         Columns(0).Name =   "description"
         Columns(0).DataField=   "description"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "IP Address"
         Columns(1).Name =   "information"
         Columns(1).DataField=   "information"
         Columns(1).FieldLen=   256
         _ExtentX        =   4789
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   12632319
         DataFieldToDisplay=   "description"
      End
      Begin MSAdodcLib.Adodc adoCompilation 
         Height          =   330
         Left            =   -74880
         Top             =   420
         Visible         =   0   'False
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   582
         ConnectMode     =   0
         CursorLocation  =   3
         IsolationLevel  =   -1
         ConnectionTimeout=   15
         CommandTimeout  =   30
         CursorType      =   3
         LockType        =   3
         CommandType     =   8
         CursorOptions   =   0
         CacheSize       =   50
         MaxRecords      =   0
         BOFAction       =   0
         EOFAction       =   0
         ConnectStringType=   1
         Appearance      =   1
         BackColor       =   8421631
         ForeColor       =   -2147483640
         Orientation     =   0
         Enabled         =   -1
         Connect         =   ""
         OLEDBString     =   ""
         OLEDBFile       =   ""
         DataSourceName  =   ""
         OtherAttributes =   ""
         UserName        =   ""
         Password        =   ""
         RecordSource    =   ""
         Caption         =   "adoCompilation"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _Version        =   393216
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdCompilation 
         Bindings        =   "frmClipcontrol.frx":53E9
         Height          =   6495
         Left            =   -74880
         TabIndex        =   284
         Top             =   720
         Width           =   6255
         _Version        =   196617
         AllowAddNew     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   6
         Columns(0).Width=   1667
         Columns(0).Caption=   "Clip ID"
         Columns(0).Name =   "sourceclipID"
         Columns(0).DataField=   "sourceclipID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Title"
         Columns(1).Name =   "eventtitle"
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   714
         Columns(2).Caption=   "Ep"
         Columns(2).Name =   "eventepisode"
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   3281
         Columns(3).Caption=   "Sub Title"
         Columns(3).Name =   "eventsubtitle"
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   900
         Columns(4).Caption=   "Order"
         Columns(4).Name =   "forder"
         Columns(4).DataField=   "forder"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "eventID"
         Columns(5).Name =   "eventID"
         Columns(5).DataField=   "eventID"
         Columns(5).FieldLen=   256
         _ExtentX        =   11033
         _ExtentY        =   11456
         _StockProps     =   79
         Caption         =   "Compilation Items"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdWebLogging 
         Bindings        =   "frmClipcontrol.frx":5406
         Height          =   6375
         Left            =   -74880
         TabIndex        =   273
         Top             =   780
         Width           =   6255
         _Version        =   196617
         AllowAddNew     =   -1  'True
         AllowDelete     =   -1  'True
         SelectTypeRow   =   3
         RowHeight       =   423
         ExtraHeight     =   53
         Columns.Count   =   13
         Columns(0).Width=   5239
         Columns(0).Caption=   "Logging Item"
         Columns(0).Name =   "segmentreference"
         Columns(0).DataField=   "segmentreference"
         Columns(0).FieldLen=   256
         Columns(1).Width=   2302
         Columns(1).Caption=   "Start"
         Columns(1).Name =   "timecodestart"
         Columns(1).DataField=   "timecodestart"
         Columns(1).FieldLen=   256
         Columns(2).Width=   2302
         Columns(2).Caption=   "End"
         Columns(2).Name =   "timecodestop"
         Columns(2).DataField=   "timecodestop"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "companyID"
         Columns(3).Name =   "companyID"
         Columns(3).DataField=   "companyID"
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "clipreference"
         Columns(4).Name =   "clipreference"
         Columns(4).DataField=   "clipreference"
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "framerate"
         Columns(5).Name =   "framerate"
         Columns(5).DataField=   "framerate"
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "Frame_Rate"
         Columns(6).Name =   "Frame_Rate"
         Columns(6).DataField=   "Frame_Rate"
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "TimeCode_In"
         Columns(7).Name =   "TimeCode_In"
         Columns(7).DataField=   "TimeCode_In"
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "TimeCode_Out"
         Columns(8).Name =   "TimeCode_Out"
         Columns(8).DataField=   "TimeCode_Out"
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "Seconds_In"
         Columns(9).Name =   "Seconds_In"
         Columns(9).DataField=   "Seconds_In"
         Columns(9).FieldLen=   256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "Seconds_Out"
         Columns(10).Name=   "Seconds_Out"
         Columns(10).DataField=   "Seconds_Out"
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "Clip_Label"
         Columns(11).Name=   "Clip_Label"
         Columns(11).DataField=   "Clip_Label"
         Columns(11).FieldLen=   256
         Columns(12).Width=   3200
         Columns(12).Visible=   0   'False
         Columns(12).Caption=   "Clip_Item"
         Columns(12).Name=   "Clip_Item"
         Columns(12).DataField=   "Clip_Item"
         Columns(12).FieldLen=   256
         _ExtentX        =   11033
         _ExtentY        =   11245
         _StockProps     =   79
         Caption         =   "Logging Items"
         BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblCaption 
         Caption         =   "End Credit Elapsed Time"
         Height          =   255
         Index           =   60
         Left            =   120
         TabIndex        =   288
         Top             =   2040
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "End Credit Start Time"
         Height          =   255
         Index           =   59
         Left            =   120
         TabIndex        =   287
         Top             =   1680
         Width           =   1635
      End
      Begin VB.Label lblCaption 
         Caption         =   "Pipeline Unit"
         Height          =   255
         Index           =   111
         Left            =   -74820
         TabIndex        =   283
         Top             =   660
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "No of Audio Tracks"
         Height          =   255
         Index           =   112
         Left            =   -74820
         TabIndex        =   282
         Top             =   1020
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Track Depth (Bits)"
         Height          =   255
         Index           =   113
         Left            =   -74820
         TabIndex        =   281
         Top             =   1380
         Width           =   1695
      End
      Begin VB.Label lblCaption 
         Caption         =   "Playback Deck"
         Height          =   255
         Index           =   79
         Left            =   -74820
         TabIndex        =   280
         Top             =   1740
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "All logging ends must be Exclusive Outs i.e. Normal VT Outs"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   -74820
         TabIndex        =   274
         Top             =   480
         Width           =   5835
      End
      Begin VB.Label lblCaption 
         Caption         =   "Text In Picture Language?"
         Height          =   255
         Index           =   58
         Left            =   -72540
         TabIndex        =   272
         Top             =   6060
         Width           =   2175
      End
      Begin VB.Label lblCaption 
         Caption         =   "DADC Forced Subtitle?"
         Height          =   255
         Index           =   57
         Left            =   -72540
         TabIndex        =   271
         Top             =   5700
         Width           =   2175
      End
      Begin VB.Label lblCaption 
         Caption         =   "DADC Active Picture?"
         Height          =   255
         Index           =   56
         Left            =   -72540
         TabIndex        =   270
         Top             =   5340
         Width           =   2175
      End
      Begin VB.Label lblCaption 
         Caption         =   "DADC Pict Format?"
         Height          =   255
         Index           =   55
         Left            =   -72540
         TabIndex        =   269
         Top             =   4980
         Width           =   2175
      End
      Begin VB.Label lblCaption 
         Caption         =   "Text In Picture?"
         Height          =   255
         Index           =   54
         Left            =   -72540
         TabIndex        =   268
         Top             =   4620
         Width           =   2175
      End
      Begin VB.Label Label4 
         Caption         =   "Don't free type in the language field. Use the Drop-down"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   -73440
         TabIndex        =   267
         Top             =   6720
         Width           =   4815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Burned In Subtitle Language?"
         Height          =   255
         Index           =   76
         Left            =   -72540
         TabIndex        =   266
         Top             =   6420
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "All DADC ends must be Exclusive Outs i.e. Normal VT Outs"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   -74820
         TabIndex        =   256
         Top             =   480
         Width           =   5835
      End
      Begin VB.Label lblCaption 
         Caption         =   "Keyframe File"
         Height          =   255
         Index           =   22
         Left            =   120
         TabIndex        =   168
         Top             =   5100
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Keyframe Time"
         Height          =   255
         Index           =   43
         Left            =   120
         TabIndex        =   167
         Top             =   5460
         Width           =   1155
      End
      Begin VB.Label Label3 
         Caption         =   "Use Clip timecodes, not zero ascending for Keyframe time"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   435
         Left            =   3360
         TabIndex        =   166
         Top             =   5400
         Width           =   2595
      End
      Begin VB.Label lblCaption 
         Caption         =   "SHA-1 Checksum"
         Height          =   255
         Index           =   92
         Left            =   120
         TabIndex        =   158
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label lblCaption 
         Caption         =   "MD5 Checksum"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   157
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label lblTechReviewAvalable 
         Alignment       =   2  'Center
         Caption         =   "Tech Review Exists"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   375
         Left            =   3240
         TabIndex        =   146
         Top             =   480
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Duration"
         Height          =   255
         Index           =   21
         Left            =   120
         TabIndex        =   132
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "End Time"
         Height          =   255
         Index           =   20
         Left            =   120
         TabIndex        =   131
         Top             =   960
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Start Time"
         Height          =   255
         Index           =   19
         Left            =   120
         TabIndex        =   130
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.ComboBox cmbStreamType 
      Height          =   315
      Left            =   1740
      TabIndex        =   108
      ToolTipText     =   "The Clip Codec"
      Top             =   5460
      Width           =   1575
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbBarcode 
      Height          =   315
      Left            =   1740
      TabIndex        =   105
      Top             =   420
      Width           =   1575
      DataFieldList   =   "Column 0"
      MaxDropDownItems=   56
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      GroupHeaders    =   0   'False
      HeadLines       =   0
      BackColorOdd    =   12632319
      RowHeight       =   423
      Columns(0).Width=   4233
      Columns(0).Caption=   "barcode"
      Columns(0).Name =   "barcode"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   2778
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "Column 0"
   End
   Begin VB.ComboBox cmbInterlace 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   92
      ToolTipText     =   "The Clip Codec"
      Top             =   7620
      Width           =   1575
   End
   Begin VB.ComboBox cmbCbrVbr 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   91
      ToolTipText     =   "The Clip Codec"
      Top             =   6180
      Width           =   1575
   End
   Begin VB.ComboBox cmbPasses 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   89
      ToolTipText     =   "The Clip Codec"
      Top             =   5820
      Width           =   1575
   End
   Begin VB.TextBox txtAudioBitrate 
      Height          =   315
      Left            =   1740
      TabIndex        =   87
      ToolTipText     =   "The clip bitrate"
      Top             =   9420
      Width           =   1575
   End
   Begin VB.TextBox txtVideoBitrate 
      Height          =   315
      Left            =   1740
      TabIndex        =   84
      ToolTipText     =   "The clip bitrate"
      Top             =   8700
      Width           =   1575
   End
   Begin VB.ComboBox cmbAudioCodec 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   83
      ToolTipText     =   "The Clip Codec"
      Top             =   9060
      Width           =   1575
   End
   Begin MSAdodcLib.Adodc adoPortalUsers 
      Height          =   330
      Left            =   60
      Top             =   13200
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPortalUsers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoPortalPermission 
      Height          =   330
      Left            =   60
      Top             =   12900
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoPortalPermission"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.CommandButton cmdNewReference 
      Caption         =   "!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3360
      TabIndex        =   76
      ToolTipText     =   "Clear the form"
      Top             =   1560
      Width           =   255
   End
   Begin VB.TextBox txtInternalReference 
      Height          =   315
      Left            =   1740
      TabIndex        =   74
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1500
      Width           =   1575
   End
   Begin VB.TextBox txtInternalNotes 
      Height          =   435
      Left            =   4680
      MultiLine       =   -1  'True
      TabIndex        =   72
      ToolTipText     =   "Notes for the clip"
      Top             =   4020
      Width           =   4575
   End
   Begin VB.ComboBox cmbClipType 
      Height          =   315
      Left            =   1740
      TabIndex        =   70
      ToolTipText     =   "The Version for the Tape"
      Top             =   3300
      Width           =   1575
   End
   Begin VB.ComboBox cmbGenre 
      Height          =   315
      ItemData        =   "frmClipcontrol.frx":5422
      Left            =   1740
      List            =   "frmClipcontrol.frx":5424
      TabIndex        =   68
      ToolTipText     =   "The Version for the Tape"
      Top             =   2940
      Width           =   1575
   End
   Begin VB.ComboBox cmbFrameRate 
      Height          =   315
      Left            =   1740
      TabIndex        =   65
      ToolTipText     =   "The Clip Codec"
      Top             =   7260
      Width           =   1575
   End
   Begin VB.ComboBox cmbPurpose 
      Height          =   315
      Left            =   1740
      TabIndex        =   61
      ToolTipText     =   "The Clip Codec"
      Top             =   3660
      Width           =   1575
   End
   Begin VB.ComboBox cmbGeometry 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   54
      ToolTipText     =   "The Clip Codec"
      Top             =   8340
      Width           =   1575
   End
   Begin VB.ComboBox cmbAspect 
      Height          =   315
      Left            =   1740
      TabIndex        =   53
      ToolTipText     =   "The Clip Codec"
      Top             =   7980
      Width           =   1575
   End
   Begin VB.TextBox txtClockNumber 
      Height          =   315
      Left            =   4680
      TabIndex        =   51
      ToolTipText     =   "A reference for the clip (readers digest)"
      Top             =   2220
      Width           =   4575
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   4680
      TabIndex        =   47
      ToolTipText     =   "A reference for the clip (readers digest)"
      Top             =   2940
      Width           =   10575
   End
   Begin VB.TextBox txtJobID 
      Height          =   315
      Left            =   1740
      TabIndex        =   45
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1860
      Width           =   1575
   End
   Begin VB.TextBox txtAltFolder 
      Height          =   315
      Left            =   4680
      TabIndex        =   43
      ToolTipText     =   "Alternative file location if not in client number folder"
      Top             =   3660
      Width           =   10575
   End
   Begin VB.TextBox txtClipfilename 
      Height          =   315
      Left            =   4680
      TabIndex        =   33
      ToolTipText     =   "The clip filename"
      Top             =   3300
      Width           =   10575
   End
   Begin VB.TextBox txtBitrate 
      Height          =   315
      Left            =   1740
      TabIndex        =   31
      ToolTipText     =   "The clip bitrate"
      Top             =   9780
      Width           =   1575
   End
   Begin VB.TextBox txtVertpixels 
      Height          =   315
      Left            =   1740
      TabIndex        =   28
      ToolTipText     =   "The number of vertical pixels"
      Top             =   6900
      Width           =   1575
   End
   Begin VB.TextBox txtHorizpixels 
      Height          =   315
      Left            =   1740
      TabIndex        =   27
      ToolTipText     =   "The number of horizontal pixels"
      Top             =   6540
      Width           =   1575
   End
   Begin VB.ComboBox cmbClipcodec 
      Height          =   315
      Left            =   1740
      TabIndex        =   26
      ToolTipText     =   "The Clip Codec"
      Top             =   4380
      Width           =   1575
   End
   Begin VB.ComboBox cmbClipformat 
      Height          =   315
      Left            =   1740
      TabIndex        =   23
      ToolTipText     =   "The Clip Format"
      Top             =   4020
      Width           =   1575
   End
   Begin VB.PictureBox picFooter 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7815
      Left            =   16800
      ScaleHeight     =   7815
      ScaleWidth      =   4035
      TabIndex        =   15
      Top             =   6420
      Width           =   4035
      Begin VB.CommandButton cmdSubmitToATS 
         Caption         =   "Submit to ATS"
         Height          =   495
         Left            =   60
         TabIndex        =   233
         ToolTipText     =   "Auto-Transcode This File"
         Top             =   6060
         Width           =   1275
      End
      Begin VB.CommandButton cmdTranscodeProgOnly 
         Caption         =   "Prog Only Auto Trans"
         Height          =   495
         Left            =   1380
         TabIndex        =   203
         ToolTipText     =   "Auto Transcode only the logged Program item from this file"
         Top             =   5520
         Width           =   1275
      End
      Begin VB.CommandButton cmdUpdateLogging 
         Caption         =   "Update Ref Logging Data"
         Height          =   495
         Left            =   60
         TabIndex        =   154
         ToolTipText     =   "Update Logging Metadata for all clips with matching reference and Owner"
         Top             =   3360
         Width           =   1275
      End
      Begin VB.CommandButton cmdNotifyUsers 
         Caption         =   "Notify Media Window Users"
         Height          =   495
         Left            =   60
         TabIndex        =   150
         ToolTipText     =   "Notify Media WIndow Users about this file"
         Top             =   4440
         Width           =   1275
      End
      Begin VB.CommandButton cmdUpdateRefKeyframe 
         Caption         =   "Update Ref Keyframe Data"
         Height          =   495
         Left            =   2700
         TabIndex        =   135
         ToolTipText     =   "Update Keyframe Data for all clips with matching reference and Owner"
         Top             =   2820
         Width           =   1275
      End
      Begin VB.CommandButton cmdOutputExcelRecord 
         Caption         =   "Excel Record"
         Height          =   495
         Left            =   1380
         TabIndex        =   134
         ToolTipText     =   "Output an Excel Record for this file"
         Top             =   4440
         Width           =   1275
      End
      Begin VB.CommandButton cmdUpdateTimings 
         Caption         =   "Update Ref Clip Timings"
         Height          =   495
         Left            =   60
         TabIndex        =   133
         ToolTipText     =   "Update Timecode timings for all clips with matching reference and Owner"
         Top             =   2820
         Width           =   1275
      End
      Begin VB.CommandButton cmdFindReferenced 
         Caption         =   "Find Reference Clips"
         Height          =   495
         Left            =   1380
         TabIndex        =   112
         ToolTipText     =   "Find all clips with matching reference"
         Top             =   3900
         Width           =   1275
      End
      Begin VB.CommandButton cmdUpdateMetadata 
         Caption         =   "Update Ref Clip Prod Data"
         Height          =   495
         Left            =   1380
         TabIndex        =   111
         ToolTipText     =   "Update Prod Metadata for all clips with matching reference and Owner"
         Top             =   2820
         Width           =   1275
      End
      Begin VB.CommandButton cmdCompile 
         Caption         =   "Compile Clips"
         Height          =   495
         Left            =   1380
         TabIndex        =   110
         ToolTipText     =   "Compile Clips to make up this file"
         Top             =   4980
         Width           =   1275
      End
      Begin VB.CommandButton cmdSegment 
         Caption         =   "Segment File"
         Height          =   495
         Left            =   60
         TabIndex        =   103
         ToolTipText     =   "Segment This File"
         Top             =   4980
         Width           =   1275
      End
      Begin VB.CommandButton cmdReplay 
         Caption         =   "Replay to Tape"
         Height          =   495
         Left            =   2700
         TabIndex        =   98
         ToolTipText     =   "Make a Tape Record for this file if Replayed to Tape"
         Top             =   4980
         Visible         =   0   'False
         Width           =   1275
      End
      Begin VB.CommandButton cmdAutoTranscode 
         Caption         =   "Auto Trans"
         Height          =   495
         Left            =   60
         TabIndex        =   96
         ToolTipText     =   "Auto-Transcode This File"
         Top             =   5520
         Width           =   1275
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   495
         Left            =   60
         TabIndex        =   67
         ToolTipText     =   "Print this file record"
         Top             =   6600
         Width           =   1275
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2700
         TabIndex        =   19
         ToolTipText     =   "Close this form"
         Top             =   7140
         Width           =   1275
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   495
         Left            =   1380
         TabIndex        =   18
         ToolTipText     =   "Save this File Record"
         Top             =   7140
         Width           =   1275
      End
      Begin VB.CommandButton cmdDuplicate 
         Caption         =   "Duplicate"
         Height          =   495
         Left            =   1380
         TabIndex        =   17
         ToolTipText     =   "Duplicate this file record"
         Top             =   6600
         Width           =   1275
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   495
         Left            =   2700
         TabIndex        =   16
         ToolTipText     =   "Clear the form"
         Top             =   6600
         Width           =   1275
      End
   End
   Begin VB.TextBox txtClipID 
      BackColor       =   &H00C0FFFF&
      Height          =   315
      Left            =   1740
      TabIndex        =   13
      ToolTipText     =   "Scan or Type the Tape Barcode"
      Top             =   60
      Width           =   1575
   End
   Begin VB.ComboBox cmbVersion 
      Height          =   315
      Left            =   4680
      TabIndex        =   6
      ToolTipText     =   "The Version for the Tape"
      Top             =   1860
      Width           =   4575
   End
   Begin VB.TextBox txtSubTitle 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   4680
      TabIndex        =   5
      ToolTipText     =   "The sub title (if known)"
      Top             =   1140
      Width           =   4575
   End
   Begin VB.ComboBox cmbEpisode 
      Height          =   315
      Left            =   8160
      TabIndex        =   4
      ToolTipText     =   "The Episode for the Tape"
      Top             =   780
      Width           =   1095
   End
   Begin VB.ComboBox cmbSeries 
      Height          =   315
      Left            =   4680
      TabIndex        =   3
      ToolTipText     =   "The Series for the Tape"
      Top             =   780
      Width           =   1155
   End
   Begin VB.ComboBox cmbSet 
      Height          =   315
      Left            =   6360
      TabIndex        =   2
      ToolTipText     =   "The Series for the Tape"
      Top             =   780
      Width           =   1095
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   4680
      TabIndex        =   0
      ToolTipText     =   "The company this tape belongs to"
      Top             =   60
      Width           =   4575
      DataFieldList   =   "name"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "accountcode"
      Columns(2).Name =   "accountcode"
      Columns(2).DataField=   "accountcode"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "telephone"
      Columns(3).Name =   "telephone"
      Columns(3).DataField=   "telephone"
      Columns(3).FieldLen=   256
      _ExtentX        =   8070
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "name"
   End
   Begin MSAdodcLib.Adodc adoKeywords 
      Height          =   330
      Left            =   60
      Top             =   12600
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoKeywords"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoKeyword 
      Height          =   330
      Left            =   60
      Top             =   12300
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoKeyword"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoClientKeywords 
      Height          =   330
      Left            =   60
      Top             =   13500
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoClientKeywords"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoCustomFieldDefs 
      Height          =   330
      Left            =   60
      Top             =   13800
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCustomFieldDefs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoChecksumHistory 
      Height          =   330
      Left            =   3060
      Top             =   12300
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoChecksumHistory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoHistory 
      Height          =   330
      Left            =   3060
      Top             =   12600
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoHistory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtTitle 
      Height          =   315
      Left            =   4680
      TabIndex        =   153
      ToolTipText     =   "The company this tape belongs to"
      Top             =   420
      Width           =   4575
      DataFieldList   =   "title"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "masterseriestitleID"
      Columns(0).Name =   "masterseriestitleID"
      Columns(0).DataField=   "masterseriestitleID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   1773
      Columns(1).Caption=   "seriesID"
      Columns(1).Name =   "seriesID"
      Columns(1).DataField=   "seriesID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   8149
      Columns(2).Caption=   "title"
      Columns(2).Name =   "title"
      Columns(2).DataField=   "title"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "companyID"
      Columns(3).Name =   "companyID"
      Columns(3).DataField=   "companyID"
      Columns(3).FieldLen=   256
      _ExtentX        =   8070
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "title"
   End
   Begin MSAdodcLib.Adodc adoMediaWindowHistory 
      Height          =   330
      Left            =   3060
      Top             =   12900
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMediaWindowHistory"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSComctlLib.ProgressBar ProgressBar3 
      Height          =   315
      Left            =   16740
      TabIndex        =   207
      Top             =   2820
      Visible         =   0   'False
      Width           =   6075
      _ExtentX        =   10716
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSAdodcLib.Adodc adoAssignedJobs 
      Height          =   330
      Left            =   3060
      Top             =   13200
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoAssignedJobs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin MSAdodcLib.Adodc adoFFProbe 
      Height          =   330
      Left            =   3060
      Top             =   13500
      Visible         =   0   'False
      Width           =   2865
      _ExtentX        =   5054
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoFFProbe"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.TextBox txtTapeBarcode 
      Height          =   315
      Left            =   10560
      TabIndex        =   229
      ToolTipText     =   "Job number for which the clip was made"
      Top             =   1500
      Width           =   1635
   End
   Begin VB.ComboBox cmbVodPlatform 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   10560
      TabIndex        =   147
      ToolTipText     =   "The Clip Codec"
      Top             =   2580
      Width           =   1635
   End
   Begin VB.ComboBox cmbLanguage 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   10560
      TabIndex        =   138
      ToolTipText     =   "The Clip Codec"
      Top             =   2220
      Width           =   1635
   End
   Begin VB.ComboBox cmbFileVersion 
      Height          =   315
      Left            =   10560
      TabIndex        =   136
      ToolTipText     =   "The Version for the Tape"
      Top             =   1860
      Width           =   1635
   End
   Begin VB.CheckBox chkSourceFromMedia 
      Caption         =   "Source from Media"
      Height          =   255
      Left            =   12360
      TabIndex        =   125
      Top             =   780
      Width           =   2835
   End
   Begin VB.CommandButton cmdCopySizeToClipboard 
      Caption         =   "Copy Size"
      Height          =   315
      Left            =   15300
      TabIndex        =   104
      Top             =   1860
      Width           =   915
   End
   Begin VB.TextBox txtNotes 
      Height          =   435
      Left            =   10560
      MultiLine       =   -1  'True
      TabIndex        =   49
      ToolTipText     =   "Notes for the clip"
      Top             =   4020
      Width           =   4695
   End
   Begin VB.TextBox txtSourceEventID 
      Height          =   315
      Left            =   10560
      TabIndex        =   42
      ToolTipText     =   "The source Clip ID"
      Top             =   60
      Width           =   1635
   End
   Begin VB.TextBox txtSourcebarcode 
      BackColor       =   &H00C0C0FF&
      Height          =   315
      Left            =   10560
      TabIndex        =   37
      ToolTipText     =   "Barcode of the source library item for the clip"
      Top             =   420
      Width           =   1635
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbMediaSpecs 
      Height          =   315
      Left            =   13260
      TabIndex        =   82
      ToolTipText     =   "The company this tape belongs to"
      Top             =   60
      Width           =   1995
      DataFieldList   =   "mediaspecname"
      MaxDropDownItems=   24
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "companyID"
      Columns(0).Name =   "mediaspecID"
      Columns(0).DataField=   "mediaspecID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   7938
      Columns(1).Caption=   "mediaspecname"
      Columns(1).Name =   "mediaspecname"
      Columns(1).DataField=   "mediaspecname"
      Columns(1).FieldLen=   256
      _ExtentX        =   3519
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "mediaspecname"
   End
   Begin VB.TextBox txtFileSize 
      Height          =   315
      Left            =   13260
      TabIndex        =   305
      ToolTipText     =   "The source Clip ID"
      Top             =   1860
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.Label lblFileUnverified 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "FILE NOT VERIFIED"
      ForeColor       =   &H000000FF&
      Height          =   315
      Left            =   13260
      TabIndex        =   306
      Top             =   1860
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.Label lblOldCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   9780
      TabIndex        =   231
      Tag             =   "CLEARFIELDS"
      Top             =   11940
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape Barcode"
      Height          =   255
      Index           =   77
      Left            =   9420
      TabIndex        =   230
      Top             =   1560
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Chroma Subsampling"
      Height          =   255
      Index           =   30
      Left            =   60
      TabIndex        =   227
      Top             =   5160
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Colour Space"
      Height          =   255
      Index           =   28
      Left            =   60
      TabIndex        =   226
      Top             =   4800
      Width           =   1095
   End
   Begin VB.Label lblDADCItems 
      Caption         =   "DADC Items Requested"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   16800
      TabIndex        =   223
      Top             =   3180
      Width           =   6015
   End
   Begin VB.Label lblAudioConfiguration 
      Height          =   2775
      Left            =   16800
      TabIndex        =   222
      Top             =   3480
      Width           =   6015
   End
   Begin VB.Label lblCaption 
      Caption         =   "CETA Job Detail ID"
      Height          =   255
      Index           =   29
      Left            =   60
      TabIndex        =   221
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Serial# / WMLS#"
      Height          =   255
      Index           =   73
      Left            =   6480
      TabIndex        =   219
      Top             =   1560
      Width           =   1275
   End
   Begin VB.Label lblCaption 
      Caption         =   "# of Channels"
      Height          =   255
      Index           =   31
      Left            =   60
      TabIndex        =   210
      Top             =   10560
      Width           =   1155
   End
   Begin VB.Label lblDeletedClip 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Caption         =   "Deleted File"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   600
      TabIndex        =   206
      Top             =   120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Svensk Pr No."
      Height          =   255
      Index           =   46
      Left            =   60
      TabIndex        =   205
      Top             =   2640
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "# of Tracks"
      Height          =   255
      Index           =   67
      Left            =   60
      TabIndex        =   156
      Top             =   10200
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series ID"
      Height          =   255
      Index           =   66
      Left            =   3720
      TabIndex        =   152
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vod Platform"
      Height          =   255
      Index           =   116
      Left            =   9420
      TabIndex        =   148
      Top             =   2640
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Main Language"
      Height          =   255
      Index           =   115
      Left            =   9420
      TabIndex        =   139
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "File Version"
      Height          =   255
      Index           =   114
      Left            =   9420
      TabIndex        =   137
      Top             =   1920
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stream Type"
      Height          =   255
      Index           =   81
      Left            =   60
      TabIndex        =   109
      Top             =   5520
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Interlace?"
      Height          =   255
      Index           =   53
      Left            =   60
      TabIndex        =   94
      Top             =   7680
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "CBR / VBR"
      Height          =   255
      Index           =   52
      Left            =   60
      TabIndex        =   93
      Top             =   6240
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "No. of Passes"
      Height          =   255
      Index           =   51
      Left            =   60
      TabIndex        =   90
      Top             =   5880
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Bitrate"
      Height          =   255
      Index           =   50
      Left            =   60
      TabIndex        =   88
      Top             =   9480
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Audio Codec"
      Height          =   255
      Index           =   49
      Left            =   60
      TabIndex        =   86
      Top             =   9120
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Video Bitrate"
      Height          =   255
      Index           =   48
      Left            =   60
      TabIndex        =   85
      Top             =   8760
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Internal Ref"
      Height          =   255
      Index           =   45
      Left            =   60
      TabIndex        =   75
      Top             =   1560
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Notes MX1"
      Height          =   255
      Index           =   42
      Left            =   3720
      TabIndex        =   73
      Top             =   4020
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip Type"
      Height          =   255
      Index           =   41
      Left            =   60
      TabIndex        =   71
      Top             =   3360
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Genre"
      Height          =   255
      Index           =   40
      Left            =   60
      TabIndex        =   69
      Top             =   3000
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Frame Rate"
      Height          =   255
      Index           =   39
      Left            =   60
      TabIndex        =   66
      Top             =   7320
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip Purpose"
      Height          =   255
      Index           =   36
      Left            =   60
      TabIndex        =   62
      Top             =   3720
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Format"
      Height          =   255
      Index           =   35
      Left            =   9420
      TabIndex        =   60
      Top             =   1200
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      Height          =   255
      Index           =   34
      Left            =   60
      TabIndex        =   59
      Top             =   1200
      Width           =   1035
   End
   Begin VB.Label lblFormat 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1740
      TabIndex        =   58
      Tag             =   "CLEARFIELDS"
      Top             =   1140
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Geometry"
      Height          =   255
      Index           =   33
      Left            =   60
      TabIndex        =   56
      Top             =   8400
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Aspect Ratio"
      Height          =   255
      Index           =   32
      Left            =   60
      TabIndex        =   55
      Top             =   8040
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clock No"
      Height          =   255
      Index           =   27
      Left            =   3720
      TabIndex        =   52
      Top             =   2280
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Notes - client"
      Height          =   195
      Index           =   26
      Left            =   9420
      TabIndex        =   50
      Top             =   4020
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      Height          =   255
      Index           =   25
      Left            =   3720
      TabIndex        =   48
      Top             =   3000
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "CETA Job ID"
      Height          =   255
      Index           =   24
      Left            =   60
      TabIndex        =   46
      Top             =   1920
      Width           =   1335
   End
   Begin VB.Label lblCaption 
      Caption         =   "Folder"
      Height          =   255
      Index           =   23
      Left            =   3720
      TabIndex        =   44
      Top             =   3720
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Clip ID"
      Height          =   255
      Index           =   17
      Left            =   9420
      TabIndex        =   41
      Top             =   120
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Lib.ID"
      Height          =   255
      Index           =   16
      Left            =   9420
      TabIndex        =   40
      Top             =   840
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Source Barcode"
      Height          =   255
      Index           =   15
      Left            =   9420
      TabIndex        =   38
      Top             =   480
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Library ID"
      Height          =   255
      Index           =   14
      Left            =   60
      TabIndex        =   36
      Top             =   840
      Width           =   1095
   End
   Begin VB.Label lblLibraryID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   1740
      TabIndex        =   35
      Tag             =   "CLEARFIELDS"
      Top             =   780
      Width           =   1575
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filename"
      Height          =   255
      Index           =   13
      Left            =   3720
      TabIndex        =   34
      Top             =   3360
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Bitrate (kbps)"
      Height          =   255
      Index           =   12
      Left            =   60
      TabIndex        =   32
      Top             =   9840
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Vert Pixels"
      Height          =   255
      Index           =   11
      Left            =   60
      TabIndex        =   30
      Top             =   6960
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Horiz Pixels"
      Height          =   255
      Index           =   10
      Left            =   60
      TabIndex        =   29
      Top             =   6600
      Width           =   1155
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip Codec"
      Height          =   255
      Index           =   7
      Left            =   60
      TabIndex        =   25
      Top             =   4440
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip Format"
      Height          =   255
      Index           =   6
      Left            =   60
      TabIndex        =   24
      Top             =   4080
      Width           =   1095
   End
   Begin VB.Label lblCaption 
      Caption         =   "Library Barcode"
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   20
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Clip ID"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   14
      Top             =   120
      Width           =   495
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   86
      Left            =   7500
      TabIndex        =   12
      Top             =   840
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Set"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   87
      Left            =   5940
      TabIndex        =   11
      Top             =   840
      Width           =   435
   End
   Begin VB.Label lblCaption 
      Caption         =   "Sub Title"
      Height          =   255
      Index           =   9
      Left            =   3720
      TabIndex        =   10
      Top             =   1200
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      Height          =   255
      Index           =   8
      Left            =   3720
      TabIndex        =   9
      Top             =   480
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      Height          =   255
      Index           =   74
      Left            =   3720
      TabIndex        =   8
      Top             =   840
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Version"
      Height          =   255
      Index           =   4
      Left            =   3720
      TabIndex        =   7
      Top             =   1920
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      Height          =   255
      Index           =   3
      Left            =   3720
      TabIndex        =   1
      Top             =   120
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Mod"
      Height          =   255
      Index           =   44
      Left            =   12360
      TabIndex        =   217
      Top             =   1560
      Width           =   795
   End
   Begin VB.Label lblLastModifiedDate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13260
      TabIndex        =   216
      Tag             =   "CLEARFIELDS"
      Top             =   1500
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "MediaInfo"
      Height          =   255
      Index           =   68
      Left            =   12360
      TabIndex        =   162
      Top             =   2640
      Width           =   855
   End
   Begin VB.Label lblLastMediaInfoQuery 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13260
      TabIndex        =   161
      Tag             =   "CLEARFIELDS"
      Top             =   2580
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "File Verified"
      Height          =   255
      Index           =   62
      Left            =   12360
      TabIndex        =   102
      Top             =   2280
      Width           =   855
   End
   Begin VB.Label lblVerifiedDate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13260
      TabIndex        =   101
      Tag             =   "CLEARFIELDS"
      Top             =   2220
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "Size"
      Height          =   255
      Index           =   61
      Left            =   12360
      TabIndex        =   100
      Top             =   1920
      Width           =   315
   End
   Begin VB.Label lblCaption 
      Caption         =   "Media Spec"
      Height          =   255
      Index           =   47
      Left            =   12360
      TabIndex        =   81
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lblLastUsed 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13260
      TabIndex        =   64
      Tag             =   "CLEARFIELDS"
      Top             =   1140
      Width           =   1995
   End
   Begin VB.Label lblCaption 
      Caption         =   "Last Used"
      Height          =   255
      Index           =   38
      Left            =   12360
      TabIndex        =   63
      Top             =   1200
      Width           =   915
   End
   Begin VB.Label lblSourceFormat 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   10560
      TabIndex        =   57
      Tag             =   "CLEARFIELDS"
      Top             =   1140
      Width           =   1635
   End
   Begin VB.Label lblSourceLibraryID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   10560
      TabIndex        =   39
      Tag             =   "CLEARFIELDS"
      Top             =   780
      Width           =   1635
   End
   Begin VB.Label lblCaption 
      Caption         =   "Comp'y ID"
      Height          =   255
      Index           =   5
      Left            =   12360
      TabIndex        =   22
      Top             =   480
      Width           =   855
   End
   Begin VB.Label lblCompanyID 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13260
      TabIndex        =   21
      Tag             =   "CLEARFIELDS"
      Top             =   420
      Width           =   1995
   End
   Begin VB.Label lblFileSize 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   13260
      TabIndex        =   99
      Tag             =   "CLEARFIELDS"
      Top             =   1860
      Width           =   1995
   End
End
Attribute VB_Name = "frmClipControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_Framerate As Integer
Dim m_intTimecodeShuffle As Integer
Dim m_blnLoggingDelete As Boolean
Dim m_strHistory As String

Private Sub cmbAspect_Click()
    lblLibraryID.Caption = GetData("library", "libraryID", "barcode", cmbBarcode.Text)
    If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
End Sub

Private Sub cmbAspect_GotFocus()
PopulateCombo "aspectratio", cmbAspect
HighLite cmbAspect
End Sub

Private Sub cmbAudioCodec_GotFocus()
PopulateCombo "audiocodec", cmbAudioCodec
HighLite cmbAudioCodec
End Sub

Private Sub cmbAudioContent1_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent1
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent1
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent1
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent1
End If
HighLite cmbAudioContent1
End Sub

Private Sub cmbAudioContent2_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent2
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent2
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent2
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent2
End If
HighLite cmbAudioContent2
End Sub

Private Sub cmbAudioContent3_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent3
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent3
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent3
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent3
End If
HighLite cmbAudioContent3
End Sub

Private Sub cmbAudioContent4_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent4
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent4
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent4
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent4
End If
HighLite cmbAudioContent4
End Sub

Private Sub cmbAudioContent5_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent5
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent5
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent5
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent5
End If
HighLite cmbAudioContent5
End Sub

Private Sub cmbAudioContent6_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent6
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent6
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent6
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent6
End If
HighLite cmbAudioContent6
End Sub

Private Sub cmbAudioContent7_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent7
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent7
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent7
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent7
End If
HighLite cmbAudioContent7
End Sub

Private Sub cmbAudioContent8_GotFocus()
If Val(lblCompanyID.Caption) <> 0 Then
    If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/discoverytracker") > 0 Then
        PopulateCombo "discoveryaudiocontent", cmbAudioContent8
    ElseIf InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/sonypicturestracker") > 0 Then
        PopulateCombo "SPEAudioContent", cmbAudioContent8
    Else
        PopulateCombo "dadcaudiocontent", cmbAudioContent8
    End If
Else
    PopulateCombo "dadcaudiocontent", cmbAudioContent8
End If
HighLite cmbAudioContent8
End Sub

Private Sub cmbAudioLanguage1_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage1
HighLite cmbAudioLanguage1
End Sub

Private Sub cmbAudioLanguage2_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage2
HighLite cmbAudioLanguage2
End Sub

Private Sub cmbAudioLanguage3_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage3
HighLite cmbAudioLanguage3
End Sub

Private Sub cmbAudioLanguage4_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage4
HighLite cmbAudioLanguage4
End Sub

Private Sub cmbAudioLanguage5_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage5
HighLite cmbAudioLanguage5
End Sub

Private Sub cmbAudioLanguage6_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage6
HighLite cmbAudioLanguage6
End Sub

Private Sub cmbAudioLanguage7_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage7
HighLite cmbAudioLanguage7
End Sub

Private Sub cmbAudioLanguage8_GotFocus()
PopulateCombo "dadclanguages", cmbAudioLanguage8
HighLite cmbAudioLanguage8
End Sub

Private Sub cmbAudioTrackDepth_DropDown()
PopulateCombo "audiotrackdepth", cmbAudioTrackDepth
HighLite cmbAudioTrackDepth
End Sub

Private Sub cmbAudioTracks_DropDown()
PopulateCombo "audiotracks", cmbAudioTracks
HighLite cmbAudioTracks
End Sub

Private Sub cmbAudioType1_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType1
HighLite cmbAudioType1
End Sub

Private Sub cmbAudioType2_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType2
HighLite cmbAudioType2
End Sub

Private Sub cmbAudioType3_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType3
HighLite cmbAudioType3
End Sub

Private Sub cmbAudioType4_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType4
HighLite cmbAudioType4
End Sub

Private Sub cmbAudioType5_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType5
HighLite cmbAudioType5
End Sub

Private Sub cmbAudioType6_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType6
HighLite cmbAudioType6
End Sub

Private Sub cmbAudioType7_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType7
HighLite cmbAudioType7
End Sub

Private Sub cmbAudioType8_GotFocus()
PopulateCombo "dadcaudiotypes", cmbAudioType8
HighLite cmbAudioType8
End Sub

Private Sub cmbBarcode_Click()
    If cmbBarcode.Text <> "" Then
        lblLibraryID.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 libraryID FROM library WHERE barcode = '" & cmbBarcode.Text & "' AND system_deleted = 0"))
        lblFormat.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & cmbBarcode.Text & "' and system_deleted = 0"))
        If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
    Else
        lblLibraryID.Caption = "0"
    End If
    If lblLibraryID.Caption = "0" Then
        Beep
        lblLibraryID.Caption = ""
        lblFormat.Caption = ""
    End If
End Sub

Private Sub cmbBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    If cmbBarcode.Text <> "" Then
        KeyAscii = 0
        cmbBarcode.Text = UCase(cmbBarcode.Text)
        lblLibraryID.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 libraryID FROM library WHERE barcode = '" & cmbBarcode.Text & "' AND system_deleted = 0"))
        lblFormat.Caption = Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & cmbBarcode.Text & "' and system_deleted = 0"))
        If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
    Else
        lblLibraryID.Caption = "0"
    End If
    If lblLibraryID.Caption = "0" Then
        Beep
        lblLibraryID.Caption = ""
        lblFormat.Caption = ""
    End If
End If
End Sub

Private Sub cmbBurnedInSubtitleLanguage_GotFocus()
PopulateCombo "dadclanguages", cmbBurnedInSubtitleLanguage
HighLite cmbBurnedInSubtitleLanguage
End Sub

Private Sub cmbCbrVbr_GotFocus()
PopulateCombo "cbrvbr", cmbCbrVbr
End Sub

Private Sub cmbClipcodec_Click()
If cmbPurpose = "" Then cmbPurpose.Text = GetData("xref", "format", "description", cmbClipcodec.Text)
End Sub

Private Sub cmbClipcodec_GotFocus()
PopulateCombo "clipcodec", cmbClipcodec
HighLite cmbClipcodec
End Sub

Private Sub cmbClipformat_Click()
'cmbPurpose.Text = GetData("xref", "videostandard", "description", cmbClipformat.Text)
End Sub

Private Sub cmbClipformat_GotFocus()
PopulateCombo "clipformat", cmbClipformat
HighLite cmbClipformat
End Sub

Private Sub cmbClipType_GotFocus()
PopulateCombo "cliptype", cmbClipType
End Sub

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

'Try and load up the Custom Field Labels and dropdowns, and display the custom fields.
'Custom field labels are stored as part of the company table, for the owner of the clip.

Dim l_strTempLabel As String, l_lngCompanyID As Long
Dim l_strTemp As String
Dim l_strSQL As String

l_lngCompanyID = Val(lblCompanyID.Caption)

'If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/sha1checksum") > 0 Then
'    cmdMD5Checksum.Caption = "Make SHA1"
'Else
'    cmdMD5Checksum.Caption = "Make"
'End If
'
If l_lngCompanyID <> 0 Then
    
    lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
    
    Dim l_conSearch As ADODB.Connection
    Dim l_rstSearch2 As ADODB.Recordset
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch2 = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"
    
    With l_rstSearch2
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch2.ActiveConnection = Nothing
    
    Set txtTitle.DataSourceList = l_rstSearch2
    
    l_conSearch.Close
    Set l_conSearch = Nothing
    
    lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", l_lngCompanyID))
    If lblField1.Caption = "" Then
        lblField1.Visible = False
        cmbField1.Visible = False
    Else
        lblField1.Visible = True
        cmbField1.Visible = True
        l_strTemp = cmbField1.Text
        PopulateCustomFieldCombo l_lngCompanyID, 1, cmbField1
        cmbField1.Text = l_strTemp
    End If
    lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", l_lngCompanyID))
    If lblField2.Caption = "" Then
        lblField2.Visible = False
        cmbField2.Visible = False
    Else
        lblField2.Visible = True
        cmbField2.Visible = True
        l_strTemp = cmbField2.Text
        PopulateCustomFieldCombo l_lngCompanyID, 2, cmbField2
        cmbField2.Text = l_strTemp
    End If
    lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", l_lngCompanyID))
    If lblField3.Caption = "" Then
        lblField3.Visible = False
        cmbField3.Visible = False
    Else
        lblField3.Visible = True
        cmbField3.Visible = True
        l_strTemp = cmbField3.Text
        PopulateCustomFieldCombo l_lngCompanyID, 3, cmbField3
        cmbField3.Text = l_strTemp
    End If
    lblField4.Caption = Trim(" " & GetData("company", "customfield4label", "companyID", l_lngCompanyID))
    If lblField4.Caption = "" Then
        lblField4.Visible = False
        cmbField4.Visible = False
    Else
        lblField4.Visible = True
        cmbField4.Visible = True
        l_strTemp = cmbField4.Text
        PopulateCustomFieldCombo l_lngCompanyID, 4, cmbField4
        cmbField4.Text = l_strTemp
    End If
    lblField5.Caption = Trim(" " & GetData("company", "customfield5label", "companyID", l_lngCompanyID))
    If lblField5.Caption = "" Then
        lblField5.Visible = False
        cmbField5.Visible = False
    Else
        lblField5.Visible = True
        cmbField5.Visible = True
        l_strTemp = cmbField5.Text
        PopulateCustomFieldCombo l_lngCompanyID, 5, cmbField5
        cmbField5.Text = l_strTemp
    End If
    lblField6.Caption = Trim(" " & GetData("company", "customfield6label", "companyID", l_lngCompanyID))
    If lblField6.Caption = "" Then
        lblField6.Visible = False
        cmbField6.Visible = False
    Else
        lblField6.Visible = True
        cmbField6.Visible = True
        l_strTemp = cmbField6.Text
        PopulateCustomFieldCombo l_lngCompanyID, 6, cmbField6
        cmbField6.Text = l_strTemp
    End If
    
    l_strSQL = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = " & l_lngCompanyID & "ORDER BY keywordtext;"
    
    adoKeyword.ConnectionString = g_strConnection
    adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = " & l_lngCompanyID & "ORDER BY keywordtext;"
    adoKeyword.Refresh
    
    If adoKeyword.Recordset.RecordCount <> 0 Then grdKeyword.Columns("keywordtext").DropDownHwnd = ddnKeyword.hWnd
    
    adoClientKeywords.ConnectionString = g_strConnection
    adoClientKeywords.RecordSource = "SELECT keywordtext, companyID FROM keyword WHERE companyID = " & l_lngCompanyID & ";"
    adoClientKeywords.Refresh
    
    adoCustomFieldDefs.ConnectionString = g_strConnection
    adoCustomFieldDefs.RecordSource = "SELECT * FROM customfieldxref where companyID = " & l_lngCompanyID & " ORDER BY customfield, textentry;"
    adoCustomFieldDefs.Refresh
    
    If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/timedpermissions") > 0 Then
        frmClipControl.grdPortalPermission.Columns("permissionstart").Visible = True
        frmClipControl.grdPortalPermission.Columns("permissionend").Visible = True
        frmClipControl.grdPortalPermission.Columns("fullname").Width = 2100
    Else
        frmClipControl.grdPortalPermission.Columns("permissionstart").Visible = False
        frmClipControl.grdPortalPermission.Columns("permissionend").Visible = False
        frmClipControl.grdPortalPermission.Columns("fullname").Width = 5490
    End If

    If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/shineversion") > 0 Then
        lblCaption(66).Caption = "Title Code"
    Else
        lblCaption(66).Caption = "Series ID"
    End If
    
    If g_blnRedNetwork = True Then
    
        If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/dadctracker") > 0 Then
            cmdDADCOutput.Enabled = True
            cmdDADCOutputAudioOnly.Enabled = True
        Else
            cmdDADCOutput.Enabled = False
            cmdDADCOutputAudioOnly.Enabled = False
        End If
                
    Else
        
        cmdDADCOutput.Enabled = False
        cmdDADCOutputAudioOnly.Enabled = False
    
    End If

End If

End Sub

Private Sub cmbCompany_KeyPress(KeyAscii As Integer)

Dim l_strTempLabel As String, l_lngCompanyID As Long
Dim l_strTemp As String
Dim l_strSQL As String

If KeyAscii = 13 Then

    lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

    'Try and load up the Custom Field Labels and dropdowns, and display the custom fields.
    'Custom field labels are stored as part of the company table, for the owner of the clip.
    
    l_lngCompanyID = Val(lblCompanyID.Caption)
    
    'If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/sha1checksum") > 0 Then
    '    cmdMD5Checksum.Caption = "Make SHA1"
    'Else
    '    cmdMD5Checksum.Caption = "Make"
    'End If
    '
    If l_lngCompanyID <> 0 Then
        
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch2 As ADODB.Recordset
        
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch2 = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"
        
        With l_rstSearch2
            .CursorLocation = adUseClient
            .LockType = adLockBatchOptimistic
            .CursorType = adOpenDynamic
            .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch2.ActiveConnection = Nothing
        
        Set txtTitle.DataSourceList = l_rstSearch2
        
        l_conSearch.Close
        Set l_conSearch = Nothing
        
        lblField1.Caption = Trim(" " & GetData("company", "customfield1label", "companyID", l_lngCompanyID))
        If lblField1.Caption = "" Then
            lblField1.Visible = False
            cmbField1.Visible = False
        Else
            lblField1.Visible = True
            cmbField1.Visible = True
            PopulateCustomFieldCombo l_lngCompanyID, 1, cmbField1
        End If
        lblField2.Caption = Trim(" " & GetData("company", "customfield2label", "companyID", l_lngCompanyID))
        If lblField2.Caption = "" Then
            lblField2.Visible = False
            cmbField2.Visible = False
        Else
            lblField2.Visible = True
            cmbField2.Visible = True
            PopulateCustomFieldCombo l_lngCompanyID, 2, cmbField2
        End If
        lblField3.Caption = Trim(" " & GetData("company", "customfield3label", "companyID", l_lngCompanyID))
        If lblField3.Caption = "" Then
            lblField3.Visible = False
            cmbField3.Visible = False
        Else
            lblField3.Visible = True
            cmbField3.Visible = True
            PopulateCustomFieldCombo l_lngCompanyID, 3, cmbField3
        End If
        lblField4.Caption = Trim(" " & GetData("company", "customfield4label", "companyID", l_lngCompanyID))
        If lblField4.Caption = "" Then
            lblField4.Visible = False
            cmbField4.Visible = False
        Else
            lblField4.Visible = True
            cmbField4.Visible = True
            PopulateCustomFieldCombo l_lngCompanyID, 4, cmbField4
        End If
        lblField5.Caption = Trim(" " & GetData("company", "customfield5label", "companyID", l_lngCompanyID))
        If lblField5.Caption = "" Then
            lblField5.Visible = False
            cmbField5.Visible = False
        Else
            lblField5.Visible = True
            cmbField5.Visible = True
            PopulateCustomFieldCombo l_lngCompanyID, 5, cmbField5
        End If
        lblField6.Caption = Trim(" " & GetData("company", "customfield6label", "companyID", l_lngCompanyID))
        If lblField6.Caption = "" Then
            lblField6.Visible = False
            cmbField6.Visible = False
        Else
            lblField6.Visible = True
            cmbField6.Visible = True
            PopulateCustomFieldCombo l_lngCompanyID, 6, cmbField6
        End If
        
        l_strSQL = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = " & l_lngCompanyID & "ORDER BY keywordtext;"
        
        adoKeyword.ConnectionString = g_strConnection
        adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = " & l_lngCompanyID & "ORDER BY keywordtext;"
        adoKeyword.Refresh
        
        If adoKeyword.Recordset.RecordCount <> 0 Then grdKeyword.Columns("keywordtext").DropDownHwnd = ddnKeyword.hWnd
        
        adoClientKeywords.ConnectionString = g_strConnection
        adoClientKeywords.RecordSource = "SELECT keywordtext, companyID FROM keyword WHERE companyID = " & l_lngCompanyID & ";"
        adoClientKeywords.Refresh
        
        adoCustomFieldDefs.ConnectionString = g_strConnection
        adoCustomFieldDefs.RecordSource = "SELECT * FROM customfieldxref where companyID = " & l_lngCompanyID & ";"
        adoCustomFieldDefs.Refresh
        
    End If
    
    KeyAscii = 0

End If
    
End Sub

Private Sub cmbEncodeReplayDeck_DropDown()
Dim l_strTemp As String
l_strTemp = cmbEncodeReplayDeck.Text
cmbEncodeReplayDeck.Clear
PopulateCombo "machine", cmbEncodeReplayDeck
cmbEncodeReplayDeck.Text = l_strTemp
End Sub

Private Sub cmbEpisode_GotFocus()
PopulateCombo "episode", cmbEpisode
HighLite cmbEpisode
End Sub

Private Sub cmbFileVersion_GotFocus()
PopulateCombo "fileversion", cmbFileVersion
HighLite cmbFileVersion
End Sub

Private Sub cmbFrameRate_Click()
    
    lblLibraryID.Caption = GetData("library", "libraryID", "barcode", cmbBarcode.Text)
    If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""
    
End Sub

Private Sub cmbFrameRate_GotFocus()

PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate

End Sub

Private Sub cmbFrameRate_LostFocus()

Set_Framerate

End Sub

Private Sub cmbGenre_GotFocus()

PopulateCombo "genre", cmbGenre

End Sub

Private Sub cmbGeometry_Click()

lblLibraryID.Caption = GetData("library", "libraryID", "barcode", cmbBarcode.Text)
If lblLibraryID.Caption = "0" Then Beep: lblLibraryID.Caption = ""

End Sub

Private Sub cmbGeometry_GotFocus()

PopulateCombo "geometry", cmbGeometry
HighLite cmbGeometry

End Sub

Private Sub cmbInterlace_GotFocus()

PopulateCombo "interlace", cmbInterlace
HighLite cmbInterlace

End Sub

Private Sub cmbIsTextInPicture_GotFocus()
PopulateCombo "SPETextInPicture", cmbIsTextInPicture
HighLite cmbIsTextInPicture
End Sub

Private Sub cmbLanguage_GotFocus()
PopulateCombo "language", cmbLanguage
HighLite cmbLanguage
End Sub

Private Sub cmbMediaSpecs_Click()

Dim l_lngMediaSpecID As Long, l_strSQL As String, l_rstSpec As ADODB.Recordset

l_lngMediaSpecID = Val(cmbMediaSpecs.Columns("mediaspecID").Text)

If l_lngMediaSpecID <> 0 Then
    l_strSQL = "SELECT * FROM mediaspec WHERE mediaspecID = '" & l_lngMediaSpecID & "';"
    
    Set l_rstSpec = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstSpec.EOF Then
        cmbClipformat.Text = Trim(" " & l_rstSpec("mediaformat"))
        cmbClipcodec.Text = Trim(" " & l_rstSpec("videocodec"))
        cmbStreamType.Text = Trim(" " & l_rstSpec("mediastreamtype"))
        cmbAudioCodec.Text = Trim(" " & l_rstSpec("audiocodec"))
        cmbPasses.Text = Trim(" " & l_rstSpec("encodepasses"))
        txtHorizpixels.Text = Trim(" " & l_rstSpec("horiz"))
        txtVertpixels.Text = Trim(" " & l_rstSpec("vert"))
        cmbFrameRate.Text = Trim(" " & l_rstSpec("framerate"))
        txtBitrate.Text = Trim(" " & l_rstSpec("totalbitrate"))
        txtVideoBitrate.Text = Trim(" " & l_rstSpec("videobitrate"))
        txtAudioBitrate.Text = Trim(" " & l_rstSpec("audiobitrate"))
        cmbAspect.Text = Trim(" " & l_rstSpec("aspectratio"))
        cmbGeometry.Text = Trim(" " & l_rstSpec("geometry"))
        If Trim(" " & l_rstSpec("altlocation")) <> "" Then
            txtAltFolder.Text = Trim(" " & l_rstSpec("altlocation"))
        End If
        If Trim(" " & l_rstSpec("graphicoverlay")) = "YES" Then
            txtInternalNotes.Text = txtInternalNotes.Text & "OVERLAY:" & Trim(" " & l_rstSpec("overlayfilename")) & " at " & Trim(" " & l_rstSpec("overlaytransparency")) & " transparency."
        End If
        If Trim(" " & l_rstSpec("otherspecs")) <> "" Then
            txtInternalNotes.Text = txtInternalNotes.Text & Trim(" " & l_rstSpec("otherspecs"))
        End If
        cmbCbrVbr.Text = Trim(" " & l_rstSpec("cbrvbr"))
        cmbInterlace.Text = Trim(" " & l_rstSpec("interlace"))
        cmbVodPlatform.Text = Trim(" " & l_rstSpec("vodplatform"))
    End If
    l_rstSpec.Close
    Set l_rstSpec = Nothing

End If

cmbMediaSpecs.Text = ""

End Sub

Private Sub cmbPasses_GotFocus()
PopulateCombo "encodepasses", cmbPasses
End Sub

Private Sub cmbPurpose_GotFocus()
PopulateCombo "clippurpose", cmbPurpose
HighLite cmbPurpose
End Sub

Private Sub cmbSeries_GotFocus()
PopulateCombo "series", cmbSeries
HighLite cmbSeries
End Sub
Private Sub cmbSet_GotFocus()
PopulateCombo "series", cmbSet
HighLite cmbSet
End Sub

Private Sub cmbStreamType_GotFocus()

PopulateCombo "clipstreamtype", cmbStreamType
HighLite cmbStreamType

End Sub

Private Sub cmbTextInPictureLanguage_GotFocus()
PopulateCombo "dadclanguages", cmbTextInPictureLanguage
HighLite cmbTextInPictureLanguage
End Sub

Private Sub cmbVersion_DropDown()

Dim l_strTemp As String
l_strTemp = cmbVersion.Text
If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/shineversion") > 0 Then
    PopulateCombo "shineversion", cmbVersion, "", True
Else
    PopulateCombo "version", cmbVersion, "", True
End If
cmbVersion.Text = l_strTemp

End Sub

Private Sub cmbVersion_GotFocus()
HighLite cmbVersion
End Sub

Private Sub cmbVodPlatform_GotFocus()
PopulateCombo "vodplatform", cmbVodPlatform
HighLite cmbVodPlatform
End Sub

Private Sub cmdAssignToJobAsMaster_Click()

Dim l_lngJobID As Long, l_strSQL As String

If Val(txtClipID.Text) = 0 Then Exit Sub

l_lngJobID = Val(InputBox("Please input the JobID to which this file should be assigned as a Master", "JobID to Assign"))
If l_lngJobID = 0 Then Exit Sub

l_strSQL = "INSERT INTO requiredmedia (jobID, eventID, CDATE, CUSER) VALUES ("
l_strSQL = l_strSQL & l_lngJobID & ", "
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "')"

Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Private Sub cmdAutoTranscode_Click()

If Validate_Timecode(txtTimecodeStart.Text, m_Framerate) <> True Then
    MsgBox "Cannot transcode a clip unless start timecode entry is valid", vbCritical, "Error."
ElseIf lblFormat.Caption <> "DISCSTORE" Then
    MsgBox "Cannot transcode a clip unless it is on a DISCSTORE", vbCritical, "Error."
Else
    
    frmXMLTranscode.chkBulkTranscoding.Value = 0
    frmXMLTranscode.lblTimecodeStart.Caption = txtTimecodeStart.Text
    frmXMLTranscode.lblTimecodeStop.Caption = txtTimecodeStop.Text
    frmXMLTranscode.chkTimecodesFromCaller.Value = 0
    frmXMLTranscode.lblSourceFormat.Caption = cmbClipformat.Text
    frmXMLTranscode.chkMOConvert.Value = 0
    
    Select Case m_Framerate
        Case TC_25
            frmXMLTranscode.optTimecodeType(0).Value = True
        Case TC_29
            frmXMLTranscode.optTimecodeType(1).Value = True
        Case TC_24
            frmXMLTranscode.optTimecodeType(3).Value = True
        Case TC_30
            frmXMLTranscode.optTimecodeType(2).Value = True
        Case TC_50
            frmXMLTranscode.optTimecodeType(4).Value = True
        Case TC_59
            frmXMLTranscode.optTimecodeType(5).Value = True
        Case TC_60
            frmXMLTranscode.optTimecodeType(6).Value = True
        Case Else
            frmXMLTranscode.optTimecodeType(0).Value = True
    End Select
    
    frmXMLTranscode.Show vbModal

End If

Unload frmXMLTranscode

End Sub

Private Sub cmdBatonQC_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If SaveClip(chkIgnoreJobID.Value) = False Then Exit Sub

If lblFormat.Caption <> "DISCSTORE" Then
    MsgBox "Cannot do a Baton check on a clip unless it is on a DISCSTORE", vbCritical, "Error."
ElseIf cmbClipformat.Text = "Folder" Then
    MsgBox "Cannot do a Baton check on a folder", vbCritical, "Error."
Else
    frmGetATSProfile.chkBatonProfile.Value = 1
    frmGetATSProfile.Show vbModal
    If frmGetATSProfile.cmbProfile.Text = "" Then
        MsgBox "No Baton Profile Selected", vbInformation
        Unload frmGetATSProfile
        Exit Sub
    End If
    
    Dim l_strSQL As String, l_strReports As String
    l_strReports = ""
    l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, NewFileName, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Baton QC") & ", "
    l_strSQL = l_strSQL & "'" & frmGetATSProfile.cmbProfile.Text & "', "
    If MsgBox("Do you want to preserve an XML of the Report?", vbYesNo + vbDefaultButton2, "Baton Report") = vbYes Then
        l_strReports = l_strReports & "1"
    ElseIf MsgBox("Do you want to preserve a PDF of the Report?", vbYesNo + vbDefaultButton2, "Baton Report") = vbYes Then
        l_strReports = l_strReports & "2"
    Else
        l_strReports = l_strReports & "0"
    End If
    l_strSQL = l_strSQL & IIf(l_strReports <> "", Val(l_strReports), "Null") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    Unload frmGetATSProfile
    
    MsgBox "Done"
End If
    
End Sub

Private Sub cmdClear_Click()
'PromptClipChanges Val(txtClipID.Text)
ClearFields Me
On Error Resume Next
picKeyframe.Picture = LoadPicture(App.Path & "\Empty.gif")
picKeyframe.Refresh
On Error GoTo 0

'Load up the keywords
adoKeywords.ConnectionString = g_strConnection
adoKeywords.RecordSource = "SELECT eventID, keywordtext  FROM eventkeyword  WHERE eventID = -1;"
adoKeywords.Refresh

adoKeyword.ConnectionString = g_strConnection
adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = -1 ORDER BY keywordtext;"
adoKeyword.Refresh
grdKeyword.Columns("keywordtext").DropDownHwnd = 0

adoClientKeywords.ConnectionString = g_strConnection
adoClientKeywords.RecordSource = "SELECT keywordtext, companyID from keyword WHERE companyID = -1;"
adoClientKeywords.Refresh

adoHistory.ConnectionString = g_strConnection
adoHistory.RecordSource = "SELECT * FROM eventhistory WHERE eventID = -1 ORDER BY datesaved DESC;"
adoHistory.Refresh

'Set the Portal Permission Stuff

adoPortalPermission.ConnectionString = g_strConnection
adoPortalPermission.RecordSource = "SELECT * FROM portalpermission WHERE eventID = -1 ORDER BY fullname;"
adoPortalPermission.Refresh

adoPortalUsers.ConnectionString = g_strConnection
adoPortalUsers.RecordSource = "SELECT * from portaluser WHERE companyID = -1;"
adoPortalUsers.Refresh
    
grdPortalPermission.Columns("fullname").DropDownHwnd = 0

End Sub

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub cmdCompile_Click()

If Val(txtClipID.Text) <> 0 Then
    frmClipCompile.Show
End If

End Sub

Private Sub cmdCopyAndChangeOwner_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

Dim l_lngNewCompanyID As Long
Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_blnPreserveMasterfileStatus As Boolean, l_lngDestinationLibraryID As Long

frmGetNewFileDetails.Caption = "Please give the details for the Copy..."
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
If frmGetNewFileDetails.chkPreserveMasterfile.Value = 0 Then l_blnPreserveMasterfileStatus = False Else l_blnPreserveMasterfileStatus = True
Unload frmGetNewFileDetails

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" And Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & l_strNewLibraryBarcode & "' AND system_deleted = 0;")) = "DISCSTORE" Then
    
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & GetData("events", "altlocation", "eventID", Val(txtClipID.Text)) & "' "
    End If
    l_strSQL = l_strSQL & "' AND clipfilename = '" & txtClipfilename.Text & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        frmGetNewCompany.Show vbModal
        If frmGetNewCompany.lblCompanyID.Caption <> "" Then
            l_lngNewCompanyID = Val(frmGetNewCompany.lblCompanyID.Caption)
        Else
            MsgBox "No new Owner specified - request aborted."
            Exit Sub
        End If
        Unload frmGetNewCompany
        
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, NewCompanyID, PreserveMasterfileStatus, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
        l_strSQL = l_strSQL & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode) & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
        End If
        If l_lngNewCompanyID <> 0 Then
            l_strSQL = l_strSQL & l_lngNewCompanyID & ", "
        Else
            l_strSQL = l_strSQL & "Null, "
        End If
        If l_blnPreserveMasterfileStatus = True Then
            l_strSQL = l_strSQL & "1, "
        Else
            l_strSQL = l_strSQL & "0, "
        End If
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        If cmbClipformat.Text = "Folder" Then
            MsgBox "Any Individual File records for files within this folder will not be processed by this action."
        End If
        MsgBox "Request Submitted"
    Else
        MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
    End If
Else
    MsgBox "Destination Barcode for copies must be a DISCSTORE", vbCritical
End If

End Sub

Private Sub cmdCopyFile_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strSQL As String, l_blnPreserveMasterfileStatus As Boolean, l_lngDestinationLibraryID As Long

frmGetNewFileDetails.Caption = "Please give the details for the Copy..."
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
If frmGetNewFileDetails.chkPreserveMasterfile.Value = 0 Then l_blnPreserveMasterfileStatus = False Else l_blnPreserveMasterfileStatus = True
Unload frmGetNewFileDetails

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" And Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & l_strNewLibraryBarcode & "' AND system_deleted = 0;")) = "DISCSTORE" Then
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & GetData("events", "altlocation", "eventID", Val(txtClipID.Text)) & "' "
    End If
    l_strSQL = l_strSQL & "' AND clipfilename = '" & txtClipfilename.Text & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, PreserveMasterfileStatus, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
        l_strSQL = l_strSQL & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode) & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
        End If
        If l_blnPreserveMasterfileStatus = True Then
            l_strSQL = l_strSQL & "1, "
        Else
            l_strSQL = l_strSQL & "0, "
        End If
        l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        If cmbClipformat.Text = "Folder" Then
            MsgBox "Any Individual File records for files within this folder will not be processed by this action."
        End If
        MsgBox "Request Submitted"
    Else
        MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
    End If
Else
    MsgBox "Destination Barcode for copies must be a DISCSTORE", vbCritical
End If

End Sub

Private Sub cmdCopyJSONToClipboard_Click()

Clipboard.Clear
Clipboard.SetText txtFFprobeNotes.Text

End Sub

Private Sub cmdCopySizeToClipboard_Click()

Clipboard.Clear
Clipboard.SetText Format(lblFileSize.Caption, "#")

End Sub

Private Sub cmdDADCOutput_Click()

Dim l_strRequirements(9) As String, l_lngCounter As Long, l_lngCounter2 As Long, l_strTemp As String, l_lngCharCount As Long, l_strTest As String, l_strTest2 As String
Dim l_blnFound As Boolean, l_blnVOD As Boolean

m_strHistory = "DADC BBC XML"
cmdSave.Value = True
m_strHistory = ""

l_lngCounter = 0
l_blnVOD = False
l_strTemp = lblAudioConfiguration.Caption
If InStr(l_strTemp, vbCrLf) > 0 Then
    Do While InStr(l_strTemp, vbCrLf) > 0
        l_lngCharCount = InStr(l_strTemp, vbCrLf)
        l_strRequirements(l_lngCounter) = Left(l_strTemp, l_lngCharCount - 1)
        If Left(l_strRequirements(l_lngCounter), 10) = "BBCW Muxed" Then
            l_lngCounter2 = InStr(l_strRequirements(l_lngCounter), ",")
            l_strRequirements(l_lngCounter) = Mid(l_strRequirements(l_lngCounter), l_lngCounter2 + 2)
        End If
        l_strTemp = Mid(l_strTemp, l_lngCharCount + 2)
        l_lngCounter = l_lngCounter + 1
    Loop
    l_strRequirements(l_lngCounter) = l_strTemp
Else
    l_strRequirements(0) = l_strTemp
End If

For l_lngCounter = 0 To 9
    If l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) <> "BBCW ProRes" And Left(l_strRequirements(l_lngCounter), 17) <> "VOD - BBCW ProRes" Then
        l_blnFound = False
        If cmbAudioContent1.Text = "M&E" Then
            l_strTest = cmbAudioType1.Text & ", M&&E"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Music" Or cmbAudioContent1.Text = "Effects" Then
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Partial M&E" Then
            l_strTest = cmbAudioType1.Text & ", Partial M&&E"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType1.Text & ", Mix Minus Narration"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage1.Text, "(") Then
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & Left(cmbAudioLanguage1.Text, InStr(cmbAudioLanguage1.Text, "(") - 2)
            l_strTest2 = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & cmbAudioLanguage1.Text
        Else
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & IIf(cmbAudioLanguage1.Text <> "", ", " & cmbAudioLanguage1.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent2.Text = "M&E" Then
            l_strTest = cmbAudioType2.Text & ", M&&E"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Music" Or cmbAudioContent2.Text = "Effects" Then
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Partial M&E" Then
            l_strTest = cmbAudioType2.Text & ", Partial M&&E"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType2.Text & ", Mix Minus Narration"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage2.Text, "(") Then
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & Left(cmbAudioLanguage2.Text, InStr(cmbAudioLanguage2.Text, "(") - 2)
            l_strTest2 = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & cmbAudioLanguage2.Text
        Else
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & IIf(cmbAudioLanguage2.Text <> "", ", " & cmbAudioLanguage2.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent3.Text = "M&E" Then
            l_strTest = cmbAudioType3.Text & ", M&&E"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Music" Or cmbAudioContent3.Text = "Effects" Then
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Partial M&E" Then
            l_strTest = cmbAudioType3.Text & ", Partial M&&E"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType3.Text & ", Mix Minus Narration"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage3.Text, "(") Then
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & Left(cmbAudioLanguage3.Text, InStr(cmbAudioLanguage3.Text, "(") - 2)
            l_strTest2 = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & cmbAudioLanguage3.Text
        Else
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & IIf(cmbAudioLanguage3.Text <> "", ", " & cmbAudioLanguage3.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent4.Text = "M&E" Then
            l_strTest = cmbAudioType4.Text & ", M&&E"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Music" Or cmbAudioContent4.Text = "Effects" Then
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Partial M&E" Then
            l_strTest = cmbAudioType4.Text & ", Partial M&&E"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType4.Text & ", Mix Minus Narration"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage4.Text, "(") Then
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & Left(cmbAudioLanguage4.Text, InStr(cmbAudioLanguage4.Text, "(") - 2)
            l_strTest2 = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & cmbAudioLanguage4.Text
        Else
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & IIf(cmbAudioLanguage4.Text <> "", ", " & cmbAudioLanguage4.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent5.Text = "M&E" Then
            l_strTest = cmbAudioType5.Text & ", M&&E"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Music" Or cmbAudioContent5.Text = "Effects" Then
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Partial M&E" Then
            l_strTest = cmbAudioType5.Text & ", Partial M&&E"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType5.Text & ", Mix Minus Narration"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage5.Text, "(") Then
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & Left(cmbAudioLanguage5.Text, InStr(cmbAudioLanguage5.Text, "(") - 2)
            l_strTest2 = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & cmbAudioLanguage5.Text
        Else
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & IIf(cmbAudioLanguage5.Text <> "", ", " & cmbAudioLanguage5.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent6.Text = "M&E" Then
            l_strTest = cmbAudioType6.Text & ", M&&E"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Music" Or cmbAudioContent6.Text = "Effects" Then
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Partial M&E" Then
            l_strTest = cmbAudioType6.Text & ", Partial M&&E"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType6.Text & ", Mix Minus Narration"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage6.Text, "(") Then
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & Left(cmbAudioLanguage6.Text, InStr(cmbAudioLanguage6.Text, "(") - 2)
            l_strTest2 = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & cmbAudioLanguage6.Text
        Else
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & IIf(cmbAudioLanguage6.Text <> "", ", " & cmbAudioLanguage6.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent7.Text = "M&E" Then
            l_strTest = cmbAudioType7.Text & ", M&&E"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Music" Or cmbAudioContent7.Text = "Effects" Then
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Partial M&E" Then
            l_strTest = cmbAudioType7.Text & ", Partial M&&E"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType7.Text & ", Mix Minus Narration"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage7.Text, "(") Then
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & Left(cmbAudioLanguage7.Text, InStr(cmbAudioLanguage7.Text, "(") - 2)
            l_strTest2 = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & cmbAudioLanguage7.Text
        Else
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & IIf(cmbAudioLanguage7.Text <> "", ", " & cmbAudioLanguage7.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent8.Text = "M&E" Then
            l_strTest = cmbAudioType8.Text & ", M&&E"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Music" Or cmbAudioContent8.Text = "Effects" Then
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Partial M&E" Then
            l_strTest = cmbAudioType8.Text & ", Partial M&&E"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType8.Text & ", Mix Minus Narration"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML Not Made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage8.Text, "(") Then
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & Left(cmbAudioLanguage8.Text, InStr(cmbAudioLanguage8.Text, "(") - 2)
            l_strTest2 = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & cmbAudioLanguage8.Text
        Else
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & IIf(cmbAudioLanguage8.Text <> "", ", " & cmbAudioLanguage8.Text, "")
        End If
        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
            l_blnFound = True
        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If l_blnFound = False Then
            MsgBox "There was a listed item " & l_strRequirements(l_lngCounter) & " that has not been logged. Cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
    ElseIf l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) = "BBCW ProRes" Then
        If InStr(l_strRequirements(l_lngCounter), txtVertpixels.Text) > 0 Then
            'Do nothing
        ElseIf InStr(l_strRequirements(l_lngCounter), "PAL") >= 0 And txtVertpixels = "576" Then
            If cmbClipcodec.Text <> "ProRes HQ" Then
                MsgBox "PAL Files must be ProRes HQ." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
             End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "NTSC") >= 0 And txtVertpixels = "480" Then
            If cmbClipcodec.Text <> "ProRes HQ" Then
                MsgBox "NTSC Files must be ProRes HQ." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "UHD") >= 0 And txtVertpixels = "2160" Then
            If cmbClipcodec.Text <> "ProRes HQ" Then
                MsgBox "UHD Files must be ProRes HQ." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "1080") >= 0 And txtVertpixels = "1080" Then
            If cmbClipcodec.Text <> "ProRes HQ" Then
                MsgBox "HD Files must be ProRes HQ." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        Else
            MsgBox "The requested Video standard " & l_strRequirements(l_lngCounter) & " does not correspond to the logged file parameters." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
            Exit Sub
        End If
        If InStr(l_strRequirements(l_lngCounter), "50i") > 0 Then
            If Not (cmbFrameRate.Text = "25" And cmbInterlace = "Interlace") Then
                MsgBox "The requested Video framerate 50i does not correspond to the logged file parameters (25fps Interlaced)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "25p") > 0 Then
            If Not (cmbFrameRate.Text = "25" And cmbInterlace = "Progressive") Then
                MsgBox "The requested Video framerate 25p does not correspond to the logged file parameters (25fps Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "25i") > 0 Then
            If Not (cmbFrameRate.Text = "25" And cmbInterlace = "Interlace") Then
                MsgBox "The requested Video framerate 25i does not correspond to the logged file parameters (25jpf Interlaced." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "24p") > 0 Then
            If Not (cmbFrameRate.Text = "23.98" And cmbInterlace = "Progressive") And Not (cmbFrameRate.Text = "24" And cmbInterlace = "Progressive") Then
                MsgBox "The requested Video framerate 24p does not correspond to the logged file parameters (23.98fps or 24fps Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "29.97p") > 0 Then
            If Not (cmbFrameRate.Text = "29.97 NDF" And cmbInterlace = "Progressive") Then
                If Not (cmbFrameRate.Text = "29.97" And cmbInterlace = "Progressive") Then
                    MsgBox "The requested Video framerate 29.97p does not correspond to the logged file parameters (Should be 29.97fps NDF Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                    Exit Sub
                ElseIf (cmbFrameRate.Text = "29.97" And cmbInterlace = "Progressive") Then
                    If MsgBox("This material is Drop-Frame. DADC Spec says Non-Drop-Frame." & vbCrLf & "Do you want to allow this through anyway?", vbYesNo + vbDefaultButton2, "Drop-Frame Alert") = vbNo Then
                        MsgBox "The requested Video framerate 29.97p does not correspond to the logged file parameters (Should be 29.97fps Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                        Exit Sub
                    End If
                End If
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "59.94i") > 0 Then
            If Not (cmbFrameRate.Text = "29.97 NDF" And cmbInterlace = "Interlace") Then
                If Not (cmbFrameRate.Text = "29.97" And cmbInterlace = "Interlace") Then
                    MsgBox "The requested Video framerate 59.94i does not correspond to the logged file parameters (Should be 29.97fps Interlaced)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                    Exit Sub
                ElseIf (cmbFrameRate.Text = "29.97 NDF" And cmbInterlace = "Interlace") Then
                    If MsgBox("This material is Drop-Frame. DADC Spec says Non-Drop-Frame." & vbCrLf & "Do you want to allow this through anyway?", vbYesNo + vbDefaultButton2, "Drop-Frame Alert") = vbNo Then
                        MsgBox "The requested Video framerate 59.94i does not correspond to the logged file parameters (Should be 29.97fps Interlaced)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                        Exit Sub
                    End If
                End If
            End If
        End If
        If InStr(l_strRequirements(l_lngCounter), "UPRES") > 0 Then
            If InStr(cmbClipcodec.Text, "UPRES") <= 0 Then
                MsgBox "The requested Video should be an UPRES, but the codec has not been logged as UPRES." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        End If
        If InStr(l_strRequirements(l_lngCounter), "HDR") > 0 Then
            If InStr(txtColorSpace.Text, "HDR") <= 0 Then
                MsgBox "The requested Video should be HDR, but it doesn't seem to be." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        End If
        If InStr(l_strRequirements(l_lngCounter), "444") > 0 Then
            If InStr(cmbClipcodec.Text, "4444") <= 0 And InStr(cmbClipcodec.Text, "ap4x") <= 0 Then
                MsgBox "The requested Video should be 444, but it doesn't seem to be." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        End If
    ElseIf l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 17) = "VOD - BBCW ProRes" Then
        l_blnVOD = True
        If InStr(l_strRequirements(l_lngCounter), txtVertpixels.Text) > 0 Then
            'Do nothing
        ElseIf InStr(l_strRequirements(l_lngCounter), "PAL") >= 0 And txtVertpixels = "576" Then
            'do nothing
        Else
            MsgBox "The requested Video standard " & l_strRequirements(l_lngCounter) & " does not correspond to the logged file parameters." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
            Exit Sub
        End If
        If InStr(l_strRequirements(l_lngCounter), "25p") > 0 Then
            If cmbFrameRate.Text <> "25" And cmbInterlace <> "Progressive" Then
                MsgBox "The requested Video framerate 25p does not correspond to the logged file parameters (25fps Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "24p") > 0 Then
            If cmbFrameRate.Text <> "23.98" And cmbInterlace <> "Progressive" Then
                MsgBox "The requested Video framerate 24p does not correspond to the logged file parameters (23.98fps Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(l_strRequirements(l_lngCounter), "29.97p") > 0 Then
            If Not (cmbFrameRate.Text = "29.97 NDF" And cmbInterlace = "Progressive") Then
                MsgBox "The requested Video framerate 29.97p does not correspond to the logged file parameters (Should be 29.97fps NDF Progressive)." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        End If
        If InStr(l_strRequirements(l_lngCounter), "UPRES") > 0 Then
            If InStr(cmbClipcodec.Text, "UPRES") <= 0 Then
                MsgBox "The requested Video should be an UPRES, but the codec has not been logged as UPRES." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        End If
        If InStr(l_strRequirements(l_lngCounter), "HDR") > 0 Then
            If InStr(txtColorSpace.Text, "HDR") <= 0 Then
                MsgBox "The requested Video should be HDR, but it doesn't seem to be." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
                Exit Sub
            End If
        End If
    End If
Next

MakeDADCXMLFileRequest l_blnVOD

End Sub

Private Sub cmdDADCOutputAudioOnly_Click()

Dim l_strRequirements(9) As String, l_lngCounter As Long, l_strTemp As String, l_lngCharCount As Long, l_strTest As String, l_blnFound As Boolean

m_strHistory = "DADC BBC Audio Send"
cmdSave.Value = True
m_strHistory = ""

l_lngCounter = 0
l_strTemp = lblAudioConfiguration.Caption
If InStr(l_strTemp, vbCrLf) > 0 Then
    Do While InStr(l_strTemp, vbCrLf) > 0
        l_lngCharCount = InStr(l_strTemp, vbCrLf)
        l_strRequirements(l_lngCounter) = Left(l_strTemp, l_lngCharCount - 1)
        l_strTemp = Mid(l_strTemp, l_lngCharCount + 2)
        l_lngCounter = l_lngCounter + 1
    Loop
    l_strRequirements(l_lngCounter) = l_strTemp
Else
    l_strRequirements(0) = l_strTemp
End If

If cmbAudioType1.Text <> "" Then
    If cmbAudioType1.Text <> "Standard Stereo" And cmbAudioType1.Text <> "Stereo" And Left(cmbAudioType1.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType2.Text <> "" Then
    If cmbAudioType2.Text <> "Standard Stereo" And cmbAudioType2.Text <> "Stereo" And Left(cmbAudioType2.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType3.Text <> "" Then
    If cmbAudioType3.Text <> "Standard Stereo" And cmbAudioType3.Text <> "Stereo" And Left(cmbAudioType3.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType4.Text <> "" Then
    If cmbAudioType4.Text <> "Standard Stereo" And cmbAudioType4.Text <> "Stereo" And Left(cmbAudioType4.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType5.Text <> "" Then
    If cmbAudioType5.Text <> "Standard Stereo" And cmbAudioType5.Text <> "Stereo" And Left(cmbAudioType5.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType6.Text <> "" Then
    If cmbAudioType6.Text <> "Standard Stereo" And cmbAudioType6.Text <> "Stereo" And Left(cmbAudioType6.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType7.Text <> "" Then
    If cmbAudioType7.Text <> "Standard Stereo" And cmbAudioType7.Text <> "Stereo" And Left(cmbAudioType7.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If
If cmbAudioType8.Text <> "" Then
    If cmbAudioType8.Text <> "Standard Stereo" And cmbAudioType8.Text <> "Stereo" And Left(cmbAudioType8.Text, 3) <> "5.1" Then
        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
        Exit Sub
    End If
End If

For l_lngCounter = 0 To 9
    If l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) <> "BBCW ProRes" Then
        l_blnFound = False
        If cmbAudioContent1.Text = "M&E" Then
            l_strTest = cmbAudioType1.Text & ", M&&E"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Music" Or cmbAudioContent1.Text = "Effects" Then
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage1.Text, "(") Then
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & Left(cmbAudioLanguage1.Text, InStr(cmbAudioLanguage1.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & cmbAudioLanguage1.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent2.Text = "M&E" Then
            l_strTest = cmbAudioType2.Text & ", M&&E"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Music" Or cmbAudioContent2.Text = "Effects" Then
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage2.Text, "(") Then
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & Left(cmbAudioLanguage2.Text, InStr(cmbAudioLanguage2.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & cmbAudioLanguage2.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent3.Text = "M&E" Then
            l_strTest = cmbAudioType3.Text & ", M&&E"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Music" Or cmbAudioContent3.Text = "Effects" Then
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage3.Text, "(") Then
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & Left(cmbAudioLanguage3.Text, InStr(cmbAudioLanguage3.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & cmbAudioLanguage3.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent4.Text = "M&E" Then
            l_strTest = cmbAudioType4.Text & ", M&&E"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Music" Or cmbAudioContent4.Text = "Effects" Then
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage4.Text, "(") Then
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & Left(cmbAudioLanguage4.Text, InStr(cmbAudioLanguage4.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & cmbAudioLanguage4.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
                
        If cmbAudioContent5.Text = "M&E" Then
            l_strTest = cmbAudioType5.Text & ", M&&E"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Music" Or cmbAudioContent5.Text = "Effects" Then
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage5.Text, "(") Then
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & Left(cmbAudioLanguage5.Text, InStr(cmbAudioLanguage5.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & cmbAudioLanguage5.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent6.Text = "M&E" Then
            l_strTest = cmbAudioType6.Text & ", M&&E"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Music" Or cmbAudioContent6.Text = "Effects" Then
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage6.Text, "(") Then
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & Left(cmbAudioLanguage6.Text, InStr(cmbAudioLanguage6.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & cmbAudioLanguage6.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent7.Text = "M&E" Then
            l_strTest = cmbAudioType7.Text & ", M&&E"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Music" Or cmbAudioContent7.Text = "Effects" Then
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage7.Text, "(") Then
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & Left(cmbAudioLanguage7.Text, InStr(cmbAudioLanguage7.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & cmbAudioLanguage7.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If cmbAudioContent8.Text = "M&E" Then
            l_strTest = cmbAudioType8.Text & ", M&&E"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Music" Or cmbAudioContent8.Text = "Effects" Then
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf InStr(cmbAudioLanguage8.Text, "(") Then
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & Left(cmbAudioLanguage8.Text, InStr(cmbAudioLanguage8.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & cmbAudioLanguage8.Text
        End If
'        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
        
        If l_blnFound = False Then
            MsgBox "There was a listed item " & l_strRequirements(l_lngCounter) & " that has not been logged. Cannot make xml", vbCritical, "Error..."
            Exit Sub
        End If
    ElseIf l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) = "BBCW ProRes" Then
        MsgBox "You cannot do an audio only xml when it appears video items have also been requested." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
        Exit Sub
    End If
Next

MakeDADCAudioOnlyXMLFileRequest False

End Sub

Sub MakeDADCXMLFileRequest(lp_blnVOD As Boolean)

Dim l_strFileNameToSave As String, l_lngChannelCount As Long, l_strProgramStart As String, l_strProgramEnd As String, l_blnFoundProg As Boolean, l_blnFoundProgEnd As Boolean, l_strDescriptionAlias As String
Dim FSO As Scripting.FileSystemObject, l_strSQL As String
Dim l_strPathForMove As String, l_strFolderForFilerequest As String, l_strPathForCopy As String
Dim l_lngNewLibraryID As Long
Dim l_rsEmailToSend As ADODB.Recordset
Dim l_blnResend As Boolean
Dim l_lngXMLclipID As Long
Dim l_lngDADC_Archive_ID As Long
Dim l_lngDADC_Operations_ID As Long
Dim l_lngDADC_Hotfolders_ID As Long
Dim l_lngDADC_AutoDelete_ID As Long
Dim l_strProgStart As String
Dim l_lngTrackerID As Long

l_lngDADC_Archive_ID = GetData("Setting", "intvalue", "name", "DADC_Store_Archive_ID")
l_lngDADC_Operations_ID = GetData("Setting", "intvalue", "name", "DADC_Store_Operations_ID")
l_lngDADC_Hotfolders_ID = GetData("Setting", "intvalue", "name", "DADC_Store_HotFolders_ID")
l_lngDADC_AutoDelete_ID = GetData("Setting", "intvalue", "name", "DADC_Store_AutoDelete_ID")

If Val(Trim(" " & GetDataSQL("SELECT TOP 1 stagefield4 FROM tracker_dadc_item WHERE itemreference = '" & txtReference.Text & "' AND stagefield15 IS NULL"))) <> 0 Then
    MsgBox "You have already made the XML for this tracker item, and it is either in the queue to send or has already been sent.", vbCritical, "Error..."
    Exit Sub
Else
    l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT TOP 1 tracker_DADC_itemID FROM tracker_dadc_item WHERE itemreference = '" & txtReference.Text & "' AND stagefield15 IS NULL")))
    If l_lngTrackerID <> 0 Then
        If Trim(" " & GetData("tracker_dadc_item", "format", "tracker_dadc_itemID", l_lngTrackerID)) = "" Then
            SetData "tracker_dadc_item", "format", "tracker_dadc_itemID", l_lngTrackerID, "FILE"
        End If
    End If
End If

If cmbClipcodec.Text <> "ProRes HQ" And cmbClipcodec.Text <> "ProRes HQ UPRES" And cmbClipcodec.Text <> "ProRes 4444" And cmbClipcodec.Text <> "ProRes 4444 UPRES" Then
    MsgBox "File is not a ProRes masterfile - you cannot send this.", vbCritical, "Error..."
    Exit Sub
End If

l_blnFoundProg = False
adoLogging.Recordset.MoveFirst
Do While Not adoLogging.Recordset.EOF
    If adoLogging.Recordset("segmentreference") = "Programme" Then
        l_blnFoundProg = True
        l_strProgStart = adoLogging.Recordset("timecodestart")
        Exit Do
    End If
    adoLogging.Recordset.MoveNext
Loop

If l_blnFoundProg = False Then
    MsgBox "There is no 'Programme' item logged - you cannot send this.", vbCritical, "Error..."
    Exit Sub
Else
    If l_strProgStart <> "10:00:00:00" Then
        If MsgBox("Prog Start is not 10:00:00:00" & vbCrLf & "Do you want to send it anyway?", vbYesNo + vbDefaultButton2) = vbNo Then Exit Sub
    End If
End If

Select Case Val(lblLibraryID.Caption)
    Case 719952, 770131, 770132, 770133, 770134, 770135, 770653, 770041
        'Do nothing
    Case Else
        MsgBox "Masterfile is not on the the Correct Store. You cannot send this.", vbCritical, "Not on " & GetData("library", "barcode", "libraryID", l_lngDADC_Archive_ID)
        Exit Sub
End Select

If txtReference.Text <> "" And txtTimecodeStart.Text <> "" And txtTimecodeStop.Text <> "" And cmbFrameRate.Text <> "" And cmbIsTextInPicture.Text <> "" And Len(txtDuration.Text) = 11 _
And ((lp_blnVOD = False And adoLogging.Recordset.RecordCount >= 4) Or (lp_blnVOD = True And adoLogging.Recordset.RecordCount >= 2)) Then
    
    l_strSQL = "INSERT INTO Event_File_Request(eventID,  event_file_request_typeID, DADC_Send_XML, DADC_Is_Resend, DADC_VOD, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & txtClipID.Text & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DADC XML") & ", "
    If MsgBox("Do you wish to automatically send this item to the DADC DBB", vbYesNo + vbDefaultButton1, "Automatic Sending") = vbYes Then
        l_strSQL = l_strSQL & "1, "
    Else
        l_strSQL = l_strSQL & "0, "
    End If
    Select Case MsgBox("Is this a resend?", vbYesNoCancel + vbDefaultButton2, "Automatic Sending")
        Case vbYes
            l_strSQL = l_strSQL & "1, "
        Case vbNo
            l_strSQL = l_strSQL & "0, "
        Case Else
            MsgBox "Aborted Send"
            Exit Sub
    End Select
    l_strSQL = l_strSQL & IIf(lp_blnVOD = True, "1", "0") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    MsgBox "DADC XML File Request Successfullly Submitted."

Else
    
    If txtReference.Text = "" Then
        MsgBox "Essential information has not been completed - no Reference Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf txtTimecodeStart.Text = "" Then
        MsgBox "Essential information has not been completed - no Timecode Start Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf txtTimecodeStop.Text = "" Then
        MsgBox "Essential information has not been completed - no Timecode End Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf cmbFrameRate.Text = "" Then
        MsgBox "Essential information has not been completed - no Framerate Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf cmbIsTextInPicture.Text = "" Then
        MsgBox "Essential information has not been completed - no Text In Picture Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf Len(txtDuration.Text) <> 11 Then
        MsgBox "Essential information has not been completed - Duration field is not correctly entered.", vbCritical, "Cannot output DADC XML file"
    ElseIf adoLogging.Recordset.RecordCount < 4 Then
        MsgBox "Essential information has not been completed - Timecode Logging Information is not complete.", vbCritical, "Cannot output DADC XML file"
    ElseIf cmbAudioType1.Text = "" Or cmbAudioContent1.Text = "" Or cmbAudioLanguage1.Text = "" Then
        MsgBox "Essential information has not been completed - Audio Logging Information is not complete.", vbCritical, "Cannot output DADC XML file"
    End If

End If

End Sub

Sub MakeDADCAudioOnlyXMLFileRequest(lp_blnVOD As Boolean)

Dim l_strFileNameToSave As String, l_lngChannelCount As Long, l_strProgramStart As String, l_strProgramEnd As String, l_blnFoundProg As Boolean, l_blnFoundProgEnd As Boolean, l_strDescriptionAlias As String
Dim FSO As Scripting.FileSystemObject, l_strSQL As String
Dim l_strPathForMove As String, l_strFolderForFilerequest As String, l_strPathForCopy As String
Dim l_lngNewLibraryID As Long
Dim l_rsEmailToSend As ADODB.Recordset
Dim l_blnResend As Boolean
Dim l_lngXMLclipID As Long
Dim l_lngDADC_Archive_ID As Long
Dim l_lngDADC_Operations_ID As Long
Dim l_lngDADC_Hotfolders_ID As Long
Dim l_lngDADC_AutoDelete_ID As Long
Dim l_lngTrackerID As Long

l_lngDADC_Archive_ID = GetData("Setting", "intvalue", "name", "DADC_Store_Archive_ID")
l_lngDADC_Operations_ID = GetData("Setting", "intvalue", "name", "DADC_Store_Operations_ID")
l_lngDADC_Hotfolders_ID = GetData("Setting", "intvalue", "name", "DADC_Store_HotFolders_ID")
l_lngDADC_AutoDelete_ID = GetData("Setting", "intvalue", "name", "DADC_Store_AutoDelete_ID")

If Val(Trim(" " & GetDataSQL("SELECT TOP 1 stagefield4 FROM tracker_dadc_item WHERE itemreference = '" & txtReference.Text & "' AND stagefield15 IS NULL"))) <> 0 Then
    MsgBox "You have already made the XML for this tracker item, and it is either in the queue to send or has already been sent.", vbCritical, "Error..."
    Exit Sub
Else
    l_lngTrackerID = Val(Trim(" " & GetDataSQL("SELECT TOP 1 tracker_DADC_itemID FROM tracker_dadc_item WHERE itemreference = '" & txtReference.Text & "' AND stagefield15 IS NULL")))
    If l_lngTrackerID <> 0 Then
        If Trim(" " & GetData("tracker_dadc_item", "format", "tracker_dadc_itemID", l_lngTrackerID)) = "" Then
            SetData "tracker_dadc_item", "format", "tracker_dadc_itemID", l_lngTrackerID, "AUDIO"
        End If
    End If
End If

If Val(lblLibraryID.Caption) <> l_lngDADC_Archive_ID Then
    MsgBox "Masterfile is not on the the Correct Store. You cannot send this.", vbCritical, "Not on " & GetData("library", "barcode", "libraryID", l_lngDADC_Archive_ID)
    Exit Sub
End If

If txtReference.Text <> "" And cmbAudioType1.Text <> "" And cmbAudioContent1.Text <> "" And cmbAudioLanguage1.Text <> "" Then
    
    l_strSQL = "INSERT INTO Event_File_Request(eventID, event_file_request_typeID, DADC_Send_XML, DADC_Is_Resend, DADC_VOD, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & txtClipID.Text & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "DADC Audio XML") & ", "
    If MsgBox("Do you wish to automatically send this item to the DADC DBB", vbYesNo + vbDefaultButton1, "Automatic Sending") = vbYes Then
        l_strSQL = l_strSQL & "1, "
    Else
        l_strSQL = l_strSQL & "0, "
    End If
    Select Case MsgBox("Is this a resend?", vbYesNoCancel + vbDefaultButton2, "Automatic Sending")
        Case vbYes
            l_strSQL = l_strSQL & "1, "
        Case vbNo
            l_strSQL = l_strSQL & "0, "
        Case Else
            MsgBox "Aborted Send"
            Exit Sub
    End Select
    l_strSQL = l_strSQL & IIf(lp_blnVOD = True, "1", "0") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    
    Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    MsgBox "DADC XML File Request successfully submitted."

Else
    
    If txtReference.Text = "" Then
        MsgBox "Essential information has not been completed - no Reference Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf cmbAudioType1.Text = "" Or cmbAudioContent1.Text = "" Or cmbAudioLanguage1.Text = "" Then
        MsgBox "Essential information has not been completed - Audio Logging Information is not complete.", vbCritical, "Cannot output DADC XML file"
    End If

End If

End Sub

Sub MakeDADCAudioRARXMLFile(lp_blnVOD As Boolean)

Dim l_strFileNameToSave As String, l_lngChannelCount As Long, l_strProgramStart As String, l_strProgramEnd As String, l_blnFoundProg As Boolean, l_blnFoundProgEnd As Boolean, l_strDescriptionAlias As String
Dim FSO As Scripting.FileSystemObject, l_strSQL As String
Dim l_strPathForMove As String, l_strFolderForFilerequest As String, l_strPathForCopy As String
Dim l_lngNewLibraryID As Long
Dim l_rsEmailToSend As ADODB.Recordset
Dim l_blnResend As Boolean

'MsgBox "Please note Audio only Packages do not auto send. You will need to manually send them and update the tracker.", vbInformation

If lblLibraryID.Caption <> "447261" Then
    If MsgBox("Masterfile is not on the WARP. You will not be able to auto-send this file." & vbCrLf & "Do you wish to continue", vbYesNo + vbDefaultButton2, "Not on WARP") = vbNo Then
        Exit Sub
    End If
End If

On Error GoTo DADC_XML_ERROR4

If txtReference.Text <> "" And cmbAudioType1.Text <> "" And cmbAudioContent1.Text <> "" And cmbAudioLanguage1.Text <> "" Then
    
    MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
    MDIForm1.dlgMain.Filter = "XML files|*.xml"
    MDIForm1.dlgMain.Filename = txtReference.Text & ".xml"
    
    MDIForm1.dlgMain.ShowSave
    
    l_strFileNameToSave = MDIForm1.dlgMain.Filename
    
    If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
        
        Open l_strFileNameToSave For Output As 1
        Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
        Print #1, "<BulkIngestData xmlns=""http://schema.dadcdigital.com/dbb/data/2010/migration/"" xmlns:inv=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"
        WriteTabs 1
        Print #1, "<ExternalTaskID></ExternalTaskID>"
        WriteTabs 1
        Print #1, "<ExternalPONumber></ExternalPONumber>"
        WriteTabs 1
        Print #1, "<ExternalDescription></ExternalDescription>"
        WriteTabs 1
        If Trim(" " & GetData("tracker_dadc_item", "externalalphaID", "itemreference", txtReference.Text)) = "" Then
            Print #1, "<ExternalAlphaID>" & GetData("tracker_dadc_item", "contentversioncode", "itemreference", txtReference.Text) & "</ExternalAlphaID>"
        Else
            Print #1, "<ExternalAlphaID>" & GetData("tracker_dadc_item", "externalalphaID", "itemreference", txtReference.Text) & "</ExternalAlphaID>"
        End If
        WriteTabs 1
        
        'Then an Audio section for each Audio set logged group
        Print #1, "<Components>"
        l_lngChannelCount = 0
        
        If cmbAudioType1.Text <> "" Then
            
            If cmbAudioType1.Text = "Standard Stereo" Or cmbAudioType1.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType1.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent1.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage1.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent1.Text, cmbAudioType1.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType1.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType1.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent1.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage1.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent1.Text, cmbAudioType1.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType1.Text = "Mono" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType1.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent1.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage1.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent1.Text, cmbAudioType1.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        If cmbAudioType2.Text <> "" Then
            
            If cmbAudioType2.Text = "Standard Stereo" Or cmbAudioType2.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType2.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent2.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage2.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent2.Text, cmbAudioType2.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType2.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType2.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent2.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage2.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent2.Text, cmbAudioType2.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType2.Text = "Mono" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType2.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent2.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage2.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent2.Text, cmbAudioType2.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
                        
        End If
        
        If cmbAudioType3.Text <> "" Then
            
            If cmbAudioType3.Text = "Standard Stereo" Or cmbAudioType3.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType3.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent3.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage3.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent3.Text, cmbAudioType3.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType3.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType3.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent3.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage3.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent3.Text, cmbAudioType3.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType3.Text = "Mono" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType3.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent3.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage3.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent3.Text, cmbAudioType3.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        If cmbAudioType4.Text <> "" Then
            
            If cmbAudioType4.Text = "Standard Stereo" Or cmbAudioType4.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType4.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent4.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage4.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent4.Text, cmbAudioType4.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType4.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType4.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent4.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage4.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent4.Text, cmbAudioType4.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType4.Text = "Mono" And cmbClipformat.Text = "BWAV" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType4.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent4.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage4.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent4.Text, cmbAudioType4.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        If cmbAudioType5.Text <> "" Then
            
            If cmbAudioType5.Text = "Standard Stereo" Or cmbAudioType5.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType5.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent5.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage5.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent5.Text, cmbAudioType5.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType5.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType5.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent5.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage5.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent5.Text, cmbAudioType5.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType5.Text = "Mono" And cmbClipformat.Text = "BWAV" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType5.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent5.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage5.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent5.Text, cmbAudioType5.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        If cmbAudioType6.Text <> "" Then
            
            If cmbAudioType6.Text = "Standard Stereo" Or cmbAudioType6.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType6.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent6.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage6.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent6.Text, cmbAudioType6.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType6.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType6.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent6.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage6.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent6.Text, cmbAudioType6.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType6.Text = "Mono" And cmbClipformat.Text = "BWAV" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType6.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent6.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage6.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent6.Text, cmbAudioType6.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        If cmbAudioType7.Text <> "" Then
            
            If cmbAudioType7.Text = "Standard Stereo" Or cmbAudioType7.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType7.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent7.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage7.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent7.Text, cmbAudioType7.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType7.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType7.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent7.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage7.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent7.Text, cmbAudioType7.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType7.Text = "Mono" And cmbClipformat.Text = "BWAV" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType7.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent7.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage7.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent7.Text, cmbAudioType7.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        If cmbAudioType8.Text <> "" Then
            
            If cmbAudioType8.Text = "Standard Stereo" Or cmbAudioType8.Text = "Mono 2.0" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType8.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent8.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage8.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "2track", l_lngChannelCount, lp_blnVOD, cmbAudioContent8.Text, cmbAudioType8.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            ElseIf cmbAudioType8.Text = "5.1" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType8.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent8.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage8.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "6track", l_lngChannelCount, lp_blnVOD, cmbAudioContent8.Text, cmbAudioType8.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                                
            ElseIf cmbAudioType8.Text = "Mono" And cmbClipformat.Text = "BWAV" Then
            
                WriteTabs 2
                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
                WriteTabs 3
                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioType8.Text & "</inv:AudioConfiguration>"
                WriteTabs 3
                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(cmbAudioContent8.Text) & "</inv:AudioContent>"
                WriteTabs 3
                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & cmbAudioLanguage8.Text & "</inv:Language>"
                WriteTabs 3
                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
                            
                WriteAudioChannelComponents 4, "1track", l_lngChannelCount, lp_blnVOD, cmbAudioContent8.Text, cmbAudioType8.Text
                WriteTabs 3
                Print #1, "</inv:AudioChannelComponents>"
                WriteTabs 2
                Print #1, "</Component>"
                
            Else
            
                MsgBox "Not equipped to do anything but Stereo, 5.1, Mono 2.0 and Mono yet for BBC audio.", vbCritical, "Cannot complete XML"
            
            End If
            
        End If
        
        WriteTabs 1
        Print #1, "</Components>"
        
        'Then close it all off
        Print #1, "</BulkIngestData>"

        Close #1
        
        If g_intUseSvenskAutoSend <> 0 And lblLibraryID.Caption = "447261" Then
        
            'Potentially?, if you get to this point, you coulkd then go ahead and 'Send' the files to DADC BBC DBB.
            'That would involve finding out whether it was a 'Send' or a 'Re-send', then copying this bunch of files in to the BBC Hot folder
            'Then also 'Moving' the files to the 'Sent' Archive folder
            
            If MsgBox("Do you wish to automatically send these items to DADC BBC DBB via Aspera", vbYesNo, "Automatic Sending") = vbYes Then
                On Error GoTo SENDING_ERROR
                Set FSO = New Scripting.FileSystemObject
                l_strPathForMove = GetData("library", "subtitle", "barcode", "WARP") & "\1261\04_FILES_TO_BE_MOVED"
'                l_strPathForMove = GetData("library", "subtitle", "barcode", "VIE-MS-1") & "\1261\04_FILES_TO_BE_MOVED"
                l_strFolderForFilerequest = Format(Now, "YYYY_MM_DD")
                If MsgBox("Is this a resend?", vbYesNo, "Automatic Sending") = vbYes Then
                    l_blnResend = True
                    Set l_rsEmailToSend = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 57", g_strExecuteError)
                    If l_rsEmailToSend.RecordCount > 0 Then
                        l_rsEmailToSend.MoveFirst
                        Do While Not l_rsEmailToSend.EOF
                            SendSMTPMail l_rsEmailToSend("email"), l_rsEmailToSend("fullname"), "DADC Resend Initiated", "", "Please note the following file has been sent to a resend folder:" & vbCrLf & "CV Code: " & _
                            GetData("tracker_dadc_item", "contentversioncode", "itemreference", txtReference.Text) & vbCrLf & "Filename: " & txtClipfilename.Text & vbCrLf & _
                            "Item Title: " & txtTitle.Text & vbCrLf & "Item Folder: " & l_strFolderForFilerequest & vbCrLf, False, "", "", g_strAdministratorEmailAddress, False, g_strUserEmailAddress
                            l_rsEmailToSend.MoveNext
                        Loop
                    End If
                    l_rsEmailToSend.Close
                Else
                    l_blnResend = False
                End If
                If FSO.FolderExists(l_strPathForMove) Then
                    If Not FSO.FolderExists(l_strPathForMove & "\" & l_strFolderForFilerequest) Then
                        FSO.CreateFolder l_strPathForMove & "\" & l_strFolderForFilerequest
                        If Not FSO.FolderExists(l_strPathForMove & "\" & l_strFolderForFilerequest) Then
                            MsgBox "Could not make the delivery archive folder on Warp", vbCritical, "Error"
                            Exit Sub
                        End If
                    End If
                Else
                    MsgBox "Could not find the Send delivery archive folder on Warp", vbCritical, "Error"
                    Exit Sub
                End If
                If l_blnResend = False Then
                    If g_intUseDADCFlipFlop <> 0 Then
                        If Val(GetCETASetting("BBC_Hotfolder_Flip")) = 1 Then
                            l_strPathForCopy = GetData("library", "subtitle", "barcode", "BBC-HOTFOLDER-1") & "\Aspera-Hotfolder"
                            l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "BBC-HOTFOLDER-1")
                            SaveCETASetting "BBC_Hotfolder_Flip", 2
                        Else
                            l_strPathForCopy = GetData("library", "subtitle", "barcode", "BBC-HOTFOLDER-2") & "\Aspera-Hotfolder"
                            l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "BBC-HOTFOLDER-2")
                            SaveCETASetting "BBC_Hotfolder_Flip", 1
                        End If
                    Else
                        l_strPathForCopy = GetData("library", "subtitle", "barcode", "BBC-HOTFOLDER-1") & "\Aspera-Hotfolder"
                        l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "BBC-HOTFOLDER-1")
                    End If
                Else
                    l_strPathForCopy = GetData("library", "subtitle", "barcode", "BBC-HOTFOLDER-RESEND") & "\Aspera-Hotfolder"
                    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "BBC-HOTFOLDER-RESEND")
                End If
                If FSO.FolderExists(l_strPathForCopy) Then
                    If Not FSO.FolderExists(l_strPathForCopy & "\" & l_strFolderForFilerequest) Then
                        FSO.CreateFolder l_strPathForCopy & "\" & l_strFolderForFilerequest
                        If Not FSO.FolderExists(l_strPathForCopy & "\" & l_strFolderForFilerequest) Then
                            MsgBox "Could not make the delivery folder on BBC-HOTFOLDER", vbCritical, "Error"
                            Exit Sub
                        End If
                    End If
                Else
                    MsgBox "Could not find the Aspera HotFolder on BBC-HOTFOLDER", vbCritical, "Error"
                    Exit Sub
                End If
                'Copy to Hot Folder - send the xml first - l_strFilenametosave and then move to archive folder.
                If l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml" <> l_strFileNameToSave Then
                    If FSO.FileExists(l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml") Then FSO.DeleteFile l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml", True
                    FSO.MoveFile l_strFileNameToSave, l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml"
                End If
                FSO.CopyFile l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml", l_strPathForCopy & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml"
                'Then so the actual asset
                'the request to move it to the 'To be moved' folder
                If (lblLibraryID.Caption <> GetData("library", "libraryID", "barcode", "WARP")) Or (lblLibraryID.Caption = GetData("library", "libraryID", "barcode", "WARP") And txtAltFolder.Text <> "1261\04_FILES_TO_BE_MOVED\" & l_strFolderForFilerequest) Then
                    On Error GoTo CONTINUE_ROUTINE
                    l_strFileNameToSave = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
                    l_strFileNameToSave = l_strFileNameToSave & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text
                    If LCase(l_strFileNameToSave) <> LCase(GetData("library", "subtitle", "barcode", "WARP") & "\1261\04_FILES_TO_BE_MOVED\" & l_strFolderForFilerequest & "\" & txtClipfilename.Text) Then
                        FSO.MoveFile l_strFileNameToSave, GetData("library", "subtitle", "barcode", "WARP") & "\1261\04_FILES_TO_BE_MOVED\" & l_strFolderForFilerequest & "\" & txtClipfilename.Text
                        SetData "events", "libraryID", "eventID", txtClipID.Text, GetData("library", "libraryID", "barcode", "WARP")
                        SetData "events", "altlocation", "eventID", txtClipID.Text, "1261\04_FILES_TO_BE_MOVED\" & l_strFolderForFilerequest
                    End If
CONTINUE_ROUTINE:
                End If
                'the copy to the Hot Folder
                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
                l_strSQL = l_strSQL & txtClipID.Text & ", "
                l_strSQL = l_strSQL & lblLibraryID.Caption & ", "
                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
                l_strSQL = l_strSQL & "'Aspera-Hotfolder\" & l_strFolderForFilerequest & "', "
                l_strSQL = l_strSQL & "1, '" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strSQL = "UPDATE tracker_dadc_item SET stagefield3 = '" & FormatSQLDate(Now) & "' WHERE itemreference = '" & txtReference.Text & "' AND readytobill = 0;"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strSQL = "UPDATE tracker_dadc_item SET stagefield4 = '" & FormatSQLDate(Now) & "' WHERE itemreference = '" & txtReference.Text & "' AND readytobill = 0;"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strSQL = "UPDATE tracker_dadc_item SET xmlmadeby = '" & g_strUserInitials & "' WHERE itemreference = '" & txtReference.Text & "' AND readytobill = 0;"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                MsgBox "Done"
            End If
        End If
    
    End If

Else
    
    If txtReference.Text = "" Then
        MsgBox "Essential information has not been completed - no Reference Entry.", vbCritical, "Cannot output DADC XML file"
    ElseIf cmbAudioType1.Text = "" Or cmbAudioContent1.Text = "" Or cmbAudioLanguage1.Text = "" Then
        MsgBox "Essential information has not been completed - Audio Logging Information is not complete.", vbCritical, "Cannot output DADC XML file"
    End If

End If


Exit Sub

DADC_XML_ERROR4:

MsgBox "Error: " & Err.Number & ", " & Err.Description, vbCritical, "An Error occured."

ABORT_XML:
Close #1
Set FSO = New Scripting.FileSystemObject
FSO.DeleteFile l_strFileNameToSave
Exit Sub

SENDING_ERROR:
MsgBox "There was a problem doing the autosend - it has probably not happened." & vbCrLf & "Please check and send the items manually if necessary.", vbCritical

End Sub

Sub WriteBulkAudioChannelComponents(lp_intTabCount As Integer, lp_strType As String, ByRef lp_lngChannelCount As Long, lp_blnVOD As Boolean, Optional lp_strAudioContent As String, Optional lp_strAudioType As String)

If lp_strType = "2track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
'    If lp_strAudioContent = "MOS" Or lp_strAudioType = "Mono 2.0" Or lp_strAudioType = "Dolby E" Then
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 1</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
'   If lp_strAudioContent = "MOS" Or lp_strAudioType = "Mono 2.0" Or lp_strAudioType = "Dolby E" Then Dolby E 1
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 2</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 2

ElseIf lp_strType = "1track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 1

ElseIf lp_strType = "6track" Then

    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 3 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 3 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Front Center</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 4 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 4 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>LFE</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 5 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 5 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 6 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 6 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 6

End If

End Sub

Sub WriteAudioChannelComponents(lp_intTabCount As Integer, lp_strType As String, ByRef lp_lngChannelCount As Long, lp_blnVOD As Boolean, Optional lp_strAudioContent As String, Optional lp_strAudioType As String)

If lp_strType = "2track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
'    If lp_strAudioContent = "MOS" Or lp_strAudioType = "Mono 2.0" Or lp_strAudioType = "Dolby E" Then
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 1</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
'   If lp_strAudioContent = "MOS" Or lp_strAudioType = "Mono 2.0" Or lp_strAudioType = "Dolby E" Then Dolby E 1
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 2</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 2

ElseIf lp_strType = "1track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 1

ElseIf lp_strType = "6track" Then

    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 3 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 3 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Front Center</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 4 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 4 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>LFE</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 5 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 5 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteFileDescriptor lp_blnVOD, lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 6 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 6 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>1</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 6

End If

End Sub
Sub WriteBulkAudioFileDescriptor(lp_intTabCount As Integer, Optional lp_strAudioType As String)

WriteTabs (lp_intTabCount)
Print #1, "<inv:FileDescriptors xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
WriteTabs (lp_intTabCount + 1)
Print #1, "<inv:FileDescriptor>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:FileName>" & txtClipfilename.Text & "</inv:FileName>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingEquipment>Final Cut Pro</inv:EncodingEquipment>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingProfile>";
Select Case Val(txtVertpixels.Text)
    Case 576
        Print #1, "BBCW ProRes PAL ";
        If cmbAspect.Text = "16:9" Then
            Print #1, "16x9";
        Else
            Print #1, "4x3";
        End If
    Case 486
        Print #1, "BBCW ProRes NTSC ";
        If cmbAspect.Text = "16:9" Then
            Print #1, "16x9";
        Else
            Print #1, "4x3";
        End If
    Case 1080
        Print #1, "BBCW ProRes ";
        If cmbFrameRate.Text = "23.98" Then
            Print #1, "1080 24p ";
        ElseIf cmbFrameRate.Text = "29.97" And cmbInterlace.Text = "Progressive" Then
            Print #1, "1080 29.97p ";
        ElseIf cmbFrameRate.Text = "29.97" And cmbInterlace.Text = "Interlace" Then
            Print #1, "1080 59.94i ";
        ElseIf cmbFrameRate.Text = "25" And cmbInterlace.Text = "Progressive" Then
            Print #1, "1080 25p ";
        ElseIf cmbFrameRate.Text = "25" And cmbInterlace.Text = "Interlace" Then
            Print #1, "1080 50i ";
        End If
        Print #1, "16x9";
    Case 2160
        Print #1, "BBCW ProRes UHD 25p"
    Case Else
        'Audio only file...
        If cmbClipformat.Text = "BWAV" Then
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Single Channel Mono BWAV PCM";
            End If
        ElseIf cmbClipformat.Text = "RAR Archive" Then
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV RAR";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV RAR";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Mono BWAV RAR";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Track Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete Track 5.1 MOV PCM";
            End If
        End If
End Select
Print #1, "</inv:EncodingProfile>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumValue></inv:ChecksumValue>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumType></inv:ChecksumType>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<FileFormat></FileFormat>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeDate>" & Format(GetData("tracker_dadc_item", "stagefield2", "itemreference", txtReference.Text), "YYYY-MM-DDTHH:NN:SS") & "</inv:EncodeDate>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeVendorOrganizationName>VDMS-London</inv:EncodeVendorOrganizationName>"
WriteTabs (lp_intTabCount + 1)
Print #1, "</inv:FileDescriptor>"
WriteTabs (lp_intTabCount)
Print #1, "</inv:FileDescriptors>"

End Sub
Sub WriteFileDescriptor(lp_blnVOD As Boolean, lp_intTabCount As Integer, Optional lp_strAudioType As String, Optional lp_strRequirements As String)
 
WriteTabs (lp_intTabCount)
Print #1, "<inv:FileDescriptors xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
WriteTabs (lp_intTabCount + 1)
Print #1, "<inv:FileDescriptor>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:FileName>" & txtClipfilename.Text & "</inv:FileName>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingEquipment></inv:EncodingEquipment>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingProfile>";
Select Case Val(txtVertpixels.Text)
    Case 576
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes PAL ";
            If cmbAspect.Text = "16:9" Then
                Print #1, "16x9";
            Else
                Print #1, "4x3";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case 486
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes NTSC ";
            If cmbAspect.Text = "16:9" Then
                Print #1, "16x9";
            Else
                Print #1, "4x3";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case 1080
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes ";
            If InStr(lp_strRequirements, "3D Left") > 0 Then
                Print #1, "3D Left ";
            ElseIf InStr(lp_strRequirements, "3D Right") > 0 Then
                Print #1, "3D Right ";
            End If
            If cmbFrameRate.Text = "23.98" Then
                Print #1, "1080 24p ";
            ElseIf cmbFrameRate.Text = "29.97" And cmbInterlace.Text = "Progressive" Then
                Print #1, "1080 29.97p ";
            ElseIf cmbFrameRate.Text = "29.97" And cmbInterlace.Text = "Interlace" Then
                Print #1, "1080 59.94i ";
            ElseIf cmbFrameRate.Text = "25" And cmbInterlace.Text = "Progressive" Then
                Print #1, "1080 25p ";
            ElseIf cmbFrameRate.Text = "25" And cmbInterlace.Text = "Interlace" Then
                Print #1, "1080 50i ";
            End If
            Print #1, "16x9";
        Else
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case 2160
        If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
            If lp_blnVOD = True Then Print #1, "VOD - ";
            Print #1, "BBCW ProRes UHD ";
            If cmbFrameRate.Text = "29.97" Then
                Print #1, "29.97p";
            ElseIf cmbFrameRate.Text = "25" Then
                Print #1, "25p";
            End If
            If Right(txtColorSpace, 3) = "HDR" Then
                Print #1, " HDR";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Muxed Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Muxed 5.1 MOV PCM";
            ElseIf lp_strAudioType = "Mono 2.0" Then
                Print #1, "BBCW Muxed Mono 2.0 MOV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Single Channel Muxed Mono MOV PCM";
            ElseIf Left(lp_strAudioType, 7) = "Dolby E" Then
                Print #1, "BBCW Muxed DolbyE MOV";
            End If
        End If
    Case Else
        'Audio only file...
        If cmbClipformat.Text = "BWAV" Then
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Single Channel Mono BWAV PCM";
            End If
        ElseIf Left(cmbClipformat.Text, 3) = "RAR" Then
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV RAR";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV RAR";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Mono BWAV RAR";
            End If
        Else
            If InStr(lblAudioConfiguration.Caption, "Multiple") > 0 Then
                If lp_strAudioType = "Standard Stereo" Then
                    Print #1, "BBCW Discrete Multiple Tracks Stereo MOV PCM";
                ElseIf lp_strAudioType = "5.1" Then
                    Print #1, "BBCW Discrete Multiple Tracks 5.1 MOV PCM";
                ElseIf lp_strAudioType = "Mono 2.0" Then
                    Print #1, "BBCW Discrete Multiple Tracks Mono 2.0 MOV PCM";
                ElseIf lp_strAudioType = "Mono" Then
                    Print #1, "BBCW Discrete Multiple Tracks Single Channel Mono MOV PCM";
                End If
            Else
                If lp_strAudioType = "Standard Stereo" Then
                    Print #1, "BBCW Discrete Track Stereo MOV PCM";
                ElseIf lp_strAudioType = "5.1" Then
                    Print #1, "BBCW Discrete Track 5.1 MOV PCM";
                ElseIf lp_strAudioType = "Mono 2.0" Then
                    Print #1, "BBCW Discrete Track Mono 2.0 MOV PCM";
                ElseIf lp_strAudioType = "Mono" Then
                    Print #1, "BBCW Discrete Track Single Channel Mono MOV PCM";
                End If
            End If
        End If
End Select
If IsEmpty(lp_strAudioType) Or lp_strAudioType = "" Then
    If InStr(cmbClipcodec.Text, "UPRES") > 0 Then Print #1, " UPRES";
End If
Print #1, "</inv:EncodingProfile>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumValue></inv:ChecksumValue>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumType></inv:ChecksumType>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:FileFormat></inv:FileFormat>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeDate>" & Format(GetData("tracker_dadc_item", "stagefield2", "itemreference", txtReference.Text), "YYYY-MM-DDTHH:NN:SS") & "</inv:EncodeDate>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeVendorOrganizationName>VDMS - London</inv:EncodeVendorOrganizationName>"
WriteTabs (lp_intTabCount + 1)
Print #1, "</inv:FileDescriptor>"
WriteTabs (lp_intTabCount)
Print #1, "</inv:FileDescriptors>"

End Sub

Private Sub WriteFileDescriptorAudio(lp_intTabCount As Integer, Optional lp_strAudioType As String)

WriteTabs (lp_intTabCount)
Print #1, "<FileDescriptors xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
WriteTabs (lp_intTabCount + 1)
Print #1, "<FileDescriptor>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<FileName>" & txtClipfilename.Text & "</FileName>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<EncodingEquipment></EncodingEquipment>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<EncodingProfile>";
Select Case Val(txtVertpixels.Text)
    Case 576
        Print #1, "BBCW ProRes PAL ";
        If cmbAspect.Text = "16:9" Then
            Print #1, "16x9";
        Else
            Print #1, "4x3";
        End If
    Case 486
        Print #1, "BBCW ProRes NTSC ";
        If cmbAspect.Text = "16:9" Then
            Print #1, "16x9";
        Else
            Print #1, "4x3";
        End If
    Case 1080
        Print #1, "BBCW ProRes ";
        If cmbFrameRate.Text = "23.98" Then
            Print #1, "1080 24p ";
        ElseIf cmbFrameRate.Text = "29.97" And cmbInterlace.Text = "Progressive" Then
            Print #1, "1080 29.97p ";
        ElseIf cmbFrameRate.Text = "29.97" And cmbInterlace.Text = "Interlace" Then
            Print #1, "1080 59.94i ";
        ElseIf cmbFrameRate.Text = "25" And cmbInterlace.Text = "Progressive" Then
            Print #1, "1080 25p ";
        ElseIf cmbFrameRate.Text = "25" And cmbInterlace.Text = "Interlace" Then
            Print #1, "1080 50i ";
        End If
        Print #1, "16x9";
    Case 2160
        Print #1, "BBCW ProRes UHD 25p"
    Case Else
        'Audio only file...
        If cmbClipformat.Text = "BWAV" Then
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Stereo BWAV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete 5.1 BWAV PCM";
            ElseIf lp_strAudioType = "Mono" Then
                Print #1, "BBCW Discrete Single Channel Mono BWAV PCM";
            End If
        Else
            If lp_strAudioType = "Standard Stereo" Then
                Print #1, "BBCW Discrete Track Stereo MOV PCM";
            ElseIf lp_strAudioType = "5.1" Then
                Print #1, "BBCW Discrete Track 5.1 MOV PCM";
            End If
        End If
End Select
Print #1, "</EncodingProfile>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<ChecksumValue></ChecksumValue>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<ChecksumType></ChecksumType>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<FileFormat></FileFormat>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<EncodeDate>" & Format(GetData("tracker_dadc_item", "stagefield2", "itemreference", txtReference.Text), "YYYY-MM-DDTHH:NN:SS") & "</EncodeDate>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<EncodeVendorOrganizationName>MX1</EncodeVendorOrganizationName>"
WriteTabs (lp_intTabCount + 1)
Print #1, "</FileDescriptor>"
WriteTabs (lp_intTabCount)
Print #1, "</FileDescriptors>"

End Sub
Private Sub WriteTabs(lp_intTabCount As Integer)

Dim Count As Integer

For Count = 1 To lp_intTabCount

    Print #1, Chr(9);

Next

End Sub

'Private Sub cmdDADCOutputBulkAudioOnly_Click()
'
'Dim l_strRequirements(8) As String, l_lngCounter As Long, l_strTemp As String, l_lngCharCount As Long, l_strTest As String, l_strTest2 As String, l_blnFound As Boolean
'
'm_strHistory = "DADC BBC Bulk Ausio XML"
'cmdSave.Value = True
'm_strHistory = ""
'
'l_lngCounter = 0
'l_strTemp = lblAudioConfiguration.Caption
'If InStr(l_strTemp, vbCrLf) > 0 Then
'    Do While InStr(l_strTemp, vbCrLf) > 0
'        l_lngCharCount = InStr(l_strTemp, vbCrLf)
'        l_strRequirements(l_lngCounter) = Left(l_strTemp, l_lngCharCount - 1)
'        l_strTemp = Mid(l_strTemp, l_lngCharCount + 2)
'        l_lngCounter = l_lngCounter + 1
'    Loop
'    l_strRequirements(l_lngCounter) = l_strTemp
'Else
'    l_strRequirements(0) = l_strTemp
'End If
'
'If cmbAudioType1.Text <> "" Then
'    If cmbAudioType1.Text <> "Standard Stereo" And Left(cmbAudioType1.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType2.Text <> "" Then
'    If cmbAudioType2.Text <> "Standard Stereo" And Left(cmbAudioType2.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType3.Text <> "" Then
'    If cmbAudioType3.Text <> "Standard Stereo" And Left(cmbAudioType3.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType4.Text <> "" Then
'    If cmbAudioType4.Text <> "Standard Stereo" And Left(cmbAudioType4.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType5.Text <> "" Then
'    If cmbAudioType5.Text <> "Standard Stereo" And Left(cmbAudioType5.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType6.Text <> "" Then
'    If cmbAudioType6.Text <> "Standard Stereo" And Left(cmbAudioType6.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType7.Text <> "" Then
'    If cmbAudioType7.Text <> "Standard Stereo" And Left(cmbAudioType7.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType8.Text <> "" Then
'    If cmbAudioType8.Text <> "Standard Stereo" And Left(cmbAudioType8.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'
'For l_lngCounter = 0 To 8
'    If l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) <> "BBCW ProRes" Then
'        l_blnFound = False
'        If cmbAudioContent1.Text = "M&E" Then
'            l_strTest = cmbAudioType1.Text & ", M&&E"
'            If cmbAudioLanguage1.Text <> "None" Then
'                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent1.Text = "Music" Or cmbAudioContent1.Text = "Effects" Then
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text
'            If cmbAudioLanguage1.Text <> "None" Then
'                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage1.Text, "(") Then
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & Left(cmbAudioLanguage1.Text, InStr(cmbAudioLanguage1.Text, "(") - 2)
'            l_strTest2 = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & cmbAudioLanguage1.Text
'        Else
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & cmbAudioLanguage1.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent2.Text = "M&E" Then
'            l_strTest = cmbAudioType2.Text & ", M&&E"
'            If cmbAudioLanguage2.Text <> "None" Then
'                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent2.Text = "Music" Or cmbAudioContent2.Text = "Effects" Then
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text
'            If cmbAudioLanguage2.Text <> "None" Then
'                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage2.Text, "(") Then
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & Left(cmbAudioLanguage2.Text, InStr(cmbAudioLanguage2.Text, "(") - 2)
'            l_strTest2 = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & cmbAudioLanguage2.Text
'        Else
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & cmbAudioLanguage2.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent3.Text = "M&E" Then
'            l_strTest = cmbAudioType3.Text & ", M&&E"
'            If cmbAudioLanguage3.Text <> "None" Then
'                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent3.Text = "Music" Or cmbAudioContent3.Text = "Effects" Then
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text
'            If cmbAudioLanguage3.Text <> "None" Then
'                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage3.Text, "(") Then
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & Left(cmbAudioLanguage3.Text, InStr(cmbAudioLanguage3.Text, "(") - 2)
'            l_strTest2 = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & cmbAudioLanguage3.Text
'        Else
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & cmbAudioLanguage3.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent4.Text = "M&E" Then
'            l_strTest = cmbAudioType4.Text & ", M&&E"
'            If cmbAudioLanguage4.Text <> "None" Then
'                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent4.Text = "Music" Or cmbAudioContent4.Text = "Effects" Then
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text
'            If cmbAudioLanguage4.Text <> "None" Then
'                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage4.Text, "(") Then
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & Left(cmbAudioLanguage4.Text, InStr(cmbAudioLanguage4.Text, "(") - 2)
'            l_strTest2 = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & cmbAudioLanguage4.Text
'        Else
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & cmbAudioLanguage4.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent5.Text = "M&E" Then
'            l_strTest = cmbAudioType5.Text & ", M&&E"
'            If cmbAudioLanguage5.Text <> "None" Then
'                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent5.Text = "Music" Or cmbAudioContent5.Text = "Effects" Then
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text
'            If cmbAudioLanguage5.Text <> "None" Then
'                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage5.Text, "(") Then
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & Left(cmbAudioLanguage5.Text, InStr(cmbAudioLanguage5.Text, "(") - 2)
'            l_strTest2 = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & cmbAudioLanguage5.Text
'        Else
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & cmbAudioLanguage5.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent6.Text = "M&E" Then
'            l_strTest = cmbAudioType6.Text & ", M&&E"
'            If cmbAudioLanguage6.Text <> "None" Then
'                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent6.Text = "Music" Or cmbAudioContent6.Text = "Effects" Then
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text
'            If cmbAudioLanguage6.Text <> "None" Then
'                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage6.Text, "(") Then
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & Left(cmbAudioLanguage6.Text, InStr(cmbAudioLanguage6.Text, "(") - 2)
'            l_strTest2 = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & cmbAudioLanguage6.Text
'        Else
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & cmbAudioLanguage6.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent7.Text = "M&E" Then
'            l_strTest = cmbAudioType7.Text & ", M&&E"
'            If cmbAudioLanguage7.Text <> "None" Then
'                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent7.Text = "Music" Or cmbAudioContent7.Text = "Effects" Then
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text
'            If cmbAudioLanguage7.Text <> "None" Then
'                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage7.Text, "(") Then
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & Left(cmbAudioLanguage7.Text, InStr(cmbAudioLanguage7.Text, "(") - 2)
'            l_strTest2 = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & cmbAudioLanguage7.Text
'        Else
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & cmbAudioLanguage7.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent8.Text = "M&E" Then
'            l_strTest = cmbAudioType8.Text & ", M&&E"
'            If cmbAudioLanguage8.Text <> "None" Then
'                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent8.Text = "Music" Or cmbAudioContent8.Text = "Effects" Then
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text
'            If cmbAudioLanguage8.Text <> "None" Then
'                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage8.Text, "(") Then
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & Left(cmbAudioLanguage8.Text, InStr(cmbAudioLanguage8.Text, "(") - 2)
'            l_strTest2 = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & cmbAudioLanguage8.Text
'        Else
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & cmbAudioLanguage8.Text
'        End If
'        If (l_strTest = l_strRequirements(l_lngCounter) Or l_strTest2 = l_strRequirements(l_lngCounter)) And l_blnFound = False Then
'            l_blnFound = True
'        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If l_blnFound = False Then
'            MsgBox "There was a listed item " & l_strRequirements(l_lngCounter) & " that has not been logged. Cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'    ElseIf l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) = "BBCW ProRes" Then
'        MsgBox "You cannot do an audio only xml when it appears video items have also been requested." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
'        Exit Sub
'    End If
'Next
'MakeDADCBulkAudioOnlyXMLFile (False)
'
'End Sub
'
'Private Sub cmdDADCRARWithoutXML_Click()
'
'Dim l_strRequirements(8) As String, l_lngCounter As Long, l_strTemp As String, l_lngCharCount As Long, l_strTest As String, l_blnFound As Boolean
'
'm_strHistory = "DADC BBC Audio RAR Send"
'cmdSave.Value = True
'm_strHistory = ""
'
'l_lngCounter = 0
'l_strTemp = lblAudioConfiguration.Caption
'If InStr(l_strTemp, vbCrLf) > 0 Then
'    Do While InStr(l_strTemp, vbCrLf) > 0
'        l_lngCharCount = InStr(l_strTemp, vbCrLf)
'        l_strRequirements(l_lngCounter) = Left(l_strTemp, l_lngCharCount - 1)
'        l_strTemp = Mid(l_strTemp, l_lngCharCount + 2)
'        l_lngCounter = l_lngCounter + 1
'    Loop
'    l_strRequirements(l_lngCounter) = l_strTemp
'Else
'    l_strRequirements(0) = l_strTemp
'End If
'
'If cmbAudioType1.Text <> "" Then
'    If cmbAudioType1.Text <> "Standard Stereo" And Left(cmbAudioType1.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType2.Text <> "" Then
'    If cmbAudioType2.Text <> "Standard Stereo" And Left(cmbAudioType2.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType3.Text <> "" Then
'    If cmbAudioType3.Text <> "Standard Stereo" And Left(cmbAudioType3.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType4.Text <> "" Then
'    If cmbAudioType4.Text <> "Standard Stereo" And Left(cmbAudioType4.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType5.Text <> "" Then
'    If cmbAudioType5.Text <> "Standard Stereo" And Left(cmbAudioType5.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType6.Text <> "" Then
'    If cmbAudioType6.Text <> "Standard Stereo" And Left(cmbAudioType6.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType7.Text <> "" Then
'    If cmbAudioType7.Text <> "Standard Stereo" And Left(cmbAudioType7.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'If cmbAudioType8.Text <> "" Then
'    If cmbAudioType8.Text <> "Standard Stereo" And Left(cmbAudioType8.Text, 3) <> "5.1" Then
'        MsgBox "For Audio only files, only Standard Stereo or 5.1 mixes can be handled at this time." & vbCrLf & "XML not made.", vbCritical, "Error..."
'        Exit Sub
'    End If
'End If
'
'For l_lngCounter = 0 To 8
'    If l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) <> "BBCW ProRes" Then
'        l_blnFound = False
'        If cmbAudioContent1.Text = "M&E" Then
'            l_strTest = cmbAudioType1.Text & ", M&&E"
'            If cmbAudioLanguage1.Text <> "None" Then
'                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent1.Text = "Music" Or cmbAudioContent1.Text = "Effects" Then
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text
'            If cmbAudioLanguage1.Text <> "None" Then
'                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage1.Text, "(") Then
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & Left(cmbAudioLanguage1.Text, InStr(cmbAudioLanguage1.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & cmbAudioLanguage1.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent2.Text = "M&E" Then
'            l_strTest = cmbAudioType2.Text & ", M&&E"
'            If cmbAudioLanguage2.Text <> "None" Then
'                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent2.Text = "Music" Or cmbAudioContent2.Text = "Effects" Then
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text
'            If cmbAudioLanguage2.Text <> "None" Then
'                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage2.Text, "(") Then
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & Left(cmbAudioLanguage2.Text, InStr(cmbAudioLanguage2.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & cmbAudioLanguage2.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent3.Text = "M&E" Then
'            l_strTest = cmbAudioType3.Text & ", M&&E"
'            If cmbAudioLanguage3.Text <> "None" Then
'                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent3.Text = "Music" Or cmbAudioContent3.Text = "Effects" Then
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text
'            If cmbAudioLanguage3.Text <> "None" Then
'                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage3.Text, "(") Then
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & Left(cmbAudioLanguage3.Text, InStr(cmbAudioLanguage3.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & cmbAudioLanguage3.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent4.Text = "M&E" Then
'            l_strTest = cmbAudioType4.Text & ", M&&E"
'            If cmbAudioLanguage4.Text <> "None" Then
'                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent4.Text = "Music" Or cmbAudioContent4.Text = "Effects" Then
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text
'            If cmbAudioLanguage4.Text <> "None" Then
'                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage4.Text, "(") Then
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & Left(cmbAudioLanguage4.Text, InStr(cmbAudioLanguage4.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & cmbAudioLanguage4.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent5.Text = "M&E" Then
'            l_strTest = cmbAudioType5.Text & ", M&&E"
'            If cmbAudioLanguage5.Text <> "None" Then
'                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent5.Text = "Music" Or cmbAudioContent5.Text = "Effects" Then
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text
'            If cmbAudioLanguage5.Text <> "None" Then
'                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage5.Text, "(") Then
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & Left(cmbAudioLanguage5.Text, InStr(cmbAudioLanguage5.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & cmbAudioLanguage5.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent6.Text = "M&E" Then
'            l_strTest = cmbAudioType6.Text & ", M&&E"
'            If cmbAudioLanguage6.Text <> "None" Then
'                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent6.Text = "Music" Or cmbAudioContent6.Text = "Effects" Then
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text
'            If cmbAudioLanguage6.Text <> "None" Then
'                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage6.Text, "(") Then
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & Left(cmbAudioLanguage6.Text, InStr(cmbAudioLanguage6.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & cmbAudioLanguage6.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent7.Text = "M&E" Then
'            l_strTest = cmbAudioType7.Text & ", M&&E"
'            If cmbAudioLanguage7.Text <> "None" Then
'                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent7.Text = "Music" Or cmbAudioContent7.Text = "Effects" Then
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text
'            If cmbAudioLanguage7.Text <> "None" Then
'                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage7.Text, "(") Then
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & Left(cmbAudioLanguage7.Text, InStr(cmbAudioLanguage7.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & cmbAudioLanguage7.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If cmbAudioContent8.Text = "M&E" Then
'            l_strTest = cmbAudioType8.Text & ", M&&E"
'            If cmbAudioLanguage8.Text <> "None" Then
'                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf cmbAudioContent8.Text = "Music" Or cmbAudioContent8.Text = "Effects" Then
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text
'            If cmbAudioLanguage8.Text <> "None" Then
'                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "XML not made.", vbCritical, "Error..."
'                Exit Sub
'            End If
'        ElseIf InStr(cmbAudioLanguage8.Text, "(") Then
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & Left(cmbAudioLanguage8.Text, InStr(cmbAudioLanguage8.Text, "(") - 2)
'        Else
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & cmbAudioLanguage8.Text
'        End If
''        If l_strTest = l_strRequirements(l_lngCounter) And l_blnFound = False Then
'        If InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 And l_blnFound = False Then
'            l_blnFound = True
''        ElseIf l_strTest = l_strRequirements(l_lngCounter) Then
'        ElseIf InStr(l_strRequirements(l_lngCounter), l_strTest) > 0 Then
'            MsgBox "Duplicate item " & l_strTest & " found - cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'
'        If l_blnFound = False Then
'            MsgBox "There was a listed item " & l_strRequirements(l_lngCounter) & " that has not been logged. Cannot make xml", vbCritical, "Error..."
'            Exit Sub
'        End If
'    ElseIf l_strRequirements(l_lngCounter) <> "" And Left(l_strRequirements(l_lngCounter), 11) = "BBCW ProRes" Then
'        MsgBox "You cannot do an audio only xml when it appears video items have also been requested." & vbCrLf & "Cannot make xml file", vbCritical, "Error..."
'        Exit Sub
'    End If
'Next
'
'MakeDADCAudioRARXMLFile (False)
'
'End Sub
'
Private Sub cmdDelete_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

Dim l_intResult As Integer, l_strSQL As String, FSO As FileSystemObject, l_strFullFilePath

If lblDeletedClip.Visible = False Then
    
    If cmbClipformat.Text = "Folder" Then
        If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Delete this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
            Exit Sub
        End If
    End If
    
    If MsgBox("Are you sure", vbYesNo + vbQuestion + vbDefaultButton2, "Delete Item " & txtClipID.Text & " ?") = vbYes Then
    
        If CheckAccess("/PurgeFilesFromDisk", True) Then
            'If the file is ion a discstore offer to actually delete it too.
            If lblFileSize.Caption <> "" And lblVerifiedDate.Caption <> "" And lblFormat.Caption = "DISCSTORE" Then
                If MsgBox("Do you wish to also purge the item from disk?", vbYesNo + vbDefaultButton2, "Please Confirm File Delete") = vbYes Then
                    If MsgBox("Are you sure you wish to delete the item?", vbYesNo + vbDefaultButton2, "Please Confirm File Delete") = vbYes Then
                        l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
                        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
                        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        MsgBox "Delete request made to the File Requests Queue.", vbInformation
                    End If
                Else
                    l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & Val(txtClipID.Text) & ";"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    'Record a History event
                    l_strSQL = "INSERT INTO eventhistory ("
                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                    l_strSQL = l_strSQL & "'" & Val(txtClipID.Text) & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                    l_strSQL = l_strSQL & "'Clip Record Deleted' "
                    l_strSQL = l_strSQL & ");"
                    
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    
                    ClearFields Me
                End If
            Else
                l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & Val(txtClipID.Text) & ";"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                'Record a History event
                l_strSQL = "INSERT INTO eventhistory ("
                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                l_strSQL = l_strSQL & "'" & Val(txtClipID.Text) & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                l_strSQL = l_strSQL & "'Clip Record Deleted' "
                l_strSQL = l_strSQL & ");"
                
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                
                ClearFields Me
            End If
        Else
            'Actually have delete set the flag system_deleted on the clip record.
            l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = -1, online = 0 WHERE eventID = " & Val(txtClipID.Text) & ";"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            'Record a History event
            l_strSQL = "INSERT INTO eventhistory ("
            l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
            l_strSQL = l_strSQL & "'" & Val(txtClipID.Text) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
            l_strSQL = l_strSQL & "'Clip Record Deleted' "
            l_strSQL = l_strSQL & ");"
            
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            
            ClearFields Me
        End If
        
    End If

Else
    
'    If MsgBox("Are you sure", vbYesNo + vbQuestion + vbDefaultButton2, "UnDelete Clip " & txtClipID.Text & " ?") = vbYes Then
    
        'Actually have delete set the flag system_deleted on the clip record.
        l_strSQL = "UPDATE events SET system_deleted = 0, hidefromweb = -1 WHERE eventID = " & Val(txtClipID.Text) & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        'Record a History event
        l_strSQL = "INSERT INTO eventhistory ("
        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
        l_strSQL = l_strSQL & "'" & Val(txtClipID.Text) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "'Clip Record UnDeleted' "
        l_strSQL = l_strSQL & ");"
        
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        ShowClipControl Val(txtClipID.Text)
        
'    End If

End If

Exit Sub

End Sub

Private Sub cmdDiscoveryOutput_Click()

m_strHistory = "Discovery XML"
cmdSave.Value = True
m_strHistory = ""
MakeDiscoveryXML

End Sub

Private Sub cmdDuplicate_Click()

Dim l_lngClipID As Long, l_lngNewClipID As Long
Dim l_strSQL As String
Dim l_rstOldStuff As ADODB.Recordset

If Val(txtClipID.Text) <> 0 Then
    'PromptClipChanges (Val(txtClipID.Text))
    l_lngClipID = txtClipID.Text
    txtJobID.Text = "999999"
    txtClipID.Text = ""
    cmbFileVersion.Text = ""
    chkFileLocked.Value = 0
    SaveClip True 'Ignoring 999999 in jobID just this once
    MsgBox "Clip Successfully Duplicated", vbInformation, "Duplicate Clip"
            
    'Record the clip usage
    l_strSQL = "INSERT INTO eventusage ("
    l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
    l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
    l_strSQL = l_strSQL & ");"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngNewClipID = Val(txtClipID.Text)
    
    'Duplicate any keywords
    l_strSQL = "SELECT * FROM eventkeyword WHERE eventID = '" & l_lngClipID & "';"
    Set l_rstOldStuff = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstOldStuff.EOF Then
        l_rstOldStuff.MoveFirst
        
        Do While Not l_rstOldStuff.EOF
            l_strSQL = "INSERT INTO eventkeyword (eventID, keywordtext) VALUES ('"
            l_strSQL = l_strSQL & l_lngNewClipID & "', '"
            l_strSQL = l_strSQL & l_rstOldStuff("keywordtext") & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstOldStuff.MoveNext
        Loop
    End If
    l_rstOldStuff.Close
    
    'Duplicate any segment or logging info
    l_strSQL = "SELECT * FROM eventsegment WHERE eventID = '" & l_lngClipID & "';"
    Set l_rstOldStuff = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstOldStuff.EOF Then
        l_rstOldStuff.MoveFirst
        
        Do While Not l_rstOldStuff.EOF
            l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, customfield1) VALUES ("
            l_strSQL = l_strSQL & l_lngNewClipID & ", "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("segmentreference") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("timecodestart") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("timecodestop") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("customfield1") & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstOldStuff.MoveNext
        Loop
    End If
    l_rstOldStuff.Close
    
    l_strSQL = "SELECT * FROM eventlogging WHERE eventID = '" & l_lngClipID & "';"
    Set l_rstOldStuff = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    
    If Not l_rstOldStuff.EOF Then
        l_rstOldStuff.MoveFirst
        
        Do While Not l_rstOldStuff.EOF
            l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
            l_strSQL = l_strSQL & l_lngNewClipID & ","
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("segmentreference") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("timecodestart") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("timecodestop") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOldStuff("note") & "');"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_rstOldStuff.MoveNext
        Loop
    End If
    l_rstOldStuff.Close
    
    Set l_rstOldStuff = Nothing
    
    ShowClipControl l_lngNewClipID
    
End If
End Sub

Private Sub cmdEditSize_Click()

If CheckAccess("/SuperUser", True) Then
    If txtFileSize.Visible = False Then
        txtFileSize.Visible = True
    ElseIf txtFileSize.Visible = True Then
        txtFileSize.Visible = False
    End If
Else
    txtFileSize.Visible = False
End If

End Sub

Private Sub cmdESIDL3XMLFile_Click()

m_strHistory = "ESI DL3 XML"
cmdSave.Value = True
m_strHistory = ""
MakeESIDL3XML

End Sub

Private Sub cmdffplay_Click()

Dim l_strNetworkPath As String, l_lngFilenumber As Long, l_curFileSize As Currency, CommandString As String

'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
If txtAltFolder.Text Like "\\*" Or Mid(txtAltFolder.Text, 2, 1) = ":" Then
    MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Problem..."
    Exit Sub
End If

If GetData("library", "format", "libraryID", lblLibraryID.Caption) = "DISCSTORE" Then
     
    l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
    l_strNetworkPath = Replace(l_strNetworkPath, "\\", "\\?\UNC\")
    If txtAltFolder.Text <> "" Then
        l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
    End If
    
    ShellExecute 0&, "open", "c:\ffmpeg\bin\ffplay.exe", """" & l_strNetworkPath & "\" & txtClipfilename.Text & """", l_strNetworkPath, vbNormalFocus
    
Else

    MsgBox "Clips can only be played if they are in one of the MX1 London DISCSTORES", vbOKOnly, "Attempting to play non-preview clip"
    
End If

End Sub

Private Sub cmdFFProbeRequest_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If SaveClip(chkIgnoreJobID.Value) = False Then Exit Sub

If lblFormat.Caption <> "DISCSTORE" Then Exit Sub
If cmbClipformat.Text = "Folder" Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "FfProbe") & ", "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
MsgBox "Done"

End Sub

Private Sub cmdFindReferenced_Click()

frmClipSearch.cmdClear.Value = True
frmClipSearch.txtReference.Text = txtReference.Text
frmClipSearch.cmdSearch.Value = True
frmClipSearch.Show
frmClipControl.Hide
frmClipSearch.ZOrder 0

End Sub

Private Sub cmdGoogleEpisodeXML_Click()

m_strHistory = "Google Ep XML"
cmdSave.Value = True
m_strHistory = ""
MakeGoogleEpisodeXML

End Sub

Private Sub cmdGoogleSeasonXML_Click()

m_strHistory = "Google Season XML"
cmdSave.Value = True
m_strHistory = ""
MakeGoogleSeasonXML

End Sub

Private Sub cmdGoogleShowXML_Click()

m_strHistory = "Google Show XML"
cmdSave.Value = True
m_strHistory = ""
MakeGoogleShowXML

End Sub

Private Sub cmdMD5Request_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If SaveClip(chkIgnoreJobID.Value) = False Then Exit Sub

If lblFormat.Caption <> "DISCSTORE" Then Exit Sub
If cmbClipformat.Text = "Folder" Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, bigfilesize, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MD5 Checksum") & ", "
l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
Unload Me

End Sub

Private Sub cmdMediaInfoRequest_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If SaveClip(chkIgnoreJobID.Value) = False Then Exit Sub

If lblFormat.Caption <> "DISCSTORE" Then Exit Sub
If cmbClipformat.Text = "Folder" Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
If MsgBox("Do you wish to include the Timecode Values", vbYesNo) = vbYes Then
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MediaInfo + TC") & ", "
Else
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MediaInfo no TC") & ", "
End If
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
Unload Me

End Sub

Private Sub cmdMoveAndChangeOwner_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request move file"
cmdSave.Value = True
m_strHistory = ""

Dim l_lngNewCompanyID As Long
Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_strSQL As String, l_lngDestinationLibraryID As Long

If cmbClipformat.Text = "Folder" Then
    If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Move this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
        Exit Sub
    End If
End If

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

frmGetNewFileDetails.Caption = "Please give the details for the Move..."
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
Unload frmGetBarcodeNumber

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" And Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & l_strNewLibraryBarcode & "' AND system_deleted = 0;")) = "DISCSTORE" Then
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & GetData("events", "altlocation", "eventID", Val(txtClipID.Text)) & "' "
    End If
    l_strSQL = l_strSQL & " AND clipfilename = '" & txtClipfilename.Text & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        frmGetNewCompany.Show vbModal
        If frmGetNewCompany.lblCompanyID.Caption <> "" Then
            l_lngNewCompanyID = Val(frmGetNewCompany.lblCompanyID.Caption)
        Else
            MsgBox "No new owner specified. Request Aborted."
            Exit Sub
        End If
        Unload frmGetNewCompany
        
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, NewCompanyID, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
        l_strSQL = l_strSQL & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode) & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
        End If
        If l_lngNewCompanyID <> 0 Then
            l_strSQL = l_strSQL & l_lngNewCompanyID & ", "
        Else
            l_strSQL = l_strSQL & l_lngNewCompanyID & "Null, "
        End If
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        Unload Me
    Else
        MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
    End If
Else
    MsgBox "Destination Barcode for copies must be a DISCSTORE", vbCritical
End If

End Sub

Private Sub cmdMoveFile_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request move file"
cmdSave.Value = True
m_strHistory = ""

Dim l_strNewLibraryBarcode As String, l_strNewFolder As String, l_strSQL As String, l_lngDestinationLibraryID As Long

If cmbClipformat.Text = "Folder" Then
    If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Move this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
        Exit Sub
    End If
End If

frmGetNewFileDetails.Caption = "Please give the details for the Move..."
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)
l_strNewFolder = frmGetNewFileDetails.txtAltLocation.Text
Unload frmGetBarcodeNumber

If Left(l_strNewLibraryBarcode, 3) = "BFS" Then
    If Not CheckAccess("/MoveToBFS") Then
        MsgBox "You do not have permission to move or copy to BFS", vbCritical
        Exit Sub
    End If
End If

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissinos to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" And Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & l_strNewLibraryBarcode & "' AND system_deleted = 0;")) = "DISCSTORE" Then
    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    If l_strNewFolder <> "" Then
        l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    Else
        l_strSQL = l_strSQL & " AND altlocation = '" & GetData("events", "altlocation", "eventID", Val(txtClipID.Text)) & "' "
    End If
    l_strSQL = l_strSQL & " AND clipfilename = '" & txtClipfilename.Text & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
        l_strSQL = l_strSQL & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode) & ", "
        If l_strNewFolder <> "" Then
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
        Else
            l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
        End If
        l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        Unload Me
    Else
        MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
    End If
Else
    MsgBox "Destination Barcode for copies must be a DISCSTORE", vbCritical
End If

End Sub

Private Sub cmdMoveToAutoDelete_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request Move to AutoDelete file"
cmdSave.Value = True
m_strHistory = ""

Dim l_strSQL As String

If cmbClipformat.Text = "Folder" Then
    MsgBox "Folders Cannot be handled in this way"
    Exit Sub
End If

l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, bigfilesize, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move to AutoDelete") & ", "
l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

MsgBox "Request Submitted"

End Sub

Private Sub cmdMoveToEdit_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request Move file"
cmdSave.Value = True
m_strHistory = ""

Dim l_strSQL As String, l_lngNewLibraryID As Long

If cmbClipformat.Text = "Folder" Then
    If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Move this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
        Exit Sub
    End If
End If

l_lngNewLibraryID = 770132

l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
l_strSQL = l_strSQL & " AND altlocation = '" & txtAltFolder.Text & "' "
l_strSQL = l_strSQL & " AND clipfilename = '" & txtClipfilename.Text & "'"
l_strSQL = l_strSQL & " AND system_deleted = 0"
If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
    l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
    l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    Unload Me
Else
    MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
End If

End Sub

Private Sub cmdMoveToOps_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request Move file"
cmdSave.Value = True
m_strHistory = ""

Dim l_strSQL As String, l_lngNewLibraryID As Long

If cmbClipformat.Text = "Folder" Then
    If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Move this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
        Exit Sub
    End If
End If

l_lngNewLibraryID = 770134

l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
l_strSQL = l_strSQL & " AND altlocation = '" & txtAltFolder.Text & "' "
l_strSQL = l_strSQL & " AND clipfilename = '" & txtClipfilename.Text & "'"
l_strSQL = l_strSQL & " AND system_deleted = 0"
If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
    l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
    l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    Unload Me
Else
    MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
End If

End Sub

Private Sub cmdMoveToPrivate_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request Move file"
cmdSave.Value = True
m_strHistory = ""

Dim l_strSQL As String, l_lngNewLibraryID As Long

If cmbClipformat.Text = "Folder" Then
    If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Move this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
        Exit Sub
    End If
End If

l_lngNewLibraryID = 770131

l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
l_strSQL = l_strSQL & " AND altlocation = '" & txtAltFolder.Text & "' "
l_strSQL = l_strSQL & " AND clipfilename = '" & txtClipfilename.Text & "'"
l_strSQL = l_strSQL & " AND system_deleted = 0"
If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
    l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
    l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    Unload Me
Else
    MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
End If

End Sub

Private Sub cmdMoveToVOD_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

m_strHistory = "Request Move file"
cmdSave.Value = True
m_strHistory = ""

Dim l_strSQL As String, l_lngNewLibraryID As Long

If cmbClipformat.Text = "Folder" Then
    If MsgBox("Individual File records for files within this folder will not be processed." & vbCrLf & "Are you sure you wish to Move this Folder", vbYesNo + vbDefaultButton2) = vbNo Then
        Exit Sub
    End If
End If

l_lngNewLibraryID = 770135

l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & l_lngNewLibraryID
l_strSQL = l_strSQL & " AND altlocation = '" & txtAltFolder.Text & "' "
l_strSQL = l_strSQL & " AND clipfilename = '" & txtClipfilename.Text & "'"
l_strSQL = l_strSQL & " AND system_deleted = 0"
If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
    l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
    l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
    l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(txtAltFolder.Text) & "', "
    l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    Unload Me
Else
    MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
End If

End Sub

Private Sub cmdNexGuardQC_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If SaveClip(chkIgnoreJobID.Value) = False Then Exit Sub

If lblFormat.Caption <> "DISCSTORE" Then Exit Sub
If cmbClipformat.Text = "Folder" Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (eventID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "NexGuard WM QC") & ", "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
MsgBox "Done"

End Sub

Private Sub cmdOPenFolderInExplorer_Click()

Dim l_strNetworkPath As String, l_lngFilenumber As Long, l_curFileSize As Currency, CommandString As String

'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
If txtAltFolder.Text Like "\\*" Or Mid(txtAltFolder.Text, 2, 1) = ":" Then
    MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Problem..."
    Exit Sub
End If

If GetData("library", "format", "libraryID", lblLibraryID.Caption) = "DISCSTORE" And (g_blnRedNetwork = True Or Val(lblLibraryID.Caption) = 796559) Then
     
    l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
    l_strNetworkPath = Replace(l_strNetworkPath, "\\", "\\?\UNC\")
    If txtAltFolder.Text <> "" Then
        l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
    End If
    
    'ShellExecute 0&, "open", "explorer.exe", """" & l_strNetworkPath & "\" & txtClipfilename.Text & """", l_strNetworkPath, vbNormalFocus
    Shell "Explorer.exe """ & l_strNetworkPath & """", vbNormalFocus
    
Else

    MsgBox "Explorer can only be opended if the clip is in one of the VDMS London Local DISCSTORES, and you are on a Red Network Machine", vbOKOnly, "Attempting to play non-preview clip"
    
End If

End Sub

Private Sub cmdRename_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

Dim l_strNewFilename As String, l_strSQL As String, l_blnResetReference As Boolean

l_strNewFilename = InputBox("Please give the new filename", "New Filename", txtClipfilename.Text)
If l_strNewFilename <> txtClipfilename.Text And l_strNewFilename <> "" Then

    If MsgBox("Are you sure you wish to rename this item to be" & vbCrLf & l_strNewFilename, vbYesNo + vbDefaultButton2, "Please Confirm File Rename") = vbYes Then
        If MsgBox("Should the Clip Reference be Rebuilt from the new Clip Filenmame", vbYesNo, "Rebuild Clip Reference") = vbYes Then l_blnResetReference = True
        l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, NewFileName, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "File Rename") & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFilename) & "', "
        l_strSQL = l_strSQL & IIf(l_blnResetReference = True, 1, 0) & ", "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        MsgBox "Rename request made to the File Requests Queue.", vbInformation
        Unload Me
    Else
        MsgBox "Request Cancelled", vbInformation
    End If

Else
    MsgBox "Request Cancelled", vbInformation
End If

End Sub

Private Sub cmdRestoreFromWasabi_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub
If Val(Format(lblFileSize.Caption, "#")) = 0 Then
    MsgBox "This file has not been verified on the S3 Bucket, so it either never got there or is still being archived." & vbCrLf & "The file cannot be restored at this time"
    Exit Sub
End If

If CheckFileOnDisc(txtClipfilename.Text, Format(lblFileSize.Caption, "#"), lblCompanyID.Caption) = True Then
    Dim l_strMsg As String, l_strBarcode As String, l_strPath As String
    l_strBarcode = GetDataSQL("SELECT barcode from vweventswithlibraryformat WHERE clipfilename = '" & QuoteSanitise(txtClipfilename.Text) & "' AND bigfilesize = " & Format(lblFileSize.Caption, "#") & " AND companyID = " & Val(lblCompanyID.Caption) & " AND format = 'DISCSTORE' and system_deleted = 0;")
    l_strPath = GetDataSQL("SELECT altlocation from vweventswithlibraryformat WHERE clipfilename = '" & QuoteSanitise(txtClipfilename.Text) & "' AND bigfilesize = " & Format(lblFileSize.Caption, "#") & " AND companyid = " & Val(lblCompanyID.Caption) & " AND format = 'DISCSTORE' and system_deleted = 0;")
    MsgBox "This file already exists on " & l_strBarcode & " in the path " & l_strPath & vbCrLf & "The file cannot be restored at this time"
    Exit Sub
End If

Dim l_strNewLibraryBarcode As String, l_lngNewLibraryID As Long, l_strNewFolder As String, l_strNewPath As String, l_strSQL As String, l_blnPreserveMasterfileStatus As Boolean, l_lngNewEventID As Long, l_lngDestinationLibraryID As Long

frmGetNewFileDetails.Caption = "Please give the details for the Restore..."
frmGetNewFileDetails.optRestoreDiscStores.Value = True
frmGetNewFileDetails.chkRestoreWithTranscode.Value = False
frmGetNewFileDetails.Show vbModal
l_strNewLibraryBarcode = UCase(frmGetNewFileDetails.txtBarcode.Text)

l_lngDestinationLibraryID = Val(GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode))
If CheckDriveWritePermissions(l_lngDestinationLibraryID) = False Then
    MsgBox "You do not have permissions to send file to that DISCSTORE", vbCritical
    Exit Sub
End If

If l_strNewLibraryBarcode <> "" And Trim(" " & GetDataSQL("SELECT TOP 1 format FROM library WHERE barcode = '" & l_strNewLibraryBarcode & "' AND system_deleted = 0;")) = "DISCSTORE" Then
    
    l_lngNewLibraryID = GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    l_strNewFolder = lblCompanyID.Caption & "\S3_Restore"
    l_strNewPath = GetData("library", "version", "barcode", l_strNewLibraryBarcode) & "/" & lblCompanyID.Caption & "/S3_Restore/" & txtClipfilename.Text
    l_blnPreserveMasterfileStatus = True

    l_strSQL = "SELECT TOP 1 eventID FROM events WHERE libraryID = " & GetData("library", "libraryID", "barcode", l_strNewLibraryBarcode)
    l_strSQL = l_strSQL & " AND altlocation = '" & l_strNewFolder & "' "
    l_strSQL = l_strSQL & "' AND clipfilename = '" & txtClipfilename.Text & "'"
    l_strSQL = l_strSQL & " AND system_deleted = 0"
    Debug.Print l_strSQL
    If Trim(" " & GetDataSQL(l_strSQL)) = "" Then
        l_lngNewEventID = CopyFileEventToLibraryID(txtClipID, l_lngNewLibraryID, True)
        SetData "events", "altlocation", "eventID", l_lngNewEventID, l_strNewFolder
        SetData "events", "bigfilesize", "eventID", l_lngNewEventID, Null
        SetData "events", "soundlay", "eventID", l_lngNewEventID, Null
        SetData "events", "online", "eventID", l_lngNewEventID, 1
        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, NewLibraryID, event_file_Request_typeID, NewFileID, FullPathToSourceFile, RequestName, RequesterEmail) VALUES ("
        l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
        l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
        l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Download From S3") & ", "
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & l_strNewPath & "', "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        MsgBox "Download Requested"
    Else
        MsgBox "A File of this name already exists in the destination folder", vbCritical, "Error"
    End If
Else
    MsgBox "Destination Barcode for Restores must be a DISCSTORE that is enabled for restoring", vbCritical
End If

Unload frmGetNewFileDetails

End Sub

Private Sub cmdSendToSMartjog_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "FTP to Smartjog") & ", "
l_strSQL = l_strSQL & "'" & g_strUserInitials & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

MsgBox "Done!"

End Sub

Private Sub cmdSHA1Request_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If SaveClip(chkIgnoreJobID.Value) = False Then Exit Sub

If lblFormat.Caption <> "DISCSTORE" Then Exit Sub
If cmbClipformat.Text = "Folder" Then Exit Sub

Dim l_strSQL As String

l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, bigfilesize, RequestName, RequesterEmail) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & Val(lblLibraryID.Caption) & ", "
l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "SHA1 Checksum") & ", "
l_strSQL = l_strSQL & Format(lblFileSize.Caption, "#") & ", "
l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError
Unload Me

End Sub

Private Sub cmdSubmitToATS_Click()

Dim l_lngProfileID As Long, l_strProfileXMLString As String
Dim l_strFullFilePath As String, l_strFilePath As String, l_strTimecodeOffset As String, l_strProgTimecode As String

If Validate_Timecode(txtTimecodeStart.Text, m_Framerate) <> True Then
    MsgBox "Cannot do ATS on a clip unless start timecode entry is valid", vbCritical, "Error."
ElseIf lblFormat.Caption <> "DISCSTORE" Then
    MsgBox "Cannot do ATS on a clip unless it is on a DISCSTORE", vbCritical, "Error."
ElseIf GetDataSQL("SELECT TOP 1 eventloggingID FROM eventlogging WHERE eventID = " & Val(txtClipID.Text) & " AND segmentreference = 'Programme';") = "" Then
    MsgBox "Cannot do ATS on a clip unless it has had its 'Programme' item logged.", vbCritical, "Error."
Else
    
    frmGetATSProfile.Show vbModal
    If frmGetATSProfile.cmbProfile.Text = "" Then
        MsgBox "No ATS Profile Selected", vbInformation
        Unload frmGetATSProfile
        Exit Sub
    End If
    
    l_lngProfileID = frmGetATSProfile.cmbProfile.Columns("ATS_Profile_ID").Text
    Unload frmGetATSProfile
    
    l_strProfileXMLString = GetData("ATS_Profile", "ATS_Profile_XML_String", "ATS_Profile_ID", l_lngProfileID)
    l_strFilePath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
    If txtAltFolder.Text <> "" Then l_strFilePath = l_strFilePath & "\" & txtAltFolder.Text
    l_strFullFilePath = l_strFilePath & "\" & txtClipfilename.Text
    l_strProgTimecode = GetDataSQL("SELECT TOP 1 timecodestart FROM eventlogging WHERE eventID = " & Val(txtClipID.Text) & " AND segmentreference = 'Programme';")
    l_strTimecodeOffset = Timecode_Subtract(Timecode_Subtract(l_strProgTimecode, txtTimecodeStart.Text, m_Framerate), IIf(m_Framerate = TC_29 Or m_Framerate = TC_59, "00:00:05;00", "00:00:05:00"), m_Framerate)
    l_strProfileXMLString = Replace(l_strProfileXMLString, "%SOURCE%", l_strFullFilePath)
    l_strProfileXMLString = Replace(l_strProfileXMLString, "%OUTPUT%", l_strFilePath & "\Output\")
    l_strProfileXMLString = Replace(l_strProfileXMLString, "%ERROR%", l_strFilePath & "\Error\")
    l_strProfileXMLString = Replace(l_strProfileXMLString, "%RESULTS%", l_strFilePath & "\Results\")
    l_strProfileXMLString = Replace(l_strProfileXMLString, "%ABSOLUTE_TC%", l_strTimecodeOffset)
    
    Open "\\warp\warpfs\96_Minnetonka_Hotfolders\XML\" & txtReference.Text & "_" & Format(Now, "YYYY-MM-DD-HH-NN-SS") & ".xml" For Output As 1
    Print #1, l_strProfileXMLString
    Close 1
    
    MsgBox "Done"
End If

End Sub

Private Sub cmdSvenskItems_Click()

Dim l_strSQL As String

If Val(txtClipID.Text) = 0 Then Exit Sub

If adoLogging.Recordset.RecordCount > 0 Then

    MsgBox "You already have logged items.", vbCritical, "Items not made"
    
End If

l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & "'Bars_and_Tones', "
l_strSQL = l_strSQL & "'09:58:30:00', "
l_strSQL = l_strSQL & "'09:59:30:00');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & "'Clock', "
l_strSQL = l_strSQL & "'09:59:30:00', "
l_strSQL = l_strSQL & "'09:59:57:00');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & "'Black', "
l_strSQL = l_strSQL & "'09:59:57:00', "
l_strSQL = l_strSQL & "'10:00:00:00');"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
l_strSQL = l_strSQL & Val(txtClipID.Text) & ", "
l_strSQL = l_strSQL & "'Programme', "
l_strSQL = l_strSQL & "'10:00:00:00', "
l_strSQL = l_strSQL & "NULL);"
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

adoLogging.Recordset.Requery

End Sub

Private Sub cmdTranscodeProgOnly_Click()

If Trim(" " & GetDataSQL("SELECT TOP 1 eventloggingID from eventlogging WHERE segmentreference = 'Programme' AND eventID = " & Val(txtClipID.Text))) = "" Then
    MsgBox "Cannot do a programme only transcode if Programme item hasn't been logged", vbCritical, "Error."
    Exit Sub
End If
If Validate_Timecode(txtTimecodeStart.Text, m_Framerate) <> True Then
    MsgBox "Cannot transcode a clip unless start timecode entry is valid", vbCritical, "Error."
Else

    frmXMLTranscode.chkBulkTranscoding.Value = 0
    frmXMLTranscode.lblTimecodeStart.Caption = txtTimecodeStart.Text
    frmXMLTranscode.lblTimecodeStop.Caption = txtTimecodeStop.Text
    frmXMLTranscode.chkMOConvert.Value = 0
    adoLogging.Recordset.MoveFirst
    Do While Not adoLogging.Recordset.EOF
        If adoLogging.Recordset("segmentreference") = "Programme" Then
            frmXMLTranscode.txtTimecodeStart.Text = adoLogging.Recordset("timecodestart")
            frmXMLTranscode.txtTimecodeStop.Text = adoLogging.Recordset("timecodestop")
            frmXMLTranscode.chkTimecodesFromCaller.Value = 1
            frmXMLTranscode.chkSkipLogging = 1
            Exit Do
        End If
        adoLogging.Recordset.MoveNext
    Loop
    If frmXMLTranscode.txtTimecodeStart.Text = "" Then
        MsgBox "Clip does not have its Programme Start and End timecodes logged." & vbCrLf & "Prog only Transcoding cancelled.", vbCritical, "Error..."
        Unload frmXMLTranscode
        Exit Sub
    End If
    
'    Select Case m_Framerate
'        Case TC_25
'            frmXMLTranscode.optTimecodeType(0).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "10:00:00:00"
'        Case TC_29
'            frmXMLTranscode.optTimecodeType(1).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "01:00:00;00"
'        Case TC_24
'            frmXMLTranscode.optTimecodeType(3).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "10:00:00:00"
'        Case TC_30
'            frmXMLTranscode.optTimecodeType(2).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "01:00:00:00"
'        Case TC_50
'            frmXMLTranscode.optTimecodeType(4).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "10:00:00:00"
'        Case TC_59
'            frmXMLTranscode.optTimecodeType(5).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "01:00:00;00"
'        Case TC_60
'            frmXMLTranscode.optTimecodeType(6).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "01:00:00:00"
'        Case Else
'            frmXMLTranscode.optTimecodeType(0).Value = True
'            frmXMLTranscode.txtStartTimecodeOverride.Text = "10:00:00:00"
'    End Select
    
    frmXMLTranscode.Show vbModal

End If

Unload frmXMLTranscode

End Sub

Private Sub cmdiTunes_Click()

If txtClipID.Text = "" Then Exit Sub

ShowClipiTunes Val(txtClipID.Text)

End Sub

Private Sub cmdMakeKeyframeLikeReference_Click()

If txtReference.Text <> "" Then txtKeyframeFilename.Text = txtReference.Text & g_strJPEGextension

End Sub

Private Sub cmdMakePipelineSchedule_Click()

Dim l_strFileNameToSave As String, l_strStorageLocation As String, l_strProgramStart As String, l_strProgramEnd As String, Count As Long

m_strHistory = "Pipeline Schedule"
cmdSave.Value = True
m_strHistory = ""

'MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
'MDIForm1.dlgMain.Filter = "XML files|*.xml"
'MDIForm1.dlgMain.FileName = txtReference.Text & ".xml"
'
'MDIForm1.dlgMain.ShowSave

l_strStorageLocation = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
If Left(l_strStorageLocation, 6) = "\\warp" Then
    Select Case g_strWorkstation
        Case "JCA-PIPELINE1"
            l_strStorageLocation = "\\warpforpipeline1\warpfs"
        Case "JCA-PIPELINE2"
            l_strStorageLocation = "\\warpforpipeline2\warpfs"
        Case "JCA-PIPELINE4"
            l_strStorageLocation = "\\warpforpipeline4\warpfs"
    End Select
End If
l_strStorageLocation = l_strStorageLocation & "\" & txtAltFolder.Text
l_strFileNameToSave = l_strStorageLocation & "\" & txtReference.Text & ".pipelineschedule"

If cmbPipelineUnit.Text = "" Or cmbEncodeReplayDeck.Text = "" Or txtHorizpixels.Text = "" Or cmbAspect.Text = "" Or txtTimecodeStart.Text = "" Or txtTimecodeStop.Text = "" Then

    MsgBox "Please select a Pipeline Unit for this capture, a replay deck, chose Pixel Sizes and Aspect Ratio" & vbCrLf & "and give the start and end times for the capture.", vbCritical, "Could not Construct PipelineSchedule"
    Exit Sub
    
End If

If cmbClipformat.Text <> "Quicktime" Or cmbClipcodec.Text <> "ProRes HQ" Then

    MsgBox "It is currently only possible to build capture schedules for ProRes HQ files", vbCritical, "Could not Construct PipelineSchedule"
    Exit Sub
    
End If

On Error GoTo FAILED

Open l_strFileNameToSave For Output As 1

On Error GoTo 0

Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
Print #1, "<Schedule xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" version=""1.0"" xmlns=""urn:telestream.net:soa:core"">"
Print #1, "  <Clip>"
Print #1, "    <Private>"
Print #1, "      <Color>None</Color>"
Print #1, "    </Private>"
Print #1, "    <TokenizedTitle>"
Print #1, "      <Tokens>"
Print #1, "        <Literal Value=""" & txtReference.Text & """ />"
Print #1, "        <Counter Value="""" Format=""1"" />"
Print #1, "        <Literal Value="""" />"
Print #1, "      </Tokens>"
Print #1, "      <Feature identifier=""91610929-c4bd-491e-b540-b66a280a4dfe"" name=""TokensDocument"" restriction=""inoperable"" />"
Print #1, "    </TokenizedTitle>"
Print #1, "    <Start>" & txtTimecodeStart.Text & "@" & cmbFrameRate.Text & "</Start>"
Print #1, "    <End>" & txtTimecodeStop.Text & "@" & cmbFrameRate.Text & "</End>"
Print #1, "    <Media identifier=""ca5e8a5c-35b4-492a-ab78-6eeab194c7f7"" />"
Print #1, "    <State>Waiting</State>"
Print #1, "    <CreationDateTime>" & Format(Now, "yyyy-mm-dd") & "T" & Format(Now, "hh:nn:ss") & "+01:00</CreationDateTime>"
Print #1, "  </Clip>"
Print #1, "  <Service>"
Print #1, "    <Name>" & cmbPipelineUnit.Columns("description").Text & "</Name>"
Print #1, "    <Address>" & cmbPipelineUnit.Columns("information").Text & "</Address>"
Print #1, "    <Port>554</Port>"
Print #1, "    <Format>PAL</Format>"
Print #1, "  </Service>"
Print #1, "  <TimeBase>Auto</TimeBase>"
Print #1, "  <AutoActivate>false</AutoActivate>"
Print #1, "  <EnableVideoPreview>true</EnableVideoPreview>"
Print #1, "  <EnableAudioPreview>true</EnableAudioPreview>"
Print #1, "  <EnableDiagnosticMonitor>false</EnableDiagnosticMonitor>"
Print #1, "  <EnableStatusMonitor>true</EnableStatusMonitor>"
Print #1, "  <HandleLength>0</HandleLength>"
Print #1, "  <Container identifier=""f896c648-5187-47a9-a22e-44c02353bab1"" name=""QuickTime"" extensions=""mov"">"
'Print #1, "    <Parameter type=""guid"" identifier=""b5f03eb8-fa0e-4a20-a77b-5cfbf08163b4"" name=""Storage Model"">ce979210-afe0-489e-86ea-8620a9aa8cae</Parameter>" ' We think this is File buffering ticked
Print #1, "    <Parameter type=""guid"" identifier=""b5f03eb8-fa0e-4a20-a77b-5cfbf08163b4"" name=""Storage Model"">e803f6de-1388-4530-a3b5-23651f9e1fcb</Parameter>" ' We think this is File Buffering not ticked
Print #1, "    <Parameter type=""string"" identifier=""ae2e2433-e1e5-4627-bbe2-835e22fae756"" name=""Storage Path"">" & l_strStorageLocation & "</Parameter>"
Print #1, "    <Parameter type=""string"" identifier=""fd11a265-f8b3-4ded-8626-8f1fe53cb109"" name=""Temp Path"">D:\</Parameter>"
Print #1, "    <Parameter type=""guid"" identifier=""f95bf324-6c98-49c5-a79f-ed2e0014fdd4"" name=""Style"">ce979210-afe0-489e-86ea-8620a9aa8cae</Parameter>"
Print #1, "    <Parameter type=""boolean"" identifier=""1b16c00d-429f-4de5-9e65-5ac83a1ff061"" name=""InsertCaptionTrack"">False</Parameter>"
Print #1, "  </Container>"
Print #1, "  <Audio>"
If cmbAudioTrackDepth.Text = "24" Then
    Print #1, "    <Format>in24</Format>"
Else
    Print #1, "    <Format>twos</Format>"
End If
Print #1, "    <Channels>" & cmbAudioTracks.Text & "</Channels>"
For Count = 1 To Val(cmbAudioTracks.Text)
    Print #1, "    <AudioTracks>"
    Print #1, "      <Channels>" & Count & "</Channels>"
    Print #1, "      <AudioFormat>twos</AudioFormat>"
    Print #1, "    </AudioTracks>"
Next
Print #1, "    <SampleRate>48000</SampleRate>"
Print #1, "  </Audio>"
Print #1, "  <Video>"
Print #1, "    <Format>apch</Format>"
Print #1, "    <HorizontalResolution>";
Select Case txtHorizpixels.Text
    Case "720"
        If cmbAspect.Text = "16:9" Then Print #1, "a90";
        If cmbAspect.Text = "4:3" Then Print #1, "c90";
    Case "1920"
        Print #1, "s240";
End Select
Print #1, "</HorizontalResolution>"
Print #1, "    <VerticalResolution>";
Select Case txtVertpixels.Text
    Case "486"
        Print #1, "i61";
    Case "576"
        Print #1, "i72";
    Case "1080"
        If cmbInterlace.Text = "Interlace" Then Print #1, "i135";
        If cmbInterlace.Text = "progressive" Then Print #1, "p135";
End Select
Print #1, "</VerticalResolution>"
Print #1, "    <FrameRate>" & cmbFrameRate.Text & "</FrameRate>"
Print #1, "  </Video>"
Print #1, "  <AudioPreview>"
Print #1, "    <Channel>true</Channel>"
Print #1, "    <Channel>true</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "    <Channel>false</Channel>"
Print #1, "  </AudioPreview>"
Print #1, "  <SchedulePrivate>"
Print #1, "    <ZoomLevel>180</ZoomLevel>"
Print #1, "    <VerticalOffsetPercent>0.41311773808908631</VerticalOffsetPercent>"
Print #1, "  </SchedulePrivate>"
Print #1, "</Schedule>"

Close #1

Beep
MsgBox "PipelineSchedule created.", vbInformation, "Success"

Exit Sub

FAILED:

Beep
MsgBox "Failed to make PipelineSchedule - could not save to chosen drive and folder.", vbCritical, "Failed"

End Sub

Private Sub cmdMediaInfo_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If VerifyClip(txtClipID.Text, txtAltFolder.Text, txtClipfilename.Text, lblLibraryID.Caption, False, True) = False Then Exit Sub

Dim l_strNetworkPath As String, MediaInfo As MediaInfoData, l_ImageDimensions As ImgDimType, temp As String

Screen.MousePointer = vbHourglass
l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
If txtAltFolder.Text <> "" Then l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
l_strNetworkPath = l_strNetworkPath & "\" & txtClipfilename.Text

MediaInfo = GetMediaInfoOnFile(l_strNetworkPath)

With MediaInfo
    If .txtFormat = "TTML" Then
        cmbClipformat.Text = "ITT Subtitles"
'    ElseIf .txtFormat = "JPEG Image" Or .txtFormat = "PNG Image" Or .txtFormat = "BMP Image" Or .txtFormat = "GIF Image" Then
'        cmbClipformat.Text = .txtFormat
'        If getImgDim(l_strNetworkPath, l_ImageDimensions, temp) = True Then
'            txtVertpixels.Text = l_ImageDimensions.Height
'            txtHorizpixels.Text = l_ImageDimensions.Width
'        End If
    ElseIf .txtFormat <> "" And .txtFormat <> "Other" Then
        cmbClipformat.Text = .txtFormat
        cmbAudioCodec.Text = .txtAudioCodec
        cmbFrameRate.Text = .txtFrameRate
        cmbCbrVbr.Text = .txtCBRVBR
        txtHorizpixels.Text = .txtWidth
        txtVertpixels.Text = .txtHeight
        txtVideoBitrate.Text = .txtVideoBitrate
        cmbInterlace.Text = .txtInterlace
        txtTimecodeStart.Text = .txtTimecodeStart
        txtTimecodeStop.Text = .txtTimecodeStop
        txtDuration.Text = .txtDuration
        txtAudioBitrate.Text = .txtAudioBitrate
        txtTrackCount.Text = .txtTrackCount
        txtAudioChannelCount.Text = .txtChannelCount
        txtBitrate.Text = Val(txtVideoBitrate.Text) + Val(txtAudioBitrate.Text)
        If txtBitrate.Text = "0" Then txtBitrate.Text = Int(Val(.txtOverallBitrate) / 1000)
        If txtBitrate.Text = "0" Then txtBitrate.Text = ""
        cmbAspect.Text = .txtAspect
        If cmbAspect.Text = "4:3" Then
            chkfourbythreeflag.Value = 1
        Else
            chkfourbythreeflag.Value = 0
        End If
        cmbGeometry.Text = .txtGeometry
        cmbClipcodec.Text = .txtVideoCodec
        txtColorSpace.Text = .txtColorSpace
        txtChromaSubsampling.Text = .txtChromaSubsmapling
'        If cmbPurpose.Text = "" Then cmbPurpose.Text = GetData("xref", "format", "description", cmbClipcodec.Text)
        cmbStreamType.Text = .txtStreamType
        If .txtSeriesTitle <> "" Then
            txtTitle.Text = .txtSeriesTitle
            cmbSeries.Text = .txtSeriesNumber
            txtSubtitle.Text = .txtProgrammeTitle
            cmbEpisode.Text = .txtEpisodeNumber
            txtNotes.Text = .txtSynopsis
            Select Case .txtAudioLayout
                Case "EBU R 48: 2a"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "MOS"
                    cmbAudioLanguage2.Text = "None"
                Case "EBU R 123: 4b"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "M&E"
                    cmbAudioLanguage2.Text = "None"
                Case "EBU R 123: 4c"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "Audio Description"
                    cmbAudioLanguage2.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                Case "EBU R 123: 16c (5.1 with M&E)"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "M&E"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "5.1"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType4.Text = "5.1"
                    cmbAudioContent4.Text = "M&E"
                    cmbAudioLanguage4.Text = "None"
                Case "EBU R 123: 16c (5.1 with AD)"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "Audio Description"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "5.1"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType4.Text = "5.1"
                    cmbAudioContent4.Text = "M&E"
                    cmbAudioLanguage4.Text = "None"
                Case "EBU R 123: 16d"
                    cmbAudioType1.Text = "5.1"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "MOS"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "5.1"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage)
                    cmbAudioType4.Text = "Standard Stereo"
                    cmbAudioContent4.Text = "MOS"
                    cmbAudioLanguage4.Text = "None"
                Case "EBU R 123: 16f"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "MOS"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "Standard Stereo"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage)
                    cmbAudioType4.Text = "Standard Stereo"
                    cmbAudioContent4.Text = "MOS"
                    cmbAudioLanguage4.Text = "None"
                    cmbAudioType5.Text = "Standard Stereo"
                    cmbAudioContent5.Text = "Composite"
                    cmbAudioLanguage5.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtTertiaryAudioLanguage)
                    cmbAudioType6.Text = "Standard Stereo"
                    cmbAudioContent6.Text = "MOS"
                    cmbAudioLanguage6.Text = "None"
                    cmbAudioType7.Text = "Standard Stereo"
                    cmbAudioContent7.Text = "MOS"
                    cmbAudioLanguage7.Text = "None"
                    cmbAudioType8.Text = "Standard Stereo"
                    cmbAudioContent8.Text = "MOS"
                    cmbAudioLanguage8.Text = "None"
            End Select
        End If
        lblLastMediaInfoQuery.Caption = Format(Now, "dd MMM yyyy HH:NN:SS")
    Else
        Select Case UCase(Right(l_strNetworkPath, 3))
            Case "ISO"
                cmbClipformat.Text = "DVD ISO Image"
            Case "PDF"
                cmbClipformat.Text = "PDF File"
            Case "PSD"
                cmbClipformat.Text = "PSD Still Image"
            Case "SCC"
                cmbClipformat.Text = "SCC File (EIA 608)"
            Case "SRT"
                cmbClipformat.Text = "SRT Subtitles"
            Case "STL"
                cmbClipformat.Text = "STL Subtitles"
            Case "TIF"
                cmbClipformat.Text = "TIF Still Image"
            Case "DOC", "DOCX"
                cmbClipformat.Text = "Word File"
            Case "XLS", "XLSX"
                cmbClipformat.Text = "XLS File"
            Case "XML"
                cmbClipformat.Text = "XML File"
            Case "RAR"
                cmbClipformat.Text = "RAR Archive"
            Case "ZIP"
                cmbClipformat.Text = "ZIP Archive"
            Case Else
                cmbClipformat.Text = "Other"
        End Select
    End If
End With

Set_Framerate

End Sub

Private Sub cmdMediaInfoPreserveTimecode_Click()

Dim l_ImageDimensions As ImgDimType, temp As String

If Val(txtClipID.Text) = 0 Then Exit Sub

If VerifyClip(txtClipID.Text, txtAltFolder.Text, txtClipfilename.Text, lblLibraryID.Caption, False, True) = False Then Exit Sub

Dim l_strNetworkPath As String, MediaInfo As MediaInfoData

Screen.MousePointer = vbHourglass
l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
If txtAltFolder.Text <> "" Then l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
l_strNetworkPath = l_strNetworkPath & "\" & txtClipfilename.Text

MediaInfo = GetMediaInfoOnFile(l_strNetworkPath)

With MediaInfo
    If .txtFormat = "TTML" Then
        cmbClipformat.Text = "ITT Subtitles"
'    ElseIf .txtFormat = "JPEG Image" Or .txtFormat = "PNG Image" Or .txtFormat = "BMP Image" Or .txtFormat = "GIF Image" Then
'        cmbClipformat.Text = .txtFormat
'        If getImgDim(l_strNetworkPath, l_ImageDimensions, temp) = True Then
'            txtVertpixels.Text = l_ImageDimensions.Height
'            txtHorizpixels.Text = l_ImageDimensions.Width
'        End If
    ElseIf .txtFormat <> "" And .txtFormat <> "Other" Then
        cmbClipformat.Text = .txtFormat
        cmbAudioCodec.Text = .txtAudioCodec
        cmbFrameRate.Text = .txtFrameRate
        cmbCbrVbr.Text = .txtCBRVBR
        txtHorizpixels.Text = .txtWidth
        txtVertpixels.Text = .txtHeight
        txtVideoBitrate.Text = .txtVideoBitrate
        cmbInterlace.Text = .txtInterlace
    '    txtTimecodestart.Text = .txtTimecodestart
    '    txtTimecodestop.Text = .txtTimecodestop
    '    txtDuration.Text = .txtDuration
        txtAudioBitrate.Text = .txtAudioBitrate
        txtTrackCount.Text = .txtTrackCount
        txtAudioChannelCount.Text = .txtChannelCount
        txtBitrate.Text = Val(txtVideoBitrate.Text) + Val(txtAudioBitrate.Text)
        If txtBitrate.Text = "0" Then txtBitrate.Text = Int(Val(.txtOverallBitrate) / 1000)
        If txtBitrate.Text = "0" Then txtBitrate.Text = ""
        cmbAspect.Text = .txtAspect
        If cmbAspect.Text = "4:3" Then
            chkfourbythreeflag.Value = 1
        Else
            chkfourbythreeflag.Value = 0
        End If
        cmbGeometry.Text = .txtGeometry
        cmbClipcodec.Text = .txtVideoCodec
        txtColorSpace.Text = .txtColorSpace
        txtChromaSubsampling.Text = .txtChromaSubsmapling
'        If cmbPurpose.Text = "" Then cmbPurpose.Text = GetData("xref", "format", "description", cmbClipcodec.Text)
        cmbStreamType.Text = .txtStreamType
        If .txtSeriesTitle <> "" Then
            txtTitle.Text = .txtSeriesTitle
            cmbSeries.Text = .txtSeriesNumber
            txtSubtitle.Text = .txtProgrammeTitle
            cmbEpisode.Text = .txtEpisodeNumber
            txtNotes.Text = .txtSynopsis
            Select Case .txtAudioLayout
                Case "EBU R 48: 2a"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "MOS"
                    cmbAudioLanguage2.Text = "None"
                Case "EBU R 123: 4b"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "M&E"
                    cmbAudioLanguage2.Text = "None"
                Case "EBU R 123: 4c"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "Audio Description"
                    cmbAudioLanguage2.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                Case "EBU R 123: 16c (5.1 with M&E)"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "M&E"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "5.1"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType4.Text = "5.1"
                    cmbAudioContent4.Text = "M&E"
                    cmbAudioLanguage4.Text = "None"
                Case "EBU R 123: 16c (5.1 with AD)"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "Audio Description"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "5.1"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType4.Text = "5.1"
                    cmbAudioContent4.Text = "M&E"
                    cmbAudioLanguage4.Text = "None"
                Case "EBU R 123: 16d"
                    cmbAudioType1.Text = "5.1"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "MOS"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "5.1"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage)
                    cmbAudioType4.Text = "Standard Stereo"
                    cmbAudioContent4.Text = "MOS"
                    cmbAudioLanguage4.Text = "None"
                Case "EBU R 123: 16f"
                    cmbAudioType1.Text = "Standard Stereo"
                    cmbAudioContent1.Text = "Composite"
                    cmbAudioLanguage1.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtPrimaryAudioLanguage)
                    cmbAudioType2.Text = "Standard Stereo"
                    cmbAudioContent2.Text = "MOS"
                    cmbAudioLanguage2.Text = "None"
                    cmbAudioType3.Text = "Standard Stereo"
                    cmbAudioContent3.Text = "Composite"
                    cmbAudioLanguage3.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtSecondaryAudioLanguage)
                    cmbAudioType4.Text = "Standard Stereo"
                    cmbAudioContent4.Text = "MOS"
                    cmbAudioLanguage4.Text = "None"
                    cmbAudioType5.Text = "Standard Stereo"
                    cmbAudioContent5.Text = "Composite"
                    cmbAudioLanguage5.Text = GetData("ISO639Language", "LanguageNameEnglish", "ISO639Language_3B", .txtTertiaryAudioLanguage)
                    cmbAudioType6.Text = "Standard Stereo"
                    cmbAudioContent6.Text = "MOS"
                    cmbAudioLanguage6.Text = "None"
                    cmbAudioType7.Text = "Standard Stereo"
                    cmbAudioContent7.Text = "MOS"
                    cmbAudioLanguage7.Text = "None"
                    cmbAudioType8.Text = "Standard Stereo"
                    cmbAudioContent8.Text = "MOS"
                    cmbAudioLanguage8.Text = "None"
            End Select
        End If
        lblLastMediaInfoQuery.Caption = Format(Now, "dd MMM yyyy HH:NN:SS")
    Else
        Select Case UCase(Right(l_strNetworkPath, 3))
            Case "ISO"
                cmbClipformat.Text = "DVD ISO Image"
            Case "PDF"
                cmbClipformat.Text = "PDF File"
            Case "PSD"
                cmbClipformat.Text = "PSD Still Image"
            Case "SCC"
                cmbClipformat.Text = "SCC File (EIA 608)"
            Case "SRT"
                cmbClipformat.Text = "SRT Subtitles"
            Case "STL"
                cmbClipformat.Text = "STL Subtitles"
            Case "TIF"
                cmbClipformat.Text = "TIF Still Image"
            Case "DOC", "DOCX"
                cmbClipformat.Text = "Word File"
            Case "XLS", "XLSX"
                cmbClipformat.Text = "XLS File"
            Case "XML"
                cmbClipformat.Text = "XML File"
            Case "RAR"
                cmbClipformat.Text = "RAR Archive"
            Case "ZIP"
                cmbClipformat.Text = "ZIP Archive"
            Case Else
                cmbClipformat.Text = "Other"
        End Select
    End If
End With

Set_Framerate

End Sub

Private Sub cmdNewReference_Click()
txtInternalReference.Text = GetNextSequence("internalreference")
End Sub

Private Sub cmdNotifyUsers_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

If MsgBox("You are about to send out Notification messages" & vbCrLf & "to all MW users who have this clip assigned." & vbCrLf & "Proceed?", vbYesNo, "Bulk E-mail Notifications") = vbNo Then Exit Sub

Dim l_lngCompanyID As Long, l_strCetaClientCode As String, l_intHours As Integer, l_intCount As Integer, Count As Long
Dim l_strEmailTo As String, l_strEmailFrom As String, l_strEmailSubject As String, l_strEmailCC As String, l_strEmailCCList As String, l_strEmailBody As String, l_strWelcomeMessage As String

If adoPortalPermission.Recordset.RecordCount > 0 Then

    l_lngCompanyID = Val(lblCompanyID.Caption)
    
    l_strWelcomeMessage = GetData("company", "portalwelcomemessage", "companyID", l_lngCompanyID)
    
    l_strEmailSubject = GetData("company", "portalemailsubject", "companyID", l_lngCompanyID)
    
    l_strEmailSubject = InputBox("Please give the Subject for the Notification Emails", "Bulk Media Window User Email Notification", l_strEmailSubject)
    
TryAgain:
    l_strEmailCCList = InputBox("Please give any CC addresses, separated by semicolons.", "Bulk Media Window User Email Notification", l_strEmailCCList)
    If MsgBox("CC List is: " & l_strEmailCCList & vbCrLf & "Please Confirm", vbYesNo, "Confirm Email CC List") = vbNo Then GoTo TryAgain
    
    adoPortalPermission.Recordset.MoveFirst
    
    Count = 0
    ProgressBar3.Max = adoPortalPermission.Recordset.RecordCount
    ProgressBar3.Value = 0
    ProgressBar3.Visible = True
    DoEvents
    
    Do While Not adoPortalPermission.Recordset.EOF
        ProgressBar3.Value = Count
        DoEvents
        l_strEmailBody = GetData("company", "portalemailbody", "companyID", l_lngCompanyID)
        If l_strEmailBody = "" Then l_strEmailBody = "Material is available for you on the Media Window."
        l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "Media Window User Full Name: " & adoPortalPermission.Recordset("Fullname") & vbCrLf & _
        "Login: " & GetData("portaluser", "username", "portaluserID", adoPortalPermission.Recordset("portaluserID")) & vbCrLf & "Password: " & GetData("portaluser", "password", "portaluserID", adoPortalPermission.Recordset("portaluserID")) & vbCrLf & _
        "Email: " & GetData("portaluser", "email", "portaluserID", adoPortalPermission.Recordset("portaluserID")) & vbCrLf & _
        "Link to Media Window Site: " & GetData("company", "portalwebsiteurl", "companyID", l_lngCompanyID) & vbCrLf & vbCrLf & _
        "This is an informational email. Please do not reply to this email." & vbCrLf
        
        If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/hours=") > 0 Then
            l_intCount = InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/hours=")
            l_intHours = Val(Mid(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), l_intCount + 7))
            If l_intHours > 0 Then
                l_strEmailBody = l_strEmailBody & vbCrLf & "Material will be available for download for " & l_intHours & " hours." & vbCrLf
            End If
        End If
        
        If l_strWelcomeMessage <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & l_strWelcomeMessage
    
        l_strEmailTo = GetData("portaluser", "email", "portaluserID", adoPortalPermission.Recordset("portaluserID"))
        If ValidateEmail(l_strEmailTo) = False Then
            MsgBox "User: " & adoPortalPermission.Recordset("fullname") & " has an invalid email - skipping this notice", vbInformation, "Bad E-mail address"
        Else
        
            l_strEmailFrom = g_strOperationsEmailAddress
            
            If l_strEmailCCList <> "" Then
                l_strEmailCC = l_strEmailCCList & ";" & GetData("portaluser", "administratoremail", "portaluserID", adoPortalPermission.Recordset("portaluserID"))
            Else
                l_strEmailCC = GetData("portaluser", "administratoremail", "portaluserID", adoPortalPermission.Recordset("portaluserID"))
            End If
            
            SendSMTPMail l_strEmailTo, adoPortalPermission.Recordset("fullname"), l_strEmailSubject, "", l_strEmailBody, True, l_strEmailCC, "", g_strAdministratorEmailAddress, True, l_strEmailFrom
        
        End If
        
        adoPortalPermission.Recordset.MoveNext
        Count = Count + 1
    
    Loop
    ProgressBar3.Visible = False

End If

Beep

End Sub

Private Sub cmdOutputExcelRecord_Click()

Dim l_intLinecount As Integer, l_strMatID As String, l_strFilename As String

'Check MatID if Company is set to Kidsco or Movies24
If cmbField2.Text = "" Or cmbField2.Text = "0" Then
    MsgBox "Material ID must be set to output an Excel record", vbCritical, "Problem..."
    Exit Sub
Else
    l_strMatID = cmbField2.Text
End If

If l_strMatID = "" Or l_strMatID = "0" Then
    MsgBox "Could not get MatID for Excel record", vbCritical, "Problem..."
    Exit Sub
End If

If txtTitle.Text = "" Then
    MsgBox "Title must be set to output an Excel record", vbCritical, "Problem..."
    Exit Sub
End If

'Check time entries
If txtDuration.Text = "" Then
    MsgBox "Duration field must be set to output an Excel record", vbCritical, "Problem..."
    Exit Sub
End If

'If txtBreak1.Text <> "" Then
'    If txtBreak1elapsed.Text = "" Then
'        MsgBox "Break 1 duration must be set if Break 1 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak2.Text <> "" Then
'    If txtBreak2elapsed.Text = "" Then
'        MsgBox "Break 2 duration must be set if Break 2 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak3.Text <> "" Then
'    If txtBreak3elapsed.Text = "" Then
'        MsgBox "Break 3 duration must be set if Break 3 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak4.Text <> "" Then
'    If txtBreak4elapsed.Text = "" Then
'        MsgBox "Break 4 duration must be set if Break 4 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak5.Text <> "" Then
'    If txtBreak5elapsed.Text = "" Then
'        MsgBox "Break 5 duration must be set if Break 5 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak6.Text <> "" Then
'    If txtBreak6elapsed.Text = "" Then
'        MsgBox "Break 6 duration must be set if Break 6 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak7.Text <> "" Then
'    If txtBreak7elapsed.Text = "" Then
'        MsgBox "Break 7 duration must be set if Break 7 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak8.Text <> "" Then
'    If txtBreak8elapsed.Text = "" Then
'        MsgBox "Break 8 duration must be set if Break 8 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak9.Text <> "" Then
'    If txtBreak9elapsed.Text = "" Then
'        MsgBox "Break 9 duration must be set if Break 9 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak10.Text <> "" Then
'    If txtBreak10elapsed.Text = "" Then
'        MsgBox "Break 10 duration must be set if Break 10 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak11.Text <> "" Then
'    If txtBreak11elapsed.Text = "" Then
'        MsgBox "Break 11 duration must be set if Break 11 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak12.Text <> "" Then
'    If txtBreak12elapsed.Text = "" Then
'        MsgBox "Break 12 duration must be set if Break 12 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak13.Text <> "" Then
'    If txtBreak13elapsed.Text = "" Then
'        MsgBox "Break 13 duration must be set if Break 13 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak14.Text <> "" Then
'    If txtBreak14elapsed.Text = "" Then
'        MsgBox "Break 14 duration must be set if Break 14 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak15.Text <> "" Then
'    If txtBreak15elapsed.Text = "" Then
'        MsgBox "Break 15 duration must be set if Break 15 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak16.Text <> "" Then
'    If txtBreak16elapsed.Text = "" Then
'        MsgBox "Break 16 duration must be set if Break 16 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak17.Text <> "" Then
'    If txtBreak17elapsed.Text = "" Then
'        MsgBox "Break 17 duration must be set if Break 17 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If
'
'If txtBreak18.Text <> "" Then
'    If txtBreak18elapsed.Text = "" Then
'        MsgBox "Break 18 duration must be set if Break 18 is set to output an Excel record", vbCritical, "Problem..."
'        Exit Sub
'    End If
'End If

l_strFilename = g_strLocationOfOmneonXLS & "\Template.xls"
Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strFilename)
l_strFilename = g_strLocationOfOmneonXLS & "\" & l_strMatID & "_" & txtTitle.Text & "_" & cmbEpisode.Text & ".xls"
On Error GoTo EXIT_FUNCTION
oWorkbook.SaveAs l_strFilename
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_intLinecount = 2

oWorksheet.Cells(l_intLinecount, 1) = txtTitle.Text
oWorksheet.Cells(l_intLinecount, 2) = cmbEpisode.Text

oWorksheet.Cells(l_intLinecount, 3) = l_strMatID
oWorksheet.Cells(l_intLinecount, 4) = txtSubtitle.Text
oWorksheet.Cells(l_intLinecount, 5) = "Full Programme"
oWorksheet.Cells(l_intLinecount, 6) = "00:00:00:00"
oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
oWorksheet.Cells(l_intLinecount, 8) = txtDuration.Text

''Any Breaks - if break 1 then at least 2 parts
'If txtBreak1.Text <> "" Then
'    l_intLinecount = l_intLinecount + 1
'    oWorksheet.Cells(l_intLinecount, 5) = "Part 1"
'    oWorksheet.Cells(l_intLinecount, 6) = "00:00:00:00"
'    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak1elapsed.Text)
'    oWorksheet.Cells(l_intLinecount, 8) = txtBreak1elapsed.Text
'
'    'If break 2 then at least a 3rd part
'    If txtBreak2.Text <> "" Then
'        l_intLinecount = l_intLinecount + 1
'        oWorksheet.Cells(l_intLinecount, 5) = "Part 2"
'        oWorksheet.Cells(l_intLinecount, 6) = txtBreak1elapsed.Text
'        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak2elapsed.Text)
'        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak2elapsed.Text, txtBreak1elapsed.Text, m_Framerate)
'
'        'If break3 then at least a 4th part
'        If txtBreak3.Text <> "" Then
'            l_intLinecount = l_intLinecount + 1
'            oWorksheet.Cells(l_intLinecount, 5) = "Part 3"
'            oWorksheet.Cells(l_intLinecount, 6) = txtBreak2elapsed.Text
'            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak3elapsed.Text)
'            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak3elapsed.Text, txtBreak2elapsed.Text, m_Framerate)
'
'            'If part 4 then at least a 5th part
'            If txtBreak4.Text <> "" Then
'                l_intLinecount = l_intLinecount + 1
'                oWorksheet.Cells(l_intLinecount, 5) = "Part 4"
'                oWorksheet.Cells(l_intLinecount, 6) = txtBreak3elapsed.Text
'                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak4elapsed.Text)
'                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak4elapsed.Text, txtBreak3elapsed.Text, m_Framerate)
'
'                'If break5 then there is a 6th part
'                If txtBreak5.Text <> "" Then
'                    l_intLinecount = l_intLinecount + 1
'                    oWorksheet.Cells(l_intLinecount, 5) = "Part 5"
'                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak4elapsed.Text
'                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak5elapsed.Text)
'                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak5elapsed.Text, txtBreak4elapsed.Text, m_Framerate)
'
'                    'If break6 then there is a 7th part
'                    If txtBreak6.Text <> "" Then
'                        l_intLinecount = l_intLinecount + 1
'                        oWorksheet.Cells(l_intLinecount, 5) = "Part 6"
'                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak5elapsed.Text
'                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak6elapsed.Text)
'                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak6elapsed.Text, txtBreak5elapsed.Text, m_Framerate)
'
'                        'If break7 then there is a 8th part
'                        If txtBreak7.Text <> "" Then
'                            l_intLinecount = l_intLinecount + 1
'                            oWorksheet.Cells(l_intLinecount, 5) = "Part 7"
'                            oWorksheet.Cells(l_intLinecount, 6) = txtBreak6elapsed.Text
'                            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak7elapsed.Text)
'                            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak7elapsed.Text, txtBreak6elapsed.Text, m_Framerate)
'
'                            'If break8 then there is a 9th part
'                            If txtBreak8.Text <> "" Then
'                                l_intLinecount = l_intLinecount + 1
'                                oWorksheet.Cells(l_intLinecount, 5) = "Part 8"
'                                oWorksheet.Cells(l_intLinecount, 6) = txtBreak7elapsed.Text
'                                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak8elapsed.Text)
'                                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak8elapsed.Text, txtBreak7elapsed.Text, m_Framerate)
'
'                                'If break9 then there is a 10th part
'                                If txtBreak9.Text <> "" Then
'                                    l_intLinecount = l_intLinecount + 1
'                                    oWorksheet.Cells(l_intLinecount, 5) = "Part 9"
'                                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak8elapsed.Text
'                                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak9elapsed.Text)
'                                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak9elapsed.Text, txtBreak8elapsed.Text, m_Framerate)
'
'                                    'If break10 then there is a 11th part
'                                    If txtBreak10.Text <> "" Then
'                                        l_intLinecount = l_intLinecount + 1
'                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 10"
'                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak9elapsed.Text
'                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak10elapsed.Text)
'                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak10elapsed.Text, txtBreak9elapsed.Text, m_Framerate)
'
'                                        'If break11 then there is a 12th part
'                                        If txtBreak11.Text <> "" Then
'                                            l_intLinecount = l_intLinecount + 1
'                                            oWorksheet.Cells(l_intLinecount, 5) = "Part 11"
'                                            oWorksheet.Cells(l_intLinecount, 6) = txtBreak10elapsed.Text
'                                            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak11elapsed.Text)
'                                            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak11elapsed.Text, txtBreak10elapsed.Text, m_Framerate)
'
'                                            'If break12 then there is a 13th part
'                                            If txtBreak12.Text <> "" Then
'                                                l_intLinecount = l_intLinecount + 1
'                                                oWorksheet.Cells(l_intLinecount, 5) = "Part 12"
'                                                oWorksheet.Cells(l_intLinecount, 6) = txtBreak11elapsed.Text
'                                                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak12elapsed.Text)
'                                                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak12elapsed.Text, txtBreak11elapsed.Text, m_Framerate)
'
'                                                'If break13 then there is a 14th part
'                                                If txtBreak13.Text <> "" Then
'                                                    l_intLinecount = l_intLinecount + 1
'                                                    oWorksheet.Cells(l_intLinecount, 5) = "Part 13"
'                                                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak12elapsed.Text
'                                                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak13elapsed.Text)
'                                                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak13elapsed.Text, txtBreak12elapsed.Text, m_Framerate)
'
'                                                    'If break14 then there is a 15th part
'                                                    If txtBreak14.Text <> "" Then
'                                                        l_intLinecount = l_intLinecount + 1
'                                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 14"
'                                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak13elapsed.Text
'                                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak14elapsed.Text)
'                                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak14elapsed.Text, txtBreak13elapsed.Text, m_Framerate)
'
'                                                        'If break15 then there is a 16th part
'                                                        If txtBreak15.Text <> "" Then
'                                                            l_intLinecount = l_intLinecount + 1
'                                                            oWorksheet.Cells(l_intLinecount, 5) = "Part 15"
'                                                            oWorksheet.Cells(l_intLinecount, 6) = txtBreak16elapsed.Text
'                                                            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak15elapsed.Text)
'                                                            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak15elapsed.Text, txtBreak14elapsed.Text, m_Framerate)
'
'                                                            'If break16 then there is a 17th part
'                                                            If txtBreak16.Text <> "" Then
'                                                                l_intLinecount = l_intLinecount + 1
'                                                                oWorksheet.Cells(l_intLinecount, 5) = "Part 16"
'                                                                oWorksheet.Cells(l_intLinecount, 6) = txtBreak15elapsed.Text
'                                                                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak16elapsed.Text)
'                                                                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak16elapsed.Text, txtBreak15elapsed.Text, m_Framerate)
                                                    
'                                                                'If break17 then there is a 18th part
'                                                                If txtBreak17.Text <> "" Then
'                                                                    l_intLinecount = l_intLinecount + 1
'                                                                    oWorksheet.Cells(l_intLinecount, 5) = "Part 17"
'                                                                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak16elapsed.Text
'                                                                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak17elapsed.Text)
'                                                                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak17elapsed.Text, txtBreak16elapsed.Text, m_Framerate)
'
'                                                                    'If break18 then there is a 19th part
'                                                                    If txtBreak18.Text <> "" Then
'                                                                        l_intLinecount = l_intLinecount + 1
'                                                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 18"
'                                                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak17elapsed.Text
'                                                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtBreak18elapsed.Text)
'                                                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtBreak18elapsed.Text, txtBreak17elapsed.Text, m_Framerate)
'                                                                        l_intLinecount = l_intLinecount + 1
'                                                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 19"
'                                                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak18elapsed.Text
'                                                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak17elapsed.Text, m_Framerate)
'                                                                    Else
'                                                                        l_intLinecount = l_intLinecount + 1
'                                                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 18"
'                                                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak17elapsed.Text
'                                                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak17elapsed.Text, m_Framerate)
'                                                                    End If
'
'                                                                Else
'                                                                    l_intLinecount = l_intLinecount + 1
'                                                                    oWorksheet.Cells(l_intLinecount, 5) = "Part 17"
'                                                                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak16elapsed.Text
'                                                                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak16elapsed.Text, m_Framerate)
'                                                                End If
                                                
'                                                            Else
'                                                                l_intLinecount = l_intLinecount + 1
'                                                                oWorksheet.Cells(l_intLinecount, 5) = "Part 16"
'                                                                oWorksheet.Cells(l_intLinecount, 6) = txtBreak15elapsed.Text
'                                                                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak15elapsed.Text, m_Framerate)
'                                                            End If
'
'                                                        Else
'                                                            l_intLinecount = l_intLinecount + 1
'                                                            oWorksheet.Cells(l_intLinecount, 5) = "Part 15"
'                                                            oWorksheet.Cells(l_intLinecount, 6) = txtBreak14elapsed.Text
'                                                            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak14elapsed.Text, m_Framerate)
''                                                        End If
'
'                                                    Else
'                                                        l_intLinecount = l_intLinecount + 1
'                                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 14"
'                                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak13elapsed.Text
'                                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak13elapsed.Text, m_Framerate)
'                                                    End If
'
'                                                Else
'                                                    l_intLinecount = l_intLinecount + 1
'                                                    oWorksheet.Cells(l_intLinecount, 5) = "Part 13"
'                                                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak12elapsed.Text
'                                                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak12elapsed.Text, m_Framerate)
'                                                End If
'
'                                            Else
'                                                l_intLinecount = l_intLinecount + 1
'                                                oWorksheet.Cells(l_intLinecount, 5) = "Part 12"
'                                                oWorksheet.Cells(l_intLinecount, 6) = txtBreak11elapsed.Text
'                                                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak11elapsed.Text, m_Framerate)
'                                            End If
'
'                                        Else
'                                            l_intLinecount = l_intLinecount + 1
'                                            oWorksheet.Cells(l_intLinecount, 5) = "Part 11"
'                                            oWorksheet.Cells(l_intLinecount, 6) = txtBreak10elapsed.Text
'                                            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak10elapsed.Text, m_Framerate)
'                                        End If
'
'                                    Else
'                                        l_intLinecount = l_intLinecount + 1
'                                        oWorksheet.Cells(l_intLinecount, 5) = "Part 10"
'                                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak9elapsed.Text
'                                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak9elapsed.Text, m_Framerate)
'                                    End If
'
'                                Else
'                                    l_intLinecount = l_intLinecount + 1
'                                    oWorksheet.Cells(l_intLinecount, 5) = "Part 9"
'                                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak8elapsed.Text
'                                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak8elapsed.Text, m_Framerate)
'                                End If
'
'                            Else
'                                l_intLinecount = l_intLinecount + 1
'                                oWorksheet.Cells(l_intLinecount, 5) = "Part 8"
'                                oWorksheet.Cells(l_intLinecount, 6) = txtBreak7elapsed.Text
'                                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak7elapsed.Text, m_Framerate)
'                            End If
'
'                        Else
'                            l_intLinecount = l_intLinecount + 1
'                            oWorksheet.Cells(l_intLinecount, 5) = "Part 7"
'                            oWorksheet.Cells(l_intLinecount, 6) = txtBreak6elapsed.Text
'                            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak6elapsed.Text, m_Framerate)
'                        End If
'
'                    Else
'                        l_intLinecount = l_intLinecount + 1
'                        oWorksheet.Cells(l_intLinecount, 5) = "Part 6"
'                        oWorksheet.Cells(l_intLinecount, 6) = txtBreak5elapsed.Text
'                        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak5elapsed.Text, m_Framerate)
'                    End If
'
'                Else
'                    l_intLinecount = l_intLinecount + 1
'                    oWorksheet.Cells(l_intLinecount, 5) = "Part 5"
'                    oWorksheet.Cells(l_intLinecount, 6) = txtBreak4elapsed.Text
'                    oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                    oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak4elapsed.Text, m_Framerate)
'                End If
'
'            Else
'                l_intLinecount = l_intLinecount + 1
'                oWorksheet.Cells(l_intLinecount, 5) = "Part 4"
'                oWorksheet.Cells(l_intLinecount, 6) = txtBreak3elapsed.Text
'                oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'                oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak3elapsed.Text, m_Framerate)
'            End If
'
'        Else
'            l_intLinecount = l_intLinecount + 1
'            oWorksheet.Cells(l_intLinecount, 5) = "Part 3"
'            oWorksheet.Cells(l_intLinecount, 6) = txtBreak2elapsed.Text
'            oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'            oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak2elapsed.Text, m_Framerate)
'        End If
'
'    Else
'        l_intLinecount = l_intLinecount + 1
'        oWorksheet.Cells(l_intLinecount, 5) = "Part 2"
'        oWorksheet.Cells(l_intLinecount, 6) = txtBreak1elapsed.Text
'        oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
'        oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtBreak1elapsed.Text, m_Framerate)
'    End If
'End If
    
l_intLinecount = l_intLinecount + 1

oWorksheet.Cells(l_intLinecount, 5) = "End Credits"
oWorksheet.Cells(l_intLinecount, 6) = txtEndCreditsElapsed.Text
oWorksheet.Cells(l_intLinecount, 7) = FrameBefore(txtDuration.Text)
oWorksheet.Cells(l_intLinecount, 8) = Timecode_Subtract(txtDuration.Text, txtEndCreditsElapsed.Text, m_Framerate)

oWorkbook.Save
Set oWorksheet = Nothing

EXIT_FUNCTION:

oWorkbook.Close vbNo
Set oWorkbook = Nothing
oExcel.Quit

Beep

End Sub

Private Sub cmdPlay_Click()

Dim l_strNetworkPath As String, l_lngFilenumber As Long, l_curFileSize As Currency, CommandString As String

'Check that the alt location hasn't got a drive letter or a UNC path at the start of it.
If txtAltFolder.Text Like "\\*" Or Mid(txtAltFolder.Text, 2, 1) = ":" Then
    MsgBox "Alt Location has either drive letter or UNC network reference. Please fix this", vbOKOnly, "Problem..."
    Exit Sub
End If

If ((g_blnRedNetwork = True) Or (Val(lblLibraryID.Caption) = 796559)) And GetData("library", "format", "libraryID", lblLibraryID.Caption) = "DISCSTORE" Then
     
    l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
    l_strNetworkPath = Replace(l_strNetworkPath, "\\", "\\?\UNC\")
    If txtAltFolder.Text <> "" Then
        l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
    End If
    
    ShellExecute 0&, "open", l_strNetworkPath & "\" & txtClipfilename.Text, vbNullString, l_strNetworkPath, vbNormalFocus
        
Else

    MsgBox "Cannot play that file from it's current location on this machine", vbOKOnly, "Attempting to play non-preview clip"
    
End If

End Sub

Private Sub cmdPrint_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

'PromptClipChanges Val(txtClipID.Text)

If Val(txtClipID.Text) = 0 Then
    NoClipSelectedMessage
    Exit Sub
End If

If Not CheckAccess("/printrecordreport") Then Exit Sub

l_strSelectionFormula = "{events.eventID} = " & txtClipID.Text
If MsgBox("Is this an ESI excel", vbYesNo, "Report Type") = vbYes Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "MediaReportESI.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "MediaReport.rpt"
End If

PrintCrystalReport l_strReportFile, l_strSelectionFormula, g_blnPreviewReport

End Sub


Private Sub cmdReplay_Click()
If Val(txtClipID.Text) <> 0 Then
    ReplayClip
Else
    MsgBox "Clip must be saved to duplicate or Transcode/Replay", vbCritical, "Cannot Duplicate"
End If
End Sub

Private Sub cmdSave_Click()

Dim l_strTest As String
txtTitle.Text = Trim(Replace(txtTitle.Text, vbCrLf, ""))
txtSeriesID.Text = Trim(Replace(txtSeriesID.Text, vbCrLf, ""))

If lblAudioConfiguration.Caption <> "" And InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/dadctracker") > 0 Then
    If cmbAudioType1.Text <> "" And cmbAudioContent1.Text <> "MOS" Then
        If cmbAudioContent1.Text = "M&E" Then
            l_strTest = cmbAudioType1.Text & ", M&&E"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Music" Or cmbAudioContent1.Text = "Effects" Then
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Partial M&E" Then
            l_strTest = cmbAudioType1.Text & ", Partial M&&E"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent1.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType1.Text & ", Mix Minus Narration"
            If cmbAudioLanguage1.Text <> "None" Then
                MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage1.Text, "(") Then
'            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & Left(cmbAudioLanguage1.Text, InStr(cmbAudioLanguage1.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType1.Text & ", " & cmbAudioContent1.Text & ", " & cmbAudioLanguage1.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 1 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType2.Text <> "" And cmbAudioContent2.Text <> "MOS" Then
        If cmbAudioContent2.Text = "M&E" Then
            l_strTest = cmbAudioType2.Text & ", M&&E"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Music" Or cmbAudioContent2.Text = "Effects" Then
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Partial M&E" Then
            l_strTest = cmbAudioType2.Text & ", Partial M&&E"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent2.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType2.Text & ", Mix Minus Narration"
            If cmbAudioLanguage2.Text <> "None" Then
                MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage2.Text, "(") Then
'            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & Left(cmbAudioLanguage2.Text, InStr(cmbAudioLanguage2.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType2.Text & ", " & cmbAudioContent2.Text & ", " & cmbAudioLanguage2.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 2 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType3.Text <> "" And cmbAudioContent3.Text <> "MOS" Then
        If cmbAudioContent3.Text = "M&E" Then
            l_strTest = cmbAudioType3.Text & ", M&&E"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Music" Or cmbAudioContent3.Text = "Effects" Then
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Partial M&E" Then
            l_strTest = cmbAudioType3.Text & ", Partial M&&E"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent3.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType3.Text & ", Mix Minus Narration"
            If cmbAudioLanguage3.Text <> "None" Then
                MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage3.Text, "(") Then
'            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & Left(cmbAudioLanguage3.Text, InStr(cmbAudioLanguage3.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType3.Text & ", " & cmbAudioContent3.Text & ", " & cmbAudioLanguage3.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 3 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType4.Text <> "" And cmbAudioContent4.Text <> "MOS" Then
        If cmbAudioContent4.Text = "M&E" Then
            l_strTest = cmbAudioType4.Text & ", M&&E"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Music" Or cmbAudioContent4.Text = "Effects" Then
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Partial M&E" Then
            l_strTest = cmbAudioType4.Text & ", Partial M&&E"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent4.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType4.Text & ", Mix Minus Narration"
            If cmbAudioLanguage4.Text <> "None" Then
                MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage4.Text, "(") Then
'            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & Left(cmbAudioLanguage4.Text, InStr(cmbAudioLanguage4.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType4.Text & ", " & cmbAudioContent4.Text & ", " & cmbAudioLanguage4.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 4 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType5.Text <> "" And cmbAudioContent5.Text <> "MOS" Then
        If cmbAudioContent5.Text = "M&E" Then
            l_strTest = cmbAudioType5.Text & ", M&&E"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Music" Or cmbAudioContent5.Text = "Effects" Then
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Partial M&E" Then
            l_strTest = cmbAudioType5.Text & ", Partial M&&E"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent5.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType5.Text & ", Mix Minus Narration"
            If cmbAudioLanguage5.Text <> "None" Then
                MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage5.Text, "(") Then
'            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & Left(cmbAudioLanguage5.Text, InStr(cmbAudioLanguage5.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType5.Text & ", " & cmbAudioContent5.Text & ", " & cmbAudioLanguage5.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 5 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType6.Text <> "" And cmbAudioContent6.Text <> "MOS" Then
        If cmbAudioContent6.Text = "M&E" Then
            l_strTest = cmbAudioType6.Text & ", M&&E"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Music" Or cmbAudioContent6.Text = "Effects" Then
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Partial M&E" Then
            l_strTest = cmbAudioType6.Text & ", Partial M&&E"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent6.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType6.Text & ", Mix Minus Narration"
            If cmbAudioLanguage6.Text <> "None" Then
                MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage6.Text, "(") Then
'            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & Left(cmbAudioLanguage6.Text, InStr(cmbAudioLanguage6.Text, "("))
        Else
            l_strTest = cmbAudioType6.Text & ", " & cmbAudioContent6.Text & ", " & cmbAudioLanguage6.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 6 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType7.Text <> "" And cmbAudioContent7.Text <> "MOS" Then
        If cmbAudioContent7.Text = "M&E" Then
            l_strTest = cmbAudioType7.Text & ", M&&E"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Music" Or cmbAudioContent7.Text = "Effects" Then
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Partial M&E" Then
            l_strTest = cmbAudioType7.Text & ", Partial M&&E"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent7.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType7.Text & ", Mix Minus Narration"
            If cmbAudioLanguage7.Text <> "None" Then
                MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage7.Text, "(") Then
'            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & Left(cmbAudioLanguage7.Text, InStr(cmbAudioLanguage7.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType7.Text & ", " & cmbAudioContent7.Text & ", " & cmbAudioLanguage7.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 7 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
    
    If cmbAudioType8.Text <> "" And cmbAudioContent8.Text <> "MOS" Then
        If cmbAudioContent8.Text = "M&E" Then
            l_strTest = cmbAudioType8.Text & ", M&&E"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Music" Or cmbAudioContent8.Text = "Effects" Then
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Partial M&E" Then
            l_strTest = cmbAudioType8.Text & ", Partial M&&E"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
        ElseIf cmbAudioContent8.Text = "Mix Minus Narration" Then
            l_strTest = cmbAudioType8.Text & ", Mix Minus Narration"
            If cmbAudioLanguage8.Text <> "None" Then
                MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
                Exit Sub
            End If
'        ElseIf InStr(cmbAudioLanguage8.Text, "(") Then
'            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & Left(cmbAudioLanguage8.Text, InStr(cmbAudioLanguage8.Text, "(") - 2)
        Else
            l_strTest = cmbAudioType8.Text & ", " & cmbAudioContent8.Text & ", " & cmbAudioLanguage8.Text
        End If
        If InStr(lblAudioConfiguration.Caption, l_strTest) <= 0 Then
            MsgBox "Audio Set 8 lists an item that is not in the DADC Requirements." & vbCrLf & "Clip Not Saved.", vbCritical, "Error..."
            Exit Sub
        End If
    End If
End If

If SaveClip(chkIgnoreJobID.Value, m_strHistory) = True Then
    ShowClipControl Val(txtClipID.Text)
Else
    MsgBox "Clip Not Saved", vbCritical, "Problem"
End If

End Sub

Private Sub cmdSegment_Click()

If txtClipID.Text <> "" And txtTimecodeStart.Text <> "" Then
    frmClipSegment.lblClipID.Caption = txtClipID.Text
    frmClipSegment.Show vbModal
End If

Unload frmClipSegment

End Sub

Private Sub cmdTechRev_Click()

If Val(txtClipID.Text) = 0 Or txtClipfilename.Text = "" Or lblFileSize.Caption = "" Then
    MsgBox "Please ensure that files are saved and verified before creating Tech Reviews", vbInformation, "Error"
    If MsgBox("Do you wish to continue to view an old tech review", vbYesNo) = vbNo Then
        Exit Sub
    End If
End If

If txtMD5Checksum.Text = "" Then
    MsgBox "There is no MD5 checksum made for this file." & vbCrLf & "CETA will request making the MD5." & vbCrLf & "Please wait until this has completed before doing your tech review.", vbInformation, "MD5 checksum not found"
    If GetDataSQL("SELECT TOP 1 event_file_requestID FROM Event_File_Request WHERE eventID = " & txtClipID.Text & " AND event_file_request_typeID = 8 AND requestcomplete IS NULL") <> 0 Then
        MsgBox "The existing MD5 Request is still not finished"
        Unload Me
        Exit Sub
    Else
        cmdMD5Request.Value = True
        Exit Sub
    End If
End If

frmFileTechRev.Show
frmFileTechRev.ZOrder 0

End Sub

Private Sub cmdUpdateLogging_Click()

If Val(txtClipID.Text) = 0 Or Val(lblCompanyID.Caption) = 0 Then Exit Sub

If MsgBox("About to replace all referenced clips logging data with the data for this clip and framerate." & vbCrLf & "Are you Sure", vbYesNo, "Replacing Loigging Data") = vbNo Then Exit Sub

Dim l_strSQL As String, l_rsClips As ADODB.Recordset, l_lngClipID As Long, l_rsLogs As ADODB.Recordset, Count As Long

l_strSQL = "SELECT eventID FROM events WHERE clipreference = '" & QuoteSanitise(txtReference.Text) & "' AND companyID = " & Val(lblCompanyID.Caption) & " AND eventID <> " & txtClipID.Text & " AND clipframerate = '" & cmbFrameRate.Text & "';"

Set l_rsClips = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rsClips.RecordCount > 0 Then
    
    Count = 0
    ProgressBar3.Max = l_rsClips.RecordCount
    ProgressBar3.Value = 0
    ProgressBar3.Visible = True
    DoEvents
    
    l_rsClips.MoveFirst
    Do While Not l_rsClips.EOF
        ProgressBar3.Value = Count
        DoEvents
        l_strSQL = "DELETE FROM eventlogging WHERE eventID = " & l_rsClips("eventID") & ";"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        Set l_rsLogs = ExecuteSQL(adoLogging.RecordSource, g_strExecuteError)
        CheckForSQLError
        If l_rsLogs.RecordCount > 0 Then
            l_rsLogs.MoveFirst
            Do While Not l_rsLogs.EOF
                l_strSQL = "INSERT INTO eventlogging (eventID, timecodestart, timecodestop, segmentreference) VALUES ("
                l_strSQL = l_strSQL & l_rsClips("eventID") & ", "
                l_strSQL = l_strSQL & "'" & l_rsLogs("timecodestart") & "', "
                l_strSQL = l_strSQL & "'" & l_rsLogs("timecodestop") & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rsLogs("segmentreference")) & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_rsLogs.MoveNext
            Loop
        End If
        l_rsLogs.Close
        Count = Count + 1
        l_rsClips.MoveNext
    Loop
    ProgressBar3.Visible = False
End If

l_rsClips.Close

Set l_rsLogs = Nothing
Set l_rsClips = Nothing

End Sub

Private Sub cmdUpdateMetadata_Click()

Dim l_strSQL As String

If txtClipID.Text <> "" And txtInternalReference.Text <> "" And txtReference.Text <> "" And lblCompanyID.Caption <> "" Then

    If MsgBox("Update Production Metadata on ALL CLIPS with this Reference." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub

    l_strSQL = "UPDATE events SET "
    l_strSQL = l_strSQL & "eventtitle = '" & QuoteSanitise(txtTitle.Text) & "', "
    l_strSQL = l_strSQL & "eventsubtitle = '" & QuoteSanitise(txtSubtitle.Text) & "', "
    l_strSQL = l_strSQL & "eventseries = '" & cmbSeries.Text & "', "
    l_strSQL = l_strSQL & "eventset = '" & cmbSet.Text & "', "
    l_strSQL = l_strSQL & "eventepisode = '" & cmbEpisode.Text & "', "
    l_strSQL = l_strSQL & "eventversion = '" & QuoteSanitise(cmbVersion.Text) & "', "
    l_strSQL = l_strSQL & "clocknumber = '" & QuoteSanitise(txtClockNumber.Text) & "', "
    l_strSQL = l_strSQL & "customfield1 = '" & QuoteSanitise(cmbField1.Text) & "', "
    l_strSQL = l_strSQL & "customfield2 = '" & QuoteSanitise(cmbField2.Text) & "', "
    l_strSQL = l_strSQL & "customfield3 = '" & QuoteSanitise(cmbField3.Text) & "', "
    l_strSQL = l_strSQL & "customfield4 = '" & QuoteSanitise(cmbField4.Text) & "', "
    l_strSQL = l_strSQL & "customfield5 = '" & QuoteSanitise(cmbField5.Text) & "', "
    l_strSQL = l_strSQL & "customfield6 = '" & QuoteSanitise(cmbField6.Text) & "', "
    l_strSQL = l_strSQL & "eventkeyframefilename = '" & QuoteSanitise(txtKeyframeFilename.Text) & "' "

    l_strSQL = l_strSQL & "WHERE clipreference = '" & QuoteSanitise(txtReference.Text) & "' and companyID = " & Val(lblCompanyID.Caption) & ";"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    ShowClipControl Val(txtClipID.Text)
    
End If

End Sub

Private Sub cmdUpdateRefKeyframe_Click()

Dim l_strSQL As String

If txtClipID.Text <> "" And txtInternalReference.Text <> "" And txtReference.Text <> "" And lblCompanyID.Caption <> "" Then

    If MsgBox("Update Keyframe and Flash Preview on ALL CLIPS with this Reference." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub
    
    l_strSQL = "UPDATE events SET "
    l_strSQL = l_strSQL & "eventkeyframefilename = '" & txtKeyframeFilename.Text & "', "
    l_strSQL = l_strSQL & "eventkeyframetimecode = '" & txtTimecodeKeyframe.Text & "' "

    l_strSQL = l_strSQL & "WHERE clipreference = '" & QuoteSanitise(txtReference.Text) & "' and companyID = " & Val(lblCompanyID.Caption) & " AND clipframerate = '" & cmbFrameRate.Text & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    ShowClipControl Val(txtClipID.Text)
    
End If

End Sub

'Private Sub cmdUpdateSvenskAudio_Click()
'
'Dim l_lngClipID As Long
'Dim l_strSQL As String
'
'If Val(txtClipID.Text) = 0 Then Exit Sub
'
'l_lngClipID = Val(txtClipID.Text)
'
'If txtClipID.Text <> "" And txtSvenskProjectNumber.Text <> "" And lblCompanyID.Caption <> "" Then
'
'  '  If MsgBox("Update timings on ALL CLIPS with this Reference." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub
'
'    l_strSQL = "UPDATE events SET "
'
'    l_strSQL = l_strSQL & "audiotypegroup1 = '" & cmbAudioType1.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup2 = '" & cmbAudioType2.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup3 = '" & cmbAudioType3.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup4 = '" & cmbAudioType4.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup5 = '" & cmbAudioType5.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup6 = '" & cmbAudioType6.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup7 = '" & cmbAudioType7.Text & "', "
'    l_strSQL = l_strSQL & "audiotypegroup8 = '" & cmbAudioType8.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup1 = '" & cmbAudioContent1.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup2 = '" & cmbAudioContent2.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup3 = '" & cmbAudioContent3.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup4 = '" & cmbAudioContent4.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup5 = '" & cmbAudioContent5.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup6 = '" & cmbAudioContent6.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup7 = '" & cmbAudioContent7.Text & "', "
'    l_strSQL = l_strSQL & "audiocontentgroup8 = '" & cmbAudioContent8.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup1 = '" & cmbAudioLanguage1.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup2 = '" & cmbAudioLanguage2.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup3 = '" & cmbAudioLanguage3.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup4 = '" & cmbAudioLanguage4.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup5 = '" & cmbAudioLanguage5.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup6 = '" & cmbAudioLanguage6.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup7 = '" & cmbAudioLanguage7.Text & "', "
'    l_strSQL = l_strSQL & "audiolanguagegroup8 = '" & cmbAudioLanguage8.Text & "', "
'
'    l_strSQL = l_strSQL & "sound_stereo_main = '" & chkStereoMain.Value & "', "
'    l_strSQL = l_strSQL & "sound_stereo_me = '" & chkStereoME.Value & "', "
'    l_strSQL = l_strSQL & "sound_ltrt_main = '" & chkLtRtMain.Value & "', "
'    l_strSQL = l_strSQL & "sound_ltrt_me = '" & chkLtRtME.Value & "', "
'    l_strSQL = l_strSQL & "sound_surround_main = '" & chkSurroundMain.Value & "', "
'    l_strSQL = l_strSQL & "sound_surround_me = '" & chkSurroundME.Value & "', "
''    l_strSQL = l_strSQL & "sound_other = '" & chkOtherSound.Value & "', "
''    l_strSQL = l_strSQL & "sound_stereo_mmn = '" & chkStereoMMN.Value & "', "
''    l_strSQL = l_strSQL & "sound_surround_mmn = '" & chkSurroundMMN.Value & "' "
'
'    l_strSQL = l_strSQL & " WHERE companyID = " & Val(lblCompanyID.Caption) & " AND svenskprojectnumber = '" & txtSvenskProjectNumber.Text & "';"
'
'    ExecuteSQL l_strSQL, g_strExecuteError
'    CheckForSQLError
'
'    ShowClipControl Val(txtClipID.Text)
'
'ElseIf txtSvenskProjectNumber.Text = "" Then
'
'    MsgBox ("There is not a Project number associated with this clip therefore updates not saved")
'
'ElseIf lblCompanyID.Caption = "" Then
'
'    MsgBox ("There is not a company selected therefore updates not saved")
'
'End If
'
'End Sub
'
Private Sub cmdUpdateTimings_Click()

Dim l_strSQL As String

If txtClipID.Text <> "" And txtInternalReference.Text <> "" And txtReference.Text <> "" And lblCompanyID.Caption <> "" Then

    If MsgBox("Update timings on ALL CLIPS with this Reference." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub
    
    l_strSQL = "UPDATE events SET "
    l_strSQL = l_strSQL & "timecodestart = '" & txtTimecodeStart.Text & "', "
    l_strSQL = l_strSQL & "timecodestop = '" & txtTimecodeStop.Text & "', "
    l_strSQL = l_strSQL & "fd_length = '" & txtDuration.Text & "', "
    l_strSQL = l_strSQL & "eventkeyframetimecode = '" & txtTimecodeKeyframe.Text & "', "
'    l_strSQL = l_strSQL & "break1 = '" & txtBreak1.Text & "', "
'    l_strSQL = l_strSQL & "break2 = '" & txtBreak2.Text & "', "
'    l_strSQL = l_strSQL & "break3 = '" & txtBreak3.Text & "', "
'    l_strSQL = l_strSQL & "break4 = '" & txtBreak4.Text & "', "
'    l_strSQL = l_strSQL & "break5 = '" & txtBreak5.Text & "', "
'    l_strSQL = l_strSQL & "break6 = '" & txtBreak6.Text & "', "
'    l_strSQL = l_strSQL & "break7 = '" & txtBreak7.Text & "', "
'    l_strSQL = l_strSQL & "break8 = '" & txtBreak8.Text & "', "
'    l_strSQL = l_strSQL & "break9 = '" & txtBreak9.Text & "', "
'    l_strSQL = l_strSQL & "break10 = '" & txtBreak10.Text & "', "
'    l_strSQL = l_strSQL & "break11 = '" & txtBreak11.Text & "', "
'    l_strSQL = l_strSQL & "break12 = '" & txtBreak12.Text & "', "
'    l_strSQL = l_strSQL & "break13 = '" & txtBreak13.Text & "', "
'    l_strSQL = l_strSQL & "break14 = '" & txtBreak14.Text & "', "
'    l_strSQL = l_strSQL & "break15 = '" & txtBreak15.Text & "', "
'    l_strSQL = l_strSQL & "break16 = '" & txtBreak16.Text & "', "
'    l_strSQL = l_strSQL & "break17 = '" & txtBreak17.Text & "', "
'    l_strSQL = l_strSQL & "break18 = '" & txtBreak18.Text & "', "
'    l_strSQL = l_strSQL & "break1elapsed = '" & txtBreak1elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break2elapsed = '" & txtBreak2elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break3elapsed = '" & txtBreak3elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break4elapsed = '" & txtBreak4elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break5elapsed = '" & txtBreak5elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break6elapsed = '" & txtBreak6elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break7elapsed = '" & txtBreak7elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break8elapsed = '" & txtBreak8elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break9elapsed = '" & txtBreak9elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break10elapsed = '" & txtBreak10elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break11elapsed = '" & txtBreak11elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break12elapsed = '" & txtBreak12elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break13elapsed = '" & txtBreak13elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break14elapsed = '" & txtBreak14elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break15elapsed = '" & txtBreak15elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break16elapsed = '" & txtBreak16elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break17elapsed = '" & txtBreak17elapsed.Text & "', "
'    l_strSQL = l_strSQL & "break18elapsed = '" & txtBreak18elapsed.Text & "', "
    l_strSQL = l_strSQL & "endcredits = '" & txtEndCredits.Text & "', "
    l_strSQL = l_strSQL & "endcreditselapsed = '" & txtEndCreditsElapsed.Text & "' "

    l_strSQL = l_strSQL & "WHERE clipreference = '" & QuoteSanitise(txtReference.Text) & "' AND companyID = " & Val(lblCompanyID.Caption) & " AND clipframerate = '" & cmbFrameRate.Text & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    ShowClipControl Val(txtClipID.Text)
    
End If

End Sub

Private Sub cmdVerifyKeyframe_Click()

Dim FSO As Scripting.FileSystemObject

If txtKeyframeFilename.Text = "" Then Exit Sub

If GetData("library", "format", "libraryID", lblLibraryID.Caption) = "DISCSTORE" Then

    Dim l_strNetworkPath As String, l_strDirectoryResult As String
    
    l_strNetworkPath = g_strKeyframeStore
    
    l_strNetworkPath = l_strNetworkPath & "\" & lblCompanyID.Caption
    
    l_strNetworkPath = l_strNetworkPath & "\" & txtKeyframeFilename.Text
    
    Set FSO = New Scripting.FileSystemObject
    
    If txtKeyframeFilename.Text <> "" Then
        If FSO.FileExists(l_strNetworkPath) Then
            MsgBox "Thumbnail Filename Verified", vbOKOnly, "Thumbnail Filename Verification"
            picKeyframe.Picture = LoadPicture(l_strNetworkPath)
            picKeyframe.Refresh
            picKeyframe.Visible = True
        Else
            MsgBox "Keyframe Filename and path did not verify", vbCritical, "Keyframe Filename Verification"
            picKeyframe.Visible = False
        End If
    Else
        MsgBox "Keyframe Filename and path did not verify", vbCritical, "Keyframe Filename Verification"
        picKeyframe.Visible = False
    End If
End If

End Sub

Private Sub cmdVerifyClip_Click()

VerifyClip txtClipID.Text, txtAltFolder.Text, txtClipfilename.Text, lblLibraryID.Caption, False, False

End Sub

Private Sub cmdVisualQuery_Click()

If Val(txtClipID.Text) = 0 Then Exit Sub

Dim l_strNetworkPath As String

Screen.MousePointer = vbHourglass
l_strNetworkPath = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
l_strNetworkPath = Replace(l_strNetworkPath, "\\", "\\?\UNC\")
If txtAltFolder.Text <> "" Then l_strNetworkPath = l_strNetworkPath & "\" & txtAltFolder.Text
l_strNetworkPath = l_strNetworkPath & "\" & txtClipfilename.Text


frmMediaInfo.lblFilePath.Caption = l_strNetworkPath
frmMediaInfo.lblFileName.Caption = txtClipfilename.Text
frmMediaInfo.Show vbModal

End Sub

Private Sub cmdMakeKeyframeFromImage_Click()

Dim l_lngOriginalImageHandle As Long, l_lngNewImageHandle As Long, l_lngCount As Long
Dim l_lngWidth As Long, l_lngHeight As Long, l_dblScaleWidth As Double, l_dblScaleHeight As Double

If lblFormat.Caption <> "DISCSTORE" Then
    MsgBox "Cannot make Thumbnails unless file is on a DISCSTORE"
    Exit Sub
End If

Select Case UCase(Right(txtClipfilename.Text, 4))

    Case ".JPG"
        l_lngOriginalImageHandle = FreeImage_Load(FIF_JPEG, GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption)) & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text, 0)
    
    Case ".BMP"
        l_lngOriginalImageHandle = FreeImage_Load(FIF_BMP, GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption)) & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text, 0)
    
    Case ".GIF"
        l_lngOriginalImageHandle = FreeImage_Load(FIF_GIF, GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption)) & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text, 0)
        
    Case ".PNG"
        l_lngOriginalImageHandle = FreeImage_Load(FIF_PNG, GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption)) & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text, PNG_IGNOREGAMMA)
    
    Case ".TIF"
        l_lngOriginalImageHandle = FreeImage_Load(FIF_TIFF, GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption)) & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text, 0)
    
    Case ".TGA"
        l_lngOriginalImageHandle = FreeImage_Load(FIF_TARGA, GetData("library", "subtitle", "libraryID", Val(lblLibraryID.Caption)) & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text, 0)
    
    Case Else
        MsgBox "File is of a format that cannot be handled. You will have to make the thumbnail another way"
        Exit Sub
End Select
    
    
' Load the original image
l_lngWidth = FreeImage_GetWidth(l_lngOriginalImageHandle)
l_lngHeight = FreeImage_GetHeight(l_lngOriginalImageHandle)

l_dblScaleWidth = 320 / l_lngWidth
l_dblScaleHeight = 180 / l_lngHeight

If l_dblScaleWidth < l_dblScaleHeight Then
    l_lngNewImageHandle = FreeImage_RescaleByFactor(l_lngOriginalImageHandle, l_dblScaleWidth, l_dblScaleWidth, True)
Else
    l_lngNewImageHandle = FreeImage_RescaleByFactor(l_lngOriginalImageHandle, l_dblScaleHeight, l_dblScaleHeight, True)
End If

l_lngCount = InStr(txtClipfilename.Text, ".")
Do While InStr(l_lngCount + 1, txtClipfilename.Text, ".") > 0
    l_lngCount = InStr(l_lngCount, txtClipfilename.Text, ".")
Loop
'MsgBox Count & " - " & txtClipfilename.Text & " - " & Left(txtClipfilename.Text, l_lngCount - 1)
' Save this image as JPEG in the Grabs folder
If FreeImage_Save(FIF_JPEG, l_lngNewImageHandle, "\\jcaweb\Keyframes\" & lblCompanyID & "\" & Left(txtClipfilename.Text, l_lngCount - 1) & ".jpg", JPEG_BASELINE) Then
    txtKeyframeFilename.Text = Left(txtClipfilename.Text, l_lngCount - 1) & ".jpg"
End If

' Unload the new image
FreeImage_Unload (l_lngNewImageHandle)

End Sub

Private Sub CorrectEventWebloggingRecords()

Dim l_rst As ADODB.Recordset, l_txtTimecodeStart As String, l_txtFrameRate As String

Set l_rst = ExecuteSQL("SELECT * FROM eventweblogging", g_strExecuteError)

If l_rst.RecordCount > 0 Then
    l_rst.MoveFirst
    Do While Not l_rst.EOF
        l_txtTimecodeStart = GetDataSQL("SELECT TOP 1 timecodestart FROM events WHERE clipreference = '" & l_rst("clipreference") & "' AND clipframerate = '" & l_rst("framerate") & "' AND companyID = " & l_rst("CompanyID") & " AND system_deleted = 0 AND bigfilesize IS NOT NULL and bigfilesize <> 0;")
        l_txtFrameRate = l_rst("framerate")
        Select Case l_txtFrameRate
        
            Case "25"
                m_Framerate = TC_25
            Case "23.98"
                m_Framerate = TC_24
            Case "29.97"
                m_Framerate = TC_29
            Case Else
                m_Framerate = TC_25
        
        End Select
        If l_txtTimecodeStart <> "" Then
            l_rst("Seconds_In") = Seconds_From_Timecode(Timecode_Subtract(l_rst("timecodestart"), l_txtTimecodeStart, m_Framerate), m_Framerate)
            l_rst("Seconds_Out") = Seconds_From_Timecode(Timecode_Subtract(l_rst("timecodestop"), l_txtTimecodeStart, m_Framerate), m_Framerate)
            l_rst.Update
        End If
        l_rst.MoveNext
    Loop
End If
l_rst.Close

Beep

End Sub

Private Sub ddnKeyword_Click()

If Val(txtClipID.Text) <> 0 Then
    grdKeyword.Columns("keywordtext").Text = ddnKeyword.Columns("keywordtext").Text
End If

End Sub

Private Sub ddnPortalUsers_Click()

If Val(txtClipID.Text) <> 0 Then
    grdPortalPermission.Columns("portaluserID").Text = ddnPortalUsers.Columns("portaluserID").Text
    grdPortalPermission.Columns("fullname").Text = ddnPortalUsers.Columns("fullname").Text
End If

End Sub

Private Sub Form_Activate()

If Not CheckAccess("/mediainfo", True) Then
    cmdVisualQuery.Visible = False
End If

If CheckAccess("/SuperUser", True) Then
    cmdEditSize.Visible = True
Else
    cmdEditSize.Visible = False
End If

If Not CheckAccess("/mediainfo", True) Then
    cmdMediaInfo.Visible = False
End If

If Not CheckAccess("/mediainfo", True) Then
    cmdMediaInfoPreserveTimecode.Visible = False
End If

If InStr(GetData("xref", "descriptionalias", "description", CurrentMachineName), "/pipeline") = 0 Then
    tabTimecodes.TabEnabled(3) = False
End If

If (g_blnRedNetwork = True Or Val(lblLibraryID.Caption) = 796559) And g_blnFFMPEG = True Then
    cmdffplay.Visible = True
Else
    cmdffplay.Visible = False
End If

If g_blnRedNetwork = False And Val(lblLibraryID.Caption) <> 796559 Then
    
    'Disable the functionality that is not possible on Blue Network
'    cmdVerifyClip.Enabled = False
    cmdVisualQuery.Enabled = False
    cmdMediaInfo.Enabled = False
    cmdMediaInfoPreserveTimecode.Enabled = False
    chkNotCheckFilenames.Value = 1
    chkNotCheckFilenames.Enabled = False
    
End If

End Sub

Private Sub Form_GotFocus()

On Error Resume Next
txtClipID.SetFocus

End Sub

Private Sub Form_Load()
'populate the company drop down (data bound)
Dim l_strSQL As String
l_strSQL = "SELECT name, accountcode, telephone, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) AND system_active = 1 ORDER BY name;"

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset
Dim l_rstSearch3 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset
Set l_rstSearch3 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

l_strSQL = "SELECT mediaspecID, mediaspecname FROM mediaspec WHERE companyID = 0 ORDER BY fd_order, mediaspecname;"

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set cmbMediaSpecs.DataSourceList = l_rstSearch2

l_strSQL = "SELECT description, information FROM xref WHERE category = 'pipelineunit' ORDER BY forder, description;"

With l_rstSearch3
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch3.ActiveConnection = Nothing

Set cmbPipelineUnit.DataSourceList = l_rstSearch3

PopulateCombo "DADCForcedSubtitle", cmbForcedSubtitle
PopulateCombo "DADCPictFormat", cmbPictFormat
PopulateCombo "DADCActiveRatio", cmbActiveRatio
PopulateCombo "discstores", cmbBarcode

l_conSearch.Close
Set l_conSearch = Nothing

g_blnFrameAccurateClipDurations = True

l_strSQL = "SELECT description FROM xref WHERE category = 'dadclogging' ORDER BY forder"
adoEventsList.ConnectionString = g_strConnection
adoEventsList.RecordSource = l_strSQL
adoEventsList.Refresh

l_strSQL = "SELECT description FROM xref WHERE category = 'dadcdistlogos' ORDER BY forder"
adoEventList2.ConnectionString = g_strConnection
adoEventList2.RecordSource = l_strSQL
adoEventList2.Refresh

CenterForm Me

'WebBrowser1.Navigate "http://online.rrsat.tv/DisplayKeyframe.asp?imagefile=/501/demo.jpg"
'WebBrowser1.Document.Body.setattribute "Scroll", "no"

If g_blnRedNetwork = False Then
    
    'Disable the functionality that is not possible on Blue Network
'    cmdVerifyClip.Enabled = False
    cmdVisualQuery.Enabled = False
    
End If

Exit Sub

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'PromptClipChanges Val(txtClipID.Text)
End Sub

Private Sub Form_Resize()

On Error Resume Next

picFooter.Top = Me.ScaleHeight - picFooter.ScaleHeight - 120
picFooter.Left = Me.ScaleWidth - picFooter.ScaleWidth - 120

'tabExtras.Height = Me.ScaleHeight - picFooter.Height - 360 - tabExtras.Top
grdKeyword.Height = tabExtras.Height - grdKeyword.Top - 60
grdClientKeywords.Height = tabExtras.Height - grdKeyword.Top - 60
grdPortalPermission.Height = tabExtras.Height - grdKeyword.Top - 60
grdMediaWindowHistory.Height = tabExtras.Height - grdKeyword.Top - 60
grdEventHistory.Height = grdKeyword.Height * 0.75 - 120
grdChecksumHistory.Height = grdKeyword.Height * 0.25
grdChecksumHistory.Top = grdEventHistory.Top + grdEventHistory.Height + 60
grdCustomFieldDefs.Height = tabExtras.Height - grdCustomFieldDefs.Top - 60

End Sub

Private Sub grdAssignedJobs_DblClick()
If Val(grdAssignedJobs.Columns("jobID").Text) <> 0 Then
    ShowJob grdAssignedJobs.Columns("jobID").Text, 1, True
'    ShowJob grdAssignedJobs.Columns("jobID").Text, 1, True
End If
End Sub

Private Sub grdClientKeywords_BeforeUpdate(Cancel As Integer)

If lblCompanyID.Caption <> "" Then
    grdClientKeywords.Columns("companyID").Text = lblCompanyID.Caption
Else
    Cancel = True
End If

End Sub

Private Sub grdCompilation_BeforeUpdate(Cancel As Integer)

grdCompilation.Columns("eventID").Text = txtClipID.Text

End Sub

Private Sub grdCompilation_RowLoaded(ByVal Bookmark As Variant)

grdCompilation.Columns("eventtitle").Text = GetData("events", "eventtitle", "eventID", grdCompilation.Columns("sourceclipID").Text)
grdCompilation.Columns("eventepisode").Text = GetData("events", "eventepisode", "eventID", grdCompilation.Columns("sourceclipID").Text)
grdCompilation.Columns("eventsubtitle").Text = GetData("events", "eventsubtitle", "eventID", grdCompilation.Columns("sourceclipID").Text)

End Sub

Private Sub grdCustomFieldDefs_BeforeUpdate(Cancel As Integer)

grdCustomFieldDefs.Columns("companyID").Text = Val(lblCompanyID.Caption)

End Sub

Private Sub grdFFProbe_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

If Not IsNull(grdFFProbe.Columns("eventchannelmapID")) Then
    txtFFprobeNotes.Text = grdFFProbe.Columns("JSONstring").Text
End If

End Sub

Private Sub grdKeyword_BeforeUpdate(Cancel As Integer)

If Val(txtClipID.Text) <> 0 Then
    grdKeyword.Columns("eventID").Text = Val(txtClipID.Text)
    grdKeyword.Columns("clipreference").Text = txtReference.Text
    grdKeyword.Columns("companyID").Text = Val(lblCompanyID.Caption)
Else
    Cancel = True
End If

End Sub

Private Sub grdKeyword_InitColumnProps()

    grdKeyword.Columns("keywordtext").DropDownHwnd = 0
    
End Sub

Private Sub grdLogging_AfterDelete(RtnDispErrMsg As Integer)
m_blnLoggingDelete = False
End Sub

Private Sub grdLogging_AfterUpdate(RtnDispErrMsg As Integer)
If m_blnLoggingDelete = True Then Exit Sub
If chkNoResorting.Value = 0 Then adoLogging.Refresh
End Sub

Private Sub grdLogging_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnLoggingDelete = True
End Sub

Private Sub grdLogging_BeforeUpdate(Cancel As Integer)

If m_blnLoggingDelete = True Then Exit Sub

If Val(txtClipID.Text) <> 0 Then
    grdLogging.Columns("eventID").Text = Val(txtClipID.Text)
Else
    Cancel = True
End If

End Sub

Private Sub grdLogging_GotFocus()

m_intTimecodeShuffle = 0

End Sub

Private Sub grdLogging_InitColumnProps()

grdLogging.Columns("segmentreference").DropDownHwnd = ddnEvents.hWnd
grdLogging.Columns("note").DropDownHwnd = ddnEvents2.hWnd

End Sub

Private Sub grdLogging_KeyPress(KeyAscii As Integer)

If g_optUseFormattedTimeCodesInEvents = 1 Then
    On Error GoTo KEYPRESS_ERROR
    If grdLogging.Columns(grdLogging.Col).Caption = "Start" Or grdLogging.Columns(grdLogging.Col).Caption = "End" Then
        If Len(grdLogging.ActiveCell.Text) < 2 Then
            If m_Framerate = TC_29 Or m_Framerate = TC_59 Then
                grdLogging.ActiveCell.Text = "00:00:00;00"
            Else
                grdLogging.ActiveCell.Text = "00:00:00:00"
            End If
            grdLogging.ActiveCell.SelStart = 0
            grdLogging.ActiveCell.SelLength = 1
        Else
            Timecode_Check_Grid grdLogging, KeyAscii, m_Framerate
        End If
    End If
End If

KEYPRESS_ERROR:

Exit Sub

End Sub

Private Sub grdLogging_LostFocus()

If grdLogging.RowChanged = True Then grdLogging.Update

End Sub

Private Sub grdLogging_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strtemptcode As String
Dim l_lngtemprow As Long
Dim l_strStartTime As String, l_strEndTime As String, l_strCalculatedDuration As String, l_blnPalFlag As Boolean, l_blnAdjustDropFrame As Boolean, l_blnFrameAccurate As Boolean
Dim l_rst As ADODB.Recordset, l_strSQL As String, l_lngLoggingID As Long
Dim l_strLastEndTime As String

If grdLogging.Row = -1 Then Exit Sub

If grdLogging.Col = -1 Then Exit Sub

'This code is for setting the start timecode of a row
'to be the same as the end timecode of the previous row, as you move from the description
'into the start timecode field.

If g_optUseFormattedTimeCodesInEvents = 1 Then
    
    If m_intTimecodeShuffle = 0 And chkNoTimecodeShuffle.Value = 0 And m_blnLoggingDelete = False Then
    
        If LastCol <> -1 Then
            If grdLogging.Columns(grdLogging.Col).Caption = "Start" And grdLogging.Columns(LastCol).Caption = "Logging Item" Then
                If grdLogging.Row > 0 Then
                    l_strSQL = adoLogging.RecordSource
                    Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
                    If l_rst.RecordCount > 0 Then
                        l_rst.MoveFirst
                        If grdLogging.Columns("eventloggingID").Text <> "" Then
                            Do While Not l_rst.EOF
                                If l_rst("eventloggingID") = grdLogging.Columns("eventloggingID").Text Then
                                    If Not l_rst.BOF Then
                                        l_rst.MovePrevious
                                        l_strLastEndTime = l_rst("timecodestop")
                                        Exit Do
                                    End If
                                End If
                                l_rst.MoveNext
                            Loop
                        Else
                            l_rst.MoveLast
                            l_strLastEndTime = l_rst("timecodestop")
                        End If
                    End If
                    l_rst.Close
                    grdLogging.ActiveCell.Text = l_strLastEndTime
                End If
            End If
        End If
    End If
End If

If g_optUseFormattedTimeCodesInEvents = 1 Then
    
    If grdLogging.Columns(grdLogging.Col).Caption = "Start" Or grdLogging.Columns(grdLogging.Col).Caption = "End" Then
        If grdLogging.ActiveCell.Text = "" Then
            If m_Framerate = TC_29 Or m_Framerate = TC_59 Then
                grdLogging.ActiveCell.Text = "00:00:00;00"
            Else
                grdLogging.ActiveCell.Text = "00:00:00:00"
            End If
        End If
        grdLogging.ActiveCell.SelLength = 0
        grdLogging.ActiveCell.SelStart = 0
    End If

End If

If LastCol = -1 Then Exit Sub

End Sub

Private Sub grdMediaWindowHistory_RowLoaded(ByVal Bookmark As Variant)

If grdMediaWindowHistory.Columns("ContactID").Text <> "0" Then
    grdMediaWindowHistory.Columns("fullname").Text = "Admin: " & GetData("contact", "name", "contactID", Val(grdMediaWindowHistory.Columns("contactID").Text))
End If

End Sub

Private Sub grdPortalPermission_AfterInsert(RtnDispErrMsg As Integer)

chkHideFromWeb.Value = 0
If Val(txtClipID.Text) <> 0 Then
    SetData "events", "hidefromweb", "eventID", Val(txtClipID.Text), 0
End If

End Sub

Private Sub grdPortalPermission_BeforeInsert(Cancel As Integer)

Dim l_rst As ADODB.Recordset

If Val(txtClipID.Text) <> 0 Then
    If VerifyClip(Val(txtClipID.Text), txtAltFolder.Text, txtClipfilename.Text, Val(lblLibraryID.Caption), True, True) = True Then
        If Val(lblLibraryID.Caption) <> 772744 And Val(lblLibraryID.Caption) <> 772745 Then
            MsgBox "Clip is not on a Delivery Store", vbCritical, "Please Check Clip Location"
            Cancel = True
        End If
        chkHideFromWeb.Value = 0
        SetData "events", "hidefromweb", "eventID", Val(txtClipID.Text), 0
        Set l_rst = ExecuteSQL("SELECT mediareference FROM portalpermission WHERE eventID = " & Val(txtClipID.Text) & " AND fullname = '" & QuoteSanitise(ddnPortalUsers.Columns("fullname").Text) & "';", g_strExecuteError)
        CheckForSQLError
        If l_rst.RecordCount > 0 Then
            Cancel = True
        End If
        l_rst.Close
        Set l_rst = Nothing
    Else
        MsgBox "File did not Verify", vbCritical, "Cannot Assign Clip"
        Cancel = True
    End If
Else
    Cancel = True
End If

End Sub

Private Sub grdPortalPermission_BeforeUpdate(Cancel As Integer)

Dim l_rst As ADODB.Recordset, SQL As String

If Val(txtClipID.Text) <> 0 Then
    Set l_rst = ExecuteSQL("SELECT mediareference FROM portalpermission WHERE eventID = " & Val(txtClipID.Text) & " AND portaluserID = " & grdPortalPermission.Columns("portaluserID").Text & ";", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount <= 0 Then
        grdPortalPermission.Columns("eventID").Text = Val(txtClipID.Text)
        grdPortalPermission.Columns("clipreference").Text = txtReference.Text
        grdPortalPermission.Columns("assignedby").Text = "MX1 - " & g_strFullUserName
        grdPortalPermission.Columns("dateassigned").Text = Now

        SQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & Val(txtClipID.Text) & ", getdate());"
        ExecuteSQL SQL, g_strExecuteError
        CheckForSQLError

    'Else
    '    Cancel = True
    End If
    l_rst.Close
    Set l_rst = Nothing
Else
    Cancel = True
End If

End Sub

Private Sub grdWebLogging_BeforeUpdate(Cancel As Integer)

If Val(lblCompanyID.Caption) <> 0 And txtReference.Text <> "" Then
    grdWebLogging.Columns("companyID").Text = Val(lblCompanyID.Caption)
    grdWebLogging.Columns("clipreference").Text = txtReference.Text
    grdWebLogging.Columns("framerate").Text = cmbFrameRate.Text
    grdWebLogging.Columns("Frame_Rate").Text = cmbFrameRate.Text
    grdWebLogging.Columns("TimeCode_In").Text = grdWebLogging.Columns("timecodestart").Text
    grdWebLogging.Columns("TimeCode_Out").Text = grdWebLogging.Columns("timecodestop").Text
    grdWebLogging.Columns("Seconds_In").Text = Seconds_From_Timecode(Timecode_Subtract(grdWebLogging.Columns("timecodestart").Text, txtTimecodeStart.Text, m_Framerate), m_Framerate)
    grdWebLogging.Columns("Seconds_Out").Text = Seconds_From_Timecode(Timecode_Subtract(grdWebLogging.Columns("timecodestop").Text, txtTimecodeStart.Text, m_Framerate), m_Framerate)
    grdWebLogging.Columns("Clip_Label").Text = grdWebLogging.Columns("segmentreference").Text
    grdWebLogging.Columns("Clip_Item").Text = adoWebLogging.Recordset.RecordCount
Else
    Cancel = True
End If

End Sub

Private Sub grdWebLogging_KeyPress(KeyAscii As Integer)

If g_optUseFormattedTimeCodesInEvents = 1 Then
    On Error GoTo KEYPRESS_ERROR
    If grdWebLogging.Columns(grdWebLogging.Col).Caption = "Start" Or grdWebLogging.Columns(grdWebLogging.Col).Caption = "End" Then
        If Len(grdWebLogging.ActiveCell.Text) < 2 Then
            If m_Framerate = TC_29 Or m_Framerate = TC_59 Then
                grdWebLogging.ActiveCell.Text = "00:00:00;00"
            Else
                grdWebLogging.ActiveCell.Text = "00:00:00:00"
            End If
            grdWebLogging.ActiveCell.SelStart = 0
            grdWebLogging.ActiveCell.SelLength = 1
        Else
            Timecode_Check_Grid grdWebLogging, KeyAscii, m_Framerate
        End If
    End If
End If

KEYPRESS_ERROR:

Exit Sub

End Sub

Private Sub tabExtras_Click(PreviousTab As Integer)

Dim l_strSQL As String

If PreviousTab = 1 Then
    If adoKeyword.ConnectionString <> "" Then adoKeyword.Refresh
End If

Select Case tabExtras.Tab
    Case 0
        'Load up the keywords
        adoKeywords.ConnectionString = g_strConnection
        l_strSQL = "SELECT eventID, keywordtext, clipreference, companyID "
        l_strSQL = l_strSQL & " FROM eventkeyword "
        l_strSQL = l_strSQL & " WHERE clipreference = '" & QuoteSanitise(txtReference.Text) & "' and companyID = " & lblCompanyID.Caption & ";"
        adoKeywords.RecordSource = l_strSQL
        adoKeywords.Refresh
    
        adoKeyword.ConnectionString = g_strConnection
        adoKeyword.RecordSource = "SELECT keywordtext, keywordID FROM keyword WHERE companyID = " & lblCompanyID.Caption & "ORDER BY keywordtext;"
        adoKeyword.Refresh
    
        If adoKeyword.Recordset.RecordCount <> 0 Then grdKeyword.Columns("keywordtext").DropDownHwnd = ddnKeyword.hWnd
    Case 1
        adoClientKeywords.ConnectionString = g_strConnection
        adoClientKeywords.RecordSource = "SELECT keywordtext, companyID from keyword WHERE companyID = " & lblCompanyID.Caption & ";"
        adoClientKeywords.Refresh
    Case 2
        adoPortalUsers.ConnectionString = g_strConnection
        adoPortalUsers.RecordSource = "SELECT * from portaluser WHERE companyID = " & lblCompanyID.Caption & " AND activeuser = 1 ORDER BY fullname;"
        adoPortalUsers.Refresh
        If adoPortalUsers.Recordset.RecordCount <> 0 Then grdPortalPermission.Columns("fullname").DropDownHwnd = ddnPortalUsers.hWnd
        adoPortalPermission.ConnectionString = g_strConnection
        adoPortalPermission.RecordSource = "SELECT * FROM portalpermission WHERE eventID = " & txtClipID.Text & " ORDER BY fullname;"
        adoPortalPermission.Refresh
        If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/timedpermissions") > 0 Then
            grdPortalPermission.Columns("permissionstart").Visible = True
            grdPortalPermission.Columns("permissionend").Visible = True
            grdPortalPermission.Columns("fullname").Width = 2100
        Else
            grdPortalPermission.Columns("permissionstart").Visible = False
            grdPortalPermission.Columns("permissionend").Visible = False
            grdPortalPermission.Columns("fullname").Width = 5490
        End If
    Case 3
        adoCustomFieldDefs.ConnectionString = g_strConnection
        adoCustomFieldDefs.RecordSource = "SELECT * from customfieldxref where companyID = " & lblCompanyID.Caption & "ORDER BY fd_order, textentry;"
        adoCustomFieldDefs.Refresh
    Case 7
        'Load the Assigned Jobs
        l_strSQL = "SELECT requiredmediaID, requiredmedia.jobID, fd_status, completeddate FROM requiredmedia INNER JOIN job on requiredmedia.jobID = job.jobID WHERE eventID = " & txtClipID.Text & " ORDER BY requiredmedia.jobID ASC;"
        adoAssignedJobs.RecordSource = l_strSQL
        adoAssignedJobs.ConnectionString = g_strConnection
        adoAssignedJobs.Refresh
    Case 5
        l_strSQL = "SELECT * FROM eventhistory WHERE eventID = " & txtClipID.Text & " ORDER BY datesaved DESC;"
        adoHistory.RecordSource = l_strSQL
        adoHistory.ConnectionString = g_strConnection
        adoHistory.Refresh
    
        adoChecksumHistory.ConnectionString = g_strConnection
        adoChecksumHistory.RecordSource = "SELECT * FROM eventchecksumhistory WHERE eventID = " & txtClipID.Text & " ORDER BY case when cdate is null then 1 else 0 end, cdate desc;"
        adoChecksumHistory.Refresh
    Case 6
        l_strSQL = "SELECT contactID, '' AS fullname, 0 AS distributionuserID, 0 AS portaluserID, accesstype, timewhen FROM webdelivery WHERE clipID = " & txtClipID.Text & _
            " UNION SELECT 0 AS contactID, fullname, 0 AS distributionuserID, portaluserID, accesstype, timewhen FROM webdeliveryportal WHERE clipid = " & txtClipID.Text & _
            " UNION SELECT 0 AS contactID, fullname, distributionuserID, 0 as portaluserID, accesstype, timewhen FROM distributiondelivery WHERE clipID = " & txtClipID.Text & " ORDER BY timewhen DESC;"
        adoMediaWindowHistory.RecordSource = l_strSQL
        adoMediaWindowHistory.ConnectionString = g_strConnection
        adoMediaWindowHistory.Refresh
    
End Select

End Sub

Private Sub tabTimecodes_Click(PreviousTab As Integer)

Dim l_strSQL As String

If txtClipID.Text = "" Then Exit Sub

Select Case tabTimecodes.Tab
'    Case 1
'        'Load the clip logging
'        l_strSQL = "SELECT * FROM eventlogging WHERE eventID = " & txtClipID.Text & " ORDER BY timecodestart ASC;"
'        adoLogging.RecordSource = l_strSQL
'        adoLogging.ConnectionString = g_strConnection
'        adoLogging.Refresh
    Case 2
        'Load the web clip logging
        l_strSQL = "SELECT * FROM eventweblogging WHERE companyID = " & lblCompanyID.Caption & " AND clipreference = '" & QuoteSanitise(txtReference.Text) & "' AND framerate = '" & cmbFrameRate.Text & "' ORDER BY timecodestart ASC;"
        adoWebLogging.RecordSource = l_strSQL
        adoWebLogging.ConnectionString = g_strConnection
        adoWebLogging.Refresh
    Case 4
        'Load the Compilation details
        l_strSQL = "SELECT sourceclipID, forder, eventID FROM eventcompilation WHERE eventID = " & txtClipID.Text & " ORDER BY forder ASC;"
        adoCompilation.RecordSource = l_strSQL
        adoCompilation.ConnectionString = g_strConnection
        adoCompilation.Refresh
End Select

End Sub

Private Sub txtAudioBitrate_GotFocus()
HighLite txtAudioBitrate
End Sub

Private Sub txtBitrate_GotFocus()
HighLite txtBitrate
End Sub

Private Sub txtBitrate_KeyPress(KeyAscii As Integer)
If KeyAscii = 61 Then
    txtBitrate.Text = Val(txtAudioBitrate.Text) + Val(txtVideoBitrate.Text)
    KeyAscii = 0
End If
End Sub

'Private Sub txtBreak1_KeyPress(KeyAscii As Integer)
'
'If Len(txtBreak1.Text) < 2 Then
'    Select Case m_Framerate
'        Case TC_29, TC_59
'            txtBreak1.Text = "00:00:00;00"
'        Case Else
'            txtBreak1.Text = "00:00:00:00"
'    End Select
'    txtBreak1.SelStart = 0
'    txtBreak1.SelLength = 1
'Else
'    Timecode_Check_Control txtBreak1, KeyAscii, m_Framerate
'End If
'
'End Sub
'
'Private Sub txtBreak1elapsed_GotFocus()
'
'Dim TempStr As String
'
'If txtBreak1elapsed.Text = "" And txtBreak1.Text <> "00:00:00:00" And txtBreak1.Text <> "00:00:00;00" And txtBreak1.Text <> "" Then
'    TempStr = Timecode_Subtract(txtBreak1.Text, txtTimecodeStart.Text, m_Framerate)
'
'    If g_blnFrameAccurateClipDurations = False Then
'        txtBreak1elapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
'    Else
'        txtBreak1elapsed.Text = TempStr
'    End If
'Else
'    HighLite txtBreak1elapsed
'End If
'
'End Sub
'
'Private Sub txtBreak2_KeyPress(KeyAscii As Integer)
'
'If Len(txtBreak2.Text) < 2 Then
'    Select Case m_Framerate
'        Case TC_29, TC_59
'            txtBreak2.Text = "00:00:00;00"
'        Case Else
'            txtBreak2.Text = "00:00:00:00"
'    End Select
'    txtBreak2.SelStart = 0
'    txtBreak2.SelLength = 1
'Else
'    Timecode_Check_Control txtBreak2, KeyAscii, m_Framerate
'End If
'
'End Sub
'
'Private Sub txtBreak2elapsed_GotFocus()
'
'Dim TempStr As String
'
'If txtBreak2elapsed.Text = "" And txtBreak2.Text <> "00:00:00:00" And txtBreak2.Text <> "00:00:00;00" And txtBreak2.Text <> "" Then
'    TempStr = Timecode_Subtract(txtBreak2.Text, txtTimecodeStart.Text, m_Framerate)
'
'    If g_blnFrameAccurateClipDurations = False Then
'        txtBreak2elapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
'    Else
'        txtBreak2elapsed.Text = TempStr
'    End If
'Else
'    HighLite txtBreak2elapsed
'End If
'
'End Sub
'
'Private Sub txtBreak3_KeyPress(KeyAscii As Integer)
'
'If Len(txtBreak3.Text) < 2 Then
'    Select Case m_Framerate
'        Case TC_29, TC_59
'            txtBreak3.Text = "00:00:00;00"
'        Case Else
'            txtBreak3.Text = "00:00:00:00"
'    End Select
'    txtBreak3.SelStart = 0
'    txtBreak3.SelLength = 1
'Else
'    Timecode_Check_Control txtBreak3, KeyAscii, m_Framerate
'End If
'
'End Sub
'
'Private Sub txtBreak3elapsed_GotFocus()
'
'Dim TempStr As String
'
'If txtBreak3elapsed.Text = "" And txtBreak3.Text <> "00:00:00:00" And txtBreak3.Text <> "00:00:00;00" And txtBreak3.Text <> "" Then
'    TempStr = Timecode_Subtract(txtBreak3.Text, txtTimecodeStart.Text, m_Framerate)
'
'    If g_blnFrameAccurateClipDurations = False Then
'        txtBreak3elapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
'    Else
'        txtBreak3elapsed.Text = TempStr
'    End If
'Else
'    HighLite txtBreak3elapsed
'End If
'
'End Sub
'
'Private Sub txtBreak4_KeyPress(KeyAscii As Integer)
'
'If Len(txtBreak4.Text) < 2 Then
'    Select Case m_Framerate
'        Case TC_29, TC_59
'            txtBreak4.Text = "00:00:00;00"
'        Case Else
'            txtBreak4.Text = "00:00:00:00"
'    End Select
'    txtBreak4.SelStart = 0
'    txtBreak4.SelLength = 1
'Else
'    Timecode_Check_Control txtBreak4, KeyAscii, m_Framerate
'End If
'
'End Sub
'
'Private Sub txtBreak4elapsed_GotFocus()
'
'Dim TempStr As String
'
'If txtBreak4elapsed.Text = "" And txtBreak4.Text <> "00:00:00:00" And txtBreak4.Text <> "00:00:00;00" And txtBreak4.Text <> "" Then
'    TempStr = Timecode_Subtract(txtBreak4.Text, txtTimecodeStart.Text, m_Framerate)
'
'    If g_blnFrameAccurateClipDurations = False Then
'        txtBreak4elapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
'    Else
'        txtBreak4elapsed.Text = TempStr
'    End If
'Else
'    HighLite txtBreak4elapsed
'End If
'
'End Sub
'
'Private Sub txtBreak5_KeyPress(KeyAscii As Integer)
'
'If Len(txtBreak5.Text) < 2 Then
'    Select Case m_Framerate
'        Case TC_29, TC_59
'            txtBreak5.Text = "00:00:00;00"
'        Case Else
'            txtBreak5.Text = "00:00:00:00"
'    End Select
'    txtBreak5.SelStart = 0
'    txtBreak5.SelLength = 1
'Else
'    Timecode_Check_Control txtBreak5, KeyAscii, m_Framerate
'End If
'
'End Sub
'
'Private Sub txtBreak5elapsed_GotFocus()
'
'Dim TempStr As String
'
'If txtBreak5elapsed.Text = "" And txtBreak5.Text <> "00:00:00:00" And txtBreak5.Text <> "00:00:00;00" And txtBreak5.Text <> "" Then
'    TempStr = Timecode_Subtract(txtBreak5.Text, txtTimecodeStart.Text, m_Framerate)
'
'    If g_blnFrameAccurateClipDurations = False Then
'        txtBreak5elapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
'    Else
'        txtBreak5elapsed.Text = TempStr
'    End If
'Else
'    HighLite txtBreak5elapsed
'End If
'
'End Sub
'
'Private Sub txtBreak6_KeyPress(KeyAscii As Integer)
'
'If Len(txtBreak6.Text) < 2 Then
'    Select Case m_Framerate
'        Case TC_29, TC_59
'            txtBreak6.Text = "00:00:00;00"
'        Case Else
'            txtBreak6.Text = "00:00:00:00"
'    End Select
'    txtBreak6.SelStart = 0
'    txtBreak6.SelLength = 1
'Else
'    Timecode_Check_Control txtBreak6, KeyAscii, m_Framerate
'End If
'
'End Sub
'
'Private Sub txtBreak6elapsed_GotFocus()
'
'Dim TempStr As String
'
'If txtBreak6elapsed.Text = "" And txtBreak6.Text <> "00:00:00:00" And txtBreak6.Text <> "00:00:00;00" And txtBreak6.Text <> "" Then
'    TempStr = Timecode_Subtract(txtBreak6.Text, txtTimecodeStart.Text, m_Framerate)
'
'    If g_blnFrameAccurateClipDurations = False Then
'        txtBreak6elapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
'    Else
'        txtBreak6elapsed.Text = TempStr
'    End If
'Else
'    HighLite txtBreak6elapsed
'End If
'
'End Sub
'
Private Sub txtClipID_GotFocus()
HighLite txtClipID
End Sub

Private Sub txtClipID_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    KeyAscii = 0
    'If Val(txtClipID.Text) <> 0 Then PromptClipChanges (Val(txtClipID.Text))
    ShowClipControl Val(txtClipID)
End If

End Sub

Private Sub txtDuration_GotFocus()

Dim TempStr As String

If txtDuration.Text = "" And txtTimecodeStop.Text <> "00:00:00:00" And txtTimecodeStop.Text <> "00:00:00;00" And txtTimecodeStop.Text <> "" Then
    TempStr = Timecode_Subtract(txtTimecodeStop.Text, txtTimecodeStart.Text, m_Framerate)
    
    If g_blnFrameAccurateClipDurations = False Then
        txtDuration.Text = Duration_From_Timecode(TempStr, m_Framerate)
    Else
        txtDuration.Text = TempStr
    End If
Else
    HighLite txtDuration
End If

End Sub

Private Sub txtEndCredits_KeyPress(KeyAscii As Integer)

If Len(txtEndCredits.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29, TC_59
            txtEndCredits.Text = "00:00:00;00"
        Case Else
            txtEndCredits.Text = "00:00:00:00"
    End Select
    txtEndCredits.SelStart = 0
    txtEndCredits.SelLength = 1
Else
    Timecode_Check_Control txtEndCredits, KeyAscii, m_Framerate
End If

End Sub

Private Sub txtEndCreditsElapsed_GotFocus()

Dim TempStr As String

If txtEndCreditsElapsed.Text = "" And txtEndCredits.Text <> "00:00:00:00" And txtEndCredits.Text <> "00:00:00;00" And txtEndCredits.Text <> "" Then
    TempStr = Timecode_Subtract(txtEndCredits.Text, txtTimecodeStart.Text, m_Framerate)

    If g_blnFrameAccurateClipDurations = False Then
        txtEndCreditsElapsed.Text = Duration_From_Timecode(TempStr, m_Framerate)
    Else
        txtEndCreditsElapsed.Text = TempStr
    End If
Else
    HighLite txtEndCreditsElapsed
End If

End Sub

Private Sub txtFileSize_GotFocus()
HighLite txtFileSize
End Sub

Private Sub txtHorizpixels_GotFocus()
HighLite txtHorizpixels
End Sub

Private Sub txtJobID_DblClick()

If Val(txtJobID.Text) <> 0 Then
    ShowJob Val(txtJobID.Text), 1, True
'    ShowJob Val(txtJobID.Text), 1, True
End If

End Sub

Private Sub txtJobID_GotFocus()
HighLite txtJobID
End Sub

Private Sub txtSourcebarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    If txtSourcebarcode.Text <> "" Then
        KeyAscii = 0
        txtSourcebarcode.Text = UCase(txtSourcebarcode.Text)
        lblSourceLibraryID.Caption = GetData("library", "libraryID", "barcode", txtSourcebarcode.Text)
        lblSourceFormat.Caption = GetData("library", "format", "barcode", txtSourcebarcode.Text)
    Else
        lblSourceLibraryID.Caption = ""
        lblSourceFormat.Caption = ""
    End If
End If
End Sub

Private Sub txtSubTitle_GotFocus()
HighLite txtSubtitle
End Sub

Private Sub txtTimecodeKeyframe_KeyPress(KeyAscii As Integer)

If Len(txtTimecodeKeyframe.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29, TC_59
            txtTimecodeKeyframe.Text = "00:00:00;00"
        Case Else
            txtTimecodeKeyframe.Text = "00:00:00:00"
    End Select
    txtTimecodeKeyframe.SelStart = 0
    txtTimecodeKeyframe.SelLength = 1
Else
    Timecode_Check_Control txtTimecodeKeyframe, KeyAscii, m_Framerate
End If

End Sub

Private Sub txtTimecodestart_KeyPress(KeyAscii As Integer)

If Len(txtTimecodeStart.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29, TC_59
            txtTimecodeStart.Text = "00:00:00;00"
        Case Else
            txtTimecodeStart.Text = "00:00:00:00"
    End Select
    txtTimecodeStart.SelStart = 0
    txtTimecodeStart.SelLength = 1
Else
    Timecode_Check_Control txtTimecodeStart, KeyAscii, m_Framerate
End If

End Sub

Private Sub txtTimecodestop_KeyPress(KeyAscii As Integer)
If Len(txtTimecodeStop.Text) < 2 Then
    Select Case m_Framerate
        Case TC_29, TC_59
            txtTimecodeStop.Text = "00:00:00;00"
        Case Else
            txtTimecodeStop.Text = "00:00:00:00"
    End Select
    txtTimecodeStop.SelStart = 0
    txtTimecodeStop.SelLength = 1
Else
    Timecode_Check_Control txtTimecodeStop, KeyAscii, m_Framerate
End If
End Sub

Private Sub txtTitle_Click()
txtSeriesID.Text = txtTitle.Columns("SeriesID").Text
End Sub

Private Sub txtVertpixels_GotFocus()
HighLite txtVertpixels
End Sub

Private Sub txtVideoBitrate_GotFocus()
HighLite txtVideoBitrate
End Sub

Public Sub Set_Framerate()

Select Case cmbFrameRate.Text

    Case "25"
        m_Framerate = TC_25
    Case "29.97"
        m_Framerate = TC_29
    Case "30", "29.97 NDF"
        m_Framerate = TC_30
    Case "24", "23.98"
        m_Framerate = TC_24
    Case "50"
        m_Framerate = TC_50
    Case "60", "59.94 NDF"
        m_Framerate = TC_60
    Case "59.94"
        m_Framerate = TC_59
    Case Else
        m_Framerate = TC_UN

End Select

End Sub

'Private Function MakeDADCXMLFileSPE() As Boolean
'
'If txtReference.Text = "" Or cmbActiveRatio.Text = "" Or cmbIsTextInPicture.Text = "" Or cmbPictFormat.Text = "" Then
'    MsgBox "Data is not completed", vbCritical, "Error"
'    Exit Function
'End If
'
'With frmClipControl
'    If .adoLogging.Recordset.RecordCount <= 0 Then
'        MsgBox "Logging is not completed", vbCritical, "Error"
'        Exit Function
'    End If
'    .adoLogging.Recordset.MoveFirst
'    Do While Not .adoLogging.Recordset.EOF
'        If Not (Validate_Timecode(.adoLogging.Recordset("timecodestart"), m_Framerate, True)) Or Not (Validate_Timecode(.adoLogging.Recordset("timecodestop"), m_Framerate, True)) Then
'            MsgBox "Problem with timecode logging on '" & .adoLogging.Recordset("segmentreference") & "'", vbCritical, "Cannot complete XML"
'            MakeDADCXMLFileSPE = False
'            Exit Function
'        End If
'        .adoLogging.Recordset.MoveNext
'    Loop
'End With
'
'If lblLibraryID.Caption <> "719953" Then
'    If MsgBox("Masterfile is not on the BFS-OPS. You will not be able to auto-send this file." & vbCrLf & "Do you wish to continue", vbYesNo + vbDefaultButton2, "Not on BFS-OPS") = vbNo Then
'        Exit Function
'    End If
'End If
'
'Dim l_strFileNameToSave As String
'Dim FSO As Scripting.FileSystemObject, l_strSQL As String
'Dim l_strPathForMove As String, l_strFolderForFilerequest As String, l_strPathForCopy As String
'Dim l_lngNewLibraryID As Long
'
'MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
'MDIForm1.dlgMain.Filter = "XML files|*.xml"
'MDIForm1.dlgMain.Filename = txtReference.Text & ".xml"
'
'MDIForm1.dlgMain.ShowSave
'
'l_strFileNameToSave = MDIForm1.dlgMain.Filename
'
'If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
'
'    Open l_strFileNameToSave For Output As 1
'
'    Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
'    Print #1, "<BulkIngestData xmlns=""http://schema.dadcdigital.com/dbb/data/2010/migration/"" xmlns:inv=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"" xmlns:z=""http://schemas.microsoft.com/2003/10/Serialization/"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://schema.dadcdigital.com/dbb/data/2010/migration/ C:/DOCUME~1/Administrator/Desktop/Diamonds_DBB_XSLT/BulkIngest.xsd"">"
'    WriteTabs 1
'    Print #1, "<ExternalTaskID></ExternalTaskID>"
'    WriteTabs 1
'    Print #1, "<ExternalPONumber></ExternalPONumber>"
'    WriteTabs 1
'    Print #1, "<ExternalDescription></ExternalDescription>"
'    WriteTabs 1
'    Print #1, "<ExternalAlphaID>" & GetData("tracker_spe_item", "Alpha_ID", "clipreference", txtReference.Text) & "</ExternalAlphaID>"
'    WriteTabs 1
'
'    'File Components - a Video section
'    Print #1, "<Components>"
'
'    Dim l_lngChannelCount As Long, l_scrCoordinates As ScreenDimensions
'
'    On Error GoTo DADC_XML_ERROR1
'
'    With frmClipControl
'
'        WriteTabs 2
'        Print #1, "<Component xsi:type=""inv:VideoComponent"">"
'        WriteTabs 3
'        Print #1, "<inv:TapeBarcode xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & GetData("tracker_spe_item", "barcode", "clipreference", .txtReference.Text) & "</inv:TapeBarcode>"
'
'        WriteNewFileDescriptorSPE 3
'
'        WriteTabs 3
'        Print #1, "<inv:VideoStandard xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/""></inv:VideoStandard>"
'        WriteTabs 3
'        Print #1, "<inv:ScreenAspectRatio xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
'        If .cmbAspect.Text = "16:9" Then
'            Print #1, "16x9";
'        Else
'            Print #1, "4x3";
'        End If
'        Print #1, "</inv:ScreenAspectRatio>"
'        WriteTabs 3
'        Print #1, "<inv:ActivePictureAspectRatio xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
'        Print #1, .cmbActiveRatio.Text;
'        Print #1, "</inv:ActivePictureAspectRatio>"
'        WriteTabs 3
'        Print #1, "<inv:IsTextInPicture xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
'        Print #1, .cmbIsTextInPicture.Text;
'        Print #1, "</inv:IsTextInPicture>"
'        WriteTabs 3
'        Print #1, "<inv:TextedLanguage xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
'        If .cmbTextInPictureLanguage.Text = "German" Then
'            Print #1, "German (Germany)";
'        Else
'            Print #1, .cmbTextInPictureLanguage.Text;
'        End If
'        Print #1, "</inv:TextedLanguage>"
'        WriteTabs 3
'        Print #1, "<inv:BurnedInSubtitleLanguage xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
'        If .cmbBurnedInSubtitleLanguage.Text = "German" Then
'            Print #1, "German (Germany)";
'        ElseIf .cmbBurnedInSubtitleLanguage.Text = "" Then
'            Print #1, "None";
'        Else
'            Print #1, .cmbBurnedInSubtitleLanguage.Text;
'        End If
'        Print #1, "</inv:BurnedInSubtitleLanguage>"
'        WriteTabs 3
'        Print #1, "<inv:FrameRate xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">";
'        Select Case .cmbFrameRate.Text
'            Case "23.98"
'                Print #1, "23.976";
'            Case "24"
'                Print #1, "24.00";
'            Case "25"
'                Print #1, "25.00";
'            Case "29.97"
'                Print #1, "29.97";
'            Case "30"
'                Print #1, "30.00";
'            Case "50"
'                Print #1, "50.00";
'            Case "59.94"
'                Print #1, "59.94";
'            Case Else
'                MsgBox "Cannot handle this frame rate."
'                MakeDADCXMLFileSPE = False
'                Exit Function
'        End Select
'        Print #1, "</inv:FrameRate>"
'        WriteTabs 3
'        Print #1, "<inv:HorizontalResolutionPixels xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .txtHorizpixels.Text & "</inv:HorizontalResolutionPixels>"
'        WriteTabs 3
'        Print #1, "<inv:VerticalResolutionPixels xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .txtVertpixels.Text & "</inv:VerticalResolutionPixels>"
'        WriteTabs 3
'        Print #1, "<inv:Runtime xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .txtDuration.Text & "</inv:Runtime>"
'        WriteTabs 3
'        l_scrCoordinates = CalculateAspectCoordinates(Val(.txtHorizpixels.Text), Val(.txtVertpixels.Text), .cmbAspect.Text, .cmbActiveRatio.Text)
'        Print #1, "<ActiveTopLeftX xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & l_scrCoordinates.lngTopLeftX & "</ActiveTopLeftX>"
'        WriteTabs 3
'        Print #1, "<ActiveTopLeftY xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & l_scrCoordinates.lngTopLeftY & "</ActiveTopLeftY>"
'        WriteTabs 3
'        Print #1, "<ActiveBottomRightX xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & l_scrCoordinates.lngBottomRightX & "</ActiveBottomRightX>"
'        WriteTabs 3
'        Print #1, "<ActiveBottomRightY xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & l_scrCoordinates.lngBottomRightY & "</ActiveBottomRightY>"
'        WriteTabs 3
'        Print #1, "<inv:ActivePictureFormat xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbPictFormat.Text & "</inv:ActivePictureFormat>"
'        WriteTabs 3
'        Print #1, "<inv:ForcedSubtitle xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbForcedSubtitle.Text & "</inv:ForcedSubtitle>"
'        WriteTabs 3
'        Print #1, "<inv:MLFCore xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">No</inv:MLFCore>"
'        WriteTabs 2
'        Print #1, "</Component>"
'
'        'Then an Audio section for each Audio set logged group
'        l_lngChannelCount = 0
'
'        If .cmbAudioType1.Text <> "" Then
'
'            If .cmbAudioType1.Text = "Standard Stereo" Or .cmbAudioType1.Text = "LT/RT" Or Left(.cmbAudioType1.Text, 7) = "Dolby E" Or .cmbAudioType1.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType1.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent1.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage1.Text <> "N/A", .cmbAudioLanguage1.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent1.Text, .cmbAudioType1.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType1.Text = "5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType1.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent1.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage1.Text <> "N/A", .cmbAudioLanguage1.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent1.Text, .cmbAudioType1.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType1.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType1.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent1.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage1.Text <> "N/A", .cmbAudioLanguage1.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent1.Text, .cmbAudioType1.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType2.Text <> "" Then
'
'            If .cmbAudioType2.Text = "Standard Stereo" Or .cmbAudioType2.Text = "LT/RT" Or Left(.cmbAudioType2.Text, 7) = "Dolby E" Or .cmbAudioType2.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType2.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent2.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage2.Text <> "N/A", .cmbAudioLanguage2.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent2.Text, .cmbAudioType2.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType2.Text = "5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType2.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent2.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage2.Text <> "N/A", .cmbAudioLanguage2.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent2.Text, .cmbAudioType2.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType2.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType2.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent2.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage2.Text <> "N/A", .cmbAudioLanguage2.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent2.Text, .cmbAudioType2.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType3.Text <> "" Then
'
'            If .cmbAudioType3.Text = "Standard Stereo" Or .cmbAudioType3.Text = "LT/RT" Or .cmbAudioType3.Text = "VOD Standard Stereo" Or .cmbAudioType3.Text = "VOD LT/RT" Or Left(.cmbAudioType3.Text, 7) = "Dolby E" Or .cmbAudioType3.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType3.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent3.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage3.Text <> "N/A", .cmbAudioLanguage3.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent3.Text, .cmbAudioType3.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType3.Text = "5.1" Or .cmbAudioType3.Text = "VOD 5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType3.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent3.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage3.Text <> "N/A", .cmbAudioLanguage3.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent3.Text, .cmbAudioType3.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType3.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType3.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent3.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage3.Text <> "N/A", .cmbAudioLanguage3.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent3.Text, .cmbAudioType3.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType4.Text <> "" Then
'
'            If .cmbAudioType4.Text = "Standard Stereo" Or .cmbAudioType4.Text = "LT/RT" Or Left(.cmbAudioType4.Text, 7) = "Dolby E" Or .cmbAudioType4.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType4.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent4.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage4.Text <> "N/A", .cmbAudioLanguage4.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent4.Text, .cmbAudioType4.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType4.Text = "5.1" Or .cmbAudioType4.Text = "VOD 5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType4.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent4.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage4.Text <> "N/A", .cmbAudioLanguage4.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent4.Text, .cmbAudioType4.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType4.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType4.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent4.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage4.Text <> "N/A", .cmbAudioLanguage4.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent4.Text, .cmbAudioType4.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType5.Text <> "" Then
'
'            If .cmbAudioType5.Text = "Standard Stereo" Or .cmbAudioType5.Text = "LT/RT" Or .cmbAudioType5.Text = "VOD Standard Stereo" Or .cmbAudioType5.Text = "VOD LT/RT" Or Left(.cmbAudioType5.Text, 7) = "Dolby E" Or .cmbAudioType5.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType5.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent5.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage5.Text <> "N/A", .cmbAudioLanguage5.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent5.Text, .cmbAudioType5.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType5.Text = "5.1" Or .cmbAudioType5.Text = "VOD 5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType5.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent5.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage5.Text <> "N/A", .cmbAudioLanguage5.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent5.Text, .cmbAudioType5.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType5.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType5.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent5.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage5.Text <> "N/A", .cmbAudioLanguage5.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent5.Text, .cmbAudioType5.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType6.Text <> "" Then
'
'            If .cmbAudioType6.Text = "Standard Stereo" Or .cmbAudioType6.Text = "LT/RT" Or .cmbAudioType6.Text = "VOD Standard Stereo" Or .cmbAudioType6.Text = "VOD LT/RT" Or Left(.cmbAudioType6.Text, 7) = "Dolby E" Or .cmbAudioType6.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType6.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent6.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage6.Text <> "N/A", .cmbAudioLanguage6.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent6.Text, .cmbAudioType6.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType6.Text = "5.1" Or .cmbAudioType6.Text = "VOD 5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType6.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent6.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage6.Text <> "N/A", .cmbAudioLanguage6.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent6.Text, .cmbAudioType6.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType6.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType6.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent6.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage6.Text <> "N/A", .cmbAudioLanguage6.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent6.Text, .cmbAudioType6.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType7.Text <> "" Then
'
'            If .cmbAudioType7.Text = "Standard Stereo" Or .cmbAudioType7.Text = "LT/RT" Or .cmbAudioType7.Text = "VOD Standard Stereo" Or .cmbAudioType7.Text = "VOD LT/RT" Or Left(.cmbAudioType7.Text, 7) = "Dolby E" Or .cmbAudioType7.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType7.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent7.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage7.Text <> "N/A", .cmbAudioLanguage7.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent7.Text, .cmbAudioType7.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType7.Text = "5.1" Or .cmbAudioType7.Text = "VOD 5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType7.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent7.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage7.Text <> "N/A", .cmbAudioLanguage7.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent7.Text, .cmbAudioType7.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType7.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType7.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent7.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage7.Text <> "N/A", .cmbAudioLanguage7.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent7.Text, .cmbAudioType7.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        If .cmbAudioType8.Text <> "" Then
'
'            If .cmbAudioType8.Text = "Standard Stereo" Or .cmbAudioType8.Text = "LT/RT" Or .cmbAudioType8.Text = "VOD Standard Stereo" Or .cmbAudioType8.Text = "VOD LT/RT" Or Left(.cmbAudioType8.Text, 7) = "Dolby E" Or .cmbAudioType8.Text = "Mono 2.0" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType8.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent8.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage8.Text <> "N/A", .cmbAudioLanguage8.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "2track", l_lngChannelCount, .cmbAudioContent8.Text, .cmbAudioType8.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType8.Text = "5.1" Or .cmbAudioType8.Text = "VOD 5.1" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType8.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent8.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage8.Text <> "N/A", .cmbAudioLanguage8.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "6track", l_lngChannelCount, .cmbAudioContent8.Text, .cmbAudioType8.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            ElseIf .cmbAudioType8.Text = "Mono" Then
'
'                WriteTabs 2
'                Print #1, "<Component xsi:type=""inv:AudioTrackComponent"">"
'                WriteTabs 3
'                Print #1, "<inv:AudioConfiguration xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & .cmbAudioType8.Text & "</inv:AudioConfiguration>"
'                WriteTabs 3
'                Print #1, "<inv:AudioContent xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & XMLSanitise(.cmbAudioContent8.Text) & "</inv:AudioContent>"
'                WriteTabs 3
'                Print #1, "<inv:Language xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">" & IIf(.cmbAudioLanguage8.Text <> "N/A", .cmbAudioLanguage8.Text, "None") & "</inv:Language>"
'                WriteTabs 3
'                Print #1, "<inv:AudioChannelComponents xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'                WriteNewAudioChannelComponentsSPE 4, "1track", l_lngChannelCount, .cmbAudioContent8.Text, .cmbAudioType8.Text
'                WriteTabs 3
'                Print #1, "</inv:AudioChannelComponents>"
'                WriteTabs 2
'                Print #1, "</Component>"
'
'            Else
'
'                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
'                MakeDADCXMLFileSPE = False
'                Exit Function
'
'            End If
'
'        End If
'
'        WriteTabs 1
'        Print #1, "</Components>"
'
'        Dim l_strProgramStart As String, l_strProgramEnd As String, l_blnFoundProg As Boolean, l_blnFoundProgEnd As Boolean, l_strDescriptionAlias As String
'
'        WriteTabs 1
'        Print #1, "<TimecodeGroups>"
'        WriteTabs 2
'        Print #1, "<TimecodeGroup>"
'        WriteTabs 3
'        Print #1, "<TimecodeRanges xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
'
'        l_strProgramStart = ""
'        l_strProgramEnd = ""
'        l_blnFoundProg = False
'        l_blnFoundProgEnd = False
'
'        .adoLogging.Recordset.MoveFirst
'
'        Do While Not .adoLogging.Recordset.EOF
'            If .adoLogging.Recordset("segmentreference") = "Programme" Then
'
'                If l_blnFoundProg = False Then
'                    l_strProgramStart = .adoLogging.Recordset("timecodestart")
'                    l_blnFoundProg = True
'                    Exit Do
'                End If
'
'            End If
'            .adoLogging.Recordset.MoveNext
'        Loop
'        .adoLogging.Recordset.MoveLast
'        Do While Not .adoLogging.Recordset.BOF
'
'            If .adoLogging.Recordset("segmentreference") = "Programme" Then
'
'                If l_blnFoundProg = True And l_blnFoundProgEnd = False Then
'                    l_blnFoundProgEnd = True
'                    l_strProgramEnd = .adoLogging.Recordset("timecodestop")
'                    Exit Do
'                End If
'
'            End If
'            .adoLogging.Recordset.MovePrevious
'        Loop
'        .adoLogging.Recordset.MoveFirst
'
'        Do While Not .adoLogging.Recordset.EOF
'            If .adoLogging.Recordset("segmentreference") <> "Programme" Then
'                WriteTabs 4
'                Print #1, "<TimecodeRange>"
'                WriteTabs 5
'                l_strDescriptionAlias = GetDataSQL("SELECT TOP 1 descriptionalias FROM xref WHERE category = 'dadclogging' and description = '" & .adoLogging.Recordset("segmentreference") & "';")
'                If l_strDescriptionAlias <> "" Then
'                    Print #1, "<TimecodeRangeType>" & UCase(l_strDescriptionAlias) & "</TimecodeRangeType>"
'                Else
'                    MsgBox "The Logging item " & .adoLogging.Recordset("segmentreference") & " is not an approved item." & vbCrLf & "No XML has been generated.", vbCritical, "Error..."
'                    Exit Function
'                End If
'                WriteTabs 5
'                Print #1, "<InPointTimecode>" & .adoLogging.Recordset("timecodestart") & "</InPointTimecode>"
'                WriteTabs 5
'                Print #1, "<OutPointTimecode>" & FrameBefore(.adoLogging.Recordset("timecodestop"), m_Framerate) & "</OutPointTimecode>"
'                If Trim(" " & .adoLogging.Recordset("note")) <> "" Then
'                    WriteTabs 5
'                    Print #1, "<TimecodeRangeDescription>" & .adoLogging.Recordset("note") & "</TimecodeRangeDescription>"
'                End If
'                WriteTabs 4
'                Print #1, "</TimecodeRange>"
'            Else
'                If l_blnFoundProg = True And l_blnFoundProgEnd = True Then
'                    If .adoLogging.Recordset("timecodestart") = l_strProgramStart Then
'                        WriteTabs 4
'                        Print #1, "<TimecodeRange>"
'                        WriteTabs 5
'                        Print #1, "<TimecodeRangeType>PROGRAM_START</TimecodeRangeType>"
'                        WriteTabs 5
'                        Print #1, "<InPointTimecode>" & l_strProgramStart & "</InPointTimecode>"
'                        WriteTabs 5
'                        Print #1, "<OutPointTimecode>" & l_strProgramStart & "</OutPointTimecode>"
'                        WriteTabs 4
'                        Print #1, "</TimecodeRange>"
'                    End If
'                    WriteTabs 4
'                    Print #1, "<TimecodeRange>"
'                    WriteTabs 5
'                    Print #1, "<TimecodeRangeType>PROGRAM</TimecodeRangeType>"
'                    WriteTabs 5
'                    Print #1, "<InPointTimecode>" & .adoLogging.Recordset("timecodestart") & "</InPointTimecode>"
'                    WriteTabs 5
'                    Print #1, "<OutPointTimecode>" & FrameBefore(.adoLogging.Recordset("timecodestop"), m_Framerate) & "</OutPointTimecode>"
'                    WriteTabs 4
'                    Print #1, "</TimecodeRange>"
'                    If .adoLogging.Recordset("timecodestop") = l_strProgramEnd Then
'                        WriteTabs 4
'                        Print #1, "<TimecodeRange>"
'                        WriteTabs 5
'                        Print #1, "<TimecodeRangeType>PROGRAM_END</TimecodeRangeType>"
'                        WriteTabs 5
'                        Print #1, "<InPointTimecode>" & FrameBefore(l_strProgramEnd, m_Framerate) & "</InPointTimecode>"
'                        WriteTabs 5
'                        Print #1, "<OutPointTimecode>" & FrameBefore(l_strProgramEnd, m_Framerate) & "</OutPointTimecode>"
'                        WriteTabs 4
'                        Print #1, "</TimecodeRange>"
'                    End If
'                Else
'                    MsgBox "Problem determining the Programme_Start and Programme_End for the XML." & vbCrLf & "No XML has been generated.", vbCritical, "Error..."
'                    Exit Function
'                End If
'            End If
'            .adoLogging.Recordset.MoveNext
'        Loop
'
'        WriteTabs 3
'        Print #1, "</TimecodeRanges>"
'        WriteTabs 2
'        Print #1, "</TimecodeGroup>"
'        WriteTabs 1
'        Print #1, "</TimecodeGroups>"
'
'    End With
'
'    'Then close it all off
'    Print #1, "</BulkIngestData>"
'
'    Close #1
'
'    If g_intUseSvenskAutoSend <> 0 And lblLibraryID.Caption = "719953" Then
'
'        'Potentially?, if you get to this point, you could then go ahead and 'Send' the files to DADC SPE DBB.
'        'That would involve finding out whether it was a 'Send' or a 'Re-send', then copying this bunch of files in to the SPE Hot folder
'        'Then also 'Moving' the files to the 'Sent' Archive folder
'
'        If MsgBox("Do you wish to automatically send these items to DADC DBB via Aspera", vbYesNo, "Automatic Sending") = vbYes Then
'            On Error GoTo SENDING_ERROR
'            Set FSO = New Scripting.FileSystemObject
'            l_strPathForMove = GetData("library", "subtitle", "barcode", "BFS-OPS") & "\2. MASTERFILES\Sony_Pictures_Home_Entertainment\Sent_to_DBB"
''                l_strPathForMove = GetData("library", "subtitle", "barcode", "VIE-MS-1") & "\1261\04_FILES_TO_BE_MOVED"
'            If MsgBox("Is this an original send?", vbYesNo, "Automatic Sending") = vbYes Then
'                l_strFolderForFilerequest = Format(Now, "YYYY_MM_DD")
'            Else
'                l_strFolderForFilerequest = Format(Now, "YYYY_MM_DD") & "_RESEND"
'            End If
'            If FSO.FolderExists(l_strPathForMove) Then
'                If Not FSO.FolderExists(l_strPathForMove & "\" & l_strFolderForFilerequest) Then
'                    FSO.CreateFolder l_strPathForMove & "\" & l_strFolderForFilerequest
'                    If Not FSO.FolderExists(l_strPathForMove & "\" & l_strFolderForFilerequest) Then
'                        MsgBox "Could not make the delivery archive folder on BFS-OPS", vbCritical, "Error"
'                        Exit Function
'                    End If
'                End If
'            Else
'                MsgBox "Could not find the Send delivery archive folder on BFS-OPS", vbCritical, "Error"
'                Exit Function
'            End If
'            l_strPathForCopy = GetData("library", "subtitle", "barcode", "SPE-HOTFOLDER") & "\Aspera-Hotfolder"
'            l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "SPE-HOTFOLDER")
'            If FSO.FolderExists(l_strPathForCopy) Then
'                If Not FSO.FolderExists(l_strPathForCopy & "\" & l_strFolderForFilerequest) Then
'                    FSO.CreateFolder l_strPathForCopy & "\" & l_strFolderForFilerequest
'                    If Not FSO.FolderExists(l_strPathForCopy & "\" & l_strFolderForFilerequest) Then
'                        MsgBox "Could not make the delivery folder on SPE-HOTFOLDER", vbCritical, "Error"
'                        Exit Function
'                    End If
'                End If
'            Else
'                MsgBox "Could not find the Aspera HotFolder on SPE-HOTFOLDER", vbCritical, "Error"
'                Exit Function
'            End If
'            'Copy to Hot Folder - send the xml first - l_strFilenametosave and then move to archive folder.
'            If l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml" <> l_strFileNameToSave Then
'                If FSO.FileExists(l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml") Then FSO.DeleteFile l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml", True
'                FSO.MoveFile l_strFileNameToSave, l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml"
'            End If
'            FSO.CopyFile l_strPathForMove & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml", l_strPathForCopy & "\" & l_strFolderForFilerequest & "\" & txtReference.Text & ".xml"
'            'Then so the actual asset
'            'the request to move it to the 'To be moved' folder
'            If MsgBox("Did you want to ONLY send the xml (and not send the asset also)?", vbYesNo + vbDefaultButton2) = vbNo Then
'                If (lblLibraryID.Caption <> GetData("library", "libraryID", "barcode", "BFS-OPS")) Or (lblLibraryID.Caption = GetData("library", "libraryID", "barcode", "BFS-OPS") And txtAltFolder.Text <> "2. MASTERFILES\Sony_Pictures_Home_Entertainment\Sent_to_DBB\" & l_strFolderForFilerequest) Then
'                    On Error GoTo CONTINUE_ROUTINE
'                    l_strFileNameToSave = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption)
'                    l_strFileNameToSave = l_strFileNameToSave & "\" & txtAltFolder.Text & "\" & txtClipfilename.Text
'                    If LCase(l_strFileNameToSave) <> LCase(GetData("library", "subtitle", "barcode", "BFS-OPS") & "\2. MASTERFILES\Sony_Pictures_Home_Entertainment\Sent_to_DBB\" & l_strFolderForFilerequest & "\" & txtClipfilename.Text) Then
'                        FSO.MoveFile l_strFileNameToSave, GetData("library", "subtitle", "barcode", "BFS-OPS") & "\2. MASTERFILES\Sony_Pictures_Home_Entertainment\Sent_to_DBB\" & l_strFolderForFilerequest & "\" & txtClipfilename.Text
'                        SetData "events", "libraryID", "eventID", txtClipID.Text, GetData("library", "libraryID", "barcode", "BFS-OPS")
'                        SetData "events", "altlocation", "eventID", txtClipID.Text, "2. MASTERFILES\Sony_Pictures_Home_Entertainment\Sent_to_DBB\" & l_strFolderForFilerequest
'                    End If
'CONTINUE_ROUTINE:
'                End If
'                On Error GoTo SENDING_ERROR
'                'the copy to the Hot Folder
'                l_strSQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewLibraryID, NewAltLocation, DoNotRecordCopy, RequestName, RequesterEmail) VALUES ("
'                l_strSQL = l_strSQL & txtClipID.Text & ", "
'                l_strSQL = l_strSQL & lblLibraryID.Caption & ", "
'                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
'                l_strSQL = l_strSQL & l_lngNewLibraryID & ", "
'                l_strSQL = l_strSQL & "'Aspera-Hotfolder\" & l_strFolderForFilerequest & "', "
'                l_strSQL = l_strSQL & "1, '" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
'                Debug.Print l_strSQL
'                ExecuteSQL l_strSQL, g_strExecuteError
'                CheckForSQLError
'            End If
'            l_strSQL = "UPDATE tracker_spe_item SET Date_XML_Made = '" & FormatSQLDate(Now) & "' WHERE clipreference = '" & txtReference.Text & "' AND readytobill = 0;"
'            ExecuteSQL l_strSQL, g_strExecuteError
'            CheckForSQLError
'            MsgBox "Done"
'        End If
'    End If
'    MakeDADCXMLFileSPE = True
'
'End If
'Beep
'
'Exit Function
'
'DADC_XML_ERROR1:
'
'MsgBox "Error: " & Err.Number & ", " & Err.Description, vbCritical, "An Error occured."
'MakeDADCXMLFileSPE = False
'
'Exit Function
'
'SENDING_ERROR:
'MsgBox "There was a problem doing the autosend - it has probably not happened." & vbCrLf & "Please check and send the items manually if necessary.", vbCritical
'
'End Function
'
Private Sub WriteNewFileDescriptorSPE(lp_intTabCount As Integer)

With frmClipControl
    
    WriteTabs (lp_intTabCount)
    Print #1, "<inv:FileDescriptors xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
    WriteTabs (lp_intTabCount + 1)
    Print #1, "<inv:FileDescriptor>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:FileName>" & .txtClipfilename.Text & "</inv:FileName>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:EncodingEquipment></inv:EncodingEquipment>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:EncodingProfile>";
    Select Case Val(.txtVertpixels.Text)
        Case 576
            If .adoLogging.Recordset.RecordCount = 1 Then
                Print #1, "P2P ProRes PAL 25i ";
                If .cmbAspect.Text = "16:9" Then
                    Print #1, "16x9";
                Else
                    Print #1, "4x3";
                End If
            Else
                Print #1, "Archival ProRes PAL 25i ";
                If .cmbAspect.Text = "16:9" Then
                    Print #1, "16x9";
                Else
                    Print #1, "4x3";
                End If
            End If
        Case 486
            If .adoLogging.Recordset.RecordCount = 1 Then
                Print #1, "P2P ProRes NTSC 29.97i ";
                If .cmbAspect.Text = "16:9" Then
                    Print #1, "16x9";
                Else
                    Print #1, "4x3";
                End If
            Else
                Print #1, "Archival ProRes NTSC 29.97i ";
                If .cmbAspect.Text = "16:9" Then
                    Print #1, "16x9";
                Else
                    Print #1, "4x3";
                End If
            End If
        Case 1080
            If .adoLogging.Recordset.RecordCount = 1 Then
                Print #1, "P2P ProRes HD ";
                If .cmbFrameRate.Text = "23.98" Or .cmbFrameRate.Text = "24" Then
                    Print #1, "24p";
                ElseIf .cmbFrameRate.Text = "29.97" And .cmbInterlace.Text = "Progressive" Then
                    Print #1, "29.97p";
                ElseIf .cmbFrameRate.Text = "29.97" And .cmbInterlace.Text = "Interlace" Then
                    Print #1, "29.97i";
                ElseIf .cmbFrameRate.Text = "25" And .cmbInterlace.Text = "Progressive" Then
                    Print #1, "25p";
                ElseIf .cmbFrameRate.Text = "25" And .cmbInterlace.Text = "Interlace" Then
                    Print #1, "25i";
                End If
            Else
                Print #1, "Archival ProRes HD ";
                If .cmbFrameRate.Text = "23.98" Or .cmbFrameRate.Text = "24" Then
                    Print #1, "24p";
                ElseIf .cmbFrameRate.Text = "29.97" And .cmbInterlace.Text = "Progressive" Then
                    Print #1, "29.97p";
                ElseIf .cmbFrameRate.Text = "29.97" And .cmbInterlace.Text = "Interlace" Then
                    Print #1, "29.97i";
                ElseIf .cmbFrameRate.Text = "25" And .cmbInterlace.Text = "Progressive" Then
                    Print #1, "25p";
                ElseIf .cmbFrameRate.Text = "25" And .cmbInterlace.Text = "Interlace" Then
                    Print #1, "25i";
                End If
            End If
    End Select
    Print #1, "</inv:EncodingProfile>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:ChecksumValue></inv:ChecksumValue>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:ChecksumType></inv:ChecksumType>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:EncodeDate></inv:EncodeDate>"
    WriteTabs (lp_intTabCount + 2)
    Print #1, "<inv:EncodeVendorOrganizationName>" & GetData("tracker_spe_item", "Mastering_Vendor", "clipreference", .txtReference.Text) & "</inv:EncodeVendorOrganizationName>"
    WriteTabs (lp_intTabCount + 1)
    Print #1, "</inv:FileDescriptor>"
    WriteTabs (lp_intTabCount)
    Print #1, "</inv:FileDescriptors>"

End With

End Sub

Private Sub WriteNewFileDescriptorSPEAudio(lp_intTabCount As Integer, lp_strType As String)

WriteTabs (lp_intTabCount)
Print #1, "<inv:FileDescriptors xmlns=""http://schema.dadcdigital.com/dbb/data/2010/inventory/"">"
WriteTabs (lp_intTabCount + 1)
Print #1, "<inv:FileDescriptor>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:FileName>" & frmClipControl.txtClipfilename.Text & "</inv:FileName>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingEquipment></inv:EncodingEquipment>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodingProfile>";
Select Case lp_strType ' cmbAudioType1.Text = "5.1" Or cmbAudioType1.Text = "VOD 5.1"
    Case "Standard Stereo"
        Print #1, "Muxed Standard Stereo MOV PCM";
    Case "LT/RT"
        Print #1, "Muxed LT/RT MOV PCM";
    Case "Mono 2.0"
        Print #1, "Muxed Mono 2.0 MOV PCM";
    Case "5.1"
        Print #1, "Muxed 5.1 MOV PCM";
    Case "Mono"
        Print #1, "Muxed Mono MOV PCM";
    Case Else
        Print #1, "Dolby E Muxed MOV";
End Select
Print #1, "</inv:EncodingProfile>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumValue></inv:ChecksumValue>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:ChecksumType></inv:ChecksumType>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeDate></inv:EncodeDate>"
WriteTabs (lp_intTabCount + 2)
Print #1, "<inv:EncodeVendorOrganizationName>" & GetData("tracker_spe_item", "Mastering_Vendor", "clipreference", txtReference.Text) & "</inv:EncodeVendorOrganizationName>"
WriteTabs (lp_intTabCount + 1)
Print #1, "</inv:FileDescriptor>"
WriteTabs (lp_intTabCount)
Print #1, "</inv:FileDescriptors>"

End Sub


Private Sub WriteNewAudioChannelComponentsSPE(lp_intTabCount As Integer, lp_strType As String, ByRef lp_lngChannelCount As Long, Optional lp_strAudioContent As String, Optional lp_strAudioType As String)

If lp_strType = "2track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 1</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioType = "LT/RT" Then
        Print #1, "<inv:AudioChannelAssignment>Left Total</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 1 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    If Left(lp_strAudioType, 7) = "Dolby E" Then
        Print #1, "<inv:AudioChannelAssignment>Dolby E 2</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioContent = "MOS" Then
        Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    ElseIf lp_strAudioType = "LT/RT" Then
        Print #1, "<inv:AudioChannelAssignment>Right Total</inv:AudioChannelAssignment>"
    Else
        Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    End If
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 2 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 2

ElseIf lp_strType = "1track" Then
    
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Mono</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 1 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 1

ElseIf lp_strType = "6track" Then

    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 1 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 1 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 1 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 2 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 2 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 2 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 3 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 3 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Front Center</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 3 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 4 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 4 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>LFE</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 4 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 5 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 5 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Left</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 5 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    WriteTabs lp_intTabCount
    Print #1, "<inv:AudioChannelComponent>"
    WriteNewFileDescriptorSPEAudio lp_intTabCount + 1, lp_strAudioType
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelInFile>" & lp_lngChannelCount + 6 & "</inv:AudioChannelInFile>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelOnTape>" & lp_lngChannelCount + 6 & "</inv:AudioChannelOnTape>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioChannelAssignment>Rear Right</inv:AudioChannelAssignment>"
    WriteTabs lp_intTabCount + 1
    Print #1, "<inv:AudioStreamInFile>" & lp_lngChannelCount + 6 & "</inv:AudioStreamInFile>"
    WriteTabs lp_intTabCount
    Print #1, "</inv:AudioChannelComponent>"
    lp_lngChannelCount = lp_lngChannelCount + 6

End If

End Sub

Private Sub MakeGoogleShowXML()

If Val(txtClipID.Text) = 0 Then Exit Sub

If Val(Trim(" " & GetDataSQL("SELECT TOP 1 tracker_google_itemID FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Google_Video_File_Name = '" & txtClipfilename.Text & "';"))) = 0 Then
    MsgBox "Cannot find a Google Tracker item where the filenames match.", vbCritical, "Error"
    Exit Sub
End If

Dim l_strFileNameToSave As String, l_rst As ADODB.Recordset, l_strSQL As String
Dim FSO As Scripting.FileSystemObject

l_strSQL = "SELECT * FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Google_Video_File_Name = '" & txtClipfilename.Text & "';"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount <= 0 Then
    l_rst.Close
    Set l_rst = Nothing
    Exit Sub
End If

'Check the items that relate to a shopw xml and verify they exist.
If Trim(" " & l_rst("Video_Order_ID")) = "" Or Trim(" " & l_rst("Landscape")) = "" Or Trim(" " & l_rst("square")) = "" Or Trim(" " & l_rst("Custom_ID_MPM_Product_ID")) = "" Or Trim(" " & l_rst("Show_Description")) = "" Or Trim(" " & l_rst("Production_Year")) = "" _
Or Trim(" " & l_rst("Show_Season_Title")) = "" Or Trim(" " & l_rst("Key_Words")) = "" Or Trim(" " & l_rst("Show_Genre")) = "" Or Trim(" " & l_rst("Google_Channel_ID")) = "" Or Trim(" " & l_rst("Spoken_Language")) = "" Then
    l_rst.Close
    Set l_rst = Nothing
    MsgBox "Essentrial Tracmer information for this item is not yet complete. Video Order ID, Artwork filenames, Custom_ID, " & vbCrLf & _
    "Show Season Description, Production Year, Show Season Title, Key Words, Google Channel ID, Spoken Language and Show Genre are all required.", vbCritical
End If

MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtReference.Text & "_show.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
    
    Open l_strFileNameToSave For Output As 1
    
    Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
    Print #1, "<feed xmlns=""http://www.youtube.com/schemas/cms/2.0"" notification_email=""jono.mahoney@rrmededia.com"" channel=""" & l_rst("Google_Channel_ID") & """>"

    Print #1, Chr(9) & "<file type=""image"" tag=""show_square"">"
    Print #1, Chr(9) & Chr(9) & "<filename>" & l_rst("square") & "</filename>"
    Print #1, Chr(9) & "</file>"
    Print #1, Chr(9) & "<file type=""image"" tag=""show_wide"">"
    Print #1, Chr(9) & Chr(9) & "<filename>" & l_rst("landscape") & "</filename>"
    Print #1, Chr(9) & "</file>"
    Print #1, Chr(9) & "<asset type=""show"">"
    Print #1, Chr(9) & Chr(9) & "<actor></actor>"
    Print #1, Chr(9) & Chr(9) & "<artwork type=""show_widescreen"" path=""/feed/file[@tag='show_wide']"" />"
    Print #1, Chr(9) & Chr(9) & "<artwork type=""show_square"" path=""/feed/file[@tag='show_square']"" />"
    Print #1, Chr(9) & Chr(9) & "<broadcaster></broadcaster>"
    Print #1, Chr(9) & Chr(9) & "<custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "</custom_id>"
    Print #1, Chr(9) & Chr(9) & "<description>" & UTF8_Encode(XMLSanitise(l_rst("Show_Description"))) & "</description>"
    Print #1, Chr(9) & Chr(9) & "<director></director>"
    Print #1, Chr(9) & Chr(9) & "<end_year></end_year>"
    Print #1, Chr(9) & Chr(9) & "<genre>" & XMLSanitise(l_rst("Show_Genre")) & "</genre>"
    Print #1, Chr(9) & Chr(9) & "<keyword>" & UTF8_Encode(XMLSanitise(l_rst("Key_Words"))) & "</keyword>"
    Print #1, Chr(9) & Chr(9) & "<producer></producer>"
    Print #1, Chr(9) & Chr(9) & "<shows_and_movies_programming>True</shows_and_movies_programming>"
    Print #1, Chr(9) & Chr(9) & "<start_year>" & l_rst("PRoduction_Year") & "</start_year>"
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(l_rst("Show_Season_Title"))) & "</title>"
    Print #1, Chr(9) & Chr(9) & "<writer></writer>"
    Print #1, Chr(9) & Chr(9) & "<spoken_language>" & l_rst("Spoken_Language") & "</spoken_language>"
    Print #1, Chr(9); "</asset>"
    Print #1, "</feed>"
    
    Close #1

End If

l_rst.Close
Set l_rst = Nothing

MsgBox "Done!", vbInformation

End Sub

Private Sub MakeGoogleSeasonXML()

If Val(txtClipID.Text) = 0 Then Exit Sub

If Val(Trim(" " & GetDataSQL("SELECT TOP 1 tracker_google_itemID FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Google_Video_File_Name = '" & txtClipfilename.Text & "';"))) = 0 Then
    MsgBox "Cannot find a Google Tracker item where the filenames match.", vbCritical, "Error"
    Exit Sub
End If

If MsgBox("Are all the expected episdes for this series present in the Google Tracker yet?", vbYesNo, "Checking Season's Episode Count") = vbNo Then
    MsgBox "Please ensure all episodes are in the tracker before making the Season XML", vbCritical
    Exit Sub
End If

Dim l_strFileNameToSave As String, l_rst As ADODB.Recordset, l_strSQL As String
Dim FSO As Scripting.FileSystemObject

l_strSQL = "SELECT * FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Google_Video_File_Name = '" & txtClipfilename.Text & "';"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount <= 0 Then
    l_rst.Close
    Set l_rst = Nothing
    Exit Sub
End If

'Check the items that relate to a shopw xml and verify they exist.
If Trim(" " & l_rst("Video_Order_ID")) = "" Or Trim(" " & l_rst("Custom_ID_MPM_Product_ID")) = "" Or Trim(" " & l_rst("Season_Name")) = "" Or Trim(" " & l_rst("Series_Number")) = "" _
Or Trim(" " & l_rst("Show_Season_Title")) = "" Or Trim(" " & l_rst("Google_Channel_ID")) = "" Then
    l_rst.Close
    Set l_rst = Nothing
    MsgBox "Essentrial Tracmer information for this item is not yet complete. Video_Order_ID, Custom_ID, " & vbCrLf & _
    "Show Season Title, Season Number, Season Name, and Google Channel ID.", vbCritical
End If

MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtReference.Text & "_season.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
    
    Open l_strFileNameToSave For Output As 1
    
    Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
    Print #1, "<feed xmlns=""http://www.youtube.com/schemas/cms/2.0"" notification_email=""jono.mahoney@rrmededia.com"" channel=""" & l_rst("Google_Channel_ID") & """>"

    Print #1, Chr(9) & "<asset type=""season"">"
    Print #1, Chr(9) & Chr(9) & "<description>" & UTF8_Encode(XMLSanitise(l_rst("Season_Description"))) & "</description>"
    Print #1, Chr(9) & Chr(9) & "<season>" & l_rst("Series_Number") & "</season>"
    Print #1, Chr(9) & Chr(9) & "<season_name>" & l_rst("Season_Name") & "</season_name>"
'    Print #1, Chr(9) & Chr(9) & "<custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "</custom_id>"
    Print #1, Chr(9) & Chr(9) & "<custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "_season" & l_rst("Series_Number") & "</custom_id>"
    Print #1, Chr(9) & Chr(9) & "<show_custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "</show_custom_id>"
    Print #1, Chr(9) & Chr(9) & "<shows_and_movies_programming>True</shows_and_movies_programming>"
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(l_rst("Show_Season_Title"))) & "</title>"
    l_strSQL = "SELECT COUNT(tracker_google_itemID) FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Custom_ID_MPM_Product_ID = '" & l_rst("Custom_ID_MPM_Product_ID") & "'"
    Debug.Print l_strSQL
    Print #1, Chr(9) & Chr(9) & "<total_episodes_expected>" & GetCount(l_strSQL) & "</total_episodes_expected>"
    Print #1, Chr(9); "</asset>"
    Print #1, "</feed>"
    
    Close #1

End If

l_rst.Close
Set l_rst = Nothing

MsgBox "Done!", vbInformation

End Sub

Private Sub MakeGoogleEpisodeXML()

If Val(txtClipID.Text) = 0 Then Exit Sub

If Val(Trim(" " & GetDataSQL("SELECT TOP 1 tracker_google_itemID FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Google_Video_File_Name = '" & txtClipfilename.Text & "';"))) = 0 Then
    MsgBox "Cannot find a Google Tracker item where the filenames match.", vbCritical, "Error"
    Exit Sub
End If

Dim l_strFileNameToSave As String, l_rst As ADODB.Recordset, l_strSQL As String
Dim FSO As Scripting.FileSystemObject

l_strSQL = "SELECT * FROM tracker_google_item WHERE companyID = " & lblCompanyID.Caption & " AND readytobill = 0 AND Google_Video_File_Name = '" & txtClipfilename.Text & "';"
Set l_rst = ExecuteSQL(l_strSQL, g_strExecuteError)
If l_rst.RecordCount <= 0 Then
    l_rst.Close
    Set l_rst = Nothing
    Exit Sub
End If

'Check the items that relate to a shopw xml and verify they exist.
If Trim(" " & l_rst("Video_Order_ID")) = "" Or Trim(" " & l_rst("Custom_ID_MPM_Product_ID")) = "" Or Trim(" " & l_rst("Episodic_title")) = "" Or Trim(" " & l_rst("Series_Number")) = "" Or Trim(" " & l_rst("Episode_Number")) = "" _
Or Trim(" " & l_rst("Episodic_Title")) = "" Or Trim(" " & l_rst("Episodic_description")) = "" Or Trim(" " & l_rst("Original_release_date")) = "" Or Trim(" " & l_rst("Google_Channel_ID")) = "" _
Or Trim(" " & l_rst("rating_system")) = "" Or Trim(" " & l_rst("Rating")) = "" Or Trim(" " & l_rst("episode_genre")) = "" Or Trim(" " & l_rst("key_words")) = "" Then
    l_rst.Close
    Set l_rst = Nothing
    MsgBox "Essentrial Tracmer information for this item is not yet complete. Video_Order_ID, Custom_ID, " & vbCrLf & _
    "Show Season Title, Season Number, Season Name, and Google Channel ID.", vbCritical
End If

MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtReference.Text & "_episode.xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
    
    Open l_strFileNameToSave For Output As 1
    
    Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
    Print #1, "<feed xmlns=""http://www.youtube.com/schemas/cms/2.0"" notification_email=""jono.mahoney@rrmededia.com"" channel=""" & l_rst("Google_Channel_ID") & """>"
    Print #1, Chr(9) & "<file type=""video"" tag=""episode" & l_rst("episode_number") & """>"
    Print #1, Chr(9) & Chr(9) & "<filename>" & txtClipfilename.Text & "</filename>"
    Print #1, Chr(9) & "</file>"
    Print #1, Chr(9) & "<asset type=""episode"" tag=""episode" & l_rst("episode_number") & """>"
    Print #1, Chr(9) & Chr(9) & "<content_type>Full Episode</content_type>"
    Print #1, Chr(9) & Chr(9) & "<description>" & UTF8_Encode(XMLSanitise(l_rst("Episodic_description"))) & "</description>"
    Print #1, Chr(9) & Chr(9) & "<episode>" & l_rst("episode_number") & "</episode>"
    Print #1, Chr(9) & Chr(9) & "<original_release_date>" & Format(l_rst("Original_release_date"), "YYYY-MM-DD") & "</original_release_date>"
    Print #1, Chr(9) & Chr(9) & "<original_release_medium>Terrestrial TV</original_release_medium>"
    Print #1, Chr(9) & Chr(9) & "<rating system=""" & l_rst("rating_system") & """>" & l_rst("rating") & "</rating>"
    Print #1, Chr(9) & Chr(9) & "<season>" & l_rst("series_number") & "</season>"
'    Print #1, Chr(9) & Chr(9) & "<custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "</custom_id>"
    Print #1, Chr(9) & Chr(9) & "<custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "_season" & l_rst("Series_Number") & "_episode" & l_rst("episode_number") & "</custom_id>"
    Print #1, Chr(9) & Chr(9) & "<show_custom_id>" & l_rst("Custom_ID_MPM_Product_ID") & "</show_custom_id>"
    Print #1, Chr(9) & Chr(9) & "<shows_and_movies_programming>True</shows_and_movies_programming>"
    Print #1, Chr(9) & Chr(9) & "<spoken_language>" & l_rst("Spoken_language") & "</spoken_language>"
'    <subtitled_language></subtitled_language>
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(l_rst("Episodic_Title"))) & "</title>"
    Print #1, Chr(9) & "</asset>"
    Print #1, Chr(9) & "<video tag=""episode" & l_rst("episode_number") & """>"
    Print #1, Chr(9) & Chr(9) & "<allow_comments>Always</allow_comments>"
    Print #1, Chr(9) & Chr(9) & "<allow_embedding>True</allow_embedding>"
    Print #1, Chr(9) & Chr(9) & "<allow_ratings>True</allow_ratings>"
    Print #1, Chr(9) & Chr(9) & "<allow_syndication>True</allow_syndication>"
    Print #1, Chr(9) & Chr(9) & "<channel>" & l_rst("Google_Channel_ID") & "</channel>"
    Print #1, Chr(9) & Chr(9) & "<description>" & UTF8_Encode(XMLSanitise(l_rst("Episodic_Description"))) & "</description>"
    Print #1, Chr(9) & Chr(9) & "<genre>" & XMLSanitise(l_rst("Episode_genre")) & "</genre>"
    Print #1, Chr(9) & Chr(9) & "<keyword>" & UTF8_Encode(XMLSanitise(l_rst("Key_Words"))) & "</keyword>"
    Print #1, Chr(9) & Chr(9) & "<video_order_id>" & l_rst("Video_Order_ID") & "</video_order_id>"
    Print #1, Chr(9) & Chr(9) & "<paid_content>true</paid_content>"
    Print #1, Chr(9) & Chr(9) & "<public>false</public>"
    Print #1, Chr(9) & Chr(9) & "<title>" & UTF8_Encode(XMLSanitise(l_rst("Episodic_title"))) & "</title>"
    Print #1, Chr(9) & "</video>"
    Print #1, Chr(9) & "<relationship>"
    Print #1, Chr(9) & Chr(9) & "<item path=""/feed/file[@tag='episode" & l_rst("episode_number") & "']"" />"
'    Print #1, Chr(9) & Chr(9) & "<related_item path=""/feed/asset[@tag='episode1']"" />"
    Print #1, Chr(9) & Chr(9) & "<related_item path=""/feed/video[@tag='episode" & l_rst("episode_number") & "']"" />"
    Print #1, Chr(9) & "</relationship>"
    Print #1, Chr(9) & "<ownership asset=""/feed/asset[@tag='episode" & l_rst("episode_number") & "']"">"
    Print #1, Chr(9) & Chr(9) & "<owner>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "<rule percentage=""100"">"
    Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<condition restriction=""include"" type=""territory"">" & GetData("iso2a", "iso2acode", "country", l_rst("Territory")) & "</condition>"
    Print #1, Chr(9) & Chr(9) & Chr(9) & "</rule>"
    Print #1, Chr(9) & Chr(9) & "</owner>"
    Print #1, Chr(9) & "</ownership>"
    Print #1, Chr(9) & "<rights_admin type=""usage"" tag=""rights_usage1"" owner=""true"" />"
    Print #1, Chr(9) & "<rights_policy tag=""rights_usage1"">"
    Print #1, Chr(9) & Chr(9) & "<rule action=""track"" />"
    Print #1, Chr(9) & "</rights_policy>"
    Print #1, Chr(9) & "<claim type=""audiovisual"" asset=""/feed/asset[@tag='episode" & l_rst("episode_number") & "']"" video=""/feed/video[@tag='episode" & l_rst("episode_number") & "']"" rights_admin=""/feed/rights_admin[@tag='rights_usage1']"" rights_policy=""/feed/rights_policy[@tag='rights_usage1']"" />"
'    Print #1, Chr(9) & "<rights_admin type=""match"" tag=""rights_match1"" owner=""true"" />"
'    Print #1, Chr(9) & "<relationship>"
'    Print #1, Chr(9) & Chr(9) & "<item path=""/feed/rights_admin[@tag='rights_match1']"" />"
'    Print #1, Chr(9) & Chr(9) & "<item path=""/external/rights_policy[@name='Monetize in all countries']"" />"
'    Print #1, Chr(9) & Chr(9) & "<related_item path=""/feed/asset[@tag='episode1']"" />"
'    Print #1, Chr(9) & "</relationship>"
    Print #1, "</feed>"
    
    Close #1

End If

l_rst.Close
Set l_rst = Nothing

MsgBox "Done!", vbInformation

End Sub

Function MakeDiscoveryXML() As Boolean

If txtReference.Text = "" Or cmbActiveRatio.Text = "" Or cmbIsTextInPicture.Text = "" Or cmbPictFormat.Text = "" Then
    MsgBox "Data is not completed", vbCritical, "Error"
    Exit Function
End If

Dim l_strFileNameToSave As String
Dim FSO As Scripting.FileSystemObject
Dim l_lngChannelCount As Long, l_lngChannel As Long, l_lngSegmentCount As Long

MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtReference.Text & ".xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
    
    Open l_strFileNameToSave For Output As 1
    
    Print #1, "<?xml version=""1.0""?>"
    Print #1, "<MediaAsset date=""" & Format(Now, "mm-dd-yyyy") & """>"
    WriteTabs 1
    Print #1, "<XMLGeneratorVr>RRmedia CETA Version 2.9.0</XMLGeneratorVr>"
    WriteTabs 1
    Print #1, "<MasterType>" & "Look up the Program Type from the Tracker" & "</MasterType>"
    WriteTabs 1
    Print #1, "<PropertyID>" & "Look up the Property ID from the Tracker" & "</PropertyID>"
    WriteTabs 1
    Print #1, "<OriginatingNetwork>" & "Lookup" & "</OriginatingNetwork>"
    WriteTabs 1
    Print #1, "<ProgramTitle>" & "Lookup" & "</ProgramTitle>"
    WriteTabs 1
    Print #1, "<EpisodeNum>" & "Lookup" & "</EpisodeNum>"
    WriteTabs 1
    Print #1, "<EpisodeTitle>" & "Lookup" & "</EpisodeTitle>"
    WriteTabs 1
    Print #1, "<SeasonNum>" & "Lookup " & "</SeasonNum>"
    WriteTabs 1
    Print #1, "<ProductionCompany>" & "Lookup" & "</ProductionCompany>"
    WriteTabs 1
    Print #1, "<CreationDate>" & Format(Now, "mm/dd/yyyy") & "</CreationDate>"
    WriteTabs 1
    Print #1, "<TRT>" & "Lookup" & "</TRT>"
    WriteTabs 1
    Print #1, "<MediaDuration>" & "Lookup" & "</MediaDuration>"
    WriteTabs 1
    Print #1, "<AspectRatio>" & "Lookup" & "<AspectRatio>"
    WriteTabs 1
    Print #1, "<VideoFormat>" & "Lookup" & "</VideoFormat>"
    WriteTabs 1
    Print #1, "<CutType>" & "Lookup" & "</CutType>"
    WriteTabs 1
    Print #1, "<TextStatus>" & "Lookup" & "</TextStatus>"
    WriteTabs 1
    Print #1, "<CreditStatus>" & "Lookup" & "</CreditStatus>"
    WriteTabs 1
    Print #1, "<Language>" & "Lookup" & "</Language>"
    WriteTabs 1
    Print #1, "<TCType>" & "Lookup" & "</TCType>"
    
    'Then an Audio Section
    l_lngChannelCount = 0
    l_lngChannel = 0
    With frmClipControl
        If .cmbAudioType1.Text <> "" Then
            If .cmbAudioType1.Text = "Standard Stereo" Or .cmbAudioType1.Text = "LT/RT" Or Left(.cmbAudioType1.Text, 7) = "Dolby E" Or .cmbAudioType1.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType1.Text = "5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType1.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType2.Text <> "" Then
            If .cmbAudioType2.Text = "Standard Stereo" Or .cmbAudioType2.Text = "LT/RT" Or Left(.cmbAudioType2.Text, 7) = "Dolby E" Or .cmbAudioType2.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType2.Text = "5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType2.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType3.Text <> "" Then
            If .cmbAudioType3.Text = "Standard Stereo" Or .cmbAudioType3.Text = "LT/RT" Or .cmbAudioType3.Text = "VOD Standard Stereo" Or .cmbAudioType3.Text = "VOD LT/RT" Or Left(.cmbAudioType3.Text, 7) = "Dolby E" Or .cmbAudioType3.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType3.Text = "5.1" Or .cmbAudioType3.Text = "VOD 5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType3.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType4.Text <> "" Then
            If .cmbAudioType4.Text = "Standard Stereo" Or .cmbAudioType4.Text = "LT/RT" Or Left(.cmbAudioType4.Text, 7) = "Dolby E" Or .cmbAudioType4.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType4.Text = "5.1" Or .cmbAudioType4.Text = "VOD 5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType4.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType5.Text <> "" Then
            If .cmbAudioType5.Text = "Standard Stereo" Or .cmbAudioType5.Text = "LT/RT" Or .cmbAudioType5.Text = "VOD Standard Stereo" Or .cmbAudioType5.Text = "VOD LT/RT" Or Left(.cmbAudioType5.Text, 7) = "Dolby E" Or .cmbAudioType5.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType5.Text = "5.1" Or .cmbAudioType5.Text = "VOD 5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType5.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType6.Text <> "" Then
            If .cmbAudioType6.Text = "Standard Stereo" Or .cmbAudioType6.Text = "LT/RT" Or .cmbAudioType6.Text = "VOD Standard Stereo" Or .cmbAudioType6.Text = "VOD LT/RT" Or Left(.cmbAudioType6.Text, 7) = "Dolby E" Or .cmbAudioType6.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType6.Text = "5.1" Or .cmbAudioType6.Text = "VOD 5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType6.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType7.Text <> "" Then
            If .cmbAudioType7.Text = "Standard Stereo" Or .cmbAudioType7.Text = "LT/RT" Or .cmbAudioType7.Text = "VOD Standard Stereo" Or .cmbAudioType7.Text = "VOD LT/RT" Or Left(.cmbAudioType7.Text, 7) = "Dolby E" Or .cmbAudioType7.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType7.Text = "5.1" Or .cmbAudioType7.Text = "VOD 5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType7.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If
        If .cmbAudioType8.Text <> "" Then
            If .cmbAudioType8.Text = "Standard Stereo" Or .cmbAudioType8.Text = "LT/RT" Or .cmbAudioType8.Text = "VOD Standard Stereo" Or .cmbAudioType8.Text = "VOD LT/RT" Or Left(.cmbAudioType8.Text, 7) = "Dolby E" Or .cmbAudioType8.Text = "Mono 2.0" Then
                l_lngChannel = l_lngChannel + 2
            ElseIf .cmbAudioType8.Text = "5.1" Or .cmbAudioType8.Text = "VOD 5.1" Then
                l_lngChannel = l_lngChannel + 6
            ElseIf .cmbAudioType8.Text = "Mono" Then
                l_lngChannel = l_lngChannel + 1
            Else
                MsgBox "Unrecognised Audio Set " & .cmbAudioType1.Text, vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
        End If

        WriteTabs 1
        Print #1, "<Audiotracks>"
        WriteTabs 1
        Print l_lngChannel
        
        If .cmbAudioType1.Text <> "" Then
            Select Case .cmbAudioType1
                Case "Standard Stereo"
                    Select Case .cmbAudioContent1
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent1
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent1
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage1.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType2.Text <> "" Then
            Select Case .cmbAudioType2
                Case "Standard Stereo"
                    Select Case .cmbAudioContent2
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent2
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage2.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType3.Text <> "" Then
            Select Case .cmbAudioType3
                Case "Standard Stereo"
                    Select Case .cmbAudioContent3
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent3
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent3
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage3.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType4.Text <> "" Then
            Select Case .cmbAudioType4
                Case "Standard Stereo"
                    Select Case .cmbAudioContent4
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent4
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent4
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage4.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType5.Text <> "" Then
            Select Case .cmbAudioType5
                Case "Standard Stereo"
                    Select Case .cmbAudioContent1
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent5
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent5
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage5.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType6.Text <> "" Then
            Select Case .cmbAudioType6
                Case "Standard Stereo"
                    Select Case .cmbAudioContent6
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent6
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent6
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage6.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType7.Text <> "" Then
            Select Case .cmbAudioType7
                Case "Standard Stereo"
                    Select Case .cmbAudioContent7
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent7
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent7
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage7.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
        If .cmbAudioType8.Text <> "" Then
            Select Case .cmbAudioType8
                Case "Standard Stereo"
                    Select Case .cmbAudioContent8
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""FML"">Full Mix Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""FMR"">Full Mix Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Mix Minus Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""MML"">Mix Minus Narration Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""MMR"">Mix Minus Narration Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""None"" Shortcode=""ML"">Music Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""None"" Shortcode=""MR"">Music Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""EL"">Sound Effects/SOT/BG dialog Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""ER"">Sound Effects/SOT/BG dialog Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case "Music and Effects"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""MEL"">Music and Effects Stereo Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""MER"">Music and Effects Stereo Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 2
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "Mono"
                    Select Case .cmbAudioContent8
                        Case "Sound Effects/SOT/BG dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""E"">Sound Effects/SOT/BG dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Interview and foreground dialog"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""D"">Interview and foreground dialog Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case "Narration"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""D"">Narration Mono</Track""" & l_lngChannelCount + 1 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 1
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case "5.1"
                    Select Case .cmbAudioContent8
                        Case "Full Mix"
                            WriteTabs 2
                            Print #1, "<Track" & l_lngChannelCount + 1 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""5FL"">Full Mix 5.1 Left</Track""" & l_lngChannelCount + 1 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 2 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""5FR"">Full Mix 5.1 Right</Track""" & l_lngChannelCount + 2 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 3 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""C"">Full Mix 5.1 Center</Track""" & l_lngChannelCount + 3 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 4 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""LFE"">Full Mix 5.1 LFE</Track""" & l_lngChannelCount + 4 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 5 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""5L"">Full Mix 5.1 Left Surround</Track""" & l_lngChannelCount + 5 & "Des>"
                            Print #1, "<Track" & l_lngChannelCount + 6 & "Des Language=""" & .cmbAudioLanguage8.Text & """ Shortcode=""5R"">Full Mix 5.1 Right Surround</Track""" & l_lngChannelCount + 6 & "Des>"
                            l_lngChannelCount = l_lngChannelCount + 6
                        Case Else
                            MsgBox "Audio content not supported for this Audio Type for Discovery On-Ramp", vbCritical
                            MakeDiscoveryXML = False
                            Exit Function
                    End Select
                Case Else
                    MsgBox "Audio Type not supported for Discovery On-Ramp", vbCritical
                    MakeDiscoveryXML = False
                    Exit Function
            End Select
        End If
                
        'Segments Next.
        'Check all logged items have valid timecode
        Do While Not .adoLogging.Recordset.EOF
            If Not (Validate_Timecode(.adoLogging.Recordset("timecodestart"), m_Framerate, True)) Or Not (Validate_Timecode(.adoLogging.Recordset("timecodestop"), m_Framerate, True)) Then
                MsgBox "Problem with timecode logging on '" & .adoLogging.Recordset("segmentreference") & "'", vbCritical, "Cannot complete XML"
                MakeDiscoveryXML = False
                Exit Function
            End If
            .adoLogging.Recordset.MoveNext
        Loop
        
        'Then find all the program items and write them out.
        l_lngSegmentCount = 0
        .adoLogging.Recordset.MoveFirst
        Do While Not .adoLogging.Recordset.EOF
            If .adoLogging.Recordset("segmentreference") = "Programme" Then
                l_lngSegmentCount = l_lngSegmentCount + 1
            End If
            .adoLogging.Recordset.MoveNext
        Loop
        WriteTabs 1
        Print #1, "</AudioTracks>"
        
        Print #1, "<Segments Count=""" & l_lngSegmentCount & """>"
        .adoLogging.Recordset.MoveFirst
        l_lngSegmentCount = 0
        Do While Not .adoLogging.Recordset.EOF
            If .adoLogging.Recordset("segmentreference") = "Programme" Then
                WriteTabs 2
                Print #1, "<Segment" & l_lngSegmentCount + 1 & " StartTime=""" & .adoLogging.Recordset("timecodestart") & """ EndTime=""" & .adoLogging.Recordset("timecodestop") & """> </Segment" & l_lngSegmentCount + 1 & ">"
                l_lngSegmentCount = l_lngSegmentCount + 1
            End If
            .adoLogging.Recordset.MoveNext
        Loop
        'Them write the Credits item
        .adoLogging.Recordset.MoveLast
        Do While Not .adoLogging.Recordset.BOF
            If .adoLogging.Recordset("segmentreference") = "Programme" Then
                WriteTabs 2
                Print #1, "<Credits StartTime=""" & .txtEndCredits.Text & """ EndTime=""" & .adoLogging.Recordset("timecodestop") & """ VOCAllowed=""Yes""> </Credits>"
                Exit Do
            End If
            .adoLogging.Recordset.MovePrevious
        Loop
        'Then write the slate item
        .adoLogging.Recordset.MoveFirst
        Do While Not .adoLogging.Recordset.EOF
            If .adoLogging.Recordset("segmentreference") = "Slate" Or .adoLogging.Recordset("segmentreference") = "Clock" Then
                WriteTabs 2
                Print #1, "<Slate StartTime=""" & .adoLogging.Recordset("timecodestart") & """ EndTime=""" & .adoLogging.Recordset("timecodestop") & """> </Slate>"
                l_lngSegmentCount = l_lngSegmentCount + 1
            End If
            .adoLogging.Recordset.MoveNext
        Loop
        WriteTabs 1
        Print #1, "</Segments>"
        
        Print #1, "</MediaAsset>"
            
    End With

    Close #1

    MakeDiscoveryXML = True
    
End If
MsgBox "Done!"

End Function

Sub MakeESIDL3XML()

If Val(txtClipID.Text) = 0 Then Exit Sub

If txtSeriesID.Text = "" Or txtSerialNumber.Text = "" Or cmbIsTextInPicture.Text = "" Or txtMD5Checksum.Text = "" Or cmbAudioType1.Text = "" Then
    MsgBox "Essential Information has not been completed for this asset." & vbCrLf & _
    "You need SeriesID and Serial Number, MD5, TextInPicture, Audio Logging and Progream Logging done for ESI DL3" & vbCrLf & "XML Not Made"
    Exit Sub
End If

If GetCount("SELECT count(eventloggingID) FROM eventlogging WHERE segmentreference = 'Programme' AND eventID = " & txtClipID.Text) <> 1 Then
    MsgBox "Could not find logging entry for Programme, or there were multiple Programme items logged" & vbCrLf & "XML not Made"
    Exit Sub
End If

Dim l_strFileNameToSave As String, l_strAudioLanguageString As String

MDIForm1.dlgMain.InitDir = GetData("library", "subtitle", "libraryID", lblLibraryID.Caption) & "\" & txtAltFolder.Text
MDIForm1.dlgMain.Filter = "XML files|*.xml"
MDIForm1.dlgMain.Filename = txtReference.Text & ".xml"

MDIForm1.dlgMain.ShowSave

l_strFileNameToSave = MDIForm1.dlgMain.Filename

If l_strFileNameToSave <> "" And Right(l_strFileNameToSave, 5) <> "*.xml" Then
    
    Open l_strFileNameToSave For Output As 1
    
    Print #1, "<?xml version=""1.0"" encoding=""utf-8""?>"
    Print #1, "<video:TechnicalMetadata  xmlns:audioType=""http://www.bydeluxe.com/esi-schema/audioConfigMapTypes/v1.3"" xmlns:video=""http://www.bydeluxe.com/esi-schema/video/archive-transfer/v1.2"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">"

    Print #1, Chr(9) & "<video:AssetUpdate>"
    Print #1, Chr(9) & Chr(9) & "<video:SerialNumber>" & txtSerialNumber.Text & "</video:SerialNumber>"
    Print #1, Chr(9) & Chr(9) & "<video:TitleCode>" & txtSeriesID.Text & "</video:TitleCode>"
    Print #1, Chr(9) & Chr(9) & "<video:ProgrammeIn>" & GetDataSQL("SELECT TOP 1 timecodestart FROM eventlogging WHERE eventID = " & txtClipID.Text & " AND segmentreference = 'Programme'") & "</video:ProgrammeIn>"
    Print #1, Chr(9) & Chr(9) & "<video:ProgrammeOut>" & GetDataSQL("SELECT TOP 1 timecodestop FROM eventlogging WHERE eventID = " & txtClipID.Text & " AND segmentreference = 'Programme'") & "</video:ProgrammeOut>"
    Print #1, Chr(9) & Chr(9) & "<video:Language>" & cmbLanguage.Text & "</video:Language>"
    Print #1, Chr(9) & Chr(9) & "<video:PictureAspectRatio>";
    Select Case cmbAspect.Text
        Case "4:3"
            Print #1, "1.33";
        Case "16:9"
            Print #1, "1.78";
        Case Else
            Print #1, cmbAspect.Text
    End Select
    Print #1, "</video:PictureAspectRatio>"
    Print #1, Chr(9) & Chr(9) & "<video:TextedTextless>";
    If cmbIsTextInPicture.Text = "Texted" Then
        Print #1, "Texted";
        If GetCount("SELECT count(eventloggingID) FROM eventlogging WHERE eventID = " & txtClipID.Text & " AND segmentreference = 'Textless'") = 1 Then
            Print #1, " with Textless Bed";
        Else
            Print #1, " Without Textless Bed";
        End If
    ElseIf cmbIsTextInPicture.Text = "Textless" Then
        Print #1, "Textless";
    Else
        MsgBox "Cannot Handle Dual Purpose texted for ESI" & vbCrLf & "XML not finished"
        Exit Sub
    End If
    Print #1, "</video:TextedTextless>"
    Print #1, Chr(9) & Chr(9) & "<video:Checksum>" & txtMD5Checksum.Text & "</video:Checksum>"
    Print #1, Chr(9) & Chr(9) & "<video:AudioConfigMap>"
    If cmbAudioContent1.Text = "Composite" Then
        l_strAudioLanguageString = IIf(cmbAudioLanguage1.Text = "English", "English (USA)", cmbAudioLanguage1.Text)
    ElseIf cmbAudioContent1.Text = "M&E" Then
        l_strAudioLanguageString = "Music &amp; Effects"
    ElseIf cmbAudioContent1.Text = "Partial M&E" Then
        l_strAudioLanguageString = "Mix Minus Narration"
    ElseIf cmbAudioContent1.Text = "Music" Then
        l_strAudioLanguageString = "Music"
    Else
        l_strAudioLanguageString = ""
    End If
    Select Case cmbAudioType1.Text
        Case "5.1"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Center</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Low Frequency</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
        Case "Standard Stereo"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "Mono"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_1.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>1.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_1.0>"
        Case "Mono 2.0"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "LT/RT"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
    End Select
    If cmbAudioContent2.Text = "Composite" Then
        l_strAudioLanguageString = IIf(cmbAudioLanguage2.Text = "English", "English (USA)", cmbAudioLanguage2.Text)
    ElseIf cmbAudioContent2.Text = "M&E" Then
        l_strAudioLanguageString = "Music &amp; Effects"
    ElseIf cmbAudioContent2.Text = "Partial M&E" Then
        l_strAudioLanguageString = "Mix Minus Narration"
    ElseIf cmbAudioContent2.Text = "Music" Then
        l_strAudioLanguageString = "Music"
    Else
        l_strAudioLanguageString = ""
    End If
    Select Case cmbAudioType2.Text
        Case "5.1"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Center</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Low Frequency</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
        Case "Standard Stereo"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "Mono"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_1.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>1.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_1.0>"
        Case "Mono 2.0"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "LT/RT"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
    End Select
    If cmbAudioContent3.Text = "Composite" Then
        l_strAudioLanguageString = IIf(cmbAudioLanguage3.Text = "English", "English (USA)", cmbAudioLanguage3.Text)
    ElseIf cmbAudioContent3.Text = "M&E" Then
        l_strAudioLanguageString = "Music &amp; Effects"
    ElseIf cmbAudioContent3.Text = "Partial M&E" Then
        l_strAudioLanguageString = "Mix Minus Narration"
    ElseIf cmbAudioContent3.Text = "Music" Then
        l_strAudioLanguageString = "Music"
    Else
        l_strAudioLanguageString = ""
    End If
    Select Case cmbAudioType3.Text
        Case "5.1"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Center</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Low Frequency</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
        Case "Standard Stereo"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "Mono"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_1.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>1.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_1.0>"
        Case "Mono 2.0"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "LT/RT"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
    End Select
    If cmbAudioContent4.Text = "Composite" Then
        l_strAudioLanguageString = IIf(cmbAudioLanguage4.Text = "English", "English (USA)", cmbAudioLanguage4.Text)
    ElseIf cmbAudioContent4.Text = "M&E" Then
        l_strAudioLanguageString = "Music &amp; Effects"
    ElseIf cmbAudioContent4.Text = "Partial M&E" Then
        l_strAudioLanguageString = "Mix Minus Narration"
    ElseIf cmbAudioContent4.Text = "Music" Then
        l_strAudioLanguageString = "Music"
    Else
        l_strAudioLanguageString = ""
    End If
    Select Case cmbAudioType4.Text
        Case "5.1"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Front Center</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Low Frequency</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_5.1>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>5.1</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Surround</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_5.1>"
        Case "Standard Stereo"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Left</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Stereo Right</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "Mono"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_1.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>1.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_1.0>"
        Case "Mono 2.0"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>2 ch. Mono</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
        Case "LT/RT"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Left Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfigMap_2.0>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioConfig>2.0</audioType:AudioConfig>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AudioChannelMap>Right Total</audioType:AudioChannelMap>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:Language>" & l_strAudioLanguageString & "</audioType:Language>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<audioType:AlternateContentType></audioType:AlternateContentType>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioType:AudioConfigMap_2.0>"
    End Select
    If cmbAudioContent5.Text <> "" Or cmbAudioContent6.Text <> "" Or cmbAudioContent7.Text <> "" Or cmbAudioContent8.Text <> "" Then
        MsgBox "Cannot handle more than 4 audio sets for ESI" & vbCrLf & "XML not completed"
        Exit Sub
    End If
    Print #1, Chr(9) & Chr(9) & "</video:AudioConfigMap>"
    Print #1, Chr(9); "</video:AssetUpdate>"
    Print #1, "</video:TechnicalMetadata>"
    
    Close #1

End If

MsgBox "Done!", vbInformation

End Sub
