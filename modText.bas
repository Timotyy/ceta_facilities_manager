Attribute VB_Name = "modText"
Option Explicit
Function StringAddress(lp_lngJobID As Long, lp_strAddressToGet As String) As String

Dim l_strSQL As String
l_strSQL = "SELECT " & lp_strAddressToGet & " FROM job WHERE jobID = '" & lp_lngJobID & "';"

Dim l_con As New ADODB.Connection
l_con.Open g_strConnection

Dim l_rst As New ADODB.Recordset
l_rst.Open l_strSQL, l_con, adOpenForwardOnly, adLockReadOnly

Dim l_strText As String

If Not l_rst.EOF Then l_strText = Replace(l_rst(0), Chr(13), "<br />")

l_rst.Close
Set l_rst = Nothing

l_con.Close
Set l_con = Nothing

StringAddress = l_strText

End Function

Function StringCostCodes(lp_lngJobID As Long) As String

Dim l_strSQL As String
l_strSQL = "SELECT costcode FROM costing WHERE jobID = '" & lp_lngJobID & "';"

Dim l_con As New ADODB.Connection
l_con.Open g_strConnection

Dim l_rst As New ADODB.Recordset
l_rst.Open l_strSQL, l_con, adOpenForwardOnly, adLockReadOnly

Dim l_strText    As String

Do While Not l_rst.EOF
    l_strText = l_strText & " " & l_rst("costcode")
    l_rst.MoveNext
Loop

l_rst.Close
Set l_rst = Nothing

l_con.Close
Set l_con = Nothing

StringCostCodes = l_strText

End Function
Function StringEquipmentList(ByVal lp_lngJobID As Long) As String

Dim l_strSQL As String
l_strSQL = "SELECT * FROM resourceschedule WHERE jobID = '" & lp_lngJobID & "' ORDER BY role, resourcename;"

Dim l_con As New ADODB.Connection
l_con.Open g_strConnection

Dim l_rst As New ADODB.Recordset
l_rst.Open l_strSQL, l_con, adOpenForwardOnly, adLockReadOnly

Dim l_strHTML As String
Dim l_strFontCode As String

l_strFontCode = "<FONT FACE='Arial, Helvetica, Tahoma' SIZE=1>"
                                      
l_strHTML = "<TABLE cellSpacing='0' cellPadding='0' border='1' WIDTH=100% BORDERCOLOR='#006633'>"

l_strHTML = l_strHTML & "<TR>"
l_strHTML = l_strHTML & "<TD>"
l_strHTML = l_strHTML & l_strFontCode & "<b>Category</b>" & "</font>"
l_strHTML = l_strHTML & "</TD>"

l_strHTML = l_strHTML & "<TD>"
l_strHTML = l_strHTML & l_strFontCode & "<b>Resource</b>" & "</font>"
l_strHTML = l_strHTML & "</TD>"

l_strHTML = l_strHTML & "<TD>"
l_strHTML = l_strHTML & l_strFontCode & "<b>Serial Number</b>" & "</font>"
l_strHTML = l_strHTML & "</TD>"

l_strHTML = l_strHTML & "</TR>"

Do While Not l_rst.EOF
    l_strHTML = l_strHTML & "<TR>"
    l_strHTML = l_strHTML & "<TD>"
    l_strHTML = l_strHTML & l_strFontCode & l_rst("role") & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "<TD>"
    l_strHTML = l_strHTML & l_strFontCode & l_rst("resourcename") & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "<TD>"
    l_strHTML = l_strHTML & l_strFontCode & l_rst("serialnumber") & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "</TR>"
    l_rst.MoveNext
Loop

l_strHTML = l_strHTML & "</TABLE>"

l_rst.Close
Set l_rst = Nothing

l_con.Close
Set l_con = Nothing

StringEquipmentList = l_strHTML

End Function

Function StringCostings(ByVal lp_lngJobID As Long) As String

    Dim l_strSQL As String
    l_strSQL = "SELECT SUM(quantity) AS 'qty', description, ratecardprice, unit, SUM(ratecardtotal) as 'totalratecardtotal', fd_time FROM costing WHERE jobID = '" & lp_lngJobID & "' GROUP BY description, ratecardprice, unit, ratecardtotal, fd_time ORDER BY totalratecardtotal DESC;"
    
    Dim l_con As New ADODB.Connection
    l_con.Open g_strConnection
    
    Dim l_rst As New ADODB.Recordset
    l_rst.Open l_strSQL, l_con, adOpenForwardOnly, adLockReadOnly
    
    Dim l_strHTML As String
    Dim l_strFontHTML As String
    
    l_strFontHTML = "<FONT FACE='Arial, Helvetica, Tahoma' SIZE=1>"
    
    l_strHTML = "<TABLE cellSpacing='0' cellPadding='2' BORDER=0 WIDTH=100%>"
    l_strHTML = l_strHTML & "<TR>"
    l_strHTML = l_strHTML & "<TD ALIGN='LEFT' VALIGN='TOP'>"
    l_strHTML = l_strHTML & l_strFontHTML & "<b>Qty</b>" & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "<TD WIDTH=270 ALIGN='LEFT' VALIGN='TOP'>"
    l_strHTML = l_strHTML & l_strFontHTML & "<b>Description</b>" & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "<TD ALIGN='CENTER' VALIGN='TOP'>"
    l_strHTML = l_strHTML & l_strFontHTML & "<b>Units</b>" & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "<TD align='right' valign='TOP'>"
    l_strHTML = l_strHTML & l_strFontHTML & "<b>Rate Card</b>" & "</font>"
    l_strHTML = l_strHTML & "</TD>"
    l_strHTML = l_strHTML & "<TD align='right' valign='Top'>"
    l_strHTML = l_strHTML & l_strFontHTML & "<b>Total</b>" & "</font>"
    l_strHTML = l_strHTML & "</TD>"
                                                             
    l_strHTML = l_strHTML & "</TR>"
    
    Do While Not l_rst.EOF
        l_strHTML = l_strHTML & "<TR>"
        l_strHTML = l_strHTML & "<TD ALIGN='LEFT' VALIGN='TOP'>"
        l_strHTML = l_strHTML & l_strFontHTML & l_rst("qty") & "</font>"
        l_strHTML = l_strHTML & "</TD>"
        l_strHTML = l_strHTML & "<TD WIDTH=270 ALIGN='LEFT' VALIGN='TOP'>"
        l_strHTML = l_strHTML & l_strFontHTML & l_rst("description") & "</font>"
        l_strHTML = l_strHTML & "</TD>"
        l_strHTML = l_strHTML & "<TD ALIGN='CENTER' VALIGN='TOP'>"
        l_strHTML = l_strHTML & l_strFontHTML & l_rst("fd_time") & " " & l_rst("unit") & "</font>"
        l_strHTML = l_strHTML & "</TD>"
        l_strHTML = l_strHTML & "<TD ALIGN='RIGHT' VALIGN='TOP'>"
        l_strHTML = l_strHTML & l_strFontHTML & Format(l_rst("ratecardprice"), "CURRENCY") & "</font>"
        l_strHTML = l_strHTML & "</TD>"
        l_strHTML = l_strHTML & "<TD ALIGN='RIGHT' VALIGN='TOP'>"
        l_strHTML = l_strHTML & l_strFontHTML & Format(l_rst("totalratecardtotal"), "CURRENCY") & "</font>"
        l_strHTML = l_strHTML & "</TD>"
        l_strHTML = l_strHTML & "</TR>"
        l_rst.MoveNext
    Loop
    
    l_strHTML = l_strHTML & "</TABLE>"
    
    l_rst.Close
    Set l_rst = Nothing
    
    l_con.Close
    Set l_con = Nothing
    
    StringCostings = l_strHTML

End Function
'Sub SendConfirmEmailTo(ByVal lp_strEmailAddress As String, lp_lngJobID As Long)
'
'On Error GoTo SendConfirmEmail_Error
'
'open the text and import as email.
'
'Dim l_strHTMLTemplate As String
'l_strHTMLTemplate = GetDataSQL("SELECT descriptionalias FROM xref WHERE category = 'jobtype' AND description = '" & QuoteSanitise(GetData("job", "jobtype", "jobID", lp_lngJobID)) & "';")
'CheckForSQLError
'
'If l_strHTMLTemplate = "" Then Exit Sub
'
'Open App.Path & "\templates\" & l_strHTMLTemplate For Input As #1
'
'Dim l_strMainHTMLBody As String, Char As String
'
'Do While Not EOF(1)
'    Char = Input(1, #1)
'    l_strMainHTMLBody = l_strMainHTMLBody & Char
'Loop
'
'Close #1
'
'Dim l_CollectionAddress$, l_DeliveryAddress$, l_type$, l_Title$, l_CostCodes$
'
'Dim l_strSQL As String
'Dim l_con As New ADODB.Connection
'Dim l_rst As New ADODB.Recordset
'
'l_strSQL = "SELECT * FROM job WHERE jobID = '" & lp_lngJobID & "';"
'
'l_con.Open g_strConnection
'l_rst.Open l_strSQL, l_con, adOpenForwardOnly, adLockReadOnly
'
'l_type$ = l_rst("jobtype")
'
'l_DeliveryAddress = GetData("job", "deliverycompanyname", "jobID", lp_lngJobID) & "<br>" & GetData("job", "deliverycontactname", "jobID", lp_lngJobID) & "<br>" & StringAddress(lp_lngJobID, "deliveryaddress")
'l_CollectionAddress = GetData("job", "collectioncompanyname", "jobID", lp_lngJobID) & "<br>" & GetData("job", "collectioncontactname", "jobID", lp_lngJobID) & "<br>" & StringAddress(lp_lngJobID, "collectionaddress")
'
'l_CostCodes$ = StringCostCodes(lp_lngJobID)
'
'Dim l_DelColNoCharge$
'
'If InStr(l_CostCodes$, "DEL") = 0 Then
'    If InStr(l_CostCodes$, "COL") = 0 Then
'        l_DelColNoCharge$ = "Delivery and Collection Not Charged"
'    Else
'        l_DelColNoCharge$ = "Delivery Not Charged"
'    End If
'Else
'    If InStr(l_CostCodes$, "COL") = 0 Then
'        l_DelColNoCharge$ = "Collection Not Charged"
'    Else
'        l_DelColNoCharge$ = ""
'    End If
'End If
'
'If l_DeliveryAddress$ = l_CollectionAddress$ Then
'    l_CollectionAddress$ = "Same as delivery address"
'End If
'
'Dim l_EquipmentList$, l_CostingsList$
'l_EquipmentList$ = StringEquipmentList(lp_lngJobID)
'l_Address$ = StringClientAddress(CLng(l_dynBookings("client number")))
'l_CostingsList$ = StringCostings(lp_lngJobID)
'
'If Not IsNull(l_dynBookings("stock type")) Then
'    l_Stock$ = l_dynBookings("stock quantity") & " x " & l_dynBookings("stock type") & " @ �" & l_dynBookings("stock price") & " each"
'Else
'    l_Stock$ = "No stock used"
'End If
'
'Dim l_JobName$
'l_JobName$ = l_rst("title1")
'
'If l_JobName$ = "" Or UCase(l_JobName$) = "TBA" Then
'    l_JobName$ = "Not specified - Please enter an order number"
'End If
'
'Dim l_dblTotalRateCardPrice As Double
'l_dblTotalRateCardPrice = GetTotalCostsForJob(lp_lngJobID, "ratecardtotal", "invoice")
'
'Dim l_dblTotalPrice As Double
'l_dblTotalPrice = GetTotalCostsForJob(lp_lngJobID, "total", "invoice")
'
'Dim l_dblTotalDiscount As Double
'l_dblTotalDiscount = l_dblTotalRateCardPrice - l_dblTotalPrice
'
'If lp_strEmailAddress = "" Then
'    lp_strEmailAddress = l_rst("email")
'End If
'
'Dim l_Attachment$, l_Insurance$, l_Subject$
'l_Attachment$ = ""
'l_Insurance$ = "Clients Own"
'
'fill in all the fields...
'If InStr(UCase(l_type$), "PENCIL") = 0 Then
'    l_Subject$ = "Your booking has been created"
'    l_Title$ = "Booking Confirmation"
'Else
'    l_Subject$ = "Your pencil booking has been created"
'    l_Title$ = "Provisional Booking"
'End If
'
'Dim l_HirePeriod$, l_Body$, l_Address$, l_Stock$, l_UKUse$
'l_HirePeriod$ = "From " & Format(l_rst("startdate"), "ddd, dd/mm/yyyy hh:nn") & " to " & Format(l_rst("enddate"), "ddd, dd/mm/yyyy hh:nn")
'
'l_Body$ = l_strMainHTMLBody
'
'If Not l_rst.EOF Then
'
'    l_Body$ = Replace(l_Body$, "{ProjectNumber}", l_rst("projectnumber"))
'    l_Body$ = Replace(l_Body$, "{BookedBy}", l_rst("createduser"))
'    l_Body$ = Replace(l_Body$, "{Hirer}", l_rst("companyname"))
'    l_Body$ = Replace(l_Body$, "{HirersAddress}", l_Address$)
'    l_Body$ = Replace(l_Body$, "{HirersPurchaseOrderNumber}", l_JobName$)
'    l_Body$ = Replace(l_Body$, "{DeliveryAddress}", l_DeliveryAddress$)
'    l_Body$ = Replace(l_Body$, "{CollectionAddress}", l_CollectionAddress$)
'    l_Body$ = Replace(l_Body$, "{EquipmentList}", l_EquipmentList$)
'    l_Body$ = Replace(l_Body$, "{Stock}", l_Stock$)
'    l_Body$ = Replace(l_Body$, "{SpecialInstructions}", Trim(" " & l_rst("notes1")))
'    l_Body$ = Replace(l_Body$, "{TotalRateCardPrice}", Format(l_dblTotalRateCardPrice, "#,#,0.00"))
'    l_Body$ = Replace(l_Body$, "{TotalDiscountAmount}", Format(l_dblTotalDiscount, "#,#,0.00"))
'    l_Body$ = Replace(l_Body$, "{HireCharge}", Format(l_dblTotalPrice, "#,#,0.00"))
'    l_Body$ = Replace(l_Body$, "{HirePeriod}", l_HirePeriod$)
'    l_Body$ = Replace(l_Body$, "{DeliveryDate}", Format(l_rst("startdate"), "dddd, dd/mm/yyyy hh:nn") & " or before.")
'    l_Body$ = Replace(l_Body$, "{UKUse}", l_UKUse$)
'    l_Body$ = Replace(l_Body$, "{Hirer}", l_rst("companyname"))
'    l_Body$ = Replace(l_Body$, "{Insurance}", l_Insurance$)
'    l_Body$ = Replace(l_Body$, "{Contact}", l_rst("contactname"))
'    l_Body$ = Replace(l_Body$, "{CostingList}", l_CostingsList$)
'    l_Body$ = Replace(l_Body$, "{Title}", l_Title$)
'    l_Body$ = Replace(l_Body$, "{DeliveryCollectionNoCharge}", l_DelColNoCharge$)
'
'End If
'
'l_rst.Close
'Set l_rst = Nothing
'
'SendOutlookEmail lp_strEmailAddress, l_Title$, l_Body$, ""
'
'Exit Sub
'
'SendConfirmEmail_Error:
'    MsgBox Error$, 48
'    Resume 'Next
'    Exit Sub
'End Sub

