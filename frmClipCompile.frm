VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmClipCompile 
   Caption         =   "Compile Clips"
   ClientHeight    =   4170
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11835
   LinkTopic       =   "Excel Orders"
   ScaleHeight     =   4170
   ScaleWidth      =   11835
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   675
      Left            =   8460
      TabIndex        =   15
      Top             =   2580
      Width           =   2355
      Begin VB.OptionButton optHidden 
         Caption         =   "Hidden"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   17
         Top             =   240
         Width           =   1035
      End
      Begin VB.OptionButton optHidden 
         Caption         =   "Visible"
         Height          =   255
         Index           =   1
         Left            =   1380
         TabIndex        =   16
         Top             =   240
         Width           =   795
      End
   End
   Begin VB.CheckBox chkOverWrite 
      Caption         =   "Overwrite if already exists"
      Height          =   255
      Left            =   8640
      TabIndex        =   14
      Top             =   2280
      Width           =   2295
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "Submit Compilation"
      Height          =   315
      Left            =   8580
      TabIndex        =   13
      Top             =   3660
      Width           =   1635
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbTranscodeSpec 
      Bindings        =   "frmClipCompile.frx":0000
      Height          =   315
      Left            =   1800
      TabIndex        =   12
      Top             =   180
      Width           =   6555
      DataFieldList   =   "transcodename"
      ListWidth       =   11033
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Transcode Spec ID"
      Columns(0).Name =   "transcodespecID"
      Columns(0).DataField=   "transcodespecID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   9313
      Columns(1).Caption=   "Transcode Name"
      Columns(1).Name =   "transcodename"
      Columns(1).DataField=   "transcodename"
      Columns(1).FieldLen=   256
      _ExtentX        =   11562
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "transcodename"
   End
   Begin MSAdodcLib.Adodc adoTranscodeSpecs 
      Height          =   330
      Left            =   8520
      Top             =   60
      Visible         =   0   'False
      Width           =   2385
      _ExtentX        =   4207
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoTranscodeSpecs"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Frame frmTimecode 
      Caption         =   "Select Timecode Type"
      Height          =   1515
      Left            =   8460
      TabIndex        =   6
      Top             =   720
      Width           =   3075
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "24 fps"
         Height          =   255
         Index           =   3
         Left            =   180
         TabIndex        =   11
         Top             =   300
         Width           =   1995
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "30 fps"
         Height          =   255
         Index           =   2
         Left            =   180
         TabIndex        =   10
         Top             =   1200
         Width           =   1995
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "29.97 fps"
         Height          =   255
         Index           =   1
         Left            =   180
         TabIndex        =   8
         Top             =   900
         Width           =   1995
      End
      Begin VB.OptionButton optTimecodeType 
         Caption         =   "25 fps"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   7
         Top             =   600
         Value           =   -1  'True
         Width           =   1995
      End
   End
   Begin MSAdodcLib.Adodc adoSegment 
      Height          =   330
      Left            =   2400
      Top             =   660
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoSegment"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdSegments 
      Bindings        =   "frmClipCompile.frx":0020
      Height          =   3255
      Left            =   240
      TabIndex        =   5
      Top             =   720
      Width           =   8115
      _Version        =   196617
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "eventID"
      Columns(0).Name =   "eventID"
      Columns(0).DataField=   "eventID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   2937
      Columns(1).Caption=   "Source Clip ID"
      Columns(1).Name =   "segmentreference"
      Columns(1).DataField=   "sourceclipid"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Start"
      Columns(2).Name =   "timecodestart"
      Columns(2).DataField=   "timecodestart"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3201
      Columns(3).Caption=   "End"
      Columns(3).Name =   "timecodestop"
      Columns(3).DataField=   "timecodestop"
      Columns(3).FieldLen=   256
      Columns(4).Width=   1270
      Columns(4).Caption=   "Order"
      Columns(4).Name =   "forder"
      Columns(4).DataField=   "forder"
      Columns(4).FieldLen=   256
      _ExtentX        =   14314
      _ExtentY        =   5741
      _StockProps     =   79
      Caption         =   "Segments"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton cmdWatchFolder 
      Caption         =   "Select"
      Height          =   315
      Left            =   7260
      TabIndex        =   3
      Top             =   4260
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txtWatchFolder 
      Height          =   315
      Left            =   8040
      TabIndex        =   2
      Top             =   4260
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdSubmit 
      Caption         =   "Submit"
      Height          =   315
      Left            =   5820
      TabIndex        =   1
      Top             =   4260
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   10320
      TabIndex        =   0
      Top             =   3660
      Width           =   1335
   End
   Begin VB.Label lblClipID 
      Appearance      =   0  'Flat
      BackColor       =   &H0080FFFF&
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   10080
      TabIndex        =   9
      Top             =   3240
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblCaption 
      Caption         =   "Transcode Spec"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   4
      Top             =   240
      Width           =   1275
   End
End
Attribute VB_Name = "frmClipCompile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdSave_Click()

Dim SQL As String, l_strFPS As Long

If optTimecodeType(0).Value = True Then
    l_strFPS = "25"
ElseIf optTimecodeType(1).Value = True Then
    l_strFPS = "29.97"
ElseIf optTimecodeType(2).Value = True Then
    l_strFPS = "30"
ElseIf optTimecodeType(3).Value = True Then
    l_strFPS = "24"
End If

If cmbTranscodeSpec.Text <> "" Then

    SQL = "INSERT INTO transcoderequest (sourceclipID, compilationneeded, fps, transcodespecID, leavehidden, overwriteifexisting, status, savedate) VALUES ("
    SQL = SQL & lblClipID.Caption & ", 1, "
    SQL = SQL & "'" & l_strFPS & "', "
    SQL = SQL & cmbTranscodeSpec.Columns("transcodespecID").Text & ", "
    If optHidden(0).Value = True Then
        SQL = SQL & "1, "
    Else
        SQL = SQL & "0, "
    End If
    
    If chkOverWrite.Value <> 0 Then
        SQL = SQL & "1, "
    Else
        SQL = SQL & "0, "
    End If
    
    SQL = SQL & GetData("transcodestatus", "transcodestatusID", "description", "Requested") & ", getdate());"
    
    Debug.Print SQL
    ExecuteSQL SQL, g_strExecuteError
    CheckForSQLError
    
End If


End Sub

Private Sub cmdSubmit_Click()

Dim l_lngClipID As Long, l_strFPS  As String

If optTimecodeType(0).Value = True Then
    l_strFPS = "25"
ElseIf optTimecodeType(1).Value = True Then
    l_strFPS = "29.97"
ElseIf optTimecodeType(2).Value = True Then
    l_strFPS = "30"
ElseIf optTimecodeType(3).Value = True Then
    l_strFPS = "24"
End If

If txtWatchFolder.Text <> "" Then

    If adoSegment.Recordset.RecordCount > 0 Then
    
        'Check all clips are in discstores
        
        adoSegment.Recordset.MoveFirst
        
        Do While Not adoSegment.Recordset.EOF
            
            If GetData("library", "format", "libraryID", GetData("events", "libraryID", "eventID", adoSegment.Recordset("sourceclipID"))) <> "DISCSTORE" Then
                MsgBox "Cannot make Compilations from clips that are not in a DISCSTORE", vbCritical, "Error Making Compilation"
                Exit Sub
            End If
            adoSegment.Recordset.MoveNext
        Loop
        
        adoSegment.Recordset.MoveFirst
        
        Open txtWatchFolder.Text & "\" & frmClipControl.txtReference.Text & ".xml" For Output As 1
        
        Print #1, "<?xml version=""1.0"" encoding=""UTF-8"" ?> "
        Print #1, "<transcodeSource>"
        Print #1, Chr(9) & "<clipList>"
        
        Do While Not adoSegment.Recordset.EOF
        
            'Routines for segmenting with AVS
            Print #1, Chr(9) & Chr(9) & "<clip>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<videoSource>"
            If Trim(" " & adoSegment.Recordset("timecodestart")) <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<trim>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<inPoint fps=""" & l_strFPS & """>" & adoSegment.Recordset("timecodestart") & "</inPoint>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<outPoint fps=""" & l_strFPS & """>" & adoSegment.Recordset("timecodestop") & "</outPoint>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</trim>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<mediaFile>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file>" & GetFullPathAndFilename(adoSegment.Recordset("sourceclipID")) & "</file>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</mediaFile>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</videoSource>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "<audioSource>"
            If Trim(" " & adoSegment.Recordset("timecodestart")) <> "" Then
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<trim>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<inPoint fps=""" & l_strFPS & """>" & adoSegment.Recordset("timecodestart") & "</inPoint>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<outPoint fps=""" & l_strFPS & """>" & adoSegment.Recordset("timecodestop") & "</outPoint>"
                Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</trim>"
            End If
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<mediaFile>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & Chr(9) & "<file>" & GetFullPathAndFilename(adoSegment.Recordset("sourceclipID")) & "</file>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & Chr(9) & "</mediaFile>"
            Print #1, Chr(9) & Chr(9) & Chr(9) & "</audioSource>"
            Print #1, Chr(9) & Chr(9) & "</clip>"
            
            adoSegment.Recordset.MoveNext
        Loop
        
        Print #1, Chr(9) & "</clipList>"
        Print #1, "</transcodeSource>"
        
        Close #1

        DoEvents
        
        MsgBox "Ceta Compilation have been created, but the clip" & vbCrLf & "specifications will need to be updated.", vbOKOnly, "Compilation Implemented."
        
    Else
        MsgBox "no clips were listed for compilation", vbInformation
    End If

    Me.Hide

End If

End Sub

Private Sub cmdWatchFolder_Click()

'Use MDIform's common dialog to get the video filename
Dim l_strTemp As String, l_lngCount As Long

MDIForm1.dlgMain.InitDir = g_strLocationOfWatchfolders & "\WatchFolders"
MDIForm1.dlgMain.Filter = "XML Files |*.xml*"
MDIForm1.dlgMain.Filename = g_strLocationOfWatchfolders & "\WatchFolders\" & frmClipControl.txtReference.Text & ".xml"

MDIForm1.dlgMain.ShowOpen

If Left(MDIForm1.dlgMain.Filename, 2) <> "\\" Then
    MsgBox "Please always use UNC network addressing for transcode jobs" & vbCrLf & "rather than drive letters.", vbCritical, "Problem..."
    Exit Sub
End If

l_strTemp = MDIForm1.dlgMain.Filename
l_lngCount = InStr(l_strTemp, MDIForm1.dlgMain.FileTitle)
If l_lngCount > 2 Then
    txtWatchFolder.Text = Left(l_strTemp, l_lngCount - 2)
Else
    txtWatchFolder.Text = ""
End If

End Sub

Private Sub Form_Load()

Me.Icon = frmClipControl.Icon

lblClipID.Caption = Val(frmClipControl.txtClipID.Text)

If Val(lblClipID.Caption) = 0 Then
    Unload Me
    Unload frmClipControl
    Exit Sub
End If

optHidden(0).Value = True

adoSegment.RecordSource = "SELECT * FROM eventcompilation WHERE eventID = " & lblClipID.Caption & " ORDER BY forder;"
adoSegment.ConnectionString = g_strConnection
adoSegment.Refresh

adoTranscodeSpecs.ConnectionString = g_strConnection
adoTranscodeSpecs.RecordSource = "SELECT transcodespecID, transcodename FROM transcodespec WHERE forder >= 1 ORDER by forder, transcodename;"
adoTranscodeSpecs.Refresh

End Sub

Private Sub Form_LostFocus()

Me.Hide

End Sub

Private Sub grdSegments_BeforeUpdate(Cancel As Integer)

grdSegments.Columns("eventID").Text = Val(lblClipID.Caption)

If grdSegments.Columns("forder").Text = "" Then

    Dim l_lngOrderby As Long
    l_lngOrderby = GetCount("SELECT COUNT(sourceclipID) FROM eventcompilation WHERE eventID = '" & lblClipID.Caption & "';")
        
    grdSegments.Columns("forder").Text = l_lngOrderby
End If


End Sub

Private Sub grdSegments_KeyPress(KeyAscii As Integer)

If g_optUseFormattedTimeCodesInEvents = 1 Then
    If grdSegments.Columns(grdSegments.Col).Caption = "Start" Or grdSegments.Columns(grdSegments.Col).Caption = "End" Then
        If Len(grdSegments.ActiveCell.Text) < 2 Then
            If optTimecodeType(1).Value = True Then
                grdSegments.ActiveCell.Text = "00:00:00;00"
            Else
                grdSegments.ActiveCell.Text = "00:00:00:00"
            End If
            grdSegments.ActiveCell.SelStart = 0
            grdSegments.ActiveCell.SelLength = 1
        Else
            If optTimecodeType(0).Value = True Then
                Timecode_Check_Grid grdSegments, KeyAscii, TC_25
            ElseIf optTimecodeType(1).Value = True Then
                Timecode_Check_Grid grdSegments, KeyAscii, TC_29
            ElseIf optTimecodeType(2).Value = True Then
                Timecode_Check_Grid grdSegments, KeyAscii, TC_30
            Else
                Timecode_Check_Grid grdSegments, KeyAscii, TC_24
            End If
        End If
    End If
End If

End Sub

Private Sub timFormClose_Timer()

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> 0 Then Unload Me

End Sub
