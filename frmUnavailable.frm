VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmUnavailable 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Make Resource Unavailable"
   ClientHeight    =   2925
   ClientLeft      =   1275
   ClientTop       =   1170
   ClientWidth     =   4155
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2925
   ScaleWidth      =   4155
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   1620
      TabIndex        =   3
      Top             =   2520
      Width           =   1155
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   315
      Left            =   2880
      TabIndex        =   4
      Top             =   2520
      Width           =   1155
   End
   Begin MSComCtl2.DTPicker datEstimatedRepairDate 
      Height          =   315
      Left            =   1020
      TabIndex        =   2
      Top             =   2100
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   556
      _Version        =   393216
      Enabled         =   0   'False
      Format          =   20578305
      CurrentDate     =   38524
   End
   Begin VB.ComboBox cmbStage 
      Height          =   315
      Left            =   1020
      TabIndex        =   0
      Top             =   120
      Width           =   3015
   End
   Begin VB.TextBox txtReason 
      Height          =   1035
      Left            =   1020
      TabIndex        =   1
      Top             =   540
      Width           =   3015
   End
   Begin MSComCtl2.DTPicker datFromdate 
      Height          =   315
      Left            =   1020
      TabIndex        =   8
      Top             =   1680
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   556
      _Version        =   393216
      Enabled         =   0   'False
      Format          =   20578305
      CurrentDate     =   38524
   End
   Begin VB.Label Label1 
      Caption         =   "From..."
      Enabled         =   0   'False
      ForeColor       =   &H80000011&
      Height          =   435
      Index           =   3
      Left            =   120
      TabIndex        =   9
      Top             =   1680
      Width           =   1035
   End
   Begin VB.Label Label1 
      Caption         =   "Est. Repair Date"
      Enabled         =   0   'False
      ForeColor       =   &H80000011&
      Height          =   435
      Index           =   2
      Left            =   120
      TabIndex        =   7
      Top             =   2100
      Width           =   1035
   End
   Begin VB.Label Label1 
      Caption         =   "Reason"
      Height          =   315
      Index           =   1
      Left            =   120
      TabIndex        =   6
      Top             =   600
      Width           =   1035
   End
   Begin VB.Label Label1 
      Caption         =   "Stage"
      Height          =   315
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1035
   End
End
Attribute VB_Name = "frmUnavailable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()
Me.Tag = "CANCELLED"
Me.Hide

End Sub

Private Sub cmdOK_Click()

If cmbStage.Text = "" Then
    MsgBox "Please select a valid stage", vbExclamation
    Exit Sub
End If

If txtReason.Text = "" Then
    MsgBox "Please enter a valid reason", vbExclamation
    Exit Sub
End If

Me.Hide
End Sub

Private Sub Form_Load()

PopulateCombo "equipmentunavailablestage", cmbStage

datFromdate.Value = Date
datEstimatedRepairDate.Value = DateAdd("m", 1, Date)
CenterForm Me

End Sub
