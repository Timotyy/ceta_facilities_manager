VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmLibraryAudit 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Library Shelf Audit"
   ClientHeight    =   9000
   ClientLeft      =   2715
   ClientTop       =   4740
   ClientWidth     =   14790
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   14790
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox cmbStockManufacturer 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6120
      TabIndex        =   12
      ToolTipText     =   "Who is this despatch for or delivered by"
      Top             =   480
      Width           =   2235
   End
   Begin VB.CommandButton cmdEndAudit 
      Caption         =   "Finish Audit"
      Height          =   315
      Left            =   6120
      TabIndex        =   11
      Top             =   120
      Width           =   2175
   End
   Begin VB.CommandButton cmdStartAudit 
      Caption         =   "Start Audit"
      Height          =   315
      Left            =   3840
      TabIndex        =   10
      Top             =   120
      Width           =   2175
   End
   Begin VB.TextBox txtShelf 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1440
      TabIndex        =   9
      ToolTipText     =   "Scan the Barcodes of the items on the Despatch"
      Top             =   120
      Width           =   2235
   End
   Begin VB.TextBox txtItemCount 
      Alignment       =   2  'Center
      Height          =   345
      Left            =   13500
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   840
      Width           =   1155
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   315
      Left            =   13500
      TabIndex        =   2
      Top             =   60
      Width           =   1155
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdMovements 
      Bindings        =   "frmLibraryAudit.frx":0000
      Height          =   7455
      Left            =   60
      TabIndex        =   5
      Top             =   1260
      Width           =   14535
      _Version        =   196617
      AllowUpdate     =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   12648384
      RowHeight       =   370
      ExtraHeight     =   79
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   25638
      _ExtentY        =   13150
      _StockProps     =   79
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.ComboBox cmbDeliveredBy 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1440
      TabIndex        =   0
      ToolTipText     =   "Who is this despatch for or delivered by"
      Top             =   480
      Width           =   2235
   End
   Begin VB.TextBox txtBarcode 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      ToolTipText     =   "Scan the Barcodes of the items on the Despatch"
      Top             =   840
      Width           =   2235
   End
   Begin MSAdodcLib.Adodc adoMovement 
      Height          =   330
      Left            =   8700
      Top             =   840
      Visible         =   0   'False
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoMovement"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin VB.Label lblCaption 
      Caption         =   "Stock Manufacturer (optional)"
      Height          =   195
      Index           =   1
      Left            =   3840
      TabIndex        =   13
      Top             =   540
      Width           =   2235
   End
   Begin VB.Label lblCaption 
      Caption         =   "Shelf"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   8
      Top             =   180
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Item Count:"
      Height          =   255
      Left            =   12420
      TabIndex        =   7
      Top             =   900
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Taken By"
      Height          =   255
      Index           =   21
      Left            =   120
      TabIndex        =   4
      Top             =   540
      Width           =   1035
   End
   Begin VB.Label lblCaption 
      Caption         =   "Barcode"
      Height          =   195
      Index           =   11
      Left            =   120
      TabIndex        =   3
      Top             =   900
      Width           =   855
   End
End
Attribute VB_Name = "frmLibraryAudit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const m_strFields = "libraryID, barcode, location, shelf as 'Sub Location' FROM library"

Private Sub cmbStockManufacturer_GotFocus()
PopulateCombo "stockmanufacturer", cmbStockManufacturer
End Sub

Private Sub cmdClose_Click()

On Error GoTo ENDSUB
If adoMovement.Recordset.RecordCount > 0 Then
    If MsgBox("There are " & adoMovement.Recordset.RecordCount & " items still not scanned back to this shelf." & vbCrLf & "Do you wish to exit?", vbYesNo, "Exit Audit") = vbNo Then
        Exit Sub
    End If
End If

ENDSUB:
'close the form
Unload Me

End Sub

Private Sub cmdEndAudit_Click()

If adoMovement.Recordset.RecordCount > 0 Then
    If MsgBox("There are " & adoMovement.Recordset.RecordCount & " items that have not been scanned back onto the shelf" & vbCrLf & "Are you finished?", vbYesNo, "Completing Shelf Audit") = vbYes Then
        adoMovement.Recordset.MoveFirst
        Do While Not adoMovement.Recordset.EOF
            SetData "library", "shelf", "libraryID", adoMovement.Recordset("libraryID"), "UNDESIGNATED SHELF"
            adoMovement.Recordset.MoveNext
        Loop
    End If
    adoMovement.Recordset.Requery
End If

txtItemCount.Text = ""
txtShelf.Text = ""
txtBarcode.Text = ""

End Sub

Private Sub cmdStartAudit_Click()

Dim l_strSQL As String, l_rstTemp As ADODB.Recordset

If txtShelf.Text = "" Or cmbDeliveredBy.Text = "" Then
    MsgBox "Please scan a shelf ID, and enter your name", vbCritical, "Cannot Start Audit"
    Exit Sub
End If

l_strSQL = "SELECT " & m_strFields & " WHERE location = 'LIBRARY' AND shelf = '" & txtShelf.Text & "' ORDER BY barcode;"
Set l_rstTemp = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstTemp.RecordCount > 0 Then

    l_rstTemp.MoveFirst
    Do While Not l_rstTemp.EOF
        SetData "library", "shelf", "libraryID", l_rstTemp("libraryID"), "AUDIT-IN-PROGRESS-" & g_strUserInitials
        l_rstTemp.MoveNext
    Loop
End If

l_rstTemp.Close
Set l_rstTemp = Nothing

'refresh the list of records
adoMovement.ConnectionString = g_strConnection
adoMovement.RecordSource = "SELECT " & m_strFields & " WHERE location = 'LIBRARY' AND shelf = 'AUDIT-IN-PROGRESS-" & g_strUserInitials & "' ORDER BY barcode;"
adoMovement.Refresh

txtItemCount.Text = adoMovement.Recordset.RecordCount

End Sub

Private Sub Form_Load()

CenterForm Me

DoEvents

PopulateCombo "users", cmbDeliveredBy

End Sub

Private Sub grdMovements_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

'variable
Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
Dim l_lngMovementID As Long, l_strInsertSQL As String


'store the bookmark count
l_intTotalBookmarks = grdMovements.SelBookmarks.Count


Dim l_intMsg As Integer
l_intMsg = MsgBox("Are you sure you want to delete these " & l_intTotalBookmarks & " movement lines?", vbQuestion + vbYesNo)
If l_intMsg = vbNo Then Exit Sub


'loop through the grid bookmarks
For l_intBookmark = 0 To l_intTotalBookmarks - 1

    'get the bookmark
    l_varCurrentBookmark = grdMovements.SelBookmarks(l_intBookmark)
    
    'pick up the costing ID
    l_lngMovementID = grdMovements.Columns("movementID").CellText(l_varCurrentBookmark)
        
    'build it (its only easy, even a monkey could do it)
    l_strInsertSQL = "DELETE FROM movement WHERE movementID = '" & l_lngMovementID & "';"
        
    'then we need to run it
    ExecuteSQL l_strInsertSQL, g_strExecuteError
    
    CheckForSQLError

Next

Cancel = True

adoMovement.Refresh
grdMovements.Refresh

End Sub

Private Sub grdMovements_RowLoaded(ByVal Bookmark As Variant)
txtItemCount.Text = grdMovements.Rows
End Sub

Private Sub txtBarcode_GotFocus()
HighLite txtBarcode
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyReturn Then
    KeyAscii = 0
    
    Dim l_lngLibraryID  As Long
    Dim l_strBarcode    As String
    Dim l_strLocation   As String
    Dim l_strTakenBy    As String
    Dim l_strShelf      As String
    Dim l_lngBatchNumber As String
    
    
    l_strBarcode = txtBarcode.Text
    l_strLocation = "LIBRARY"
    l_strTakenBy = cmbDeliveredBy.Text
    l_strShelf = txtShelf.Text
    
    'check for a valid user name
    If l_strTakenBy = "" Then
        MsgBox "You must specify a taken by name before scanning a barcode", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If
    
    'check for a valid barcode
    If l_strBarcode <> "" Then
        
        'pick up the library ID
        l_lngLibraryID = GetData("library", "libraryID", "barcode", l_strBarcode)
        
        'if there is a library item for this barcode
        If l_lngLibraryID <> 0 Then
            
            If Not IsBarcodeAlreadyAudited(l_strShelf, l_strBarcode) Then
            
                'move the library item
                SetData "library", "shelf", "libraryID", l_lngLibraryID, txtShelf.Text
                SetData "library", "inlibrary", "libraryID", l_lngLibraryID, "YES"
                SetData "library", "location", "libraryID", l_lngLibraryID, "LIBRARY"
                If cmbStockManufacturer.Text <> "" Then SetData "library", "stockmanufacturer", "libraryID", l_lngLibraryID, cmbStockManufacturer.Text
            
                adoMovement.ConnectionString = g_strConnection
                adoMovement.RecordSource = "SELECT " & m_strFields & " WHERE location = 'LIBRARY' AND shelf = 'AUDIT-IN-PROGRESS-" & g_strUserInitials & "' ORDER BY barcode;"
                adoMovement.Refresh
                
                txtItemCount.Text = adoMovement.Recordset.RecordCount
            Else
                Beep
                MsgBox "This item is a duplicate barcode on this shelf"
            End If
            HighLite txtBarcode

        Else
            
            MsgBox "Could not locate barcode", vbExclamation
            HighLite txtBarcode
            Exit Sub
            
        End If
    
    Else
        MsgBox "You must specify a barcode", vbExclamation
        HighLite txtBarcode
        Exit Sub
    End If
   

End If

End Sub
