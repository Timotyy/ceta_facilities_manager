VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Begin VB.Form frmTrackerSvensk 
   Caption         =   "Svensk Tracker"
   ClientHeight    =   15750
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   28680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   15750
   ScaleWidth      =   28680
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame13 
      Caption         =   "Bulk Management"
      Height          =   1575
      Left            =   21900
      TabIndex        =   248
      Top             =   60
      Visible         =   0   'False
      Width           =   3615
      Begin VB.CommandButton cmdPendingOn 
         Caption         =   "On Pending"
         Height          =   495
         Left            =   420
         TabIndex        =   251
         Top             =   840
         Width           =   1215
      End
      Begin VB.CommandButton cmdPendingOff 
         Caption         =   "Off Pending"
         Height          =   495
         Left            =   1860
         TabIndex        =   250
         Top             =   840
         Width           =   1215
      End
      Begin VB.TextBox Text1 
         Height          =   315
         Left            =   960
         TabIndex        =   249
         Top             =   360
         Width           =   2415
      End
      Begin VB.Label Label10 
         Caption         =   "Comment"
         Height          =   375
         Left            =   180
         TabIndex        =   252
         Top             =   420
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdMakeFixJobsheet 
      Caption         =   "Make Fix Jobsheet"
      Height          =   315
      Left            =   21960
      TabIndex        =   247
      Top             =   1860
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsSummary 
      Caption         =   "Summary Fields"
      Height          =   375
      Left            =   19620
      TabIndex        =   246
      Top             =   1140
      Width           =   2175
   End
   Begin VB.TextBox txtSpecialProject 
      Height          =   315
      Left            =   1620
      TabIndex        =   244
      Top             =   2160
      Width           =   2835
   End
   Begin VB.CommandButton cmdDuplicateLineForResupply 
      Caption         =   "Duplicate for Resupply"
      Height          =   315
      Left            =   19620
      TabIndex        =   243
      Top             =   1860
      Width           =   2175
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Urgent"
      Height          =   255
      Index           =   19
      Left            =   10680
      TabIndex        =   242
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1395
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnCommentTypes 
      Height          =   855
      Left            =   17820
      TabIndex        =   133
      Top             =   13380
      Width           =   2295
      DataFieldList   =   "column 0"
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      ColumnHeaders   =   0   'False
      BackColorOdd    =   16761024
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).Caption=   "value"
      Columns(0).Name =   "value"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      _ExtentX        =   4048
      _ExtentY        =   1508
      _StockProps     =   77
      DataFieldToDisplay=   "column 0"
   End
   Begin VB.PictureBox picValidationSubs 
      Height          =   4755
      Left            =   17040
      ScaleHeight     =   4695
      ScaleWidth      =   4335
      TabIndex        =   192
      Top             =   3240
      Visible         =   0   'False
      Width           =   4395
      Begin VB.CommandButton cmdSubsValidationSave 
         Caption         =   "Save Validation Info"
         Height          =   375
         Left            =   180
         TabIndex        =   221
         Top             =   4080
         Width           =   2055
      End
      Begin VB.Frame Frame46 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2340
         TabIndex        =   218
         Top             =   3540
         Width           =   975
         Begin VB.OptionButton optSubsValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   220
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optSubsValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   219
            Top             =   120
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdCloseSubsValidation 
         Caption         =   "Close"
         Height          =   375
         Left            =   2460
         TabIndex        =   217
         Top             =   4080
         Width           =   1695
      End
      Begin VB.Frame Frame36 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   213
         Top             =   1500
         Width           =   1455
         Begin VB.OptionButton optEnglishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   216
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optEnglishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   215
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optEnglishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   214
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame35 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   209
         Top             =   1860
         Width           =   1455
         Begin VB.OptionButton optNorwegianSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   212
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   211
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   210
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame34 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   205
         Top             =   2220
         Width           =   1455
         Begin VB.OptionButton optSwedishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   208
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optSwedishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   207
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optSwedishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   206
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame33 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   201
         Top             =   2580
         Width           =   1455
         Begin VB.OptionButton optDanishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   204
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optDanishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   203
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optDanishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   202
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame32 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   197
         Top             =   2940
         Width           =   1455
         Begin VB.OptionButton optFinnishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   200
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optFinnishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   199
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optFinnishSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   198
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame24 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   193
         Top             =   3300
         Width           =   1455
         Begin VB.OptionButton optIcelandicSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   196
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   195
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicSubsInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   194
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sr."
         Height          =   255
         Index           =   71
         Left            =   240
         TabIndex        =   238
         Top             =   780
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ep."
         Height          =   255
         Index           =   70
         Left            =   900
         TabIndex        =   237
         Top             =   780
         Width           =   255
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Caption         =   "Master File Validation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   0
         TabIndex        =   236
         Top             =   60
         Width           =   4275
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Yes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   69
         Left            =   2400
         TabIndex        =   235
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   68
         Left            =   2940
         TabIndex        =   234
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Validation Complete?"
         Height          =   255
         Index           =   67
         Left            =   240
         TabIndex        =   233
         Top             =   3660
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   61
         Left            =   3480
         TabIndex        =   232
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label8 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   780
         TabIndex        =   231
         Top             =   420
         Width           =   3555
      End
      Begin VB.Label Label7 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   230
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label6 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1200
         TabIndex        =   229
         Top             =   780
         Width           =   375
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title:"
         Height          =   255
         Index           =   51
         Left            =   240
         TabIndex        =   228
         Top             =   480
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "English Subs In Spec ?"
         Height          =   255
         Index           =   41
         Left            =   240
         TabIndex        =   227
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Norwegian Subs In Spec ?"
         Height          =   255
         Index           =   40
         Left            =   240
         TabIndex        =   226
         Top             =   1860
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Caption         =   "Swedish Subs In Spec ?"
         Height          =   255
         Index           =   39
         Left            =   240
         TabIndex        =   225
         Top             =   2220
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Danish Subs In Spec ?"
         Height          =   255
         Index           =   38
         Left            =   240
         TabIndex        =   224
         Top             =   2580
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Finnish Subs In Spec ?"
         Height          =   255
         Index           =   37
         Left            =   240
         TabIndex        =   223
         Top             =   2940
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Icelandic Subs In Spec ?"
         Height          =   255
         Index           =   36
         Left            =   240
         TabIndex        =   222
         Top             =   3300
         Width           =   1815
      End
   End
   Begin VB.PictureBox picValidationAudioFiles 
      Height          =   4755
      Left            =   12540
      ScaleHeight     =   4695
      ScaleWidth      =   4335
      TabIndex        =   137
      Top             =   3240
      Visible         =   0   'False
      Width           =   4395
      Begin VB.Frame Frame23 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   188
         Top             =   2580
         Width           =   1455
         Begin VB.OptionButton optDanishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   191
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optDanishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   190
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optDanishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   189
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame22 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   184
         Top             =   2940
         Width           =   1455
         Begin VB.OptionButton optFinnishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   187
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optFinnishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   186
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optFinnishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   185
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame18 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   180
         Top             =   3300
         Width           =   1455
         Begin VB.OptionButton optIcelandicInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   183
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   182
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optIcelandicInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   181
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame16 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   162
         Top             =   6840
         Width           =   1455
      End
      Begin VB.Frame Frame17 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   161
         Top             =   6480
         Width           =   1455
      End
      Begin VB.Frame Frame19 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   160
         Top             =   5760
         Width           =   1455
      End
      Begin VB.Frame Frame20 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   159
         Top             =   5400
         Width           =   1455
      End
      Begin VB.Frame Frame21 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   158
         Top             =   5040
         Width           =   1455
      End
      Begin VB.CommandButton cmdCloseAudioValidation 
         Caption         =   "Close"
         Height          =   375
         Left            =   2460
         TabIndex        =   157
         Top             =   4080
         Width           =   1695
      End
      Begin VB.Frame Frame25 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   153
         Top             =   2220
         Width           =   1455
         Begin VB.OptionButton optSwedishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   156
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optSwedishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   155
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optSwedishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   154
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame26 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   149
         Top             =   1860
         Width           =   1455
         Begin VB.OptionButton optNorwegianInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   152
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   151
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optNorwegianInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   150
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame27 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   145
         Top             =   1500
         Width           =   1455
         Begin VB.OptionButton optEnglishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   2
            Left            =   1200
            TabIndex        =   148
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optEnglishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   660
            TabIndex        =   147
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optEnglishInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   146
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame28 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   144
         Top             =   2520
         Width           =   975
      End
      Begin VB.Frame Frame29 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   143
         Top             =   2160
         Width           =   975
      End
      Begin VB.Frame Frame30 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   142
         Top             =   1440
         Width           =   975
      End
      Begin VB.Frame Frame31 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2340
         TabIndex        =   139
         Top             =   3540
         Width           =   975
         Begin VB.OptionButton optAudioValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   141
            Top             =   120
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optAudioValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   140
            Top             =   120
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdAudioValidationSave 
         Caption         =   "Save Validation Info"
         Height          =   375
         Left            =   180
         TabIndex        =   138
         Top             =   4080
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "Icelandic In Spec ?"
         Height          =   255
         Index           =   42
         Left            =   240
         TabIndex        =   179
         Top             =   3300
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title:"
         Height          =   255
         Index           =   43
         Left            =   240
         TabIndex        =   178
         Top             =   480
         Width           =   315
      End
      Begin VB.Label Label2 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1200
         TabIndex        =   177
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label3 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   176
         Top             =   780
         Width           =   375
      End
      Begin VB.Label Label4 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   720
         TabIndex        =   175
         Top             =   480
         Width           =   3555
      End
      Begin VB.Label lblCaption 
         Caption         =   "Finnish In Spec ?"
         Height          =   255
         Index           =   44
         Left            =   240
         TabIndex        =   174
         Top             =   2940
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Danish In Spec ?"
         Height          =   255
         Index           =   45
         Left            =   240
         TabIndex        =   173
         Top             =   2580
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Swedish In Spec ?"
         Height          =   255
         Index           =   46
         Left            =   240
         TabIndex        =   172
         Top             =   2220
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Norwegian In Spec ?"
         Height          =   255
         Index           =   47
         Left            =   240
         TabIndex        =   171
         Top             =   1860
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   48
         Left            =   3480
         TabIndex        =   170
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "English In Spec ?"
         Height          =   255
         Index           =   49
         Left            =   240
         TabIndex        =   169
         Top             =   1500
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Validation Complete?"
         Height          =   255
         Index           =   54
         Left            =   240
         TabIndex        =   168
         Top             =   3660
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   55
         Left            =   2940
         TabIndex        =   167
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Yes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   56
         Left            =   2400
         TabIndex        =   166
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "Audio File(s) Validation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   0
         TabIndex        =   165
         Top             =   60
         Width           =   4275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ep."
         Height          =   255
         Index           =   57
         Left            =   900
         TabIndex        =   164
         Top             =   780
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sr."
         Height          =   255
         Index           =   58
         Left            =   240
         TabIndex        =   163
         Top             =   780
         Width           =   315
      End
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Workable"
      Height          =   255
      Index           =   18
      Left            =   10680
      TabIndex        =   136
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Workable"
      Height          =   255
      Index           =   17
      Left            =   10680
      TabIndex        =   135
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1395
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnProjectManager 
      Bindings        =   "frmTrackerSvensk.frx":0000
      Height          =   1755
      Left            =   1740
      TabIndex        =   134
      Top             =   6120
      Width           =   5775
      DataFieldList   =   "name"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "name"
      Columns(0).DataField=   "name"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "contactID"
      Columns(1).Name =   "contactID"
      Columns(1).DataField=   "contactID"
      Columns(1).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "name"
   End
   Begin VB.ComboBox cmbProjectManager 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   126
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1800
      Width           =   2835
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnChannel 
      Bindings        =   "frmTrackerSvensk.frx":001B
      Height          =   1755
      Left            =   720
      TabIndex        =   124
      Top             =   4080
      Width           =   5775
      DataFieldList   =   "portalcompanyname"
      ListAutoValidate=   0   'False
      MaxDropDownItems=   20
      _Version        =   196617
      ColumnHeaders   =   0   'False
      DefColWidth     =   8819
      ForeColorEven   =   0
      BackColorOdd    =   16761024
      RowHeight       =   423
      ExtraHeight     =   53
      Columns.Count   =   2
      Columns(0).Width=   9710
      Columns(0).Caption=   "Name"
      Columns(0).Name =   "portalcompanyname"
      Columns(0).DataField=   "portalcompanyname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   8819
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   10186
      _ExtentY        =   3096
      _StockProps     =   77
      DataFieldToDisplay=   "portalcompanyname"
   End
   Begin VB.CommandButton cmdFieldsAssets 
      Caption         =   "Assets Fields"
      Height          =   375
      Left            =   17340
      TabIndex        =   115
      Top             =   1140
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsAudio 
      Caption         =   "Audio Fields"
      Height          =   375
      Left            =   15060
      TabIndex        =   110
      Top             =   1140
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsAll 
      Caption         =   "All Fields"
      Height          =   375
      Left            =   10500
      TabIndex        =   109
      Top             =   1140
      Width           =   2175
   End
   Begin VB.CommandButton cmdFieldsVideo 
      Caption         =   "Video Fields"
      Height          =   375
      Left            =   12780
      TabIndex        =   108
      Top             =   1140
      Width           =   2175
   End
   Begin VB.Frame fraReport 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7740
      TabIndex        =   103
      Top             =   1800
      Width           =   4335
      Begin VB.CommandButton cmdReport 
         Caption         =   "Report"
         Height          =   315
         Left            =   60
         TabIndex        =   107
         Top             =   60
         Width           =   915
      End
      Begin VB.OptionButton optReportComments 
         Caption         =   "Last Comm't"
         Height          =   195
         Index           =   0
         Left            =   1080
         TabIndex        =   106
         Top             =   120
         Width           =   1215
      End
      Begin VB.OptionButton optReportComments 
         Caption         =   "1st Comm't"
         Height          =   195
         Index           =   1
         Left            =   2400
         TabIndex        =   105
         Top             =   120
         Width           =   1095
      End
      Begin VB.CommandButton cmdClrComments 
         Caption         =   "Clr"
         Height          =   315
         Left            =   3600
         TabIndex        =   104
         Top             =   60
         Width           =   735
      End
   End
   Begin VB.CommandButton cmdGenerateSFUniqueIDs 
      Caption         =   "GenerateSFUniqueIDs"
      Height          =   495
      Left            =   25320
      TabIndex        =   96
      Top             =   2040
      Visible         =   0   'False
      Width           =   3195
   End
   Begin VB.PictureBox picValidationMasterFiles 
      Height          =   6795
      Left            =   8040
      ScaleHeight     =   6735
      ScaleWidth      =   4335
      TabIndex        =   54
      Top             =   3240
      Visible         =   0   'False
      Width           =   4395
      Begin VB.Frame Frame12 
         BorderStyle     =   0  'None
         Height          =   375
         Left            =   2340
         TabIndex        =   239
         Top             =   5640
         Width           =   975
         Begin VB.OptionButton optMasterValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   241
            Top             =   120
            Width           =   195
         End
         Begin VB.OptionButton optMasterValidationComplete 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   240
            Top             =   120
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame11 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   121
         Top             =   5400
         Width           =   1455
         Begin VB.OptionButton optTextlessSeparator 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   123
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optTextlessSeparator 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   122
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame10 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   117
         Top             =   5040
         Width           =   1455
         Begin VB.OptionButton optBlackAtEnd 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   119
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optBlackAtEnd 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   118
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame9 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   111
         Top             =   4680
         Width           =   1455
         Begin VB.OptionButton optTextlessPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   113
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optTextlessPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   112
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdCloseMasterValidation 
         Caption         =   "Close"
         Height          =   375
         Left            =   2460
         TabIndex        =   95
         Top             =   6180
         Width           =   1695
      End
      Begin VB.Frame Frame8 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   91
         Top             =   4320
         Width           =   1455
         Begin VB.OptionButton optSurroundPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   93
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optSurroundPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   92
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame7 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   86
         Top             =   3960
         Width           =   1455
         Begin VB.OptionButton optMEStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   88
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optMEStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   87
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame6 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   83
         Top             =   3600
         Width           =   1455
         Begin VB.OptionButton optMainStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   85
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optMainStereoPresent 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   84
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame5 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   79
         Top             =   3240
         Width           =   1455
         Begin VB.OptionButton optBarsAndToneInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   81
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optBarsAndToneInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   80
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame4 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   74
         Top             =   2880
         Width           =   1455
         Begin VB.OptionButton optTimecodeInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   76
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optTimecodeInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   75
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   70
         Top             =   2520
         Width           =   975
         Begin VB.OptionButton optClockSlateInfoCorrect 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   72
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optClockSlateInfoCorrect 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   71
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   66
         Top             =   2160
         Width           =   975
         Begin VB.OptionButton optHDFlag 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   68
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
         Begin VB.OptionButton optHDFlag 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   67
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.ComboBox cmbFrameRate 
         Height          =   315
         Left            =   2400
         TabIndex        =   65
         ToolTipText     =   "The Clip Codec"
         Top             =   1740
         Width           =   1395
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   195
         Left            =   2340
         TabIndex        =   61
         Top             =   1440
         Width           =   975
         Begin VB.OptionButton optVideoInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   63
            Top             =   0
            Width           =   195
         End
         Begin VB.OptionButton optVideoInSpec 
            Alignment       =   1  'Right Justify
            Height          =   195
            Index           =   1
            Left            =   660
            TabIndex        =   62
            Top             =   0
            Value           =   -1  'True
            Width           =   195
         End
      End
      Begin VB.CommandButton cmdMasterValidationSave 
         Caption         =   "Save Validation Info"
         Height          =   375
         Left            =   180
         TabIndex        =   56
         Top             =   6180
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "30 sec black befor Textless ?"
         Height          =   255
         Index           =   28
         Left            =   240
         TabIndex        =   120
         Top             =   5400
         Width           =   2175
      End
      Begin VB.Label lblCaption 
         Caption         =   "1 sec Black At End ?"
         Height          =   255
         Index           =   27
         Left            =   240
         TabIndex        =   116
         Top             =   5040
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Textless Present ?"
         Height          =   255
         Index           =   26
         Left            =   240
         TabIndex        =   114
         Top             =   4680
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title:"
         Height          =   255
         Index           =   25
         Left            =   240
         TabIndex        =   102
         Top             =   480
         Width           =   315
      End
      Begin VB.Label lblValidationEpisode 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   1200
         TabIndex        =   99
         Top             =   780
         Width           =   375
      End
      Begin VB.Label lblValidationSeries 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   480
         TabIndex        =   98
         Top             =   780
         Width           =   375
      End
      Begin VB.Label lblValidationTitle 
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   720
         TabIndex        =   97
         Top             =   480
         Width           =   3555
      End
      Begin VB.Label lblCaption 
         Caption         =   "5.1 Present ?"
         Height          =   255
         Index           =   22
         Left            =   240
         TabIndex        =   94
         Top             =   4320
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "M&&E Stereo Present ?"
         Height          =   255
         Index           =   21
         Left            =   240
         TabIndex        =   90
         Top             =   3960
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Main Stereo Present ?"
         Height          =   255
         Index           =   20
         Left            =   240
         TabIndex        =   89
         Top             =   3600
         Width           =   1875
      End
      Begin VB.Label lblCaption 
         Caption         =   "Bars && Tone Correct ?"
         Height          =   255
         Index           =   19
         Left            =   240
         TabIndex        =   82
         Top             =   3240
         Width           =   1995
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "N/A"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   3480
         TabIndex        =   78
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Caption         =   "Timecode Correct ?"
         Height          =   255
         Index           =   17
         Left            =   240
         TabIndex        =   77
         Top             =   2880
         Width           =   1815
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clock / Slate Info Correct?"
         Height          =   255
         Index           =   16
         Left            =   240
         TabIndex        =   73
         Top             =   2520
         Width           =   2055
      End
      Begin VB.Label lblCaption 
         Caption         =   "HD ?"
         Height          =   255
         Index           =   15
         Left            =   240
         TabIndex        =   69
         Top             =   2160
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Frame Rate"
         Height          =   255
         Index           =   14
         Left            =   240
         TabIndex        =   64
         Top             =   1800
         Width           =   1275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Video In Spec ?"
         Height          =   255
         Index           =   12
         Left            =   240
         TabIndex        =   60
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Caption         =   "Validation Complete?"
         Height          =   255
         Index           =   11
         Left            =   240
         TabIndex        =   59
         Top             =   5760
         Width           =   1575
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "No"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   9
         Left            =   2940
         TabIndex        =   58
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label lblCaption 
         Alignment       =   2  'Center
         Caption         =   "Yes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   2400
         TabIndex        =   57
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Master File Validation"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   315
         Left            =   0
         TabIndex        =   55
         Top             =   60
         Width           =   4275
      End
      Begin VB.Label lblCaption 
         Caption         =   "Ep."
         Height          =   255
         Index           =   24
         Left            =   900
         TabIndex        =   101
         Top             =   780
         Width           =   255
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sr."
         Height          =   255
         Index           =   23
         Left            =   240
         TabIndex        =   100
         Top             =   780
         Width           =   315
      End
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Decision Tree"
      Height          =   255
      Index           =   16
      Left            =   8940
      TabIndex        =   53
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1395
   End
   Begin VB.ComboBox cmbRightsOwner 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   46
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1080
      Width           =   2835
   End
   Begin VB.ComboBox cmbProgramType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1620
      TabIndex        =   45
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1440
      Width           =   2835
   End
   Begin VB.ComboBox cmbFormat 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   44
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   720
      Width           =   1815
   End
   Begin VB.ComboBox cmbComponentType 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   6180
      TabIndex        =   43
      ToolTipText     =   "What Copy Type is the tape?"
      Top             =   1080
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Tape Not Here"
      Height          =   255
      Index           =   14
      Left            =   19260
      TabIndex        =   38
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent XML without File"
      Height          =   255
      Index           =   13
      Left            =   17040
      TabIndex        =   37
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent without XML"
      Height          =   255
      Index           =   12
      Left            =   17040
      TabIndex        =   36
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made"
      Height          =   255
      Index           =   5
      Left            =   12660
      TabIndex        =   35
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made"
      Height          =   255
      Index           =   8
      Left            =   14820
      TabIndex        =   34
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Billed"
      Height          =   255
      Index           =   3
      Left            =   8940
      TabIndex        =   33
      Tag             =   "NOCLEAR"
      Top             =   1260
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Pending"
      Height          =   255
      Index           =   1
      Left            =   8940
      TabIndex        =   32
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Sent with XML"
      Height          =   255
      Index           =   11
      Left            =   17040
      TabIndex        =   31
      Tag             =   "NOCLEAR"
      Top             =   60
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Not Made"
      Height          =   255
      Index           =   7
      Left            =   12660
      TabIndex        =   30
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Not Made"
      Height          =   255
      Index           =   10
      Left            =   14820
      TabIndex        =   29
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   1815
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Tape Sent Back"
      Height          =   255
      Index           =   15
      Left            =   19260
      TabIndex        =   28
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1995
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "XML Made but Not Sent"
      Height          =   255
      Index           =   9
      Left            =   14820
      TabIndex        =   27
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   2055
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "File Made but not Sent"
      Height          =   255
      Index           =   6
      Left            =   12660
      TabIndex        =   26
      Tag             =   "NOCLEAR"
      Top             =   360
      Width           =   1995
   End
   Begin VB.CheckBox chkHideDemo 
      Alignment       =   1  'Right Justify
      Caption         =   "Hide Demo"
      Height          =   255
      Left            =   4620
      TabIndex        =   23
      Tag             =   "NOCLEAR"
      Top             =   1860
      Value           =   1  'Checked
      Width           =   1215
   End
   Begin VB.TextBox txtReference 
      Height          =   315
      Left            =   1620
      TabIndex        =   21
      Top             =   0
      Width           =   2835
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   4
      Left            =   19260
      TabIndex        =   19
      Tag             =   "NOCLEAR"
      Top             =   660
      Width           =   2355
   End
   Begin VB.CheckBox chkLockDur 
      Alignment       =   1  'Right Justify
      Caption         =   "Lock Duration"
      Height          =   255
      Left            =   6180
      TabIndex        =   18
      Top             =   1860
      Width           =   1335
   End
   Begin VB.TextBox txtSeries 
      Height          =   315
      Left            =   6180
      TabIndex        =   15
      Top             =   0
      Width           =   1815
   End
   Begin VB.TextBox txtEpisode 
      Height          =   315
      Left            =   6180
      TabIndex        =   14
      Top             =   360
      Width           =   1815
   End
   Begin VB.TextBox txtSubtitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   13
      Top             =   720
      Width           =   2835
   End
   Begin VB.TextBox txtTitle 
      Height          =   315
      Left            =   1620
      TabIndex        =   10
      Top             =   360
      Width           =   2835
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Finished"
      Height          =   255
      Index           =   2
      Left            =   8940
      TabIndex        =   7
      Tag             =   "NOCLEAR"
      Top             =   960
      Width           =   1395
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   8940
      TabIndex        =   6
      Tag             =   "NOCLEAR"
      Top             =   60
      Value           =   -1  'True
      Width           =   1395
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   5940
      TabIndex        =   1
      Top             =   14760
      Width           =   19755
      Begin VB.CommandButton cmdBillItem 
         Caption         =   "Bill Item"
         Height          =   315
         Left            =   10560
         TabIndex        =   52
         Top             =   0
         Visible         =   0   'False
         Width           =   1755
      End
      Begin VB.CommandButton cmdManualBillItem 
         Caption         =   "Manually Mark as Billed"
         Height          =   315
         Left            =   8160
         TabIndex        =   51
         Top             =   0
         Visible         =   0   'False
         Width           =   2235
      End
      Begin VB.CommandButton cmdUnbill 
         Caption         =   "Unmark as Billed"
         Height          =   315
         Left            =   6180
         TabIndex        =   50
         Top             =   0
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdBillAll 
         Caption         =   "Bill All"
         Height          =   315
         Left            =   4320
         TabIndex        =   49
         Top             =   0
         Width           =   1695
      End
      Begin VB.CommandButton cmdUnbillAll 
         Caption         =   "Unmark Billing All"
         Height          =   315
         Left            =   2580
         TabIndex        =   48
         Top             =   0
         Width           =   1575
      End
      Begin VB.CommandButton cmdUpdateAll 
         Caption         =   "Update File Information"
         Height          =   315
         Left            =   12420
         TabIndex        =   25
         Top             =   0
         Width           =   2235
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   18480
         TabIndex        =   5
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   17220
         TabIndex        =   4
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear"
         Height          =   315
         Left            =   15960
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "Print"
         Height          =   315
         Left            =   14700
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   120
      Top             =   2580
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmTrackerSvensk.frx":0036
      Height          =   9315
      Left            =   0
      TabIndex        =   0
      Top             =   2640
      Width           =   28515
      ScrollBars      =   3
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvensk.frx":004D
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmTrackerSvensk.frx":0069
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmTrackerSvensk.frx":0085
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      StyleSet        =   "headerfield"
      RowHeight       =   450
      Columns.Count   =   118
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "tracker_itemID"
      Columns(0).Name =   "tracker_svensk_itemID"
      Columns(0).DataField=   "tracker_svensk_itemID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Filename"
      Columns(2).Name =   "itemfilename"
      Columns(2).DataField=   "itemfilename"
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "GB Sent"
      Columns(3).Name =   "gbsent"
      Columns(3).DataField=   "gbsent"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   2461
      Columns(4).Caption=   "RRsat Client"
      Columns(4).Name =   "channel"
      Columns(4).DataField=   "Column 104"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   873
      Columns(5).Caption=   "Repl"
      Columns(5).Name =   "Resupply"
      Columns(5).DataField=   "Resupply"
      Columns(5).FieldLen=   256
      Columns(5).Style=   2
      Columns(5).StyleSet=   "headerfield"
      Columns(6).Width=   873
      Columns(6).Caption=   "Pend"
      Columns(6).Name =   "rejected"
      Columns(6).DataField=   "rejected"
      Columns(6).FieldLen=   256
      Columns(6).Style=   2
      Columns(6).StyleSet=   "headerfield"
      Columns(7).Width=   3200
      Columns(7).Visible=   0   'False
      Columns(7).Caption=   "Rejected Date"
      Columns(7).Name =   "rejecteddate"
      Columns(7).DataField=   "rejecteddate"
      Columns(7).FieldLen=   256
      Columns(8).Width=   2117
      Columns(8).Caption=   "Rej Date"
      Columns(8).Name =   "Rejected Date"
      Columns(8).DataField=   "RejectedDate"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(9).Width=   2117
      Columns(9).Caption=   "Off Rej Date"
      Columns(9).Name =   "offRejectedDate"
      Columns(9).DataField=   "OffRejectedDate"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(9).Locked=   -1  'True
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "Recent Rej Date"
      Columns(10).Name=   "MostRecentRejectedDate"
      Columns(10).DataField=   "MostRecentRejectedDate"
      Columns(10).DataType=   7
      Columns(10).FieldLen=   256
      Columns(10).Locked=   -1  'True
      Columns(11).Width=   1429
      Columns(11).Caption=   "Days Rej"
      Columns(11).Name=   "DaysonRejected"
      Columns(11).DataField=   "daysonRejected"
      Columns(11).DataType=   10
      Columns(11).FieldLen=   256
      Columns(12).Width=   1111
      Columns(12).Caption=   "D.Tree"
      Columns(12).Name=   "decisiontree"
      Columns(12).DataField=   "decisiontree"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(12).StyleSet=   "headerfield"
      Columns(13).Width=   2117
      Columns(13).Caption=   "DT Date"
      Columns(13).Name=   "decisiontreedate"
      Columns(13).DataField=   "decisiontreedate"
      Columns(13).FieldLen=   256
      Columns(13).Locked=   -1  'True
      Columns(14).Width=   3200
      Columns(14).Visible=   0   'False
      Columns(14).Caption=   "mostrecentdecisiontreedate"
      Columns(14).Name=   "mostrecentdecisiontreedate"
      Columns(14).DataField=   "mostrecentdecisiontreedate"
      Columns(14).FieldLen=   256
      Columns(15).Width=   2117
      Columns(15).Caption=   "Off DT Date"
      Columns(15).Name=   "offdecisiontreedate"
      Columns(15).DataField=   "offdecisiontreedate"
      Columns(15).FieldLen=   256
      Columns(15).Locked=   -1  'True
      Columns(16).Width=   1429
      Columns(16).Caption=   "Days DT"
      Columns(16).Name=   "daysondecisiontree"
      Columns(16).DataField=   "daysondecisiontree"
      Columns(16).FieldLen=   256
      Columns(17).Width=   926
      Columns(17).Caption=   "Fixed"
      Columns(17).Name=   "fixed"
      Columns(17).DataField=   "fixed"
      Columns(17).FieldLen=   256
      Columns(17).Style=   2
      Columns(17).StyleSet=   "headerfield"
      Columns(18).Width=   1508
      Columns(18).Caption=   "Fix Job #"
      Columns(18).Name=   "fixedjobID"
      Columns(18).DataField=   "fixedjobID"
      Columns(18).FieldLen=   256
      Columns(18).StyleSet=   "headerfield"
      Columns(19).Width=   3201
      Columns(19).Caption=   "SF UniqueID"
      Columns(19).Name=   "uniqueID"
      Columns(19).DataField=   "uniqueID"
      Columns(19).FieldLen=   256
      Columns(19).StyleSet=   "headerfield"
      Columns(20).Width=   2249
      Columns(20).Caption=   "Rights Owner"
      Columns(20).Name=   "RightsOwner"
      Columns(20).DataField=   "RightsOwner"
      Columns(20).FieldLen=   256
      Columns(20).StyleSet=   "headerfield"
      Columns(21).Width=   2249
      Columns(21).Caption=   "Project Manager"
      Columns(21).Name=   "ProjectManager"
      Columns(21).DataField=   "ProjectManager"
      Columns(21).FieldLen=   256
      Columns(21).Locked=   -1  'True
      Columns(21).StyleSet=   "headerfield"
      Columns(22).Width=   1111
      Columns(22).Caption=   "ProjectManagerContactID"
      Columns(22).Name=   "ProjectManagerContactID"
      Columns(22).DataField=   "ProjectManagerContactID"
      Columns(22).FieldLen=   256
      Columns(23).Width=   979
      Columns(23).Caption=   "Urgent"
      Columns(23).Name=   "urgent"
      Columns(23).DataField=   "urgent"
      Columns(23).FieldLen=   256
      Columns(23).Style=   2
      Columns(23).StyleSet=   "headerfield"
      Columns(24).Width=   979
      Columns(24).Caption=   "iTunes"
      Columns(24).Name=   "iTunes"
      Columns(24).DataField=   "iTunes"
      Columns(24).FieldLen=   256
      Columns(24).Style=   2
      Columns(24).StyleSet=   "headerfield"
      Columns(25).Width=   2302
      Columns(25).Caption=   "AlphaDisplayCode"
      Columns(25).Name=   "AlphaDisplayCode"
      Columns(25).DataField=   "AlphaDisplayCode"
      Columns(25).DataType=   8
      Columns(25).FieldLen=   256
      Columns(25).StyleSet=   "headerfield"
      Columns(26).Width=   979
      Columns(26).Caption=   "Rev #"
      Columns(26).Name=   "RevisionNumber"
      Columns(26).DataField=   "RevisionNumber"
      Columns(26).FieldLen=   256
      Columns(26).StyleSet=   "headerfield"
      Columns(27).Width=   3519
      Columns(27).Caption=   "Title"
      Columns(27).Name=   "title"
      Columns(27).DataField=   "title"
      Columns(27).FieldLen=   256
      Columns(27).StyleSet=   "headerfield"
      Columns(28).Width=   794
      Columns(28).Caption=   "SR #"
      Columns(28).Name=   "Series"
      Columns(28).DataField=   "Series"
      Columns(28).FieldLen=   256
      Columns(28).StyleSet=   "headerfield"
      Columns(29).Width=   3519
      Columns(29).Caption=   "Episode Title"
      Columns(29).Name=   "subtitle"
      Columns(29).DataField=   "subtitle"
      Columns(29).FieldLen=   256
      Columns(29).StyleSet=   "headerfield"
      Columns(30).Width=   714
      Columns(30).Caption=   "Eps"
      Columns(30).Name=   "episode"
      Columns(30).DataField=   "episode"
      Columns(30).FieldLen=   256
      Columns(30).StyleSet=   "headerfield"
      Columns(31).Width=   3200
      Columns(31).Visible=   0   'False
      Columns(31).Caption=   "storagebarcode"
      Columns(31).Name=   "storagebarcode"
      Columns(31).DataField=   "storagebarcode"
      Columns(31).FieldLen=   256
      Columns(32).Width=   1852
      Columns(32).Caption=   "Program Type"
      Columns(32).Name=   "programtype"
      Columns(32).DataField=   "programtype"
      Columns(32).FieldLen=   256
      Columns(32).StyleSet=   "headerfield"
      Columns(33).Width=   2117
      Columns(33).Caption=   "Barcode"
      Columns(33).Name=   "barcode"
      Columns(33).DataField=   "barcode"
      Columns(33).DataType=   10
      Columns(33).FieldLen=   256
      Columns(33).StyleSet=   "headerfield"
      Columns(34).Width=   1402
      Columns(34).Caption=   "Format"
      Columns(34).Name=   "Format"
      Columns(34).DataField=   "Format"
      Columns(34).FieldLen=   256
      Columns(34).StyleSet=   "headerfield"
      Columns(35).Width=   2064
      Columns(35).Caption=   "Project Number"
      Columns(35).Name=   "projectnumber"
      Columns(35).DataField=   "projectnumber"
      Columns(35).FieldLen=   256
      Columns(35).StyleSet=   "headerfield"
      Columns(36).Width=   2196
      Columns(36).Caption=   "Component Type"
      Columns(36).Name=   "componenttype"
      Columns(36).DataField=   "componenttype"
      Columns(36).FieldLen=   256
      Columns(36).StyleSet=   "headerfield"
      Columns(37).Width=   1588
      Columns(37).Caption=   "Language"
      Columns(37).Name=   "language"
      Columns(37).DataField=   "language"
      Columns(37).FieldLen=   256
      Columns(38).Width=   1588
      Columns(38).Caption=   "Req Fr Rate"
      Columns(38).Name=   "requestedframerate"
      Columns(38).DataField=   "requestedframerate"
      Columns(38).FieldLen=   256
      Columns(38).StyleSet=   "headerfield"
      Columns(39).Width=   1588
      Columns(39).Caption=   "Frame Rate"
      Columns(39).Name=   "framerate"
      Columns(39).DataField=   "framerate"
      Columns(39).FieldLen=   256
      Columns(39).Locked=   -1  'True
      Columns(39).StyleSet=   "headerfield"
      Columns(40).Width=   1164
      Columns(40).Caption=   "Proxy?"
      Columns(40).Name=   "proxyreq"
      Columns(40).DataField=   "proxyreq"
      Columns(40).FieldLen=   256
      Columns(40).Style=   2
      Columns(40).StyleSet=   "headerfield"
      Columns(41).Width=   1323
      Columns(41).Caption=   "Conform?"
      Columns(41).Name=   "conform"
      Columns(41).DataField=   "conform"
      Columns(41).FieldLen=   256
      Columns(41).Style=   2
      Columns(41).StyleSet=   "headerfield"
      Columns(42).Width=   1323
      Columns(42).Caption=   "Textless?"
      Columns(42).Name=   "TextlessRequired"
      Columns(42).DataField=   "TextlessRequired"
      Columns(42).FieldLen=   256
      Columns(42).Style=   2
      Columns(42).StyleSet=   "headerfield"
      Columns(43).Width=   1746
      Columns(43).Caption=   "ME 2.0 Req?"
      Columns(43).Name=   "MERequired"
      Columns(43).DataField=   "MERequired"
      Columns(43).FieldLen=   256
      Columns(43).Style=   2
      Columns(43).StyleSet=   "headerfield"
      Columns(44).Width=   1746
      Columns(44).Caption=   "ME 5.1 Req?"
      Columns(44).Name=   "ME51"
      Columns(44).DataField=   "ME51Required"
      Columns(44).FieldLen=   256
      Columns(44).Style=   2
      Columns(45).Width=   900
      Columns(45).Caption=   "E Aud"
      Columns(45).Name=   "englishsepaudio"
      Columns(45).DataField=   "englishsepaudio"
      Columns(45).FieldLen=   256
      Columns(45).Style=   2
      Columns(45).StyleSet=   "headerfield"
      Columns(46).Width=   900
      Columns(46).Caption=   "E Sub"
      Columns(46).Name=   "englishsubs"
      Columns(46).DataField=   "englishsubs"
      Columns(46).FieldLen=   256
      Columns(46).Style=   2
      Columns(46).StyleSet=   "headerfield"
      Columns(47).Width=   926
      Columns(47).Caption=   "N Aud"
      Columns(47).Name=   "norwegiansepaudio"
      Columns(47).DataField=   "norwegiansepaudio"
      Columns(47).FieldLen=   256
      Columns(47).Style=   2
      Columns(47).StyleSet=   "headerfield"
      Columns(48).Width=   926
      Columns(48).Caption=   "N Sub"
      Columns(48).Name=   "norwegiansubs"
      Columns(48).DataField=   "norwegiansubs"
      Columns(48).FieldLen=   256
      Columns(48).Style=   2
      Columns(48).StyleSet=   "headerfield"
      Columns(49).Width=   900
      Columns(49).Caption=   "S Aud"
      Columns(49).Name=   "swedishsepaudio"
      Columns(49).DataField=   "swedishsepaudio"
      Columns(49).FieldLen=   256
      Columns(49).Style=   2
      Columns(49).StyleSet=   "headerfield"
      Columns(50).Width=   900
      Columns(50).Caption=   "S Sub"
      Columns(50).Name=   "swedishsubs"
      Columns(50).DataField=   "swedishsubs"
      Columns(50).FieldLen=   256
      Columns(50).Style=   2
      Columns(50).StyleSet=   "headerfield"
      Columns(51).Width=   900
      Columns(51).Caption=   "D Aud"
      Columns(51).Name=   "danishsepaudio"
      Columns(51).DataField=   "danishsepaudio"
      Columns(51).FieldLen=   256
      Columns(51).Style=   2
      Columns(51).StyleSet=   "headerfield"
      Columns(52).Width=   900
      Columns(52).Caption=   "D Sub"
      Columns(52).Name=   "danishsubs"
      Columns(52).DataField=   "danishsubs"
      Columns(52).FieldLen=   256
      Columns(52).Style=   2
      Columns(52).StyleSet=   "headerfield"
      Columns(53).Width=   900
      Columns(53).Caption=   "F Aud"
      Columns(53).Name=   "finnishsepaudio"
      Columns(53).DataField=   "finnishsepaudio"
      Columns(53).FieldLen=   256
      Columns(53).Style=   2
      Columns(53).StyleSet=   "headerfield"
      Columns(54).Width=   900
      Columns(54).Caption=   "F Sub"
      Columns(54).Name=   "finnishsubs"
      Columns(54).DataField=   "finnishsubs"
      Columns(54).FieldLen=   256
      Columns(54).Style=   2
      Columns(54).StyleSet=   "headerfield"
      Columns(55).Width=   900
      Columns(55).Caption=   "I Aud"
      Columns(55).Name=   "icelandicsepaudio"
      Columns(55).DataField=   "icelandicsepaudio"
      Columns(55).FieldLen=   256
      Columns(55).Style=   2
      Columns(55).StyleSet=   "headerfield"
      Columns(56).Width=   900
      Columns(56).Caption=   "I Sub"
      Columns(56).Name=   "icelandicsubs"
      Columns(56).DataField=   "icelandicsubs"
      Columns(56).FieldLen=   256
      Columns(56).Style=   2
      Columns(56).StyleSet=   "headerfield"
      Columns(57).Width=   2117
      Columns(57).Caption=   "Due Date"
      Columns(57).Name=   "duedate"
      Columns(57).DataField=   "duedate"
      Columns(57).FieldLen=   256
      Columns(57).Style=   1
      Columns(58).Width=   2011
      Columns(58).Caption=   "Special Project"
      Columns(58).Name=   "SpecialProject"
      Columns(58).DataField=   "SpecialProject"
      Columns(58).FieldLen=   256
      Columns(58).StyleSet=   "headerfield"
      Columns(59).Width=   2037
      Columns(59).Caption=   "Master Here"
      Columns(59).Name=   "masterarrived"
      Columns(59).DataField=   "masterarrived"
      Columns(59).FieldLen=   256
      Columns(59).Style=   1
      Columns(59).StyleSet=   "stagefield"
      Columns(60).Width=   2117
      Columns(60).Caption=   "Validation"
      Columns(60).Name=   "Validation"
      Columns(60).DataField=   "Validation"
      Columns(60).FieldLen=   256
      Columns(60).Style=   1
      Columns(60).StyleSet=   "stagefield"
      Columns(61).Width=   2117
      Columns(61).Caption=   "Encode"
      Columns(61).Name=   "filemade"
      Columns(61).DataField=   "filemade"
      Columns(61).FieldLen=   256
      Columns(61).Style=   1
      Columns(61).StyleSet=   "stagefield"
      Columns(62).Width=   2117
      Columns(62).Caption=   "File Sent"
      Columns(62).Name=   "filesent"
      Columns(62).DataField=   "filesent"
      Columns(62).FieldLen=   256
      Columns(62).Style=   1
      Columns(62).StyleSet=   "stagefield"
      Columns(63).Width=   2117
      Columns(63).Caption=   "En. Arrived"
      Columns(63).Name=   "englisharrived"
      Columns(63).DataField=   "englisharrived"
      Columns(63).FieldLen=   256
      Columns(63).Style=   1
      Columns(63).StyleSet=   "stagefield"
      Columns(64).Width=   2117
      Columns(64).Caption=   "En. Subs. Arr."
      Columns(64).Name=   "englishsubsarrived"
      Columns(64).DataField=   "englishsubsarrived"
      Columns(64).FieldLen=   256
      Columns(64).Style=   1
      Columns(64).StyleSet=   "stagefield"
      Columns(65).Width=   2117
      Columns(65).Caption=   "En. Validation"
      Columns(65).Name=   "englishvalidation"
      Columns(65).DataField=   "englishvalidation"
      Columns(65).FieldLen=   256
      Columns(65).Style=   1
      Columns(65).StyleSet=   "stagefield"
      Columns(66).Width=   2117
      Columns(66).Caption=   "En. Subs Val"
      Columns(66).Name=   "englishsubsvalidation"
      Columns(66).DataField=   "englishsubsvalidation"
      Columns(66).FieldLen=   256
      Columns(66).Style=   1
      Columns(66).StyleSet=   "stagefield"
      Columns(67).Width=   2117
      Columns(67).Caption=   "En. Sent"
      Columns(67).Name=   "englishsent"
      Columns(67).DataField=   "englishsent"
      Columns(67).FieldLen=   256
      Columns(67).Style=   1
      Columns(67).StyleSet=   "stagefield"
      Columns(68).Width=   2117
      Columns(68).Caption=   "En. Subs Sent"
      Columns(68).Name=   "englishsubssent"
      Columns(68).DataField=   "englishsubssent"
      Columns(68).FieldLen=   256
      Columns(68).Style=   1
      Columns(68).StyleSet=   "stagefield"
      Columns(69).Width=   2117
      Columns(69).Caption=   "No. Subs Sent"
      Columns(69).Name=   "norwegiansubssent"
      Columns(69).DataField=   "norwegiansubssent"
      Columns(69).FieldLen=   256
      Columns(69).Style=   1
      Columns(69).StyleSet=   "stagefield"
      Columns(70).Width=   2117
      Columns(70).Caption=   "No. Arrived"
      Columns(70).Name=   "norwegianarrived"
      Columns(70).DataField=   "norwegianarrived"
      Columns(70).FieldLen=   256
      Columns(70).Style=   1
      Columns(70).StyleSet=   "stagefield"
      Columns(71).Width=   2117
      Columns(71).Caption=   "No Subs. Arr."
      Columns(71).Name=   "norwegiansubsarrived"
      Columns(71).DataField=   "norwegiansubsarrived"
      Columns(71).FieldLen=   256
      Columns(71).Style=   1
      Columns(71).StyleSet=   "stagefield"
      Columns(72).Width=   2117
      Columns(72).Caption=   "No. Validation"
      Columns(72).Name=   "norwegianvalidation"
      Columns(72).DataField=   "norwegianvalidation"
      Columns(72).FieldLen=   256
      Columns(72).Style=   1
      Columns(72).StyleSet=   "stagefield"
      Columns(73).Width=   2117
      Columns(73).Caption=   "No. Subs Val."
      Columns(73).Name=   "norwegiansubsvalidation"
      Columns(73).DataField=   "norwegiansubsvalidation"
      Columns(73).FieldLen=   256
      Columns(73).Style=   1
      Columns(73).StyleSet=   "stagefield"
      Columns(74).Width=   2117
      Columns(74).Caption=   "No. Sent"
      Columns(74).Name=   "norwegiansent"
      Columns(74).DataField=   "norwegiansent"
      Columns(74).FieldLen=   256
      Columns(74).Style=   1
      Columns(74).StyleSet=   "stagefield"
      Columns(75).Width=   2117
      Columns(75).Caption=   "Sw. Arrived"
      Columns(75).Name=   "swedisharrived"
      Columns(75).DataField=   "swedisharrived"
      Columns(75).FieldLen=   256
      Columns(75).Style=   1
      Columns(75).StyleSet=   "stagefield"
      Columns(76).Width=   2117
      Columns(76).Caption=   "Sw. Subs. Arr"
      Columns(76).Name=   "swedishsubsarrived"
      Columns(76).DataField=   "swedishsubsarrived"
      Columns(76).FieldLen=   256
      Columns(76).Style=   1
      Columns(76).StyleSet=   "stagefield"
      Columns(77).Width=   2117
      Columns(77).Caption=   "Sw. Validation"
      Columns(77).Name=   "swedishvalidation"
      Columns(77).DataField=   "swedishvalidation"
      Columns(77).FieldLen=   256
      Columns(77).Style=   1
      Columns(77).StyleSet=   "stagefield"
      Columns(78).Width=   2117
      Columns(78).Caption=   "Sw Subs Val."
      Columns(78).Name=   "swedishsubsvalidation"
      Columns(78).DataField=   "swedishsubsvalidation"
      Columns(78).FieldLen=   256
      Columns(78).Style=   1
      Columns(78).StyleSet=   "stagefield"
      Columns(79).Width=   2117
      Columns(79).Caption=   "Sw. Sent"
      Columns(79).Name=   "swedishsent"
      Columns(79).DataField=   "swedishsent"
      Columns(79).FieldLen=   256
      Columns(79).Style=   1
      Columns(79).StyleSet=   "stagefield"
      Columns(80).Width=   2117
      Columns(80).Caption=   "Sw. Subs Sent"
      Columns(80).Name=   "swedishsubssent"
      Columns(80).DataField=   "swedishsubssent"
      Columns(80).FieldLen=   256
      Columns(80).Style=   1
      Columns(80).StyleSet=   "stagefield"
      Columns(81).Width=   2117
      Columns(81).Caption=   "Da. Arrived"
      Columns(81).Name=   "danisharrived"
      Columns(81).DataField=   "danisharrived"
      Columns(81).FieldLen=   256
      Columns(81).Style=   1
      Columns(81).StyleSet=   "stagefield"
      Columns(82).Width=   2117
      Columns(82).Caption=   "Da. Subs. Arr."
      Columns(82).Name=   "danishsubsarrived"
      Columns(82).DataField=   "danishsubsarrived"
      Columns(82).FieldLen=   256
      Columns(82).Style=   1
      Columns(82).StyleSet=   "stagefield"
      Columns(83).Width=   2117
      Columns(83).Caption=   "Da. Validation"
      Columns(83).Name=   "danishvalidation"
      Columns(83).DataField=   "danishvalidation"
      Columns(83).FieldLen=   256
      Columns(83).Style=   1
      Columns(83).StyleSet=   "stagefield"
      Columns(84).Width=   2117
      Columns(84).Caption=   "Da. Subs Val."
      Columns(84).Name=   "danishsubsvalidation"
      Columns(84).DataField=   "danishsubsvalidation"
      Columns(84).FieldLen=   256
      Columns(84).Style=   1
      Columns(84).StyleSet=   "stagefield"
      Columns(85).Width=   2117
      Columns(85).Caption=   "Da. Sent"
      Columns(85).Name=   "danishsent"
      Columns(85).DataField=   "danishsent"
      Columns(85).FieldLen=   256
      Columns(85).Style=   1
      Columns(85).StyleSet=   "stagefield"
      Columns(86).Width=   2117
      Columns(86).Caption=   "Da. Subs Sent"
      Columns(86).Name=   "danishsubssent"
      Columns(86).DataField=   "danishsubssent"
      Columns(86).FieldLen=   256
      Columns(86).Style=   1
      Columns(86).StyleSet=   "stagefield"
      Columns(87).Width=   2117
      Columns(87).Caption=   "Fi. Arrived"
      Columns(87).Name=   "finnisharrived"
      Columns(87).DataField=   "finnisharrived"
      Columns(87).FieldLen=   256
      Columns(87).Style=   1
      Columns(87).StyleSet=   "stagefield"
      Columns(88).Width=   2117
      Columns(88).Caption=   "Fi. Subs. Arr."
      Columns(88).Name=   "finnishsubsarrived"
      Columns(88).DataField=   "finnishsubsarrived"
      Columns(88).FieldLen=   256
      Columns(88).Style=   1
      Columns(88).StyleSet=   "stagefield"
      Columns(89).Width=   2117
      Columns(89).Caption=   "Fi. Validation"
      Columns(89).Name=   "finnishvalidation"
      Columns(89).DataField=   "finnishvalidation"
      Columns(89).FieldLen=   256
      Columns(89).Style=   1
      Columns(89).StyleSet=   "stagefield"
      Columns(90).Width=   2117
      Columns(90).Caption=   "Fi. Subs Val."
      Columns(90).Name=   "finnishsubsvalidation"
      Columns(90).DataField=   "finnishsubsvalidation"
      Columns(90).FieldLen=   256
      Columns(90).Style=   1
      Columns(90).StyleSet=   "stagefield"
      Columns(91).Width=   2117
      Columns(91).Caption=   "Fi. Sent"
      Columns(91).Name=   "finnishsent"
      Columns(91).DataField=   "finnishsent"
      Columns(91).FieldLen=   256
      Columns(91).Style=   1
      Columns(91).StyleSet=   "stagefield"
      Columns(92).Width=   2117
      Columns(92).Caption=   "Fi. Subs Sent"
      Columns(92).Name=   "finnishsubssent"
      Columns(92).DataField=   "finnishsubssent"
      Columns(92).FieldLen=   256
      Columns(92).Style=   1
      Columns(92).StyleSet=   "stagefield"
      Columns(93).Width=   2117
      Columns(93).Caption=   "Ic. Arrived"
      Columns(93).Name=   "icelandicarrived"
      Columns(93).DataField=   "icelandicarrived"
      Columns(93).FieldLen=   256
      Columns(93).Style=   1
      Columns(93).StyleSet=   "stagefield"
      Columns(94).Width=   2117
      Columns(94).Caption=   "Ic. Subs. Arr."
      Columns(94).Name=   "icelandicsubsarrived"
      Columns(94).DataField=   "icelandicsubsarrived"
      Columns(94).FieldLen=   256
      Columns(94).Style=   1
      Columns(94).StyleSet=   "stagefield"
      Columns(95).Width=   2117
      Columns(95).Caption=   "Ic. Validation"
      Columns(95).Name=   "icelandicvalidation"
      Columns(95).DataField=   "icelandicvalidation"
      Columns(95).FieldLen=   256
      Columns(95).Style=   1
      Columns(95).StyleSet=   "stagefield"
      Columns(96).Width=   2117
      Columns(96).Caption=   "Ic. Subs Val."
      Columns(96).Name=   "icelandicsubsvalidation"
      Columns(96).DataField=   "icelandicsubsvalidation"
      Columns(96).FieldLen=   256
      Columns(96).Style=   1
      Columns(96).StyleSet=   "stagefield"
      Columns(97).Width=   2117
      Columns(97).Caption=   "Ic. Sent"
      Columns(97).Name=   "icelandicsent"
      Columns(97).DataField=   "icelandicsent"
      Columns(97).FieldLen=   256
      Columns(97).Style=   1
      Columns(97).StyleSet=   "stagefield"
      Columns(98).Width=   2117
      Columns(98).Caption=   "Ic. Subs Sent"
      Columns(98).Name=   "icelandicsubssent"
      Columns(98).DataField=   "icelandicsubssent"
      Columns(98).FieldLen=   256
      Columns(98).Style=   1
      Columns(98).StyleSet=   "stagefield"
      Columns(99).Width=   2117
      Columns(99).Caption=   "Proxy Made"
      Columns(99).Name=   "proxymade"
      Columns(99).DataField=   "proxymade"
      Columns(99).FieldLen=   256
      Columns(99).Style=   1
      Columns(99).StyleSet=   "stagefield"
      Columns(100).Width=   2117
      Columns(100).Caption=   "Proxy Sent"
      Columns(100).Name=   "proxysent"
      Columns(100).DataField=   "proxysent"
      Columns(100).FieldLen=   256
      Columns(100).Style=   1
      Columns(100).StyleSet=   "stagefield"
      Columns(101).Width=   2117
      Columns(101).Caption=   "XML Made"
      Columns(101).Name=   "xmlmade"
      Columns(101).DataField=   "xmlmade"
      Columns(101).FieldLen=   256
      Columns(101).Style=   1
      Columns(101).StyleSet=   "stagefield"
      Columns(102).Width=   2117
      Columns(102).Caption=   "XML Sent"
      Columns(102).Name=   "xmlsent"
      Columns(102).DataField=   "xmlsent"
      Columns(102).FieldLen=   256
      Columns(102).Style=   1
      Columns(102).StyleSet=   "stagefield"
      Columns(103).Width=   2117
      Columns(103).Caption=   "Resend"
      Columns(103).Name=   "resend"
      Columns(103).DataField=   "resend"
      Columns(103).FieldLen=   256
      Columns(103).Style=   1
      Columns(103).StyleSet=   "stagefield"
      Columns(104).Width=   3200
      Columns(104).Visible=   0   'False
      Columns(104).Caption=   "Tape Out"
      Columns(104).Name=   "tapereturned"
      Columns(104).DataField=   "tapereturned"
      Columns(104).FieldLen=   256
      Columns(104).Style=   1
      Columns(104).StyleSet=   "stagefield"
      Columns(105).Width=   2117
      Columns(105).Caption=   "Complete"
      Columns(105).Name=   "complete"
      Columns(105).DataField=   "complete"
      Columns(105).FieldLen=   256
      Columns(105).Style=   1
      Columns(105).StyleSet=   "stagefield"
      Columns(106).Width=   873
      Columns(106).Caption=   "Here"
      Columns(106).Name=   "assetshere"
      Columns(106).DataField=   "assetshere"
      Columns(106).FieldLen=   256
      Columns(106).Style=   2
      Columns(106).StyleSet=   "conclusionfield"
      Columns(107).Width=   873
      Columns(107).Caption=   "Done"
      Columns(107).Name=   "readytobill"
      Columns(107).DataField=   "readytobill"
      Columns(107).FieldLen=   256
      Columns(107).Locked=   -1  'True
      Columns(107).Style=   2
      Columns(107).StyleSet=   "conclusionfield"
      Columns(108).Width=   873
      Columns(108).Caption=   "Billed"
      Columns(108).Name=   "billed"
      Columns(108).DataField=   "billed"
      Columns(108).FieldLen=   256
      Columns(108).Style=   2
      Columns(108).StyleSet=   "conclusionfield"
      Columns(109).Width=   926
      Columns(109).Caption=   "Faulty"
      Columns(109).Name=   "complainedabout"
      Columns(109).DataField=   "complainedabout"
      Columns(109).FieldLen=   256
      Columns(109).Style=   2
      Columns(109).StyleSet=   "conclusionfield"
      Columns(110).Width=   3200
      Columns(110).Visible=   0   'False
      Columns(110).Caption=   "jobID"
      Columns(110).Name=   "jobID"
      Columns(110).DataField=   "jobID"
      Columns(110).FieldLen=   256
      Columns(111).Width=   873
      Columns(111).Caption=   "Redo"
      Columns(111).Name=   "complaintredoitem"
      Columns(111).DataField=   "complaintredoitem"
      Columns(111).FieldLen=   256
      Columns(111).Style=   2
      Columns(111).StyleSet=   "conclusionfield"
      Columns(112).Width=   1826
      Columns(112).Caption=   "cdate"
      Columns(112).Name=   "cdate"
      Columns(112).DataField=   "cdate"
      Columns(112).FieldLen=   256
      Columns(113).Width=   1323
      Columns(113).Caption=   "mdate"
      Columns(113).Name=   "mdate"
      Columns(113).DataField=   "mdate"
      Columns(113).FieldLen=   256
      Columns(114).Width=   609
      Columns(114).Caption=   "Ref"
      Columns(114).Name=   "itemreference"
      Columns(114).DataField=   "itemreference"
      Columns(114).FieldLen=   256
      Columns(114).StyleSet=   "headerfield"
      Columns(115).Width=   714
      Columns(115).Caption=   "Dur"
      Columns(115).Name=   "duration"
      Columns(115).DataField=   "duration"
      Columns(115).FieldLen=   256
      Columns(115).StyleSet=   "headerfield"
      Columns(116).Width=   1508
      Columns(116).Caption=   "Length"
      Columns(116).Name=   "timecodeduration"
      Columns(116).DataField=   "timecodeduration"
      Columns(116).FieldLen=   256
      Columns(116).StyleSet=   "headerfield"
      Columns(117).Width=   3200
      Columns(117).Visible=   0   'False
      Columns(117).Caption=   "hdflag"
      Columns(117).Name=   "hdflag"
      Columns(117).DataField=   "hdflag"
      Columns(117).FieldLen=   256
      Columns(117).Style=   2
      Columns(117).StyleSet=   "headerfield"
      _ExtentX        =   50297
      _ExtentY        =   16431
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSAdodcLib.Adodc adoComments 
      Height          =   330
      Left            =   13500
      Top             =   13020
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdComments 
      Bindings        =   "frmTrackerSvensk.frx":00A1
      Height          =   2175
      Left            =   120
      TabIndex        =   47
      Top             =   12000
      Width           =   27045
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets.count =   1
      stylesets(0).Name=   "comment"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmTrackerSvensk.frx":00BB
      AllowAddNew     =   -1  'True
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      BackColorOdd    =   12582847
      RowHeight       =   476
      ExtraHeight     =   291
      Columns.Count   =   17
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "trackerhistoryID"
      Columns(0).Name =   "tracker_svensk_commentID"
      Columns(0).DataField=   "tracker_svensk_commentID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "trackerprogramID"
      Columns(1).Name =   "tracker_svensk_itemID"
      Columns(1).DataField=   "tracker_svensk_itemID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   10266
      Columns(2).Caption=   "Comments - PLEASE FOLLOW THE NEW DECISION TREE INSTRUCTIONS"
      Columns(2).Name =   "comment"
      Columns(2).DataField=   "comment"
      Columns(2).FieldLen=   255
      Columns(2).HasForeColor=   -1  'True
      Columns(2).StyleSet=   "comment"
      Columns(3).Width=   5768
      Columns(3).Caption=   "File Location"
      Columns(3).Name =   "filelocation"
      Columns(3).DataField=   "filelocation"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "comment"
      Columns(4).Width=   3360
      Columns(4).Caption=   "Date"
      Columns(4).Name =   "cdate"
      Columns(4).DataField=   "cdate"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).Style=   1
      Columns(4).HasForeColor=   -1  'True
      Columns(4).ForeColor=   8388608
      Columns(4).StyleSet=   "comment"
      Columns(5).Width=   2117
      Columns(5).Caption=   "Entered By"
      Columns(5).Name =   "cuser"
      Columns(5).DataField=   "cuser"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).StyleSet=   "comment"
      Columns(6).Width=   2487
      Columns(6).Caption=   "Decision Tree Comment?"
      Columns(6).Name =   "commenttype"
      Columns(6).DataField=   "commenttype"
      Columns(6).FieldLen=   256
      Columns(6).StyleSet=   "comment"
      Columns(7).Width=   3200
      Columns(7).Caption=   "Comment Type"
      Columns(7).Name =   "commenttypedecoded"
      Columns(7).FieldLen=   256
      Columns(8).Width=   1296
      Columns(8).Caption=   "Clock?"
      Columns(8).Name =   "Email - Clock?"
      Columns(8).DataField=   "emailclock"
      Columns(8).FieldLen=   256
      Columns(8).Style=   2
      Columns(8).StyleSet=   "comment"
      Columns(9).Width=   1085
      Columns(9).Caption=   "B&T?"
      Columns(9).Name =   "Email - B&T?"
      Columns(9).DataField=   "emailbarsandtone"
      Columns(9).FieldLen=   256
      Columns(9).Style=   2
      Columns(9).StyleSet=   "comment"
      Columns(10).Width=   873
      Columns(10).Caption=   "T/C?"
      Columns(10).Name=   "Email - T/C?"
      Columns(10).DataField=   "emailtimecode"
      Columns(10).FieldLen=   256
      Columns(10).Style=   2
      Columns(10).StyleSet=   "comment"
      Columns(11).Width=   2117
      Columns(11).Caption=   "Black at End?"
      Columns(11).Name=   "Email - Black at End?"
      Columns(11).DataField=   "emailblackatend"
      Columns(11).FieldLen=   256
      Columns(11).Style=   2
      Columns(11).StyleSet=   "comment"
      Columns(12).Width=   4128
      Columns(12).Caption=   "Textless Not Rq but Present?"
      Columns(12).Name=   "Email - Textless Not Rq"
      Columns(12).DataField=   "emailtextlesspresent"
      Columns(12).FieldLen=   256
      Columns(12).Style=   2
      Columns(12).StyleSet=   "comment"
      Columns(13).Width=   2381
      Columns(13).Caption=   "Black on White?"
      Columns(13).Name=   "emailblackonwhitetexstless"
      Columns(13).DataField=   "emailblackonwhitetexstless"
      Columns(13).FieldLen=   256
      Columns(13).Style=   2
      Columns(13).StyleSet=   "comment"
      Columns(14).Width=   3043
      Columns(14).Caption=   "Track Layout Wrong?"
      Columns(14).Name=   "emailtracklayoutwrong"
      Columns(14).DataField=   "emailtracklayoutwrong"
      Columns(14).FieldLen=   256
      Columns(14).Style=   2
      Columns(14).StyleSet=   "comment"
      Columns(15).Width=   1429
      Columns(15).Caption=   "Resolved?"
      Columns(15).Name=   "issueresolved"
      Columns(15).DataField=   "issueresolved"
      Columns(15).FieldLen=   256
      Columns(15).Style=   2
      Columns(16).Width=   1773
      Columns(16).Caption=   "Send Email?"
      Columns(16).Name=   "Email?"
      Columns(16).DataField=   "Column 5"
      Columns(16).DataType=   8
      Columns(16).FieldLen=   256
      Columns(16).Style=   4
      Columns(16).StyleSet=   "comment"
      UseDefaults     =   0   'False
      _ExtentX        =   47704
      _ExtentY        =   3836
      _StockProps     =   79
      Caption         =   "Tracker Comments - If a comment relates to the Decision Tree, please tick the box!!!"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial Narrow"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
      Height          =   315
      Left            =   6180
      TabIndex        =   125
      Tag             =   "NOCLEAR"
      ToolTipText     =   "The company this job is for"
      Top             =   1440
      Width           =   1815
      DataFieldList   =   "portalcompanyname"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BorderStyle     =   0
      ColumnHeaders   =   0   'False
      BeveColorScheme =   1
      ForeColorEven   =   -2147483640
      ForeColorOdd    =   -2147483640
      BackColorEven   =   -2147483643
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   6376
      Columns(0).Caption=   "name"
      Columns(0).Name =   "portalcompanyname"
      Columns(0).DataField=   "portalcompanyname"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "companyID"
      Columns(1).Name =   "companyID"
      Columns(1).DataField=   "companyID"
      Columns(1).FieldLen=   256
      _ExtentX        =   3201
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   16761087
      DataFieldToDisplay=   "portalcompanyname"
   End
   Begin VB.Label lblCaption 
      Caption         =   "Special Project"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   32
      Left            =   120
      TabIndex        =   245
      Top             =   2220
      Width           =   1515
   End
   Begin VB.Label lblSearchCompanyID 
      Height          =   315
      Left            =   3240
      TabIndex        =   132
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "On"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   31
      Left            =   16800
      TabIndex        =   131
      Top             =   1920
      Width           =   255
   End
   Begin VB.Label lblLastValidationBy 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   14520
      TabIndex        =   130
      Top             =   1860
      Width           =   2175
   End
   Begin VB.Label lblCaption 
      Alignment       =   1  'Right Justify
      Caption         =   "Last Validation By"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   30
      Left            =   12300
      TabIndex        =   129
      Top             =   1920
      Width           =   2055
   End
   Begin VB.Label lblLastValidationDate 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Left            =   17160
      TabIndex        =   128
      Top             =   1860
      Width           =   1815
   End
   Begin VB.Label lblCaption 
      Caption         =   "Project Manager"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   29
      Left            =   120
      TabIndex        =   127
      Top             =   1860
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Component Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   4620
      TabIndex        =   42
      Top             =   1140
      Width           =   1515
   End
   Begin VB.Label lblCaption 
      Caption         =   "Program Type"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   120
      TabIndex        =   41
      Top             =   1500
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Format"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   4620
      TabIndex        =   40
      Top             =   780
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "SF Department"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   39
      Top             =   1140
      Width           =   1395
   End
   Begin VB.Label lblLastTrackerItemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   1260
      TabIndex        =   24
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCaption 
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   10
      Left            =   4620
      TabIndex        =   22
      Top             =   1500
      Width           =   915
   End
   Begin VB.Label lblCaption 
      Caption         =   "Reference"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   13
      Left            =   120
      TabIndex        =   20
      Top             =   60
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Series"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   4620
      TabIndex        =   17
      Top             =   60
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Episode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   4620
      TabIndex        =   16
      Top             =   420
      Width           =   795
   End
   Begin VB.Label lblCaption 
      Caption         =   "Subtitle"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   780
      Width           =   1395
   End
   Begin VB.Label lblCaption 
      Caption         =   "Title"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   11
      Top             =   420
      Width           =   1395
   End
   Begin VB.Label lblTrackeritemID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   180
      TabIndex        =   9
      Top             =   14820
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblCompanyID 
      Height          =   315
      Left            =   2340
      TabIndex        =   8
      Top             =   14820
      Visible         =   0   'False
      Width           =   735
   End
End
Attribute VB_Name = "frmTrackerSvensk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strSearch As String
Dim m_strOrderby As String
Dim m_blnDelete As Boolean
Dim m_blnDontVerifyXML As Boolean
Dim m_blnBilling As Boolean
Dim m_blnSilent As Boolean

Private Sub HideAllColumns()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = False
grdItems.Columns("subtitle").Visible = False
grdItems.Columns("programtype").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = False
grdItems.Columns("Framerate").Visible = False
grdItems.Columns("proxyreq").Visible = False
grdItems.Columns("englishsepaudio").Visible = False
grdItems.Columns("englishsubs").Visible = False
grdItems.Columns("norwegiansepaudio").Visible = False
grdItems.Columns("norwegiansubs").Visible = False
grdItems.Columns("swedishsepaudio").Visible = False
grdItems.Columns("swedishsubs").Visible = False
grdItems.Columns("danishsepaudio").Visible = False
grdItems.Columns("danishsubs").Visible = False
grdItems.Columns("finnishsepaudio").Visible = False
grdItems.Columns("finnishsubs").Visible = False
grdItems.Columns("icelandicsepaudio").Visible = False
grdItems.Columns("icelandicsubs").Visible = False
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("englisharrived").Visible = False
grdItems.Columns("englishsubsarrived").Visible = False
grdItems.Columns("englishvalidation").Visible = False
grdItems.Columns("englishsubsvalidation").Visible = False
grdItems.Columns("englishsent").Visible = False
grdItems.Columns("englishsubssent").Visible = False
grdItems.Columns("norwegianarrived").Visible = False
grdItems.Columns("norwegiansubsarrived").Visible = False
grdItems.Columns("norwegiansubsvalidation").Visible = False
grdItems.Columns("norwegianvalidation").Visible = False
grdItems.Columns("norwegiansent").Visible = False
grdItems.Columns("norwegiansubssent").Visible = False
grdItems.Columns("swedisharrived").Visible = False
grdItems.Columns("swedishsubsarrived").Visible = False
grdItems.Columns("swedishvalidation").Visible = False
grdItems.Columns("swedishsubsvalidation").Visible = False
grdItems.Columns("swedishsent").Visible = False
grdItems.Columns("swedishsubssent").Visible = False
grdItems.Columns("danisharrived").Visible = False
grdItems.Columns("danishsubsarrived").Visible = False
grdItems.Columns("danishvalidation").Visible = False
grdItems.Columns("danishsubsvalidation").Visible = False
grdItems.Columns("danishsent").Visible = False
grdItems.Columns("danishsubssent").Visible = False
grdItems.Columns("finnisharrived").Visible = False
grdItems.Columns("finnishsubsarrived").Visible = False
grdItems.Columns("finnishvalidation").Visible = False
grdItems.Columns("finnishsubsvalidation").Visible = False
grdItems.Columns("finnishsent").Visible = False
grdItems.Columns("finnishsubssent").Visible = False
grdItems.Columns("icelandicarrived").Visible = False
grdItems.Columns("icelandicsubsarrived").Visible = False
grdItems.Columns("icelandicvalidation").Visible = False
grdItems.Columns("icelandicsubsvalidation").Visible = False
grdItems.Columns("icelandicsent").Visible = False
grdItems.Columns("icelandicsubssent").Visible = False
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = False
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub chkHideDemo_Click()

Dim l_strSQL As String

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' and companyID > 100 AND system_active = 1 ORDER BY name;"
Else
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' AND system_active = 1 ORDER BY name;"
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdItems.Columns("channel").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbCompany_Click()

lblSearchCompanyID.Caption = cmbCompany.Columns("companyID").Text
m_strSearch = " WHERE companyID = " & lblSearchCompanyID.Caption
'm_strOrderby = " ORDER BY headertext1, headerint1, headerint2, headerint3, headerint4, headerint5"

DoEvents

Dim l_rstChoices As ADODB.Recordset, l_blnWide As Boolean, l_strSQL As String

If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
    l_blnWide = True
Else
    l_blnWide = False
End If

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT name, contactID FROM contact WHERE contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblSearchCompanyID.Caption) & ") ORDER BY name;"

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

Set ddnProjectManager.DataSource = l_rstSearch1

l_rstSearch1.ActiveConnection = Nothing

grdItems.Columns("projectmanager").DropDownHwnd = ddnProjectManager.hWnd
grdItems.Columns("projectmanager").Locked = False

l_conSearch.Close
Set l_conSearch = Nothing

cmdSearch.Value = True

End Sub

Private Sub cmbComponentType_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbComponentType.Text
    cmbComponentType.Clear
    PopulateCombo "svenskcomponenttype", cmbComponentType, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbComponentType.Text = l_strTemp
End If
HighLite cmbComponentType
End Sub

Private Sub cmbFormat_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbFormat.Text
    cmbFormat.Clear
    PopulateCombo "svenskformat", cmbFormat, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbFormat.Text = l_strTemp
End If
HighLite cmbFormat
End Sub

Private Sub cmbFrameRate_GotFocus()

PopulateCombo "framerate", cmbFrameRate, "MEDIA"
HighLite cmbFrameRate

End Sub

Private Sub cmbProgramType_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbProgramType.Text
    cmbProgramType.Clear
    PopulateCombo "svenskprogramtype", cmbProgramType, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbProgramType.Text = l_strTemp
End If
HighLite cmbProgramType
End Sub

Private Sub cmbProjectManager_DropDown()

Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbProjectManager.Text
    cmbProjectManager.Clear
    PopulateCombo "svenskprojectmanager", cmbProjectManager, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbProjectManager.Text = l_strTemp
End If
HighLite cmbProjectManager

End Sub

Private Sub cmbRightsOwner_DropDown()
Dim l_strTemp As String
If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strTemp = cmbRightsOwner.Text
    cmbRightsOwner.Clear
    PopulateCombo "svenskrightsowner", cmbRightsOwner, "companyID = " & Val(lblSearchCompanyID.Caption) & " "
    cmbRightsOwner.Text = l_strTemp
End If
HighLite cmbRightsOwner
End Sub

Private Sub cmdAudioValidationSave_Click()

Dim tempdate As Date, l_strSQL As String

If optAudioValidationComplete(0).Value = True Then
    tempdate = FormatDateTime(Now, vbLongDate)
    If grdItems.Columns("englishsepaudio").Text <> 0 Then
        grdItems.Columns("englishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("norwegiansepaudio").Text <> 0 Then
        grdItems.Columns("norwegianvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("swedishsepaudio").Text <> 0 Then
        grdItems.Columns("swedishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("danishsepaudio").Text <> 0 Then
        grdItems.Columns("danishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("finnishsepaudio").Text <> 0 Then
        grdItems.Columns("finnishvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("icelandicsepaudio").Text <> 0 Then
        grdItems.Columns("icelandicvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
Else
    grdItems.Columns("englishvalidation").Text = ""
    grdItems.Columns("norwegianvalidation").Text = ""
    grdItems.Columns("swedishvalidation").Text = ""
    grdItems.Columns("danishvalidation").Text = ""
    grdItems.Columns("finnishvalidation").Text = ""
    grdItems.Columns("icelandicvalidation").Text = ""
End If

If optEnglishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optEnglishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optNorwegianInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "NorwegianInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optNorwegianInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "NorwegianInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "NorwegianInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optSwedishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "SwedishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optSwedishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "SwedishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "SwedishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optDanishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "DanishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optDanishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "DanishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "DanishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optFinnishInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "FinnishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optFinnishInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "FinnishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "FinnishInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optIcelandicInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "IcelandicInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optIcelandicInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "IcelandicInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "IcelandicInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

l_strSQL = "INSERT INTO tracker_svensk_validation (tracker_svensk_itemID, ValidationDate, ValidatedBy) VALUES ("
l_strSQL = l_strSQL & Val(lblTrackeritemID.Caption) & ", "
l_strSQL = l_strSQL & "getdate(), "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdCloseAudioValidation.Value = True

End Sub

Private Sub cmdBillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then
    
    While Not adoItems.Recordset.EOF
        If cmdBillItem.Visible = True Then
            cmdBillItem.Value = True
        End If
        adoItems.Recordset.MoveNext
        DoEvents
    Wend
End If

End Sub

Private Sub cmdBillItem_Click()

Dim l_strChargeCode As String, l_rstTrackerChargeCode As ADODB.Recordset, l_strJobLine As String, l_lngJobID As Long, l_lngDuration As Long, l_lngMinuteBilling As Long
Dim l_lngQuantity As Long, l_strCode As String, l_lngFactor As Long, l_blnFirstItem As Boolean, l_blnBulkBilling As Boolean, l_lngLoggingItemCount As Long
Dim l_lngEventID As Long, l_strLastComment As String, l_strLastCommentCuser As String, l_datLastCommentDate As Date, l_blnError As Boolean, l_lngSubsQuantity As Long

l_blnFirstItem = True
m_blnBilling = True
l_blnError = False
If frmJob.txtJobID.Text <> "" Then
    'A job is loaded - now check that it isn't locked.
    l_lngJobID = Val(frmJob.txtJobID.Text)
    'Check that a company has been chosen in the Drop down - we can only add to a job sheet of the correct company
    If cmbCompany.Text <> "" Then
        If frmJob.txtStatus.Text = "Confirmed" And frmJob.lblCompanyID.Caption = lblCompanyID.Caption Then
            'The Job is valid - go ahead and create Job lines for this item.
            m_blnDontVerifyXML = True
            l_lngQuantity = 0
            'l_strJobLine = grdItems.Columns("itemreference").Text
            l_strJobLine = grdItems.Columns("title").Text
            If grdItems.Columns("Series").Text <> "" Then l_strJobLine = l_strJobLine & " - " & grdItems.Columns("series").Text
            If grdItems.Columns("episode").Text <> "" Then l_strJobLine = l_strJobLine & " - Ep " & grdItems.Columns("episode").Text
            If grdItems.Columns("complete").Text <> "" Then
                'DADC Svensk Tracker Billing
                'If it a tape encode - if so we charge for an Encode Process
                'If it is a file submission, we only charge for the Validation process.
                'If it is a fix job, we don't charge validation from the tracker.
                If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") <= 0 Then
                    If grdItems.Columns("barcode").Text <> "" Then
                        'We have a tape
                        If grdItems.Columns("duration").Text <> "" Then
                            l_strCode = "I"
                            l_lngLoggingItemCount = 0
                            l_lngDuration = Val(grdItems.Columns("duration").Text)
                            l_lngQuantity = 1
                            If adoComments.Recordset.RecordCount > 0 Then
                                adoComments.Recordset.MoveLast
                                l_strLastCommentCuser = adoComments.Recordset("cuser")
                                l_datLastCommentDate = adoComments.Recordset("cdate")
                                l_strLastComment = adoComments.Recordset("comment")
                                If adoComments.Recordset("emailclock") <> 0 Then l_strLastComment = l_strLastComment & ", JCA to Replace Clock"
                                If adoComments.Recordset("emailbarsandtone") <> 0 Then l_strLastComment = l_strLastComment & ", JCA to Replace Bars and Tone"
                                If adoComments.Recordset("emailtimecode") <> 0 Then l_strLastComment = l_strLastComment & ", JCA to Correct Timecode"
                                If adoComments.Recordset("emailblackatend") <> 0 Then l_strLastComment = l_strLastComment & ", JCA to correct Black at End"
                            Else
                                l_strLastCommentCuser = ""
                                l_datLastCommentDate = 0
                                l_strLastComment = ""
                            End If
                            If grdItems.Columns("format").Text = "HDCAM-SR" Then
                                l_strChargeCode = "DADCINGESTHDSR"
                            ElseIf grdItems.Columns("format").Text = "HDCAM" Then
                                l_strChargeCode = "DADCINGESTHD"
                            ElseIf grdItems.Columns("format").Text = "C-FORM" Then
                                l_strChargeCode = "DADCINGESTCFORM"
                            ElseIf grdItems.Columns("Format").Text = "D3" Then
                                l_strChargeCode = "DADCINGESTD3"
                            Else
                                l_strChargeCode = "DADCINGESTSD"
                            End If
                            If l_strLastComment <> "" Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, l_strLastComment, l_datLastCommentDate, l_strLastCommentCuser, Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                            Else
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                            End If
                        Else
                            l_blnError = True
                            MsgBox "The current Item must have a duration in order to bill it.", vbCritical, "Cannot Bill Item"
                        End If
                    Else
                        If grdItems.Columns("componenttype").Text <> "" Then
                            'We have file(s)
                            l_strCode = "O"
                            l_lngQuantity = 1
                            l_lngDuration = 0
                            l_lngSubsQuantity = 0
                            If Val(grdItems.Columns("duration").Text) < 30 Then
                                If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD0", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                    Else
                                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK0", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                    End If
                                End If
                            Else
                                If (UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "TRAILER") And grdItems.Columns("validation").Text <> "" Then
                                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK-HD", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                    Else
                                        MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Video and Embedded Audio - " & grdItems.Columns("language").Text, l_lngQuantity, "DADCFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                    End If
                                End If
                            End If
                            If grdItems.Columns("englishvalidation").Text <> "" Then
                                If grdItems.Columns("englishsepaudio").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate English", l_lngQuantity, "DADCAUDFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                End If
                                If grdItems.Columns("englishsubs").Text <> 0 Then l_lngSubsQuantity = l_lngSubsQuantity + 1
                            End If
                            If grdItems.Columns("norwegianvalidation").Text <> "" Then
                                If grdItems.Columns("norwegiansepaudio").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Norwegian", l_lngQuantity, "DADCAUDFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                End If
                                If grdItems.Columns("norwegiansubs").Text <> 0 Then l_lngSubsQuantity = l_lngSubsQuantity + 1
                            End If
                            If grdItems.Columns("swedishvalidation").Text <> "" Then
                                If grdItems.Columns("swedishsepaudio").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Swedish", l_lngQuantity, "DADCAUDFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                End If
                                If grdItems.Columns("swedishsubs").Text <> 0 Then l_lngSubsQuantity = l_lngSubsQuantity + 1
                            End If
                            If grdItems.Columns("danishvalidation").Text <> "" Then
                                If grdItems.Columns("danishsepaudio").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Danish", l_lngQuantity, "DADCAUDFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                End If
                                If grdItems.Columns("danishsubs").Text <> 0 Then l_lngSubsQuantity = l_lngSubsQuantity + 1
                            End If
                            If grdItems.Columns("finnishvalidation").Text <> "" Then
                                If grdItems.Columns("finnishsepaudio").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Finnish", l_lngQuantity, "DADCAUDFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                End If
                                If grdItems.Columns("finnishsubs").Text <> 0 Then l_lngSubsQuantity = l_lngSubsQuantity + 1
                            End If
                            If grdItems.Columns("icelandicvalidation").Text <> "" Then
                                If grdItems.Columns("icelandicsepaudio").Text <> 0 Then
                                    MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Icelandic", l_lngQuantity, "DADCAUDFILECHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                                End If
                                If grdItems.Columns("icelandicsubs").Text <> 0 Then l_lngSubsQuantity = l_lngSubsQuantity + 1
                            End If
                            If l_lngSubsQuantity > 1 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate " & l_lngSubsQuantity & " Subtitle Files", l_lngSubsQuantity, "DADCSUBSCHECK2+", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                            ElseIf l_lngSubsQuantity > 0 Then
                                MakeJobDetailLine l_lngJobID, l_strCode, l_strJobLine & " - Validate Subtitle File", l_lngSubsQuantity, "DADCSUBSCHECK", l_lngDuration, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                            End If
                        Else
                            l_blnError = True
                        End If
                    End If
                End If
                If InStr(GetData("company", "cetaclientcode", "companyID", Val(lblCompanyID.Caption)), "/svenskbillonlylogging") > 0 And grdItems.Columns("xmlmade").Text <> "" Then
                    If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                    Else
                        MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                    End If
                ElseIf grdItems.Columns("xmlmade").Text <> "" Then
                    l_lngLoggingItemCount = 0
                    l_lngEventID = Val(GetDataSQL("SELECT eventID FROM events WHERE clipreference = '" & QuoteSanitise(grdItems.Columns("Itemreference").Text) & "' AND libraryID <> 307744;"))
                    If l_lngEventID <> 0 Then
                        Set l_rstTrackerChargeCode = ExecuteSQL("SELECT Count(eventlogging.eventloggingID) FROM eventlogging WHERE eventID = " & l_lngEventID & ";", g_strExecuteError)
                        CheckForSQLError
                        If l_rstTrackerChargeCode.RecordCount >= 0 Then
                            l_lngLoggingItemCount = l_rstTrackerChargeCode(0)
                        End If
                        l_rstTrackerChargeCode.Close
                        Set l_rstTrackerChargeCode = Nothing
                    Else
                        MsgBox "problem counting logging items for this clip - assuming min 5 items", vbCritical, "Error Billing..."
                        'm_blnBilling = False
                        'Exit Sub
                    End If
                    If l_lngLoggingItemCount > 15 Then
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING25-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING25", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                        End If
                    Else 'If l_lngLoggingItemCount > 4 Then
                        If Val(grdItems.Columns("hdflag").Text) <> 0 Then
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15-HD", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                        Else
                            MakeJobDetailLine l_lngJobID, "O", l_strJobLine, 1, "DADCLOGGING15", 0, Now, g_strUserInitials, frmJob.adoJobDetail.Recordset.RecordCount, 0, "", 0, "", Null, "", Null, Null, Null, Null, grdItems.Columns("ProjectManager").Text, grdItems.Columns("ProjectNumber").Text
                        End If
                    End If
                End If
            End If
            frmJob.adoJobDetail.Refresh
            frmJob.adoJobDetail.Refresh
            If l_blnError = False Then
                grdItems.Columns("billed").Text = -1
                grdItems.Columns("jobID").Text = l_lngJobID
                grdItems.Update
                cmdBillItem.Visible = False
                cmdManualBillItem.Visible = False
            End If
            m_blnDontVerifyXML = False
        Else
            MsgBox "The current Job must belong to the correct company and be confirmed in order to bill an item.", vbCritical, "Cannot Bill Item"
        End If
    Else
        MsgBox "There must be a company chosen in order to do billing.", vbCritical, "Cannot Bill Item"
    End If
Else
    MsgBox "A Job must be created and loaded into the Jobs form in order to bill an item.", vbCritical, "Cannot Bill Item"
End If

m_blnBilling = False

End Sub

Private Sub cmdClear_Click()

ClearFields Me
cmbCompany.Text = ""
lblCompanyID.Caption = ""
lblSearchCompanyID.Caption = ""
m_strSearch = " WHERE 1 = 1 "
cmdSearch.Value = True

grdItems.Columns("ProjectManager").DropDownHwnd = 0

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdCloseAudioValidation_Click()

picValidationAudioFiles.Visible = False

End Sub

Private Sub cmdCloseMasterValidation_Click()

picValidationMasterFiles.Visible = False

End Sub

Private Sub cmdCloseSubsValidation_Click()

picValidationSubs.Visible = False

End Sub

Private Sub cmdClrComments_Click()

optReportComments(0).Value = False
optReportComments(1).Value = False

End Sub

Private Sub cmdDuplicateLineForResupply_Click()

''variable
'Dim l_intBookmark As Integer, l_intTotalBookmarks As Integer, l_varCurrentBookmark As Variant
'Dim l_lngMovementID As Long, l_strInsertSQL As String
'
''store the bookmark count
'l_intTotalBookmarks = grdItems.SelBookmarks.Count
'
''loop through the grid bookmarks
'For l_intBookmark = 0 To l_intTotalBookmarks - 1
'
'    'get the bookmark
'    l_varCurrentBookmark = grdItems.SelBookmarks(l_intBookmark)
'
'    'pick up the costing ID
'    l_lngMovementID = grdItems.Columns("tracker_svensk_itemID").CellText(l_varCurrentBookmark)
'
'
'
'Next
'
'Cancel = True

Dim l_lngNewTrackerID As Long

If Val(lblTrackeritemID.Caption) <> 0 Then
    l_lngNewTrackerID = CreateNewSvenskTrackerLine(Val(lblTrackeritemID.Caption))
    If l_lngNewTrackerID <> 0 Then
        SetData "tracker_svensk_item", "resupply", "tracker_svensk_itemID", l_lngNewTrackerID, 1
        SetData "tracker_svensk_item", "complaintredoitem", "tracker_svensk_itemID", l_lngNewTrackerID, 0
        adoItems.Refresh
        grdItems.Refresh
'        MsgBox ("Updated")
        Me.Refresh
    End If
End If

End Sub

Private Sub cmdFieldsAll_Click()

grdItems.Columns("Rejected Date").Visible = True
grdItems.Columns("offRejectedDate").Visible = True
grdItems.Columns("DaysonRejected").Visible = True
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("fixedjobID").Visible = True
grdItems.Columns("uniqueID").Visible = True
grdItems.Columns("Rightsowner").Visible = True
grdItems.Columns("projectManager").Visible = True
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = True
grdItems.Columns("format").Visible = True
grdItems.Columns("ProjectNumber").Visible = True
grdItems.Columns("ComponentType").Visible = True
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("englishsepaudio").Visible = True
grdItems.Columns("englishsubs").Visible = True
grdItems.Columns("norwegiansepaudio").Visible = True
grdItems.Columns("norwegiansubs").Visible = True
grdItems.Columns("swedishsepaudio").Visible = True
grdItems.Columns("swedishsubs").Visible = True
grdItems.Columns("danishsepaudio").Visible = True
grdItems.Columns("danishsubs").Visible = True
grdItems.Columns("finnishsepaudio").Visible = True
grdItems.Columns("finnishsubs").Visible = True
grdItems.Columns("icelandicsepaudio").Visible = True
grdItems.Columns("icelandicsubs").Visible = True
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = True
grdItems.Columns("filemade").Visible = True
grdItems.Columns("filesent").Visible = True
grdItems.Columns("englisharrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("englishvalidation").Visible = True
grdItems.Columns("englishsubsvalidation").Visible = True
grdItems.Columns("englishsent").Visible = True
grdItems.Columns("englishsubssent").Visible = True
grdItems.Columns("norwegianarrived").Visible = True
grdItems.Columns("norwegiansubsarrived").Visible = True
grdItems.Columns("norwegiansubsvalidation").Visible = True
grdItems.Columns("norwegianvalidation").Visible = True
grdItems.Columns("norwegiansent").Visible = True
grdItems.Columns("norwegiansubssent").Visible = True
grdItems.Columns("swedisharrived").Visible = True
grdItems.Columns("swedishsubsarrived").Visible = True
grdItems.Columns("swedishvalidation").Visible = True
grdItems.Columns("swedishsubsvalidation").Visible = True
grdItems.Columns("swedishsent").Visible = True
grdItems.Columns("swedishsubssent").Visible = True
grdItems.Columns("danisharrived").Visible = True
grdItems.Columns("danishsubsarrived").Visible = True
grdItems.Columns("danishvalidation").Visible = True
grdItems.Columns("danishsubsvalidation").Visible = True
grdItems.Columns("danishsent").Visible = True
grdItems.Columns("danishsubssent").Visible = True
grdItems.Columns("finnisharrived").Visible = True
grdItems.Columns("finnishsubsarrived").Visible = True
grdItems.Columns("finnishvalidation").Visible = True
grdItems.Columns("finnishsubsvalidation").Visible = True
grdItems.Columns("finnishsent").Visible = True
grdItems.Columns("finnishsubssent").Visible = True
grdItems.Columns("icelandicarrived").Visible = True
grdItems.Columns("icelandicsubsarrived").Visible = True
grdItems.Columns("icelandicvalidation").Visible = True
grdItems.Columns("icelandicsubsvalidation").Visible = True
grdItems.Columns("icelandicsent").Visible = True
grdItems.Columns("icelandicsubssent").Visible = True
grdItems.Columns("proxymade").Visible = True
grdItems.Columns("proxysent").Visible = True
grdItems.Columns("xmlmade").Visible = True
grdItems.Columns("xmlsent").Visible = True
grdItems.Columns("resend").Visible = True
grdItems.Columns("tapereturned").Visible = True
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = True
grdItems.Columns("mdate").Visible = True

End Sub

Private Sub cmdFieldsAssets_Click()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = False
grdItems.Columns("Framerate").Visible = False
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("englishsepaudio").Visible = False
grdItems.Columns("englishsubs").Visible = False
grdItems.Columns("norwegiansepaudio").Visible = False
grdItems.Columns("norwegiansubs").Visible = False
grdItems.Columns("swedishsepaudio").Visible = False
grdItems.Columns("swedishsubs").Visible = False
grdItems.Columns("danishsepaudio").Visible = False
grdItems.Columns("danishsubs").Visible = False
grdItems.Columns("finnishsepaudio").Visible = False
grdItems.Columns("finnishsubs").Visible = False
grdItems.Columns("icelandicsepaudio").Visible = False
grdItems.Columns("icelandicsubs").Visible = False
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("englisharrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("englishvalidation").Visible = False
grdItems.Columns("englishsubsvalidation").Visible = False
grdItems.Columns("englishsent").Visible = False
grdItems.Columns("englishsubssent").Visible = False
grdItems.Columns("norwegianarrived").Visible = True
grdItems.Columns("norwegiansubsarrived").Visible = True
grdItems.Columns("norwegiansubsvalidation").Visible = False
grdItems.Columns("norwegianvalidation").Visible = False
grdItems.Columns("norwegiansent").Visible = False
grdItems.Columns("norwegiansubssent").Visible = False
grdItems.Columns("swedisharrived").Visible = True
grdItems.Columns("swedishsubsarrived").Visible = True
grdItems.Columns("swedishvalidation").Visible = False
grdItems.Columns("swedishsubsvalidation").Visible = False
grdItems.Columns("swedishsent").Visible = False
grdItems.Columns("swedishsubssent").Visible = False
grdItems.Columns("danisharrived").Visible = True
grdItems.Columns("danishsubsarrived").Visible = True
grdItems.Columns("danishvalidation").Visible = False
grdItems.Columns("danishsubsvalidation").Visible = False
grdItems.Columns("danishsent").Visible = False
grdItems.Columns("danishsubssent").Visible = False
grdItems.Columns("finnisharrived").Visible = True
grdItems.Columns("finnishsubsarrived").Visible = True
grdItems.Columns("finnishvalidation").Visible = False
grdItems.Columns("finnishsubsvalidation").Visible = False
grdItems.Columns("finnishsent").Visible = False
grdItems.Columns("finnishsubssent").Visible = False
grdItems.Columns("icelandicarrived").Visible = True
grdItems.Columns("icelandicsubsarrived").Visible = True
grdItems.Columns("icelandicvalidation").Visible = False
grdItems.Columns("icelandicsubsvalidation").Visible = False
grdItems.Columns("icelandicsent").Visible = False
grdItems.Columns("icelandicsubssent").Visible = False
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub cmdFieldsAudio_Click()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = False
grdItems.Columns("subtitle").Visible = False
grdItems.Columns("programtype").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = False
grdItems.Columns("englishsepaudio").Visible = True
grdItems.Columns("englishsubs").Visible = True
grdItems.Columns("norwegiansepaudio").Visible = True
grdItems.Columns("norwegiansubs").Visible = True
grdItems.Columns("swedishsepaudio").Visible = True
grdItems.Columns("swedishsubs").Visible = True
grdItems.Columns("danishsepaudio").Visible = True
grdItems.Columns("danishsubs").Visible = True
grdItems.Columns("finnishsepaudio").Visible = True
grdItems.Columns("finnishsubs").Visible = True
grdItems.Columns("icelandicsepaudio").Visible = True
grdItems.Columns("icelandicsubs").Visible = True
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("englisharrived").Visible = True
grdItems.Columns("englishsubsarrived").Visible = True
grdItems.Columns("englishvalidation").Visible = True
grdItems.Columns("englishsubsvalidation").Visible = True
grdItems.Columns("englishsent").Visible = True
grdItems.Columns("englishsubssent").Visible = True
grdItems.Columns("norwegianarrived").Visible = True
grdItems.Columns("norwegiansubsarrived").Visible = True
grdItems.Columns("norwegiansubsvalidation").Visible = True
grdItems.Columns("norwegianvalidation").Visible = True
grdItems.Columns("norwegiansent").Visible = True
grdItems.Columns("norwegiansubssent").Visible = True
grdItems.Columns("swedisharrived").Visible = True
grdItems.Columns("swedishsubsarrived").Visible = True
grdItems.Columns("swedishvalidation").Visible = True
grdItems.Columns("swedishsubsvalidation").Visible = True
grdItems.Columns("swedishsent").Visible = True
grdItems.Columns("swedishsubssent").Visible = True
grdItems.Columns("danisharrived").Visible = True
grdItems.Columns("danishsubsarrived").Visible = True
grdItems.Columns("danishvalidation").Visible = True
grdItems.Columns("danishsubsvalidation").Visible = True
grdItems.Columns("danishsent").Visible = True
grdItems.Columns("danishsubssent").Visible = True
grdItems.Columns("finnisharrived").Visible = True
grdItems.Columns("finnishsubsarrived").Visible = True
grdItems.Columns("finnishvalidation").Visible = True
grdItems.Columns("finnishsubsvalidation").Visible = True
grdItems.Columns("finnishsent").Visible = True
grdItems.Columns("finnishsubssent").Visible = True
grdItems.Columns("icelandicarrived").Visible = True
grdItems.Columns("icelandicsubsarrived").Visible = True
grdItems.Columns("icelandicvalidation").Visible = True
grdItems.Columns("icelandicsubsvalidation").Visible = True
grdItems.Columns("icelandicsent").Visible = True
grdItems.Columns("icelandicsubssent").Visible = True
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub cmdFieldsSummary_Click()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = False
grdItems.Columns("duration").Visible = False
grdItems.Columns("timecodeduration").Visible = False
grdItems.Columns("fixedjobID").Visible = True
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = True
grdItems.Columns("subtitle").Visible = True
grdItems.Columns("programtype").Visible = True
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("englishsepaudio").Visible = False
grdItems.Columns("englishsubs").Visible = False
grdItems.Columns("norwegiansepaudio").Visible = False
grdItems.Columns("norwegiansubs").Visible = False
grdItems.Columns("swedishsepaudio").Visible = False
grdItems.Columns("swedishsubs").Visible = False
grdItems.Columns("danishsepaudio").Visible = False
grdItems.Columns("danishsubs").Visible = False
grdItems.Columns("finnishsepaudio").Visible = False
grdItems.Columns("finnishsubs").Visible = False
grdItems.Columns("icelandicsepaudio").Visible = False
grdItems.Columns("icelandicsubs").Visible = False
grdItems.Columns("masterarrived").Visible = False
grdItems.Columns("validation").Visible = False
grdItems.Columns("filemade").Visible = False
grdItems.Columns("filesent").Visible = False
grdItems.Columns("englisharrived").Visible = False
grdItems.Columns("englishsubsarrived").Visible = False
grdItems.Columns("englishvalidation").Visible = False
grdItems.Columns("englishsubsvalidation").Visible = False
grdItems.Columns("englishsent").Visible = False
grdItems.Columns("englishsubssent").Visible = False
grdItems.Columns("norwegianarrived").Visible = False
grdItems.Columns("norwegiansubsarrived").Visible = False
grdItems.Columns("norwegiansubsvalidation").Visible = False
grdItems.Columns("norwegianvalidation").Visible = False
grdItems.Columns("norwegiansent").Visible = False
grdItems.Columns("norwegiansubssent").Visible = False
grdItems.Columns("swedisharrived").Visible = False
grdItems.Columns("swedishsubsarrived").Visible = False
grdItems.Columns("swedishvalidation").Visible = False
grdItems.Columns("swedishsubsvalidation").Visible = False
grdItems.Columns("swedishsent").Visible = False
grdItems.Columns("swedishsubssent").Visible = False
grdItems.Columns("danisharrived").Visible = False
grdItems.Columns("danishsubsarrived").Visible = False
grdItems.Columns("danishvalidation").Visible = False
grdItems.Columns("danishsubsvalidation").Visible = False
grdItems.Columns("danishsent").Visible = False
grdItems.Columns("danishsubssent").Visible = False
grdItems.Columns("finnisharrived").Visible = False
grdItems.Columns("finnishsubsarrived").Visible = False
grdItems.Columns("finnishvalidation").Visible = False
grdItems.Columns("finnishsubsvalidation").Visible = False
grdItems.Columns("finnishsent").Visible = False
grdItems.Columns("finnishsubssent").Visible = False
grdItems.Columns("icelandicarrived").Visible = False
grdItems.Columns("icelandicsubsarrived").Visible = False
grdItems.Columns("icelandicvalidation").Visible = False
grdItems.Columns("icelandicsubsvalidation").Visible = False
grdItems.Columns("icelandicsent").Visible = False
grdItems.Columns("icelandicsubssent").Visible = False
grdItems.Columns("proxymade").Visible = False
grdItems.Columns("proxysent").Visible = False
grdItems.Columns("xmlmade").Visible = False
grdItems.Columns("xmlsent").Visible = False
grdItems.Columns("resend").Visible = False
grdItems.Columns("tapereturned").Visible = False
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub cmdFieldsVideo_Click()

grdItems.Columns("Rejected Date").Visible = False
grdItems.Columns("offRejectedDate").Visible = False
grdItems.Columns("DaysonRejected").Visible = False
grdItems.Columns("itemreference").Visible = True
grdItems.Columns("duration").Visible = True
grdItems.Columns("timecodeduration").Visible = True
grdItems.Columns("fixedjobID").Visible = False
grdItems.Columns("uniqueID").Visible = False
grdItems.Columns("Rightsowner").Visible = False
grdItems.Columns("projectManager").Visible = False
grdItems.Columns("series").Visible = False
grdItems.Columns("subtitle").Visible = False
grdItems.Columns("programtype").Visible = False
grdItems.Columns("barcode").Visible = False
grdItems.Columns("format").Visible = False
grdItems.Columns("ProjectNumber").Visible = False
grdItems.Columns("ComponentType").Visible = False
grdItems.Columns("RequestedFramerate").Visible = True
grdItems.Columns("Framerate").Visible = True
grdItems.Columns("proxyreq").Visible = True
grdItems.Columns("englishsepaudio").Visible = False
grdItems.Columns("englishsubs").Visible = False
grdItems.Columns("norwegiansepaudio").Visible = False
grdItems.Columns("norwegiansubs").Visible = False
grdItems.Columns("swedishsepaudio").Visible = False
grdItems.Columns("swedishsubs").Visible = False
grdItems.Columns("danishsepaudio").Visible = False
grdItems.Columns("danishsubs").Visible = False
grdItems.Columns("finnishsepaudio").Visible = False
grdItems.Columns("finnishsubs").Visible = False
grdItems.Columns("icelandicsepaudio").Visible = False
grdItems.Columns("icelandicsubs").Visible = False
grdItems.Columns("masterarrived").Visible = True
grdItems.Columns("validation").Visible = True
grdItems.Columns("filemade").Visible = True
grdItems.Columns("filesent").Visible = True
grdItems.Columns("englisharrived").Visible = False
grdItems.Columns("englishsubsarrived").Visible = False
grdItems.Columns("englishvalidation").Visible = False
grdItems.Columns("englishsubsvalidation").Visible = False
grdItems.Columns("englishsent").Visible = False
grdItems.Columns("englishsubssent").Visible = False
grdItems.Columns("norwegianarrived").Visible = False
grdItems.Columns("norwegiansubsarrived").Visible = False
grdItems.Columns("norwegiansubsvalidation").Visible = False
grdItems.Columns("norwegianvalidation").Visible = False
grdItems.Columns("norwegiansent").Visible = False
grdItems.Columns("norwegiansubssent").Visible = False
grdItems.Columns("swedisharrived").Visible = False
grdItems.Columns("swedishsubsarrived").Visible = False
grdItems.Columns("swedishvalidation").Visible = False
grdItems.Columns("swedishsubsvalidation").Visible = False
grdItems.Columns("swedishsent").Visible = False
grdItems.Columns("swedishsubssent").Visible = False
grdItems.Columns("danisharrived").Visible = False
grdItems.Columns("danishsubsarrived").Visible = False
grdItems.Columns("danishvalidation").Visible = False
grdItems.Columns("danishsubsvalidation").Visible = False
grdItems.Columns("danishsent").Visible = False
grdItems.Columns("danishsubssent").Visible = False
grdItems.Columns("finnisharrived").Visible = False
grdItems.Columns("finnishsubsarrived").Visible = False
grdItems.Columns("finnishvalidation").Visible = False
grdItems.Columns("finnishsubsvalidation").Visible = False
grdItems.Columns("finnishsent").Visible = False
grdItems.Columns("finnishsubssent").Visible = False
grdItems.Columns("icelandicarrived").Visible = False
grdItems.Columns("icelandicsubsarrived").Visible = False
grdItems.Columns("icelandicvalidation").Visible = False
grdItems.Columns("icelandicsubsvalidation").Visible = False
grdItems.Columns("icelandicsent").Visible = False
grdItems.Columns("icelandicsubssent").Visible = False
grdItems.Columns("proxymade").Visible = True
grdItems.Columns("proxysent").Visible = True
grdItems.Columns("xmlmade").Visible = True
grdItems.Columns("xmlsent").Visible = True
grdItems.Columns("resend").Visible = True
grdItems.Columns("tapereturned").Visible = True
grdItems.Columns("complete").Visible = True
grdItems.Columns("cdate").Visible = False
grdItems.Columns("mdate").Visible = False

End Sub

Private Sub cmdGenerateSFUniqueIDs_Click()

Dim l_strSFUniqueID As String

adoItems.Recordset.MoveFirst

While Not adoItems.Recordset.EOF
    If Trim(" " & adoItems.Recordset("projectnumber")) <> "" Then
        adoItems.Recordset("uniqueID") = adoItems.Recordset("projectnumber") & "_" & adoItems.Recordset("title") & "_" & adoItems.Recordset("programtype") & "_" & Format(Val(Trim(" " & adoItems.Recordset("series"))), "00") & "_" & Format(Val(Trim(" " & adoItems.Recordset("episode"))), "00")
        adoItems.Recordset.Update
    End If
    adoItems.Recordset.MoveNext
Wend

End Sub

Private Sub cmdMakeFixJobsheet_Click()

Dim l_strSQL As String, l_lngJobID As Long

If Val(lblTrackeritemID.Caption) <> 0 Then

    l_strSQL = "INSERT INTO job (createduserID, createduser, createddate, modifieddate, modifieduserID, modifieduser, fd_status, jobtype, joballocation, deadlinedate, despatchdate, projectID, productID, companyID, companyname) VALUES ("
    
    l_strSQL = l_strSQL & g_lngUserID & ", "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & g_lngUserID & ","
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "'Confirmed', "
    l_strSQL = l_strSQL & "'Dubbing', "
    l_strSQL = l_strSQL & "'Regular', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 2, Now)) & "', "
    l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 2, Now)) & "', "
    l_strSQL = l_strSQL & "0, "
    l_strSQL = l_strSQL & "0, "
    l_strSQL = l_strSQL & "1330, "
    l_strSQL = l_strSQL & "'Sony DADC Ltd (Svensk Fixes)'); "
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngJobID = g_lngLastID
    
    l_strSQL = grdItems.Columns("title").Text & " - " & grdItems.Columns("programtype").Text
    SetData "job", "title1", "jobID", l_lngJobID, l_strSQL
    l_strSQL = grdItems.Columns("language").Text
    SetData "job", "title2", "jobID", l_lngJobID, l_strSQL
    l_strSQL = grdItems.Columns("projectnumber").Text & " - " & grdItems.Columns("projectmanager").Text
    SetData "job", "orderreference", "jobID", l_lngJobID, l_strSQL
    SetData "job", "contactID", "jobID", l_lngJobID, 3356
    SetData "job", "contactname", "jobID", l_lngJobID, "Hilary Weeks"
    
    grdItems.Columns("fixedjobID").Text = l_lngJobID
    grdItems.Update
    
    ShowJob l_lngJobID, 1, True

End If

End Sub

Private Sub cmdManualBillItem_Click()

grdItems.Columns("billed").Text = -1
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdMasterValidationSave_Click()

Dim tempdate As Date, l_strSQL As String

If optMasterValidationComplete(0).Value = True Then
    tempdate = FormatDateTime(Now, vbLongDate)
    grdItems.Columns("Validation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
Else
    grdItems.Columns("Validation").Text = ""
End If

If optVideoInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

SetData "tracker_svensk_item", "framerate", "tracker_svensk_itemID", Val(lblTrackeritemID), cmbFrameRate.Text

If optHDFlag(0).Value = True Then
    SetData "tracker_svensk_item", "hdflag", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "hdflag", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optTimecodeInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "TimecodeCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "TimecodeCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optBarsAndToneInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "BarsAndToneCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "BarsAndToneCorrect", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optMainStereoPresent(0).Value = True Then
    SetData "tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optMEStereoPresent(0).Value = True Then
    SetData "tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optSurroundPresent(0).Value = True Then
    SetData "tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optTextlessPresent(0).Value = True Then
    SetData "tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optBlackAtEnd(0).Value = True Then
    SetData "tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

If optTextlessSeparator(0).Value = True Then
    SetData "tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", Val(lblTrackeritemID), 1
Else
    SetData "tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", Val(lblTrackeritemID), 0
End If

l_strSQL = "INSERT INTO tracker_svensk_validation (tracker_svensk_itemID, ValidationDate, ValidatedBy) VALUES ("
l_strSQL = l_strSQL & Val(lblTrackeritemID.Caption) & ", "
l_strSQL = l_strSQL & "getdate(), "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdCloseMasterValidation.Value = True

End Sub

Private Sub cmdPendingOff_Click()
    
    If MsgBox("You are about to bulk update selected records to Off Pending." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub

End Sub

Private Sub cmdPendingOn_Click()

    If MsgBox("You are about to bulk update selected records to On Pending." & vbCrLf & "Are you sure?", vbYesNo, "Confirm") = vbNo Then Exit Sub

End Sub

Private Sub cmdPrint_Click()

Dim l_strSQL As String

If lblCompanyID.Caption <> "" Then
    l_strSQL = adoItems.RecordSource
    PrintCrystalReportUsingCleanSQL g_strLocationOfCrystalReportFiles & "GenericTrackerReport.rpt", l_strSQL, True
End If

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

If lblSearchCompanyID.Caption = "" Then
    MsgBox "Please Select a Company Before running the report", vbCritical, "Error..."
    Exit Sub
End If

If optComplete(0).Value = True Then 'not complete
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) "
ElseIf optComplete(1).Value = True Then 'Pending
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) or {tracker_svensk_item.readytobill} = 0) and {tracker_svensk_item.rejected} <> 0"
ElseIf optComplete(2).Value = True Then 'Finished
    l_strSelectionFormula = "{tracker_svensk_item.readytobilled} <> 0 and {tracker_svensk_item.billed} = 0"
ElseIf optComplete(3).Value = True Then 'Billed
    l_strSelectionFormula = "{tracker_svensk_item.readytobilled} <> 0 and {tracker_svensk_item.billed} <> 0"
ElseIf optComplete(4).Value = True Then 'All Items
    l_strSelectionFormula = "1 = 1"
ElseIf optComplete(5).Value = True Then 'File Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filemade} <> 0"
ElseIf optComplete(6).Value = True Then 'File made but not sent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filemade} <> 0 and (isnull({tracker_svensk_item.filesent}) OR {tracker_svensk_item.filesent} = 0)"
ElseIf optComplete(7).Value = True Then 'File Not Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND (isnull({tracker_svensk_item.filemade}) Or {tracker_svensk_item.filemade} = 0)"
ElseIf optComplete(8).Value = True Then 'xml Made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlmade} <> 0"
ElseIf optComplete(9).Value = True Then 'xml Made but not sent
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlmade} <> 0 and (isnull({tracker_svensk_item.xmlsent}) OR {tracker_svensk_item.xmlsent} = 0)"
ElseIf optComplete(10).Value = True Then 'xml not made
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND (isnull({tracker_svensk_item.xmlmade}) or {tracker_svensk_item.xmlmade} = 0)"
ElseIf optComplete(11).Value = True Then 'Sent with xml
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filesent} <> 0 AND {tracker_svensk_item.xmlsent} <> 0"
ElseIf optComplete(12).Value = True Then 'Sent without xml
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.filesent} <> 0 AND (isnull({tracker_svensk_item.xmlsent}) or {tracker_svensk_item.xmlsent} = 0)"
ElseIf optComplete(13).Value = True Then 'xml sent without File
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.xmlsent} <> 0 AND (isnull({tracker_svensk_item.filesent}) or {tracker_svensk_item.filesent} = 0)"
ElseIf optComplete(14).Value = True Then 'Tape Not Here
    l_strSelectionFormula = "(isnull({tracker_svensk_item.masterarrived}) or (not isnull({tracker_svensk_item.masterarrived}) AND (not isnull({tracker_svensk_item.tapereturned})))"
ElseIf optComplete(15).Value = True Then 'Tape Sent Back
    l_strSelectionFormula = "(not isnull({tracker_svensk_item.tapereturned})"
ElseIf optComplete(16).Value = True Then 'Decision Tree
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.decisiontree} <> 0"
ElseIf optComplete(17).Value = True Then 'Workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND {tracker_svensk_item.rejected} = 0 AND {tracker_svensk_item.assetshere} <> 0 "
ElseIf optComplete(18).Value = True Then 'Not workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.rejected} <> 0 OR {tracker_svensk_item.assetshere} = 0) "
ElseIf optComplete(19).Value = True Then 'Not workable
    l_strSelectionFormula = "(isnull({tracker_svensk_item.readytobill}) OR {tracker_svensk_item.readytobill} = 0) AND ({tracker_svensk_item.urgent} <> 0) "
End If

If txtReference.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.itemreference} LIKE '" & txtReference.Text & "*' "
End If

If txtTitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.title} LIKE '" & txtTitle.Text & "*' "
End If
If txtSubtitle.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.subtitle} LIKE '" & txtSubtitle.Text & "*' "
End If
If cmbRightsOwner.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.rightsowner} LIKE '" & cmbRightsOwner.Text & "*' "
End If
If cmbProgramType.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.programtype}) AND {tracker_svensk_item.programtype} like '" & cmbProgramType.Text & "*') "
End If
If txtSeries.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.series}) AND {tracker_svensk_item.series} like '" & txtSeries.Text & "*') "
End If
If cmbFormat.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.format} LIKE '" & cmbFormat.Text & "*' "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.episode} = " & Val(txtEpisode.Text) & " "
End If
If cmbComponentType.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.componenttype}) AND {tracker_svensk_item.componenttype} like '" & cmbComponentType.Text & "*') "
End If
If txtSpecialProject.Text <> "" Then
    l_strSelectionFormula = l_strSelectionFormula & " AND (not isnull({tracker_svensk_item.SpecialProject}) AND {tracker_svensk_item.SpecialProject} like '" & txtSpecialProject.Text & "*') "
End If

l_strSelectionFormula = l_strSelectionFormula & " AND {tracker_svensk_item.companyID} = " & Val(lblSearchCompanyID.Caption) & " "
    
If optReportComments(0).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report_lastcommentonly.rpt"
ElseIf optReportComments(1).Value = True Then
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report_firstcommentonly.rpt"
Else
    l_strReportFile = g_strLocationOfCrystalReportFiles & "Svensk_report.rpt"
End If

Debug.Print l_strSelectionFormula
'MsgBox l_strSelectionFormula, vbInformation
PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String

l_strSQL = ""

If chkHideDemo.Value <> 0 Then
    l_strSQL = l_strSQL & " AND companyID > 100 "
End If

If optComplete(0).Value = True Then 'Not Complete
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) "
ElseIf optComplete(1).Value = True Then 'Pending
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND rejected <> 0 "
ElseIf optComplete(2).Value = True Then 'Complete
    l_strSQL = l_strSQL & "AND readytobill <> 0  AND billed = 0 "
ElseIf optComplete(3).Value = True Then 'Billed
    l_strSQL = l_strSQL & "AND readytobill <> 0 AND billed <> 0 "
ElseIf optComplete(4).Value = True Then
    l_strSQL = l_strSQL & "AND 1=1 "
ElseIf optComplete(5).Value = True Then 'File Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filemade <> 0 "
ElseIf optComplete(6).Value = True Then 'File Made but not sent
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filemade <> 0 AND (filesent IS NULL or filesent = 0) "
ElseIf optComplete(7).Value = True Then 'File Not Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (filemade IS NULL OR filemade = 0) "
ElseIf optComplete(8).Value = True Then 'XML Made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (xmlmade <> 0) "
ElseIf optComplete(9).Value = True Then 'XML Made but not sent
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND xmlmade <> 0 AND (xmlsent IS NULL or xmlsent = 0) "
ElseIf optComplete(10).Value = True Then ' XML not made
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (xmlmade IS NULL OR xmlmade = 0) "
ElseIf optComplete(11).Value = True Then 'Sent with XML
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filesent <> 0 AND xmlsent <> 0 "
ElseIf optComplete(12).Value = True Then ' Sent without xml
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND filesent <> 0 AND (xmlsent IS NULL or xmlsent = 0) "
ElseIf optComplete(13).Value = True Then 'Sent xml without file
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (filesent IS NULL or filesent = 0) AND (xmlsent <> 0) "
ElseIf optComplete(14).Value = True Then 'Tape Not Here
    l_strSQL = l_strSQL & "AND (masterarrived IS NULL OR (masterarrived IS NOT NULL AND tapereturned IS NOT NULL)) "
ElseIf optComplete(15).Value = True Then 'Tape sent back
    l_strSQL = l_strSQL & "AND tapereturned IS NOT NULL "
ElseIf optComplete(16).Value = True Then 'Decision Tree
    l_strSQL = l_strSQL & " AND (readytobill IS NULL OR readytobill = 0) AND decisiontree <> 0  "
ElseIf optComplete(17).Value = True Then 'Workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND rejected = 0 AND assetshere <> 0 "
ElseIf optComplete(18).Value = True Then 'Not workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (rejected <> 0 OR assetshere = 0) "
ElseIf optComplete(19).Value = True Then 'Not workable
    l_strSQL = l_strSQL & "AND (readytobill IS NULL OR readytobill = 0) AND (urgent <> 0) "
End If

If txtReference.Text <> "" Then
    l_strSQL = l_strSQL & " AND itemreference LIKE '" & QuoteSanitise(txtReference.Text) & "%' "
End If
If txtTitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND title LIKE '" & QuoteSanitise(txtTitle.Text) & "%' "
End If
If txtSubtitle.Text <> "" Then
    l_strSQL = l_strSQL & " AND subtitle LIKE '" & QuoteSanitise(txtSubtitle.Text) & "%' "
End If
If cmbRightsOwner.Text <> "" Then
    l_strSQL = l_strSQL & " AND rightsowner LIKE '" & QuoteSanitise(cmbRightsOwner.Text) & "%' "
End If
If cmbProgramType.Text <> "" Then
    l_strSQL = l_strSQL & " AND (programtype IS NOT NULL AND programtype like '" & cmbProgramType.Text & "%') "
End If
If txtSeries.Text <> "" Then
    l_strSQL = l_strSQL & " AND (series IS NOT NULL AND series like '" & txtSeries.Text & "%') "
End If
If Val(txtEpisode.Text) <> 0 Then
    l_strSQL = l_strSQL & " AND (episode IS NOT NULL AND episode = " & Val(txtEpisode.Text) & ") "
End If
If cmbFormat.Text <> "" Then
    l_strSQL = l_strSQL & " AND (format IS NOT NULL AND series = '" & cmbFormat.Text & "') "
End If
If cmbComponentType.Text <> "" Then
    l_strSQL = l_strSQL & " AND (componenttype IS NOT NULL AND componenttype like '" & cmbComponentType.Text & "%') "
End If
If cmbProjectManager.Text <> "" Then
    l_strSQL = l_strSQL & " AND (projectmanager IS NOT NULL AND projectmanager like '" & cmbProjectManager.Text & "%') "
End If
If txtSpecialProject.Text <> "" Then
    l_strSQL = l_strSQL & " AND (SpecialProject IS NOT NULL AND SpecialProject like '" & QuoteSanitise(txtSpecialProject.Text) & "%') "
End If


If Val(lblSearchCompanyID.Caption) <> 0 Then
    l_strSQL = l_strSQL & " AND companyID = " & Val(lblSearchCompanyID.Caption) & " "
End If

m_blnSilent = True
adoItems.ConnectionString = g_strConnection
adoItems.RecordSource = "SELECT * FROM tracker_Svensk_item " & m_strSearch & l_strSQL & m_strOrderby & ";"
adoItems.Refresh

adoItems.Caption = adoItems.Recordset.RecordCount & " item(s)"
m_blnSilent = False

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveLast
    adoItems.Recordset.MoveFirst
End If

End Sub

Private Sub cmdSubsValidationSave_Click()

Dim tempdate As Date, l_strSQL As String

If optSubsValidationComplete(0).Value = True Then
    tempdate = FormatDateTime(Now, vbLongDate)
    If grdItems.Columns("englishsubs").Text <> 0 Then
        grdItems.Columns("englishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("norwegiansubs").Text <> 0 Then
        grdItems.Columns("norwegiansubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("swedishsubs").Text <> 0 Then
        grdItems.Columns("swedishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("danishsubs").Text <> 0 Then
        grdItems.Columns("danishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("finnishsubs").Text <> 0 Then
        grdItems.Columns("finnishsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
    If grdItems.Columns("icelandicsubs").Text <> 0 Then
        grdItems.Columns("icelandicsubsvalidation").Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
Else
    grdItems.Columns("englishsubsvalidation").Text = ""
    grdItems.Columns("norwegiansubsvalidation").Text = ""
    grdItems.Columns("swedishsubsvalidation").Text = ""
    grdItems.Columns("danishsubsvalidation").Text = ""
    grdItems.Columns("finnishsubsvalidation").Text = ""
    grdItems.Columns("icelandicsubsvalidation").Text = ""
End If

If optEnglishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optEnglishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optNorwegianSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "NorwegianSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optNorwegianSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "NorwegianSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "NorwegianSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optSwedishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "SwedishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optSwedishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "SwedishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "SwedishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optDanishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "DanishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optDanishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "DanishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "DanishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optFinnishSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "FinnishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optFinnishSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "FinnishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "FinnishSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

If optIcelandicSubsInSpec(0).Value = True Then
    SetData "tracker_svensk_item", "IcelandicSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 0
ElseIf optIcelandicSubsInSpec(1).Value = True Then
    SetData "tracker_svensk_item", "IcelandicSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 1
Else
    SetData "tracker_svensk_item", "IcelandicSubsInSpec", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption), 2
End If

l_strSQL = "INSERT INTO tracker_svensk_validation (tracker_svensk_itemID, ValidationDate, ValidatedBy) VALUES ("
l_strSQL = l_strSQL & Val(lblTrackeritemID.Caption) & ", "
l_strSQL = l_strSQL & "getdate(), "
l_strSQL = l_strSQL & "'" & g_strFullUserName & "');"

ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

cmdCloseSubsValidation.Value = True

End Sub

Private Sub cmdUnbill_Click()

grdItems.Columns("billed").Text = 0
grdItems.Columns("jobid").Text = 0
grdItems.Update
cmdBillItem.Visible = False
cmdManualBillItem.Visible = False

End Sub

Private Sub cmdUnbillAll_Click()

adoItems.Recordset.MoveFirst

If Not adoItems.Recordset.EOF Then

    While Not adoItems.Recordset.EOF
        adoItems.Recordset("billed") = 0
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Wend
End If

grdItems.Refresh

End Sub

Private Sub cmdUpdateAll_Click()

If adoItems.Recordset.RecordCount > 0 Then

    adoItems.Recordset.MoveFirst
    While Not adoItems.Recordset.EOF
        adoItems.Recordset("itemreference") = adoItems.Recordset("itemreference") & " "
        adoItems.Recordset("itemreference") = Trim(adoItems.Recordset("itemreference"))
        adoItems.Recordset.Update
        adoItems.Recordset.MoveNext
    Wend
    adoItems.Recordset.MoveFirst
End If

End Sub



Private Sub ddnChannel_CloseUp()

grdItems.Columns("companyID").Text = ddnChannel.Columns("companyID").Text
grdItems.Columns("channel").Text = ddnChannel.Columns("name").Text

End Sub

Private Sub ddnProjectManager_CloseUp()

grdItems.Columns("projectmanagercontactID").Text = ddnProjectManager.Columns("contactID").Text

End Sub

Private Sub Form_Load()

Dim l_strSQL As String

Dim l_blnWide As Boolean

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch1 As ADODB.Recordset
Dim l_rstSearch2 As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch1 = New ADODB.Recordset
Set l_rstSearch2 = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

If chkHideDemo.Value <> 0 Then
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' and companyID > 100 ORDER BY name;"
Else
    l_strSQL = "SELECT portalcompanyname, companyID FROM company WHERE cetaclientcode like '%/Svensktracker%' ORDER BY name;"
End If

With l_rstSearch1
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch1.ActiveConnection = Nothing

Set cmbCompany.DataSourceList = l_rstSearch1

With l_rstSearch2
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch2.ActiveConnection = Nothing

Set ddnChannel.DataSource = l_rstSearch2
grdItems.Columns("channel").DropDownHwnd = ddnChannel.hWnd

l_conSearch.Close
Set l_conSearch = Nothing

adoComments.ConnectionString = g_strConnection

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

grdItems.StyleSets.Add "NotRequired"
grdItems.StyleSets("NotRequired").BackColor = &H10101

grdItems.StyleSets.Add "Priority"
grdItems.StyleSets("Priority").BackColor = &H70B0FF

grdItems.StyleSets.Add "Fixed"
grdItems.StyleSets("Fixed").BackColor = &HA0FFA0
grdItems.StyleSets.Add "Notify"
grdItems.StyleSets("Notify").BackColor = &HFFFF40
optComplete(0).Value = True

m_strSearch = " WHERE 1 = 1 "
m_strOrderby = " ORDER BY urgent, duedate, title;"
m_blnSilent = False

PopulateCombo "trackercommenttypes", ddnCommentTypes
grdComments.Columns("commenttypedecoded").DropDownHwnd = ddnCommentTypes.hWnd

HideAllColumns

DoEvents

cmdSearch.Value = True
cmdFieldsSummary.Value = True

End Sub

Private Sub Form_Resize()

On Error Resume Next

grdItems.width = Me.ScaleWidth - grdItems.Left - 120
grdItems.height = (Me.ScaleHeight - grdItems.Top - frmButtons.height) * 0.75 - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.width - 120
grdComments.Top = grdItems.Top + grdItems.height + 120
grdComments.height = frmButtons.Top - grdComments.Top - 120
grdComments.width = grdItems.width

End Sub

Private Sub grdComments_AfterDelete(RtnDispErrMsg As Integer)

m_blnDelete = False

End Sub

Private Sub grdComments_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)

m_blnDelete = True

End Sub

Private Sub grdComments_BeforeUpdate(Cancel As Integer)

If m_blnDelete = False Then
    If grdComments.Columns("comment").Text = "" Then
        MsgBox "Cannot Save a Comment with no actual comment", vbCritical, "Comment Not Saved"
        Cancel = True
        Exit Sub
    End If
    
    
    
    grdComments.Columns("tracker_Svensk_itemID").Text = lblTrackeritemID.Caption
    If grdComments.Columns("cdate").Text = "" Then
        grdComments.Columns("cdate").Text = Now
    End If
    grdComments.Columns("cuser").Text = g_strFullUserName
    
    If grdComments.Columns("commenttypedecoded").Text = "" Then grdComments.Columns("commenttypedecoded").Text = "General"
    grdComments.Columns("commenttype").Text = GetDataSQL("SELECT information FROM xref WHERE category = 'trackercommenttypes' AND description = '" & grdComments.Columns("commenttypedecoded").Text & "';")
        
    If (grdItems.Columns("decisiontree").Text <> 0 And Val(grdComments.Columns("commenttype").Text) <> 1) Or (grdItems.Columns("rejected").Text <> 0 And Val(grdComments.Columns("commenttype").Text) <> 0) _
    Or (grdItems.Columns("decisiontree").Text = 0 And grdItems.Columns("rejected").Text = 0 And Val(grdComments.Columns("commenttype").Text) <> 2) Then
        If MsgBox("Comment type does not match up with Tracker item Decision Tree status." & vbCrLf & "Are you Sure", vbYesNo, "Please Confirm Decision Tree Comment Status") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If

    If grdComments.Columns("commenttype").Text = 0 And grdComments.Columns("comment").Text <> "" Then
    
        If MsgBox("If the issue requires fixing, have you included details of what needs doing?" & vbCrLf, vbYesNo, "Please Confirm you know how to fix") = vbNo Then
            Cancel = True
            Exit Sub
        End If
    End If

End If


End Sub

Private Sub grdComments_BtnClick()

Dim l_strOurEmailContact As String, l_strOurContactName As String, l_strEmailBody As String

Dim l_rstWhoToEmail As ADODB.Recordset

If MsgBox("Send Email?", vbYesNo, "Automatic Email") = vbYes Then
    
    l_strEmailBody = "A comment was created " & _
        "By: " & grdComments.Columns("cuser").Text & vbCrLf & _
        "About: " & grdItems.Columns("itemreference").Text & vbCrLf & _
        "Title: " & grdItems.Columns("title").Text & vbCrLf & _
        "Series: " & grdItems.Columns("series").Text & vbCrLf & _
        "Episode #: " & grdItems.Columns("episode").Text & vbCrLf & _
        "Language: " & grdItems.Columns("Language").Text & vbCrLf & _
        "Comment: " & grdComments.Columns("comment").Text & vbCrLf
        If Val(grdComments.Columns("Email - Clock?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will replace Clock." & vbCrLf
        If Val(grdComments.Columns("Email - B&T?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will replace Bars and Tone." & vbCrLf
        If Val(grdComments.Columns("Email - T/C?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will re-stripe the Timecode." & vbCrLf
        If Val(grdComments.Columns("Email - Black at End?").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will add correct Black at End." & vbCrLf
        If Val(grdComments.Columns("Email - Textless Not Rq").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "Textless Elements Present but not Requested" & vbCrLf & "RRsat will take the Textless Elements." & vbCrLf
        If Val(grdComments.Columns("emailblackonwhitetexstless").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "White on Black Textless Elements." & vbCrLf
        If Val(grdComments.Columns("emailtracklayoutwrong").Text) <> 0 Then l_strEmailBody = l_strEmailBody & "RRsat will Fix Track Layout." & vbCrLf
        
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", grdItems.Columns("companyID").Text) & "' AND trackermessageID = 32;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Svensk Tracker Comment Created", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
    
End If

End Sub

Private Sub grdComments_RowLoaded(ByVal Bookmark As Variant)

If grdComments.Columns("commenttype").Text = 0 Then
    grdComments.Columns("Commenttypedecoded").Text = "Pending"
ElseIf grdComments.Columns("Commenttype").Text = 2 Then
    grdComments.Columns("Commenttypedecoded").Text = "General"
Else
    grdComments.Columns("Commenttypedecoded").Text = "Decision Tree"
End If



End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)
m_blnDelete = False
End Sub

Private Sub grdItems_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
m_blnDelete = True
End Sub

Private Sub grdItems_BeforeRowColChange(Cancel As Integer)

If picValidationMasterFiles.Visible = True Then
    cmdMasterValidationSave.Value = True
    picValidationMasterFiles.Visible = False
End If

If picValidationAudioFiles.Visible = True Then
    cmdAudioValidationSave.Value = True
    picValidationAudioFiles.Visible = False
End If

If picValidationSubs.Visible = True Then
    cmdSubsValidationSave.Value = True
    picValidationSubs.Visible = False
End If

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If m_blnDelete = True Then Exit Sub

If grdItems.Columns("companyID").Text = "" Then

    MsgBox "Please select an RRsat Client." & vbCrLf & "Row not saved", vbCritical, "Error..."
    Cancel = 1
    Exit Sub
End If

Dim temp As Boolean, l_strDuration As String, l_lngRunningTime As Long, l_curFileSize As Currency, l_strFilename As String, l_rst As ADODB.Recordset
Dim l_lngClipID As Long, l_strNetworkPath As String, l_strSQL As String, l_lngProjectManagerContactID As Long
Dim l_datRejectDateNow As Date
Dim l_datRejectDateCur As Date
Dim l_datMostRecentdDateCur As Date

'Update the UniqueID

'grdItems.Columns("uniqueID").Text = grdItems.Columns("projectnumber").Text & "_" & grdItems.Columns("title").Text & "_" & grdItems.Columns("programtype").Text & "_" & Format(Val(grdItems.Columns("series").Text), "00") & "_" & Format(Val(grdItems.Columns("episode").Text), "00")

'Check ReadytoBill - setting temp to false if any items are not ready to bill
If grdItems.Columns("complete").Text <> "" Then
    grdItems.Columns("readytobill").Text = 1
    If grdItems.Columns("proxyreq").Text <> 0 Then
        If grdItems.Columns("proxymade").Text = "" Then
            If MsgBox("Please confirm that you are about to mark a row as complete when the proxy does not appear to have been done yet." & vbCrLf & "Do you wish to continue?", vbYesNo, "Cancel") = vbNo Then
                Cancel = 1
                Exit Sub
            End If
        End If
    End If
Else
    grdItems.Columns("readytobill").Text = 0
End If

'Check if Project Manager has changed, and if so, look up the new contactID and enter it in the hidden field
If Val(lblTrackeritemID.Caption) <> 0 Then
'    If grdItems.Columns("projectmanager").Text <> GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", Val(lblTrackeritemID.Caption)) Then
        l_lngProjectManagerContactID = Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & grdItems.Columns("projectmanager").Text & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = " & Val(lblCompanyID.Caption) & ");")))
        If l_lngProjectManagerContactID <> 0 Then
            grdItems.Columns("projectmanagercontactID").Text = l_lngProjectManagerContactID
        Else
            MsgBox "You have typed or chosen a Project Manager who is not listed as a contact for that client." & vbCrLf & "Line not saved", vbCritical, "Error..."
            Cancel = 1
            Exit Sub
        End If
'    End If
End If

If grdItems.Columns("cdate").Text = "" Then grdItems.Columns("cdate").Text = Now
grdItems.Columns("mdate").Text = Now
l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser) VALUES (" & Val(lblTrackeritemID.Caption) & ", getdate(), '" & g_strFullUserName & "');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

temp = True
If ((UCase(grdItems.Columns("componenttype").Text) = "VIDEO" Or UCase(grdItems.Columns("componenttype").Text) = "AUDIO") And grdItems.Columns("masterarrived").Text = "") Then temp = False
If Val(grdItems.Columns("englishsepaudio").Text) <> 0 And grdItems.Columns("englisharrived").Text = "" Then temp = False
If Val(grdItems.Columns("englishsubs").Text) <> 0 And grdItems.Columns("englishsubsarrived").Text = "" Then temp = False
If Val(grdItems.Columns("norwegiansepaudio").Text) <> 0 And grdItems.Columns("norwegianarrived").Text = "" Then temp = False
If Val(grdItems.Columns("norwegiansubs").Text) <> 0 And grdItems.Columns("norwegiansubsarrived").Text = "" Then temp = False
If Val(grdItems.Columns("swedishsepaudio").Text) <> 0 And grdItems.Columns("swedisharrived").Text = "" Then temp = False
If Val(grdItems.Columns("swedishsubs").Text) <> 0 And grdItems.Columns("swedishsubsarrived").Text = "" Then temp = False
If Val(grdItems.Columns("danishsepaudio").Text) <> 0 And grdItems.Columns("danisharrived").Text = "" Then temp = False
If Val(grdItems.Columns("danishsubs").Text) <> 0 And grdItems.Columns("danishsubsarrived").Text = "" Then temp = False
If Val(grdItems.Columns("finnishsepaudio").Text) <> 0 And grdItems.Columns("finnisharrived").Text = "" Then temp = False
If Val(grdItems.Columns("finnishsubs").Text) <> 0 And grdItems.Columns("finnishsubsarrived").Text = "" Then temp = False
If Val(grdItems.Columns("icelandicsepaudio").Text) <> 0 And grdItems.Columns("icelandicarrived").Text = "" Then temp = False
If Val(grdItems.Columns("icelandicsubs").Text) <> 0 And grdItems.Columns("icelandicsubsarrived").Text = "" Then temp = False
    
grdItems.Columns("assetshere").Text = CLng(temp)

'Check the Pending situation
If Val(grdItems.Columns("rejected").Text) <> 0 And (IsNull(grdItems.Columns("rejecteddate").Text) Or grdItems.Columns("rejecteddate").Text = "") Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("rejecteddate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("MostRecentRejectedDate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")
      
ElseIf Val(grdItems.Columns("rejected").Text) <> 0 And adoItems.Recordset("rejected") = 0 Then
     
    grdItems.Columns("MostRecentRejectedDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("rejected").Text) = 0 And adoItems.Recordset("rejected") <> 0 Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("offRejectedDate").Text = l_datRejectDateNow

    l_datRejectDateCur = FormatDateTime(grdItems.Columns("rejecteddate").Text, vbShortDate)
    l_datMostRecentdDateCur = FormatDateTime(grdItems.Columns("MostRecentRejectedDate").Text, vbShortDate)

'    If l_datRejectDateCur = l_datMostRecentdDateCur Then 'rejectdatenow Then
'        grdItems.Columns("daysonRejected").Text = 1
'    Else
        grdItems.Columns("daysonRejected").Text = Val(grdItems.Columns("daysonRejected").Text) + DateDiff("d", l_datMostRecentdDateCur, CDate(grdItems.Columns("offRejectedDate").Text) + 1)
'    End If
End If

'Check the Decisiontree situation
If Val(grdItems.Columns("decisiontree").Text) <> 0 And (IsNull(grdItems.Columns("decisiontreedate").Text) Or grdItems.Columns("decisiontreedate").Text = "") Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("decisiontreedate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("mostrecentdecisiontreedate").Text = Format(l_datRejectDateNow, "yyyy-mm-dd hh:nn:ss")

ElseIf Val(grdItems.Columns("decisiontree").Text) <> 0 And adoItems.Recordset("decisiontree") = 0 Then
     
    grdItems.Columns("MostRecentDecisionTreeDate").Text = Format(Now, "yyyy-mm-dd hh:nn:ss")
    
ElseIf Val(grdItems.Columns("decisiontree").Text) = 0 And adoItems.Recordset("decisiontree") <> 0 Then
    l_datRejectDateNow = Format(Now, "yyyy-mm-dd hh:nn:ss")
    grdItems.Columns("offdecisiontreeDate").Text = l_datRejectDateNow

    l_datRejectDateCur = FormatDateTime(grdItems.Columns("decisiontreedate").Text, vbShortDate)
    l_datMostRecentdDateCur = FormatDateTime(grdItems.Columns("MostRecentdecisiontreeDate").Text, vbShortDate)
    grdItems.Columns("daysondecisiontree").Text = Val(grdItems.Columns("daysondecisiontree").Text) + DateDiff("d", l_datMostRecentdDateCur, CDate(grdItems.Columns("offdecisiontreeDate").Text) + 1)

End If

'Get and update the file related items from the reference field.

If grdItems.Columns("itemreference").Text <> "" Then
    'see if there is a clip record to get the duration from reference
    l_strSQL = "SELECT fd_length FROM events WHERE clipreference = '" & grdItems.Columns("itemreference").Text & "' "
'    l_strSQL = l_strSQL & "AND companyid IN (SELECT companyID FROM company WHERE source = '" & GetData("company", "source", "companyID", Val(lblCompanyID.Caption)) & "') "
'    l_strSQL = l_strSQL & "AND companyID = " & lblCompanyID.Caption & " "
    l_strSQL = l_strSQL & "AND clipfilename = '" & grdItems.Columns("itemreference").Text & ".mov' AND system_deleted = 0;"
    Debug.Print l_strSQL
    l_strDuration = GetDataSQL(l_strSQL)
    grdItems.Columns("timecodeduration").Text = l_strDuration
    If l_strDuration <> "" Then
        l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
        l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
        If Val(Mid(l_strDuration, 7, 2)) > 30 Then
            l_lngRunningTime = l_lngRunningTime + 1
        End If
        If l_lngRunningTime = 0 Then l_lngRunningTime = 1
        If (grdItems.Columns("duration").Text = "") Or (grdItems.Columns("duration").Text <> "" And chkLockDur.Value = 0) Then
            grdItems.Columns("duration").Text = l_lngRunningTime
        End If
    End If
    
    'See if there is a size to go in the GB sent column
    l_curFileSize = 0
    Set l_rst = ExecuteSQL("SELECT bigfilesize FROM events WHERE clipreference = '" & QuoteSanitise(grdItems.Columns("itemreference").Text) & "' AND companyID = " & Val(grdItems.Columns("companyID").Text) & " AND clipfilename = '" & QuoteSanitise(grdItems.Columns("itemreference").Text) & ".mov' AND system_deleted = 0;", g_strExecuteError)
    CheckForSQLError
    If l_rst.RecordCount > 0 Then
        l_rst.MoveFirst
        While Not l_rst.EOF
            If Not IsNull(l_rst("bigfilesize")) Then
                If l_rst("bigfilesize") > l_curFileSize Then l_curFileSize = l_rst("bigfilesize")
            End If
            l_rst.MoveNext
        Wend
    End If
    l_rst.Close
    If l_curFileSize <> 0 Then
        If (grdItems.Columns("gbsent").Text = "") Or (grdItems.Columns("gbsent").Text <> "") Then
            grdItems.Columns("gbsent").Text = Int(l_curFileSize / 1024 / 1024 / 1024 + 0.999)
        End If
    End If
    
    Set l_rst = Nothing
End If

'Check the Complaint situation
If Val(grdItems.Columns("complainedabout").Text) <> 0 And adoItems.Recordset("complainedabout") = 0 Then
    l_strSQL = "INSERT INTO complaint (companyID, contactID, complainttype, originalsvensktrackerID, cdate, cuser, mdate, muser) VALUES (" & lblCompanyID.Caption & ", 2840, 2, " & grdItems.Columns("tracker_svensk_itemID").Text & ", getdate(), '" & g_strFullUserName & "', getdate(), '" & g_strFullUserName & "');"
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
End If



End Sub

Private Sub grdItems_BtnClick()

Dim tempdate As String

If grdItems.Columns(grdItems.Col).Name = "Validation" Then
    picValidationMasterFiles.Visible = True
    lblValidationTitle.Caption = grdItems.Columns("title").Text
    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
    If grdItems.Columns("validation").Text <> "" Then
        optMasterValidationComplete(0).Value = True
    Else
        optMasterValidationComplete(1).Value = True
    End If
    If GetData("tracker_svensk_item", "videoinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption) <> 0 Then
        optVideoInSpec(0).Value = True
    Else
        optVideoInSpec(1).Value = True
    End If
    cmbFrameRate.Text = GetData("tracker_svensk_item", "framerate", "tracker_svensk_itemID", lblTrackeritemID.Caption)
    If GetData("tracker_svensk_item", "hdflag", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optHDFlag(0).Value = True
    Else
        optHDFlag(1).Value = True
    End If
    If GetData("tracker_svensk_item", "MainStereoPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optMainStereoPresent(0).Value = True
    Else
        optMainStereoPresent(1).Value = True
    End If
    If GetData("tracker_svensk_item", "MEStereoPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optMEStereoPresent(0).Value = True
    Else
        optMEStereoPresent(1).Value = True
    End If
    If GetData("tracker_svensk_item", "SurroundPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optSurroundPresent(0).Value = True
    Else
        optSurroundPresent(1).Value = True
    End If
    If GetData("tracker_svensk_item", "TextlessPresent", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optTextlessPresent(0).Value = True
    Else
        optTextlessPresent(1).Value = True
    End If
    If GetData("tracker_svensk_item", "BlackAtEnd", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optBlackAtEnd(0).Value = True
    Else
        optBlackAtEnd(1).Value = True
    End If
    If GetData("tracker_svensk_item", "TextlessSeparator", "tracker_svensk_itemID", lblTrackeritemID.Caption) Then
        optTextlessSeparator(0).Value = True
    Else
        optTextlessSeparator(1).Value = True
    End If

ElseIf grdItems.Columns(grdItems.Col).Name = "englishvalidation" Or grdItems.Columns(grdItems.Col).Name = "norwegianvalidation" _
Or grdItems.Columns(grdItems.Col).Name = "swedishvalidation" Or grdItems.Columns(grdItems.Col).Name = "danishvalidation" Or grdItems.Columns(grdItems.Col).Name = "finnishvalidation" _
Or grdItems.Columns(grdItems.Col).Name = "icelandicvalidation" Then
    If grdItems.Columns(grdItems.Col).Text <> "" Then
        optAudioValidationComplete(0).Value = True
    Else
        optAudioValidationComplete(1).Value = True
    End If
    picValidationAudioFiles.Visible = True
    lblValidationTitle.Caption = grdItems.Columns("title").Text
    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
    optEnglishInSpec(Val(GetData("tracker_svensk_item", "englishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optNorwegianInSpec(Val(GetData("tracker_svensk_item", "norwegianinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optSwedishInSpec(Val(GetData("tracker_svensk_item", "swedishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optDanishInSpec(Val(GetData("tracker_svensk_item", "danishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optFinnishInSpec(Val(GetData("tracker_svensk_item", "finnishinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optIcelandicInSpec(Val(GetData("tracker_svensk_item", "icelandicinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    If grdItems.Columns("englishsepaudio").Text = 0 Then optEnglishInSpec(2).Value = True
    If grdItems.Columns("norwegiansepaudio").Text = 0 Then optNorwegianInSpec(2).Value = True
    If grdItems.Columns("swedishsepaudio").Text = 0 Then optSwedishInSpec(2).Value = True
    If grdItems.Columns("danishsepaudio").Text = 0 Then optDanishInSpec(2).Value = True
    If grdItems.Columns("finnishsepaudio").Text = 0 Then optFinnishInSpec(2).Value = True
    If grdItems.Columns("icelandicsepaudio").Text = 0 Then optIcelandicInSpec(2).Value = True
ElseIf grdItems.Columns(grdItems.Col).Name = "englishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "norwegiansubsvalidation" _
Or grdItems.Columns(grdItems.Col).Name = "swedishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "danishsubsvalidation" Or grdItems.Columns(grdItems.Col).Name = "finnishsubsvalidation" _
Or grdItems.Columns(grdItems.Col).Name = "icelandicsubsvalidation" Then
    If grdItems.Columns(grdItems.Col).Text <> "" Then
        optSubsValidationComplete(0).Value = True
    Else
        optSubsValidationComplete(1).Value = True
    End If
    picValidationSubs.Visible = True
    lblValidationTitle.Caption = grdItems.Columns("title").Text
    lblValidationSeries.Caption = Format(Val(grdItems.Columns("series").Text), "00")
    lblValidationEpisode.Caption = Format(Val(grdItems.Columns("episode").Text), "00")
    optEnglishSubsInSpec(Val(GetData("tracker_svensk_item", "englishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optNorwegianSubsInSpec(Val(GetData("tracker_svensk_item", "norwegiansubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optSwedishSubsInSpec(Val(GetData("tracker_svensk_item", "swedishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optDanishSubsInSpec(Val(GetData("tracker_svensk_item", "danishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optFinnishSubsInSpec(Val(GetData("tracker_svensk_item", "finnishsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    optIcelandicSubsInSpec(Val(GetData("tracker_svensk_item", "icelandicsubsinspec", "tracker_svensk_itemID", lblTrackeritemID.Caption))) = True
    If grdItems.Columns("englishsubs").Text = 0 Then optEnglishSubsInSpec(2).Value = True
    If grdItems.Columns("norwegiansubs").Text = 0 Then optNorwegianSubsInSpec(2).Value = True
    If grdItems.Columns("swedishsubs").Text = 0 Then optSwedishSubsInSpec(2).Value = True
    If grdItems.Columns("danishsubs").Text = 0 Then optDanishSubsInSpec(2).Value = True
    If grdItems.Columns("finnishsubs").Text = 0 Then optFinnishSubsInSpec(2).Value = True
    If grdItems.Columns("icelandicsubs").Text = 0 Then optIcelandicSubsInSpec(2).Value = True
ElseIf grdItems.ActiveCell.Text <> "" Then
    grdItems.ActiveCell.Text = ""
Else
    If InStr(GetData("company", "cetaclientcode", "companyID", lblCompanyID.Caption), "/trackerdatetime") > 0 Then
        grdItems.ActiveCell.Text = Now
    Else
        tempdate = FormatDateTime(Now, vbLongDate)
        grdItems.ActiveCell.Text = Format(Val(Day(tempdate)), "##") & " " & Left(MonthName(Month(tempdate)), 3) & " " & Right(Year(tempdate), 2)
    End If
End If

End Sub

Private Sub grdItems_DblClick()

ShowClipSearch "", grdItems.Columns("itemreference").Text

End Sub

Private Sub grdItems_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)

Dim l_strSQL As String

If m_blnSilent = False Then
    lblTrackeritemID.Caption = grdItems.Columns("tracker_Svensk_itemID").Text
    lblCompanyID.Caption = grdItems.Columns("companyID").Text
    
    If Val(lblTrackeritemID.Caption) <> Val(lblLastTrackerItemID.Caption) Then
        
        If Val(lblTrackeritemID.Caption) = 0 Then
            
            l_strSQL = "SELECT * FROM tracker_Svensk_comment WHERE tracker_Svensk_itemID = -1 ORDER BY cdate;"
            
            adoComments.RecordSource = l_strSQL
            adoComments.ConnectionString = g_strConnection
            adoComments.Refresh
            
            lblLastTrackerItemID.Caption = ""
        
            Exit Sub
        
        End If
        
        l_strSQL = "SELECT * FROM tracker_Svensk_comment WHERE tracker_Svensk_itemID = " & lblTrackeritemID.Caption & " ORDER BY cdate;"
        
        adoComments.RecordSource = l_strSQL
        adoComments.ConnectionString = g_strConnection
        adoComments.Refresh
    
        lblLastTrackerItemID.Caption = lblTrackeritemID.Caption
        
        If grdItems.Columns("readytobill").Text <> 0 And grdItems.Columns("billed").Text = 0 And grdItems.Columns("complaintredoitem").Text = 0 Then
            cmdBillItem.Visible = True
            cmdManualBillItem.Visible = True
        ElseIf grdItems.Columns("complaintredoitem").Text <> 0 Then
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = True
        Else
            cmdBillItem.Visible = False
            cmdManualBillItem.Visible = False
        End If
        
        If grdItems.Columns("billed").Text <> 0 Then
            cmdUnbill.Visible = True
        Else
            cmdUnbill.Visible = False
        End If
        
        lblLastValidationBy.Caption = GetDataSQL("SELECT ValidatedBy FROM tracker_svensk_validation WHERE tracker_svensk_itemID = " & Val(lblTrackeritemID) & " ORDER BY ValidationDate DESC;")
        lblLastValidationDate.Caption = GetDataSQL("SELECT ValidationDate FROM tracker_svensk_validation WHERE tracker_svensk_itemID = " & Val(lblTrackeritemID) & " ORDER BY ValidationDate DESC;")
    
    End If

End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

'Set the error states on the individual columns that can have errors

grdItems.Columns("channel").Text = GetData("company", "portalcompanyname", "companyID", Val(grdItems.Columns("companyID").Text))

If Val(grdItems.Columns("rejected").Text) <> 0 Then
    grdItems.Columns("rejected").CellStyleSet "Error"
End If

If Val(grdItems.Columns("resupply").Text) <> 0 Then
    grdItems.Columns("resupply").CellStyleSet "Error"
End If

If Val(grdItems.Columns("decisiontree").Text) <> 0 Then
    grdItems.Columns("decisiontree").CellStyleSet "Error"
End If

If Val(grdItems.Columns("urgent").Text) <> 0 Then
    grdItems.Columns("urgent").CellStyleSet "Error"
End If

If Val(grdItems.Columns("iTunes").Text) <> 0 Then
    grdItems.Columns("iTunes").CellStyleSet "Notify"
    grdItems.Columns("title").CellStyleSet "Notify"
End If

If Val(grdItems.Columns("englishsepaudio").Text) = 0 Then
    grdItems.Columns("englisharrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("englisharrived").Text) = "" Then
    grdItems.Columns("englisharrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("englishsubs").Text) = 0 Then
    grdItems.Columns("englishsubsarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("englishsubsarrived").Text) = "" Then
    grdItems.Columns("englishsubsarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("norwegiansepaudio").Text) = 0 Then
    grdItems.Columns("norwegianarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("norwegianarrived").Text) = "" Then
    grdItems.Columns("norwegianarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("norwegiansubs").Text) = 0 Then
    grdItems.Columns("norwegiansubsarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("norwegiansubsarrived").Text) = "" Then
    grdItems.Columns("norwegiansubsarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("swedishsepaudio").Text) = 0 Then
    grdItems.Columns("swedisharrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("swedisharrived").Text) = "" Then
    grdItems.Columns("swedisharrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("swedishsubs").Text) = 0 Then
    grdItems.Columns("swedishsubsarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("swedishsubsarrived").Text) = "" Then
    grdItems.Columns("swedishsubsarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("danishsepaudio").Text) = 0 Then
    grdItems.Columns("danisharrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("danisharrived").Text) = "" Then
    grdItems.Columns("danisharrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("danishsubs").Text) = 0 Then
    grdItems.Columns("danishsubsarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("danishsubsarrived").Text) = "" Then
    grdItems.Columns("danishsubsarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("finnishsepaudio").Text) = 0 Then
    grdItems.Columns("finnisharrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("finnisharrived").Text) = "" Then
    grdItems.Columns("finnisharrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("finnishsubs").Text) = 0 Then
    grdItems.Columns("finnishsubsarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("finnishsubsarrived").Text) = "" Then
    grdItems.Columns("finnishsubsarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("icelandicsepaudio").Text) = 0 Then
    grdItems.Columns("icelandicarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("icelandicarrived").Text) = "" Then
    grdItems.Columns("icelandicarrived").CellStyleSet "Error"
End If

If Val(grdItems.Columns("icelandicsubs").Text) = 0 Then
    grdItems.Columns("icelandicsubsarrived").CellStyleSet "NotRequired"
ElseIf Trim(" " & grdItems.Columns("icelandicsubsarrived").Text) = "" Then
    grdItems.Columns("icelandicsubsarrived").CellStyleSet "Error"
End If

'If Val(grdItems.Columns("rejected").Text) <> 0 Then
'    grdItems.Columns("rejected").CellStyleSet "Error"
'    grdItems.Columns("readytobill").CellStyleSet "Error"
'    grdItems.Columns("billed").CellStyleSet "Error"
'    grdItems.Columns("fixed").CellStyleSet "Error"
'End If
'
'If Val(grdItems.Columns("fixed").Text) <> 0 Then
'    grdItems.Columns("rejected").CellStyleSet "Fixed"
'    grdItems.Columns("readytobill").CellStyleSet "Fixed"
'    grdItems.Columns("billed").CellStyleSet "Fixed"
'    grdItems.Columns("fixed").CellStyleSet "Fixed"
'End If

End Sub

'Private Sub grdItems_SelChange(ByVal SelType As Integer, Cancel As Integer, DispSelRowOverflow As Integer)
'MsgBox ("selected")
'End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Function CheckReadyForTX() As Boolean

Dim temp As Boolean

temp = True

If Trim(" " & grdItems.Columns("masterarrived").Text) = "" Then temp = False
If Trim(" " & grdItems.Columns("filemade").Text) = "" Then temp = False
'If grdItems.Columns("senttosoundhouse").Text = "" Or UCase(Right(grdItems.Columns("senttosoundhouse").Text, 2)) = "ER" Then temp = False
'If Val(grdItems.Columns("englishaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("englishaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("englishaudioarrived").Text, 2)) = "ER" Then temp = False
'End If
'If Val(grdItems.Columns("frenchaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("frenchaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("frenchaudioarrived").Text, 2)) = "ER" Then temp = False
'End If
'If Val(grdItems.Columns("brazillianportaudioneeded").Text) <> 0 Then
'    If grdItems.Columns("brazillianportaudioarrived").Text = "" Or UCase(Right(grdItems.Columns("brazillianportaudioarrived").Text, 2)) = "ER" Then temp = False
'End If

CheckReadyForTX = temp

End Function

