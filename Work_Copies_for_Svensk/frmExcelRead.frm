VERSION 5.00
Begin VB.Form frmExcelRead 
   Caption         =   "Process Excel Orders"
   ClientHeight    =   4575
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   16245
   LinkTopic       =   "Excel Orders"
   ScaleHeight     =   4575
   ScaleWidth      =   16245
   Begin VB.CommandButton cmdHDDtask 
      Caption         =   "BBCW HDD Task"
      Height          =   375
      Left            =   7740
      TabIndex        =   35
      Top             =   2220
      Width           =   1995
   End
   Begin VB.CommandButton cmdOffAir 
      Caption         =   "Process WW Offairs"
      Height          =   375
      Left            =   7740
      TabIndex        =   34
      Top             =   1800
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCSvenskCompilations 
      Caption         =   "Svensk Compilations"
      Height          =   375
      Left            =   3540
      TabIndex        =   33
      Top             =   2220
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCSvenskFixes 
      Caption         =   "Svensk IMD Fixes Sheet"
      Height          =   375
      Left            =   3540
      TabIndex        =   32
      Top             =   1800
      Width           =   1995
   End
   Begin VB.CommandButton cmdiTunesOMD 
      Caption         =   "iTunes OMD Sheet"
      Height          =   375
      Left            =   3540
      TabIndex        =   31
      Top             =   2640
      Width           =   1995
   End
   Begin VB.CommandButton cmdPatheKeywords 
      Caption         =   "Pathe Keyword Update 1"
      Height          =   375
      Left            =   4380
      TabIndex        =   30
      Top             =   3360
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdNewDADCIngestRequest 
      Caption         =   "BBC DADC Ingest"
      Height          =   375
      Left            =   3540
      TabIndex        =   29
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdShineSeriesIDs 
      Caption         =   "Shine SeriesIDs"
      Height          =   375
      Left            =   11940
      TabIndex        =   28
      Top             =   2220
      Width           =   1995
   End
   Begin VB.CommandButton cmdShineSMVRemoval 
      Caption         =   "SMV Library Removals"
      Height          =   375
      Left            =   11940
      TabIndex        =   27
      Top             =   1800
      Width           =   1995
   End
   Begin VB.CommandButton cmdShineSeriesIDMediaReadback 
      Caption         =   "Shine Media Readback"
      Height          =   375
      Left            =   300
      TabIndex        =   26
      Top             =   3360
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdShineSeriesIDReadback 
      Caption         =   "Shine Library Readback"
      Height          =   375
      Left            =   2340
      TabIndex        =   25
      Top             =   3360
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdPublicis 
      Caption         =   "Process Publicis Order"
      Height          =   375
      Left            =   6420
      TabIndex        =   24
      Top             =   3360
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdShineSMVDigital 
      Caption         =   "SMV Digital Update"
      Height          =   375
      Left            =   11940
      TabIndex        =   23
      Top             =   1380
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCSvenskInboundData 
      Caption         =   "Svensk IMD Sheet"
      Height          =   375
      Left            =   3540
      TabIndex        =   22
      Top             =   1380
      Width           =   1995
   End
   Begin VB.CommandButton cmdShineSMVLibrary 
      Caption         =   "SMV Library Update"
      Height          =   375
      Left            =   11940
      TabIndex        =   21
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdFremantle 
      Caption         =   "Import Fremantle Sheet"
      Height          =   375
      Left            =   6180
      TabIndex        =   20
      Top             =   4740
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDisneyTracker 
      Caption         =   "Disney Jellyroll Tracker"
      Height          =   375
      Left            =   9840
      TabIndex        =   19
      Top             =   2220
      Width           =   1995
   End
   Begin VB.CommandButton cmdDisneyImport 
      Caption         =   "Import Disney Sheet"
      Height          =   375
      Left            =   2100
      TabIndex        =   18
      Top             =   4740
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCBatchUpdate 
      Caption         =   "DADAC AlphaID Update"
      Height          =   375
      Left            =   4140
      TabIndex        =   17
      Top             =   4740
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDADCBatch 
      Caption         =   "DADC Batch"
      Height          =   375
      Left            =   8220
      TabIndex        =   16
      Top             =   4740
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdSpecial 
      Caption         =   "Net-A-Porter Library"
      Height          =   375
      Left            =   9840
      TabIndex        =   15
      Top             =   1800
      Width           =   1995
   End
   Begin VB.CommandButton cmdMotionGallery 
      Caption         =   "Process Motion Gallery"
      Height          =   375
      Left            =   60
      TabIndex        =   14
      Top             =   4740
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdDespatchBarcodes 
      Caption         =   "Despatch Barcodes"
      Height          =   375
      Left            =   1440
      TabIndex        =   13
      Top             =   1380
      Width           =   1995
   End
   Begin VB.CommandButton cmdDHCourier 
      Caption         =   "Global Courier Charges"
      Height          =   375
      Left            =   5640
      TabIndex        =   12
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdLibraryMovements 
      Caption         =   "Library Movements"
      Height          =   375
      Left            =   1440
      TabIndex        =   11
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdWellcomeSegments 
      Caption         =   "Wellcome Segments"
      Height          =   375
      Left            =   9840
      TabIndex        =   10
      Top             =   1380
      Width           =   1995
   End
   Begin VB.CommandButton cmdWellcome 
      Caption         =   "Wellcome Batch"
      Height          =   375
      Left            =   9840
      TabIndex        =   9
      Top             =   960
      Width           =   1995
   End
   Begin VB.CommandButton cmdKidsCo 
      Caption         =   "Process KidsCo"
      Height          =   375
      Left            =   8460
      TabIndex        =   6
      Top             =   3360
      Visible         =   0   'False
      Width           =   1995
   End
   Begin VB.CommandButton cmdTradBBCWW 
      Caption         =   "Process Trad BBCWW"
      Height          =   375
      Left            =   7740
      TabIndex        =   5
      Top             =   1380
      Width           =   1995
   End
   Begin VB.CommandButton cmdSelectFile 
      Caption         =   "Select Excel File"
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   180
      Width           =   1995
   End
   Begin VB.CommandButton cmdInclBBCWW 
      Caption         =   "Process Incl BBCWW"
      Height          =   375
      Left            =   7740
      TabIndex        =   3
      Top             =   960
      Width           =   1995
   End
   Begin VB.TextBox txtExcelFilename 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   600
      Width           =   14595
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   375
      Left            =   14040
      TabIndex        =   0
      Top             =   960
      Width           =   1995
   End
   Begin VB.Label lblProgress 
      Height          =   255
      Left            =   360
      TabIndex        =   8
      Top             =   3840
      Width           =   15495
   End
   Begin VB.Label lblFileTitle 
      Height          =   255
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.Label lblCaption 
      Caption         =   "Filename"
      Height          =   255
      Left            =   300
      TabIndex        =   2
      Top             =   660
      Width           =   915
   End
End
Attribute VB_Name = "frmExcelRead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_lngCommacount As Long

'declare a variable to hold which comma we are on
Dim m_lngCurrentComma As Long
Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdDADCBatch_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngBatchNumber As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "Sheet1")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 4))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_lngBatchNumber = GetXLData(l_lngXLRowCounter, 1)
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 51))
    l_strAlphaBusiness = Trim(GetXLData(l_lngXLRowCounter, 32))
    l_strReference = l_strAlphaBusiness & "_" & SanitiseBarcode(l_strBarcode)
    l_strTitle = GetXLData(l_lngXLRowCounter, 10)
    l_strSubTitle = GetXLData(l_lngXLRowCounter, 21)
    l_lngEpisode = GetXLData(l_lngXLRowCounter, 22)
    l_strFormat = GetAlias(GetXLData(l_lngXLRowCounter, 56))
    l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 59)
    l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 65)
    l_strLegacyPictureElement = GetXLData(l_lngXLRowCounter, 58)
    l_strLanguage = GetXLData(l_lngXLRowCounter, 61)
    l_strSubtitlesLanguage = GetXLData(l_lngXLRowCounter, 64)
    l_strAudioConfiguration = GetXLData(l_lngXLRowCounter, 60)
    l_strDurationEstimate = GetXLData(l_lngXLRowCounter, 57)
    
    lblProgress.Caption = l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
    DoEvents
    
    l_strSQL = "INSERT INTO tracker_dadc_item (companyID, itemreference, title, subtitle, barcode, newbarcode, format, externalalphaID, originalexternalalphaID, legacylinestandard, imageaspectratio, legacypictureelement, language, "
    l_strSQL = l_strSQL & "subtitleslanguage, audioconfiguration, episode, batch, durationestimate) VALUES ("
    l_strSQL = l_strSQL & "1239, "
    l_strSQL = l_strSQL & "'" & l_strReference & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
    l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
    l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
    l_strSQL = l_strSQL & "'" & l_strFormat & "', "
    l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
    l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
    l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
    l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
    l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
    l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
    l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
    l_strSQL = l_strSQL & "'" & l_strAudioConfiguration & "', "
    l_strSQL = l_strSQL & l_lngEpisode & ", "
    l_strSQL = l_strSQL & l_lngBatchNumber & ", "
    l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ");"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdDADCSvenskCompilations_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strSubTitle As String, l_strEpisode As String, l_strComponentType As String, l_strConform As String, l_strBarcode As String, l_strAlphaDisplayCode As String
Dim l_strLanguage As String, l_strProjectNumber As String, l_strProjectManager As String, l_strRightsOwner As String, l_strProgramType As String, l_strSeries As String, l_strFramerate As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset, l_strProxyRequired As String, l_strFixNotes As String
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngTrackerID As Long

Dim l_strSQL As String, TempStr As String, l_blnError As Boolean, l_strSFUniqueID As String, l_strOldLanguage As String, l_strTextlessRequired As String, l_strMERequired As String, l_striTunesRequired As String
Dim l_strAuditDescription As String, l_strRevisionNumber As String, l_strUrgent As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "SF Inbound Material Data")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 5))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 8) <> ""
    
    l_blnError = False
    l_strProjectNumber = Trim(" " & GetXLData(l_lngXLRowCounter, 1)) 'A
    If l_strProjectNumber = "" Then l_strProjectNumber = "999999"
    l_strProjectManager = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_strRightsOwner = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strDueDate = Trim(GetXLData(l_lngXLRowCounter, 6)) 'F
    If IsDate(l_strDueDate) Then l_datDueDate = l_strDueDate
    l_strProgramType = Trim(GetXLData(l_lngXLRowCounter, 7)) 'G
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 8)) 'H
    l_strSeries = Trim(GetXLData(l_lngXLRowCounter, 9)) 'I
    l_strSubTitle = Trim(GetXLData(l_lngXLRowCounter, 10)) 'J
    l_strEpisode = Trim(GetXLData(l_lngXLRowCounter, 11)) 'K
    l_strComponentType = Trim(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, 13)) 'M
    l_strConform = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N
    l_strProxyRequired = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_striTunesRequired = Trim(GetXLData(l_lngXLRowCounter, 16)) 'P
    l_strTextlessRequired = Trim(GetXLData(l_lngXLRowCounter, 17)) 'Q
    l_strMERequired = Trim(GetXLData(l_lngXLRowCounter, 18)) 'R
    l_strFramerate = Trim(GetXLData(l_lngXLRowCounter, 20)) 'T
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 21)) 'U
    l_strAlphaDisplayCode = Trim(GetXLData(l_lngXLRowCounter, 23)) 'W
    l_strRevisionNumber = Trim(GetXLData(l_lngXLRowCounter, 24)) 'X
    l_strUrgent = Trim(GetXLData(l_lngXLRowCounter, 25)) 'Y
    lblProgress.Caption = "Row:" & l_lngXLRowCounter & ", " & l_strProjectNumber & " - " & l_strTitle & " - " & l_strComponentType
    l_strSFUniqueID = l_strProjectNumber & "_" & l_strTitle & "_" & l_strProgramType & "_" & Format(Val(l_strSeries), "00") & "_" & Format(Val(l_strEpisode), "00")
    
    If ValidateEnglishCharacters(l_strProjectManager) = True And ValidateEnglishCharacters(l_strTitle) = True And ValidateEnglishCharacters(l_strSubTitle) = True Then ' And ValidateEnglishCharacters(l_strRightsOwner) = True
        l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
        If l_strSeries <> "" Then
            l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
        End If
        If l_strEpisode = "" Then
            l_strSQL = l_strSQL & " AND episode IS NULL"
        Else
            l_strSQL = l_strSQL & " AND episode = " & Val(l_strEpisode)
        End If
        If l_strRevisionNumber <> "" Then
            l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
        Else
            l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
        End If
        l_strSQL = l_strSQL & " AND companyID = 1434 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill = 0 "
        TempStr = GetDataSQL(l_strSQL)
        If (Val(TempStr) = 0 And (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE")) Then
            'Check whether this is an existing item that is completed...
            l_strSQL = l_strSQL & " AND companyID = 1434 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill <> 0 "
            TempStr = GetDataSQL(l_strSQL)
            If (Val(TempStr) = 0 And (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE")) Then
                'Insert new record
                l_strSQL = "INSERT INTO tracker_svensk_item ("
                If UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE" Then
                    If UCase(l_strComponentType) = "AUDIO" Then
                        Select Case UCase(l_strLanguage)
                            Case "ENGLISH"
                                l_strSQL = l_strSQL & "englishsepaudio, "
                            Case "NORWEGIAN"
                                l_strSQL = l_strSQL & "norwegiansepaudio, "
                            Case "SWEDISH"
                                l_strSQL = l_strSQL & "swedishsepaudio, "
                            Case "DANISH"
                                l_strSQL = l_strSQL & "danishsepaudio, "
                            Case "FINNISH"
                                l_strSQL = l_strSQL & "finnishsepaudio, "
                            Case "ICELANDIC"
                                l_strSQL = l_strSQL & "icelandicsepaudio, "
                            Case Else
                                MsgBox "Line " & l_lngXLRowCounter & ": Unidentified Language - '" & l_strLanguage & "' in Audio or Subtitle file", vbCritical, "Line not read in"
                                l_blnError = True
                        End Select
                    ElseIf UCase(Left(l_strComponentType, 8)) = "SUBTITLE" Then
                        Select Case UCase(l_strLanguage)
                            Case "ENGLISH"
                                l_strSQL = l_strSQL & "englishsubs, "
                            Case "NORWEGIAN"
                                l_strSQL = l_strSQL & "norwegiansubs, "
                            Case "SWEDISH"
                                l_strSQL = l_strSQL & "swedishsubs, "
                            Case "DANISH"
                                l_strSQL = l_strSQL & "danishsubs, "
                            Case "FINNISH"
                                l_strSQL = l_strSQL & "finnishsubs, "
                            Case "ICELANDIC"
                                l_strSQL = l_strSQL & "icelandicsubs, "
                            Case Else
                                MsgBox "Line " & l_lngXLRowCounter & ": Unidentified Language - '" & l_strLanguage & "' in Audio or Subtitle file", vbCritical, "Line not read in"
                                l_blnError = True
                        End Select
                    End If
                End If
                If UCase(l_strTextlessRequired) = "YES" Then l_strSQL = l_strSQL & "textlessrequired, "
                If UCase(l_strMERequired) = "YES" Then l_strSQL = l_strSQL & "merequired, "
                If UCase(l_strProxyRequired) = "YES" Then l_strSQL = l_strSQL & "proxyreq, "
                If l_strFramerate <> "" Then l_strSQL = l_strSQL & "requestedframerate, "
                If l_strBarcode <> "" Then l_strSQL = l_strSQL & "barcode, "
                If l_strRevisionNumber <> "" Then l_strSQL = l_strSQL & "RevisionNumber, "
                If l_strUrgent <> "" Then l_strSQL = l_strSQL & "urgent, "
                l_strSQL = l_strSQL & "cdate, mdate, UniqueID, companyID, projectnumber, projectmanager, projectmanagercontactID, rightsowner, duedate, programtype, title, series, "
                l_strSQL = l_strSQL & "subtitle, episode, componenttype, language, iTunes, alphadisplaycode, conform) VALUES ("
                If UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE" Then
                    l_strSQL = l_strSQL & "1, "
                End If
                If UCase(l_strTextlessRequired) = "YES" Then l_strSQL = l_strSQL & "1, "
                If UCase(l_strMERequired) = "YES" Then l_strSQL = l_strSQL & "1, "
                If UCase(l_strProxyRequired) = "YES" Then l_strSQL = l_strSQL & "1, "
                If l_strFramerate <> "" Then l_strSQL = l_strSQL & "'" & l_strFramerate & "', "
                If l_strBarcode <> "" Then l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
                If l_strRevisionNumber <> "" Then l_strSQL = l_strSQL & "'" & l_strRevisionNumber & "', "
                If l_strUrgent <> "" Then l_strSQL = l_strSQL & "-1, "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'" & l_strSFUniqueID & "', "
                l_strSQL = l_strSQL & "1434, "
                l_strSQL = l_strSQL & "'" & l_strProjectNumber & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProjectManager) & "', "
                If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1434);"))) <> 0 Then
                    l_strSQL = l_strSQL & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                Else
                    MsgBox "Line " & l_lngXLRowCounter & " has no single Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                    l_blnError = True
                End If
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRightsOwner) & "', "
                If l_datDueDate > Now Then
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
                Else
                    MsgBox "Line " & l_lngXLRowCounter & " has a Due Date earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                    l_blnError = True
                End If
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgramType) & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
                l_strSQL = l_strSQL & "'" & l_strSeries & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
                If l_strEpisode <> "" Then
                    l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
                Else
                    l_strSQL = l_strSQL & "NULL, "
                End If
                l_strSQL = l_strSQL & "'" & l_strComponentType & "', "
                l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
                If UCase(l_striTunesRequired) = "YES" Then
                    l_strSQL = l_strSQL & "1, "
                Else
                    l_strSQL = l_strSQL & "0, "
                End If
                l_strSQL = l_strSQL & "'" & l_strAlphaDisplayCode & "', "
                If UCase(l_strConform) = "YES" Then
                    l_strSQL = l_strSQL & "1); "
                Else
                    l_strSQL = l_strSQL & "0); "
                End If
                If l_blnError = False Then
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strEmailBody = l_strEmailBody & "Added: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                    l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & g_lngLastID & ", getdate(), '" & g_strFullUserName & "', 'Insert from IMD');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            Else
                If Not (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
                    'Odd Component type
                    MsgBox "Line " & l_lngXLRowCounter & " has an unrecognised Component Type '" & l_strComponentType & "' - Line not read in.", vbCritical, "Error Reading Sheet"
                ElseIf Val(TempStr) <> 0 Then
                    ' Existing completed line
                    MsgBox "Line " & l_lngXLRowCounter & " was an Update to an item we have already completed - Line not read in.", vbCritical, "Error Reading Sheet"
                Else
                    MsgBox "Line " & l_lngXLRowCounter & " had an unrecognised error - Line not read in.", vbCritical, "Error Reading Sheet"
                End If
            End If
        Else
            l_strOldLanguage = GetData("tracker_svensk_item", "language", "tracker_svensk_itemID", TempStr)
            l_strAuditDescription = ""
            Select Case UCase(Left(l_strComponentType, 8))
                Case "VIDEO", "TRAILER"
                    l_strSQL = "UPDATE tracker_svensk_item SET "
                    If l_strAlphaDisplayCode <> "" Then l_strSQL = l_strSQL & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
                    If l_strRevisionNumber <> "" Then
                        l_strSQL = l_strSQL & "RevisionNumber = '" & l_strRevisionNumber & "', "
                        l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
                    End If
                    If l_strUrgent <> "" Then
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> -1 Then
                            l_strSQL = l_strSQL & "urgent = -1, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
                        End If
                    Else
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> 0 Then
                            l_strSQL = l_strSQL & "urgent = 0, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
                        End If
                    End If
                    If GetData("tracker_svensk_item", "componenttype", "tracker_svensk_itemID", TempStr) <> l_strComponentType Then
                        l_strSQL = l_strSQL & "componenttype = '" & l_strComponentType & "', "
                        l_strAuditDescription = l_strAuditDescription & "componenttype = '" & l_strComponentType & "', "
                    End If
                    If GetData("tracker_svensk_item", "language", "tracker_svensk_itemID", TempStr) <> l_strLanguage Then
                        l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
                        l_strAuditDescription = l_strAuditDescription & "language = '" & l_strLanguage & "', "
                    End If
                    If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", TempStr) <> l_datDueDate Then
                        If l_datDueDate > Now Then
                            l_strSQL = l_strSQL & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
                            l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
                        Else
                            MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                        End If
                    End If
                    If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1434);"))) <> 0 Then
                        If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", TempStr) <> l_strProjectManager Then
                            l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                            l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                            l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & l_strProjectManager & "', "
                        End If
                    Else
                        MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                        l_blnError = True
                    End If
                    If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", TempStr) <> l_strRightsOwner Then
                        l_strSQL = l_strSQL & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                        l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & l_strRightsOwner & "', "
                    End If
                    If UCase(l_strProxyRequired) = "YES" Then
                        l_strSQL = l_strSQL & "proxyreq = 1, "
                    Else
                        l_strSQL = l_strSQL & "proxyreq = 0, "
                    End If
                    l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
                    If UCase(l_striTunesRequired) = "YES" Then
                        l_strSQL = l_strSQL & "itunes = 1, "
                    Else
                        l_strSQL = l_strSQL & "itunes = 0, "
                    End If
                    If UCase(l_strTextlessRequired) = "YES" Then l_strSQL = l_strSQL & "textlessrequired = 1, "
                    If UCase(l_strMERequired) = "YES" Then l_strSQL = l_strSQL & "merequired = 1, "
                    If UCase(l_strConform) = "YES" Then
                        l_strSQL = l_strSQL & "conform = 1 "
                    Else
                        l_strSQL = l_strSQL & "conform = 0 "
                    End If
                    If l_strOldLanguage = l_strLanguage Then
                        Select Case UCase(l_strLanguage)
                            Case "ENGLISH"
                                l_strSQL = l_strSQL & ", englishsepaudio = 0 "
                            Case "NORWEGIAN"
                                l_strSQL = l_strSQL & ", norwegiansepaudio = 0 "
                            Case "SWEDISH"
                                l_strSQL = l_strSQL & ", swedishsepaudio = 0 "
                            Case "DANISH"
                                l_strSQL = l_strSQL & ", danishsepaudio = 0 "
                            Case "FINNISH"
                                l_strSQL = l_strSQL & ", finnishsepaudio = 0 "
                            Case "ICELANDIC"
                                l_strSQL = l_strSQL & ", icelandicsepaudio = 0 "
                        End Select
                    End If
                Case "AUDIO"
                    If l_strLanguage <> l_strOldLanguage Then
                        Select Case UCase(l_strLanguage)
                            Case "ENGLISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "englishsepaudio = 1, "
                            Case "NORWEGIAN"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "norwegiansepaudio = 1, "
                            Case "SWEDISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "swedishsepaudio = 1, "
                            Case "DANISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "danishsepaudio = 1, "
                            Case "FINNISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "finnishsepaudio = 1, "
                            Case "ICELANDIC"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "icelandicsepaudio = 1, "
                            Case Else
                                MsgBox "Line " & l_lngXLRowCounter & " has an unidentified audio language - Line not read in.", vbCritical, "Error Reading Sheet"
                                l_blnError = True
                        End Select
                        If l_strAlphaDisplayCode <> "" Then l_strSQL = l_strSQL & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
                        If l_strRevisionNumber <> "" Then
                            l_strSQL = l_strSQL & "RevisionNumber = '" & l_strRevisionNumber & "', "
                            l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
                        End If
                        If l_strUrgent <> "" Then
                            If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> -1 Then
                                l_strSQL = l_strSQL & "urgent = -1, "
                                l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
                            End If
                        Else
                            If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> 0 Then
                                l_strSQL = l_strSQL & "urgent = 0, "
                                l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
                            End If
                        End If
                        If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", TempStr) <> l_datDueDate Then
                            If l_datDueDate > Now Then
                                l_strSQL = l_strSQL & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
                                l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
                            Else
                                MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                                l_blnError = True
                            End If
                        End If
                        If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1434);"))) <> 0 Then
                            If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", TempStr) <> l_strProjectManager Then
                                l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                                l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                                l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & l_strProjectManager & "', "
                            End If
                        Else
                            MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                        End If
                        If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", TempStr) <> l_strRightsOwner Then
                            l_strSQL = l_strSQL & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                            l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & l_strRightsOwner & "', "
                        End If
                        If UCase(l_strProxyRequired) = "YES" Then
                            l_strSQL = l_strSQL & "proxyreq = 1, "
                        Else
                            l_strSQL = l_strSQL & "proxyreq = 0, "
                        End If
                        l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
                        If UCase(l_striTunesRequired) = "YES" Then
                            l_strSQL = l_strSQL & "itunes = 1, "
                        Else
                            l_strSQL = l_strSQL & "itunes = 0, "
                        End If
                        If UCase(l_strConform) = "YES" Then
                            l_strSQL = l_strSQL & "conform = 1 "
                        Else
                            l_strSQL = l_strSQL & "conform = 0 "
                        End If
                    Else
                        l_strSQL = ""
                    End If
                Case "SUBTITLE"
                    Select Case UCase(l_strLanguage)
                        Case "ENGLISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "englishsubs = 1, "
                        Case "NORWEGIAN"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "norwegiansubs = 1, "
                        Case "SWEDISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "swedishsubs = 1, "
                        Case "DANISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "danishsubs = 1, "
                        Case "FINNISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "finnishsubs = 1, "
                        Case "ICELANDIC"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "icelandicsubs = 1, "
                        Case Else
                            MsgBox "Line " & l_lngXLRowCounter & " has an unidentified subtitle language - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                    End Select
                    If l_strAlphaDisplayCode <> "" Then l_strSQL = l_strSQL & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
                    If l_strRevisionNumber <> "" Then
                        l_strSQL = l_strSQL & "RevisionNumber = '" & l_strRevisionNumber & "', "
                        l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
                    End If
                    If l_strUrgent <> "" Then
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> -1 Then
                            l_strSQL = l_strSQL & "urgent = -1, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
                        End If
                    Else
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> 0 Then
                            l_strSQL = l_strSQL & "urgent = 0, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
                        End If
                    End If
                    If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", TempStr) <> l_datDueDate Then
                        If l_datDueDate > Now Then
                            l_strSQL = l_strSQL & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
                            l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
                        Else
                            MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                        End If
                    End If
                    If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1434);"))) <> 0 Then
                        If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", TempStr) <> l_strProjectManager Then
                            l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                            l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                            l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & l_strProjectManager & "', "
                        End If
                    Else
                        MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                        l_blnError = True
                    End If
                    If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", TempStr) <> l_strRightsOwner Then
                        l_strSQL = l_strSQL & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                        l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & l_strRightsOwner & "', "
                    End If
                    If UCase(l_strProxyRequired) = "YES" Then
                        l_strSQL = l_strSQL & "proxyreq = 1, "
                    Else
                        l_strSQL = l_strSQL & "proxyreq = 0, "
                    End If
                    l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
                    If UCase(l_striTunesRequired) = "YES" Then
                        l_strSQL = l_strSQL & "itunes = 1, "
                    Else
                        l_strSQL = l_strSQL & "itunes = 0, "
                    End If
                    If UCase(l_strConform) = "YES" Then
                        l_strSQL = l_strSQL & "conform = 1 "
                    Else
                        l_strSQL = l_strSQL & "conform = 0 "
                    End If
                Case Else
                    l_blnError = True
            End Select
            If l_strSQL <> "" Then l_strSQL = l_strSQL & "WHERE tracker_svensk_itemID = " & TempStr & ";"
            
            If l_blnError = False And l_strSQL <> "" Then
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strEmailBody = l_strEmailBody & "Updated: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                If l_strAuditDescription <> "" Then
                    l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
                    l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from IMD: " & QuoteSanitise(l_strAuditDescription) & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            ElseIf l_blnError = True Then
                MsgBox "Line " & l_lngXLRowCounter & " has an unidentified component type '" & l_strComponentType & "' - Line not read in.", vbCritical, "Error Reading Sheet"
            End If
        End If
    Else
        If ValidateEnglishCharacters(l_strProjectManager) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Project Manager' column - Line not read in.", vbCritical, "Error Reading Sheet"
        If ValidateEnglishCharacters(l_strTitle) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Title' column - Line not read in.", vbCritical, "Error Reading Sheet"
        If ValidateEnglishCharacters(l_strSubTitle) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Episode Title' column - Line not read in.", vbCritical, "Error Reading Sheet"
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new items were added to the DADC Svensk Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1434) & "' AND trackermessageID = 32;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Svensk Ingest Tracker Items Added or Updated", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdDADCSvenskFixes_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strSubTitle As String, l_strEpisode As String, l_strComponentType As String, l_strConform As String, l_strBarcode As String, l_strAlphaDisplayCode As String
Dim l_strLanguage As String, l_strProjectNumber As String, l_strProjectManager As String, l_strRightsOwner As String, l_strProgramType As String, l_strSeries As String, l_strFramerate As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset, l_strProxyRequired As String, l_strFixNotes As String
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngTrackerID As Long

Dim l_strSQL As String, TempStr As String, l_blnError As Boolean, l_strSFUniqueID As String, l_strOldLanguage As String, l_strTextlessRequired As String, l_striTunesRequired As String
Dim l_strComment As String
Dim l_strAuditDescription As String, l_strRevisionNumber As String, l_strUrgent As String, l_strMERequired As String, l_strME51Required As String, l_strSpecialProject As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "SF Inbound Material Data")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 5))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While Trim(GetXLData(l_lngXLRowCounter, 8)) <> "" ' checking the title column
    
    l_blnError = False
    l_strProjectNumber = Trim(GetXLData(l_lngXLRowCounter, 1)) 'A
    If l_strProjectNumber = "" Then l_strProjectNumber = "999999"
    l_strProjectManager = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_strRightsOwner = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strDueDate = Trim(GetXLData(l_lngXLRowCounter, 6)) 'F
    If IsDate(l_strDueDate) Then l_datDueDate = l_strDueDate
    l_strProgramType = Trim(GetXLData(l_lngXLRowCounter, 8)) 'H
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 9)) 'I
    l_strSeries = Trim(GetXLData(l_lngXLRowCounter, 10)) 'J
    l_strSubTitle = Trim(GetXLData(l_lngXLRowCounter, 11)) 'K
    l_strEpisode = Trim(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strComponentType = Trim(GetXLData(l_lngXLRowCounter, 13)) 'M
    l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N
    l_strConform = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_strProxyRequired = Trim(GetXLData(l_lngXLRowCounter, 16)) 'P
    l_striTunesRequired = Trim(GetXLData(l_lngXLRowCounter, 17)) 'Q
    l_strTextlessRequired = Trim(GetXLData(l_lngXLRowCounter, 18)) 'R
    l_strMERequired = Trim(GetXLData(l_lngXLRowCounter, 19)) 'S
    l_strME51Required = Trim(GetXLData(l_lngXLRowCounter, 20)) 'T
    l_strAuditDescription = Trim(GetXLData(l_lngXLRowCounter, 21)) 'U
    l_strFramerate = Trim(GetXLData(l_lngXLRowCounter, 22)) 'V
    l_strComment = Trim(GetXLData(l_lngXLRowCounter, 23)) 'W
    l_strAlphaDisplayCode = Trim(GetXLData(l_lngXLRowCounter, 24)) 'X
    l_strRevisionNumber = Trim(GetXLData(l_lngXLRowCounter, 25)) 'Y
    l_strUrgent = Trim(GetXLData(l_lngXLRowCounter, 26)) 'Z
    l_strSpecialProject = Trim(GetXLData(l_lngXLRowCounter, 27)) 'AA
    lblProgress.Caption = "Row:" & l_lngXLRowCounter & ", " & l_strProjectNumber & " - " & l_strTitle & " - " & l_strComponentType
    l_strSFUniqueID = l_strProjectNumber & "_" & l_strTitle & "_" & l_strProgramType & "_" & Format(Val(l_strSeries), "00") & "_" & Format(Val(l_strEpisode), "00")
    
    If ValidateEnglishCharacters(l_strProjectManager) = True And ValidateEnglishCharacters(l_strTitle) = True And ValidateEnglishCharacters(l_strSubTitle) = True Then ' And ValidateEnglishCharacters(l_strRightsOwner) = True
        l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
        If l_strSeries <> "" Then
            l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
        End If
        If l_strEpisode = "" Then
            l_strSQL = l_strSQL & " AND episode IS NULL"
        Else
            l_strSQL = l_strSQL & " AND episode = " & Val(l_strEpisode)
        End If
        If l_strRevisionNumber <> "" Then
            l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
        Else
            l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
        End If
        l_strSQL = l_strSQL & " AND companyID = 1330 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill = 0;"
        TempStr = GetDataSQL(l_strSQL)
        If Val(TempStr) <> 0 And (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
            'Existing line that is not completed yet...
            l_strOldLanguage = GetData("tracker_svensk_item", "language", "tracker_svensk_itemID", TempStr)
            l_strAuditDescription = ""
            Select Case UCase(Left(l_strComponentType, 8))
                Case "VIDEO", "TRAILER"
                    l_strSQL = "UPDATE tracker_svensk_item SET "
                    If l_strAlphaDisplayCode <> "" Then l_strSQL = l_strSQL & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
                    If l_strRevisionNumber <> "" Then
                        l_strSQL = l_strSQL & "RevisionNumber = '" & l_strRevisionNumber & "', "
                        l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
                    End If
                    If l_strUrgent <> "" Then
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> -1 Then
                            l_strSQL = l_strSQL & "urgent = -1, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
                        End If
                    Else
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> 0 Then
                            l_strSQL = l_strSQL & "urgent = 0, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
                        End If
                    End If
                    If GetData("tracker_svensk_item", "componenttype", "tracker_svensk_itemID", TempStr) <> l_strComponentType Then
                        l_strSQL = l_strSQL & "componenttype = '" & l_strComponentType & "', "
                        l_strAuditDescription = l_strAuditDescription & "componenttype = '" & l_strComponentType & "', "
                    End If
                    If GetData("tracker_svensk_item", "language", "tracker_svensk_itemID", TempStr) <> l_strLanguage Then
                        l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
                        l_strAuditDescription = l_strAuditDescription & "language = '" & l_strLanguage & "', "
                    End If
                    If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", TempStr) <> l_datDueDate Then
                        If l_datDueDate > Now Then
                            l_strSQL = l_strSQL & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
                            l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
                        Else
                            MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                        End If
                    End If
                    If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1415);"))) <> 0 Then
                        If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", TempStr) <> l_strProjectManager Then
                            l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                            l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                            l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                        End If
                    Else
                        MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                        l_blnError = True
                    End If
                    If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", TempStr) <> l_strRightsOwner Then
                        l_strSQL = l_strSQL & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                        l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                    End If
                    If GetData("tracker_svensk_item", "SpecialProject", "tracker_svensk_itemID", TempStr) <> l_strSpecialProject Then
                        l_strSQL = l_strSQL & "SpecialProject = '" & QuoteSanitise(l_strSpecialProject) & "', "
                        l_strAuditDescription = l_strAuditDescription & "SpecialProject = '" & QuoteSanitise(l_strSpecialProject) & "', "
                    End If
                    If UCase(l_strProxyRequired) = "YES" Then
                        l_strSQL = l_strSQL & "proxyreq = 1, "
                    Else
                        l_strSQL = l_strSQL & "proxyreq = 0, "
                    End If
                    If UCase(l_strMERequired) = "YES" Then
                        l_strSQL = l_strSQL & "MErequired = 1, "
                    Else
                        l_strSQL = l_strSQL & "MErequired = 0, "
                    End If
                    If UCase(l_strME51Required) = "YES" Then
                        l_strSQL = l_strSQL & "ME51required = 1, "
                    Else
                        l_strSQL = l_strSQL & "ME51required = 0, "
                    End If
                    l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
                    If UCase(l_striTunesRequired) = "YES" Then
                        l_strSQL = l_strSQL & "itunes = 1, "
                    Else
                        l_strSQL = l_strSQL & "itunes = 0, "
                    End If
                    If UCase(l_strConform) = "YES" Then
                        l_strSQL = l_strSQL & "conform = 1 "
                    Else
                        l_strSQL = l_strSQL & "conform = 0 "
                    End If
                    If l_strOldLanguage = l_strLanguage Then
                        Select Case UCase(l_strLanguage)
                            Case "ENGLISH"
                                l_strSQL = l_strSQL & ", englishsepaudio = 0 "
                            Case "NORWEGIAN"
                                l_strSQL = l_strSQL & ", norwegiansepaudio = 0 "
                            Case "SWEDISH"
                                l_strSQL = l_strSQL & ", swedishsepaudio = 0 "
                            Case "DANISH"
                                l_strSQL = l_strSQL & ", danishsepaudio = 0 "
                            Case "FINNISH"
                                l_strSQL = l_strSQL & ", finnishsepaudio = 0 "
                            Case "ICELANDIC"
                                l_strSQL = l_strSQL & ", icelandicsepaudio = 0 "
                        End Select
                    End If
                Case "AUDIO"
                    If l_strLanguage <> l_strOldLanguage Then
                        Select Case UCase(l_strLanguage)
                            Case "ENGLISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "englishsepaudio = 1, "
                            Case "NORWEGIAN"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "norwegiansepaudio = 1, "
                            Case "SWEDISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "swedishsepaudio = 1, "
                            Case "DANISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "danishsepaudio = 1, "
                            Case "FINNISH"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "finnishsepaudio = 1, "
                            Case "ICELANDIC"
                                l_strSQL = "UPDATE tracker_svensk_item SET "
                                l_strSQL = l_strSQL & "icelandicsepaudio = 1, "
                            Case Else
                                MsgBox "Line " & l_lngXLRowCounter & " has an unidentified audio language - Line not read in.", vbCritical, "Error Reading Sheet"
                                l_blnError = True
                        End Select
                        If l_strAlphaDisplayCode <> "" Then l_strSQL = l_strSQL & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
                        If l_strRevisionNumber <> "" Then
                            l_strSQL = l_strSQL & "RevisionNumber = '" & l_strRevisionNumber & "', "
                            l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
                        End If
                        If l_strUrgent <> "" Then
                            If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> -1 Then
                                l_strSQL = l_strSQL & "urgent = -1, "
                                l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
                            End If
                        Else
                            If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> 0 Then
                                l_strSQL = l_strSQL & "urgent = 0, "
                                l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
                            End If
                        End If
                        If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", TempStr) <> l_datDueDate Then
                            If l_datDueDate > Now Then
                                l_strSQL = l_strSQL & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
                                l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
                            Else
                                MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                                l_blnError = True
                            End If
                        End If
                        If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1415);"))) <> 0 Then
                            If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", TempStr) <> l_strProjectManager Then
                                l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                                l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                                l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & l_strProjectManager & "', "
                            End If
                        Else
                            MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                        End If
                        If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", TempStr) <> l_strRightsOwner Then
                            l_strSQL = l_strSQL & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                            l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & l_strRightsOwner & "', "
                        End If
                        If UCase(l_strProxyRequired) = "YES" Then
                            l_strSQL = l_strSQL & "proxyreq = 1, "
                        Else
                            l_strSQL = l_strSQL & "proxyreq = 0, "
                        End If
                        l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
                        If UCase(l_striTunesRequired) = "YES" Then
                            l_strSQL = l_strSQL & "itunes = 1, "
                        Else
                            l_strSQL = l_strSQL & "itunes = 0, "
                        End If
                        If UCase(l_strConform) = "YES" Then
                            l_strSQL = l_strSQL & "conform = 1 "
                        Else
                            l_strSQL = l_strSQL & "conform = 0 "
                        End If
                    Else
                        l_strSQL = ""
                    End If
                Case "SUBTITLE"
                    Select Case UCase(l_strLanguage)
                        Case "ENGLISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "englishsubs = 1, "
                        Case "NORWEGIAN"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "norwegiansubs = 1, "
                        Case "SWEDISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "swedishsubs = 1, "
                        Case "DANISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "danishsubs = 1, "
                        Case "FINNISH"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "finnishsubs = 1, "
                        Case "ICELANDIC"
                            l_strSQL = "UPDATE tracker_svensk_item SET "
                            l_strSQL = l_strSQL & "icelandicsubs = 1, "
                        Case Else
                            MsgBox "Line " & l_lngXLRowCounter & " has an unidentified subtitle language - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                    End Select
                    If l_strAlphaDisplayCode <> "" Then l_strSQL = l_strSQL & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
                    If l_strRevisionNumber <> "" Then
                        l_strSQL = l_strSQL & "RevisionNumber = '" & l_strRevisionNumber & "', "
                        l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
                    End If
                    If l_strUrgent <> "" Then
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> -1 Then
                            l_strSQL = l_strSQL & "urgent = -1, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
                        End If
                    Else
                        If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(TempStr)) <> 0 Then
                            l_strSQL = l_strSQL & "urgent = 0, "
                            l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
                        End If
                    End If
                    If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", TempStr) <> l_datDueDate Then
                        If l_datDueDate > Now Then
                            l_strSQL = l_strSQL & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
                            l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
                        Else
                            MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                            l_blnError = True
                        End If
                    End If
                    If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1415);"))) <> 0 Then
                        If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", TempStr) <> l_strProjectManager Then
                            l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
                            l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                            l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & l_strProjectManager & "', "
                        End If
                    Else
                        MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                        l_blnError = True
                    End If
                    If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", TempStr) <> l_strRightsOwner Then
                        l_strSQL = l_strSQL & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
                        l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & l_strRightsOwner & "', "
                    End If
                    If UCase(l_strProxyRequired) = "YES" Then
                        l_strSQL = l_strSQL & "proxyreq = 1, "
                    Else
                        l_strSQL = l_strSQL & "proxyreq = 0, "
                    End If
                    l_strSQL = l_strSQL & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
                    If UCase(l_striTunesRequired) = "YES" Then
                        l_strSQL = l_strSQL & "itunes = 1, "
                    Else
                        l_strSQL = l_strSQL & "itunes = 0, "
                    End If
                    If UCase(l_strConform) = "YES" Then
                        l_strSQL = l_strSQL & "conform = 1 "
                    Else
                        l_strSQL = l_strSQL & "conform = 0 "
                    End If
                Case Else
                    l_blnError = True
            End Select
            If l_strSQL <> "" Then l_strSQL = l_strSQL & "WHERE tracker_svensk_itemID = " & TempStr & ";"
            
            If l_blnError = False And l_strSQL <> "" Then
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strEmailBody = l_strEmailBody & "Updated: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                If l_strAuditDescription <> "" Then
                    l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
                    l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from IMD: " & QuoteSanitise(l_strAuditDescription) & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            End If
                
        Else
                        
            'Check whether this is an existing item that is completed...
            l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "' AND series = '" & l_strSeries & "' "
            If l_strEpisode = "" Then
                l_strSQL = l_strSQL & " AND episode IS NULL"
            Else
                l_strSQL = l_strSQL & " AND episode = " & Val(l_strEpisode)
            End If
            If l_strRevisionNumber <> "" Then
                l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
            Else
                l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
            End If
            l_strSQL = l_strSQL & " AND companyID = 1330 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill <> 0 "
            TempStr = GetDataSQL(l_strSQL)
            If (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
                If Val(TempStr) <> 0 Then
                    MsgBox "Row " & l_lngXLRowCounter & " was an update for an item that we have already completed. Row not read in.", vbInformation, "Error...."
                Else
            
                    l_strSQL = "INSERT INTO tracker_svensk_item ("
                    If UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE" Then
                        If UCase(l_strComponentType) = "AUDIO" Then
                            Select Case UCase(l_strLanguage)
                                Case "ENGLISH"
                                    l_strSQL = l_strSQL & "englishsepaudio, "
                                Case "NORWEGIAN"
                                    l_strSQL = l_strSQL & "norwegiansepaudio, "
                                Case "SWEDISH"
                                    l_strSQL = l_strSQL & "swedishsepaudio, "
                                Case "DANISH"
                                    l_strSQL = l_strSQL & "danishsepaudio, "
                                Case "FINNISH"
                                    l_strSQL = l_strSQL & "finnishsepaudio, "
                                Case "ICELANDIC"
                                    l_strSQL = l_strSQL & "icelandicsepaudio, "
                                Case Else
                                    MsgBox "Line " & l_lngXLRowCounter & ": Unidentified Language - '" & l_strLanguage & "' in Audio or Subtitle file", vbCritical, "Line not read in"
                                    l_blnError = True
                            End Select
                        ElseIf UCase(Left(l_strComponentType, 8)) = "SUBTITLE" Then
                            Select Case UCase(l_strLanguage)
                                Case "ENGLISH"
                                    l_strSQL = l_strSQL & "englishsubs, "
                                Case "NORWEGIAN"
                                    l_strSQL = l_strSQL & "norwegiansubs, "
                                Case "SWEDISH"
                                    l_strSQL = l_strSQL & "swedishsubs, "
                                Case "DANISH"
                                    l_strSQL = l_strSQL & "danishsubs, "
                                Case "FINNISH"
                                    l_strSQL = l_strSQL & "finnishsubs, "
                                Case "ICELANDIC"
                                    l_strSQL = l_strSQL & "icelandicsubs, "
                                Case Else
                                    MsgBox "Line " & l_lngXLRowCounter & ": Unidentified Language - '" & l_strLanguage & "' in Audio or Subtitle file", vbCritical, "Line not read in"
                                    l_blnError = True
                            End Select
                        End If
                    End If
                    If UCase(l_strTextlessRequired) = "YES" Then l_strSQL = l_strSQL & "textlessrequired, "
                    If UCase(l_strMERequired) = "YES" Then l_strSQL = l_strSQL & "merequired, "
                    If UCase(l_strME51Required) = "YES" Then l_strSQL = l_strSQL & "me51required, "
                    If UCase(l_strProxyRequired) = "YES" Then l_strSQL = l_strSQL & "proxyreq, "
                    If l_strFramerate <> "" Then l_strSQL = l_strSQL & "requestedframerate, "
                    If l_strBarcode <> "" Then l_strSQL = l_strSQL & "barcode, "
                    If l_strRevisionNumber <> "" Then l_strSQL = l_strSQL & "RevisionNumber, "
                    If l_strUrgent <> "" Then l_strSQL = l_strSQL & "urgent, "
                    If l_strSpecialProject <> "" Then l_strSQL = l_strSQL & "SpecialProject, "
                    l_strSQL = l_strSQL & "cdate, mdate, UniqueID, companyID, projectnumber, projectmanager, projectmanagercontactID, rightsowner, duedate, programtype, title, series, "
                    l_strSQL = l_strSQL & "subtitle, episode, componenttype, language, alphadisplaycode, conform) VALUES ("
                    If UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE" Then
                        l_strSQL = l_strSQL & "1, "
                    End If
                    If UCase(l_strTextlessRequired) = "YES" Then l_strSQL = l_strSQL & "1, "
                    If UCase(l_strMERequired) = "YES" Then l_strSQL = l_strSQL & "1, "
                    If UCase(l_strME51Required) = "YES" Then l_strSQL = l_strSQL & "1, "
                    If UCase(l_strProxyRequired) = "YES" Then l_strSQL = l_strSQL & "1, "
                    If l_strFramerate <> "" Then l_strSQL = l_strSQL & "'" & l_strFramerate & "', "
                    If l_strBarcode <> "" Then l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
                    If l_strRevisionNumber <> "" Then l_strSQL = l_strSQL & "'" & l_strRevisionNumber & "', "
                    If l_strUrgent <> "" Then l_strSQL = l_strSQL & "-1, "
                    If l_strSpecialProject <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSpecialProject) & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & l_strSFUniqueID & "', "
                    l_strSQL = l_strSQL & "1330, "
                    l_strSQL = l_strSQL & "'" & l_strProjectNumber & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProjectManager) & "', "
                    If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1330);"))) <> 0 Then
                        l_strSQL = l_strSQL & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
                    Else
                        l_strSQL = l_strSQL & GetData("contact", "contactID", "name", "DADC MIS") & ", "
                    End If
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRightsOwner) & "', "
                    If l_datDueDate > Now Then
                        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
                    Else
                        MsgBox "Line " & l_lngXLRowCounter & " has a due date earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                        l_blnError = True
                    End If
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgramType) & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
                    l_strSQL = l_strSQL & "'" & l_strSeries & "', "
                    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
                    If l_strEpisode <> "" Then
                        l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
                    Else
                        l_strSQL = l_strSQL & "NULL, "
                    End If
                    l_strSQL = l_strSQL & "'" & l_strComponentType & "', "
                    l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
                    l_strSQL = l_strSQL & "'" & l_strAlphaDisplayCode & "', "
                    If UCase(l_strConform) = "YES" Then
                        l_strSQL = l_strSQL & "1); "
                    Else
                        l_strSQL = l_strSQL & "0); "
                    End If
                    If l_blnError = False Then
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                        TempStr = g_lngLastID
                        l_strEmailBody = l_strEmailBody & "Added: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                        l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & g_lngLastID & ", getdate(), '" & g_strFullUserName & "', 'Insert from IMD');"
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                End If
            Else
                If Not (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
                    'Odd Component type
                    MsgBox "Line " & l_lngXLRowCounter & " has an unrecognised Component Type '" & l_strComponentType & "' - Line not read in.", vbCritical, "Error Reading Sheet"
                Else
                    MsgBox "Line " & l_lngXLRowCounter & " had an unrecognised error - Line not read in.", vbCritical, "Error Reading Sheet"
                End If
                l_blnError = True
            End If
        
            If l_blnError = False And l_strComment <> "" And Val(TempStr) <> 0 Then
                l_strSQL = "INSERT INTO tracker_svensk_comment(tracker_svensk_itemID, cdate, cuser, comment) VALUES ("
                l_strSQL = l_strSQL & Val(TempStr) & ", "
                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                l_strSQL = l_strSQL & "'DADC FIX JOB', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strComment) & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                SetData "tracker_svensk_item", "rejected", "tracker_svensk_itemID", Val(TempStr), 1
                SetData "tracker_svensk_item", "rejecteddate", "tracker_svensk_itemID", Val(TempStr), FormatSQLDate(Now)
            End If
        
        End If
            
    Else
        If ValidateEnglishCharacters(l_strProjectManager) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Project Manager' column - Line not read in.", vbCritical, "Error Reading Sheet"
        If ValidateEnglishCharacters(l_strTitle) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Title' column - Line not read in.", vbCritical, "Error Reading Sheet"
        If ValidateEnglishCharacters(l_strSubTitle) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Episode Title' column - Line not read in.", vbCritical, "Error Reading Sheet"
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new items were added to the DADC Svensk Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1330) & "' AND trackermessageID = 32;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Svensk Ingest Tracker Items Added or Updated", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdDADCSvenskInboundData_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strSubTitle As String, l_strEpisode As String, l_strComponentType As String, l_strConform As String, l_strBarcode As String, l_strAlphaDisplayCode As String
Dim l_strLanguage As String, l_strProjectNumber As String, l_strProjectManager As String, l_strRightsOwner As String, l_strProgramType As String, l_strSeries As String, l_strFramerate As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset, l_strProxyRequired As String, l_strFixNotes As String
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngTrackerID As Long

Dim l_strSQL As String, TempStr As String, l_blnError As Boolean, l_strSFUniqueID As String, l_strOldLanguage As String, l_strTextlessRequired As String, l_striTunesRequired As String
Dim l_strAuditDescription As String, l_strRevisionNumber As String, l_strUrgent As String, l_strMERequired As String, l_strME51Required As String, l_strSpecialProject As String

Dim l_blnFoundOne As Boolean

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "SF Inbound Material Data")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 5))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 8) <> ""
    
    l_blnError = False
    l_strProjectNumber = Trim(" " & GetXLData(l_lngXLRowCounter, 1)) 'A
    If l_strProjectNumber = "" Then l_strProjectNumber = "999999"
    l_strProjectManager = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_strRightsOwner = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strDueDate = Trim(GetXLData(l_lngXLRowCounter, 6)) 'F
    If IsDate(l_strDueDate) Then l_datDueDate = l_strDueDate
    l_strProgramType = Trim(GetXLData(l_lngXLRowCounter, 8)) 'H
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 9)) 'I
    l_strSeries = Trim(GetXLData(l_lngXLRowCounter, 10)) 'J
    l_strSubTitle = Trim(GetXLData(l_lngXLRowCounter, 11)) 'K
    l_strEpisode = Trim(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strComponentType = Trim(GetXLData(l_lngXLRowCounter, 13)) 'M
    l_strLanguage = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N
    l_strConform = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_strProxyRequired = Trim(GetXLData(l_lngXLRowCounter, 16)) 'P
    l_striTunesRequired = Trim(GetXLData(l_lngXLRowCounter, 17)) 'Q
    l_strTextlessRequired = Trim(GetXLData(l_lngXLRowCounter, 18)) 'R
    l_strMERequired = Trim(GetXLData(l_lngXLRowCounter, 19)) 'S
    l_strME51Required = Trim(GetXLData(l_lngXLRowCounter, 20)) 'T
    l_strAuditDescription = Trim(GetXLData(l_lngXLRowCounter, 21)) 'U
    l_strFramerate = Trim(GetXLData(l_lngXLRowCounter, 22)) 'V
    l_strAlphaDisplayCode = Trim(GetXLData(l_lngXLRowCounter, 24)) 'X
    l_strRevisionNumber = Trim(GetXLData(l_lngXLRowCounter, 25)) 'Y
    l_strUrgent = Trim(GetXLData(l_lngXLRowCounter, 26)) 'Z
    l_strSpecialProject = Trim(GetXLData(l_lngXLRowCounter, 27)) 'AA
    lblProgress.Caption = "Row:" & l_lngXLRowCounter & ", " & l_strProjectNumber & " - " & l_strTitle & " - " & l_strComponentType
    l_strSFUniqueID = l_strProjectNumber & "_" & l_strTitle & "_" & l_strProgramType & "_" & Format(Val(l_strSeries), "00") & "_" & Format(Val(l_strEpisode), "00")
    
    If ValidateEnglishCharacters(l_strProjectManager) = True And ValidateEnglishCharacters(l_strTitle) = True And ValidateEnglishCharacters(l_strSubTitle) = True Then ' And ValidateEnglishCharacters(l_strRightsOwner) = True
        l_blnFoundOne = False
        'Test whether a constructable line of this item already exists
        l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
        If l_strSeries <> "" Then
            l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
        End If
        If l_strEpisode = "" Then
            l_strSQL = l_strSQL & " AND episode IS NULL"
        Else
            l_strSQL = l_strSQL & " AND episode = '" & l_strEpisode & "'"
        End If
        If l_strRevisionNumber <> "" Then
            l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
        Else
            l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
        End If
        If l_strComponentType <> "" Then
            l_strSQL = l_strSQL & " AND componenttype = '" & l_strComponentType & "'"
        Else
            l_strSQL = l_strSQL & " AND componenttype IS NULL"
        End If
        If l_strLanguage <> "" Then
            l_strSQL = l_strSQL & " AND language = '" & l_strLanguage & "'"
        Else
            l_strSQL = l_strSQL & " AND language IS NULL"
        End If
        l_strSQL = l_strSQL & " AND companyID = 1415 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill = 0;"
        Debug.Print l_strSQL
        TempStr = GetDataSQL(l_strSQL)
        
        If Val(TempStr) <> 0 And (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
            
            l_blnFoundOne = True
            l_strSQL = ""
            l_strSQL = Construct_Svensk_Update_String(TempStr, l_strAuditDescription, l_blnError, l_strAlphaDisplayCode, l_strRevisionNumber, l_strUrgent, _
            l_datDueDate, l_strProjectManager, l_strRightsOwner, l_strSpecialProject, l_strProxyRequired, l_strMERequired, l_strME51Required, l_strSubTitle, l_striTunesRequired, l_strConform, l_lngXLRowCounter)
            
            If l_strSQL <> "" Then l_strSQL = l_strSQL & " WHERE tracker_svensk_itemID = " & TempStr & ";"
            
            If l_blnError = False And l_strSQL <> "" Then
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strEmailBody = l_strEmailBody & "Updated: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                If l_strAuditDescription <> "" Then
                    l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
                    l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from IMD: " & QuoteSanitise(l_strAuditDescription) & "');"
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                End If
            End If
                
        End If
                        
        If l_blnFoundOne = False And (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER") Then
        
            'Test whether a constructable line exists for an audio item in this language if this is a Video or Trailer item
        
            l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
            If l_strSeries <> "" Then
                l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
            End If
            If l_strEpisode = "" Then
                l_strSQL = l_strSQL & " AND episode IS NULL"
            Else
                l_strSQL = l_strSQL & " AND episode = '" & l_strEpisode & "'"
            End If
            If l_strRevisionNumber <> "" Then
                l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
            Else
                l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
            End If
            l_strSQL = l_strSQL & " AND componenttype = 'audio'"
            If l_strLanguage <> "" Then
                l_strSQL = l_strSQL & " AND language = '" & l_strLanguage & "'"
            Else
                l_strSQL = l_strSQL & " AND language IS NULL"
            End If
            l_strSQL = l_strSQL & " AND companyID = 1415 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill = 0;"
            Debug.Print l_strSQL
            TempStr = GetDataSQL(l_strSQL)
            If Val(TempStr) <> 0 Then
            
                l_blnFoundOne = True
                l_strSQL = ""
                l_strSQL = Construct_Svensk_Update_String(TempStr, l_strAuditDescription, l_blnError, l_strAlphaDisplayCode, l_strRevisionNumber, l_strUrgent, _
                l_datDueDate, l_strProjectManager, l_strRightsOwner, l_strSpecialProject, l_strProxyRequired, l_strMERequired, l_strME51Required, l_strSubTitle, l_striTunesRequired, l_strConform, l_lngXLRowCounter)
                
                'Then add a change of component to make it match the video (or trailer)
                If l_strSQL <> "" Then
                    l_strSQL = l_strSQL & ", componenttype = '" & l_strComponentType & "' "
                    l_strAuditDescription = l_strAuditDescription & "componenttype = '" & l_strComponentType & "', "
                End If
                If l_strSQL <> "" Then l_strSQL = l_strSQL & " WHERE tracker_svensk_itemID = " & TempStr & ";"
                
                If l_blnError = False And l_strSQL <> "" Then
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strEmailBody = l_strEmailBody & "Updated: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                    If l_strAuditDescription <> "" Then
                        l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
                        l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from IMD: " & QuoteSanitise(l_strAuditDescription) & "');"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                End If
            
            End If
        
        End If
            
        If l_blnFoundOne = False And (UCase(l_strComponentType) = "AUDIO") Then
        
            'Test whether a constructable line exists for an video item in this language if this is a Audio or Subs item
        
            l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "'"
            If l_strSeries <> "" Then
                l_strSQL = l_strSQL & " AND series = '" & l_strSeries & "' "
            End If
            If l_strEpisode = "" Then
                l_strSQL = l_strSQL & " AND episode IS NULL"
            Else
                l_strSQL = l_strSQL & " AND episode = '" & l_strEpisode & "'"
            End If
            If l_strRevisionNumber <> "" Then
                l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
            Else
                l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
            End If
            l_strSQL = l_strSQL & " AND componenttype = 'video'"
            If l_strLanguage <> "" Then
                l_strSQL = l_strSQL & " AND language = '" & l_strLanguage & "'"
            Else
                l_strSQL = l_strSQL & " AND language IS NULL"
            End If
            l_strSQL = l_strSQL & " AND companyID = 1415 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill = 0;"
            Debug.Print l_strSQL
            TempStr = GetDataSQL(l_strSQL)
            If Val(TempStr) <> 0 Then
            
                l_blnFoundOne = True
                l_strSQL = ""
                l_strSQL = Construct_Svensk_Update_String(TempStr, l_strAuditDescription, l_blnError, l_strAlphaDisplayCode, l_strRevisionNumber, l_strUrgent, _
                l_datDueDate, l_strProjectManager, l_strRightsOwner, l_strSpecialProject, l_strProxyRequired, l_strMERequired, l_strME51Required, l_strSubTitle, l_striTunesRequired, l_strConform, l_lngXLRowCounter)
                
                If l_strSQL <> "" Then l_strSQL = l_strSQL & " WHERE tracker_svensk_itemID = " & TempStr & ";"
                
                If l_blnError = False And l_strSQL <> "" Then
                    Debug.Print l_strSQL
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_strEmailBody = l_strEmailBody & "Updated: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                    If l_strAuditDescription <> "" Then
                        l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
                        l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from IMD: " & QuoteSanitise(l_strAuditDescription) & "');"
                        Debug.Print l_strSQL
                        ExecuteSQL l_strSQL, g_strExecuteError
                        CheckForSQLError
                    End If
                End If
            
            End If
        
        End If
            
        If l_blnFoundOne = False And (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
            
            l_blnFoundOne = True
            'Check whether this is an existing item that is completed...
            l_strSQL = "SELECT tracker_svensk_itemID FROM tracker_svensk_item WHERE title='" & QuoteSanitise(l_strTitle) & "' AND series = '" & l_strSeries & "' "
            If l_strEpisode = "" Then
                l_strSQL = l_strSQL & " AND episode IS NULL"
            Else
                l_strSQL = l_strSQL & " AND episode = " & Val(l_strEpisode)
            End If
            If l_strRevisionNumber <> "" Then
                l_strSQL = l_strSQL & " AND RevisionNumber = '" & l_strRevisionNumber & "'"
            Else
                l_strSQL = l_strSQL & " AND RevisionNumber IS NULL"
            End If
            l_strSQL = l_strSQL & " AND companyID = 1415 AND ProgramType = '" & l_strProgramType & "' and projectnumber = '" & l_strProjectNumber & "' AND readytobill <> 0 "
            Debug.Print l_strSQL
            TempStr = GetDataSQL(l_strSQL)

            If Val(TempStr) <> 0 Then
                MsgBox "Row " & l_lngXLRowCounter & " was an update for an item that we have already completed. New Line will be made - please update the revision information manually.", vbInformation, "Error......"
            End If
            
            l_blnError = False
            l_strSQL = "INSERT INTO tracker_svensk_item ("
            If Left(UCase(l_strTextlessRequired), 1) = "Y" Then l_strSQL = l_strSQL & "textlessrequired, "
            If Left(UCase(l_strMERequired), 1) = "Y" Then l_strSQL = l_strSQL & "merequired, "
            If Left(UCase(l_strME51Required), 1) = "Y" Then l_strSQL = l_strSQL & "me51required, "
            If Left(UCase(l_strProxyRequired), 1) = "Y" Then l_strSQL = l_strSQL & "proxyreq, "
            If Left(UCase(l_striTunesRequired), 1) = "Y" Then l_strSQL = l_strSQL & "iTunes, "
            If l_strFramerate <> "" Then l_strSQL = l_strSQL & "requestedframerate, "
            If l_strBarcode <> "" Then l_strSQL = l_strSQL & "barcode, "
            If l_strRevisionNumber <> "" Then l_strSQL = l_strSQL & "RevisionNumber, "
            If l_strUrgent <> "" Then l_strSQL = l_strSQL & "urgent, "
            If l_strSpecialProject <> "" Then l_strSQL = l_strSQL & "SpecialProject, "
            l_strSQL = l_strSQL & "cdate, mdate, UniqueID, companyID, projectnumber, projectmanager, projectmanagercontactID, rightsowner, duedate, programtype, title, series, "
            l_strSQL = l_strSQL & "subtitle, episode, componenttype, language, alphadisplaycode, conform) VALUES ("
            If Left(UCase(l_strTextlessRequired), 1) = "Y" Then l_strSQL = l_strSQL & "1, "
            If Left(UCase(l_strMERequired), 1) = "Y" Then l_strSQL = l_strSQL & "1, "
            If Left(UCase(l_strME51Required), 1) = "Y" Then l_strSQL = l_strSQL & "1, "
            If Left(UCase(l_strProxyRequired), 1) = "Y" Then l_strSQL = l_strSQL & "1, "
            If Left(UCase(l_striTunesRequired), 1) = "Y" Then l_strSQL = l_strSQL & "1, "
            If l_strFramerate <> "" Then l_strSQL = l_strSQL & "'" & l_strFramerate & "', "
            If l_strBarcode <> "" Then l_strSQL = l_strSQL & "'" & l_strBarcode & "', "
            If l_strRevisionNumber <> "" Then l_strSQL = l_strSQL & "'" & l_strRevisionNumber & "', "
            If l_strUrgent <> "" Then l_strSQL = l_strSQL & "-1, "
            If l_strSpecialProject <> "" Then l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSpecialProject) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & l_strSFUniqueID & "', "
            l_strSQL = l_strSQL & "1415, "
            l_strSQL = l_strSQL & "'" & l_strProjectNumber & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProjectManager) & "', "
            If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1415);"))) <> 0 Then
                l_strSQL = l_strSQL & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
            Else
                l_strSQL = l_strSQL & GetData("contact", "contactID", "name", "DADC MIS") & ", "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRightsOwner) & "', "
            If l_datDueDate > Now Then
                l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
            Else
                MsgBox "Line " & l_lngXLRowCounter & " has a due date that is before Now - Line not read in.", vbCritical, "Error Reading Sheet"
                l_blnError = True
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProgramType) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & "'" & l_strSeries & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
            If l_strEpisode <> "" Then
                l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            l_strSQL = l_strSQL & "'" & l_strComponentType & "', "
            l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
            l_strSQL = l_strSQL & "'" & l_strAlphaDisplayCode & "', "
            If UCase(l_strConform) = "YES" Then
                l_strSQL = l_strSQL & "1); "
            Else
                l_strSQL = l_strSQL & "0); "
            End If
            If l_blnError = False Then
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                TempStr = g_lngLastID
                l_strEmailBody = l_strEmailBody & "Added: " & l_strTitle & ", Ep " & l_strEpisode & ", " & l_strSubTitle & ", Lang: " & l_strLanguage & ", Component Type: " & l_strComponentType & ", Due Date: " & l_datDueDate & ", for: " & l_strProjectManager & vbCrLf
                l_strSQL = "INSERT INTO tracker_svensk_audit (tracker_svensk_itemID, mdate, muser, description) VALUES (" & g_lngLastID & ", getdate(), '" & g_strFullUserName & "', 'Insert from IMD');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
        End If
        If l_blnFoundOne = False Then
            If Not (UCase(l_strComponentType) = "VIDEO" Or UCase(l_strComponentType) = "TRAILER" Or UCase(l_strComponentType) = "AUDIO" Or UCase(Left(l_strComponentType, 8)) = "SUBTITLE") Then
                'Odd Component type
                MsgBox "Line " & l_lngXLRowCounter & " has an unrecognised Component Type '" & l_strComponentType & "' - Line not read in.", vbCritical, "Error Reading Sheet"
            Else
                MsgBox "Line " & l_lngXLRowCounter & " had an unrecognised error - Line not read in.", vbCritical, "Error Reading Sheet"
            End If
            l_blnError = True
        End If
        
    Else
        If ValidateEnglishCharacters(l_strProjectManager) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Project Manager' column - Line not read in.", vbCritical, "Error Reading Sheet"
        If ValidateEnglishCharacters(l_strTitle) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Title' column - Line not read in.", vbCritical, "Error Reading Sheet"
        If ValidateEnglishCharacters(l_strSubTitle) = False Then MsgBox "Line " & l_lngXLRowCounter & " has non-English letters in the 'Episode Title' column - Line not read in.", vbCritical, "Error Reading Sheet"
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new items were added to the DADC Svensk Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1415) & "' AND trackermessageID = 32;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Svensk Ingest Tracker Items Added or Updated", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdDHCourier_Click()

If MsgBox("Are all the tracker entries covered by this sheet already contract billed?", vbYesNo, "Reading Skibbly Courier Costs") = vbNo Then Exit Sub

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String

Dim l_strXLDate As String, l_lngXLJCADespatchNumber As Long, l_lngJCADespatchID As Long, l_dblCourierCost As Double, l_lngXLRowCounter As Long, l_strWaybillNumber As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_strSQL As String

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Wellcome Excel Read", "Invoice ??????")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 3
l_strXLDate = ""

l_strXLDate = GetXLData(l_lngXLRowCounter, 1)

While l_strXLDate <> "TOTAL"
    
    'Read the relevant items from the XL line
    l_lngXLJCADespatchNumber = Val(GetXLData(l_lngXLRowCounter, 2))
    If l_lngXLJCADespatchNumber <> 0 Then
        
        l_lngJCADespatchID = GetData("despatch", "despatchID", "despatchnumber", l_lngXLJCADespatchNumber)
        l_dblCourierCost = Val(Mid(GetXLData(l_lngXLRowCounter, 10), 2))
        l_strWaybillNumber = GetXLData(l_lngXLRowCounter, 6)
                
        If l_dblCourierCost <> 0 Then
            
            lblProgress.Caption = l_lngXLJCADespatchNumber
            DoEvents
        
            'Enter the data into a Skibbly tracker line
            SetData "skibblytracker", "waybillnumber", "despatchID", l_lngJCADespatchID, l_strWaybillNumber
            SetData "skibblytracker", "despatchcost", "despatchID", l_lngJCADespatchID, l_dblCourierCost
            SetData "skibblytracker", "readytobilldelivery", "despatchID", l_lngJCADespatchID, -1
        
        End If
    
    End If
    
    'Increment the count and loop
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    l_strXLDate = GetXLData(l_lngXLRowCounter, 1)

Wend

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
Beep
DoEvents

End Sub

Private Sub cmdDisneyImport_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strRequirement As String, l_strElementNumber As String, l_strAgileNumber As String, l_strOrderNumber As String
Dim l_lngDisneyContactID As Long, l_strJCAcontact As String, l_datDueDate As Date, l_datDateToOrder As Date, l_datDateRequested As Date
Dim l_datReferredToDisney As Date, l_datMaterialsIn As Date, l_datReorderDate As Date

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

l_datDueDate = 0
l_datDateToOrder = 0
l_datDateRequested = 0
l_datReferredToDisney = 0
l_datMaterialsIn = 0
l_datReorderDate = 0

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "JR delivery schedule"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 5

While GetXLData(l_lngXLRowCounter, 2) <> ""
    
    l_strTitle = GetXLData(l_lngXLRowCounter, 2)
    l_strRequirement = GetXLData(l_lngXLRowCounter, 3)
    l_strElementNumber = GetXLData(l_lngXLRowCounter, 4)
    l_strAgileNumber = GetXLData(l_lngXLRowCounter, 5)
    l_strOrderNumber = GetXLData(l_lngXLRowCounter, 6)
    l_lngDisneyContactID = Val(GetAlias(GetXLData(l_lngXLRowCounter, 7)))
    l_strJCAcontact = GetXLData(l_lngXLRowCounter, 8)
    If IsDate(GetXLData(l_lngXLRowCounter, 9)) Then l_datDueDate = GetXLData(l_lngXLRowCounter, 9) Else l_datDueDate = 0
    If IsDate(GetXLData(l_lngXLRowCounter, 10)) Then l_datDateToOrder = GetXLData(l_lngXLRowCounter, 10) Else l_datDateToOrder = 0
    If IsDate(GetXLData(l_lngXLRowCounter, 11)) Then l_datDateRequested = GetXLData(l_lngXLRowCounter, 11) Else l_datDateRequested = 0
    If IsDate(GetXLData(l_lngXLRowCounter, 13)) Then l_datReferredToDisney = GetXLData(l_lngXLRowCounter, 13) Else l_datReferredToDisney = 0
    If IsDate(GetXLData(l_lngXLRowCounter, 14)) Then l_datMaterialsIn = GetXLData(l_lngXLRowCounter, 14) Else l_datMaterialsIn = 0
    If IsDate(GetXLData(l_lngXLRowCounter, 15)) Then l_datReorderDate = GetXLData(l_lngXLRowCounter, 15) Else l_datReorderDate = 0
    
    lblProgress.Caption = l_strTitle
    DoEvents
    
    l_strSQL = "INSERT INTO disneytracker (companyID, title, requirement,  elementnumber, agilenumber, ordernumber, disneycontactID, jcacontact, duedate, datetoorderfromJR, "
    l_strSQL = l_strSQL & "daterequestedfromJR, referredtodisney, materialsindate, reorderdelivery) VALUES("
    l_strSQL = l_strSQL & "1163, "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRequirement) & "', "
    l_strSQL = l_strSQL & "'" & l_strElementNumber & "', "
    l_strSQL = l_strSQL & "'" & l_strAgileNumber & "', "
    l_strSQL = l_strSQL & "'" & l_strOrderNumber & "', "
    l_strSQL = l_strSQL & l_lngDisneyContactID & ", "
    l_strSQL = l_strSQL & "'" & l_strJCAcontact & "', "
    If l_datDueDate <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    If l_datDateToOrder <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDateToOrder) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    
    If l_datDateRequested <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDateRequested) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    
    If l_datReferredToDisney <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datReferredToDisney) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    
    If l_datMaterialsIn <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datMaterialsIn) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    
    If l_datReorderDate <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datReorderDate) & "', "
    Else
        l_strSQL = l_strSQL & "Null"
    End If
    
    
    l_strSQL = l_strSQL & ");"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdDisneyTracker_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strAgileNumber As String, l_strStatus As String, l_strFilename As String, l_strWMLSNumber As String

Dim l_strSQL As String, l_datMaterialsInDate As Date

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "JCA"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strAgileNumber = GetXLData(l_lngXLRowCounter, 1)
    l_strFilename = GetXLData(l_lngXLRowCounter, 2)
    l_strStatus = GetXLData(l_lngXLRowCounter, 6)
    l_strWMLSNumber = GetXLData(l_lngXLRowCounter, 8)
    
    lblProgress.Caption = l_strAgileNumber
    DoEvents
    
    If Val(l_strAgileNumber) <> 0 Then
        
        Debug.Print Val(l_strAgileNumber)
        If l_strStatus = "complete" Then
            
            If GetData("disneytracker", "disneytrackerID", "agilenumber", Val(l_strAgileNumber)) <> 0 Then
                If GetData("disneytracker", "materialinplace", "agilenumber", Val(l_strAgileNumber)) = 0 Then
                    SetData "disneytracker", "materialsindate", "agilenumber", Val(l_strAgileNumber), FormatSQLDate(Now)
                    SetData "disneytracker", "materialinplace", "agilenumber", Val(l_strAgileNumber), -1
                    SetData "disneytracker", "elementnumber", "agilenumber", Val(l_strAgileNumber), l_strWMLSNumber
                    MakeDisneyAgileClip "JELLYROLL", l_strAgileNumber, l_strFilename, l_strWMLSNumber
                End If
            ElseIf GetData("disneytracker", "disneytrackerID", "elementnumber", Val(l_strWMLSNumber)) <> 0 Then
                If GetData("disneytracker", "materialinplace", "elementnumber", Val(l_strWMLSNumber)) = 0 Then
                    SetData "disneytracker", "materialsindate", "elementnumber", Val(l_strWMLSNumber), FormatSQLDate(Now)
                    SetData "disneytracker", "materialinplace", "elementnumber", Val(l_strWMLSNumber), -1
                    SetData "disneytracker", "agilenumber", "elementnumber", Val(l_strWMLSNumber), l_strAgileNumber
                    MakeDisneyAgileClip "JELLYROLL", l_strAgileNumber, l_strFilename, l_strWMLSNumber
                End If
            Else
                MsgBox "The file " & l_strFilename & ", Agile number: " & l_strAgileNumber & " has been listed as arrived on Jellyroll, " & vbCrLf & _
                    "but neither the Agile number or the WMLS number is listed in our Disney Tracker", vbInformation, "Unexpected File Found."
            End If
        
        End If
    
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

l_strExcelSheetName = "JCATELECITY"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strAgileNumber = GetXLData(l_lngXLRowCounter, 1)
    l_strFilename = GetXLData(l_lngXLRowCounter, 2)
    l_strStatus = GetXLData(l_lngXLRowCounter, 6)
    l_strWMLSNumber = GetXLData(l_lngXLRowCounter, 8)
    
    lblProgress.Caption = l_strAgileNumber
    DoEvents
    
    If Val(l_strAgileNumber) <> 0 Then
        
        Debug.Print Val(l_strAgileNumber)
        If l_strStatus = "complete" Then
            
            If GetData("disneytracker", "disneytrackerID", "agilenumber", Val(l_strAgileNumber)) <> 0 Then
                If GetData("disneytracker", "materialinplace", "agilenumber", Val(l_strAgileNumber)) = 0 Then
                    SetData "disneytracker", "materialsindate", "agilenumber", Val(l_strAgileNumber), FormatSQLDate(Now)
                    SetData "disneytracker", "materialinplace", "agilenumber", Val(l_strAgileNumber), -1
                    SetData "disneytracker", "elementnumber", "agilenumber", Val(l_strAgileNumber), l_strWMLSNumber
                    MakeDisneyAgileClip "JELLYROLL2", l_strAgileNumber, l_strFilename, l_strWMLSNumber
                End If
            ElseIf GetData("disneytracker", "disneytrackerID", "elementnumber", Val(l_strWMLSNumber)) <> 0 Then
                If GetData("disneytracker", "materialinplace", "elementnumber", Val(l_strWMLSNumber)) = 0 Then
                    SetData "disneytracker", "materialsindate", "elementnumber", Val(l_strWMLSNumber), FormatSQLDate(Now)
                    SetData "disneytracker", "materialinplace", "elementnumber", Val(l_strWMLSNumber), -1
                    SetData "disneytracker", "agilenumber", "elementnumber", Val(l_strWMLSNumber), l_strAgileNumber
                    MakeDisneyAgileClip "JELLYROLL2", l_strAgileNumber, l_strFilename, l_strWMLSNumber
                End If
            Else
                MsgBox "The file " & l_strFilename & ", Agile number: " & l_strAgileNumber & " has been listed as arrived on Jellyroll-2, " & vbCrLf & _
                    "but neither the Agile number or the WMLS number is listed in our Disney Tracker", vbInformation, "Unexpected File Found."
            End If
        
        End If
    
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdFremantle_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, lo_strSQL As String
Dim l_strTitle As String, l_strSeries As String, l_strEpisode As String

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "All Phases"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2

While GetXLData(l_lngXLRowCounter, 2) <> ""

    l_strTitle = GetXLData(l_lngXLRowCounter, 2)
    l_strSeries = GetXLData(l_lngXLRowCounter, 3)
    l_strEpisode = GetXLData(l_lngXLRowCounter, 4)
    
    lblProgress.Caption = l_strTitle & " - series " & l_strSeries & ", Ep. " & l_strEpisode
    DoEvents
    
    l_strSQL = "INSERT INTO tracker_item (companyID, headertext2, headerint1, headerint2) VALUES ("
    l_strSQL = l_strSQL & "1162, '" & QuoteSanitise(l_strTitle) & "', "
    l_strSQL = l_strSQL & Val(l_strSeries) & ", "
    l_strSQL = l_strSQL & Val(l_strEpisode) & ");"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

lblProgress.Caption = ""
DoEvents

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep


End Sub

Private Sub cmdHDDtask_Click()

'If Not CheckAccess("/processexcelorders") Then
'    Exit Sub
'End If


Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long

Dim l_strClient As String, l_strTerritory As String, l_lngRequestID As Long, l_lngLineItemID As Long, l_datDueDate As Date
Dim l_datDoNotDeliverBeforeDate As Date, l_strRequestName As String, l_strScheduledBy As String, l_strComponentsAvailable As String
Dim l_strSeriesTitle As String, l_strEpisodeTitle As String, l_strEspisodeNumber As String, l_strCVDisplayName As String, l_strFileLocation As String
Dim l_strFilename As String, l_lngFileSizeGB As Double, l_strFormatRequiredType As String, l_strClientContact As String, l_strDestination As String
Dim l_strStatus As String, l_lngShippingMethodTypeID As Long, l_strTrackingNumber As String, l_strMediaRequiredType As String, l_strMediaType As String
Dim l_strMediaBarcodeNo As String, l_datShipDate As Date, l_strDepartmentChargeCode As String


Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

l_strClient = 0
l_strTerritory = 0
l_lngRequestID = 0
l_lngLineItemID = 0
l_datDueDate = 0
l_datDoNotDeliverBeforeDate = 0
l_strRequestName = 0
l_strScheduledBy = 0
l_strComponentsAvailable = 0
l_strSeriesTitle = 0
l_strEpisodeTitle = 0
l_strEspisodeNumber = 0
l_strCVDisplayName = 0
l_strFileLocation = 0
l_strFilename = 0
l_lngFileSizeGB = 0
l_strFormatRequiredType = 0
l_strClientContact = 0
l_strDestination = 0
l_strStatus = 0
l_lngShippingMethodTypeID = 0
l_strTrackingNumber = 0
l_strMediaRequiredType = 0
l_strMediaType = 0
l_strMediaBarcodeNo = 0
l_datShipDate = 0
l_strDepartmentChargeCode = 0


Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = "BBCW HDD Task Report"
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 4

While GetXLData(l_lngXLRowCounter, 2) <> ""
    
    l_strClient = Trim(GetXLData(l_lngXLRowCounter, 1))
    l_strTerritory = Trim(GetXLData(l_lngXLRowCounter, 2))
    l_lngRequestID = Trim(Val(GetXLData(l_lngXLRowCounter, 3)))
    l_lngLineItemID = Trim(Val(GetXLData(l_lngXLRowCounter, 4)))
 '   If IsDate(l_datDueDate = GetXLData(l_lngXLRowCounter, 5)) Then l_datDueDate = GetXLData(l_lngXLRowCounter, 5) Else l_datDueDate = 0
 '   If IsDate(l_datDoNotDeliverBeforeDate = GetXLData(l_lngXLRowCounter, 6)) Then l_datDoNotDeliverBeforeDate = GetXLData(l_lngXLRowCounter, 6) Else l_datDoNotDeliverBeforeDate = 0
    l_datDueDate = Trim(GetXLData(l_lngXLRowCounter, 5))
    l_datDoNotDeliverBeforeDate = Trim(GetXLData(l_lngXLRowCounter, 6))
    l_strRequestName = Trim(GetXLData(l_lngXLRowCounter, 7))
    l_strScheduledBy = Trim(GetXLData(l_lngXLRowCounter, 8))
    l_strComponentsAvailable = Trim(GetXLData(l_lngXLRowCounter, 9))
    l_strSeriesTitle = Trim(GetXLData(l_lngXLRowCounter, 10))
    l_strEpisodeTitle = Trim(GetXLData(l_lngXLRowCounter, 11))
    l_strEspisodeNumber = Trim(GetXLData(l_lngXLRowCounter, 12))
    l_strCVDisplayName = Trim(GetXLData(l_lngXLRowCounter, 13))
    l_strFileLocation = Trim(GetXLData(l_lngXLRowCounter, 14))
    l_strFilename = Trim(GetXLData(l_lngXLRowCounter, 15))
    l_lngFileSizeGB = Trim(Val(GetXLData(l_lngXLRowCounter, 16)))
    l_strFormatRequiredType = Trim(GetXLData(l_lngXLRowCounter, 17))
    l_strClientContact = Trim(GetXLData(l_lngXLRowCounter, 18))
    l_strDestination = Trim(GetXLData(l_lngXLRowCounter, 19))
    l_strStatus = Trim(GetXLData(l_lngXLRowCounter, 20))
    l_lngShippingMethodTypeID = Trim(Val(GetXLData(l_lngXLRowCounter, 21)))
    l_strTrackingNumber = Trim(GetXLData(l_lngXLRowCounter, 22))
    l_strMediaRequiredType = Trim(GetXLData(l_lngXLRowCounter, 23))
    l_strMediaType = Trim(GetXLData(l_lngXLRowCounter, 24))
    l_strMediaBarcodeNo = Trim(GetXLData(l_lngXLRowCounter, 25))
    If IsDate(l_datShipDate = Trim(GetXLData(l_lngXLRowCounter, 26))) Then l_datShipDate = GetXLData(l_lngXLRowCounter, 26) Else l_datShipDate = 0
    l_strDepartmentChargeCode = Trim(GetXLData(l_lngXLRowCounter, 28))


    
    lblProgress.Caption = l_strClient
    DoEvents
    
    l_strSQL = "INSERT INTO tracker_hdd_task (CompanyID, Client, Territory, RequestID, LineItemID, DueDate, DoNotDeliverBeforeDate, RequestName, ScheduledBy, ComponentsAvailable, "
    l_strSQL = l_strSQL & "SeriesTitle, EpisodeTitle, EspisodeNumber, CVDisplayName, FileLocation, fileName, FileSizeGB, FormatRequiredType, ClientContact, Destination, Status, MediaRequiredType, DepartmentChargeCode)VALUES("
    'l_strSQL = l_strSQL & "ShippingMethodTypeID, TrackingNumber, MediaRequiredType, MediaType, MediaBarcodeNo, DepartmentChargeCode)VALUES("
   l_strSQL = l_strSQL & "1418, "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strClient) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTerritory) & "', "
    l_strSQL = l_strSQL & "'" & l_lngRequestID & "', "
    l_strSQL = l_strSQL & "'" & l_lngLineItemID & "', "
    If l_datDueDate <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    If l_datDoNotDeliverBeforeDate <> 0 Then
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDoNotDeliverBeforeDate) & "', "
    Else
        l_strSQL = l_strSQL & "Null, "
    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strRequestName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strScheduledBy) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strComponentsAvailable) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSeriesTitle) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEpisodeTitle) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strEspisodeNumber) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strCVDisplayName) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFileLocation) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFilename) & "', "
    l_strSQL = l_strSQL & "'" & l_lngFileSizeGB & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strFormatRequiredType) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strClientContact) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDestination) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strStatus) & "', "
'    l_strSQL = l_strSQL & "'" & l_lngShippingMethodTypeID & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTrackingNumber) & "', "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaRequiredType) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaType) & "', "
'    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strMediaBarcodeNo) & "', "
'    If l_datShipDate <> 0 Then
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datShipDate) & "', "
'    Else
'        l_strSQL = l_strSQL & "Null, "
'    End If
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strDepartmentChargeCode) & "'"

  
    
    l_strSQL = l_strSQL & ");"
  Debug.Print l_strSQL
    ExecuteSQL l_strSQL, g_strExecuteError
   ' CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep


End Sub

Private Sub cmdInclBBCWW_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If

'MsgBox "This routine not yet implemented.", vbInformation, "No activity Possible"

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset, l_rsxJobs As ADODB.Recordset
Dim l_rsxDetails As ADODB.Recordset, l_lngLibraryID As Long

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsxJobs = New ADODB.Recordset
Set l_rsxDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM [job] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM [jobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxJobs = ExecuteSQL("SELECT * FROM [extendedjob] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxDetails = ExecuteSQL("SELECT * FROM [extendedjobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

'get the fields and place them into variables
Dim l_BookedBy$, l_AccountNumber$, l_Telephone$, l_FACNumber$, l_jcacontact$, l_ClientName$, l_ContactName$, l_BookingNumber$
Dim l_Lastprogrammetitle$

'get the header details, i.e. Client name, etc.
l_AccountNumber$ = GetXLData(g_cntAccountNumberRow%, g_cntAccountNumberColumn%)
l_Telephone$ = GetXLData(g_cntTelephoneRow%, g_cntTelephoneColumn%)
l_ClientName$ = "BBC Worldwide"
l_ContactName$ = GetXLData(5, 6)
l_FACNumber$ = GetXLData(3, 2)
l_FACNumber$ = Mid(l_FACNumber$, 16, Len(l_FACNumber$))
l_jcacontact$ = GetXLData(7, 2)


'declare a variable for keeping track of how far down the page we are
Dim l_CurrentTop&

'set it to the first load of data
l_CurrentTop& = 10

'declare lots of variables for taking the values off the spreadsheet
Dim l_OrderNumber$, l_CustomerName$, l_ChargeCode$, l_BusinessAreaCode$, l_NominalCode$, l_ProgrammeTitle$, l_Requireddate$, l_Notes$
Dim l_MasterEpisodeNumber$, l_MasterSpoolNumber$, l_MasterLanguage$, l_MasterSpoolDuration$, l_MasterAspectRatio$, l_MasterTapeFormat$, l_MasterLineStandard$
Dim l_DupeEpisodeNumber$, l_DupeAudio$, l_DupeCeefax$, l_DupeTotalDuration$, l_DupeLanguage$, l_DupeQuantity$, l_DupeTapeFormat$, l_DupeLineStandard$, l_DupeAspectRatio$
Dim l_LastorderNumber$, l_PreviousJobNumber&, l_Original_MasterStandard$, l_Original_DupeStandard$, l_Saveflag%, l_Stk&, l_CalculatedDuration&, l_Mastercount&
Dim l_Orig_masterspool$, l_Orig_masterformat$, l_Orig_masterstandard$, l_Orig_masteraspect$, l_Orig_masterduration$, l_comma&, l_IndividualFAC$, l_ProgrammeNumber$
Dim l_CountryCode$, l_BusinessArea$, l_OldMasterSpoolNumber$, l_NewJobNumber&, l_BBCMasterTapeFormat$, l_BBCDupeTapeFormat$, l_EpisodeNumber$
Dim l_EpisodeCount As Long, l_CopyCount As Long, l_OrigCopyCount As Long, l_intExtraCopiesFromSameReplay As Integer

l_LastorderNumber$ = "CETA"
l_Saveflag% = False


'declare the Sound channels
Dim l_Sound1$, l_Sound2$, l_Sound3$, l_Sound4$, l_Sound5$, l_Sound6$, l_Sound7$, l_Sound8$

'start a loop that cycles through each tenth line
l_EpisodeCount = 1
l_CopyCount = 1
l_intExtraCopiesFromSameReplay = 0
Do
    
    'if there is no data in the Bookingnumber field then this is either one of those blue lines or the end of the order.
    'Check which and then exit the do loop if necessary. While there, count the number of episodes in the current group,
    'and enter the data for the last one, rather than any others.
    
    If GetXLData(l_CurrentTop&, 2) = "" Then
        l_CurrentTop& = l_CurrentTop& + 2
        l_MasterSpoolNumber$ = ""
        l_OldMasterSpoolNumber$ = "GINGER"
        If GetXLData(l_CurrentTop&, 2) = "" Then Exit Do
    End If
    
    l_EpisodeCount = 1
    l_CopyCount = 1
    l_Mastercount& = 1
    l_BookingNumber$ = GetXLData(l_CurrentTop&, 2)
    
    If l_BookingNumber$ = "" Then Exit Do
    
    l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
    
    'here is where we work out if there is two masters on this sheet, so check for a comma
    If InStr(l_MasterSpoolNumber$, ",") <> 0 Then
    
        'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
        
        l_Mastercount& = Countcommas(l_MasterSpoolNumber$) + 1
        
    End If
        
    l_Orig_masterspool$ = l_MasterSpoolNumber$
    l_Orig_masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
    l_Orig_masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
    l_Orig_masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
    l_Orig_masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)

    'go through the spreadsheet and retrieve the values and put them into
    'variables to be used later. These are from the Order Details section
    l_OrderNumber$ = GetXLData(l_CurrentTop& + 1, 2)
    l_IndividualFAC$ = GetXLData(l_CurrentTop& + 0, 2)
    l_CustomerName$ = GetXLData(l_CurrentTop& + 2, 2)
    l_ChargeCode$ = GetXLData(l_CurrentTop& + 3, 2)
    l_BusinessArea$ = GetXLData(l_CurrentTop& + 4, 2)
    l_BusinessAreaCode$ = GetXLData(l_CurrentTop& + 5, 2)
    l_NominalCode$ = GetXLData(l_CurrentTop& + 6, 2)
    l_ProgrammeTitle$ = GetXLData(l_CurrentTop& + 7, 2)
    l_ProgrammeNumber$ = GetXLData(l_CurrentTop& + 8, 2)
    l_CountryCode$ = GetXLData(l_CurrentTop& + 8, 4)
    l_EpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    l_Requireddate$ = GetXLData(l_CurrentTop& + 9, 2)
    l_Notes$ = GetXLData(l_CurrentTop& + 10, 2)
    
    'get the next chapter, which is Master Material
    l_MasterEpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    
    If l_Mastercount& = 1 Then
        l_MasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_MasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
        l_BBCMasterTapeFormat$ = GetXLData(l_CurrentTop& + 4, 4)
        l_MasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
        l_MasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
    End If
    
    l_MasterLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 7, 4))
    
    'increment the looper so we move to the next section  or empty line.
    l_CurrentTop& = l_CurrentTop& + 11
        
    If UCase(l_OrderNumber$) <> UCase(l_LastorderNumber$) Then
                    
        If l_LastorderNumber$ <> "STAR ORDER" Then
        
            'add a new record to the Jobs table
            l_rsJobs.AddNew
            l_rsJobs("createduserID") = 33
            l_rsJobs("createduser") = "Excel"
            l_rsJobs("createddate") = Now
            l_rsJobs("modifieddate") = Now
            l_rsJobs("modifieduserID") = 33
            l_rsJobs("modifieduser") = l_jcacontact$
            l_rsJobs("flagemail") = 1
            l_rsJobs("fd_status") = "Confirmed"
            l_rsJobs("jobtype") = "Dubbing"
            l_rsJobs("joballocation") = "Regular"
            l_rsJobs("deadlinedate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("despatchdate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("projectID") = 0
            l_rsJobs("productID") = 0
            If l_Telephone$ <> "" Then l_rsJobs("telephone") = Left$(l_Telephone$, 50)
'            Dim l_NewJobNumber&
'            l_NewJobNumber& = GetNextJobNumber()
'            l_PreviousJobNumber& = l_NewJobNumber&
'
'            l_rsJobs("job number") = l_NewJobNumber&
            l_rsJobs("companyID") = 570
            l_rsJobs("companyname") = "BBC Worldwide Int Ops"
            
            'parse the contactname to get the CETA contact ID and name.
            If l_ContactName$ <> "" Then
                l_rsJobs("contactname") = l_ContactName$
                l_rsJobs("contactID") = GetContactID(l_ContactName$)
            End If
            
            If l_ProgrammeTitle$ <> "" Then l_rsJobs("title1") = Left$(l_ProgrammeTitle$, 50)
            l_Lastprogrammetitle$ = UCase(l_ProgrammeTitle$)
            If l_MasterEpisodeNumber$ <> "" Then l_rsJobs("title2") = Left$(l_MasterEpisodeNumber$, 50)
            If l_Notes$ <> "" Then l_rsJobs("notes2") = "PLEASE CHECK ORIGINAL WW ORDER SHEETS FOR JOB DETAILS. " & l_Notes$
            
            'check whether this is a STAR order or a normal one.
            
            If l_OrderNumber$ Like "STAR*" Then
                l_rsJobs("orderreference") = "STAR ORDER"
                l_LastorderNumber$ = "STAR ORDER"
                l_rsJobs("title1") = "Various Titles - Star Order"
            Else
                If l_OrderNumber$ <> "" Then l_rsJobs("orderreference") = UCase(l_OrderNumber$)
                l_LastorderNumber$ = UCase(l_OrderNumber$)
            End If
                
            l_rsJobs.Update
            
            
            'this part here!
            l_rsJobs.Bookmark = l_rsJobs.Bookmark
            l_NewJobNumber& = l_rsJobs("JobID")
            l_PreviousJobNumber& = l_NewJobNumber&
            
            'Set the projectnumber to be the job ID - new CETA feature.
            l_rsJobs("projectnumber") = l_rsJobs("JobID")
            l_rsJobs.Update
                        
            DBEngine.Idle
            DoEvents
            
            If l_NewJobNumber& <> 0 Then
                
                l_rsxJobs.AddNew
                l_rsxJobs("JobID") = l_NewJobNumber&
                l_rsxJobs("BBCStarJob") = 1
                l_rsxJobs("BBCBookingnumber") = l_FACNumber$
                If l_CustomerName$ <> "" Then l_rsxJobs("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxJobs("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxJobs("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxJobs("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxJobs("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxJobs("BBCCountryCode") = l_CountryCode$
                l_rsxJobs.Update
                
            End If

            DBEngine.Idle
            DoEvents
            
        End If
        
    Else
        'make sure the title program stays the same all the way
        If UCase(l_ProgrammeTitle$) <> UCase(l_Lastprogrammetitle$) Then
            'need to update the other job to change the title
            l_rsJobs.Close
            Set l_rsJobs = ExecuteSQL("Select [title1] from [Job] where [jobID] = " & l_PreviousJobNumber&, g_strExecuteError)
            CheckForSQLError
                        
            If l_rsJobs.RecordCount <> 0 Then
                l_rsJobs("title1") = "Various Titles - See Below"
                l_rsJobs.Update
                l_rsJobs.Close
                DBEngine.Idle
                DoEvents
                Set l_rsJobs = ExecuteSQL("SELECT * FROM [Job] WHERE [JobID] = -1", g_strExecuteError)
                CheckForSQLError
            End If
        End If
    End If

    DBEngine.Idle: DoEvents
    
    'The master detail line(s) get added next
    While l_Mastercount& > 0
        
        'Get next master into normal variables if there is more than 1, trim the original variables and decrease the mastercount.
        
        If l_Mastercount > 1 Then
            l_comma& = InStr(l_Orig_masterspool$, ",")
            l_MasterSpoolNumber$ = Left(l_Orig_masterspool$, l_comma& - 1)
            l_Orig_masterspool$ = Mid(l_Orig_masterspool$, l_comma& + 1, Len(l_Orig_masterspool$))
            l_comma& = InStr(l_Orig_masterformat$, ",")
            l_BBCMasterTapeFormat$ = Left(l_Orig_masterformat$, l_comma& - 1)
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_Orig_masterformat$ = Mid(l_Orig_masterformat$, l_comma + 1, Len(l_Orig_masterformat$))
            l_comma& = InStr(l_Orig_masterstandard$, ",")
            l_MasterLineStandard$ = GetAlias(Left(l_Orig_masterstandard$, l_comma - 1))
            l_Orig_masterstandard$ = Mid(l_Orig_masterstandard$, l_comma + 1, Len(l_Orig_masterstandard$))
            l_comma& = InStr(l_Orig_masteraspect$, ",")
            If l_comma > 0 Then
                l_MasterAspectRatio$ = GetAlias(Left(l_Orig_masteraspect$, l_comma - 1))
                l_Orig_masteraspect$ = Mid(l_Orig_masteraspect$, l_comma + 1, Len(l_Orig_masteraspect$))
            Else
                l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            End If
            l_comma& = InStr(l_Orig_masterduration$, ",")
            l_MasterSpoolDuration$ = Left(l_Orig_masterduration$, l_comma - 1)
            l_Orig_masterduration$ = Mid(l_Orig_masterduration$, l_comma + 1, Len(l_Orig_masterduration$))
        End If
        l_Mastercount& = l_Mastercount& - 1
        
        'process that master
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_MasterSpoolNumber$ <> "" Then
            If l_EpisodeNumber$ <> "" Then
                l_rsDetails("description") = Left(l_ProgrammeTitle$, 50) & " Ep." & l_EpisodeNumber$
            Else
                l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
            End If
        End If
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & Trim(l_MasterSpoolNumber$) & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rsLib.EOF Then
            If l_rsLib("format") <> "" Then
                l_rsDetails("format") = Left(l_rsLib("format"), 50)
                If l_rsDetails("format") Like "HI8DAT*" Then l_rsDetails("format") = "HI8DAT"
                If l_rsDetails("format") Like "DAT*" Then l_rsDetails("format") = "DAT"
            ElseIf l_MasterTapeFormat$ <> "" Then
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("videostandard") = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("videostandard") <> "" Then l_MasterLineStandard$ = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("AspectRatio") <> "" Then
                l_rsDetails("aspectratio") = Left((l_rsLib("AspectRatio") & l_rsLib("GeometricLinearity")), 50)
                l_MasterAspectRatio$ = l_rsDetails("aspectratio")
            Else
                If l_MasterAspectRatio$ <> "" Then
                    l_rsDetails("aspectratio") = l_MasterAspectRatio$
                Else
                    If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                        l_rsDetails("aspectratio") = "16:9AN"
                        l_MasterAspectRatio$ = "16:9AN"
                    End If
                    If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                        l_rsDetails("aspectratio") = "16:9LE"
                        l_MasterAspectRatio$ = "16:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*15:9*" Then
                        l_rsDetails("aspectratio") = "15:9LE"
                        l_MasterAspectRatio$ = "15:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*14:9*" Then
                        l_rsDetails("aspectratio") = "14:9LE"
                        l_MasterAspectRatio$ = "14:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*4:3*" Then
                        l_rsDetails("aspectratio") = "4:3NOR"
                        l_MasterAspectRatio$ = "4:3NOR"
                    End If
                End If
            End If
            If l_rsLib("Totalduration") <> "" Then
                l_CalculatedDuration& = 60 * Val(Left(l_rsLib("Totalduration"), 2)) + Val(Mid(l_rsLib("totalduration"), 4, 2))
                If Val(Mid(l_rsLib("totalduration"), 7, 2)) > 0 Then l_CalculatedDuration& = l_CalculatedDuration& + 1
            End If
            If l_CalculatedDuration& > 0 Then
                l_rsDetails("runningtime") = l_CalculatedDuration&
                l_MasterSpoolDuration$ = Str(l_CalculatedDuration&)
            Else
                l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            End If
            If l_rsLib("ch1") <> "" Then
                l_rsDetails("sound1") = Process_Sound(l_rsLib("ch1"))
            Else
                l_rsDetails("sound1") = Left(l_rsLib("ch1"), 50)
            End If
            If l_rsLib("ch2") <> "" Then
                l_rsDetails("sound2") = Process_Sound(l_rsLib("ch2"))
            Else
                l_rsDetails("sound2") = Left(l_rsLib("ch2"), 50)
            End If
            If l_rsLib("ch3") <> "" Then
                l_rsDetails("sound3") = Process_Sound(l_rsLib("ch3"))
            Else
                l_rsDetails("sound3") = Left(l_rsLib("ch3"), 50)
            End If
            If l_rsLib("ch4") <> "" Then
                l_rsDetails("sound4") = Process_Sound(l_rsLib("ch4"))
            Else
                l_rsDetails("sound4") = Left(l_rsLib("ch4"), 50)
            End If
            
            'Make the tape required entry
            l_lngLibraryID = l_rsLib("libraryID")
            l_rsHistory.Open "SELECT * FROM requiredmedia WHERE jobID = " & l_NewJobNumber& & " AND libraryID = " & l_lngLibraryID & ";", g_strConnection, adOpenKeyset, adLockOptimistic
            If l_rsHistory.RecordCount <= 0 Then
                l_strSQL = "INSERT INTO requiredmedia (jobID, libraryID, barcode) VALUES (" & l_NewJobNumber& & ", " & l_lngLibraryID & ", '" & l_MasterSpoolNumber$ & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            l_rsHistory.Close
        Else
            'As well as loading up the job detail line from the provided data, we create a new tape entry in the library.
            l_rsLib.AddNew
            l_rsLib("barcode") = Trim(l_MasterSpoolNumber$)
            l_rsLib("title") = l_ProgrammeTitle$
            l_rsLib("subtitle") = l_DupeEpisodeNumber$
            l_rsLib("companyID") = 287
            l_rsLib("companyname") = "BBC Worldwide Ltd"
            l_rsLib("location") = "OFF SITE"
            If l_MasterTapeFormat$ <> "" Then
                l_rsLib("format") = l_MasterTapeFormat$
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsLib("copytype") = "MASTER"
            l_rsLib("version") = "MASTER"
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            If l_MasterLineStandard$ = "ASK" Then
                l_MasterLineStandard$ = "625PAL"
                l_rsDetails("videoStandard") = "625PAL"
                l_rsLib("videostandard") = "625PAL"
            Else
                If l_MasterLineStandard$ <> "" Then
                    l_rsDetails("videostandard") = Left(l_MasterLineStandard$, 10)
                    l_rsLib("videostandard") = Left(l_MasterLineStandard$, 10)
                End If
            End If
            If l_MasterAspectRatio$ <> "" Then
                l_rsDetails("aspectratio") = l_MasterAspectRatio$
            Else
                If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_MasterAspectRatio$ = "16:9AN"
                    l_rsLib("aspectratio") = "16:9"
                    l_rsLib("geometriclinearity") = "ANAMORPHIC"
                End If
                If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_MasterAspectRatio$ = "16:9LE"
                    l_rsLib("aspectratio") = "16:9"
                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_MasterAspectRatio$ = "15:9LE"
                    l_rsLib("aspectratio") = "15:9"
                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_MasterAspectRatio$ = "14:9LE"
                    l_rsLib("aspectratio") = "14:9"
                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_MasterAspectRatio$ = "4:3NOR"
                    l_rsLib("aspectratio") = "4:3"
                    l_rsLib("geometriclinearity") = "NORMAL"
                End If
            End If
            If l_MasterAspectRatio$ = "ASK" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_MasterAspectRatio$ = "4:3NOR"
                l_rsLib("aspectratio") = "4:3"
                l_rsLib("geometriclinearity") = "NORMAL"
            End If
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_rsLib("cuser") = "XLS"
            l_rsLib("cdate") = Now
            l_rsLib("muser") = "XLS"
            l_rsLib("mdate") = Now
            l_rsLib("code128barcode") = CIA_CODE128(Trim(l_MasterSpoolNumber$))
            l_rsLib.Update
            
            'Then we make a history entry too.
            l_rsLib.Bookmark = l_rsLib.Bookmark
            l_lngLibraryID = l_rsLib("libraryID")
            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", g_strConnection, adOpenKeyset, adLockOptimistic
            l_rsHistory.AddNew
            l_rsHistory("libraryID") = l_lngLibraryID
            l_rsHistory("destination") = "Created"
            l_rsHistory("cuser") = "XLS"
            l_rsHistory("cdate") = Now
            l_rsHistory.Update
            l_rsHistory.Close
            
            'Make the tape required entry
            l_rsHistory.Open "SELECT * FROM requiredmedia WHERE jobID = " & l_NewJobNumber& & " AND libraryID = " & l_lngLibraryID & ";", g_strConnection, adOpenKeyset, adLockOptimistic
            If l_rsHistory.RecordCount <= 0 Then
                l_strSQL = "INSERT INTO requiredmedia (jobID, libraryID, barcode) VALUES (" & l_NewJobNumber& & ", " & l_lngLibraryID & ", '" & l_MasterSpoolNumber$ & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
            l_rsHistory.Close
        End If
        l_rsLib.Close
'        l_BBCMasterTapeFormat$ = l_MasterTapeFormat$
        If l_rsDetails("format") = "" Then l_rsDetails("format") = "ASK"
        
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update

        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcsourceformat") = l_BBCMasterTapeFormat$
        If l_DupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_DupeTapeFormat$
        l_rsxDetails.Update
                
        'update the variables if the mastercount is currentrly 1
        If l_Mastercount& = 1 Then
            l_BBCMasterTapeFormat$ = l_Orig_masterformat$
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_MasterSpoolNumber$ = l_Orig_masterspool$
            l_MasterLineStandard$ = GetAlias(l_Orig_masterstandard$)
            l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            l_MasterSpoolDuration$ = l_Orig_masterduration$
        End If
    Wend
    
    'Then the copy detail line gets added
    l_OrigCopyCount = l_CopyCount
    While l_CopyCount > 0
        
        l_CurrentTop& = l_CurrentTop& - 11
        l_CopyCount = l_CopyCount - 1
        
        'Get the Duplication details for this copy
        
        l_DupeEpisodeNumber$ = GetXLData(l_CurrentTop&, 6)
        l_DupeTotalDuration$ = GetXLData(l_CurrentTop& + 1, 6)
        l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
        l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)
        l_DupeTapeFormat$ = GetAlias(l_BBCDupeTapeFormat$)
        l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
        l_Original_DupeStandard$ = GetXLData(l_CurrentTop& + 4, 6)
        l_DupeAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 5, 6))
        l_DupeLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 6, 6))
        l_DupeCeefax$ = GetAlias(GetXLData(l_CurrentTop& + 7, 6))
        l_DupeAudio$ = GetXLData(l_CurrentTop& + 8, 6)
        
        'we need to get the different sections of the sounds out of the sound notes
        If l_DupeAudio$ <> "" Then
        
            'append another comma seperated value onto the end of the audio
            'channels to ensure we get the last value
            l_DupeAudio$ = l_DupeAudio$ & ",END"
                
            'get each Sound channel using the GetTextPart function from
            'modRoutines
            l_Sound1$ = GetAlias(GetTextPart(l_DupeAudio$, 1, ","))
            l_Sound2$ = GetAlias(GetTextPart(l_DupeAudio$, 2, ","))
            l_Sound3$ = GetAlias(GetTextPart(l_DupeAudio$, 3, ","))
            l_Sound4$ = GetAlias(GetTextPart(l_DupeAudio$, 4, ","))
            l_Sound5$ = GetAlias(GetTextPart(l_DupeAudio$, 5, ","))
            l_Sound6$ = GetAlias(GetTextPart(l_DupeAudio$, 6, ","))
            l_Sound7$ = GetAlias(GetTextPart(l_DupeAudio$, 7, ","))
            l_Sound8$ = GetAlias(GetTextPart(l_DupeAudio$, 8, ","))
        
        End If
        
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_EpisodeNumber$ <> "" Then
            l_rsDetails("description") = Left(l_ProgrammeTitle$, 50) & " Ep." & l_EpisodeNumber$
        Else
            l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        End If
        If l_DupeTapeFormat$ <> "" Then l_rsDetails("format") = GetInclusiveCode(l_DupeTapeFormat$, l_MasterTapeFormat$, l_MasterLineStandard$, l_DupeLineStandard$)
        If l_rsDetails("format") = "WWSDENC" Then
            l_rsDetails("chargerealminutes") = -1
            l_rsDetails("copytype") = "C"
        Else
            l_rsDetails("copytype") = "I"
        End If
        If l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD" Then
            l_rsDetails("quantity") = Val(l_DupeQuantity$) * l_EpisodeCount
        Else
            l_rsDetails("quantity") = l_EpisodeCount
        End If
        If l_DupeTapeFormat$ = "DBETA" Or l_DupeTapeFormat$ = "BETASP" Then
            If Val(l_DupeQuantity$) > 1 Then
                l_intExtraCopiesFromSameReplay = Val(l_DupeQuantity$) - 1
            End If
        End If
        If l_rsDetails("format") = "WWSDVHS" Then
            If Val(l_DupeQuantity$) <= 10 Then
                l_rsDetails("format") = l_rsDetails("format") & "10"
            ElseIf Val(l_DupeQuantity$) <= 20 Then
                l_rsDetails("format") = l_rsDetails("format") & "20"
            Else
                l_rsDetails("format") = l_rsDetails("format") & "20+"
            End If
        End If
        If l_DupeLineStandard$ <> "" Then l_rsDetails("videostandard") = Left(l_DupeLineStandard$, 10)
        If Int(Val(l_MasterSpoolDuration$)) > Int(Val(l_DupeTotalDuration$)) Then
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_DupeTotalDuration$ = l_MasterSpoolDuration$
        Else
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
        End If
        If l_DupeAspectRatio$ <> "" And l_DupeAspectRatio$ <> "ASK" Then
            l_rsDetails("aspectratio") = l_DupeAspectRatio$
        Else
            If l_Original_DupeStandard$ Like "*16:9 WI*" Then
                l_rsDetails("aspectratio") = "16:9AN"
                l_DupeAspectRatio$ = "16:9AN"
            End If
            If l_Original_DupeStandard$ Like "*16:9 LE*" Then
                l_rsDetails("aspectratio") = "16:9LE"
                l_DupeAspectRatio$ = "16:9LE"
            End If
            If l_Original_DupeStandard$ Like "*15:9*" Then
                l_rsDetails("aspectratio") = "15:9LE"
                l_DupeAspectRatio$ = "15:9LE"
            End If
            If l_Original_DupeStandard$ Like "*14:9*" Then
                l_rsDetails("aspectratio") = "14:9LE"
                l_DupeAspectRatio$ = "14:9LE"
            End If
            If l_Original_DupeStandard$ Like "*4:3*" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_DupeAspectRatio$ = "4:3NOR"
            End If
        End If
        If l_rsDetails("aspectratio") = "" Then
            l_rsDetails("aspectratio") = "4:3NOR"
            l_DupeAspectRatio$ = "4:3NOR"
        End If
        If l_Sound1$ <> "" Then l_rsDetails("sound1") = Left(l_Sound1$, 6)
        If l_Sound2$ <> "" Then l_rsDetails("sound2") = Left(l_Sound2$, 6)
        If l_Sound3$ <> "" Then l_rsDetails("sound3") = Left(l_Sound3$, 6)
        If l_Sound4$ <> "" Then l_rsDetails("sound4") = Left(l_Sound4$, 6)
        
    
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update
        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCorderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcSourceFormat") = l_BBCMasterTapeFormat$
        If l_BBCDupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_BBCDupeTapeFormat$
        l_rsxDetails.Update
            
            
        'Are extra broadcast copies needed
        If l_intExtraCopiesFromSameReplay <> 0 Then
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Extra " & l_DupeTapeFormat$ & " copies"
            If l_DupeTapeFormat$ = "DBETA" Then
                l_rsDetails("format") = "WWADDB"
            Else
                l_rsDetails("format") = "WWADSP"
            End If
            l_rsDetails("copytype") = "I"
            l_rsDetails("quantity") = l_intExtraCopiesFromSameReplay * l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
            l_intExtraCopiesFromSameReplay = 0
        End If
        
        
        If UCase(l_DupeCeefax$) = "TRUE" Then
            'create a new ceefax line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "CEEFAX Subtitler"
            l_rsDetails("format") = "SUBT"
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
    
        End If
        
        If l_DupeLineStandard$ <> l_MasterLineStandard$ Then
            ' create a standards converter line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Standards Converter"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "CONV"
            Else
                l_rsDetails("format") = "B-CONV"
            End If
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
            
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
        
        End If
        
        If Left(l_MasterAspectRatio$, 6) <> Left(l_DupeAspectRatio$, 6) Then
            If Not (l_MasterAspectRatio$ Like "*4:3*" And l_DupeAspectRatio$ Like "*4:3*") Then
                ' create a ARC line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("description") = "Aspect Ratio Converter"
                l_rsDetails("format") = "ARATIO"
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
            
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
            End If
        End If
    
        If (l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD") And Val(l_DupeQuantity$) > 1 Then
            
            'Do another line to add the extra DVD and VHS copies
            
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Additional Copies"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "WWADVHS"
            Else
                l_rsDetails("format") = "WWADDVD"
            End If
            l_rsDetails("copytype") = "I"
            l_rsDetails("quantity") = (Val(l_DupeQuantity$) - 1) * l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
            
        End If
        DBEngine.Idle
        DoEvents
    Wend
    'Copy Loop has finished - need to reset the currenttop to beyond the end of the last group again
    l_CurrentTop& = l_CurrentTop& + (11 * l_OrigCopyCount)
Loop

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
Set l_rsLib = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

'Rename file into the done folder - commented out during testing.
Name l_strExcelFileName As "\\jcaserver\ceta\Processed Star Orders\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdiTunesOMD_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strOrderDate As String, l_strOrderNumber As String, l_strRightsOwner As String, l_strOrderType As String, l_strComponentType As String, l_strFeatureHD As String, l_strFeatureAudio As String
Dim l_strTitle As String, l_strSubTitle As String, l_strEpisode As String, l_strFeatureLanguage1 As String, l_strFeatureLanguage2 As String, l_strFeatureLanguage3 As String, l_strFeatureLanguage4 As String
Dim l_strTrailerHD As String, l_strTrailerAudio As String, l_strTrailerSubs As String, l_strTrailerBIS As String, l_strTrailerCreate As String, l_strDestination As String
Dim l_strTrailerLanguage1 As String, l_strTrailerLanguage2 As String, l_strTrailerLanguage3 As String, l_strTrailerLanguage4 As String
Dim l_strArtworkLanguage1 As String, l_strArtworkLanguage2 As String, l_strArtworkLanguage3 As String, l_strArtworkLanguage4 As String
Dim l_strSubsLanguage1 As String, l_strSubsLanguage2 As String, l_strSubsLanguage3 As String, l_strSubsLanguage4 As String
Dim l_strChaptering As String, l_lngCompanyID As Long, l_strCompanyName As String, l_striTunesVendorID As String, l_strMetadataNeeded As String
Dim l_strLanguage As String, l_strProjectManager As String, l_strSeries As String, l_strVolume As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset, l_strProxyRequired As String, l_strFixNotes As String
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngTrackerID As Long

Dim l_strSQL As String, TempStr As String, l_blnError As Boolean, l_strUrgent As String, l_strAlphaDisplayCode As String, l_strAuditDescription As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "DADC Anif PO")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 2) <> ""
    
    l_blnError = False
    l_strOrderDate = Trim(GetXLData(l_lngXLRowCounter, 1)) 'A
    l_strOrderNumber = Trim(GetXLData(l_lngXLRowCounter, 2)) 'B
    l_strRightsOwner = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strDestination = Trim(GetXLData(l_lngXLRowCounter, 4)) 'D
    l_strOrderType = Trim(GetXLData(l_lngXLRowCounter, 5)) 'E
    l_strDueDate = Trim(GetXLData(l_lngXLRowCounter, 9)) 'I
    If IsDate(l_strDueDate) Then l_datDueDate = l_strDueDate
    l_striTunesVendorID = Trim(GetXLData(l_lngXLRowCounter, 13)) 'M
    l_strProjectManager = Trim(GetXLData(l_lngXLRowCounter, 12)) 'L
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 14)) 'N
    l_strSeries = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_strVolume = Trim(GetXLData(l_lngXLRowCounter, 16)) 'P
    l_strSubTitle = Trim(GetXLData(l_lngXLRowCounter, 17)) 'Q
    l_strEpisode = Trim(GetXLData(l_lngXLRowCounter, 18)) 'R
    l_strComponentType = GetAlias(Trim(GetXLData(l_lngXLRowCounter, 19))) 'S
    l_strFeatureHD = Trim(GetXLData(l_lngXLRowCounter, 20)) 'T
    l_strFeatureAudio = Trim(GetXLData(l_lngXLRowCounter, 21)) 'U
    l_strFeatureLanguage1 = Trim(GetXLData(l_lngXLRowCounter, 22)) 'V
    l_strFeatureLanguage2 = Trim(GetXLData(l_lngXLRowCounter, 23)) 'W
    l_strFeatureLanguage3 = Trim(GetXLData(l_lngXLRowCounter, 24)) 'X
    l_strFeatureLanguage4 = Trim(GetXLData(l_lngXLRowCounter, 25)) 'Y
    l_strTrailerHD = Trim(GetXLData(l_lngXLRowCounter, 26)) 'Z
    l_strTrailerBIS = Trim(GetXLData(l_lngXLRowCounter, 27)) 'AA
    l_strTrailerCreate = Trim(GetXLData(l_lngXLRowCounter, 28)) 'AB
    l_strTrailerAudio = Trim(GetXLData(l_lngXLRowCounter, 29)) 'AC
    l_strTrailerLanguage1 = Trim(GetXLData(l_lngXLRowCounter, 30)) 'AD
    l_strTrailerLanguage2 = Trim(GetXLData(l_lngXLRowCounter, 31)) 'AE
    l_strTrailerLanguage3 = Trim(GetXLData(l_lngXLRowCounter, 32)) 'AF
    l_strTrailerLanguage4 = Trim(GetXLData(l_lngXLRowCounter, 33)) 'AG
    l_strMetadataNeeded = Trim(GetXLData(l_lngXLRowCounter, 34)) 'AH
    l_strArtworkLanguage1 = Trim(GetXLData(l_lngXLRowCounter, 35)) 'AI
    l_strArtworkLanguage2 = Trim(GetXLData(l_lngXLRowCounter, 36)) 'AJ
    l_strArtworkLanguage3 = Trim(GetXLData(l_lngXLRowCounter, 37)) 'AK
    l_strArtworkLanguage4 = Trim(GetXLData(l_lngXLRowCounter, 38)) 'AL
    l_strSubsLanguage1 = Trim(GetXLData(l_lngXLRowCounter, 39)) 'AM
    l_strSubsLanguage2 = Trim(GetXLData(l_lngXLRowCounter, 40)) 'AN
    l_strSubsLanguage3 = Trim(GetXLData(l_lngXLRowCounter, 41)) 'AO
    l_strSubsLanguage4 = Trim(GetXLData(l_lngXLRowCounter, 42)) 'AP
    l_strChaptering = Trim(GetXLData(l_lngXLRowCounter, 43)) 'AQ
    l_strAlphaDisplayCode = Trim(GetXLData(l_lngXLRowCounter, 54)) 'BB
    l_strUrgent = Trim(GetXLData(l_lngXLRowCounter, 55)) 'BC
    lblProgress.Caption = "Row:" & l_lngXLRowCounter & ", " & l_strOrderNumber & " - " & l_strTitle
    
    l_strCompanyName = GetAlias(l_strRightsOwner)
    l_lngCompanyID = GetData("company", "companyID", "name", l_strCompanyName)
        
    l_strSQL = "SELECT tracker_itunes_itemID FROM tracker_itunes_item WHERE OrderNumber = '" & QuoteSanitise(l_strOrderNumber) & "' AND companyID = " & GetData("company", "companyID", "name", l_strCompanyName) & " AND readytobill = 0;"
    Debug.Print l_strSQL
    TempStr = GetDataSQL(l_strSQL)
    If Val(TempStr) <> 0 Then
        l_strAuditDescription = ""
        l_strSQL = "UPDATE tracker_itunes_item SET "
        l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "iTunesOrderType = '" & l_strDestination & "', "
        l_strSQL = l_strSQL & "iTunesSubmissionType = '" & l_strOrderType & "', "
        l_strSQL = l_strSQL & "iTunesVendorID = '" & l_striTunesVendorID & "', "
        l_strSQL = l_strSQL & "AlphaDisplayCode = '" & l_strAlphaDisplayCode & "', "
        If GetData("tracker_itunes_item", "requiredby", "tracker_itunes_itemID", Val(TempStr)) <> l_datDueDate Then
            If l_datDueDate > Now Then
                l_strSQL = l_strSQL & "requiredby = '" & FormatSQLDate(l_datDueDate) & "', "
                l_strAuditDescription = l_strAuditDescription & "requiredby = '" & l_datDueDate & "', "
            Else
                MsgBox "Line " & l_lngXLRowCounter & " attempt to update due date to a time before Now - Line not read in.", vbCritical, "Error Reading Sheet"
                l_blnError = True
            End If
        End If
        If l_strUrgent <> "" Then
            l_strSQL = l_strSQL & "priority = -1, "
        Else
            l_strSQL = l_strSQL & "priority = 0, "
        End If
        If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = " & GetData("company", "companyID", "name", l_strCompanyName) & ");"))) <> 0 Then
            l_strSQL = l_strSQL & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
            l_strSQL = l_strSQL & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
        Else
            MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
            l_blnError = True
        End If
        l_strSQL = l_strSQL & "title = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "series = '" & l_strSeries & "', "
        l_strSQL = l_strSQL & "volume = '" & l_strVolume & "', "
        l_strSQL = l_strSQL & "episodetitle = '" & QuoteSanitise(l_strSubTitle) & "', "
        l_strSQL = l_strSQL & "episode = '" & l_strEpisode & "', "
        If UCase(Left(l_strFeatureHD, 1)) = "Y" Or UCase(l_strFeatureHD) = "HD" Then
            l_strSQL = l_strSQL & "featureHD = 1, "
        Else
            l_strSQL = l_strSQL & "featureHD = 0, "
        End If
        l_strSQL = l_strSQL & "FeatureAudioSpec = '" & l_strFeatureAudio & "', "
        l_strSQL = l_strSQL & "Featurelanguage1 = '" & l_strFeatureLanguage1 & "', "
        l_strSQL = l_strSQL & "Featurelanguage2 = '" & l_strFeatureLanguage2 & "', "
        l_strSQL = l_strSQL & "Featurelanguage3 = '" & l_strFeatureLanguage3 & "', "
        l_strSQL = l_strSQL & "Featurelanguage4 = '" & l_strFeatureLanguage4 & "', "
        If UCase(Left(l_strTrailerHD, 1)) = "Y" Or UCase(l_strTrailerHD) = "HD" Then
            l_strSQL = l_strSQL & "trailerHD = 1, "
        Else
            l_strSQL = l_strSQL & "trailerHD = 0, "
        End If
        l_strSQL = l_strSQL & "trailerbis = '" & l_strTrailerBIS & "', "
        l_strSQL = l_strSQL & "trailercreate = '" & l_strTrailerCreate & "', "
        l_strSQL = l_strSQL & "TrailerAudioSpec = '" & l_strTrailerAudio & "', "
        l_strSQL = l_strSQL & "TrailerLanguage1 = '" & l_strTrailerLanguage1 & "', "
        l_strSQL = l_strSQL & "TrailerLanguage2 = '" & l_strTrailerLanguage2 & "', "
        l_strSQL = l_strSQL & "TrailerLanguage3 = '" & l_strTrailerLanguage3 & "', "
        l_strSQL = l_strSQL & "TrailerLanguage4 = '" & l_strTrailerLanguage4 & "', "
        l_strSQL = l_strSQL & "ArtworkLanguage1 = '" & l_strArtworkLanguage1 & "', "
        l_strSQL = l_strSQL & "ArtworkLanguage2 = '" & l_strArtworkLanguage2 & "', "
        l_strSQL = l_strSQL & "ArtworkLanguage3 = '" & l_strArtworkLanguage3 & "', "
        l_strSQL = l_strSQL & "ArtworkLanguage4 = '" & l_strArtworkLanguage4 & "', "
        l_strSQL = l_strSQL & "SubsLanguage1 = '" & l_strSubsLanguage1 & "', "
        l_strSQL = l_strSQL & "SubsLanguage2 = '" & l_strSubsLanguage2 & "', "
        l_strSQL = l_strSQL & "SubsLanguage3 = '" & l_strSubsLanguage3 & "', "
        l_strSQL = l_strSQL & "SubsLanguage4 = '" & l_strSubsLanguage4 & "', "
        If UCase(Left(l_strMetadataNeeded, 1)) = "Y" Then
            l_strSQL = l_strSQL & "MetadataNeeded = 1, "
        Else
            l_strSQL = l_strSQL & "MetadataNeeded = 0, "
        End If
        l_strSQL = l_strSQL & "chaptering = '" & l_strChaptering & "'"
        l_strSQL = l_strSQL & "WHERE tracker_itunes_itemID = " & Val(TempStr) & ";"
        
        If l_blnError = False Then
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            l_strEmailBody = l_strEmailBody & "Updated: Order No: " & l_strOrderNumber & ", Vendor ID: " & l_striTunesVendorID & ", Title: " & l_strTitle & ", Lang: " & l_strFeatureLanguage1 & ", Due Date: " & l_datDueDate & ", For: " & l_strCompanyName & ", " & l_strProjectManager & vbCrLf
            If l_strAuditDescription <> "" Then
                l_strEmailBody = l_strEmailBody & l_strAuditDescription & vbCrLf
                l_strSQL = "INSERT INTO tracker_iTunes_audit (tracker_itunes_itemID, mdate, muser, description) VALUES (" & TempStr & ", getdate(), '" & g_strFullUserName & "', 'Update from OMD: " & QuoteSanitise(l_strAuditDescription) & "');"
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
        End If
    Else
        'Check whether an existing line exists that is already finished...
        l_strSQL = "SELECT tracker_itunes_itemID FROM tracker_itunes_item WHERE OrderNumber = '" & QuoteSanitise(l_strOrderNumber) & "' AND companyID = " & GetData("company", "companyID", "name", l_strCompanyName) & " AND readytobill <> 0;"
        Debug.Print l_strSQL
        TempStr = GetDataSQL(l_strSQL)
        
        If Val(TempStr) <> 0 Then
            MsgBox "Row " & l_lngXLRowCounter & " was an update for an item that we have already completed. Row not read in.", vbInformation, "Error..."
        Else
        
            'SQL to Insert
            l_strSQL = "INSERT INTO tracker_iTunes_item ("
            l_strSQL = l_strSQL & "cdate, mdate, companyID, OrderNumber, iTunesVendorID, iTunesOrderType, iTunesSubmissionType, projectmanager, projectmanagercontactID, requiredby, priority, title, series, volume, "
            l_strSQL = l_strSQL & "episodetitle, episode, featurehd, FeatureAudioSpec, Featurelanguage1, Featurelanguage2, Featurelanguage3, Featurelanguage4, trailerHD, trailerBIS, trailercreate, "
            l_strSQL = l_strSQL & "TrailerAudioSpec, TrailerLanguage1, TrailerLanguage2, TrailerLanguage3, TrailerLanguage4, chaptering, AlphaDisplayCode, "
            l_strSQL = l_strSQL & "ArtworkLanguage1, ArtworkLanguage2, ArtworkLanguage3, ArtworkLanguage4, SubsLanguage1, SubsLanguage2, SubsLanguage3, SubsLanguage4, Metadataneeded"
            l_strSQL = l_strSQL & ") VALUES ("
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & l_lngCompanyID & ", "
            l_strSQL = l_strSQL & "'" & l_strOrderNumber & "', "
            l_strSQL = l_strSQL & "'" & l_striTunesVendorID & "', "
            l_strSQL = l_strSQL & "'" & l_strDestination & "', "
            l_strSQL = l_strSQL & "'" & l_strOrderType & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strProjectManager) & "', "
            If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = " & GetData("company", "companyID", "name", l_strCompanyName) & ");"))) <> 0 Then
                l_strSQL = l_strSQL & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
            Else
                MsgBox "Line " & l_lngXLRowCounter & " has no single usable Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
                l_blnError = True
            End If
            If l_datDueDate > Now Then
                l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
            Else
                MsgBox "Line " & l_lngXLRowCounter & " has a Due Date earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
                l_blnError = True
            End If
            If l_strUrgent <> "" Then
                l_strSQL = l_strSQL & "-1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & "'" & l_strSeries & "', "
            l_strSQL = l_strSQL & "'" & l_strVolume & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
            l_strSQL = l_strSQL & "'" & l_strEpisode & "', "
            If UCase(Left(l_strFeatureHD, 1)) = "Y" Or UCase(l_strFeatureHD) = "HD" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strFeatureAudio & "', "
            l_strSQL = l_strSQL & "'" & l_strFeatureLanguage1 & "', "
            l_strSQL = l_strSQL & "'" & l_strFeatureLanguage2 & "', "
            l_strSQL = l_strSQL & "'" & l_strFeatureLanguage3 & "', "
            l_strSQL = l_strSQL & "'" & l_strFeatureLanguage4 & "', "
            If UCase(Left(l_strTrailerHD, 1)) = "Y" Or UCase(l_strTrailerHD) = "HD" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strTrailerBIS & "', "
            l_strSQL = l_strSQL & "'" & l_strTrailerCreate & "', "
            l_strSQL = l_strSQL & "'" & l_strTrailerAudio & "', "
            l_strSQL = l_strSQL & "'" & l_strTrailerLanguage1 & "', "
            l_strSQL = l_strSQL & "'" & l_strTrailerLanguage2 & "', "
            l_strSQL = l_strSQL & "'" & l_strTrailerLanguage3 & "', "
            l_strSQL = l_strSQL & "'" & l_strTrailerLanguage4 & "', "
            l_strSQL = l_strSQL & "'" & l_strChaptering & "', "
            l_strSQL = l_strSQL & "'" & l_strAlphaDisplayCode & "', "
            l_strSQL = l_strSQL & "'" & l_strArtworkLanguage1 & "', "
            l_strSQL = l_strSQL & "'" & l_strArtworkLanguage2 & "', "
            l_strSQL = l_strSQL & "'" & l_strArtworkLanguage3 & "', "
            l_strSQL = l_strSQL & "'" & l_strArtworkLanguage4 & "', "
            l_strSQL = l_strSQL & "'" & l_strSubsLanguage1 & "', "
            l_strSQL = l_strSQL & "'" & l_strSubsLanguage2 & "', "
            l_strSQL = l_strSQL & "'" & l_strSubsLanguage3 & "', "
            l_strSQL = l_strSQL & "'" & l_strSubsLanguage4 & "', "
            If UCase(Left(l_strMetadataNeeded, 1)) = "Y" Then
                l_strSQL = l_strSQL & "1"
            Else
                l_strSQL = l_strSQL & "0"
            End If
            l_strSQL = l_strSQL & "); "
                
            If l_blnError = False Then
                Debug.Print l_strSQL
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_strEmailBody = l_strEmailBody & "Added: Order No: " & l_strOrderNumber & ", Vendor ID: " & l_striTunesVendorID & ", Title: " & l_strTitle & ", Lang: " & l_strFeatureLanguage1 & ", Due Date: " & l_datDueDate & ", For: " & l_strCompanyName & ", " & l_strProjectManager & vbCrLf
            End If
        End If
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new items were added / updated with the DADC iTunes Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT DISTINCT email, fullname FROM trackernotification WHERE trackermessageID = 36;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC iTunes Tracker Items Added or Updated", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdKidsCo_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If

'emtpy the comma count field
m_lngCommacount = 0

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_datRequiredDate As Date, l_datSoundHouseDate As Date, l_datTXDate As Date
Dim l_strExcelSheetName As String, i As Long


l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsTracker As ADODB.Recordset
Dim l_strTmpDate As String

Dim l_strSQL As String
            
'open the recordsets
Set l_rsTracker = New ADODB.Recordset

Set l_rsTracker = ExecuteSQL("SELECT * FROM [trackerprogram] WHERE [trackerprogramID] = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'Get the sheet name and open it or close down and exit
'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "KidsCo Excel Read", "1")
'If l_strExcelSheetName = "" Then
'    oWorkbook.Close
'    l_rsTracker.Close
'    Exit Sub
'End If

For i = 1 To oExcel.Worksheets.Count

    If oExcel.Worksheets(i).Name <> "" Then
        lblProgress.Caption = oExcel.Worksheets(i).Name
        DoEvents
        
        Set oWorksheet = oWorkbook.Worksheets(oExcel.Worksheets(i).Name)
        
        'declare lots of variables for taking the values off the spreadsheet
        Dim l_OrderNumber$, l_ProgrammeTitle$, l_ProgrammeEpisode$, l_Requireddate$, l_Notes$
        Dim l_MasterSpoolNumber$, l_Saveflag%, l_CalculatedDuration&, l_OriginalMaterial$
        Dim l_ProgrammeNumber$, l_Supplier$, l_Channel1$, l_Channel2$, l_Channel3$, l_Channel4$
        Dim l_NewJobNumber&
        
        l_Saveflag% = False
        
        l_OrderNumber$ = GetXLData(5, 4)
        
        If l_OrderNumber$ = "" Then
            Beep
            MsgBox "Sheet could not be read", vbCritical, "Problem!"
            Exit Sub
        End If
        
        l_MasterSpoolNumber$ = GetXLData(9, 8)
        
        'go through the spreadsheet and retrieve the values and put them into
        'variables to be used later. These are from the Order Details section
        l_ProgrammeTitle$ = GetXLData(7, 4)
        l_ProgrammeEpisode$ = GetXLData(7, 11)
        l_ProgrammeNumber$ = GetXLData(9, 13)
        'l_Requireddate$ = GetXLData(7, 16)
        l_OriginalMaterial$ = GetXLData(11, 5)
        l_Supplier$ = GetXLData(9, 4)
        l_Channel1$ = GetXLData(11, 8)
        l_Channel2$ = GetXLData(11, 11)
        l_Channel3$ = GetXLData(11, 14)
        l_Channel4$ = GetXLData(11, 17)
        
        'l_Notes$ = GetXLData(35, 2)
        
        'If l_Requireddate$ <> "" Then
        '    l_datRequiredDate = Format(l_Requireddate$, "dd/mm/yyyy")
        'Else
        '    l_datRequiredDate = Now
        'End If
        
        'l_datTXDate = l_datRequiredDate
        
        If l_Channel1$ <> "" Then
            l_rsTracker.AddNew
            'l_rsTracker("TXdate") = l_datTXDate
            l_rsTracker("companyID") = GetAlias(l_Channel1$)
            
            If l_ProgrammeTitle$ <> "" Then l_rsTracker("title") = Left$(l_ProgrammeTitle$, 50)
            If l_ProgrammeEpisode$ <> "" Then l_rsTracker("episode") = Left$(l_ProgrammeEpisode$, 50)
            If l_ProgrammeNumber$ <> "" Then l_rsTracker("matID") = l_ProgrammeNumber$
            If l_Supplier$ <> "" Then l_rsTracker("supplier") = l_Supplier$
            l_rsTracker("readytowrap") = "NO"
            
            l_rsTracker.Update
        End If
        
        If l_Channel2$ <> "" Then
            l_rsTracker.AddNew
            'l_rsTracker("TXdate") = l_datTXDate
            l_rsTracker("companyID") = GetAlias(l_Channel2$)
            
            If l_ProgrammeTitle$ <> "" Then l_rsTracker("title") = Left$(l_ProgrammeTitle$, 50)
            If l_ProgrammeEpisode$ <> "" Then l_rsTracker("episode") = Left$(l_ProgrammeEpisode$, 50)
            If l_ProgrammeNumber$ <> "" Then l_rsTracker("matID") = l_ProgrammeNumber$
            If l_Supplier$ <> "" Then l_rsTracker("supplier") = l_Supplier$
            l_rsTracker("readytowrap") = "NO"
            
            l_rsTracker.Update
        End If
        
        If l_Channel3$ <> "" Then
            l_rsTracker.AddNew
            'l_rsTracker("TXdate") = l_datTXDate
            l_rsTracker("companyID") = GetAlias(l_Channel3$)
            
            If l_ProgrammeTitle$ <> "" Then l_rsTracker("title") = Left$(l_ProgrammeTitle$, 50)
            If l_ProgrammeEpisode$ <> "" Then l_rsTracker("episode") = Left$(l_ProgrammeEpisode$, 50)
            If l_ProgrammeNumber$ <> "" Then l_rsTracker("matID") = l_ProgrammeNumber$
            If l_Supplier$ <> "" Then l_rsTracker("supplier") = l_Supplier$
            l_rsTracker("readytowrap") = "NO"
            
            l_rsTracker.Update
        End If
        
        If l_Channel4$ <> "" Then
            l_rsTracker.AddNew
            'l_rsTracker("TXdate") = l_datTXDate
            l_rsTracker("companyID") = GetAlias(l_Channel4$)
            
            If l_ProgrammeTitle$ <> "" Then l_rsTracker("title") = Left$(l_ProgrammeTitle$, 50)
            If l_ProgrammeEpisode$ <> "" Then l_rsTracker("episode") = Left$(l_ProgrammeEpisode$, 50)
            If l_ProgrammeNumber$ <> "" Then l_rsTracker("matID") = l_ProgrammeNumber$
            If l_Supplier$ <> "" Then l_rsTracker("supplier") = l_Supplier$
            l_rsTracker("readytowrap") = "NO"
            
            l_rsTracker.Update
        End If
        
        DBEngine.Idle
        DoEvents
    
    End If

Next
'Soundhouse matters

'If GetXLData(22, 2) = "YES" Or GetXLData(24, 2) = "YES" Or GetXLData(26, 2) = "YES" Or GetXLData(28, 2) = "YES" Or GetXLData(30, 2) = "YES" Then
'
'    'Check Which Languages are needed
'    If GetXLData(22, 2) = "YES" Then l_rsTracker("polishsupplier") = GetXLData(22, 14) Else l_rsTracker("polishsupplier") = GetXLData(11, 11)
'    If GetXLData(24, 2) = "YES" Then l_rsTracker("hungariansupplier") = GetXLData(24, 14) Else l_rsTracker("hungariansupplier") = GetXLData(11, 11)
'    If GetXLData(26, 2) = "YES" Then l_rsTracker("romaniansupplier") = GetXLData(26, 14) Else l_rsTracker("romaniansupplier") = GetXLData(11, 11)
'    If GetXLData(28, 2) = "YES" Then l_rsTracker("russiansupplier") = GetXLData(28, 14) Else l_rsTracker("russiansupplier") = GetXLData(11, 11)
'    If GetXLData(30, 2) = "YES" Then l_rsTracker("turkishsupplier") = GetXLData(30, 14) Else l_rsTracker("turkishsupplier") = GetXLData(11, 11)
'    l_rsTracker.Update
'
'End If
    
'close the database and recordset
l_rsTracker.Close: Set l_rsTracker = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing
lblProgress.Caption = ""

'Make some clips in advance
'MakeClipInAdvance 65, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & ".m2v", "clip.dir\media.dir"
'MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "English", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "ENGA.wav", "clip.dir\media.dir"
'MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Polish", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "POLA.wav", "clip.dir\media.dir"
'MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Hungarian", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "HUNA.wav", "clip.dir\media.dir"
'MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Romanian", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "RONA.wav", "clip.dir\media.dir"
'MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Turkish", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "TURA.wav", "clip.dir\media.dir"
'MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Russian", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "RUSA.wav", "clip.dir\media.dir"
'MakeClipInAdvance 64, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "", _
'    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & ".mov", "clip.dir"

'Rename file into the done folder - commented out during testing.
Name l_strExcelFileName As "\\Jcaserver\ceta\Kidsco Processed\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdLibraryMovements_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strShelf As String, l_strBarcode As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

frmMovement.Show
frmMovement.cmbDeliveredBy.Text = g_strFullUserName
frmMovement.cmbLocation.Text = "Library"

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = 2

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strShelf = GetXLData(l_lngXLRowCounter, 1)
    l_strBarcode = GetXLData(l_lngXLRowCounter, 2)
    
    frmMovement.cmbShelf.Text = l_strShelf
    frmMovement.txtBarcode.Text = l_strBarcode
    frmMovement.cmdProcessMovement.Value = True
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

frmMovement.cmdSave.Value = True

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub cmdMotionGallery_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If


'emtpy the comma count field
m_lngCommacount = 0

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset
Dim l_lngLibraryID As Long, l_lngJobID As Long, l_datDueDate As Date

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM [job] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM [jobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName, False, True)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

Dim l_strOrderRef As String, l_strDeliveryFormat As String, l_strContact As String, l_lngCurrent As Long, l_lngTapeCount As Long
Dim l_strBarcode As String

l_strOrderRef = GetXLData(7, 8)
l_strDeliveryFormat = GetXLData(6, 8)
l_strContact = GetXLData(4, 8)
If IsDate(GetXLData(5, 8)) Then
    l_datDueDate = GetXLData(5, 8)
Else
    l_datDueDate = Now
End If

If l_strOrderRef <> "" Then
    'add a new record to the Jobs table
    l_rsJobs.AddNew
    l_rsJobs("createduserID") = 33
    l_rsJobs("createduser") = "Excel"
    l_rsJobs("createddate") = Now
    l_rsJobs("modifieddate") = Now
    l_rsJobs("modifieduserID") = 33
    l_rsJobs("modifieduser") = "Excel"
    l_rsJobs("flagemail") = 0
    l_rsJobs("fd_status") = "Confirmed"
    l_rsJobs("jobtype") = "Dubbing"
    l_rsJobs("joballocation") = "Regular"
    l_rsJobs("deadlinedate") = l_datDueDate
    l_rsJobs("despatchdate") = Now
    l_rsJobs("projectID") = 0
    l_rsJobs("productID") = 0
    
    l_rsJobs("companyID") = 924
    l_rsJobs("companyname") = "BBC Motion Gallery"
    
    'parse the contactname to get the CETA contact ID and name.
    If l_strContact <> "" Then
        l_rsJobs("contactname") = l_strContact
        l_rsJobs("contactID") = GetContactID(l_strContact, 924)
    End If
    
    l_rsJobs("title1") = "Motion Gallery Clips"
    l_rsJobs("title2") = l_strOrderRef
    l_rsJobs("notes2") = l_strDeliveryFormat
    
    
    l_rsJobs.Update
    
    'this part here!
    l_rsJobs.Bookmark = l_rsJobs.Bookmark
    l_lngJobID = l_rsJobs("JobID")
    
    'Set the projectnumber to be the job ID - new CETA feature.
    l_rsJobs("projectnumber") = l_rsJobs("JobID")
    l_rsJobs.Update
                
    DBEngine.Idle
    DoEvents
    
    l_lngCurrent = 0: l_lngTapeCount = 0
    
    While GetXLData(12 + l_lngCurrent, 3) <> ""
        l_strBarcode = Trim(GetXLData(12 + l_lngCurrent, 3))
        
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & l_strBarcode & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        CheckForSQLError
        If l_rsLib.RecordCount > 0 Then
            l_rsLib.MoveFirst
            l_lngLibraryID = l_rsLib("libraryID")
            If l_lngLibraryID <> 0 Then
                'Make the tape required entry
                l_rsHistory.Open "SELECT * FROM requiredmedia WHERE jobID = " & l_lngJobID & " AND libraryID = " & l_lngLibraryID & ";", g_strConnection, adOpenKeyset, adLockOptimistic
                If l_rsHistory.RecordCount <= 0 Then
                    l_strSQL = "INSERT INTO requiredmedia (jobID, libraryID, barcode) VALUES (" & l_lngJobID & ", " & l_lngLibraryID & ", '" & l_strBarcode & "');"
                    ExecuteSQL l_strSQL, g_strExecuteError
                    CheckForSQLError
                    l_lngTapeCount = l_lngTapeCount + 1
                End If
                l_rsHistory.Close
            End If
        Else
            l_rsLib.AddNew
            l_rsLib("barcode") = l_strBarcode
            l_rsLib("title") = "BBC Motion Gallery Tape"
            l_rsLib("companyID") = 924
            l_rsLib("companyname") = "BBC Motion Gallery"
            l_rsLib("location") = "OFF SITE"
            l_rsLib("copytype") = "MASTER"
            l_rsLib("version") = "MASTER"
        
            l_rsLib("cuser") = "XLS"
            l_rsLib("cdate") = Now
            l_rsLib("muser") = "XLS"
            l_rsLib("mdate") = Now
            l_rsLib("code128barcode") = CIA_CODE128(l_strBarcode)
            l_rsLib.Update
            
            'Then we make a history entry too.
            l_rsLib.Bookmark = l_rsLib.Bookmark
            l_lngLibraryID = l_rsLib("libraryID")
            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", g_strConnection, adOpenKeyset, adLockOptimistic
            l_rsHistory.AddNew
            l_rsHistory("libraryID") = l_lngLibraryID
            l_rsHistory("destination") = "Created"
            l_rsHistory("cuser") = "XLS"
            l_rsHistory("cdate") = Now
            l_rsHistory.Update
            l_rsHistory.Close
            
            'Make the tape required entry
            l_rsHistory.Open "SELECT * FROM requiredmedia WHERE jobID = " & l_lngJobID & " AND libraryID = " & l_lngLibraryID & ";", g_strConnection, adOpenKeyset, adLockOptimistic
            If l_rsHistory.RecordCount <= 0 Then
                l_strSQL = "INSERT INTO requiredmedia (jobID, libraryID, barcode) VALUES (" & l_lngJobID & ", " & l_lngLibraryID & ", '" & l_strBarcode & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
                l_lngTapeCount = l_lngTapeCount + 1
            End If
            l_rsHistory.Close
        End If
        l_rsLib.Close
        l_lngCurrent = l_lngCurrent + 1
    Wend
    
    If l_lngCurrent > 0 Then
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_lngJobID
        l_rsDetails("copytype") = "M"
        l_rsDetails("format") = "MEDHR"
        l_rsDetails("videostandard") = "625PAL"
        l_rsDetails("description") = l_lngCurrent & " Clips from " & l_lngTapeCount & " Tapes"
        l_rsDetails("quantity") = 1
        l_rsDetails("runningtime") = 0
        l_rsDetails.Update
        
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_lngJobID
        l_rsDetails("Copytype") = "O"
        l_rsDetails("quantity") = 1
        l_rsDetails("description") = "Media Del. (FTP/Aspera/Portal Download... per GB.)"
        l_rsDetails("format") = "MEDDEL"
        l_rsDetails.Update
    End If
End If

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
Set l_rsLib = Nothing
Set l_rsHistory = Nothing

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

'Rename file into the done folder - commented out during testing.
Name l_strExcelFileName As "\\jcaserver\ceta\Motion Gallery WorkOrders Processed\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdNewDADCIngestRequest_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strLastBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngCRID As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String, l_strLegacyFrameRate As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String, l_strDurationEstimate As String, l_lngLastTrackerID As Long, l_strAudioContent As String
Dim l_strDADCStatus As String, l_datDueDate As Date, l_strDueDate As String, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset, l_lngExternalAlphaKey As Long, l_lngWorkflowID As Long
Dim l_lngMonth As Long, l_lngDay As Long, l_lngYear As Long, l_lngCount1 As Long, l_lngCount2 As Long, l_blnDateChange As Boolean, l_datOldDate As Date, l_lngBatchNumber As Long
Dim l_strAdditionalElements As String, l_strContentVersionCode As String, l_lngContentVersionID As Long

Dim l_strSQL As String, TempStr As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "BBCW Ingest Queue Inventory Rep")
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "Ingest Queue Inventory")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strLastBarcode = ""
l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_blnDateChange = False
    l_lngCRID = Val(Trim(GetXLData(l_lngXLRowCounter, 2))) 'B
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 31)) 'AE
    l_strDADCStatus = Trim(GetXLData(l_lngXLRowCounter, 3)) 'C
    l_strAlphaBusiness = Trim(GetXLData(l_lngXLRowCounter, 29)) 'AC
    l_strContentVersionCode = Trim(GetXLData(l_lngXLRowCounter, 15)) 'O
    l_lngContentVersionID = Val(Trim(GetXLData(l_lngXLRowCounter, 80))) 'CB
    
    
    If LCase(l_strDADCStatus) = "requested" And l_strBarcode <> "" Then
    
        l_strTitle = GetXLData(l_lngXLRowCounter, 10) 'J
        l_strSubTitle = GetXLData(l_lngXLRowCounter, 13) 'M
        l_lngEpisode = GetXLData(l_lngXLRowCounter, 12) 'L
        l_strFormat = ""
        l_strLegacyFrameRate = GetXLData(l_lngXLRowCounter, 34) 'AH
        l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 35) 'AI
        l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 38) 'AL
        l_strLegacyPictureElement = ""
        l_strAdditionalElements = GetXLData(l_lngXLRowCounter, 45) 'AS
        l_strLanguage = GetXLData(l_lngXLRowCounter, 48) 'AV
        If l_strLanguage = "None" Then l_strLanguage = ""
        l_strAudioConfiguration = GetXLData(l_lngXLRowCounter, 46) 'AT
        l_strAudioContent = GetXLData(l_lngXLRowCounter, 47) 'AU
        l_strDurationEstimate = ""
        l_strDueDate = GetXLData(l_lngXLRowCounter, 75) 'BW
        If l_strDueDate <> "" Then
'            'Code for dwaling with US format dates
'            l_lngCount1 = InStr(l_strDueDate, "/")
'            l_lngDay = Val(Mid(l_strDueDate, l_lngCount1 + 1))
'            l_lngCount2 = InStr(Mid(l_strDueDate, l_lngCount1 + 1), "/")
'            l_lngYear = Val(Mid(Mid(l_strDueDate, l_lngCount1 + 1), l_lngCount2 + 1, 4))
'            l_lngMonth = Val(l_strDueDate)
'            l_datDueDate = l_lngYear & "-" & Format(l_lngMonth, "00") & "-" & Format(l_lngDay, "00")
            'Code for dealing with UK format dates
            If IsDate(l_strDueDate) Then
                l_datDueDate = l_strDueDate
            Else
                MsgBox "Unreadable Due Date - " & l_strDueDate & vbCrLf & "2099-12-12  will be used instead.", vbInformation, "Error reading Excel Sheet"
                l_datDueDate = "2099-12-12"
            End If
        Else
            l_datDueDate = "2099-12-12"
        End If
        l_lngBatchNumber = Format(l_datDueDate, "ddmmyy")
'        l_lngExternalAlphaKey = Val(GetXLData(l_lngXLRowCounter, 30)) 'AD
        l_lngWorkflowID = Val(GetXLData(l_lngXLRowCounter, 54)) 'BB
        If l_strContentVersionCode <> "" Then l_strAlphaBusiness = ""
        If l_strAlphaBusiness = "" Then
            l_strReference = l_strContentVersionCode & "_" & SanitiseBarcode(l_strBarcode) & "_" & l_lngContentVersionID & "_" & l_lngWorkflowID
            If l_lngContentVersionID = 0 Or l_lngWorkflowID = 0 Then
                l_strReference = ""
            End If
        Else
            l_strReference = l_strAlphaBusiness & "_" & SanitiseBarcode(l_strBarcode) & "_" & l_lngExternalAlphaKey & "_" & l_lngWorkflowID
            If l_lngExternalAlphaKey = 0 Or l_lngWorkflowID = 0 Then
                l_strReference = ""
            End If
        End If
        If Not IsDate(l_strDueDate) Then l_strDueDate = ""
        
        lblProgress.Caption = l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
        DoEvents
        
        TempStr = ""
        If l_strAlphaBusiness <> "" Then
            TempStr = GetDataSQL("SELECT tracker_dadc_itemID FROM tracker_dadc_item WHERE barcode='" & UCase(l_strBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1261;")
        Else
            TempStr = GetDataSQL("SELECT tracker_dadc_itemID FROM tracker_dadc_item WHERE barcode='" & UCase(l_strBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1261;")
        End If
        If Val(TempStr) = 0 Then
            TempStr = GetDataSQL("SELECT tracker_dadc_itemID FROM tracker_dadc_item WHERE barcode='" & UCase(l_strBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            If l_strAlphaBusiness = "" Then
                l_strSQL = "INSERT INTO tracker_dadc_item (cdate, mdate, companyID, itemreference, title, subtitle, barcode, newbarcode, format, contentversioncode, legacylinestandard, legacyframerate, "
                l_strSQL = l_strSQL & "imageaspectratio, legacypictureelement, additionalelementsrequested, language, "
                l_strSQL = l_strSQL & "subtitleslanguage, fivepointonerequested, dolbyErequested, musicandeffectsrequested, episode, dbbcrid, duedateupload, contentversionID, workflowID, durationestimate, batch) VALUES ("
            Else
                l_strSQL = "INSERT INTO tracker_dadc_item (cdate, mdate, companyID, itemreference, title, subtitle, barcode, newbarcode, format, contentversioncode, externalalphaID, originalexternalalphaID, legacylinestandard, legacyframerate, "
                l_strSQL = l_strSQL & "imageaspectratio, legacypictureelement, additionalelementsrequested, language, "
                l_strSQL = l_strSQL & "subtitleslanguage, fivepointonerequested, dolbyErequested, musicandeffectsrequested, episode, dbbcrid, duedateupload, workflowID, durationestimate, batch) VALUES ("
            End If
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
            l_strSQL = l_strSQL & "1261, "
            If l_strReference <> "" Then
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strReference) & "', "
            Else
                l_strSQL = l_strSQL & "NULL, "
            End If
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strTitle) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strSubTitle) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(UCase(l_strBarcode)) & "', "
            l_strSQL = l_strSQL & "'" & l_strFormat & "', "
            If l_strAlphaBusiness = "" Then
                l_strSQL = l_strSQL & "'" & l_strContentVersionCode & "', "
            Else
                l_strSQL = l_strSQL & "'" & l_strContentVersionCode & "', "
                l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
                l_strSQL = l_strSQL & "'" & l_strAlphaBusiness & "', "
            End If
            l_strSQL = l_strSQL & "'" & l_strLegacyLineStandard & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyFrameRate & "', "
            l_strSQL = l_strSQL & "'" & l_strImageAspectRatio & "', "
            l_strSQL = l_strSQL & "'" & l_strLegacyPictureElement & "', "
            If l_strAdditionalElements = "Yes" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & "'" & l_strLanguage & "', "
            l_strSQL = l_strSQL & "'" & l_strSubtitlesLanguage & "', "
            If l_strAudioConfiguration = "5.1" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            If l_strAudioContent = "M&E" Then
                l_strSQL = l_strSQL & "1, "
            Else
                l_strSQL = l_strSQL & "0, "
            End If
            l_strSQL = l_strSQL & l_lngEpisode & ", "
            l_strSQL = l_strSQL & l_lngCRID & ", "
            l_strSQL = l_strSQL & "'" & FormatSQLDate(l_datDueDate) & "', "
'            l_strSQL = l_strSQL & l_lngExternalAlphaKey & ", "
            If l_strAlphaBusiness = "" Then l_strSQL = l_strSQL & l_lngContentVersionID & ", "
            l_strSQL = l_strSQL & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & Int(Val(l_strDurationEstimate)) & ", "
            l_strSQL = l_strSQL & l_lngBatchNumber & ");"
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If Val(TempStr) <> 0 Then
                l_strEmailBody = l_strEmailBody & "Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & " - already existed for Bulk Ingest" & vbCrLf
            Else
                l_strEmailBody = l_strEmailBody & "Added: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle & vbCrLf
            End If
        Else
            If Trim(" " & GetData("tracker_dadc_item", "duedateupload", "tracker_dadc_itemID", TempStr)) <> "" Then
                l_datOldDate = CDate(Trim(" " & GetData("tracker_dadc_item", "duedateupload", "tracker_dadc_itemID", TempStr)))
                If l_datDueDate <> l_datOldDate And (l_datOldDate = "2099-12-12" Or l_datDueDate <> "2099-12-12") Then
                    l_blnDateChange = True
                End If
            Else
                l_blnDateChange = True
            End If
            TempStr = ""
            If l_strAlphaBusiness <> "" Then TempStr = GetDataSQL("SELECT tracker_dadc_itemID FROM tracker_dadc_item WHERE barcode='" & UCase(l_strBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1239;")
            l_strSQL = "UPDATE tracker_dadc_item SET "
            If l_strReference <> "" Then l_strSQL = l_strSQL & "itemreference = '" & QuoteSanitise(l_strReference) & "', "
            If l_strImageAspectRatio <> "" Then l_strSQL = l_strSQL & "imageaspectratio = '" & l_strImageAspectRatio & "', "
            If l_strLegacyFrameRate <> "" Then l_strSQL = l_strSQL & "legacyframerate = '" & l_strLegacyFrameRate & "', "
            If l_strLegacyLineStandard <> "" Then l_strSQL = l_strSQL & "legacylinestandard = '" & l_strLegacyLineStandard & "', "
            If l_blnDateChange = True Then
                l_strSQL = l_strSQL & "duedateupload = '" & FormatSQLDate(l_datDueDate) & "', "
                l_strSQL = l_strSQL & "batch = " & l_lngBatchNumber & ", "
            End If
            If l_lngExternalAlphaKey <> 0 Then l_strSQL = l_strSQL & "externalalphakey = " & l_lngExternalAlphaKey & ", "
            If l_lngContentVersionID <> 0 Then l_strSQL = l_strSQL & "contentversionID = " & l_lngContentVersionID & ", "
            If l_lngWorkflowID <> 0 Then l_strSQL = l_strSQL & "workflowID = " & l_lngWorkflowID & ", "
            l_strSQL = l_strSQL & "dbbcrid = " & l_lngCRID & ", "
            If l_strAudioConfiguration = "5.1" Then l_strSQL = l_strSQL & "fivepointonerequested = 1, "
            If Left(l_strAudioConfiguration, 7) = "Dolby E" Then l_strSQL = l_strSQL & "dolbyErequested = 1, "
            If l_strAudioContent = "M&E" Then l_strSQL = l_strSQL & "musicandeffectsrequested = 1, "
            If l_strAdditionalElements = "Yes" Then l_strSQL = l_strSQL & "additionalelementsrequested = 1, "
            If l_strLanguage <> "" Then l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
            l_strSQL = l_strSQL & "mdate = '" & FormatSQLDate(Now) & "' "
            If l_strAlphaBusiness <> "" Then
                l_strSQL = l_strSQL & "WHERE barcode = '" & QuoteSanitise(l_strBarcode) & "' AND externalalphaID = '" & l_strAlphaBusiness & "' AND companyID = 1261;"
            Else
                l_strSQL = l_strSQL & "WHERE barcode = '" & QuoteSanitise(l_strBarcode) & "' AND contentversioncode = '" & l_strContentVersionCode & "' AND companyID = 1261;"
            End If
            Debug.Print l_strSQL
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
            If l_blnDateChange = True Then
                l_strEmailBody = l_strEmailBody & "Updated with Date Change: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle
            Else
                l_strEmailBody = l_strEmailBody & "Updated: " & l_strReference & " - Request ID " & l_lngCRID & ", " & l_strTitle & ", Ep " & l_lngEpisode & ", " & l_strSubTitle
            End If
            If Val(TempStr) <> 0 Then
                l_strEmailBody = l_strEmailBody & " - already existed for Bulk Ingest" & vbCrLf
            Else
                l_strEmailBody = l_strEmailBody & vbCrLf
            End If
        End If
        
    End If
    
    l_strLastBarcode = l_strBarcode
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new items were added to the DBB Manual Ingest Tracker: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 1261) & "' AND trackermessageID = 13;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "DADC Manual Ingest Tracker Items Added", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdPatheKeywords_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_lngXLRowCounter As Long
Dim l_strExcelSheetName As String, i As Long, l_blnKeepKeywords As Boolean
Dim l_rstClip As ADODB.Recordset, l_strSQL As String
Dim l_strClipReference As String, l_strExp_Title As String, l_strExp_Desc As String, l_strFilm_ID As String, l_strSummary As String, l_strKeyword As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

'Get the sheet name and open it or close down and exit
l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Worksheet Name", "Pathe YT Data")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "Worksheet Row", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

If MsgBox("Replace Keywords on clip " & l_strClipReference, vbYesNo, "Replace Keywords") = vbYes Then
    l_blnKeepKeywords = False
Else
    l_blnKeepKeywords = True
End If

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    lblProgress.Caption = GetXLData(l_lngXLRowCounter, 1)
    DoEvents
    
    l_strClipReference = GetXLData(l_lngXLRowCounter, 6)
    l_strFilm_ID = GetXLData(l_lngXLRowCounter, 4)
    l_strSummary = GetXLData(l_lngXLRowCounter, 5)
    l_strExp_Title = GetXLData(l_lngXLRowCounter, 2)
    l_strExp_Desc = GetXLData(l_lngXLRowCounter, 3)
    
    If Val(Trim(" " & GetDataSQL("SELECT eventID FROM events WHERE companyID = 905 AND clipreference = '" & l_strClipReference & "';"))) <> 0 Then

        l_strSQL = "UPDATE events SET video_longdescription = '" & QuoteSanitise(l_strSummary) & "', "
        l_strSQL = l_strSQL & "notes = '" & QuoteSanitise(l_strExp_Desc) & "', "
        l_strSQL = l_strSQL & "eventtitle = '" & QuoteSanitise(l_strExp_Title) & "' "
        l_strSQL = l_strSQL & "WHERE companyID = 905 AND clipreference = '" & l_strClipReference & "';"
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    
        If l_blnKeepKeywords = False Then
            l_strSQL = "DELETE FROM eventkeyword WHERE companyID = 905 and clipreference = '" & l_strClipReference & "';"
            ExecuteSQL l_strSQL, g_strExecuteError
            CheckForSQLError
        End If
        
        For i = 7 To 71
            l_strKeyword = GetXLData(l_lngXLRowCounter, i)
            If l_strKeyword <> "" Then
                l_strSQL = "INSERT INTO eventkeyword (companyID, clipreference, keywordtext) VALUES ("
                l_strSQL = l_strSQL & "905, "
                l_strSQL = l_strSQL & "'" & l_strClipReference & "', "
                l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strKeyword) & "');"
                ExecuteSQL l_strSQL, g_strExecuteError
                CheckForSQLError
            End If
        Next
    Else
        
        MsgBox "Row - " & GetXLData(l_lngXLRowCounter, 1) & ", reference " & l_strClipReference & " record not found", vbInformation, "No Record"
    
    End If
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

'Private Sub cmdSvensk_Click()
'
'If Not CheckAccess("/processexcelorders") Then
'    Exit Sub
'End If
'
''have a variable to hold the filename we are going to process
'Dim l_strExcelFileName As String, l_strExcelFileTitle As String, l_datRequiredDate As Date, l_datSoundHouseDate As Date, l_datTXDate As Date
'Dim l_strExcelSheetName As String, i As Long
'
'
'l_strExcelFileName = txtExcelFilename.Text
'l_strExcelFileTitle = lblFileTitle.Caption
'
'If l_strExcelFileName = "" Then
'
'    Beep
'    Exit Sub
'
'End If
'
'Dim l_rsTracker As ADODB.Recordset
'Dim l_strTmpDate As String
'
'Dim l_strSQL As String
'
''open the recordsets
'Set l_rsTracker = New ADODB.Recordset
'
'Set l_rsTracker = ExecuteSQL("SELECT * FROM tracker_Svensk_item WHERE tracker_Svensk_itemID = -1", g_strExecuteError)
'CheckForSQLError
'
''free up locks
'DoEvents
'
''open the spreadsheet from the root of C:\
'
'Set oExcel = CreateObject("Excel.Application")
'Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
'
''Get the sheet name and open it or close down and exit
''l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "KidsCo Excel Read", "1")
''If l_strExcelSheetName = "" Then
''    oWorkbook.Close
''    l_rsTracker.Close
''    Exit Sub
''End If
'
'For i = 1 To oExcel.Worksheets.Count
'
'    If oExcel.Worksheets(i).Name <> "" Then
'        lblProgress.Caption = oExcel.Worksheets(i).Name
'        DoEvents
'
'        Set oWorksheet = oWorkbook.Worksheets(oExcel.Worksheets(i).Name)
'
'        'declare lots of variables for taking the values off the spreadsheet
'        Dim l_OrderNumber$, l_ProgrammeTitle$, l_ProgrammeEpisode$, l_Priority$, l_Notes$
'        Dim l_Channel$
'        Dim l_ProgrammeNumber$, l_Version$
'        Dim l_NewJobNumber&
'        Dim l_Supplier$
'
'        l_OrderNumber$ = GetXLData(4, 4)
'
'        If l_OrderNumber$ = "" Then
'            Beep
'            MsgBox "Sheet could not be read", vbCritical, "Problem!"
'            Exit Sub
'        End If
'
'        'go through the spreadsheet and retrieve the values and put them into
'        'variables to be used later. These are from the Order Details section
'        l_ProgrammeTitle$ = GetXLData(6, 4)
'        l_ProgrammeEpisode$ = GetXLData(6, 11)
'        l_ProgrammeNumber$ = GetXLData(9, 16)
'        l_Supplier$ = GetXLData(9, 4)
'        l_Version$ = GetXLData(7, 4)
'        l_Channel$ = GetXLData(12, 15)
'        l_Channel$ = LCase(l_Channel$)
'        l_Channel$ = UCase(Left(l_Channel$, 1)) & Mid(l_Channel$, 2)
'        l_Priority$ = GetXLData(6, 16)
'
'        l_Notes$ = GetXLData(29, 2)
'
'        l_rsTracker.AddNew
'        'l_rsTracker("TXdate") = l_datTXDate
'        l_rsTracker("companyID") = 1262
'
'        If l_ProgrammeTitle$ <> "" Then l_rsTracker("title") = Left$(l_ProgrammeTitle$, 255)
'        If l_Supplier$ <> "" Then l_rsTracker("supplier") = Left(l_Supplier$, 50)
'        If l_ProgrammeEpisode$ <> "" Then
'            If Val(l_ProgrammeEpisode$) <> 0 Then
'                l_rsTracker("episode") = Val(l_ProgrammeEpisode$)
'            Else
'                l_rsTracker("subtitle") = Left(l_ProgrammeEpisode$, 255)
'            End If
'        End If
'
'        If l_ProgrammeNumber$ <> "" Then l_rsTracker("itemreference") = l_ProgrammeNumber$
'        If l_Channel$ <> "" Then
'            l_rsTracker("channel") = l_Channel$
'            Select Case l_Channel$
'                Case "English"
'                    l_rsTracker("englishaudioneeded") = 1
'                    l_rsTracker("frenchaudioneeded") = 0
'                    l_rsTracker("brazillianportaudioneeded") = 0
''                    l_rsTracker("englishsubsneeded") = 1
''                    l_rsTracker("frenchsubsneeded") = 0
''                    l_rsTracker("brazillianportsubsneeded") = 0
'                Case "French"
'                    l_rsTracker("englishaudioneeded") = 0
'                    l_rsTracker("frenchaudioneeded") = 1
'                    l_rsTracker("brazillianportaudioneeded") = 0
''                    l_rsTracker("englishsubsneeded") = 0
''                    l_rsTracker("frenchsubsneeded") = 1
''                    l_rsTracker("brazillianportsubsneeded") = 0
'                Case Else
'                    l_rsTracker("englishaudioneeded") = 0
'                    l_rsTracker("frenchaudioneeded") = 0
'                    l_rsTracker("brazillianportaudioneeded") = 1
' '                   l_rsTracker("englishsubsneeded") = 0
' '                   l_rsTracker("frenchsubsneeded") = 0
' '                   l_rsTracker("brazillianportsubsneeded") = 1
'            End Select
'        End If
'
'        If l_Priority$ = "SEND TO TX WITHIN 24 HOURS OF DELIVERY" Then
'            l_rsTracker("priorityflag") = 1
'        Else
'            l_rsTracker("priorityflag") = 0
'        End If
'
'        l_rsTracker("readyforTX") = 0
'
'        l_rsTracker.Update
'
'        l_strSQL = "INSERT INTO job (createduserID, createduser, createddate, modifieddate, modifieduserID, modifieduser, fd_status, jobtype, joballocation, deadlinedate, despatchdate, projectID, productID, "
'        l_strSQL = l_strSQL & "companyID, companyname, contactID, contactname, title1, title2, orderreference) VALUES ("
'
'        l_strSQL = l_strSQL & "33, "
'        l_strSQL = l_strSQL & "'Excel', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
'        l_strSQL = l_strSQL & "33, "
'        l_strSQL = l_strSQL & "'Excel', "
'        l_strSQL = l_strSQL & "'Confirmed', "
'        l_strSQL = l_strSQL & "'Dubbing', "
'        l_strSQL = l_strSQL & "'Regular', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 1, Now)) & "', "
'        l_strSQL = l_strSQL & "'" & FormatSQLDate(DateAdd("d", 1, Now)) & "', "
'        l_strSQL = l_strSQL & "0, "
'        l_strSQL = l_strSQL & "0, "
'        l_strSQL = l_strSQL & "1262, "
'        l_strSQL = l_strSQL & "'Svensk Modem (Renault TV)', "
'        l_strSQL = l_strSQL & "2555, "
'        l_strSQL = l_strSQL & "'Nikki Cripps-Harding', "
'        l_strSQL = l_strSQL & "'" & Left(l_ProgrammeTitle$, 50) & "', "
'        If l_ProgrammeEpisode$ <> "" Then
'            If Val(l_ProgrammeEpisode$) <> 0 Then
'                l_strSQL = l_strSQL & "' Ep. " & Val(l_ProgrammeEpisode$) & "', "
'            Else
'                l_strSQL = l_strSQL & "'" & Left(l_ProgrammeEpisode$, 50) & "', "
'            End If
'        Else
'            l_strSQL = l_strSQL & "Null, "
'        End If
'        l_strSQL = l_strSQL & "'" & l_OrderNumber$ & "');"
'        ExecuteSQL l_strSQL, g_strExecuteError
'        CheckForSQLError
'
'        DBEngine.Idle
'        DoEvents
'
'    End If
'
'Next
'
''close the database and recordset
'l_rsTracker.Close: Set l_rsTracker = Nothing
'
'Set oWorksheet = Nothing
'oWorkbook.Close
'Set oWorkbook = Nothing
'oExcel.Quit
'Set oExcel = Nothing
'lblProgress.Caption = ""
'
''Make some clips in advance
''MakeClipInAdvance 65, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & ".m2v", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "English", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "ENGA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Polish", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "POLA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Hungarian", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "HUNA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Romanian", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "RONA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Turkish", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "TURA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 66, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "Russian", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & "RUSA.wav", "clip.dir\media.dir"
''MakeClipInAdvance 64, l_ProgrammeTitle$, Val(Right(l_ProgrammeEpisode$, 5)), l_NewJobNumber&, l_ProgrammeNumber$, "", _
''    GetNextSequence("internalreference"), "JCAOMNEON", 782, l_Supplier$, l_ProgrammeNumber$ & ".mov", "clip.dir"
'
''Rename file into the done folder - commented out during testing.
'Name l_strExcelFileName As "\\Jcaserver\ceta\Svensk Processed\" & l_strExcelFileTitle
'
'MsgBox "File Sucessfully Read", vbInformation, "Excel Read"
'
'End Sub
'
Private Sub cmdSelectFile_Click()

'Select the file using MDIform's common dialog box.

MDIForm1.dlgMain.InitDir = g_strExcelOrderLocation
MDIForm1.dlgMain.Filter = "All Files|*.*"
MDIForm1.dlgMain.ShowOpen

txtExcelFilename.Text = MDIForm1.dlgMain.fileName
lblFileTitle.Caption = MDIForm1.dlgMain.FileTitle

End Sub

Private Sub cmdShineSeriesIDMediaReadback_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strVersion As String, l_strSeries As String, l_strSeriesID As String, l_lngEventID As Long

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Shine Library Readback", "clipsearch")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "SMVB Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_lngEventID = Val(Trim(GetXLData(l_lngXLRowCounter, 1)))
    lblProgress.Caption = l_lngXLRowCounter & ": " & l_lngEventID
    If l_lngEventID <> 0 Then
        'Tape exists already in library
        lblProgress.Caption = lblProgress.Caption & " - UPDATE"
    
        If GetData("events", "companyID", "EventID", l_lngEventID) = 769 Then
            SetData "events", "eventtitle", "EventID", l_lngEventID, Trim(GetXLData(l_lngXLRowCounter, 2))
            SetData "events", "eventepisode", "EventID", l_lngEventID, Trim(GetXLData(l_lngXLRowCounter, 5))
            SetData "events", "eventsubtitle", "EventID", l_lngEventID, Trim(GetXLData(l_lngXLRowCounter, 6))
            SetData "events", "eventversion", "EventID", l_lngEventID, Trim(GetXLData(l_lngXLRowCounter, 7))
            SetData "events", "customfield1", "EventID", l_lngEventID, Trim(GetXLData(l_lngXLRowCounter, 9))
            SetData "events", "customfield2", "EventID", l_lngEventID, Trim(GetXLData(l_lngXLRowCounter, 10))
        Else
            MsgBox "Shine File Not Found", vbInformation, "Nothing Updated"
        End If
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdShineSeriesIDReadback_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strVersion As String, l_strSeries As String, l_strSeriesID As String, l_lngLibraryID As Long

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Shine Library Readback", "Shine_Tapes")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "SMVB Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_lngLibraryID = Val(Trim(GetXLData(l_lngXLRowCounter, 1)))
    lblProgress.Caption = l_lngXLRowCounter & ": " & l_lngLibraryID
    If l_lngLibraryID <> 0 Then
        'Tape exists already in library
        lblProgress.Caption = lblProgress.Caption & " - UPDATE"
    
        If GetData("library", "companyID", "LibraryID", l_lngLibraryID) = 769 Then
            SetData "library", "title", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 5))
            SetData "library", "episode", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 8))
            SetData "library", "subtitle", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 11))
            SetData "library", "version", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 10))
            SetData "library", "customfield1", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 13))
            SetData "library", "customfield2", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 14))
        End If
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdShineSeriesIDs_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strTitle As String, l_strSeriesID As String, l_strOldSeriesID As String, l_strType As String, l_lngNumberOfEpisodes As Long

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Shine Excel Read", "JagTest Output")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "Shine Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

While GetXLData(l_lngXLRowCounter, 4) <> ""
    l_strSeriesID = Trim(GetXLData(l_lngXLRowCounter, 4))
    l_strTitle = Trim(GetXLData(l_lngXLRowCounter, 5))
    l_strType = GetXLData(l_lngXLRowCounter, 3)
    l_lngNumberOfEpisodes = Val(GetXLData(l_lngXLRowCounter, 13))
    lblProgress.Caption = l_lngXLRowCounter & ": " & l_strSeriesID
    
    If GetDataSQL("SELECT masterseriestitleID FROM masterseriestitle WHERE seriesID = " & Val(l_strSeriesID) & " AND companyID = 769;") <> "" And l_strType = "Finished Tape" Then
        lblProgress.Caption = lblProgress.Caption & " - Update - " & l_strTitle
        If GetDataSQL("SELECT title FROM masterseriestitle WHERE seriesID = " & Val(l_strSeriesID) & " AND companyID = 769;") <> l_strTitle Then
            MsgBox "New Title: " & l_strTitle, vbInformation, "Title Changed"
'            l_strSQL = "UPDATE library SET "
'            l_strSQL = l_strSQL & "title = '" & Trim(QuoteSanitise(l_strTitle)) & "' "
'            l_strSQL = l_strSQL & "WHERE seriesID = " & Val(l_strSeriesID) & " AND companyID = 769;"
'            ExecuteSQL l_strSQL, g_strExecuteError
'            CheckForSQLError
'            l_strSQL = "UPDATE events SET "
'            l_strSQL = l_strSQL & "eventtitle = '" & Trim(QuoteSanitise(l_strTitle)) & "' "
'            l_strSQL = l_strSQL & "WHERE seriesID = " & Val(l_strSeriesID) & " AND companyID = 769;"
'            ExecuteSQL l_strSQL, g_strExecuteError
'            CheckForSQLError
        End If
        l_strSQL = "UPDATE masterseriestitle SET "
        l_strSQL = l_strSQL & "title = '" & QuoteSanitise(l_strTitle) & "', "
        l_strSQL = l_strSQL & "numberofepisodes = " & l_lngNumberOfEpisodes & ", "
        l_strSQL = l_strSQL & "mdate = getdate(), "
        l_strSQL = l_strSQL & "muser = '" & g_strFullUserName & "' "
        l_strSQL = l_strSQL & "WHERE seriesID = " & Val(l_strSeriesID) & " AND companyID = 769;"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    ElseIf l_strType = "Finished Tape" Then
        lblProgress.Caption = lblProgress.Caption & " - New Title - " & l_strTitle
        l_strSQL = "INSERT INTO masterseriestitle (seriesID, title, numberofepisodes, companyID, cdate, mdate, cuser, muser) VALUES (" & Val(l_strSeriesID) & ", '" & QuoteSanitise(l_strTitle) & "', " & l_lngNumberOfEpisodes & ", 769, getdate(), getdate(), '" & g_strFullUserName & "', '" & g_strFullUserName & "');"
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1
Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdShineSMVDigital_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strTrackCount As String
Dim l_strFilename As String, l_strSeriesTitle As String, l_strSeries As String, l_strEpisode As String
Dim l_strFormat As String, l_strVideoCodec As String, l_strVideoBitrate As String, l_strFramerate As String, l_strWidth As String, l_strHeight As String, l_strAspectRatio As String, l_strPAR As String
Dim l_strDuration As String, l_strAudioCodec As String, l_strAudioBitRate As String, l_strAudioBitDepth As String, l_strSampleRate As String

Dim l_strSQL As String, l_lngEventID As Long, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "SMV Excel Read", "Shine Inventory")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "SMV Excel Read", 1))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strFilename = Trim(GetXLData(l_lngXLRowCounter, 1)) ' A
    lblProgress.Caption = l_lngXLRowCounter & ": " & l_strFilename
    If GetDataSQL("SELECT eventID FROM events WHERE (libraryID = 497200 OR libraryID = 445810) AND companyID = 769 AND clipfilename = '" & QuoteSanitise(l_strFilename) & "';") <> 0 Then
        'clip exists already in library
        l_lngEventID = GetDataSQL("SELECT eventID FROM events WHERE (libraryID = 497200 OR libraryID = 445810) AND companyID = 769 AND clipfilename = '" & QuoteSanitise(l_strFilename) & "';")
        SetData "events", "libraryID", "eventID", l_lngEventID, 445810
        SetData "events", "hidefromweb", "eventID", l_lngEventID, 0
        SetData "events", "online", "eventID", l_lngEventID, 1
        l_strEmailBody = l_strEmailBody & l_strFilename & vbCrLf
    Else
        'New clip - needs entries making
        l_lngEventID = CreateClip
        SetData "events", "companyID", "eventID", l_lngEventID, 769
        SetData "events", "companyname", "eventID", l_lngEventID, "Shine International"
        SetData "events", "libraryID", "eventID", l_lngEventID, 445810
        SetData "events", "hidefromweb", "eventID", l_lngEventID, 0
        l_strEmailBody = l_strEmailBody & l_strFilename & vbCrLf
        SetData "events", "online", "eventID", l_lngEventID, 1
    End If
    
    'Read the Data
    l_strSeriesTitle = Trim(GetXLData(l_lngXLRowCounter, 2)) ' B
    l_strSeries = Trim(GetXLData(l_lngXLRowCounter, 3)) ' C
    l_strEpisode = Trim(GetXLData(l_lngXLRowCounter, 4)) ' D
    
    l_strFormat = GetAlias(Trim(GetXLData(l_lngXLRowCounter, 10))) ' J
    l_strVideoCodec = GetAlias(Trim(GetXLData(l_lngXLRowCounter, 11))) ' K
    l_strFramerate = GetXLData(l_lngXLRowCounter, 7) ' G
    l_strWidth = GetXLData(l_lngXLRowCounter, 12) ' L
    l_strHeight = GetXLData(l_lngXLRowCounter, 13) ' M
    l_strAspectRatio = GetAlias(GetXLData(l_lngXLRowCounter, 15)) ' O
    l_strDuration = GetXLData(l_lngXLRowCounter, 6) ' F
    l_strAudioCodec = GetXLData(l_lngXLRowCounter, 18) ' R
    l_strAudioBitDepth = GetXLData(l_lngXLRowCounter, 20) ' T
    l_strSampleRate = GetXLData(l_lngXLRowCounter, 19) ' S
    l_strTrackCount = GetXLData(l_lngXLRowCounter, 17) ' Q
    
    'Write the data to the clip record - either updates an existing, or sets data on a new.
    If GetData("events", "seriesID", "eventID", l_lngEventID) = 0 Then SetData "events", "eventtitle", "eventID", l_lngEventID, QuoteSanitise(l_strSeriesTitle)
    SetData "events", "eventseries", "eventID", l_lngEventID, l_strSeries
    SetData "events", "eventepisode", "eventID", l_lngEventID, l_strEpisode
    SetData "events", "clipformat", "eventID", l_lngEventID, l_strFormat
    SetData "events", "fileversion", "eventID", l_lngEventID, "Original Masterfile"
    SetData "events", "clipcodec", "eventID", l_lngEventID, l_strVideoCodec
    'SetData "events", "videobitrate", "eventID", l_lngEventID, Val(l_strVideoBitrate)
    'SetData "events", "audiobitrate", "eventID", l_lngEventID, Val(l_strAudioBitRate)
    'SetData "events", "clipbitrate", "eventID", l_lngEventID, Val(l_strVideoBitrate) + Val(l_strAudioBitRate)
    SetData "events", "cliphorizontalpixels", "eventID", l_lngEventID, Val(l_strWidth)
    SetData "events", "clipverticalpixels", "eventID", l_lngEventID, Val(l_strHeight)
    SetData "events", "aspectratio", "eventID", l_lngEventID, l_strAspectRatio
    If (l_strAspectRatio = "4:3") Or (l_strAspectRatio = "4x3") Then SetData "events", "fourbythreeflag", "eventID", l_lngEventID, 1
    'SetData "events", "geometriclinearity", "eventID", l_lngEventID, l_strPAR
    SetData "events", "clipframerate", "eventID", l_lngEventID, l_strFramerate
    If l_strFramerate = "29.97" Then
        SetData "events", "timecodestart", "eventID", l_lngEventID, "00:00:00;00"
        SetData "events", "timecodestop", "eventID", l_lngEventID, Translate_NDF_To_DF(l_strDuration)
        SetData "events", "fd_length", "eventID", l_lngEventID, Translate_NDF_To_DF(l_strDuration)
    Else
        SetData "events", "timecodestart", "eventID", l_lngEventID, "00:00:00:00"
        SetData "events", "timecodestop", "eventID", l_lngEventID, l_strDuration
        SetData "events", "fd_length", "eventID", l_lngEventID, l_strDuration
    End If
    SetData "events", "clipaudiocodec", "eventID", l_lngEventID, l_strAudioCodec & " " & l_strAudioBitDepth & " " & l_strSampleRate
    SetData "events", "trackcount", "eventID", l_lngEventID, Val(l_strTrackCount)
    SetData "events", "clipfilename", "eventID", l_lngEventID, l_strFilename
    If InStr(l_strFilename, ".") > 0 Then
        SetData "events", "clipreference", "eventID", l_lngEventID, Left(l_strFilename, InStr(l_strFilename, ".") - 1)
    Else
        SetData "events", "clipreference", "eventID", l_lngEventID, l_strFilename
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new SMV items were added to the Shine Digital File  Library, and will require Series IDs adding: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 769) & "' AND trackermessageID = 22;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Shine Library Updates", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdShineSMVLibrary_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strVersion As String
Dim l_strFormat As String, l_strStandard As String, l_strAspectRatio As String, l_strShelf As String
Dim l_strDurationEstimate As String, l_strCh1 As String, l_strCh2 As String, l_strCh3 As String, l_strCh4 As String, l_strCh5 As String, l_strCh6 As String
Dim l_strCh7 As String, l_strCh8 As String, l_strCh9 As String, l_strCh10 As String, l_strCh11 As String, l_strCh12 As String

Dim l_strSQL As String, l_lngLibraryID As Long, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "SMV Excel Read", "smv_reports")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "SMV Excel Read", 3))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 1))
    lblProgress.Caption = l_lngXLRowCounter & ": " & l_strBarcode
    If GetData("library", "libraryID", "barcode", Trim(l_strBarcode)) <> 0 Then
        'Tape exists already in library
        lblProgress.Caption = lblProgress.Caption & " - UPDATE"
        l_lngLibraryID = GetData("library", "libraryID", "barcode", Trim(l_strBarcode))
        If GetData("library", "companyID", "LibraryID", l_lngLibraryID) = 769 Then
            'No longer updating title in case we have already set it for the SeriesIDs
            'SetData "library", "title", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 2)), 255)
        Else
            l_strBarcode = l_strBarcode & "SMV"
            lblProgress.Caption = l_lngXLRowCounter & ": " & l_strBarcode
            If GetData("library", "libraryID", "barcode", Trim(l_strBarcode)) <> 0 Then
                lblProgress.Caption = lblProgress.Caption & " - UPDATE"
                If GetData("library", "companyID", "LibraryID", l_lngLibraryID) = 769 Then
                    'No longer updating title in case we have already set it for the SeriesIDs
                    'SetData "library", "title", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 2)), 255)
                End If
            Else
                'New tape - needs entries making
                l_lngLibraryID = CreateLibraryItem(l_strBarcode, Left(Trim(GetXLData(l_lngXLRowCounter, 2)), 255), 0, GetNextSequence("internalreference"), 769)
                lblProgress.Caption = lblProgress.Caption & " - NEW TAPE"
                l_strEmailBody = l_strEmailBody & l_strBarcode & " - " & Trim(GetXLData(l_lngXLRowCounter, 2)) & vbCrLf
            End If
        End If
    Else
        'New tape - needs entries making
        l_lngLibraryID = CreateLibraryItem(l_strBarcode, Left(Trim(GetXLData(l_lngXLRowCounter, 2)), 255), 0, GetNextSequence("internalreference"), 769)
        lblProgress.Caption = lblProgress.Caption & " - NEW TAPE"
        l_strEmailBody = l_strEmailBody & l_strBarcode & " - " & Trim(GetXLData(l_lngXLRowCounter, 2)) & vbCrLf
    End If
    
    If GetData("library", "companyID", "LibraryID", l_lngLibraryID) = 769 Then
        'SetData "library", "version", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 4)), 50)
        SetData "library", "episode", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 3)), 50)
        SetData "library", "subtitle", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 4)), 50)
        SetData "library", "format", "libraryID", l_lngLibraryID, Left(GetAlias(GetXLData(l_lngXLRowCounter, 5), "Format"), 50)
        SetData "library", "videostandard", "libraryID", l_lngLibraryID, Left(GetAlias(GetXLData(l_lngXLRowCounter, 6), "Video Standard"), 50)
        'SetData "library", "totalduration", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 6))
        'SetData "library", "totalduration", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 10))
        'SetData "library", "shelf", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 14))
        SetData "library", "Ch1", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 11)), 50)
        SetData "library", "Ch2", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 12)), 50)
        SetData "library", "Ch3", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 13)), 50)
        SetData "library", "Ch4", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 14)), 50)
        'SetData "library", "Ch5", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 12))
        'SetData "library", "Ch6", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 13))
        'SetData "library", "Ch7", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 14))
        'SetData "library", "Ch8", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 15))
        'SetData "library", "Ch9", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 16))
        'SetData "library", "Ch10", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 17))
        'SetData "library", "Ch11", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 18))
        'SetData "library", "Ch12", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 19))
        SetData "library", "location", "libraryID", l_lngLibraryID, "LIBRARY"
        SetData "library", "shelf", "libraryID", l_lngLibraryID, "SMV"
        SetData "library", "companyname", "libraryID", l_lngLibraryID, "Shine International"
        SetData "library", "copytype", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 9)), 50)
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following new SMV items were added to the Shine Tape Library, and will require Series IDs adding: " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 769) & "' AND trackermessageID = 22;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Shine Library Updates", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdShineSMVRemoval_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strVersion As String

Dim l_strSQL As String, l_lngLibraryID As Long, l_strEmailBody As String, l_rstWhoToEmail As ADODB.Recordset

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "SMV Excel Read", "SHINE MASTERS AT BONDED")
If l_strExcelSheetName = "" Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "SMV Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

l_strEmailBody = ""

While GetXLData(l_lngXLRowCounter, 1) <> ""
    l_strBarcode = Trim(GetXLData(l_lngXLRowCounter, 1))
    lblProgress.Caption = l_lngXLRowCounter & ": " & l_strBarcode
    If GetData("library", "libraryID", "barcode", Trim(l_strBarcode)) <> 0 Then
        'Tape exists already in library
        lblProgress.Caption = lblProgress.Caption & " - UPDATE"
        l_lngLibraryID = GetData("library", "libraryID", "barcode", Trim(l_strBarcode))
        If GetData("library", "companyID", "LibraryID", l_lngLibraryID) = 769 Then
            SetData "library", "title", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 2))
        End If
        l_strEmailBody = l_strEmailBody & l_strBarcode & " - " & Trim(GetXLData(l_lngXLRowCounter, 2)) & " - Removed to Bonded Storage" & vbCrLf
    Else
        'New tape - needs entries making
        l_lngLibraryID = CreateLibraryItem(l_strBarcode, Trim(GetXLData(l_lngXLRowCounter, 2)), 0, GetNextSequence("internalreference"), 769)
        lblProgress.Caption = lblProgress.Caption & " - NEW TAPE"
        l_strEmailBody = l_strEmailBody & l_strBarcode & " - " & Trim(GetXLData(l_lngXLRowCounter, 2)) & " - Removed to Bonded Storage" & vbCrLf
    End If
    
    If GetData("library", "companyID", "LibraryID", l_lngLibraryID) = 769 Then
        SetData "library", "version", "libraryID", l_lngLibraryID, Left(Trim(GetXLData(l_lngXLRowCounter, 4)), 50)
        SetData "library", "episode", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 3))
        SetData "library", "format", "libraryID", l_lngLibraryID, GetAlias(GetXLData(l_lngXLRowCounter, 5), "Format")
        SetData "library", "videostandard", "libraryID", l_lngLibraryID, GetAlias(GetXLData(l_lngXLRowCounter, 6), "Video Standard")
        SetData "library", "location", "libraryID", l_lngLibraryID, "BONDED"
        SetData "library", "companyname", "libraryID", l_lngLibraryID, "Shine International"
        SetData "library", "copytype", "libraryID", l_lngLibraryID, Trim(GetXLData(l_lngXLRowCounter, 7))
        l_strSQL = "INSERT INTO trans (libraryID, destination, cuser, cdate) VALUES (" & l_lngLibraryID & ", 'BONDED', 'XLS', getdate());"
        ExecuteSQL l_strSQL, g_strExecuteError
        'CheckForSQLError
    End If
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

If l_strEmailBody <> "" Then
    
    l_strEmailBody = "The following SMV tapes were moved from location 'SMV' to location 'BONDED': " & vbCrLf & vbCrLf & l_strEmailBody
    
    Set l_rstWhoToEmail = ExecuteSQL("SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", 769) & "' AND trackermessageID = 22;", g_strExecuteError)
    CheckForSQLError
    
    If l_rstWhoToEmail.RecordCount > 0 Then
        l_rstWhoToEmail.MoveFirst
        While Not l_rstWhoToEmail.EOF
            SendSMTPMail l_rstWhoToEmail("email"), l_rstWhoToEmail("fullname"), "Shine Library Updates", "", l_strEmailBody, True, "", ""
    
            l_rstWhoToEmail.MoveNext
        Wend
    End If
    l_rstWhoToEmail.Close
    Set l_rstWhoToEmail = Nothing
End If

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub cmdSpecial_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub
    
End If

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Wellcome Excel Read", "sheet1")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = Val(InputBox("Please give the row number where the data starts", "DADC Excel Read", 2))
Debug.Print l_lngXLRowCounter
If l_lngXLRowCounter = 0 Then
    oWorkbook.Close
    Set oWorkbook = Nothing
    Set oExcel = Nothing
    Exit Sub
End If

Dim l_strTapeBarcode As String, l_strTapeTitle As String, l_strTapeSubtitle As String, l_strTapeVersion As String, l_strTapeFormat As String, l_lngLibraryID As Long, l_strItemName As String, l_strTapeSeason As String

l_strTapeBarcode = GetXLData(l_lngXLRowCounter, 1)
l_strTapeTitle = GetXLData(l_lngXLRowCounter, 3)
l_strTapeSubtitle = GetXLData(l_lngXLRowCounter, 5)
l_strTapeVersion = GetXLData(l_lngXLRowCounter, 7)
l_strTapeFormat = GetAlias(GetXLData(l_lngXLRowCounter, 12))
l_strTapeSeason = GetXLData(l_lngXLRowCounter, 9)

While GetXLData(l_lngXLRowCounter, 4) <> ""
    
    If GetXLData(l_lngXLRowCounter, 1) <> "" Then l_strTapeBarcode = GetXLData(l_lngXLRowCounter, 1)
    If GetXLData(l_lngXLRowCounter, 3) <> "" Then l_strTapeTitle = GetXLData(l_lngXLRowCounter, 3)
    If GetXLData(l_lngXLRowCounter, 5) <> "" Then l_strTapeSubtitle = GetXLData(l_lngXLRowCounter, 5)
    If GetXLData(l_lngXLRowCounter, 7) <> "" Then l_strTapeVersion = GetXLData(l_lngXLRowCounter, 7)
    If GetXLData(l_lngXLRowCounter, 12) <> "" Then l_strTapeFormat = GetAlias(GetXLData(l_lngXLRowCounter, 12))
    If GetXLData(l_lngXLRowCounter, 6) <> "" Then l_strTapeSeason = GetXLData(l_lngXLRowCounter, 9)
    l_strItemName = GetXLData(l_lngXLRowCounter, 4)

    lblProgress.Caption = l_lngXLRowCounter & " - " & l_strItemName
    DoEvents
    
    l_lngLibraryID = GetData("library", "libraryID", "barcode", l_strTapeBarcode)
    If l_lngLibraryID = 0 Then
        l_lngLibraryID = CreateLibraryItem(l_strTapeBarcode, l_strTapeTitle, 0, 0, 1182)
        SetData "library", "subtitle", "libraryID", l_lngLibraryID, l_strTapeSubtitle
        SetData "library", "version", "libraryID", l_lngLibraryID, l_strTapeVersion
        SetData "library", "format", "libraryID", l_lngLibraryID, l_strTapeFormat
        SetData "library", "location", "libraryID", l_lngLibraryID, "OFF SITE"
    End If
    
    AddEventToTape l_lngLibraryID, l_strItemName, "", l_strTapeSeason, 0, "", 0, "", Now

    l_lngXLRowCounter = l_lngXLRowCounter + 1
    
Wend

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub cmdTradBBCWW_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If


'emtpy the comma count field
m_lngCommacount = 0

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset, l_rsxJobs As ADODB.Recordset
Dim l_rsxDetails As ADODB.Recordset, l_lngLibraryID As Long

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsxJobs = New ADODB.Recordset
Set l_rsxDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM [job] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM [jobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxJobs = ExecuteSQL("SELECT * FROM [extendedjob] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxDetails = ExecuteSQL("SELECT * FROM [extendedjobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

'get the fields and place them into variables
Dim l_BookedBy$, l_AccountNumber$, l_Telephone$, l_FACNumber$, l_jcacontact$, l_ClientName$, l_ContactName$, l_BookingNumber$
Dim l_Lastprogrammetitle$

'get the header details, i.e. Client name, etc.
l_AccountNumber$ = GetXLData(g_cntAccountNumberRow%, g_cntAccountNumberColumn%)
l_Telephone$ = GetXLData(g_cntTelephoneRow%, g_cntTelephoneColumn%)
l_ClientName$ = "BBC Worldwide"
l_ContactName$ = GetXLData(5, 6)
l_FACNumber$ = GetXLData(3, 2)
l_FACNumber$ = Mid(l_FACNumber$, 16, Len(l_FACNumber$))
l_jcacontact$ = GetXLData(7, 2)


'declare a variable for keeping track of how far down the page we are
Dim l_CurrentTop&

'set it to the first load of data
l_CurrentTop& = 10

'declare lots of variables for taking the values off the spreadsheet
Dim l_OrderNumber$, l_CustomerName$, l_ChargeCode$, l_BusinessAreaCode$, l_NominalCode$, l_ProgrammeTitle$, l_Requireddate$, l_Notes$
Dim l_MasterEpisodeNumber$, l_MasterSpoolNumber$, l_MasterLanguage$, l_MasterSpoolDuration$, l_MasterAspectRatio$, l_MasterTapeFormat$, l_MasterLineStandard$
Dim l_DupeEpisodeNumber$, l_DupeAudio$, l_DupeCeefax$, l_DupeTotalDuration$, l_DupeLanguage$, l_DupeQuantity$, l_DupeTapeFormat$, l_DupeLineStandard$, l_DupeAspectRatio$
Dim l_LastorderNumber$, l_PreviousJobNumber&, l_Original_MasterStandard$, l_Original_DupeStandard$, l_Saveflag%, l_Stk&, l_CalculatedDuration&, l_Mastercount&
Dim l_Orig_masterspool$, l_Orig_masterformat$, l_Orig_masterstandard$, l_Orig_masteraspect$, l_Orig_masterduration$, l_comma&, l_IndividualFAC$, l_ProgrammeNumber$
Dim l_CountryCode$, l_BusinessArea$, l_OldMasterSpoolNumber$, l_NewJobNumber&, l_BBCMasterTapeFormat$, l_BBCDupeTapeFormat$
Dim l_EpisodeCount As Long, l_CopyCount As Long, l_OrigCopyCount As Long

l_LastorderNumber$ = "CETA"
l_Saveflag% = False


'declare the Sound channels
Dim l_Sound1$, l_Sound2$, l_Sound3$, l_Sound4$, l_Sound5$, l_Sound6$, l_Sound7$, l_Sound8$

'start a loop that cycles through each tenth line
l_EpisodeCount = 0
l_CopyCount = 1
Do
    
    'if there is no data in the Bookingnumber field then this is either one of those blue lines or the end of the order.
    'Check which and then exit the do loop if necessary. While there, count the number of episodes in the current group,
    'and enter the data for the last one, rather than any others.
    
    If GetXLData(l_CurrentTop&, 2) = "" Then
        l_CurrentTop& = l_CurrentTop& + 2
        l_EpisodeCount = 0
        l_CopyCount = 1
        l_MasterSpoolNumber$ = ""
        l_OldMasterSpoolNumber$ = "GINGER"
        If GetXLData(l_CurrentTop&, 2) = "" Then Exit Do
    End If
    
    While GetXLData(l_CurrentTop&, 2) <> ""
        l_EpisodeCount = l_EpisodeCount + 1
        l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
        If l_MasterSpoolNumber$ = l_OldMasterSpoolNumber$ Then
            'We've just found another different copy from the same master:
            l_EpisodeCount = l_EpisodeCount - 1
            l_CopyCount = l_CopyCount + 1
        Else
            'Set the old Spool number for checking the next set isn't the same master spool
            l_OldMasterSpoolNumber$ = l_MasterSpoolNumber$
        End If
        'increment the looper so we move to the next section - when we fall out of this loop we should be sitting on the last Ep in the group
        l_CurrentTop& = l_CurrentTop& + 11
    Wend
    
    l_CurrentTop& = l_CurrentTop& - 11
    
    l_Mastercount& = 1
    l_BookingNumber$ = GetXLData(l_CurrentTop&, 2)
    
    If l_BookingNumber$ = "" Then Exit Do
    
    l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
    
    'here is where we work out if there is two masters on this sheet, so check for a comma
    If InStr(l_MasterSpoolNumber$, ",") <> 0 Then
    
        'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
        
        l_Mastercount& = Countcommas(l_MasterSpoolNumber$) + 1
        
    End If
        
    l_Orig_masterspool$ = l_MasterSpoolNumber$
    l_Orig_masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
    l_Orig_masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
    l_Orig_masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
    l_Orig_masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)

    'go through the spreadsheet and retrieve the values and put them into
    'variables to be used later. These are from the Order Details section
    l_OrderNumber$ = GetXLData(l_CurrentTop& + 1, 2)
    l_IndividualFAC$ = GetXLData(l_CurrentTop& + 0, 2)
    l_CustomerName$ = GetXLData(l_CurrentTop& + 2, 2)
    l_ChargeCode$ = GetXLData(l_CurrentTop& + 3, 2)
    l_BusinessArea$ = GetXLData(l_CurrentTop& + 4, 2)
    l_BusinessAreaCode$ = GetXLData(l_CurrentTop& + 5, 2)
    l_NominalCode$ = GetXLData(l_CurrentTop& + 6, 2)
    l_ProgrammeTitle$ = GetXLData(l_CurrentTop& + 7, 2)
    l_ProgrammeNumber$ = GetXLData(l_CurrentTop& + 8, 2)
    l_CountryCode$ = GetXLData(l_CurrentTop& + 8, 4)
    l_Requireddate$ = GetXLData(l_CurrentTop& + 9, 2)
    l_Notes$ = GetXLData(l_CurrentTop& + 10, 2)
    
    'get the next chapter, which is Master Material
    l_MasterEpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    
    If l_Mastercount& = 1 Then
        l_MasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_MasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
        l_MasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
        l_MasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
    End If
    
    l_MasterLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 7, 4))
    
    'increment the looper so we move to the next section  or empty line.
    l_CurrentTop& = l_CurrentTop& + 11
        
    If UCase(l_OrderNumber$) <> UCase(l_LastorderNumber$) Then
                    
        If l_LastorderNumber$ <> "STAR ORDER" Then
        
            'add a new record to the Jobs table
            l_rsJobs.AddNew
            l_rsJobs("createduserID") = 33
            l_rsJobs("createduser") = "Excel"
            l_rsJobs("createddate") = Now
            l_rsJobs("modifieddate") = Now
            l_rsJobs("modifieduserID") = 33
            l_rsJobs("modifieduser") = l_jcacontact$
            l_rsJobs("flagemail") = 1
            l_rsJobs("fd_status") = "Confirmed"
            l_rsJobs("jobtype") = "Dubbing"
            l_rsJobs("joballocation") = "Regular"
            l_rsJobs("deadlinedate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("despatchdate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("projectID") = 0
            l_rsJobs("productID") = 0
            If l_Telephone$ <> "" Then l_rsJobs("telephone") = Left$(l_Telephone$, 50)
'            Dim l_NewJobNumber&
'            l_NewJobNumber& = GetNextJobNumber()
'            l_PreviousJobNumber& = l_NewJobNumber&
'
'            l_rsJobs("job number") = l_NewJobNumber&
            l_rsJobs("companyID") = 570
            l_rsJobs("companyname") = "BBC Worldwide Int Ops"
            
            'parse the contactname to get the CETA contact ID and name.
            If l_ContactName$ <> "" Then
                l_rsJobs("contactname") = l_ContactName$
                l_rsJobs("contactID") = GetContactID(l_ContactName$)
            End If
            
            If l_ProgrammeTitle$ <> "" Then l_rsJobs("title1") = Left$(l_ProgrammeTitle$, 50)
            l_Lastprogrammetitle$ = UCase(l_ProgrammeTitle$)
            If l_MasterEpisodeNumber$ <> "" Then l_rsJobs("title2") = Left$(l_MasterEpisodeNumber$, 50)
            If l_Notes$ <> "" Then l_rsJobs("notes2") = "PLEASE CHECK ORIGINAL WW ORDER SHEETS FOR JOB DETAILS. " & l_Notes$
            
            'check whether this is a STAR order or a normal one.
            
            If l_OrderNumber$ Like "STAR*" Then
                l_rsJobs("orderreference") = "STAR ORDER"
                l_LastorderNumber$ = "STAR ORDER"
                l_rsJobs("title1") = "Various Titles - Star Order"
            Else
                If l_OrderNumber$ <> "" Then l_rsJobs("orderreference") = UCase(l_OrderNumber$)
                l_LastorderNumber$ = UCase(l_OrderNumber$)
            End If
                
            l_rsJobs.Update
            
            
            'this part here!
            l_rsJobs.Bookmark = l_rsJobs.Bookmark
            l_NewJobNumber& = l_rsJobs("JobID")
            l_PreviousJobNumber& = l_NewJobNumber&
            
            'Set the projectnumber to be the job ID - new CETA feature.
            l_rsJobs("projectnumber") = l_rsJobs("JobID")
            l_rsJobs.Update
                        
            DBEngine.Idle
            DoEvents
            
            If l_NewJobNumber& <> 0 Then
                
                l_rsxJobs.AddNew
                l_rsxJobs("JobID") = l_NewJobNumber&
                l_rsxJobs("BBCStarJob") = 1
                l_rsxJobs("BBCBookingnumber") = l_FACNumber$
                If l_CustomerName$ <> "" Then l_rsxJobs("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxJobs("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxJobs("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxJobs("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxJobs("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxJobs("BBCCountryCode") = l_CountryCode$
                l_rsxJobs.Update
                
            End If
            
            DBEngine.Idle
            DoEvents
            
        End If
        
    Else
        'make sure the title program stays the same all the way
        If UCase(l_ProgrammeTitle$) <> UCase(l_Lastprogrammetitle$) Then
            'need to update the other job to change the title
            l_rsJobs.Close
            Set l_rsJobs = ExecuteSQL("Select [title1] from [Job] where [jobID] = " & l_PreviousJobNumber&, g_strExecuteError)
            CheckForSQLError
                        
            If l_rsJobs.RecordCount <> 0 Then
                l_rsJobs("title1") = "Various Titles - See Below"
                l_rsJobs.Update
                l_rsJobs.Close
                DBEngine.Idle
                DoEvents
                Set l_rsJobs = ExecuteSQL("SELECT * FROM [Job] WHERE [JobID] = -1", g_strExecuteError)
                CheckForSQLError
            End If
        End If
    End If

    DBEngine.Idle: DoEvents
    
    'The master detail line(s) get added next
    While l_Mastercount& > 0
        
        'Get next master into normal variables if there is more than 1, trim the original variables and decrease the mastercount.
        
        If l_Mastercount > 1 Then
            l_comma& = InStr(l_Orig_masterspool$, ",")
            l_MasterSpoolNumber$ = Left(l_Orig_masterspool$, l_comma& - 1)
            l_Orig_masterspool$ = Mid(l_Orig_masterspool$, l_comma& + 1, Len(l_Orig_masterspool$))
            l_comma& = InStr(l_Orig_masterformat$, ",")
            l_BBCMasterTapeFormat$ = Left(l_Orig_masterformat$, l_comma& - 1)
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_Orig_masterformat$ = Mid(l_Orig_masterformat$, l_comma + 1, Len(l_Orig_masterformat$))
            l_comma& = InStr(l_Orig_masterstandard$, ",")
            l_MasterLineStandard$ = GetAlias(Left(l_Orig_masterstandard$, l_comma - 1))
            l_Orig_masterstandard$ = Mid(l_Orig_masterstandard$, l_comma + 1, Len(l_Orig_masterstandard$))
            l_comma& = InStr(l_Orig_masteraspect$, ",")
            If l_comma > 0 Then
                l_MasterAspectRatio$ = GetAlias(Left(l_Orig_masteraspect$, l_comma - 1))
                l_Orig_masteraspect$ = Mid(l_Orig_masteraspect$, l_comma + 1, Len(l_Orig_masteraspect$))
            Else
                l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            End If
            l_comma& = InStr(l_Orig_masterduration$, ",")
            l_MasterSpoolDuration$ = Left(l_Orig_masterduration$, l_comma - 1)
            l_Orig_masterduration$ = Mid(l_Orig_masterduration$, l_comma + 1, Len(l_Orig_masterduration$))
        End If
        l_Mastercount& = l_Mastercount& - 1
        
        'process that master
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_MasterSpoolNumber$ <> "" Then l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & l_MasterSpoolNumber$ & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rsLib.EOF Then
            If l_rsLib("format") <> "" Then
                l_rsDetails("format") = Left(l_rsLib("format"), 50)
                If l_rsDetails("format") Like "HI8DAT*" Then l_rsDetails("format") = "HI8DAT"
                If l_rsDetails("format") Like "DAT*" Then l_rsDetails("format") = "DAT"
            ElseIf l_MasterTapeFormat$ <> "" Then
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("videostandard") = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("videostandard") <> "" Then l_MasterLineStandard$ = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("AspectRatio") <> "" Then
                l_rsDetails("aspectratio") = Left((l_rsLib("AspectRatio") & l_rsLib("GeometricLinearity")), 50)
                l_MasterAspectRatio$ = l_rsDetails("aspectratio")
            Else
                If l_MasterAspectRatio$ <> "" Then
                    l_rsDetails("aspectratio") = l_MasterAspectRatio$
                Else
                    If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                        l_rsDetails("aspectratio") = "16:9AN"
                        l_MasterAspectRatio$ = "16:9AN"
                    End If
                    If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                        l_rsDetails("aspectratio") = "16:9LE"
                        l_MasterAspectRatio$ = "16:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*15:9*" Then
                        l_rsDetails("aspectratio") = "15:9LE"
                        l_MasterAspectRatio$ = "15:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*14:9*" Then
                        l_rsDetails("aspectratio") = "14:9LE"
                        l_MasterAspectRatio$ = "14:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*4:3*" Then
                        l_rsDetails("aspectratio") = "4:3NOR"
                        l_MasterAspectRatio$ = "4:3NOR"
                    End If
                End If
            End If
            If l_rsLib("Totalduration") <> "" Then
                l_CalculatedDuration& = 60 * Val(Left(l_rsLib("Totalduration"), 2)) + Val(Mid(l_rsLib("totalduration"), 4, 2))
                If Val(Mid(l_rsLib("totalduration"), 7, 2)) > 0 Then l_CalculatedDuration& = l_CalculatedDuration& + 1
            End If
            If l_CalculatedDuration& > 0 Then
                l_rsDetails("runningtime") = l_CalculatedDuration&
                l_MasterSpoolDuration$ = Str(l_CalculatedDuration&)
            Else
                l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            End If
            If l_rsLib("ch1") <> "" Then
                l_rsDetails("sound1") = Process_Sound(l_rsLib("ch1"))
            Else
                l_rsDetails("sound1") = Left(l_rsLib("ch1"), 50)
            End If
            If l_rsLib("ch2") <> "" Then
                l_rsDetails("sound2") = Process_Sound(l_rsLib("ch2"))
            Else
                l_rsDetails("sound2") = Left(l_rsLib("ch2"), 50)
            End If
            If l_rsLib("ch3") <> "" Then
                l_rsDetails("sound3") = Process_Sound(l_rsLib("ch3"))
            Else
                l_rsDetails("sound3") = Left(l_rsLib("ch3"), 50)
            End If
            If l_rsLib("ch4") <> "" Then
                l_rsDetails("sound4") = Process_Sound(l_rsLib("ch4"))
            Else
                l_rsDetails("sound4") = Left(l_rsLib("ch4"), 50)
            End If
        Else
            'As well as loading up the job detail line from the provided data, we create a new tape entry in the library.
            'Well - we used to - I'm commenting all that code out, because we're not sure its helpful to do that any more.
'            l_rsLib.AddNew
'            l_rsLib("barcode") = l_MasterSpoolNumber$
'            l_rsLib("title") = l_ProgrammeTitle$
'            l_rsLib("subtitle") = l_DupeEpisodeNumber$
'            l_rsLib("companyID") = 287
'            l_rsLib("companyname") = "BBC Worldwide Ltd"
'            l_rsLib("location") = "OFF SITE"
            If l_MasterTapeFormat$ <> "" Then
'                l_rsLib("format") = l_MasterTapeFormat$
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
'            l_rsLib("copytype") = "MASTER"
'            l_rsLib("version") = "MASTER"
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            If l_MasterLineStandard$ = "ASK" Then
                l_MasterLineStandard$ = "625PAL"
                l_rsDetails("videoStandard") = "625PAL"
'                l_rsLib("videostandard") = "625PAL"
            Else
                If l_MasterLineStandard$ <> "" Then
                    l_rsDetails("videostandard") = Left(l_MasterLineStandard$, 10)
'                    l_rsLib("videostandard") = Left(l_MasterLineStandard$, 10)
                End If
            End If
            If l_MasterAspectRatio$ <> "" Then
                l_rsDetails("aspectratio") = l_MasterAspectRatio$
            Else
                If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_MasterAspectRatio$ = "16:9AN"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "ANAMORPHIC"
                End If
                If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_MasterAspectRatio$ = "16:9LE"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_MasterAspectRatio$ = "15:9LE"
'                    l_rsLib("aspectratio") = "15:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_MasterAspectRatio$ = "14:9LE"
'                    l_rsLib("aspectratio") = "14:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_MasterAspectRatio$ = "4:3NOR"
'                    l_rsLib("aspectratio") = "4:3"
'                    l_rsLib("geometriclinearity") = "NORMAL"
                End If
            End If
            If l_MasterAspectRatio$ = "ASK" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_MasterAspectRatio$ = "4:3NOR"
'                l_rsLib("aspectratio") = "4:3"
'                l_rsLib("geometriclinearity") = "NORMAL"
            End If
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
'            l_rsLib("cuser") = "XLS"
'            l_rsLib("cdate") = Now
'            l_rsLib("muser") = "XLS"
'            l_rsLib("mdate") = Now
'            l_rsLib("code128barcode") = CIA_CODE128(l_MasterSpoolNumber$)
'            l_rsLib.Update
            'Then we make a history entry too.
            'No - we don't any more.
'            l_rsLib.Bookmark = l_rsLib.Bookmark
'            l_lngLibraryID = l_rsLib("libraryID")
'            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", l_conn, adOpenKeyset, adLockOptimistic
'            l_rsHistory.AddNew
'            l_rsHistory("libraryID") = l_lngLibraryID
'            l_rsHistory("destination") = "Created"
'            l_rsHistory("cuser") = "XLS"
'            l_rsHistory("cdate") = Now
'            l_rsHistory.Update
'            l_rsHistory.Close
        End If
        l_rsLib.Close
        l_BBCMasterTapeFormat$ = l_MasterTapeFormat$
        If l_rsDetails("format") = "" Then l_rsDetails("format") = "ASK"
        
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update

        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcsourceformat") = l_BBCMasterTapeFormat$
        If l_DupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_DupeTapeFormat$
        l_rsxDetails.Update
                
        'update the variables if the mastercount is currentrly 1
        If l_Mastercount& = 1 Then
            l_BBCMasterTapeFormat$ = l_Orig_masterformat$
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_MasterSpoolNumber$ = l_Orig_masterspool$
            l_MasterLineStandard$ = GetAlias(l_Orig_masterstandard$)
            l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            l_MasterSpoolDuration$ = l_Orig_masterduration$
        End If
    Wend
    
    'Then the copy detail line gets added
    l_OrigCopyCount = l_CopyCount
    While l_CopyCount > 0
        
        l_CurrentTop& = l_CurrentTop& - 11
        l_CopyCount = l_CopyCount - 1
        
        'Get the Duplication details for this copy
        
        l_DupeEpisodeNumber$ = GetXLData(l_CurrentTop&, 6)
        l_DupeTotalDuration$ = GetXLData(l_CurrentTop& + 1, 6)
        l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
        l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)
        l_DupeTapeFormat$ = GetAlias(l_BBCDupeTapeFormat$)
        l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
        l_Original_DupeStandard$ = GetXLData(l_CurrentTop& + 4, 6)
        l_DupeAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 5, 6))
        l_DupeLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 6, 6))
        l_DupeCeefax$ = GetAlias(GetXLData(l_CurrentTop& + 7, 6))
        l_DupeAudio$ = GetXLData(l_CurrentTop& + 8, 6)
        
        'we need to get the different sections of the sounds out of the sound notes
        If l_DupeAudio$ <> "" Then
        
            'append another comma seperated value onto the end of the audio
            'channels to ensure we get the last value
            l_DupeAudio$ = l_DupeAudio$ & ",END"
                
            'get each Sound channel using the GetTextPart function from
            'modRoutines
            l_Sound1$ = GetAlias(GetTextPart(l_DupeAudio$, 1, ","))
            l_Sound2$ = GetAlias(GetTextPart(l_DupeAudio$, 2, ","))
            l_Sound3$ = GetAlias(GetTextPart(l_DupeAudio$, 3, ","))
            l_Sound4$ = GetAlias(GetTextPart(l_DupeAudio$, 4, ","))
            l_Sound5$ = GetAlias(GetTextPart(l_DupeAudio$, 5, ","))
            l_Sound6$ = GetAlias(GetTextPart(l_DupeAudio$, 6, ","))
            l_Sound7$ = GetAlias(GetTextPart(l_DupeAudio$, 7, ","))
            l_Sound8$ = GetAlias(GetTextPart(l_DupeAudio$, 8, ","))
        
        End If
        
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        If l_DupeTapeFormat$ <> "" Then l_rsDetails("format") = l_DupeTapeFormat$
        l_rsDetails("copytype") = "C"
        If l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD" Then
            l_rsDetails("quantity") = l_EpisodeCount
        Else
            l_rsDetails("quantity") = Val(l_DupeQuantity$) * l_EpisodeCount
        End If
        If l_DupeLineStandard$ <> "" Then l_rsDetails("videostandard") = Left(l_DupeLineStandard$, 10)
        If Int(Val(l_MasterSpoolDuration$)) > Int(Val(l_DupeTotalDuration$)) Then
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_DupeTotalDuration$ = l_MasterSpoolDuration$
        Else
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
        End If
        If l_DupeAspectRatio$ <> "" And l_DupeAspectRatio$ <> "ASK" Then
            l_rsDetails("aspectratio") = l_DupeAspectRatio$
        Else
            If l_Original_DupeStandard$ Like "*16:9 WI*" Then
                l_rsDetails("aspectratio") = "16:9AN"
                l_DupeAspectRatio$ = "16:9AN"
            End If
            If l_Original_DupeStandard$ Like "*16:9 LE*" Then
                l_rsDetails("aspectratio") = "16:9LE"
                l_DupeAspectRatio$ = "16:9LE"
            End If
            If l_Original_DupeStandard$ Like "*15:9*" Then
                l_rsDetails("aspectratio") = "15:9LE"
                l_DupeAspectRatio$ = "15:9LE"
            End If
            If l_Original_DupeStandard$ Like "*14:9*" Then
                l_rsDetails("aspectratio") = "14:9LE"
                l_DupeAspectRatio$ = "14:9LE"
            End If
            If l_Original_DupeStandard$ Like "*4:3*" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_DupeAspectRatio$ = "4:3NOR"
            End If
        End If
        If l_rsDetails("aspectratio") = "" Then
            l_rsDetails("aspectratio") = "4:3NOR"
            l_DupeAspectRatio$ = "4:3NOR"
        End If
        If l_Sound1$ <> "" Then l_rsDetails("sound1") = Left(l_Sound1$, 6)
        If l_Sound2$ <> "" Then l_rsDetails("sound2") = Left(l_Sound2$, 6)
        If l_Sound3$ <> "" Then l_rsDetails("sound3") = Left(l_Sound3$, 6)
        If l_Sound4$ <> "" Then l_rsDetails("sound4") = Left(l_Sound4$, 6)
        
    
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update
        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCorderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcSourceFormat") = l_BBCMasterTapeFormat$
        If l_BBCDupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_BBCDupeTapeFormat$
        l_rsxDetails.Update
            
        If UCase(l_DupeCeefax$) = "TRUE" Then
            'create a new ceefax line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "CEEFAX Subtitler"
            l_rsDetails("format") = "SUBT"
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
    
        End If
        
        If l_DupeLineStandard$ <> l_MasterLineStandard$ Then
            ' create a standards converter line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Standards Converter"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "CONV"
            Else
                l_rsDetails("format") = "B-CONV"
            End If
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
            
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
        
        End If
        
        If Left(l_MasterAspectRatio$, 6) <> Left(l_DupeAspectRatio$, 6) Then
            If Not (l_MasterAspectRatio$ Like "*4:3*" And l_DupeAspectRatio$ Like "*4:3*") Then
                ' create a ARC line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("description") = "Aspect Ratio Converter"
                l_rsDetails("format") = "ARATIO"
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
            
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
            End If
        End If
    
        If (l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD") And Val(l_DupeQuantity$) > 1 Then
            
            'Do another line to add the extra DVD and VHS copies
            
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Additional Copies"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "WWADVHS"
            Else
                l_rsDetails("format") = "WWADDVD"
            End If
            l_rsDetails("copytype") = "I"
            l_rsDetails("quantity") = (Val(l_DupeQuantity$) - 1) * l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
            
        End If
        DBEngine.Idle
        DoEvents
    Wend
    'Copy Loop has finished - need to reset the currenttop to beyond the end of the last group again
    l_CurrentTop& = l_CurrentTop& + (11 * l_OrigCopyCount)
Loop

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
l_rsxJobs.Close: Set l_rsxJobs = Nothing
l_rsxDetails.Close: Set l_rsxDetails = Nothing
Set l_rsLib = Nothing
Set l_rsHistory = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

'Rename file into the done folder - commented out during testing.
Name l_strExcelFileName As "\\jcaserver\ceta\Processed Star Orders\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdOffAir_Click()

If Not CheckAccess("/processexcelorders") Then
    Exit Sub
End If


'emtpy the comma count field
m_lngCommacount = 0

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_rsJobs As ADODB.Recordset, l_rsDetails As ADODB.Recordset, l_rsLib As ADODB.Recordset, l_rsHistory As ADODB.Recordset, l_rsxJobs As ADODB.Recordset
Dim l_rsxDetails As ADODB.Recordset, l_lngLibraryID As Long

Dim l_rsOffAir As ADODB.Recordset

Dim l_strSQL As String, l_lngdetailID As Long
            
'open the recordsets
Set l_rsJobs = New ADODB.Recordset
Set l_rsDetails = New ADODB.Recordset
Set l_rsxJobs = New ADODB.Recordset
Set l_rsxDetails = New ADODB.Recordset
Set l_rsLib = New ADODB.Recordset
Set l_rsHistory = New ADODB.Recordset

Set l_rsJobs = ExecuteSQL("SELECT * FROM [job] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsDetails = ExecuteSQL("SELECT * FROM [jobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxJobs = ExecuteSQL("SELECT * FROM [extendedjob] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError
Set l_rsxDetails = ExecuteSQL("SELECT * FROM [extendedjobdetail] WHERE [jobID] = -1", g_strExecuteError)
CheckForSQLError

'free up locks
DoEvents

'open the spreadsheet from the root of C:\

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("sheet1")

'get the fields and place them into variables
Dim l_BookedBy$, l_AccountNumber$, l_Telephone$, l_FACNumber$, l_jcacontact$, l_ClientName$, l_ContactName$, l_BookingNumber$
Dim l_Lastprogrammetitle$

'get the header details, i.e. Client name, etc.
l_AccountNumber$ = GetXLData(g_cntAccountNumberRow%, g_cntAccountNumberColumn%)
l_Telephone$ = GetXLData(g_cntTelephoneRow%, g_cntTelephoneColumn%)
l_ClientName$ = "BBC Worldwide"
l_ContactName$ = GetXLData(5, 6)
l_FACNumber$ = GetXLData(3, 2)
l_FACNumber$ = Mid(l_FACNumber$, 16, Len(l_FACNumber$))
l_jcacontact$ = GetXLData(7, 2)


'declare a variable for keeping track of how far down the page we are
Dim l_CurrentTop&

'set it to the first load of data
l_CurrentTop& = 10

'declare lots of variables for taking the values off the spreadsheet
Dim l_OrderNumber$, l_CustomerName$, l_ChargeCode$, l_BusinessAreaCode$, l_NominalCode$, l_ProgrammeTitle$, l_Requireddate$, l_Notes$
Dim l_MasterEpisodeNumber$, l_MasterSpoolNumber$, l_MasterLanguage$, l_MasterSpoolDuration$, l_MasterAspectRatio$, l_MasterTapeFormat$, l_MasterLineStandard$
Dim l_DupeEpisodeNumber$, l_DupeAudio$, l_DupeCeefax$, l_DupeTotalDuration$, l_DupeLanguage$, l_DupeQuantity$, l_DupeTapeFormat$, l_DupeLineStandard$, l_DupeAspectRatio$
Dim l_LastorderNumber$, l_PreviousJobNumber&, l_Original_MasterStandard$, l_Original_DupeStandard$, l_Saveflag%, l_Stk&, l_CalculatedDuration&, l_Mastercount&
Dim l_Orig_masterspool$, l_Orig_masterformat$, l_Orig_masterstandard$, l_Orig_masteraspect$, l_Orig_masterduration$, l_comma&, l_IndividualFAC$, l_ProgrammeNumber$
Dim l_CountryCode$, l_BusinessArea$, l_OldMasterSpoolNumber$, l_NewJobNumber&, l_BBCMasterTapeFormat$, l_BBCDupeTapeFormat$
Dim l_EpisodeCount As Long, l_CopyCount As Long, l_OrigCopyCount As Long

Dim l_strChannel As String, l_datTXDateAndTime As Date, l_strTXDate As String, l_strTXTime As String, l_intCount As Integer, l_strProgRefID As String
Dim l_datPublishedStartTime As Date, l_datPublishedEndTime As Date, l_strMainTitle As String

l_LastorderNumber$ = "CETA"
l_Saveflag% = False


'declare the Sound channels
Dim l_Sound1$, l_Sound2$, l_Sound3$, l_Sound4$, l_Sound5$, l_Sound6$, l_Sound7$, l_Sound8$

'start a loop that cycles through each tenth line
l_EpisodeCount = 0
l_CopyCount = 1
Do
    
    'if there is no data in the Bookingnumber field then this is either one of those blue lines or the end of the order.
    'Check which and then exit the do loop if necessary. While there, count the number of episodes in the current group,
    'and enter the data for the last one, rather than any others.
    
    If GetXLData(l_CurrentTop&, 2) = "" Then
        l_CurrentTop& = l_CurrentTop& + 2
        l_MasterSpoolNumber$ = ""
        l_OldMasterSpoolNumber$ = "GINGER"
        If GetXLData(l_CurrentTop&, 2) = "" Then Exit Do
    End If
    
    l_EpisodeCount = 1
    l_CopyCount = 1
    l_Mastercount& = 1
    l_BookingNumber$ = GetXLData(l_CurrentTop&, 2)
    
    If l_BookingNumber$ = "" Then Exit Do
    
    l_MasterSpoolNumber$ = GetXLData(l_CurrentTop& + 2, 4)
    
    'here is where we work out if there is two masters on this sheet, so check for a comma
    If InStr(l_MasterSpoolNumber$, ",") <> 0 Then
    
        'There's a comma in the master spoolnumbers line. This means we're dealing with multiple masters, and need to pick them apart.
        
        l_Mastercount& = Countcommas(l_MasterSpoolNumber$) + 1
        
    End If
        
    l_Orig_masterspool$ = l_MasterSpoolNumber$
    l_Orig_masterformat$ = GetXLData(l_CurrentTop& + 4, 4)
    l_Orig_masterstandard$ = GetXLData(l_CurrentTop& + 5, 4)
    l_Orig_masterduration$ = GetXLData(l_CurrentTop& + 3, 4)
    l_Orig_masteraspect$ = GetXLData(l_CurrentTop& + 6, 4)

    'go through the spreadsheet and retrieve the values and put them into
    'variables to be used later. These are from the Order Details section
    l_OrderNumber$ = GetXLData(l_CurrentTop& + 1, 2)
    l_IndividualFAC$ = GetXLData(l_CurrentTop& + 0, 2)
    l_CustomerName$ = GetXLData(l_CurrentTop& + 2, 2)
    l_ChargeCode$ = GetXLData(l_CurrentTop& + 3, 2)
    l_BusinessArea$ = GetXLData(l_CurrentTop& + 4, 2)
    l_BusinessAreaCode$ = GetXLData(l_CurrentTop& + 5, 2)
    l_NominalCode$ = GetXLData(l_CurrentTop& + 6, 2)
    l_ProgrammeTitle$ = GetXLData(l_CurrentTop& + 7, 2)
    l_ProgrammeNumber$ = GetXLData(l_CurrentTop& + 8, 2)
    l_CountryCode$ = GetXLData(l_CurrentTop& + 8, 4)
    l_Requireddate$ = GetXLData(l_CurrentTop& + 9, 2)
    l_Notes$ = GetXLData(l_CurrentTop& + 10, 2)
    l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
    l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
    l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)

    
    'get the next chapter, which is Master Material
    l_MasterEpisodeNumber$ = GetXLData(l_CurrentTop& + 1, 4)
    
    If l_Mastercount& = 1 Then
        l_MasterSpoolDuration$ = GetXLData(l_CurrentTop& + 3, 4)
        l_MasterTapeFormat$ = GetAlias(GetXLData(l_CurrentTop& + 4, 4))
        l_MasterLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 5, 4))
        l_MasterAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 6, 4))
    End If
    
    l_MasterLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 7, 4))
    
    'increment the looper so we move to the next section  or empty line.
    l_CurrentTop& = l_CurrentTop& + 11
        
    If UCase(l_OrderNumber$) <> UCase(l_LastorderNumber$) Then
                    
        If l_LastorderNumber$ <> "STAR ORDER" Then
        
            'add a new record to the Jobs table
            l_rsJobs.AddNew
            l_rsJobs("createduserID") = 33
            l_rsJobs("createduser") = "Excel"
            l_rsJobs("createddate") = Now
            l_rsJobs("modifieddate") = Now
            l_rsJobs("modifieduserID") = 33
            l_rsJobs("modifieduser") = l_jcacontact$
            l_rsJobs("flagemail") = 1
            l_rsJobs("fd_status") = "Confirmed"
            l_rsJobs("jobtype") = "Dubbing"
            l_rsJobs("joballocation") = "Regular"
            l_rsJobs("deadlinedate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("despatchdate") = Format(l_Requireddate$, "dd/mm/yyyy")
            l_rsJobs("projectID") = 0
            l_rsJobs("productID") = 0
            If l_Telephone$ <> "" Then l_rsJobs("telephone") = Left$(l_Telephone$, 50)
'            Dim l_NewJobNumber&
'            l_NewJobNumber& = GetNextJobNumber()
'            l_PreviousJobNumber& = l_NewJobNumber&
'
'            l_rsJobs("job number") = l_NewJobNumber&
            l_rsJobs("companyID") = 570
            l_rsJobs("companyname") = "BBC Worldwide Int Ops"
            
            'parse the contactname to get the CETA contact ID and name.
            If l_ContactName$ <> "" Then
                l_rsJobs("contactname") = l_ContactName$
                l_rsJobs("contactID") = GetContactID(l_ContactName$)
            End If
            
            If l_ProgrammeTitle$ <> "" Then l_rsJobs("title1") = Left$(l_ProgrammeTitle$, 50)
            l_Lastprogrammetitle$ = UCase(l_ProgrammeTitle$)
            If l_MasterEpisodeNumber$ <> "" Then l_rsJobs("title2") = Left$(l_MasterEpisodeNumber$, 50)
            If l_Notes$ <> "" Then l_rsJobs("notes2") = "PLEASE CHECK ORIGINAL WW ORDER SHEETS FOR JOB DETAILS. " & l_Notes$
            
            'check whether this is a STAR order or a normal one.
            
            If l_OrderNumber$ Like "STAR*" Then
                l_rsJobs("orderreference") = "STAR ORDER"
                l_LastorderNumber$ = "STAR ORDER"
                l_rsJobs("title1") = "Various Titles - Star Order"
            Else
                If l_OrderNumber$ <> "" Then l_rsJobs("orderreference") = UCase(l_OrderNumber$)
                l_LastorderNumber$ = UCase(l_OrderNumber$)
            End If
                
            l_rsJobs.Update
            
            
            'this part here!
            l_rsJobs.Bookmark = l_rsJobs.Bookmark
            l_NewJobNumber& = l_rsJobs("JobID")
            l_PreviousJobNumber& = l_NewJobNumber&
            
            'Set the projectnumber to be the job ID - new CETA feature.
            l_rsJobs("projectnumber") = l_rsJobs("JobID")
            l_rsJobs.Update
                        
            DBEngine.Idle
            DoEvents
            
            If l_NewJobNumber& <> 0 Then
                
                l_rsxJobs.AddNew
                l_rsxJobs("JobID") = l_NewJobNumber&
                l_rsxJobs("BBCStarJob") = 1
                l_rsxJobs("BBCBookingnumber") = l_FACNumber$
                If l_CustomerName$ <> "" Then l_rsxJobs("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxJobs("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxJobs("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxJobs("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxJobs("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxJobs("BBCCountryCode") = l_CountryCode$
                l_rsxJobs.Update
                
            End If
            
            DBEngine.Idle
            DoEvents
            
        End If
        
    Else
        'make sure the title program stays the same all the way
        If UCase(l_ProgrammeTitle$) <> UCase(l_Lastprogrammetitle$) Then
            'need to update the other job to change the title
            l_rsJobs.Close
            Set l_rsJobs = ExecuteSQL("Select [title1] from [Job] where [jobID] = " & l_PreviousJobNumber&, g_strExecuteError)
            CheckForSQLError
                        
            If l_rsJobs.RecordCount <> 0 Then
                l_rsJobs("title1") = "Various Titles - See Below"
                l_rsJobs.Update
                l_rsJobs.Close
                DBEngine.Idle
                DoEvents
                Set l_rsJobs = ExecuteSQL("SELECT * FROM [Job] WHERE [JobID] = -1", g_strExecuteError)
                CheckForSQLError
            End If
        End If
    End If

    DBEngine.Idle: DoEvents
    
    'The master detail line(s) get added next
    While l_Mastercount& > 0
        
        'Get next master into normal variables if there is more than 1, trim the original variables and decrease the mastercount.
        
        If l_Mastercount > 1 Then
            l_comma& = InStr(l_Orig_masterspool$, ",")
            l_MasterSpoolNumber$ = Left(l_Orig_masterspool$, l_comma& - 1)
            l_Orig_masterspool$ = Mid(l_Orig_masterspool$, l_comma& + 1, Len(l_Orig_masterspool$))
            l_comma& = InStr(l_Orig_masterformat$, ",")
            l_BBCMasterTapeFormat$ = Left(l_Orig_masterformat$, l_comma& - 1)
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_Orig_masterformat$ = Mid(l_Orig_masterformat$, l_comma + 1, Len(l_Orig_masterformat$))
            l_comma& = InStr(l_Orig_masterstandard$, ",")
            l_MasterLineStandard$ = GetAlias(Left(l_Orig_masterstandard$, l_comma - 1))
            l_Orig_masterstandard$ = Mid(l_Orig_masterstandard$, l_comma + 1, Len(l_Orig_masterstandard$))
            l_comma& = InStr(l_Orig_masteraspect$, ",")
            If l_comma > 0 Then
                l_MasterAspectRatio$ = GetAlias(Left(l_Orig_masteraspect$, l_comma - 1))
                l_Orig_masteraspect$ = Mid(l_Orig_masteraspect$, l_comma + 1, Len(l_Orig_masteraspect$))
            Else
                l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            End If
            l_comma& = InStr(l_Orig_masterduration$, ",")
            l_MasterSpoolDuration$ = Left(l_Orig_masterduration$, l_comma - 1)
            l_Orig_masterduration$ = Mid(l_Orig_masterduration$, l_comma + 1, Len(l_Orig_masterduration$))
        End If
        l_Mastercount& = l_Mastercount& - 1
        
        'process that master
        
        'process the OffAir details first
        
        'Make an entry in the old offair schedule table
        
        l_strChannel = ""
        l_strTXDate = ""
        l_strTXTime = ""
        l_datTXDateAndTime = 0
        Set l_rsOffAir = ExecuteSQL("select * from offairschedule where offairscheduleID = -1", g_strExecuteError)
        CheckForSQLError
        l_rsOffAir.AddNew
        l_rsOffAir("starordernumber") = l_OrderNumber$
        For l_intCount = 1 To Len(l_ProgrammeNumber$)
            If Mid(l_ProgrammeNumber$, l_intCount, 3) <> " - " Then
                l_strChannel = l_strChannel & Mid(l_ProgrammeNumber$, l_intCount, 1)
            Else
                Exit For
            End If
        Next
        l_rsOffAir("offairchannel") = l_strChannel
        l_strTXTime = Right(l_ProgrammeNumber$, 5)
        l_strTXDate = l_MasterSpoolNumber$
        l_datTXDateAndTime = Format(l_strTXDate, "dd/mm/yy") & " " & l_strTXTime
        l_rsOffAir("offairdate") = l_datTXDateAndTime
        l_rsOffAir("progtitle") = l_ProgrammeTitle$
        l_rsOffAir("duration") = l_MasterSpoolDuration$
    
        If l_BBCDupeTapeFormat$ Like "DVD*" Then
            If UCase(l_ProgrammeTitle$) Like "NEWSNIGHT*" Then
                l_rsOffAir("dvdjca") = l_DupeQuantity$
            Else
                l_rsOffAir("dvdpal") = l_DupeQuantity$
            End If
        Else
            l_rsOffAir("dvdntsc") = l_DupeQuantity$
        End If
        l_rsOffAir.Update
        l_rsOffAir.Close
        
        'Look up the necessary details in the EPG schedule and make an entry in the epg_recording_request table
        
        If fb_bst(l_datTXDateAndTime) = True Then l_datTXDateAndTime = DateAdd("h", -1, l_datTXDateAndTime)

        Set l_rsOffAir = ExecuteSQL("SELECT progrefID, publishedstarttime, publishedendtime FROM epg_schedule WHERE channelname = '" & l_strChannel & "' AND publishedstarttime = '" & FormatSQLDate(l_datTXDateAndTime) & "';", g_strExecuteError)
        CheckForSQLError
        If l_rsOffAir.RecordCount > 0 Then
            l_strProgRefID = Trim(" " & l_rsOffAir("progrefID"))
            l_datPublishedStartTime = l_rsOffAir("publishedstarttime")
            l_datPublishedEndTime = l_rsOffAir("publishedendtime")
            l_rsOffAir.Close
            l_strMainTitle = GetData("epg_programme", "maintitle", "progrefid", l_strProgRefID)
            Set l_rsOffAir = ExecuteSQL("SELECT * FROM epg_recording_request WHERE epg_recording_requestID = -1;", g_strExecuteError)
            CheckForSQLError
            l_rsOffAir.AddNew
            l_rsOffAir("userID") = 667
            l_rsOffAir("recording_statusID") = 3
            l_rsOffAir("progrefID") = l_strProgRefID
            l_rsOffAir("maintitle") = l_strMainTitle
            l_rsOffAir("publishedstarttime") = l_datPublishedStartTime
            l_rsOffAir("actualstarttime") = l_datPublishedStartTime
            l_rsOffAir("publishedendtime") = l_datPublishedEndTime
            l_rsOffAir("actualendtime") = l_datPublishedEndTime
            l_rsOffAir("channelname") = l_strChannel
            l_rsOffAir("daterequested") = Now
            l_rsOffAir("dateconfirmed") = Now
            l_rsOffAir("businessarea") = 5
            If l_BBCDupeTapeFormat$ Like "DVD*" Then
                l_rsOffAir("dvdpalrequested") = l_DupeQuantity$
            Else
                l_rsOffAir("olcrequested") = l_DupeQuantity$
            End If
            l_rsOffAir("starordernumber") = l_OrderNumber$
            l_rsOffAir.Update
            l_rsOffAir.Close
        Else
            l_rsOffAir.Close
            MsgBox "One request found with no EPG entry: " & l_ProgrammeTitle$ & " " & l_strChannel & " " & l_datTXDateAndTime, vbInformation, "EPG Error"
        End If

        Set l_rsOffAir = Nothing
        
        'Then do the actual job stuff
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        If l_MasterSpoolNumber$ <> "" Then l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        'check if we know about this tape in the library and get details if we do.
        'otherwise use the details they provide on the excel sheet.
        l_strSQL = "SELECT * FROM Library WHERE Barcode = '" & l_MasterSpoolNumber$ & "'"
        Set l_rsLib = ExecuteSQL(l_strSQL, g_strExecuteError)
        If Not l_rsLib.EOF Then
            If l_rsLib("format") <> "" Then
                l_rsDetails("format") = Left(l_rsLib("format"), 50)
                If l_rsDetails("format") Like "HI8DAT*" Then l_rsDetails("format") = "HI8DAT"
                If l_rsDetails("format") Like "DAT*" Then l_rsDetails("format") = "DAT"
            ElseIf l_MasterTapeFormat$ <> "" Then
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("videostandard") = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("videostandard") <> "" Then l_MasterLineStandard$ = Left(l_rsLib("videostandard"), 10)
            If l_rsLib("AspectRatio") <> "" Then
                l_rsDetails("aspectratio") = Left((l_rsLib("AspectRatio") & l_rsLib("GeometricLinearity")), 50)
                l_MasterAspectRatio$ = l_rsDetails("aspectratio")
            Else
                If l_MasterAspectRatio$ <> "" Then
                    l_rsDetails("aspectratio") = l_MasterAspectRatio$
                Else
                    If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                        l_rsDetails("aspectratio") = "16:9AN"
                        l_MasterAspectRatio$ = "16:9AN"
                    End If
                    If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                        l_rsDetails("aspectratio") = "16:9LE"
                        l_MasterAspectRatio$ = "16:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*15:9*" Then
                        l_rsDetails("aspectratio") = "15:9LE"
                        l_MasterAspectRatio$ = "15:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*14:9*" Then
                        l_rsDetails("aspectratio") = "14:9LE"
                        l_MasterAspectRatio$ = "14:9LE"
                    End If
                    If l_Orig_masterstandard$ Like "*4:3*" Then
                        l_rsDetails("aspectratio") = "4:3NOR"
                        l_MasterAspectRatio$ = "4:3NOR"
                    End If
                End If
            End If
            If l_rsLib("Totalduration") <> "" Then
                l_CalculatedDuration& = 60 * Val(Left(l_rsLib("Totalduration"), 2)) + Val(Mid(l_rsLib("totalduration"), 4, 2))
                If Val(Mid(l_rsLib("totalduration"), 7, 2)) > 0 Then l_CalculatedDuration& = l_CalculatedDuration& + 1
            End If
            If l_CalculatedDuration& > 0 Then
                l_rsDetails("runningtime") = l_CalculatedDuration&
                l_MasterSpoolDuration$ = Str(l_CalculatedDuration&)
            Else
                l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            End If
            If l_rsLib("ch1") <> "" Then
                l_rsDetails("sound1") = Process_Sound(l_rsLib("ch1"))
            Else
                l_rsDetails("sound1") = Left(l_rsLib("ch1"), 50)
            End If
            If l_rsLib("ch2") <> "" Then
                l_rsDetails("sound2") = Process_Sound(l_rsLib("ch2"))
            Else
                l_rsDetails("sound2") = Left(l_rsLib("ch2"), 50)
            End If
            If l_rsLib("ch3") <> "" Then
                l_rsDetails("sound3") = Process_Sound(l_rsLib("ch3"))
            Else
                l_rsDetails("sound3") = Left(l_rsLib("ch3"), 50)
            End If
            If l_rsLib("ch4") <> "" Then
                l_rsDetails("sound4") = Process_Sound(l_rsLib("ch4"))
            Else
                l_rsDetails("sound4") = Left(l_rsLib("ch4"), 50)
            End If
        Else
            'As well as loading up the job detail line from the provided data, we create a new tape entry in the library.
            'Well - we used to - I'm commenting all that code out, because we're not sure its helpful to do that any more.
'            l_rsLib.AddNew
'            l_rsLib("barcode") = l_MasterSpoolNumber$
'            l_rsLib("title") = l_ProgrammeTitle$
'            l_rsLib("subtitle") = l_DupeEpisodeNumber$
'            l_rsLib("companyID") = 287
'            l_rsLib("companyname") = "BBC Worldwide Ltd"
'            l_rsLib("location") = "OFF SITE"
            If l_MasterTapeFormat$ <> "" Then
'                l_rsLib("format") = l_MasterTapeFormat$
                l_rsDetails("format") = l_MasterTapeFormat$
            End If
'            l_rsLib("copytype") = "MASTER"
'            l_rsLib("version") = "MASTER"
            l_rsDetails("copytype") = "M"
            l_rsDetails("quantity") = l_EpisodeCount
            If l_MasterLineStandard$ = "ASK" Then
                l_MasterLineStandard$ = "625PAL"
                l_rsDetails("videoStandard") = "625PAL"
'                l_rsLib("videostandard") = "625PAL"
            Else
                If l_MasterLineStandard$ <> "" Then
                    l_rsDetails("videostandard") = Left(l_MasterLineStandard$, 10)
'                    l_rsLib("videostandard") = Left(l_MasterLineStandard$, 10)
                End If
            End If
            If l_MasterAspectRatio$ <> "" Then
                l_rsDetails("aspectratio") = l_MasterAspectRatio$
            Else
                If l_Orig_masterstandard$ Like "*16:9 WI*" Then
                    l_rsDetails("aspectratio") = "16:9AN"
                    l_MasterAspectRatio$ = "16:9AN"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "ANAMORPHIC"
                End If
                If l_Orig_masterstandard$ Like "*16:9 LE*" Then
                    l_rsDetails("aspectratio") = "16:9LE"
                    l_MasterAspectRatio$ = "16:9LE"
'                    l_rsLib("aspectratio") = "16:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*15:9*" Then
                    l_rsDetails("aspectratio") = "15:9LE"
                    l_MasterAspectRatio$ = "15:9LE"
'                    l_rsLib("aspectratio") = "15:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*14:9*" Then
                    l_rsDetails("aspectratio") = "14:9LE"
                    l_MasterAspectRatio$ = "14:9LE"
'                    l_rsLib("aspectratio") = "14:9"
'                    l_rsLib("geometriclinearity") = "LETTERBOX"
                End If
                If l_Orig_masterstandard$ Like "*4:3*" Then
                    l_rsDetails("aspectratio") = "4:3NOR"
                    l_MasterAspectRatio$ = "4:3NOR"
'                    l_rsLib("aspectratio") = "4:3"
'                    l_rsLib("geometriclinearity") = "NORMAL"
                End If
            End If
            If l_MasterAspectRatio$ = "ASK" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_MasterAspectRatio$ = "4:3NOR"
'                l_rsLib("aspectratio") = "4:3"
'                l_rsLib("geometriclinearity") = "NORMAL"
            End If
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
'            l_rsLib("cuser") = "XLS"
'            l_rsLib("cdate") = Now
'            l_rsLib("muser") = "XLS"
'            l_rsLib("mdate") = Now
'            l_rsLib("code128barcode") = CIA_CODE128(l_MasterSpoolNumber$)
'            l_rsLib.Update
            'Then we make a history entry too.
            'No - we don't any more.
'            l_rsLib.Bookmark = l_rsLib.Bookmark
'            l_lngLibraryID = l_rsLib("libraryID")
'            l_rsHistory.Open "SELECT * FROM [trans] WHERE [libraryID] = -1", l_conn, adOpenKeyset, adLockOptimistic
'            l_rsHistory.AddNew
'            l_rsHistory("libraryID") = l_lngLibraryID
'            l_rsHistory("destination") = "Created"
'            l_rsHistory("cuser") = "XLS"
'            l_rsHistory("cdate") = Now
'            l_rsHistory.Update
'            l_rsHistory.Close
        End If
        l_rsLib.Close
        l_BBCMasterTapeFormat$ = l_MasterTapeFormat$
        If l_rsDetails("format") = "" Then l_rsDetails("format") = "ASK"
        
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update

        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcsourceformat") = l_BBCMasterTapeFormat$
        If l_DupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_DupeTapeFormat$
        l_rsxDetails.Update
                
        'update the variables if the mastercount is currentrly 1
        If l_Mastercount& = 1 Then
            l_BBCMasterTapeFormat$ = l_Orig_masterformat$
            l_MasterTapeFormat$ = GetAlias(l_BBCMasterTapeFormat$)
            l_MasterSpoolNumber$ = l_Orig_masterspool$
            l_MasterLineStandard$ = GetAlias(l_Orig_masterstandard$)
            l_MasterAspectRatio$ = GetAlias(l_Orig_masteraspect$)
            l_MasterSpoolDuration$ = l_Orig_masterduration$
        End If
    Wend
    
    'Then the copy detail line gets added
    l_OrigCopyCount = l_CopyCount
    While l_CopyCount > 0
        
        l_CurrentTop& = l_CurrentTop& - 11
        l_CopyCount = l_CopyCount - 1
        
        'Get the Duplication details for this copy
        
        l_DupeEpisodeNumber$ = GetXLData(l_CurrentTop&, 6)
        l_DupeTotalDuration$ = GetXLData(l_CurrentTop& + 1, 6)
        l_DupeQuantity$ = GetXLData(l_CurrentTop& + 2, 6)
        l_BBCDupeTapeFormat$ = GetXLData(l_CurrentTop& + 3, 6)
        l_DupeTapeFormat$ = GetAlias(l_BBCDupeTapeFormat$)
        l_DupeLineStandard$ = GetAlias(GetXLData(l_CurrentTop& + 4, 6))
        l_Original_DupeStandard$ = GetXLData(l_CurrentTop& + 4, 6)
        l_DupeAspectRatio$ = GetAlias(GetXLData(l_CurrentTop& + 5, 6))
        l_DupeLanguage$ = GetAlias(GetXLData(l_CurrentTop& + 6, 6))
        l_DupeCeefax$ = GetAlias(GetXLData(l_CurrentTop& + 7, 6))
        l_DupeAudio$ = GetXLData(l_CurrentTop& + 8, 6)
        
        'we need to get the different sections of the sounds out of the sound notes
        If l_DupeAudio$ <> "" Then
        
            'append another comma seperated value onto the end of the audio
            'channels to ensure we get the last value
            l_DupeAudio$ = l_DupeAudio$ & ",END"
                
            'get each Sound channel using the GetTextPart function from
            'modRoutines
            l_Sound1$ = GetAlias(GetTextPart(l_DupeAudio$, 1, ","))
            l_Sound2$ = GetAlias(GetTextPart(l_DupeAudio$, 2, ","))
            l_Sound3$ = GetAlias(GetTextPart(l_DupeAudio$, 3, ","))
            l_Sound4$ = GetAlias(GetTextPart(l_DupeAudio$, 4, ","))
            l_Sound5$ = GetAlias(GetTextPart(l_DupeAudio$, 5, ","))
            l_Sound6$ = GetAlias(GetTextPart(l_DupeAudio$, 6, ","))
            l_Sound7$ = GetAlias(GetTextPart(l_DupeAudio$, 7, ","))
            l_Sound8$ = GetAlias(GetTextPart(l_DupeAudio$, 8, ","))
        
        End If
        
        l_rsDetails.AddNew
        l_rsDetails("jobID") = l_NewJobNumber&
        l_rsDetails("description") = Left(l_ProgrammeTitle$, 50)
        If l_DupeTapeFormat$ <> "" Then l_rsDetails("format") = l_DupeTapeFormat$
        l_rsDetails("copytype") = "C"
        If l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD" Then
            l_rsDetails("quantity") = l_EpisodeCount
        Else
            l_rsDetails("quantity") = Val(l_DupeQuantity$) * l_EpisodeCount
        End If
        If l_DupeLineStandard$ <> "" Then l_rsDetails("videostandard") = Left(l_DupeLineStandard$, 10)
        If Int(Val(l_MasterSpoolDuration$)) > Int(Val(l_DupeTotalDuration$)) Then
            l_rsDetails("runningtime") = Int(Val(l_MasterSpoolDuration$))
            l_DupeTotalDuration$ = l_MasterSpoolDuration$
        Else
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
        End If
        If l_DupeAspectRatio$ <> "" And l_DupeAspectRatio$ <> "ASK" Then
            l_rsDetails("aspectratio") = l_DupeAspectRatio$
        Else
            If l_Original_DupeStandard$ Like "*16:9 WI*" Then
                l_rsDetails("aspectratio") = "16:9AN"
                l_DupeAspectRatio$ = "16:9AN"
            End If
            If l_Original_DupeStandard$ Like "*16:9 LE*" Then
                l_rsDetails("aspectratio") = "16:9LE"
                l_DupeAspectRatio$ = "16:9LE"
            End If
            If l_Original_DupeStandard$ Like "*15:9*" Then
                l_rsDetails("aspectratio") = "15:9LE"
                l_DupeAspectRatio$ = "15:9LE"
            End If
            If l_Original_DupeStandard$ Like "*14:9*" Then
                l_rsDetails("aspectratio") = "14:9LE"
                l_DupeAspectRatio$ = "14:9LE"
            End If
            If l_Original_DupeStandard$ Like "*4:3*" Then
                l_rsDetails("aspectratio") = "4:3NOR"
                l_DupeAspectRatio$ = "4:3NOR"
            End If
        End If
        If l_rsDetails("aspectratio") = "" Then
            l_rsDetails("aspectratio") = "4:3NOR"
            l_DupeAspectRatio$ = "4:3NOR"
        End If
        If l_Sound1$ <> "" Then l_rsDetails("sound1") = Left(l_Sound1$, 6)
        If l_Sound2$ <> "" Then l_rsDetails("sound2") = Left(l_Sound2$, 6)
        If l_Sound3$ <> "" Then l_rsDetails("sound3") = Left(l_Sound3$, 6)
        If l_Sound4$ <> "" Then l_rsDetails("sound4") = Left(l_Sound4$, 6)
        
    
        l_rsDetails.Update
        
        l_rsDetails.Bookmark = l_rsDetails.Bookmark
        l_lngdetailID = l_rsDetails("jobdetailID")
        l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
        l_rsDetails.Update
        
        l_rsxDetails.AddNew
        l_rsxDetails("jobID") = l_NewJobNumber&
        l_rsxDetails("jobdetailID") = l_lngdetailID
        If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
        If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
        If l_OrderNumber$ <> "" Then l_rsxDetails("BBCorderNumber") = l_OrderNumber$
        If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
        If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
        If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
        If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
        If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
        If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
        If l_BBCMasterTapeFormat$ <> "" Then l_rsxDetails("bbcSourceFormat") = l_BBCMasterTapeFormat$
        If l_BBCDupeTapeFormat$ <> "" Then l_rsxDetails("bbcRecordFormat") = l_BBCDupeTapeFormat$
        l_rsxDetails.Update
            
        If UCase(l_DupeCeefax$) = "TRUE" Then
            'create a new ceefax line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "CEEFAX Subtitler"
            l_rsDetails("format") = "SUBT"
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
    
        End If
        
        If l_DupeLineStandard$ <> l_MasterLineStandard$ Then
            ' create a standards converter line
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Standards Converter"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "CONV"
            Else
                l_rsDetails("format") = "B-CONV"
            End If
            l_rsDetails("copytype") = "A"
            l_rsDetails("quantity") = l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
            
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
        
        End If
        
        If Left(l_MasterAspectRatio$, 6) <> Left(l_DupeAspectRatio$, 6) Then
            If Not (l_MasterAspectRatio$ Like "*4:3*" And l_DupeAspectRatio$ Like "*4:3*") Then
                ' create a ARC line
                l_rsDetails.AddNew
                l_rsDetails("jobID") = l_NewJobNumber&
                l_rsDetails("description") = "Aspect Ratio Converter"
                l_rsDetails("format") = "ARATIO"
                l_rsDetails("copytype") = "A"
                l_rsDetails("quantity") = l_EpisodeCount
                l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
                l_rsDetails.Update
            
                l_rsDetails.Bookmark = l_rsDetails.Bookmark
                l_lngdetailID = l_rsDetails("jobdetailID")
                l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
                l_rsDetails.Update
                
                l_rsxDetails.AddNew
                l_rsxDetails("jobID") = l_NewJobNumber&
                l_rsxDetails("jobdetailID") = l_lngdetailID
                If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
                If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
                If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
                If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
                If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
                If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
                If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
                If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
                If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
                l_rsxDetails.Update
            End If
        End If
    
        If (l_DupeTapeFormat$ = "VHS" Or l_DupeTapeFormat$ = "DVD") And Val(l_DupeQuantity$) > 1 Then
            
            'Do another line to add the extra DVD and VHS copies
            
            l_rsDetails.AddNew
            l_rsDetails("jobID") = l_NewJobNumber&
            l_rsDetails("description") = "Additional Copies"
            If l_DupeTapeFormat$ = "VHS" Then
                l_rsDetails("format") = "WWADVHS"
            Else
                l_rsDetails("format") = "WWADDVD"
            End If
            l_rsDetails("copytype") = "I"
            l_rsDetails("quantity") = (Val(l_DupeQuantity$) - 1) * l_EpisodeCount
            l_rsDetails("runningtime") = Int(Val(l_DupeTotalDuration$))
            l_rsDetails.Update
    
            l_rsDetails.Bookmark = l_rsDetails.Bookmark
            l_lngdetailID = l_rsDetails("jobdetailID")
            l_rsDetails("fd_orderby") = l_rsDetails.RecordCount - 1
            l_rsDetails.Update
            
            l_rsxDetails.AddNew
            l_rsxDetails("jobID") = l_NewJobNumber&
            l_rsxDetails("jobdetailID") = l_lngdetailID
            If l_BookingNumber$ <> "" Then l_rsxDetails("bbcbookingnumber") = l_BookingNumber$
            If l_ProgrammeNumber$ <> "" Then l_rsxDetails("BBCProgrammeID") = l_ProgrammeNumber$
            If l_OrderNumber$ <> "" Then l_rsxDetails("BBCOrderNumber") = l_OrderNumber$
            If l_CustomerName$ <> "" Then l_rsxDetails("bbccustomername") = l_CustomerName$
            If l_ChargeCode$ <> "" Then l_rsxDetails("bbcchargecode") = l_ChargeCode$
            If l_BusinessArea$ <> "" Then l_rsxDetails("bbcbusinessarea") = l_BusinessArea$
            If l_BusinessAreaCode$ <> "" Then l_rsxDetails("bbcbusinessareacode") = l_BusinessAreaCode$
            If l_NominalCode$ <> "" Then l_rsxDetails("bbcnominalcode") = l_NominalCode$
            If l_CountryCode$ <> "" Then l_rsxDetails("BBCCountryCode") = l_CountryCode$
            l_rsxDetails.Update
            
        End If
        DBEngine.Idle
        DoEvents
    Wend
    'Copy Loop has finished - need to reset the currenttop to beyond the end of the last group again
    l_CurrentTop& = l_CurrentTop& + (11 * l_OrigCopyCount)
Loop

'close the database and recordset
l_rsJobs.Close: Set l_rsJobs = Nothing
l_rsDetails.Close: Set l_rsDetails = Nothing
l_rsxJobs.Close: Set l_rsxJobs = Nothing
l_rsxDetails.Close: Set l_rsxDetails = Nothing
Set l_rsLib = Nothing
Set l_rsHistory = Nothing

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

'Rename file into the done folder - commented out during testing.
'Name l_strExcelFileName As "\\jcaserver\ceta\Processed Star Orders\" & l_strExcelFileTitle

MsgBox "File Sucessfully Read", vbInformation, "Excel Read"

End Sub

Private Sub cmdWellcome_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String

Dim l_strXLProgName As String, l_strXLISAN As String, l_strXLProdDate As String, l_strXLFormat As String, l_lngXLDuration As Long
Dim l_strXLBarcode As String, l_strXLBatchnumber As String, l_lngXLRowCounter As Long

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_strSQL As String

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Wellcome Excel Read", "sheet1")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2
l_strXLProgName = ""

l_strXLProgName = GetXLData(l_lngXLRowCounter, 1)

While l_strXLProgName <> ""
    
    If Len(l_strXLProgName) > 200 Then l_strXLProgName = Left(l_strXLProgName, 200)
    
    lblProgress.Caption = l_strXLProgName
    DoEvents
    
    'Read the relevant items from the XL line
    l_strXLISAN = GetXLData(l_lngXLRowCounter, 2)
    l_strXLBatchnumber = GetXLData(l_lngXLRowCounter, 3)
    l_strXLProdDate = GetXLData(l_lngXLRowCounter, 4)
    l_strXLFormat = GetXLData(l_lngXLRowCounter, 5)
    l_lngXLDuration = Val(GetXLData(l_lngXLRowCounter, 6))
    If l_lngXLDuration < 15 Then l_lngXLDuration = 15
    l_strXLBarcode = GetXLData(l_lngXLRowCounter, 7)
    
    'Make a Progress Item.
    l_strSQL = "INSERT INTO progressitem (companyID, title, ISAN, runningtime, format, cdate, cuser, batchnumber, productionyear, originalbarcode) VALUES ("
    l_strSQL = l_strSQL & "833, "
    l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strXLProgName) & "', "
    l_strSQL = l_strSQL & "'" & l_strXLISAN & "', "
    l_strSQL = l_strSQL & "'" & l_lngXLDuration & "', "
    l_strSQL = l_strSQL & "'" & GetAlias(l_strXLFormat) & "', "
    l_strSQL = l_strSQL & "getdate(), "
    l_strSQL = l_strSQL & "'" & g_strUserName & "', "
    l_strSQL = l_strSQL & "'" & l_strXLBatchnumber & "', "
    l_strSQL = l_strSQL & "'" & l_strXLProdDate & "', "
    l_strSQL = l_strSQL & "'" & l_strXLBarcode & "');"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'Increment the count and loop
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    l_strXLProgName = GetXLData(l_lngXLRowCounter, 1)

Wend

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
Beep
DoEvents

End Sub

Private Sub cmdWellcomeSegments_Click()

'have a variable to hold the filename we are going to process
Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String

Dim l_strXLISAN As String, l_strXLStartTime As String, l_strXLEndTime As String, l_strXLDuration As String
Dim l_lngXLRowCounter As Long, l_lngClipID As Long, l_rstClips As ADODB.Recordset

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Dim l_strSQL As String

'open the spreadsheet

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "Wellcome Excel Read", "Sheet1")

Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = 2
l_strXLISAN = ""

l_strXLISAN = GetXLData(l_lngXLRowCounter, 1)

While l_strXLISAN <> ""
    
    'Read the relevant items from the XL line
    l_strXLStartTime = GetXLData(l_lngXLRowCounter, 2)
    l_strXLEndTime = GetXLData(l_lngXLRowCounter, 3)
    l_strXLDuration = GetXLData(l_lngXLRowCounter, 4)
    
    lblProgress.Caption = l_strXLISAN & ": " & l_strXLStartTime & " - " & l_strXLEndTime
    DoEvents
    
    'Make a Segment Item.
    
    l_strSQL = "SELECT eventID FROM events WHERE clipreference LIKE '" & Left(l_strXLISAN, 15) & "%' AND clipformat LIKE 'MPEG 2%' AND companyID = 833;"
    Set l_rstClips = ExecuteSQL(l_strSQL, g_strExecuteError)
    CheckForSQLError
    If l_rstClips.RecordCount > 0 Then
        l_lngClipID = l_rstClips("eventID")
        l_rstClips.Close
        Set l_rstClips = Nothing
    Else
        MsgBox "No master clip found for " & l_strXLISAN, vbInformation, "Segment skipped."
        l_rstClips.Close
        Set l_rstClips = Nothing
        GoTo NextStage
    End If
    
    l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop) VALUES ("
    l_strSQL = l_strSQL & "'" & l_lngClipID & "', "
    l_strSQL = l_strSQL & "'" & l_strXLISAN & "', "
    l_strSQL = l_strSQL & "'" & l_strXLStartTime & "', "
    l_strSQL = l_strSQL & "'" & l_strXLEndTime & "');"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    'Increment the count and loop
    l_lngXLRowCounter = l_lngXLRowCounter + 1
    l_strXLISAN = GetXLData(l_lngXLRowCounter, 1)

NextStage:

Wend

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
Beep
DoEvents

End Sub

Private Sub cmdDespatchBarcodes_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long, l_strShelf As String, l_strBarcode As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)
Set oWorksheet = oWorkbook.Worksheets("Sheet1")

l_lngXLRowCounter = 1

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
    l_strBarcode = GetXLData(l_lngXLRowCounter, 1)
    
    frmJobDespatch.txtBarcode.Text = l_strBarcode
    frmJobDespatch.cmdBarcodeKeypressReturn.Value = True
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

Beep

End Sub

Private Sub cmdDADCBatchUpdate_Click()

Dim l_strExcelFileName As String, l_strExcelFileTitle As String
Dim l_strExcelSheetName As String, l_lngXLRowCounter As Long
Dim l_strBarcode As String, l_strReference As String, l_strTitle As String, l_strSubTitle As String, l_lngEpisode As Long, l_strAlphaBusiness As String
Dim l_lngBatchNumber As Long, l_strFormat As String, l_strLegacyLineStandard As String, l_strImageAspectRatio As String, l_strLegacyPictureElement As String
Dim l_strLanguage As String, l_strSubtitlesLanguage As String, l_strAudioConfiguration As String

Dim l_strSQL As String

l_strExcelFileName = txtExcelFilename.Text
l_strExcelFileTitle = lblFileTitle.Caption

If l_strExcelFileName = "" Then
    
    Beep
    Exit Sub

End If

Set oExcel = CreateObject("Excel.Application")
Set oWorkbook = oExcel.Workbooks.Open(l_strExcelFileName)

l_strExcelSheetName = InputBox("Please give the Worksheet Name to be read", "DADC Excel Read", "Sheet2")
Set oWorksheet = oWorkbook.Worksheets(l_strExcelSheetName)

l_lngXLRowCounter = InputBox("Please give the row number where the data starts", "DADC Excel Read")

While GetXLData(l_lngXLRowCounter, 1) <> ""
    
'    l_lngBatchNumber = GetXLData(l_lngXLRowCounter, 1)
    l_strBarcode = GetXLData(l_lngXLRowCounter, 42)
    l_strAlphaBusiness = GetXLData(l_lngXLRowCounter, 32)
'    l_strReference = l_strAlphaBusiness & "_" & SanitiseBarcode(l_strBarcode)
'    l_strTitle = GetXLData(l_lngXLRowCounter, 10)
'    l_strSubTitle = GetXLData(l_lngXLRowCounter, 20)
'    l_lngEpisode = GetXLData(l_lngXLRowCounter, 21)
'    l_strFormat = GetAlias(GetXLData(l_lngXLRowCounter, 46))
'    l_strLegacyLineStandard = GetXLData(l_lngXLRowCounter, 49)
'    l_strImageAspectRatio = GetXLData(l_lngXLRowCounter, 55)
'    l_strLegacyPictureElement = GetXLData(l_lngXLRowCounter, 48)
'    l_strLanguage = GetXLData(l_lngXLRowCounter, 51)
''   l_strSubtitlesLanguage = GetXLData(l_lngXLRowCounter, 54)
'    l_strAudioConfiguration = GetXLData(l_lngXLRowCounter, 50)
    
    lblProgress.Caption = l_strBarcode & " - " & l_strTitle & " Ep. " & l_lngEpisode
    DoEvents
    
    l_strSQL = "UPDATE tracker_dadc_item SET "
    l_strSQL = l_strSQL & "externalalphaID = '" & l_strAlphaBusiness & "' "
'    l_strSQL = l_strSQL & "legacylinestandard = '" & l_strLegacyLineStandard & "', "
'    l_strSQL = l_strSQL & "imageaspectratio = '" & l_strImageAspectRatio & "', "
'    l_strSQL = l_strSQL & "legacypictureelement = '" & l_strLegacyPictureElement & "', "
'    l_strSQL = l_strSQL & "language = '" & l_strLanguage & "', "
'    l_strSQL = l_strSQL & "subtitleslanguage = '" & l_strSubtitlesLanguage & "', "
'    l_strSQL = l_strSQL & "audioconfiguration = '" & l_strAudioConfiguration & "' "
    l_strSQL = l_strSQL & "WHERE barcode = '" & l_strBarcode & "';"
    
    ExecuteSQL l_strSQL, g_strExecuteError
    CheckForSQLError
    
    l_lngXLRowCounter = l_lngXLRowCounter + 1

Wend

Set oWorksheet = Nothing
oWorkbook.Close False
Set oWorkbook = Nothing
oExcel.Quit
Set oExcel = Nothing

lblProgress.Caption = ""
DoEvents
Beep

End Sub

Private Sub Form_Load()

CenterForm Me

End Sub

Function Construct_Svensk_Update_String(lp_strTrackerID As String, ByRef l_strAuditDescription As String, ByRef l_blnError As Boolean, l_strAlphaDisplayCode As String, l_strRevisionNumber As String, l_strUrgent As String, _
l_datDueDate As Date, l_strProjectManager As String, l_strRightsOwner As String, l_strSpecialProject As String, l_strProxyRequired As String, l_strMERequired As String, l_strME51Required As String, l_strSubTitle As String, _
l_striTunesRequired As String, l_strConform As String, l_lngXLRowCounter As Long) As String


'Existing line that is not completed yet...
l_strAuditDescription = ""
Construct_Svensk_Update_String = "UPDATE tracker_svensk_item SET "
If l_strAlphaDisplayCode <> "" Then Construct_Svensk_Update_String = Construct_Svensk_Update_String & "alphadisplaycode = '" & l_strAlphaDisplayCode & "', "
If l_strRevisionNumber <> "" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "RevisionNumber = '" & l_strRevisionNumber & "', "
    l_strAuditDescription = l_strAuditDescription & "RevisionNumer = " & l_strRevisionNumber & ", "
End If
If l_strUrgent <> "" Then
    If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(lp_strTrackerID)) <> -1 Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "urgent = -1, "
        l_strAuditDescription = l_strAuditDescription & "urgent = -1, "
    End If
'Else
'    If GetData("tracker_svensk_item", "urgent", "tracker_svensk_itemID", Val(lp_strTrackerID)) <> 0 Then
'        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "urgent = 0, "
'        l_strAuditDescription = l_strAuditDescription & "urgent = 0, "
'    End If
End If
If GetData("tracker_svensk_item", "duedate", "tracker_svensk_itemID", lp_strTrackerID) <> l_datDueDate Then
    If l_datDueDate > Now Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "duedate = '" & FormatSQLDate(l_datDueDate) & "', "
        l_strAuditDescription = l_strAuditDescription & "duedate = '" & l_datDueDate & "', "
    Else
        MsgBox "Line " & l_lngXLRowCounter & " has a Due Date that is earlier than Now - Line not read in.", vbCritical, "Error Reading Sheet"
        l_blnError = True
    End If
End If
If Val(Trim(" " & GetDataSQL("SELECT contactID FROM contact WHERE name = '" & l_strProjectManager & "' AND contactID IN (SELECT contactID FROM employee WHERE companyID = 1415);"))) <> 0 Then
    If GetData("tracker_svensk_item", "projectmanager", "tracker_svensk_itemID", lp_strTrackerID) <> l_strProjectManager Then
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
        Construct_Svensk_Update_String = Construct_Svensk_Update_String & "projectmanagercontactID = " & GetData("contact", "contactID", "name", l_strProjectManager) & ", "
        l_strAuditDescription = l_strAuditDescription & "projectmanager = '" & QuoteSanitise(l_strProjectManager) & "', "
    End If
Else
    MsgBox "Line " & l_lngXLRowCounter & " has an unidentified Project Manager - Line not read in.", vbCritical, "Error Reading Sheet"
    l_blnError = True
End If
If GetData("tracker_svensk_item", "rightsowner", "tracker_svensk_itemID", lp_strTrackerID) <> l_strRightsOwner Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
    l_strAuditDescription = l_strAuditDescription & "rightsowner = '" & QuoteSanitise(l_strRightsOwner) & "', "
End If
If GetData("tracker_svensk_item", "SpecialProject", "tracker_svensk_itemID", lp_strTrackerID) <> l_strSpecialProject Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "SpecialProject = '" & QuoteSanitise(l_strSpecialProject) & "', "
    l_strAuditDescription = l_strAuditDescription & "SpecialProject = '" & QuoteSanitise(l_strSpecialProject) & "', "
End If
If Left(UCase(l_strProxyRequired), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "proxyreq = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "proxyreq = 0, "
End If
If Left(UCase(l_strMERequired), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "MErequired = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "MErequired = 0, "
End If
If Left(UCase(l_strME51Required), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "ME51required = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "ME51required = 0, "
End If
Construct_Svensk_Update_String = Construct_Svensk_Update_String & "subtitle = '" & QuoteSanitise(l_strSubTitle) & "', "
If Left(UCase(l_striTunesRequired), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "itunes = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "itunes = 0, "
End If
If Left(UCase(l_strConform), 1) = "Y" Then
    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "conform = 1, "
'Else
'    Construct_Svensk_Update_String = Construct_Svensk_Update_String & "conform = 0, "
End If

If Right(Construct_Svensk_Update_String, 2) = ", " Then Construct_Svensk_Update_String = Left(Construct_Svensk_Update_String, Len(Construct_Svensk_Update_String) - 2) & " "

End Function
