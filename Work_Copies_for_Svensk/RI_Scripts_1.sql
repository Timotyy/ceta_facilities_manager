/*Transfer back items from the eventsarchive table to the events table*/
SET IDENTITY_INSERT events ON;
INSERT INTO events ( eventID, libraryID, timecodestart,timecodestop, eventtitle, fd_length, cdate, cuser, mdate, muser, eventtype, resourceID, editor1, editor2, vodplatform, editor3, soundlay, clocknumber, notes, 
jobID, sourcelibraryID, aspectratio, projectnumber, eventdate, geometriclinearity, clipstoreID, sourceeventID, sourcetapeeventID, sourcefrommedia, clipformat, clipcodec, mediastreamtype, 
clipaudiocodec, cliphorizontalpixels, clipverticalpixels, clipsoundformat, clipbitrate, videobitrate, audiobitrate, trackcount, clipfilename, companyID, companyname, eventseries, eventset, eventepisode, 
eventversion, altlocation, clipreference, eventmediatype, clipframerate, cliptype, clipgenre, internalnotes, eventkeyframetimecode, eventkeyframefilename, istextinpicture, 
customfield1, customfield2, customfield3, customfield4, customfield5, customfield6, internalreference, eventhighreskeyframefilename, online, encodepasses, cbrvbr, interlace, 
break1, break2, break3, break4, break5, break6, break7, break8, endcredits, break1elapsed, break2elapsed, break3elapsed, break4elapsed, break5elapsed, break6elapsed, break7elapsed, break8elapsed, endcreditselapsed, 
break9, break10, break11, break12, break13, break14, break15, break16, break17, break18, break9elapsed, break10elapsed, break11elapsed, break12elapsed, break13elapsed, break14elapsed, break15elapsed, break16elapsed, break17elapsed, break18elapsed, 
audiotypegroup1, audiotypegroup2, audiotypegroup3, audiotypegroup4, audiotypegroup5, audiotypegroup6, audiotypegroup7, audiotypegroup8, audiotypegroup9, audiotypegroup10, 
audiolanguagegroup1, audiolanguagegroup2, audiolanguagegroup3, audiolanguagegroup4, audiolanguagegroup5, audiolanguagegroup6, audiolanguagegroup7, audiolanguagegroup8, audiolanguagegroup9, audiolanguagegroup10, 
audiocontentgroup1, audiocontentgroup2, audiocontentgroup3, audiocontentgroup4, audiocontentgroup5, audiocontentgroup6, audiocontentgroup7, audiocontentgroup8, audiocontentgroup9, audiocontentgroup10, 
eventflashpreviewfilename, fourbythreeflag, bigfilesize, eventsubtitle, hidefromweb, provider, video_type, video_networkname, video_vendorID, itunestechcomment, iTunesVendorOfferCode, 
video_ep_prod_no, video_containerID, video_containerposition, video_releasedate, video_copyright_cline, video_shortdescription, video_longdescription, video_previewstarttime, 
itunescroptop, itunescropbottom, itunescropleft, itunescropright, language, seriesID, lastmediainfoquery, sound_stereo_mmn, sound_surround_mmn, textless, svenskprojectnumber, 
video_ISAN, md5checksum, sha1checksum, rating_au, rating_ca, rating_de, rating_fr, rating_uk, rating_us, rating_jp, video_title, sound_stereo_main, sound_stereo_me, 
sound_ltrt_main, sound_ltrt_me, sound_surround_main, sound_surround_me, sound_other, video_language, originalspokenlocale, metadata_language, sound_stereo_russian, system_deleted) 
SELECT eventID, libraryID, timecodestart,timecodestop, eventtitle, fd_length, cdate, cuser, mdate, muser, eventtype, resourceID, editor1, editor2, vodplatform, 
editor3, soundlay, clocknumber, notes, jobID, sourcelibraryID, aspectratio, projectnumber, eventdate, geometriclinearity, clipstoreID, sourceeventID, sourcetapeeventID, sourcefrommedia, clipformat, clipcodec, mediastreamtype, 
clipaudiocodec, cliphorizontalpixels, clipverticalpixels, clipsoundformat, clipbitrate, videobitrate, audiobitrate, trackcount, clipfilename, companyID, companyname, eventseries, eventset, eventepisode, 
eventversion, altlocation, clipreference, eventmediatype, clipframerate, cliptype, clipgenre, internalnotes, eventkeyframetimecode, eventkeyframefilename, istextinpicture, 
customfield1, customfield2, customfield3, customfield4, customfield5, customfield6, internalreference, eventhighreskeyframefilename, online, encodepasses, cbrvbr, interlace, 
break1, break2, break3, break4, break5, break6, break7, break8, endcredits, break1elapsed, break2elapsed, break3elapsed, break4elapsed, break5elapsed, break6elapsed, break7elapsed, break8elapsed, endcreditselapsed, 
break9, break10, break11, break12, break13, break14, break15, break16, break17, break18, break9elapsed, break10elapsed, break11elapsed, break12elapsed, break13elapsed, break14elapsed, break15elapsed, break16elapsed, break17elapsed, break18elapsed, 
audiotypegroup1, audiotypegroup2, audiotypegroup3, audiotypegroup4, audiotypegroup5, audiotypegroup6, audiotypegroup7, audiotypegroup8, audiotypegroup9, audiotypegroup10, 
audiolanguagegroup1, audiolanguagegroup2, audiolanguagegroup3, audiolanguagegroup4, audiolanguagegroup5, audiolanguagegroup6, audiolanguagegroup7, audiolanguagegroup8, audiolanguagegroup9, audiolanguagegroup10, 
audiocontentgroup1, audiocontentgroup2, audiocontentgroup3, audiocontentgroup4, audiocontentgroup5, audiocontentgroup6, audiocontentgroup7, audiocontentgroup8, audiocontentgroup9, audiocontentgroup10, 
eventflashpreviewfilename, fourbythreeflag, bigfilesize, eventsubtitle, hidefromweb, provider, video_type, video_networkname, video_vendorID, itunestechcomment, iTunesVendorOfferCode, 
video_ep_prod_no, video_containerID, video_containerposition, video_releasedate, video_copyright_cline, video_shortdescription, video_longdescription, video_previewstarttime, 
itunescroptop, itunescropbottom, itunescropleft, itunescropright, language, seriesID, lastmediainfoquery, sound_stereo_mmn, sound_surround_mmn, textless, svenskprojectnumber, 
video_ISAN, md5checksum, sha1checksum, rating_au, rating_ca, rating_de, rating_fr, rating_uk, rating_us, rating_jp, video_title, sound_stereo_main, sound_stereo_me, 
sound_ltrt_main, sound_ltrt_me, sound_surround_main, sound_surround_me, sound_other, video_language, originalspokenlocale, metadata_language, sound_stereo_russian, 1 AS system_deleted 
FROM eventsarchive 
WHERE eventmediatype = 'FILE';
SET IDENTITY_INSERT events OFF;
DELETE FROM eventsarchive WHERE eventmediatype = 'FILE';

/*Add a Date Created field to Event Logging, default to server current date*/
alter table eventlogging
add cdate datetime not null default getdate()
