VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSkibblyTracker 
   Caption         =   "Work Tracker"
   ClientHeight    =   17565
   ClientLeft      =   3060
   ClientTop       =   3210
   ClientWidth     =   25005
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSkibblyTracker.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   17565
   ScaleWidth      =   25005
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame3 
      Caption         =   "DVCAM 16:9 PAL"
      Height          =   2415
      Index           =   18
      Left            =   20880
      TabIndex        =   321
      Top             =   9420
      Width           =   3195
      Begin VB.CheckBox chkPAL16x9DVCam5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   340
         Top             =   1980
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCam5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   339
         Top             =   1920
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9DVCam1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   329
         Top             =   240
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCam1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   328
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCam2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   327
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCam2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   326
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCam3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   325
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCam3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   324
         Top             =   1140
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCam4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   323
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCam4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   322
         Top             =   1500
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "DVC-Pro50 16:9 PAL"
      Height          =   4095
      Index           =   20
      Left            =   17400
      TabIndex        =   306
      Top             =   8100
      Width           =   3195
      Begin VB.CheckBox chkPAL16x9DVCPro509 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 9 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   352
         Top             =   3660
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCPro508 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 8 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   351
         Top             =   3240
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCPro509barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   350
         Top             =   3600
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9DVCPro508barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   349
         Top             =   3180
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCPro504 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   320
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCPro504barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   319
         Top             =   1500
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9DVCPro505barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   318
         Top             =   1920
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9DVCPro506barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   317
         Top             =   2340
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCPro505 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   316
         Top             =   1980
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCPro506 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 6 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   315
         Top             =   2400
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCPro503 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   314
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCPro503barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   313
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCPro502 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   312
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCPro502barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   311
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCPro501 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   310
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCPro501barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   309
         Top             =   240
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCPro507 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 7 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   308
         Top             =   2820
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCPro507barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   307
         Top             =   2760
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "BetaSX 4:3 PAL"
      Height          =   2415
      Index           =   12
      Left            =   20880
      TabIndex        =   297
      Top             =   6840
      Width           =   3195
      Begin VB.CheckBox chkPAL4x3BetaSX5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   346
         Top             =   1980
         Width           =   1575
      End
      Begin VB.TextBox txtPAL4x3BetaSX5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   345
         Top             =   1920
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL4x3BetaSX3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   305
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtPAL4x3BetaSX3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   304
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL4x3BetaSX2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   303
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL4x3BetaSX2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   302
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL4x3BetaSX1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   301
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL4x3BetaSX1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   300
         Top             =   240
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL4x3BetaSX4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   299
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtPAL4x3BetaSX4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   298
         Top             =   1500
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "BetaSX 16:9 PAL"
      Height          =   2415
      Index           =   11
      Left            =   20880
      TabIndex        =   278
      Top             =   4260
      Width           =   3195
      Begin VB.TextBox txtPAL16x9BetaSX5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   344
         Top             =   1920
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9BetaSX5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   343
         Top             =   1980
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9BetaSX4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   286
         Top             =   1500
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9BetaSX4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   285
         Top             =   1560
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9BetaSX3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   284
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9BetaSX3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   283
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9BetaSX2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   282
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9BetaSX2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   281
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9BetaSX1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   280
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9BetaSX1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   279
         Top             =   240
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "DIGI 16x9 PAL "
      Height          =   2415
      Index           =   2
      Left            =   20880
      TabIndex        =   269
      Top             =   1680
      Width           =   3195
      Begin VB.CheckBox chkPAL16x9Digi5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   342
         Top             =   1980
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9Digi5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   341
         Top             =   1920
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9Digi4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   277
         Top             =   1500
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9Digi4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   276
         Top             =   1560
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9Digi3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   275
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9Digi3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   274
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9Digi2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   273
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9Digi2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   272
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9Digi1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   271
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9Digi1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   270
         Top             =   240
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   " HDCAM 1080/50"
      Height          =   2415
      Index           =   0
      Left            =   13980
      TabIndex        =   260
      Top             =   1680
      Width           =   3195
      Begin VB.TextBox txtPal16x9hdcam5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   334
         Top             =   1920
         Width           =   1275
      End
      Begin VB.CheckBox chkPal16x9hdcam5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   333
         Top             =   1980
         Width           =   1575
      End
      Begin VB.TextBox txtPal16x9hdcam1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   268
         Top             =   240
         Width           =   1275
      End
      Begin VB.CheckBox chkPal16x9hdcam1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   267
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPal16x9hdcam2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   266
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPal16x9hdcam2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   265
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPal16x9hdcam3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   264
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPal16x9hdcam3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   263
         Top             =   1140
         Width           =   1575
      End
      Begin VB.CheckBox chkPal16x9hdcam4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   262
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtPal16x9hdcam4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   261
         Top             =   1500
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "HDCAM 1080/59.94"
      Height          =   1635
      Index           =   1
      Left            =   13980
      TabIndex        =   253
      Top             =   4260
      Width           =   3195
      Begin VB.CheckBox chkNTSC16x9hdcam3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   259
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9hdcam3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   258
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkNTSC16x9hdcam2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   257
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9hdcam2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   256
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkNTSC16x9hdcam1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   255
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9hdcam1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   254
         Top             =   240
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "DVC-Pro HD 16:9 PAL"
      Height          =   3255
      Index           =   21
      Left            =   17400
      TabIndex        =   238
      Top             =   1680
      Width           =   3195
      Begin VB.TextBox txtPAL16x9DVCProHD1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   252
         Top             =   240
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   251
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCProHD2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   250
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   249
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCProHD3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   248
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   247
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCProHD4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   246
         Top             =   1500
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9DVCProHD5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   245
         Top             =   1920
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9DVCProHD6barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   244
         Top             =   2340
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   243
         Top             =   1560
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   242
         Top             =   1980
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD6 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 6 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   241
         Top             =   2400
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9DVCProHD7 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 7 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   240
         Top             =   2820
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9DVCProHD7barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   239
         Top             =   2760
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "XDCAM HD 16:9 PAL"
      Height          =   4095
      Index           =   23
      Left            =   13980
      TabIndex        =   221
      Top             =   6060
      Width           =   3195
      Begin VB.CheckBox chkPAL16x9XDCAMHD9 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 9 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   348
         Top             =   3660
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD9barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   347
         Top             =   3600
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   237
         Top             =   240
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   236
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   235
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   234
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   233
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   232
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   231
         Top             =   1500
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   230
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   229
         Top             =   1920
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD6barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   228
         Top             =   2340
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   227
         Top             =   1980
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD6 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 6 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   226
         Top             =   2400
         Width           =   1575
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD7 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 7 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   225
         Top             =   2820
         Width           =   1575
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD7barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   224
         Top             =   2760
         Width           =   1275
      End
      Begin VB.TextBox txtPAL16x9XDCAMHD8barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   223
         Top             =   3180
         Width           =   1275
      End
      Begin VB.CheckBox chkPAL16x9XDCAMHD8 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 8 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   222
         Top             =   3240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "XDCAM HD 16:9 NTSC"
      Height          =   2835
      Index           =   24
      Left            =   13980
      TabIndex        =   208
      Top             =   10320
      Width           =   3195
      Begin VB.CheckBox chkntsc16x9XDCAMHD6 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 6 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   220
         Top             =   2400
         Width           =   1575
      End
      Begin VB.CheckBox chkntsc16x9XDCAMHD5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   219
         Top             =   1980
         Width           =   1575
      End
      Begin VB.TextBox txtntsc16x9XDCAMHD6barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   218
         Top             =   2340
         Width           =   1275
      End
      Begin VB.TextBox txtntsc16x9XDCAMHD5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   217
         Top             =   1920
         Width           =   1275
      End
      Begin VB.CheckBox chkntsc16x9XDCAMHD4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   216
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtntsc16x9XDCAMHD4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   215
         Top             =   1500
         Width           =   1275
      End
      Begin VB.CheckBox chkntsc16x9XDCAMHD3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   214
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtntsc16x9XDCAMHD3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   213
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkntsc16x9XDCAMHD2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   212
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtntsc16x9XDCAMHD2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   211
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkntsc16x9XDCAMHD1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   210
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtntsc16x9XDCAMHD1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   209
         Top             =   240
         Width           =   1275
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "DVC-Pro HD 16:9 NTSC"
      Height          =   2835
      Index           =   7
      Left            =   17400
      TabIndex        =   195
      Top             =   5100
      Width           =   3195
      Begin VB.CheckBox chkNTSC16x9DVCProHD6 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 6 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   207
         Top             =   2400
         Width           =   1575
      End
      Begin VB.CheckBox chkNTSC16x9DVCProHD5 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 5 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   206
         Top             =   1980
         Width           =   1575
      End
      Begin VB.CheckBox chkNTSC16x9DVCProHD4 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 4 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   205
         Top             =   1560
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9DVCProHD6barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   204
         Top             =   2340
         Width           =   1275
      End
      Begin VB.TextBox txtNTSC16x9DVCProHD5barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   203
         Top             =   1920
         Width           =   1275
      End
      Begin VB.TextBox txtNTSC16x9DVCProHD4barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   202
         Top             =   1500
         Width           =   1275
      End
      Begin VB.CheckBox chkNTSC16x9DVCProHD3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 3 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   201
         Top             =   1140
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9DVCProHD3barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   200
         Top             =   1080
         Width           =   1275
      End
      Begin VB.CheckBox chkNTSC16x9DVCProHD2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 2 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   199
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9DVCProHD2barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   198
         Top             =   660
         Width           =   1275
      End
      Begin VB.CheckBox chkNTSC16x9DVCProHD1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tape 1 Required?"
         Height          =   255
         Left            =   120
         TabIndex        =   197
         Top             =   300
         Width           =   1575
      End
      Begin VB.TextBox txtNTSC16x9DVCProHD1barcode 
         Height          =   375
         Left            =   1800
         TabIndex        =   196
         Top             =   240
         Width           =   1275
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Unused Filters"
      Height          =   1515
      Left            =   11460
      TabIndex        =   177
      Top             =   15420
      Visible         =   0   'False
      Width           =   12015
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "XDCAM 16x9 PAL"
         Height          =   255
         Index           =   17
         Left            =   9660
         TabIndex        =   296
         Top             =   300
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVC-Pro 4:3 PAL"
         Height          =   255
         Index           =   15
         Left            =   7440
         TabIndex        =   295
         Top             =   840
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVC-Pro 16:9 PAL"
         Height          =   255
         Index           =   21
         Left            =   7440
         TabIndex        =   294
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "BetaSX 16:9 NTSC"
         Height          =   255
         Index           =   14
         Left            =   5700
         TabIndex        =   293
         Top             =   540
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVCAM 4:3 PAL"
         Height          =   255
         Index           =   10
         Left            =   3840
         TabIndex        =   292
         Top             =   540
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVCAM 4:3 NTSC"
         Height          =   255
         Index           =   11
         Left            =   3840
         TabIndex        =   291
         Top             =   1140
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVCAM 16:9 NTSC"
         Height          =   255
         Index           =   28
         Left            =   3840
         TabIndex        =   290
         Top             =   840
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DIGI 4:3 PAL"
         Height          =   255
         Index           =   4
         Left            =   2340
         TabIndex        =   289
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DIGI 16:9 NTSC"
         Height          =   255
         Index           =   5
         Left            =   2340
         TabIndex        =   288
         Top             =   540
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVCAM HD 16:9 PAL"
         Height          =   255
         Index           =   23
         Left            =   180
         TabIndex        =   287
         Top             =   540
         Width           =   1995
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "HDCAM SR 16:9 PAL"
         Height          =   255
         Index           =   20
         Left            =   180
         TabIndex        =   178
         Top             =   300
         Width           =   1875
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   12075
      Left            =   120
      TabIndex        =   76
      Top             =   15000
      Visible         =   0   'False
      Width           =   10515
      _ExtentX        =   18547
      _ExtentY        =   21299
      _Version        =   393216
      Tabs            =   4
      Tab             =   2
      TabsPerRow      =   4
      TabHeight       =   520
      Enabled         =   0   'False
      TabCaption(0)   =   "HD Tapes"
      TabPicture(0)   =   "frmSkibblyTracker.frx":08CA
      Tab(0).ControlEnabled=   0   'False
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Beta Tapes"
      TabPicture(1)   =   "frmSkibblyTracker.frx":08E6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3(13)"
      Tab(1).Control(1)=   "Frame3(4)"
      Tab(1).Control(2)=   "Frame3(3)"
      Tab(1).ControlCount=   3
      TabCaption(2)   =   "Other Tapes"
      TabPicture(2)   =   "frmSkibblyTracker.frx":0902
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Frame3(9)"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Frame3(10)"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "Frame3(16)"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "Frame3(14)"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "Frame3(26)"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "Frame3(6)"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Frame3(19)"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "Frame3(22)"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).ControlCount=   8
      TabCaption(3)   =   "File Delivery"
      TabPicture(3)   =   "frmSkibblyTracker.frx":091E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).ControlCount=   0
      Begin VB.Frame Frame3 
         Caption         =   "DVCAM HD 16:9 PAL"
         Height          =   1995
         Index           =   22
         Left            =   3660
         TabIndex        =   186
         Top             =   5940
         Visible         =   0   'False
         Width           =   3195
         Begin VB.TextBox txtPAL16x9DVCamHD4barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   194
            Top             =   1500
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCamHD4 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 4 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   193
            Top             =   1560
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9DVCamHD1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   192
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCamHD1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   191
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9DVCamHD2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   190
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCamHD2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   189
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9DVCamHD3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   188
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCamHD3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   187
            Top             =   1140
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   " HDCAM SR 16x9 PAL "
         Height          =   1635
         Index           =   19
         Left            =   3660
         TabIndex        =   179
         Top             =   4020
         Visible         =   0   'False
         Width           =   3195
         Begin VB.TextBox txtPal16x9hdcamSR1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   185
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkPal16x9hdcamSR1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   184
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPal16x9hdcamSR2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   183
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPal16x9hdcamSR2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   182
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPal16x9hdcamSR3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   181
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPal16x9hdcamSR3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   180
            Top             =   1140
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DVCAM 16:9 NTSC"
         Height          =   1635
         Index           =   6
         Left            =   240
         TabIndex        =   146
         Top             =   5040
         Width           =   3195
         Begin VB.CheckBox chkNTSC16x9DVCam3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   152
            Top             =   1140
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC16x9DVCam3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   151
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC16x9DVCam2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   150
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC16x9DVCam2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   149
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC16x9DVCam1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   148
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC16x9DVCam1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   147
            Top             =   240
            Width           =   1275
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DVC-Pro 16:9 PAL"
         Height          =   2055
         Index           =   26
         Left            =   7020
         TabIndex        =   137
         Top             =   480
         Width           =   3195
         Begin VB.TextBox txtPAL16x9DVCPro4barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   168
            Top             =   1500
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCPro4 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 4 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   167
            Top             =   1560
            Width           =   1575
         End
         Begin VB.CheckBox chkPAL16x9DVCPro3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   143
            Top             =   1140
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9DVCPro3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   142
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCPro2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   141
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9DVCPro2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   140
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9DVCPro1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   139
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9DVCPro1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   138
            Top             =   240
            Width           =   1275
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DVC-Pro 4:3 PAL"
         Height          =   2115
         Index           =   14
         Left            =   7020
         TabIndex        =   125
         Top             =   2760
         Width           =   3195
         Begin VB.CheckBox chkPAL4x3DVCPro3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   133
            Top             =   1140
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCPro3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   132
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCPro2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   131
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCPro2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   130
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCPro1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   129
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCPro1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   128
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCPro4 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 4 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   127
            Top             =   1560
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCPro4barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   126
            Top             =   1500
            Width           =   1275
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "XDCAM 16:9 PAL"
         Height          =   3315
         Index           =   16
         Left            =   3660
         TabIndex        =   112
         Top             =   480
         Width           =   3195
         Begin VB.TextBox txtPAL16x9XDCAM7barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   166
            Top             =   2760
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9XDCAM7 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 7 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   165
            Top             =   2820
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9XDCAM1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   124
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9XDCAM1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   123
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9XDCAM2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   122
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9XDCAM2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   121
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9XDCAM3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   120
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9XDCAM3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   119
            Top             =   1140
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9XDCAM4barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   118
            Top             =   1500
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9XDCAM4 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 4 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   117
            Top             =   1560
            Width           =   1575
         End
         Begin VB.TextBox txtPAL16x9XDCAM5barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   116
            Top             =   1920
            Width           =   1275
         End
         Begin VB.TextBox txtPAL16x9XDCAM6barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   115
            Top             =   2340
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL16x9XDCAM5 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 5 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   114
            Top             =   1980
            Width           =   1575
         End
         Begin VB.CheckBox chkPAL16x9XDCAM6 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 6 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   113
            Top             =   2400
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DVCAM 4:3 NTSC"
         Height          =   1635
         Index           =   10
         Left            =   240
         TabIndex        =   105
         Top             =   6900
         Width           =   3195
         Begin VB.TextBox txtNTSC4x3DVCam1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   111
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC4x3DVCam1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   110
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC4x3DVCam2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   109
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC4x3DVCam2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   108
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC4x3DVCam3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   107
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC4x3DVCam3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   106
            Top             =   1140
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DVCAM 4:3 PAL"
         Height          =   2055
         Index           =   9
         Left            =   240
         TabIndex        =   98
         Top             =   2760
         Width           =   3195
         Begin VB.TextBox txtPAL4x3DVCam4barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   164
            Top             =   1500
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCam4 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 4 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   163
            Top             =   1560
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCam1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   104
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCam1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   103
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCam2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   102
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCam2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   101
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3DVCam3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   100
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3DVCam3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   99
            Top             =   1140
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "BetaSX 16:9 NTSC"
         Height          =   1635
         Index           =   13
         Left            =   -67920
         TabIndex        =   91
         Top             =   4800
         Width           =   3195
         Begin VB.TextBox txtNTSC4x3BetaSX1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   97
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC4x3BetaSX1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   96
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC4x3BetaSX2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   95
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC4x3BetaSX2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   94
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC4x3BetaSX3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   93
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC4x3BetaSX3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   92
            Top             =   1140
            Width           =   1575
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DIGI 4x3 PAL "
         Height          =   1995
         Index           =   4
         Left            =   -74760
         TabIndex        =   84
         Top             =   4440
         Width           =   3195
         Begin VB.TextBox txtPAL4x3Digi4barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   162
            Top             =   1500
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3Digi4 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 4 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   161
            Top             =   1560
            Width           =   1575
         End
         Begin VB.CheckBox chkPAL4x3Digi3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   90
            Top             =   1140
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3Digi3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   89
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3Digi2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   88
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3Digi2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   87
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkPAL4x3Digi1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   86
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtPAL4x3Digi1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   85
            Top             =   240
            Width           =   1275
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "DIGI 16x9 NTSC "
         Height          =   1635
         Index           =   3
         Left            =   -74760
         TabIndex        =   77
         Top             =   2640
         Width           =   3195
         Begin VB.CheckBox chkNTSC16x9Digi3 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 3 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   83
            Top             =   1140
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC16x9Digi3barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   82
            Top             =   1080
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC16x9Digi2 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 2 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   81
            Top             =   720
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC16x9Digi2barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   80
            Top             =   660
            Width           =   1275
         End
         Begin VB.CheckBox chkNTSC16x9Digi1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tape 1 Required?"
            Height          =   255
            Left            =   120
            TabIndex        =   79
            Top             =   300
            Width           =   1575
         End
         Begin VB.TextBox txtNTSC16x9Digi1barcode 
            Height          =   375
            Left            =   1800
            TabIndex        =   78
            Top             =   240
            Width           =   1275
         End
      End
   End
   Begin VB.Frame FraWheresMyTape 
      Caption         =   "Where's My Tape?"
      Height          =   4215
      Left            =   10620
      TabIndex        =   53
      Top             =   4020
      Width           =   3195
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   8
         Left            =   2280
         TabIndex        =   355
         Top             =   3720
         Width           =   795
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   8
         Left            =   840
         TabIndex        =   353
         Top             =   3720
         Width           =   1335
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   7
         Left            =   840
         TabIndex        =   174
         Top             =   3300
         Width           =   1335
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   7
         Left            =   2280
         TabIndex        =   173
         Top             =   3300
         Width           =   795
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   6
         Left            =   2280
         TabIndex        =   160
         Top             =   2880
         Width           =   795
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   6
         Left            =   840
         TabIndex        =   159
         Top             =   2880
         Width           =   1335
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   5
         Left            =   2280
         TabIndex        =   66
         Top             =   2460
         Width           =   795
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   4
         Left            =   2280
         TabIndex        =   65
         Top             =   2040
         Width           =   795
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   3
         Left            =   2280
         TabIndex        =   64
         Top             =   1620
         Width           =   795
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   2
         Left            =   2280
         TabIndex        =   63
         Top             =   1200
         Width           =   795
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   5
         Left            =   840
         TabIndex        =   62
         Top             =   2460
         Width           =   1335
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   4
         Left            =   840
         TabIndex        =   61
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   3
         Left            =   840
         TabIndex        =   60
         Top             =   1620
         Width           =   1335
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   2
         Left            =   840
         TabIndex        =   59
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   1
         Left            =   2280
         TabIndex        =   58
         Top             =   780
         Width           =   795
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   1
         Left            =   840
         TabIndex        =   57
         Top             =   780
         Width           =   1335
      End
      Begin VB.CommandButton cmdSearchForTape 
         Caption         =   "Search"
         Height          =   375
         Index           =   0
         Left            =   2280
         TabIndex        =   56
         Top             =   360
         Width           =   795
      End
      Begin VB.TextBox txtSearchForTape 
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   54
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 9"
         Height          =   195
         Index           =   31
         Left            =   120
         TabIndex        =   354
         Top             =   3780
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 8"
         Height          =   195
         Index           =   28
         Left            =   120
         TabIndex        =   175
         Top             =   3360
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 7"
         Height          =   195
         Index           =   25
         Left            =   120
         TabIndex        =   158
         Top             =   2940
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 6"
         Height          =   195
         Index           =   20
         Left            =   120
         TabIndex        =   71
         Top             =   2520
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 5"
         Height          =   195
         Index           =   19
         Left            =   120
         TabIndex        =   70
         Top             =   2100
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 4"
         Height          =   195
         Index           =   18
         Left            =   120
         TabIndex        =   69
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 3"
         Height          =   195
         Index           =   17
         Left            =   120
         TabIndex        =   68
         Top             =   1260
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 2"
         Height          =   195
         Index           =   16
         Left            =   120
         TabIndex        =   67
         Top             =   840
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 1"
         Height          =   195
         Index           =   15
         Left            =   120
         TabIndex        =   55
         Top             =   420
         Width           =   1335
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Tape Allocation - Operators use to allocate tape to next Customer"
      Height          =   2115
      Left            =   4020
      TabIndex        =   39
      Top             =   420
      Width           =   5835
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   9
         Left            =   4560
         TabIndex        =   335
         Top             =   360
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   8
         Left            =   2580
         TabIndex        =   171
         Top             =   1560
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   7
         Left            =   2580
         TabIndex        =   157
         Top             =   1140
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   6
         Left            =   2580
         TabIndex        =   49
         Top             =   720
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   5
         Left            =   2580
         TabIndex        =   48
         Top             =   300
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   4
         Left            =   660
         TabIndex        =   47
         Top             =   1560
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   3
         Left            =   660
         TabIndex        =   46
         Top             =   1140
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   2
         Left            =   660
         TabIndex        =   41
         Top             =   720
         Width           =   1155
      End
      Begin VB.TextBox txtAllocateReel 
         Height          =   375
         Index           =   1
         Left            =   660
         TabIndex        =   40
         Top             =   300
         Width           =   1155
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 9"
         Height          =   195
         Index           =   29
         Left            =   3960
         TabIndex        =   336
         Top             =   480
         Width           =   615
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 8"
         Height          =   195
         Index           =   27
         Left            =   2040
         TabIndex        =   172
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 7"
         Height          =   195
         Index           =   24
         Left            =   2040
         TabIndex        =   156
         Top             =   1260
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 6"
         Height          =   195
         Index           =   14
         Left            =   2040
         TabIndex        =   51
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 5"
         Height          =   195
         Index           =   13
         Left            =   2040
         TabIndex        =   50
         Top             =   420
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 4"
         Height          =   195
         Index           =   12
         Left            =   120
         TabIndex        =   45
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 3"
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   44
         Top             =   1260
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 2"
         Height          =   195
         Index           =   10
         Left            =   120
         TabIndex        =   43
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Tape 1"
         Height          =   195
         Index           =   9
         Left            =   120
         TabIndex        =   42
         Top             =   420
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Despatch Details"
      ForeColor       =   &H00000000&
      Height          =   2055
      Left            =   10560
      TabIndex        =   18
      Top             =   1680
      Width           =   3195
      Begin VB.TextBox txtDespatchNumber 
         Height          =   375
         Left            =   1440
         TabIndex        =   144
         Top             =   660
         Width           =   1515
      End
      Begin VB.TextBox txtWaybillNumber 
         Height          =   375
         Left            =   1440
         TabIndex        =   72
         Top             =   1500
         Width           =   1515
      End
      Begin VB.TextBox txtDespatchID 
         Height          =   375
         Left            =   1440
         TabIndex        =   20
         Top             =   240
         Width           =   1515
      End
      Begin VB.TextBox txtDespatchCost 
         Height          =   375
         Left            =   1440
         TabIndex        =   19
         Top             =   1080
         Width           =   1515
      End
      Begin VB.Label lblCaption 
         Caption         =   "Despatch Number"
         Height          =   195
         Index           =   22
         Left            =   120
         TabIndex        =   145
         Top             =   780
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Waybill Number"
         Height          =   195
         Index           =   21
         Left            =   120
         TabIndex        =   73
         Top             =   1620
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Despatch ID"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   22
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label lblCaption 
         Caption         =   "Despatch Cost"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   21
         Top             =   1200
         Width           =   1335
      End
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Despatched but not Contract billed"
      Height          =   255
      Index           =   4
      Left            =   10560
      TabIndex        =   12
      Tag             =   "NOCLEAR"
      Top             =   9180
      Width           =   3195
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Ready to Despatch"
      Height          =   255
      Index           =   3
      Left            =   10560
      TabIndex        =   11
      Tag             =   "NOCLEAR"
      Top             =   8880
      Width           =   3195
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Not Complete"
      Height          =   255
      Index           =   0
      Left            =   10560
      TabIndex        =   10
      Tag             =   "NOCLEAR"
      Top             =   8580
      Value           =   -1  'True
      Width           =   3195
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "Contract Billed, but not Delivery Billed"
      Height          =   255
      Index           =   1
      Left            =   10560
      TabIndex        =   9
      Tag             =   "NOCLEAR"
      Top             =   9480
      Width           =   3195
   End
   Begin VB.OptionButton optComplete 
      Caption         =   "All Items"
      Height          =   255
      Index           =   2
      Left            =   10560
      TabIndex        =   8
      Tag             =   "NOCLEAR"
      Top             =   9780
      Width           =   3195
   End
   Begin VB.CommandButton cmdInsertNew 
      Caption         =   "New Item"
      Height          =   315
      Left            =   2340
      TabIndex        =   7
      Top             =   2220
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filter to Include"
      Height          =   1575
      Left            =   10560
      TabIndex        =   6
      Top             =   0
      Width           =   7845
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVCAM 16:9 PAL"
         Height          =   255
         Index           =   19
         Left            =   1980
         TabIndex        =   332
         Top             =   840
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVC-Pro50 16:9 PAL"
         Height          =   255
         Index           =   27
         Left            =   4080
         TabIndex        =   331
         Top             =   840
         Width           =   1815
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "BetaSX 4:3 PAL"
         Height          =   255
         Index           =   13
         Left            =   6060
         TabIndex        =   330
         Top             =   840
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVC-Pro HD 16:9 NTSC"
         Height          =   255
         Index           =   7
         Left            =   4080
         TabIndex        =   153
         Top             =   540
         Width           =   1995
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "XDCAM HD 16x9 NTSC"
         Height          =   255
         Index           =   25
         Left            =   1980
         TabIndex        =   136
         Top             =   540
         Width           =   2055
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "XDCAM HD 16x9 PAL"
         Height          =   255
         Index           =   24
         Left            =   1980
         TabIndex        =   135
         Top             =   240
         Width           =   2055
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DVC-Pro HD 16:9 PAL"
         Height          =   255
         Index           =   22
         Left            =   4080
         TabIndex        =   134
         Top             =   240
         Width           =   1995
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "All Formats"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   1200
         Value           =   -1  'True
         Width           =   1515
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "BetaSX 16:9 PAL"
         Height          =   255
         Index           =   12
         Left            =   6060
         TabIndex        =   16
         Top             =   540
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "DIGI 16:9 PAL"
         Height          =   255
         Index           =   3
         Left            =   6060
         TabIndex        =   15
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "HDCAM 16:9 NTSC"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   14
         Top             =   540
         Width           =   1695
      End
      Begin VB.OptionButton optSearchFilter 
         Caption         =   "HDCAM 16:9 PAL"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   1695
      End
   End
   Begin MSAdodcLib.Adodc adoCustomers 
      Height          =   330
      Left            =   2220
      Top             =   3240
      Visible         =   0   'False
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   8421631
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoCustomers"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBDropDown ddnSkibblyCustomers 
      Bindings        =   "frmSkibblyTracker.frx":093A
      Height          =   1455
      Left            =   2040
      TabIndex        =   5
      Top             =   3660
      Width           =   4395
      DataFieldList   =   "customername"
      _Version        =   196617
      ColumnHeaders   =   0   'False
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "skibbltcustomerID"
      Columns(0).Name =   "skibblycustomerID"
      Columns(0).DataField=   "skibblycustomerID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   7276
      Columns(1).Caption=   "customername"
      Columns(1).Name =   "customername"
      Columns(1).DataField=   "customername"
      Columns(1).FieldLen=   256
      _ExtentX        =   7752
      _ExtentY        =   2566
      _StockProps     =   77
      DataFieldToDisplay=   "customername"
   End
   Begin VB.Frame frmButtons 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   7200
      TabIndex        =   1
      Top             =   14520
      Width           =   16935
      Begin VB.CommandButton cmdReport 
         Caption         =   "Delivery Waybill Report"
         Height          =   315
         Left            =   4440
         TabIndex        =   75
         Top             =   0
         Width           =   2115
      End
      Begin VB.CommandButton cmdDespatchBillAll 
         Caption         =   "Despatch Bill All Items"
         Height          =   315
         Left            =   6720
         TabIndex        =   74
         Top             =   0
         Visible         =   0   'False
         Width           =   2535
      End
      Begin VB.CommandButton cmdDuplicateGrid 
         Caption         =   "Copy All Items to make New Run"
         Height          =   315
         Left            =   1740
         TabIndex        =   52
         Top             =   0
         Width           =   2535
      End
      Begin VB.CommandButton cmdContractBilling 
         Caption         =   "Contract Billing"
         Height          =   315
         Left            =   11880
         TabIndex        =   25
         Top             =   0
         Visible         =   0   'False
         Width           =   2535
      End
      Begin VB.CommandButton cmdUnAdvanceProcess 
         Caption         =   "Un-Advance Process"
         Height          =   315
         Left            =   9300
         TabIndex        =   24
         Top             =   0
         Visible         =   0   'False
         Width           =   2535
      End
      Begin VB.CommandButton cmdAdvanceProcess 
         Caption         =   "Advance Process"
         Height          =   315
         Left            =   11880
         TabIndex        =   23
         Top             =   0
         Visible         =   0   'False
         Width           =   2535
      End
      Begin VB.CommandButton cmdClose 
         Caption         =   "Close"
         Height          =   315
         Left            =   15720
         TabIndex        =   3
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdSearch 
         Caption         =   "Search (F5)"
         Height          =   315
         Left            =   14460
         TabIndex        =   2
         Top             =   0
         Width           =   1215
      End
   End
   Begin MSAdodcLib.Adodc adoItems 
      Height          =   330
      Left            =   60
      Top             =   2220
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "adoItems"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBGrid grdItems 
      Bindings        =   "frmSkibblyTracker.frx":0955
      Height          =   9915
      Left            =   60
      TabIndex        =   0
      Top             =   2640
      Width           =   10365
      _Version        =   196617
      stylesets.count =   3
      stylesets(0).Name=   "conclusionfield"
      stylesets(0).ForeColor=   0
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSkibblyTracker.frx":096C
      stylesets(1).Name=   "stagefield"
      stylesets(1).ForeColor=   0
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSkibblyTracker.frx":0988
      stylesets(2).Name=   "headerfield"
      stylesets(2).ForeColor=   0
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSkibblyTracker.frx":09A4
      AllowDelete     =   -1  'True
      SelectTypeRow   =   3
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   11
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "skibblytrackerID"
      Columns(0).Name =   "skibblytrackerID"
      Columns(0).DataField=   "skibblytrackerID"
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "skibblycustomerID"
      Columns(1).Name =   "skibblycustomerID"
      Columns(1).DataField=   "skibblycustomerID"
      Columns(1).FieldLen=   256
      Columns(2).Width=   4419
      Columns(2).Caption=   "skibblycustomer"
      Columns(2).Name =   "skibblycustomer"
      Columns(2).DataField=   "skibblycustomername"
      Columns(2).FieldLen=   256
      Columns(2).StyleSet=   "headerfield"
      Columns(3).Width=   4419
      Columns(3).Caption=   "title"
      Columns(3).Name =   "title"
      Columns(3).DataField=   "title"
      Columns(3).FieldLen=   256
      Columns(3).StyleSet=   "headerfield"
      Columns(4).Width=   1667
      Columns(4).Caption=   "Rdy Desp."
      Columns(4).Name =   "readytodespatch"
      Columns(4).Alignment=   2
      Columns(4).DataField=   "readytodespatch"
      Columns(4).FieldLen=   256
      Columns(4).Locked=   -1  'True
      Columns(4).Style=   2
      Columns(4).StyleSet=   "conclusionfield"
      Columns(5).Width=   1667
      Columns(5).Caption=   "Rdy Co. Bill"
      Columns(5).Name =   "readytobillcontract"
      Columns(5).Alignment=   2
      Columns(5).DataField=   "readytobillcontract"
      Columns(5).FieldLen=   256
      Columns(5).Locked=   -1  'True
      Columns(5).Style=   2
      Columns(5).StyleSet=   "conclusionfield"
      Columns(6).Width=   1667
      Columns(6).Caption=   "Co. Billed"
      Columns(6).Name =   "contractbilled"
      Columns(6).Alignment=   2
      Columns(6).DataField=   "contractbilled"
      Columns(6).FieldLen=   256
      Columns(6).Locked=   -1  'True
      Columns(6).Style=   2
      Columns(6).StyleSet=   "conclusionfield"
      Columns(7).Width=   1773
      Columns(7).Caption=   "Rdy Del. Bill"
      Columns(7).Name =   "readytobilldelivery"
      Columns(7).Alignment=   2
      Columns(7).DataField=   "readytobilldelivery"
      Columns(7).FieldLen=   256
      Columns(7).Locked=   -1  'True
      Columns(7).Style=   2
      Columns(7).StyleSet=   "conclusionfield"
      Columns(8).Width=   1667
      Columns(8).Caption=   "Del. Billed"
      Columns(8).Name =   "deliverybilled"
      Columns(8).Alignment=   2
      Columns(8).DataField=   "deliverybilled"
      Columns(8).FieldLen=   256
      Columns(8).Locked=   -1  'True
      Columns(8).Style=   2
      Columns(8).StyleSet=   "conclusionfield"
      Columns(9).Width=   3200
      Columns(9).Visible=   0   'False
      Columns(9).Caption=   "despatchID"
      Columns(9).Name =   "despatchID"
      Columns(9).DataField=   "despatchID"
      Columns(9).FieldLen=   256
      Columns(10).Width=   3200
      Columns(10).Visible=   0   'False
      Columns(10).Caption=   "despatchcost"
      Columns(10).Name=   "despatchcost"
      Columns(10).DataField=   "despatchcost"
      Columns(10).FieldLen=   256
      _ExtentX        =   18283
      _ExtentY        =   17489
      _StockProps     =   79
      Caption         =   "Tracker Items"
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtTitleSearch 
      Height          =   315
      Left            =   5160
      TabIndex        =   176
      ToolTipText     =   "The company this tape belongs to"
      Top             =   60
      Width           =   4335
      DataFieldList   =   "title"
      _Version        =   196617
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColorOdd    =   16761087
      RowHeight       =   423
      Columns(0).Width=   8149
      Columns(0).Caption=   "title"
      Columns(0).Name =   "title"
      Columns(0).DataField=   "title"
      Columns(0).FieldLen=   256
      _ExtentX        =   7646
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   12632319
      DataFieldToDisplay=   "title"
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   9
      Left            =   3360
      TabIndex        =   338
      Top             =   1500
      Width           =   555
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 9 left to do"
      Height          =   195
      Index           =   30
      Left            =   2100
      TabIndex        =   337
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 8 left to do"
      Height          =   195
      Index           =   26
      Left            =   2100
      TabIndex        =   170
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   8
      Left            =   3360
      TabIndex        =   169
      Top             =   1140
      Width           =   555
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   7
      Left            =   3360
      TabIndex        =   155
      Top             =   780
      Width           =   555
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 7 left to do"
      Height          =   195
      Index           =   23
      Left            =   2100
      TabIndex        =   154
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Search Title"
      Height          =   195
      Index           =   8
      Left            =   4260
      TabIndex        =   38
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 6 left to do"
      Height          =   195
      Index           =   7
      Left            =   2100
      TabIndex        =   37
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 5 left to do"
      Height          =   195
      Index           =   6
      Left            =   2100
      TabIndex        =   36
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 4 left to do"
      Height          =   195
      Index           =   5
      Left            =   120
      TabIndex        =   35
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 3 left to do"
      Height          =   195
      Index           =   4
      Left            =   120
      TabIndex        =   34
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 2 left to do"
      Height          =   195
      Index           =   3
      Left            =   120
      TabIndex        =   33
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblCaption 
      Caption         =   "Tape 1 left to do"
      Height          =   195
      Index           =   2
      Left            =   120
      TabIndex        =   32
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   1
      Left            =   1440
      TabIndex        =   31
      Top             =   60
      Width           =   555
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   2
      Left            =   1440
      TabIndex        =   30
      Top             =   420
      Width           =   555
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   3
      Left            =   1440
      TabIndex        =   29
      Top             =   780
      Width           =   555
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   6
      Left            =   3360
      TabIndex        =   28
      Top             =   420
      Width           =   555
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   5
      Left            =   3360
      TabIndex        =   27
      Top             =   60
      Width           =   555
   End
   Begin VB.Label lblTapeRemaining 
      BorderStyle     =   1  'Fixed Single
      Height          =   315
      Index           =   4
      Left            =   1440
      TabIndex        =   26
      Top             =   1140
      Width           =   555
   End
   Begin VB.Label lblTrackerID 
      BackColor       =   &H0080FFFF&
      Height          =   315
      Left            =   10560
      TabIndex        =   4
      Top             =   10200
      Width           =   975
   End
End
Attribute VB_Name = "frmSkibblyTracker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_strFormat As String, m_blnSkipMoveComplete As Boolean

Private Sub adoItems_MoveComplete(ByVal adReason As ADODB.EventReasonEnum, ByVal pError As ADODB.Error, adStatus As ADODB.EventStatusEnum, ByVal pRecordset As ADODB.Recordset)

Dim l_str As String, l_rsTemp As ADODB.Recordset

If m_blnSkipMoveComplete = True Then Exit Sub

If adoItems.Recordset.EOF = True Or adoItems.Recordset.BOF Then
    adoItems.Caption = adoItems.Recordset.RecordCount & " items"
    Exit Sub
Else
    If adoItems.Recordset("skibblytrackerID") = "" Then
        Exit Sub
    End If
End If

adoItems.Caption = adoItems.Recordset.RecordCount & " items"
l_str = Trim(" " & adoItems.Recordset("title"))
'If l_str <> "" Then lblTitle.Caption = l_str & " for " & GetData("skibblycustomer", "customername", "skibblycustomerID", adoItems.Recordset("skibblycustomerID"))

lblTrackerID.Caption = adoItems.Recordset("skibblytrackerID")
Set l_rsTemp = ExecuteSQL("SELECT * FROM skibblytracker WHERE skibblytrackerID = " & Val(lblTrackerID.Caption) & ";", g_strExecuteError)
CheckForSQLError

txtDespatchID.Text = Trim(" " & l_rsTemp("despatchID"))
If Val(txtDespatchID.Text) <> 0 Then txtDespatchNumber.Text = GetData("despatch", "despatchnumber", "despatchID", Val(txtDespatchID.Text))
txtDespatchCost.Text = Trim(" " & l_rsTemp("despatchcost"))
txtWaybillNumber.Text = Trim(" " & l_rsTemp("waybillnumber"))

chkPal16x9hdcam1.Value = Val(" " & l_rsTemp("pal16x9hdcam1"))
chkPal16x9hdcam2.Value = Val(" " & l_rsTemp("pal16x9hdcam2"))
chkPal16x9hdcam3.Value = Val(" " & l_rsTemp("pal16x9hdcam3"))
chkPal16x9hdcam4.Value = Val(" " & l_rsTemp("pal16x9hdcam4"))
chkPal16x9hdcam5.Value = Val(" " & l_rsTemp("pal16x9hdcam5"))
txtPal16x9hdcam1barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcam1barcode"))
txtPal16x9hdcam2barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcam2barcode"))
txtPal16x9hdcam3barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcam3barcode"))
txtPal16x9hdcam4barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcam4barcode"))
txtPal16x9hdcam5barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcam5barcode"))

chkPal16x9hdcamSR1.Value = Val(" " & l_rsTemp("pal16x9hdcamsr1"))
chkPal16x9hdcamSR2.Value = Val(" " & l_rsTemp("pal16x9hdcamsr2"))
chkPal16x9hdcamSR3.Value = Val(" " & l_rsTemp("pal16x9hdcamsr3"))
txtPal16x9hdcamSR1barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcamsr1barcode"))
txtPal16x9hdcamSR2barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcamsr2barcode"))
txtPal16x9hdcamSR3barcode.Text = Trim(" " & l_rsTemp("pal16x9hdcamsr3barcode"))

chkNTSC16x9hdcam1.Value = Val(" " & l_rsTemp("ntsc16x9hdcam1"))
chkNTSC16x9hdcam2.Value = Val(" " & l_rsTemp("ntsc16x9hdcam2"))
chkNTSC16x9hdcam3.Value = Val(" " & l_rsTemp("ntsc16x9hdcam3"))
txtNTSC16x9hdcam1barcode.Text = Trim(" " & l_rsTemp("ntsc16x9hdcam1barcode"))
txtNTSC16x9hdcam2barcode.Text = Trim(" " & l_rsTemp("ntsc16x9hdcam2barcode"))
txtNTSC16x9hdcam3barcode.Text = Trim(" " & l_rsTemp("ntsc16x9hdcam3barcode"))

chkPAL16x9DVCProHD1.Value = Val(" " & l_rsTemp("pal16x9DVCProHD1"))
chkPAL16x9DVCProHD2.Value = Val(" " & l_rsTemp("pal16x9DVCProHD2"))
chkPAL16x9DVCProHD3.Value = Val(" " & l_rsTemp("pal16x9DVCProHD3"))
chkPAL16x9DVCProHD4.Value = Val(" " & l_rsTemp("pal16x9DVCProHD4"))
chkPAL16x9DVCProHD5.Value = Val(" " & l_rsTemp("pal16x9DVCProHD5"))
chkPAL16x9DVCProHD6.Value = Val(" " & l_rsTemp("pal16x9DVCProHD6"))
chkPAL16x9DVCProHD7.Value = Val(" " & l_rsTemp("pal16x9DVCProHD7"))
txtPAL16x9DVCProHD1barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD1barcode"))
txtPAL16x9DVCProHD2barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD2barcode"))
txtPAL16x9DVCProHD3barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD3barcode"))
txtPAL16x9DVCProHD4barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD4barcode"))
txtPAL16x9DVCProHD5barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD5barcode"))
txtPAL16x9DVCProHD6barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD6barcode"))
txtPAL16x9DVCProHD7barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCProHD7barcode"))

chkNTSC16x9DVCProHD1.Value = Val(" " & l_rsTemp("ntsc16x9DVCProHD1"))
chkNTSC16x9DVCProHD2.Value = Val(" " & l_rsTemp("ntsc16x9DVCProHD2"))
chkNTSC16x9DVCProHD3.Value = Val(" " & l_rsTemp("ntsc16x9DVCProHD3"))
chkNTSC16x9DVCProHD4.Value = Val(" " & l_rsTemp("ntsc16x9DVCProHD4"))
chkNTSC16x9DVCProHD5.Value = Val(" " & l_rsTemp("ntsc16x9DVCProHD5"))
chkNTSC16x9DVCProHD6.Value = Val(" " & l_rsTemp("ntsc16x9DVCProHD6"))
txtNTSC16x9DVCProHD1barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCProHD1barcode"))
txtNTSC16x9DVCProHD2barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCProHD2barcode"))
txtNTSC16x9DVCProHD3barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCProHD3barcode"))
txtNTSC16x9DVCProHD4barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCProHD4barcode"))
txtNTSC16x9DVCProHD5barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCProHD5barcode"))
txtNTSC16x9DVCProHD6barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCProHD6barcode"))

chkPAL16x9DVCamHD1.Value = Val(" " & l_rsTemp("pal16x9DVCamHD1"))
chkPAL16x9DVCamHD2.Value = Val(" " & l_rsTemp("pal16x9DVCamHD2"))
chkPAL16x9DVCamHD3.Value = Val(" " & l_rsTemp("pal16x9DVCamHD3"))
chkPAL16x9DVCamHD4.Value = Val(" " & l_rsTemp("pal16x9DVCamHD4"))
txtPAL16x9DVCamHD1barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCamHD1barcode"))
txtPAL16x9DVCamHD2barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCamHD2barcode"))
txtPAL16x9DVCamHD3barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCamHD3barcode"))
txtPAL16x9DVCamHD4barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCamHD4barcode"))

chkPAL16x9XDCAMHD1.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD1"))
chkPAL16x9XDCAMHD2.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD2"))
chkPAL16x9XDCAMHD3.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD3"))
chkPAL16x9XDCAMHD4.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD4"))
chkPAL16x9XDCAMHD5.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD5"))
chkPAL16x9XDCAMHD6.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD6"))
chkPAL16x9XDCAMHD7.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD7"))
chkPAL16x9XDCAMHD8.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD8"))
chkPAL16x9XDCAMHD9.Value = Val(" " & l_rsTemp("PAL16x9XDCAMHD9"))
txtPAL16x9XDCAMHD1barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD1barcode"))
txtPAL16x9XDCAMHD2barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD2barcode"))
txtPAL16x9XDCAMHD3barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD3barcode"))
txtPAL16x9XDCAMHD4barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD4barcode"))
txtPAL16x9XDCAMHD5barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD5barcode"))
txtPAL16x9XDCAMHD6barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD6barcode"))
txtPAL16x9XDCAMHD7barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD7barcode"))
txtPAL16x9XDCAMHD8barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD8barcode"))
txtPAL16x9XDCAMHD9barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAMHD9barcode"))

chkntsc16x9XDCAMHD1.Value = Val(" " & l_rsTemp("NTSC16x9XDCAMHD1"))
chkntsc16x9XDCAMHD2.Value = Val(" " & l_rsTemp("NTSC16x9XDCAMHD2"))
chkntsc16x9XDCAMHD3.Value = Val(" " & l_rsTemp("NTSC16x9XDCAMHD3"))
chkntsc16x9XDCAMHD4.Value = Val(" " & l_rsTemp("NTSC16x9XDCAMHD4"))
chkntsc16x9XDCAMHD5.Value = Val(" " & l_rsTemp("NTSC16x9XDCAMHD5"))
chkntsc16x9XDCAMHD6.Value = Val(" " & l_rsTemp("NTSC16x9XDCAMHD6"))
txtntsc16x9XDCAMHD1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAMHD1barcode"))
txtntsc16x9XDCAMHD2barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAMHD2barcode"))
txtntsc16x9XDCAMHD3barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAMHD3barcode"))
txtntsc16x9XDCAMHD4barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAMHD4barcode"))
txtntsc16x9XDCAMHD5barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAMHD5barcode"))
txtntsc16x9XDCAMHD6barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAMHD6barcode"))

chkPAL16x9Digi1.Value = Val(" " & l_rsTemp("pal16x9digi1"))
chkPAL16x9Digi2.Value = Val(" " & l_rsTemp("pal16x9digi2"))
chkPAL16x9Digi3.Value = Val(" " & l_rsTemp("pal16x9digi3"))
chkPAL16x9Digi4.Value = Val(" " & l_rsTemp("pal16x9digi4"))
chkPAL16x9Digi5.Value = Val(" " & l_rsTemp("pal16x9digi5"))
txtPAL16x9Digi1barcode.Text = Trim(" " & l_rsTemp("pal16x9digi1barcode"))
txtPAL16x9Digi2barcode.Text = Trim(" " & l_rsTemp("pal16x9digi2barcode"))
txtPAL16x9Digi3barcode.Text = Trim(" " & l_rsTemp("pal16x9digi3barcode"))
txtPAL16x9Digi4barcode.Text = Trim(" " & l_rsTemp("pal16x9digi4barcode"))
txtPAL16x9Digi5barcode.Text = Trim(" " & l_rsTemp("pal16x9digi5barcode"))
chkNTSC16x9Digi1.Value = Val(" " & l_rsTemp("ntsc16x9digi1"))
chkNTSC16x9Digi2.Value = Val(" " & l_rsTemp("ntsc16x9digi2"))
chkNTSC16x9Digi3.Value = Val(" " & l_rsTemp("ntsc16x9digi3"))
txtNTSC16x9Digi1barcode.Text = Trim(" " & l_rsTemp("ntsc16x9digi1barcode"))
txtNTSC16x9Digi2barcode.Text = Trim(" " & l_rsTemp("ntsc16x9digi2barcode"))
txtNTSC16x9Digi3barcode.Text = Trim(" " & l_rsTemp("ntsc16x9digi3barcode"))

chkPAL4x3Digi1.Value = Val(" " & l_rsTemp("pal4x3digi1"))
chkPAL4x3Digi2.Value = Val(" " & l_rsTemp("pal4x3digi2"))
chkPAL4x3Digi3.Value = Val(" " & l_rsTemp("pal4x3digi3"))
chkPAL4x3Digi4.Value = Val(" " & l_rsTemp("pal4x3digi4"))
txtPAL4x3Digi1barcode.Text = Trim(" " & l_rsTemp("pal4x3digi1barcode"))
txtPAL4x3Digi2barcode.Text = Trim(" " & l_rsTemp("pal4x3digi2barcode"))
txtPAL4x3Digi3barcode.Text = Trim(" " & l_rsTemp("pal4x3digi3barcode"))
txtPAL4x3Digi4barcode.Text = Trim(" " & l_rsTemp("pal4x3digi4barcode"))
'chkNTSC4x3Digi1.Value = Val(" " & l_rsTemp("ntsc4x3digi1"))
'chkNTSC4x3Digi2.Value = Val(" " & l_rsTemp("ntsc4x3digi2"))
'chkNTSC4x3Digi3.Value = Val(" " & l_rsTemp("ntsc4x3digi3"))
'txtNTSC4x3Digi1barcode.Text = Trim(" " & l_rsTemp("ntsc4x3digi1barcode"))
'txtNTSC4x3Digi2barcode.Text = Trim(" " & l_rsTemp("ntsc4x3digi2barcode"))
'txtNTSC4x3Digi3barcode.Text = Trim(" " & l_rsTemp("ntsc4x3digi3barcode"))

chkPAL16x9DVCam1.Value = Val(" " & l_rsTemp("pal16x9DVCam1"))
chkPAL16x9DVCam2.Value = Val(" " & l_rsTemp("pal16x9DVCam2"))
chkPAL16x9DVCam3.Value = Val(" " & l_rsTemp("pal16x9DVCam3"))
chkPAL16x9DVCam4.Value = Val(" " & l_rsTemp("pal16x9DVCam4"))
chkPAL16x9DVCam5.Value = Val(" " & l_rsTemp("pal16x9DVCam5"))
txtPAL16x9DVCam1barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCam1barcode"))
txtPAL16x9DVCam2barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCam2barcode"))
txtPAL16x9DVCam3barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCam3barcode"))
txtPAL16x9DVCam4barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCam4barcode"))
txtPAL16x9DVCam5barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCam5barcode"))

chkPAL4x3DVCam1.Value = Val(" " & l_rsTemp("pal4x3DVCam1"))
chkPAL4x3DVCam2.Value = Val(" " & l_rsTemp("pal4x3DVCam2"))
chkPAL4x3DVCam3.Value = Val(" " & l_rsTemp("pal4x3DVCam3"))
chkPAL4x3DVCam4.Value = Val(" " & l_rsTemp("pal4x3DVCam4"))
txtPAL4x3DVCam1barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCam1barcode"))
txtPAL4x3DVCam2barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCam2barcode"))
txtPAL4x3DVCam3barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCam3barcode"))
txtPAL4x3DVCam4barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCam4barcode"))

chkNTSC4x3DVCam1.Value = Val(" " & l_rsTemp("ntsc4x3DVCam1"))
chkNTSC4x3DVCam2.Value = Val(" " & l_rsTemp("ntsc4x3DVCam2"))
chkNTSC4x3DVCam3.Value = Val(" " & l_rsTemp("ntsc4x3DVCam3"))
txtNTSC4x3DVCam1barcode.Text = Trim(" " & l_rsTemp("ntsc4x3DVCam1barcode"))
txtNTSC4x3DVCam2barcode.Text = Trim(" " & l_rsTemp("ntsc4x3DVCam2barcode"))
txtNTSC4x3DVCam3barcode.Text = Trim(" " & l_rsTemp("ntsc4x3DVCam3barcode"))
chkNTSC16x9DVCam1.Value = Val(" " & l_rsTemp("ntsc16x9DVCam1"))
chkNTSC16x9DVCam2.Value = Val(" " & l_rsTemp("ntsc16x9DVCam2"))
chkNTSC16x9DVCam3.Value = Val(" " & l_rsTemp("ntsc16x9DVCam3"))
txtNTSC16x9DVCam1barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCam1barcode"))
txtNTSC16x9DVCam2barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCam2barcode"))
txtNTSC16x9DVCam3barcode.Text = Trim(" " & l_rsTemp("ntsc16x9DVCam3barcode"))

chkPAL16x9BetaSX1.Value = Val(" " & l_rsTemp("pal16x9betasx1"))
chkPAL16x9BetaSX2.Value = Val(" " & l_rsTemp("pal16x9betasx2"))
chkPAL16x9BetaSX3.Value = Val(" " & l_rsTemp("pal16x9betasx3"))
chkPAL16x9BetaSX4.Value = Val(" " & l_rsTemp("pal16x9betasx4"))
chkPAL16x9BetaSX5.Value = Val(" " & l_rsTemp("pal16x9betasx5"))
txtPAL16x9BetaSX1barcode.Text = Trim(" " & l_rsTemp("pal16x9betasx1barcode"))
txtPAL16x9BetaSX2barcode.Text = Trim(" " & l_rsTemp("pal16x9betasx2barcode"))
txtPAL16x9BetaSX3barcode.Text = Trim(" " & l_rsTemp("pal16x9betasx3barcode"))
txtPAL16x9BetaSX4barcode.Text = Trim(" " & l_rsTemp("pal16x9betasx4barcode"))
txtPAL16x9BetaSX5barcode.Text = Trim(" " & l_rsTemp("pal16x9betasx5barcode"))
chkPAL4x3BetaSX1.Value = Val(" " & l_rsTemp("pal4x3betasx1"))
chkPAL4x3BetaSX2.Value = Val(" " & l_rsTemp("pal4x3betasx2"))
chkPAL4x3BetaSX3.Value = Val(" " & l_rsTemp("pal4x3betasx3"))
chkPAL4x3BetaSX4.Value = Val(" " & l_rsTemp("pal4x3betasx4"))
chkPAL4x3BetaSX5.Value = Val(" " & l_rsTemp("pal4x3betasx5"))
txtPAL4x3BetaSX1barcode.Text = Trim(" " & l_rsTemp("pal4x3betasx1barcode"))
txtPAL4x3BetaSX2barcode.Text = Trim(" " & l_rsTemp("pal4x3betasx2barcode"))
txtPAL4x3BetaSX3barcode.Text = Trim(" " & l_rsTemp("pal4x3betasx3barcode"))
txtPAL4x3BetaSX4barcode.Text = Trim(" " & l_rsTemp("pal4x3betasx4barcode"))
txtPAL4x3BetaSX5barcode.Text = Trim(" " & l_rsTemp("pal4x3betasx5barcode"))

chkNTSC4x3BetaSX1.Value = Val(" " & l_rsTemp("NTSC4x3betasx1"))
chkNTSC4x3BetaSX2.Value = Val(" " & l_rsTemp("NTSC4x3betasx2"))
chkNTSC4x3BetaSX3.Value = Val(" " & l_rsTemp("NTSC4x3betasx3"))
txtNTSC4x3BetaSX1barcode.Text = Trim(" " & l_rsTemp("NTSC4x3betasx1barcode"))
txtNTSC4x3BetaSX2barcode.Text = Trim(" " & l_rsTemp("NTSC4x3betasx2barcode"))
txtNTSC4x3BetaSX3barcode.Text = Trim(" " & l_rsTemp("NTSC4x3betasx3barcode"))
'chkNTSC16x9BetaSX1.Value = Val(" " & l_rsTemp("NTSC16x9BetaSX1"))
'chkNTSC16x9BetaSX2.Value = Val(" " & l_rsTemp("NTSC16x9BetaSX2"))
'chkNTSC16x9BetaSX3.Value = Val(" " & l_rsTemp("NTSC16x9BetaSX3"))
'txtNTSC16x9BetaSX1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9BetaSX1barcode"))
'txtNTSC16x9BetaSX2barcode.Text = Trim(" " & l_rsTemp("NTSC16x9BetaSX2barcode"))
'txtNTSC16x9BetaSX3barcode.Text = Trim(" " & l_rsTemp("NTSC16x9BetaSX3barcode"))

chkPAL16x9DVCPro1.Value = Val(" " & l_rsTemp("pal16x9DVCPRO1"))
chkPAL16x9DVCPro2.Value = Val(" " & l_rsTemp("pal16x9DVCPRO2"))
chkPAL16x9DVCPro3.Value = Val(" " & l_rsTemp("pal16x9DVCPRO3"))
chkPAL16x9DVCPro4.Value = Val(" " & l_rsTemp("pal16x9DVCPRO4"))
txtPAL16x9DVCPro1barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO1barcode"))
txtPAL16x9DVCPro2barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO2barcode"))
txtPAL16x9DVCPro3barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO3barcode"))
txtPAL16x9DVCPro4barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO4barcode"))

chkPAL16x9DVCPro501.Value = Val(" " & l_rsTemp("pal16x9DVCPRO501"))
chkPAL16x9DVCPro502.Value = Val(" " & l_rsTemp("pal16x9DVCPRO502"))
chkPAL16x9DVCPro503.Value = Val(" " & l_rsTemp("pal16x9DVCPRO503"))
chkPAL16x9DVCPro504.Value = Val(" " & l_rsTemp("pal16x9DVCPRO504"))
chkPAL16x9DVCPro505.Value = Val(" " & l_rsTemp("pal16x9DVCPRO505"))
chkPAL16x9DVCPro506.Value = Val(" " & l_rsTemp("pal16x9DVCPRO506"))
chkPAL16x9DVCPro507.Value = Val(" " & l_rsTemp("pal16x9DVCPRO507"))
chkPAL16x9DVCPro508.Value = Val(" " & l_rsTemp("pal16x9DVCPRO508"))
chkPAL16x9DVCPro509.Value = Val(" " & l_rsTemp("pal16x9DVCPRO509"))
txtPAL16x9DVCPro501barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO501barcode"))
txtPAL16x9DVCPro502barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO502barcode"))
txtPAL16x9DVCPro503barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO503barcode"))
txtPAL16x9DVCPro504barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO504barcode"))
txtPAL16x9DVCPro505barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO505barcode"))
txtPAL16x9DVCPro506barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO506barcode"))
txtPAL16x9DVCPro507barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO507barcode"))
txtPAL16x9DVCPro508barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO508barcode"))
txtPAL16x9DVCPro509barcode.Text = Trim(" " & l_rsTemp("pal16x9DVCPRO509barcode"))

chkPAL4x3DVCPro1.Value = Val(" " & l_rsTemp("pal4x3DVCPRO1"))
chkPAL4x3DVCPro2.Value = Val(" " & l_rsTemp("pal4x3DVCPRO2"))
chkPAL4x3DVCPro3.Value = Val(" " & l_rsTemp("pal4x3DVCPRO3"))
chkPAL4x3DVCPro4.Value = Val(" " & l_rsTemp("pal4x3DVCPRO4"))
txtPAL4x3DVCPro1barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCPRO1barcode"))
txtPAL4x3DVCPro2barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCPRO2barcode"))
txtPAL4x3DVCPro3barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCPRO3barcode"))
txtPAL4x3DVCPro4barcode.Text = Trim(" " & l_rsTemp("pal4x3DVCPRO4barcode"))

chkPAL16x9XDCAM1.Value = Val(" " & l_rsTemp("PAL16x9XDCAM1"))
chkPAL16x9XDCAM2.Value = Val(" " & l_rsTemp("PAL16x9XDCAM2"))
chkPAL16x9XDCAM3.Value = Val(" " & l_rsTemp("PAL16x9XDCAM3"))
chkPAL16x9XDCAM4.Value = Val(" " & l_rsTemp("PAL16x9XDCAM4"))
chkPAL16x9XDCAM5.Value = Val(" " & l_rsTemp("PAL16x9XDCAM5"))
chkPAL16x9XDCAM6.Value = Val(" " & l_rsTemp("PAL16x9XDCAM6"))
chkPAL16x9XDCAM7.Value = Val(" " & l_rsTemp("PAL16x9XDCAM7"))
txtPAL16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM1barcode"))
txtPAL16x9XDCAM2barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM2barcode"))
txtPAL16x9XDCAM3barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM3barcode"))
txtPAL16x9XDCAM4barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM4barcode"))
txtPAL16x9XDCAM5barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM5barcode"))
txtPAL16x9XDCAM6barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM6barcode"))
txtPAL16x9XDCAM7barcode.Text = Trim(" " & l_rsTemp("PAL16x9XDCAM7barcode"))

'chkNTSC16x9XDCAM1.Value = Val(" " & l_rsTemp("NTSC16x9XDCAM1"))
'chkNTSC16x9XDCAM2.Value = Val(" " & l_rsTemp("NTSC16x9XDCAM2"))
'chkNTSC16x9XDCAM3.Value = Val(" " & l_rsTemp("NTSC16x9XDCAM3"))
'chkNTSC16x9XDCAM4.Value = Val(" " & l_rsTemp("NTSC16x9XDCAM4"))
'chkNTSC16x9XDCAM5.Value = Val(" " & l_rsTemp("NTSC16x9XDCAM5"))
'chkNTSC16x9XDCAM6.Value = Val(" " & l_rsTemp("NTSC16x9XDCAM6"))
'txtNTSC16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAM1barcode"))
'txtNTSC16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAM2barcode"))
'txtNTSC16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAM3barcode"))
'txtNTSC16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAM4barcode"))
'txtNTSC16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAM5barcode"))
'txtNTSC16x9XDCAM1barcode.Text = Trim(" " & l_rsTemp("NTSC16x9XDCAM6barcode"))

l_rsTemp.Close
Set l_rsTemp = Nothing

RefreshWithBookmark2

End Sub

Private Sub chkNTSC16x9DVCProHD1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCPROHD1", chkNTSC16x9DVCProHD1) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCProHD2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCPROHD2", chkNTSC16x9DVCProHD2) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCProHD3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCPROHD3", chkNTSC16x9DVCProHD3) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCProHD4_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCPROHD4", chkNTSC16x9DVCProHD4) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCProHD5_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCPROHD5", chkNTSC16x9DVCProHD5) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCProHD6_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCPROHD6", chkNTSC16x9DVCProHD6) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCam1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCam1", chkNTSC16x9DVCam1) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCam2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCam2", chkNTSC16x9DVCam2) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9DVCam3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9DVCam3", chkNTSC16x9DVCam3) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9hdcam3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9hdcam3", chkNTSC16x9hdcam3) = False Then Cancel = True

End Sub

'Private Sub chkNTSC16x9XDCAM1_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9XDCAM1", chkNTSC16x9XDCAM1) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9XDCAM2_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9XDCAM2", chkNTSC16x9XDCAM2) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9XDCAM3_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9XDCAM3", chkNTSC16x9XDCAM3) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9XDCAM4_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9XDCAM4", chkNTSC16x9XDCAM4) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9XDCAM5_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9XDCAM5", chkNTSC16x9XDCAM5) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9XDCAM6_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9XDCAM6", chkNTSC16x9XDCAM6) = False Then Cancel = True
'
'End Sub

Private Sub chkntsc16x9XDCAMHD1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9XDCAMHD1", chkntsc16x9XDCAMHD1) = False Then Cancel = True

End Sub

Private Sub chkntsc16x9XDCAMHD2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9XDCAMHD2", chkntsc16x9XDCAMHD2) = False Then Cancel = True

End Sub

Private Sub chkntsc16x9XDCAMHD3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9XDCAMHD3", chkntsc16x9XDCAMHD3) = False Then Cancel = True

End Sub

Private Sub chkntsc16x9XDCAMHD4_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9XDCAMHD4", chkntsc16x9XDCAMHD4) = False Then Cancel = True

End Sub

Private Sub chkntsc16x9XDCAMHD5_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9XDCAMHD5", chkntsc16x9XDCAMHD5) = False Then Cancel = True

End Sub

Private Sub chkntsc16x9XDCAMHD6_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9XDCAMHD6", chkntsc16x9XDCAMHD6) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9Digi1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9Digi1", chkNTSC16x9Digi1) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9Digi2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9Digi2", chkNTSC16x9Digi2) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9Digi3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9Digi3", chkNTSC16x9Digi3) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9hdcam1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9hdcam1", chkNTSC16x9hdcam1) = False Then Cancel = True

End Sub

Private Sub chkNTSC16x9hdcam2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC16x9hdcam2", chkNTSC16x9hdcam2) = False Then Cancel = True

End Sub

Private Sub chkNTSC4x3BetaSX1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC4x3BetaSX1", chkNTSC4x3BetaSX1) = False Then Cancel = True

End Sub

Private Sub chkNTSC4x3BetaSX2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC4x3BetaSX2", chkNTSC4x3BetaSX2) = False Then Cancel = True

End Sub

Private Sub chkNTSC4x3BetaSX3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC4x3BetaSX3", chkNTSC4x3BetaSX3) = False Then Cancel = True

End Sub

'Private Sub chkNTSC4x3Digi1_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC4x3Digi1", chkNTSC4x3Digi1) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC4x3Digi2_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC4x3Digi2", chkNTSC4x3Digi2) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC4x3Digi3_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC4x3Digi3", chkNTSC4x3Digi3) = False Then Cancel = True
'
'End Sub

Private Sub chkNTSC4x3DVCam1_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC4x3DVCam1", chkNTSC4x3DVCam1) = False Then Cancel = True

End Sub

Private Sub chkNTSC4x3DVCam2_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC4x3DVCam2", chkNTSC4x3DVCam2) = False Then Cancel = True

End Sub

Private Sub chkNTSC4x3DVCam3_Validate(Cancel As Boolean)

If CheckboxValidate("NTSC4x3DVCam3", chkNTSC4x3DVCam3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9BetaSX1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9BetaSx1", chkPAL16x9BetaSX1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9BetaSX2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9BetaSx2", chkPAL16x9BetaSX2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9BetaSX3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9BetaSx3", chkPAL16x9BetaSX3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9BetaSX4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9BetaSx4", chkPAL16x9BetaSX4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9BetaSX5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9BetaSx5", chkPAL16x9BetaSX5) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9Digi1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9Digi1", chkPAL16x9Digi1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9Digi2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9Digi2", chkPAL16x9Digi2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9Digi3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9Digi3", chkPAL16x9Digi3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9Digi4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9Digi4", chkPAL16x9Digi4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9Digi5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9Digi5", chkPAL16x9Digi5) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCam4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCam4", chkPAL16x9DVCam4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCam5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCam5", chkPAL16x9DVCam5) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCamHD1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9dvcamHD1", chkPAL16x9DVCamHD1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCamHD2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9dvcamHD2", chkPAL16x9DVCamHD2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCamHD3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9dvcamHD3", chkPAL16x9DVCamHD3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCamHD4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9dvcamHD4", chkPAL16x9DVCamHD4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO1", chkPAL16x9DVCPro1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO2", chkPAL16x9DVCPro2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO3", chkPAL16x9DVCPro3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO4", chkPAL16x9DVCPro4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro501_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO501", chkPAL16x9DVCPro501) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro502_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO502", chkPAL16x9DVCPro502) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro503_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO503", chkPAL16x9DVCPro503) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro504_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO504", chkPAL16x9DVCPro504) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro505_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO505", chkPAL16x9DVCPro505) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro506_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO506", chkPAL16x9DVCPro506) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro507_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO507", chkPAL16x9DVCPro507) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro508_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO508", chkPAL16x9DVCPro508) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCPro509_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPRO509", chkPAL16x9DVCPro509) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD1", chkPAL16x9DVCProHD1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD2", chkPAL16x9DVCProHD2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD3", chkPAL16x9DVCProHD3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD4", chkPAL16x9DVCProHD4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD5", chkPAL16x9DVCProHD5) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD6_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD6", chkPAL16x9DVCProHD6) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCProHD7_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCPROHD7", chkPAL16x9DVCProHD7) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcam1_Validate(Cancel As Boolean)
    
If CheckboxValidate("pal16x9hdcam1", chkPal16x9hdcam1) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcam2_Validate(Cancel As Boolean)

If CheckboxValidate("pal16x9hdcam2", chkPal16x9hdcam2) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcam3_Validate(Cancel As Boolean)

If CheckboxValidate("Pal16x9hdcam3", chkPal16x9hdcam3) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcam4_Validate(Cancel As Boolean)

If CheckboxValidate("Pal16x9hdcam4", chkPal16x9hdcam4) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcam5_Validate(Cancel As Boolean)

If CheckboxValidate("Pal16x9hdcam5", chkPal16x9hdcam5) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcamSR1_Validate(Cancel As Boolean)

If CheckboxValidate("pal16x9hdcamsr1", chkPal16x9hdcamSR1) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcamSR2_Validate(Cancel As Boolean)

If CheckboxValidate("pal16x9hdcamsr2", chkPal16x9hdcamSR2) = False Then Cancel = True

End Sub

Private Sub chkPal16x9hdcamSR3_Validate(Cancel As Boolean)

If CheckboxValidate("pal16x9hdcamsr3", chkPal16x9hdcamSR3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM1", chkPAL16x9XDCAM1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM2", chkPAL16x9XDCAM2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM3", chkPAL16x9XDCAM3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM4", chkPAL16x9XDCAM4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM5", chkPAL16x9XDCAM5) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM6_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM6", chkPAL16x9XDCAM6) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAM7_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAM7", chkPAL16x9XDCAM7) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD1", chkPAL16x9XDCAMHD1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD2", chkPAL16x9XDCAMHD2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD3", chkPAL16x9XDCAMHD3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD4", chkPAL16x9XDCAMHD4) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD5", chkPAL16x9XDCAMHD5) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD6_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD6", chkPAL16x9XDCAMHD6) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD7_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD7", chkPAL16x9XDCAMHD7) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD8_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD8", chkPAL16x9XDCAMHD8) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9XDCAMHD9_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9XDCAMHD9", chkPAL16x9XDCAMHD9) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3BetaSX1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3BetaSX1", chkPAL4x3BetaSX1) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3BetaSX2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3BetaSX2", chkPAL4x3BetaSX2) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3BetaSX3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3BetaSX3", chkPAL4x3BetaSX3) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3BetaSX4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3BetaSX4", chkPAL4x3BetaSX4) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3BetaSX5_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3BetaSX5", chkPAL4x3BetaSX5) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3Digi1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3Digi1", chkPAL4x3Digi1) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3Digi2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3Digi2", chkPAL4x3Digi2) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3Digi3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3Digi3", chkPAL4x3Digi3) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCam1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCam1", chkPAL16x9DVCam1) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCam2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCam2", chkPAL16x9DVCam2) = False Then Cancel = True

End Sub

Private Sub chkPAL16x9DVCam3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL16x9DVCam3", chkPAL16x9DVCam3) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3Digi4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3Digi4", chkPAL4x3Digi4) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCam1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCam1", chkPAL4x3DVCam1) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCam2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCam2", chkPAL4x3DVCam2) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCam3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCam3", chkPAL4x3DVCam3) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCam4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCam4", chkPAL4x3DVCam4) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCPro1_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCPRO1", chkPAL4x3DVCPro1) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCPro2_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCPRO2", chkPAL4x3DVCPro2) = False Then Cancel = True

End Sub

Private Sub chkPAL4x3DVCPro3_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCPRO3", chkPAL4x3DVCPro3) = False Then Cancel = True

End Sub

'Private Sub chkNTSC16x9BetaSX1_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9BetaSX1", chkNTSC16x9BetaSX1) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9BetaSX2_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9BetaSX2", chkNTSC16x9BetaSX2) = False Then Cancel = True
'
'End Sub
'
'Private Sub chkNTSC16x9BetaSX3_Validate(Cancel As Boolean)
'
'If CheckboxValidate("NTSC16x9BetaSX3", chkNTSC16x9BetaSX3) = False Then Cancel = True
'
'End Sub

Private Sub chkPAL4x3DVCPro4_Validate(Cancel As Boolean)

If CheckboxValidate("PAL4x3DVCPRO4", chkPAL4x3DVCPro4) = False Then Cancel = True

End Sub

Private Sub cmdAdvanceProcess_Click()

Dim l_lngDespatchID As Long

Select Case cmdAdvanceProcess.Caption

    Case "Advance Process"
        'Nothing
    Case "Despatch Item"
        With frmJobDespatch
            .cmdClear.Value = True
            'Create the Despatch note
            .cmbCompany.Text = grdItems.Columns("skibblycustomer").Text
            .cmbContact.Text = GetData("skibblycustomer", "attentionof", "skibblycustomerID", grdItems.Columns("skibblycustomerID").Text)
            .txtAddress.Text = GetData("skibblycustomer", "address", "skibblycustomerID", grdItems.Columns("skibblycustomerID").Text)
            .txtPostCode.Text = GetData("skibblycustomer", "postcode", "skibblycustomerID", grdItems.Columns("skibblycustomerID").Text)
            .txtCountry.Text = GetData("skibblycustomer", "country", "skibblycustomerID", grdItems.Columns("skibblycustomerID").Text)
            .txtTelephone.Text = GetData("skibblycustomer", "telephone", "skibblycustomerID", grdItems.Columns("skibblycustomerID").Text)
            .cmbDirection.Text = "OUT"
            .lblBillToCompanyID.Caption = 1105
            .cmbBillToCompany.Text = "Skibbly Media Ltd"
            .lblSilentSave.Caption = "Yes"
            .cmdSave.Value = True
            .lblSilentSave.Caption = ""
            DoEvents
            
            'Add the items to it
            l_lngDespatchID = .txtDespatchID.Text
            DoEvents
            
            If txtPal16x9hdcam1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcam1barcode.Text
            If txtPal16x9hdcam2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcam2barcode.Text
            If txtPal16x9hdcam3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcam3barcode.Text
            If txtPal16x9hdcam4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcam4barcode.Text
            If txtPal16x9hdcam5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcam5barcode.Text
            If txtNTSC16x9hdcam1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9hdcam1barcode.Text
            If txtNTSC16x9hdcam2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9hdcam2barcode.Text
            If txtNTSC16x9hdcam3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9hdcam3barcode.Text
            If txtPAL16x9Digi1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9Digi1barcode.Text
            If txtPAL16x9Digi2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9Digi2barcode.Text
            If txtPAL16x9Digi3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9Digi3barcode.Text
            If txtPAL16x9Digi4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9Digi4barcode.Text
            If txtPAL16x9Digi5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9Digi5barcode.Text
            If txtNTSC16x9Digi1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9Digi1barcode.Text
            If txtNTSC16x9Digi2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9Digi2barcode.Text
            If txtNTSC16x9Digi3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9Digi3barcode.Text
            If txtPAL4x3Digi1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3Digi1barcode.Text
            If txtPAL4x3Digi2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3Digi2barcode.Text
            If txtPAL4x3Digi3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3Digi3barcode.Text
            If txtPAL4x3Digi4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3Digi4barcode.Text
'            If txtNTSC4x3Digi1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3Digi1barcode.Text
'            If txtNTSC4x3Digi2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3Digi2barcode.Text
'            If txtNTSC4x3Digi3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3Digi3barcode.Text
            If txtPAL16x9DVCam1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCam1barcode.Text
            If txtPAL16x9DVCam2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCam2barcode.Text
            If txtPAL16x9DVCam3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCam3barcode.Text
            If txtPAL16x9DVCam4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCam4barcode.Text
            If txtPAL16x9DVCam5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCam5barcode.Text
            If txtPAL4x3DVCam1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCam1barcode.Text
            If txtPAL4x3DVCam2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCam2barcode.Text
            If txtPAL4x3DVCam3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCam3barcode.Text
            If txtPAL4x3DVCam4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCam4barcode.Text
            If txtNTSC4x3DVCam1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3DVCam1barcode.Text
            If txtNTSC4x3DVCam2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3DVCam2barcode.Text
            If txtNTSC4x3DVCam3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3DVCam3barcode.Text
            If txtPAL16x9BetaSX1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9BetaSX1barcode.Text
            If txtPAL16x9BetaSX2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9BetaSX2barcode.Text
            If txtPAL16x9BetaSX3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9BetaSX3barcode.Text
            If txtPAL16x9BetaSX4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9BetaSX4barcode.Text
            If txtPAL16x9BetaSX5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9BetaSX5barcode.Text
            If txtPAL4x3BetaSX1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3BetaSX1barcode.Text
            If txtPAL4x3BetaSX2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3BetaSX2barcode.Text
            If txtPAL4x3BetaSX3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3BetaSX3barcode.Text
            If txtPAL4x3BetaSX4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3BetaSX4barcode.Text
            If txtPAL4x3BetaSX5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3BetaSX5barcode.Text
'            If txtNTSC16x9BetaSX1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9BetaSX1barcode.Text
'            If txtNTSC16x9BetaSX2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9BetaSX2barcode.Text
'            If txtNTSC16x9BetaSX3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9BetaSX3barcode.Text
            If txtNTSC4x3BetaSX1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3BetaSX1barcode.Text
            If txtNTSC4x3BetaSX2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3BetaSX2barcode.Text
            If txtNTSC4x3BetaSX3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC4x3BetaSX3barcode.Text
            If txtPAL4x3DVCPro1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCPro1barcode.Text
            If txtPAL4x3DVCPro2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCPro2barcode.Text
            If txtPAL4x3DVCPro3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCPro3barcode.Text
            If txtPAL4x3DVCPro4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL4x3DVCPro4barcode.Text
            If txtPAL16x9XDCAM1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM1barcode.Text
            If txtPAL16x9XDCAM2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM2barcode.Text
            If txtPAL16x9XDCAM3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM3barcode.Text
            If txtPAL16x9XDCAM4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM4barcode.Text
            If txtPAL16x9XDCAM5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM5barcode.Text
            If txtPAL16x9XDCAM6barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM6barcode.Text
            If txtPAL16x9XDCAM7barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAM7barcode.Text
            If txtPal16x9hdcamSR1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcamSR1barcode.Text
            If txtPal16x9hdcamSR2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcamSR2barcode.Text
            If txtPal16x9hdcamSR3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPal16x9hdcamSR3barcode.Text
            If txtPAL16x9DVCPro1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro1barcode.Text
            If txtPAL16x9DVCPro2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro2barcode.Text
            If txtPAL16x9DVCPro3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro3barcode.Text
            If txtPAL16x9DVCPro4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro4barcode.Text
            If txtPAL16x9DVCProHD1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD1barcode.Text
            If txtPAL16x9DVCProHD2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD2barcode.Text
            If txtPAL16x9DVCProHD3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD3barcode.Text
            If txtPAL16x9DVCProHD4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD4barcode.Text
            If txtPAL16x9DVCProHD5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD5barcode.Text
            If txtPAL16x9DVCProHD6barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD6barcode.Text
            If txtPAL16x9DVCProHD7barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCProHD7barcode.Text
            If txtPAL16x9DVCamHD1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCamHD1barcode.Text
            If txtPAL16x9DVCamHD2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCamHD2barcode.Text
            If txtPAL16x9DVCamHD3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCamHD3barcode.Text
            If txtPAL16x9DVCamHD4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCamHD4barcode.Text
            If txtPAL16x9XDCAMHD1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD1barcode.Text
            If txtPAL16x9XDCAMHD2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD2barcode.Text
            If txtPAL16x9XDCAMHD3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD3barcode.Text
            If txtPAL16x9XDCAMHD4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD4barcode.Text
            If txtPAL16x9XDCAMHD5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD5barcode.Text
            If txtPAL16x9XDCAMHD6barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD6barcode.Text
            If txtPAL16x9XDCAMHD7barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD7barcode.Text
            If txtPAL16x9XDCAMHD8barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD8barcode.Text
            If txtPAL16x9XDCAMHD9barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9XDCAMHD9barcode.Text
            If txtntsc16x9XDCAMHD1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtntsc16x9XDCAMHD1barcode.Text
            If txtntsc16x9XDCAMHD2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtntsc16x9XDCAMHD2barcode.Text
            If txtntsc16x9XDCAMHD3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtntsc16x9XDCAMHD3barcode.Text
            If txtntsc16x9XDCAMHD4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtntsc16x9XDCAMHD4barcode.Text
            If txtntsc16x9XDCAMHD5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtntsc16x9XDCAMHD5barcode.Text
            If txtntsc16x9XDCAMHD6barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtntsc16x9XDCAMHD6barcode.Text
'            If txtNTSC16x9XDCAM1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9XDCAM1barcode.Text
'            If txtNTSC16x9XDCAM2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9XDCAM2barcode.Text
'            If txtNTSC16x9XDCAM3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9XDCAM3barcode.Text
'            If txtNTSC16x9XDCAM4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9XDCAM4barcode.Text
'            If txtNTSC16x9XDCAM5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9XDCAM5barcode.Text
'            If txtNTSC16x9XDCAM6barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9XDCAM6barcode.Text
            If txtPAL16x9DVCPro501barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro501barcode.Text
            If txtPAL16x9DVCPro502barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro502barcode.Text
            If txtPAL16x9DVCPro503barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro503barcode.Text
            If txtPAL16x9DVCPro504barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro504barcode.Text
            If txtPAL16x9DVCPro505barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro505barcode.Text
            If txtPAL16x9DVCPro506barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro506barcode.Text
            If txtPAL16x9DVCPro507barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro507barcode.Text
            If txtPAL16x9DVCPro508barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro508barcode.Text
            If txtPAL16x9DVCPro509barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtPAL16x9DVCPro509barcode.Text
            If txtNTSC16x9DVCam1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCam1barcode.Text
            If txtNTSC16x9DVCam2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCam2barcode.Text
            If txtNTSC16x9DVCam3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCam3barcode.Text
            If txtNTSC16x9DVCProHD1barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCProHD1barcode.Text
            If txtNTSC16x9DVCProHD2barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCProHD2barcode.Text
            If txtNTSC16x9DVCProHD3barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCProHD3barcode.Text
            If txtNTSC16x9DVCProHD4barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCProHD4barcode.Text
            If txtNTSC16x9DVCProHD5barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCProHD5barcode.Text
            If txtNTSC16x9DVCProHD6barcode.Text <> "" Then DespatchLibraryItem l_lngDespatchID, txtNTSC16x9DVCProHD6barcode.Text
            
            ShowDespatchDetail l_lngDespatchID
            .txtNoOfCopies.Text = 2
            DoEvents
            .cmdPrint.Value = True
            DoEvents
        End With
        frmJobDespatch.Hide
        Unload frmJobDespatch
        grdItems.Columns("readytobillcontract").Text = 1
        grdItems.Columns("despatchID").Text = l_lngDespatchID
        grdItems.Update
        RefreshWithBookmark
        frmSkibblyTracker.WindowState = 2
        
    Case "Bill on Despatch Job"
        Dim l_lngDuration As Long, l_lngRunningTime As Long, l_strDuration As String, l_strJobLine As String, l_strChargeCode As String, l_strCode As String, _
            l_strStockCode As String, l_lngStockQuantity As Long, l_lngJobID As Long, l_lngJobDetailID As Long, l_lngCostingID As Long

        If frmJob.txtJobID.Text <> "" And frmJob.txtStatus.Text = "Confirmed" Then
            l_lngJobID = Val(frmJob.txtJobID.Text)
            l_lngDuration = 0
            l_strJobLine = "Delivery - " & grdItems.Columns("title").Text & " for " & grdItems.Columns("skibblycustomer").Text
        
            l_strChargeCode = "COLDEL"
            l_lngJobDetailID = MakeJobDetailLine(l_lngJobID, "O", l_strJobLine, 1, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, 0, 0, l_strStockCode, l_lngStockQuantity)
            l_lngCostingID = CreateCostingLine(l_lngJobDetailID, l_lngJobID, "O", l_strJobLine, l_strChargeCode, 1, 0, Val(adoItems.Recordset("despatchcost")) * 1.2, 0, GetVATRate("1"), 0, 0, 0, 0)
            grdItems.Columns("deliverybilled").Text = 1
            grdItems.Update
            RefreshWithBookmark
        Else
            MsgBox "You need to have a valid Confirmed job loaded to be able to Bill", vbCritical

        End If

End Select

End Sub

Private Sub cmdClose_Click()

Unload Me

End Sub

Private Sub cmdContractBilling_Click()

Dim l_lngDuration As Long, l_lngRunningTime As Long, l_strDuration As String, l_strJobLine As String, l_strChargeCode As String, l_lngQuantity As Long, l_strCode As String, l_lngJobID As Long, l_blnHDflag As Boolean

l_blnHDflag = False

If optComplete(4).Value <> 0 Then

    If frmJob.txtJobID.Text <> "" And frmJob.txtStatus.Text = "Confirmed" Then
        l_lngJobID = Val(frmJob.txtJobID.Text)
        l_strJobLine = grdItems.Columns("title").Text
        If optSearchFilter(0).Value <> 0 Then
            MsgBox "We can only add individual filtered items to the bill. Please filter a type of tape and re-do", vbCritical
            Exit Sub
        ElseIf optSearchFilter(1).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " HDCAM PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPal16x9hdcam1barcode.Text)
            If txtPal16x9hdcam2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPal16x9hdcam2barcode.Text)
            End If
            If txtPal16x9hdcam3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPal16x9hdcam3barcode.Text)
            End If
            If txtPal16x9hdcam4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPal16x9hdcam4barcode.Text)
            End If
            If txtPal16x9hdcam5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPal16x9hdcam5barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMHDCAM"
            If l_lngQuantity >= 16 Then
                l_strChargeCode = l_strChargeCode & "16"
            ElseIf l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "15"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(2).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " HDCAM NTSC 16x9"
            l_lngDuration = GetDurationOfTape(txtNTSC16x9hdcam1barcode.Text)
            If txtNTSC16x9hdcam2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9hdcam2barcode.Text)
            End If
            If txtNTSC16x9hdcam3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9hdcam3barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMHDCAMXC"
        ElseIf optSearchFilter(3).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DigiBeta PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9Digi1barcode.Text)
            If txtPAL16x9Digi2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9Digi2barcode.Text)
            End If
            If txtPAL16x9Digi3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9Digi3barcode.Text)
            End If
            If txtPAL16x9Digi4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9Digi4barcode.Text)
            End If
            If txtPAL16x9Digi5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9Digi5barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDB"
            If l_lngQuantity >= 16 Then
                l_strChargeCode = l_strChargeCode & "16"
            ElseIf l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "15"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(4).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DigiBeta PAL 4x3"
            l_lngDuration = GetDurationOfTape(txtPAL4x3Digi1barcode.Text)
            If txtPAL4x3Digi2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3Digi2barcode.Text)
            End If
            If txtPAL4x3Digi3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3Digi3barcode.Text)
            End If
            If txtPAL4x3Digi4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3Digi4barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDBAC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(5).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DigiBeta NTSC 16x9"
            l_lngDuration = GetDurationOfTape(txtNTSC16x9Digi1barcode.Text)
            If txtNTSC16x9Digi2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9Digi2barcode.Text)
            End If
            If txtNTSC16x9Digi3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9Digi3barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDBXC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
'        ElseIf optSearchFilter(6).Value <> 0 Then
'            l_strJobLine = l_strJobLine & " DigiBeta NTSC 4x3"
'            l_lngDuration = GetDurationOfTape(txtNTSC4x3Digi1barcode.Text)
'            If txtNTSC4x3Digi2barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC4x3Digi2barcode.Text)
'            End If
'            If txtNTSC4x3Digi3barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC4x3Digi3barcode.Text)
'            End If
'            l_lngQuantity = adoItems.Recordset.RecordCount
'            l_strChargeCode = "SKDBDBXCAC"
'            If l_lngQuantity >= 10 Then
'                l_strChargeCode = l_strChargeCode & "11"
'            ElseIf l_lngQuantity >= 7 Then
'                l_strChargeCode = l_strChargeCode & "10"
'            Else
'                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
'            End If
        ElseIf optSearchFilter(10).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCAM PAL 4x3"
            l_lngDuration = GetDurationOfTape(txtPAL4x3DVCam1barcode.Text)
            If txtPAL4x3DVCam2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3DVCam2barcode.Text)
            End If
            If txtPAL4x3DVCam3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3DVCam3barcode.Text)
            End If
            If txtPAL4x3DVCam4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3DVCam4barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCAMAC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(11).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCAM NTSC 4x3"
            l_lngDuration = GetDurationOfTape(txtNTSC4x3DVCam1barcode.Text)
            If txtNTSC4x3DVCam2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC4x3DVCam2barcode.Text)
            End If
            If txtNTSC4x3DVCam3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC4x3DVCam3barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCAMXCAC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(12).Value <> 0 Then
            l_strJobLine = l_strJobLine & " BetaSX PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9BetaSX1barcode.Text)
            If txtPAL16x9BetaSX2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9BetaSX2barcode.Text)
            End If
            If txtPAL16x9BetaSX3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9BetaSX3barcode.Text)
            End If
            If txtPAL16x9BetaSX4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9BetaSX4barcode.Text)
            End If
            If txtPAL16x9BetaSX5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9BetaSX5barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBSX"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(13).Value <> 0 Then
            l_strJobLine = l_strJobLine & " BetaSX PAL 4x3"
            l_lngDuration = GetDurationOfTape(txtPAL4x3BetaSX1barcode.Text)
            If txtPAL4x3BetaSX2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3BetaSX2barcode.Text)
            End If
            If txtPAL4x3BetaSX3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3BetaSX3barcode.Text)
            End If
            If txtPAL4x3BetaSX4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3BetaSX4barcode.Text)
            End If
            If txtPAL4x3BetaSX5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3BetaSX5barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBSXAC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(14).Value <> 0 Then
            l_strJobLine = l_strJobLine & " BetaSX NTSC 4x3"
            l_lngDuration = GetDurationOfTape(txtNTSC4x3BetaSX1barcode.Text)
            If txtNTSC4x3BetaSX2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC4x3BetaSX2barcode.Text)
            End If
            If txtNTSC4x3BetaSX3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC4x3BetaSX3barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBSXXCAC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(15).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCPro PAL 4x3"
            l_lngDuration = GetDurationOfTape(txtPAL4x3DVCPro1barcode.Text)
            If txtPAL4x3DVCPro2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3DVCPro2barcode.Text)
            End If
            If txtPAL4x3DVCPro3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3DVCPro3barcode.Text)
            End If
            If txtPAL4x3DVCPro4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL4x3DVCPro4barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCPROAC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(17).Value <> 0 Then
            l_strJobLine = l_strJobLine & " XDCAM PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9XDCAM1barcode.Text)
            If txtPAL16x9XDCAM2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAM2barcode.Text)
            End If
            If txtPAL16x9XDCAM3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAM3barcode.Text)
            End If
            If txtPAL16x9XDCAM4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAM4barcode.Text)
            End If
            If txtPAL16x9XDCAM5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAM5barcode.Text)
            End If
            If txtPAL16x9XDCAM6barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAM6barcode.Text)
            End If
            If txtPAL16x9XDCAM7barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAM7barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBXDCAM"
'        ElseIf optSearchFilter(18).Value <> 0 Then
'            l_strJobLine = l_strJobLine & " BetaSX NTSC 16x9"
'            l_lngDuration = GetDurationOfTape(txtPAL16x9BetaSX1barcode.Text)
'            If txtPAL16x9BetaSX2barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9BetaSX2barcode.Text)
'            End If
'            If txtPAL16x9BetaSX3barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9BetaSX3barcode.Text)
'            End If
'            l_lngQuantity = adoItems.Recordset.RecordCount
'            l_strChargeCode = "SKDBSXXC"
'            If l_lngQuantity >= 11 Then
'                l_strChargeCode = l_strChargeCode & "11"
'            ElseIf l_lngQuantity >= 7 Then
'                l_strChargeCode = l_strChargeCode & "10"
'            ElseIf l_lngQuantity >= 2 Then
'                l_strChargeCode = l_strChargeCode & "06"
'            Else
'                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
'            End If
        ElseIf optSearchFilter(19).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCAM PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9DVCam1barcode.Text)
            If txtPAL16x9DVCam2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCam2barcode.Text)
            End If
            If txtPAL16x9DVCam3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCam3barcode.Text)
            End If
            If txtPAL16x9DVCam4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCam4barcode.Text)
            End If
            If txtPAL16x9DVCam5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCam5barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCAM"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(20).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " HDCAM SR PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPal16x9hdcamSR1barcode.Text)
            If txtPal16x9hdcamSR2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPal16x9hdcamSR2barcode.Text)
            End If
            If txtPal16x9hdcamSR3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPal16x9hdcamSR3barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMHDSR"
        ElseIf optSearchFilter(21).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCPro PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9DVCPro1barcode.Text)
            If txtPAL16x9DVCPro2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro2barcode.Text)
            End If
            If txtPAL16x9DVCPro3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro3barcode.Text)
            End If
            If txtPAL16x9DVCPro4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro4barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCPRO"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(22).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " DVCPro HD PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9DVCProHD1barcode.Text)
            If txtPAL16x9DVCProHD2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCProHD2barcode.Text)
            End If
            If txtPAL16x9DVCProHD3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCProHD3barcode.Text)
            End If
            If txtPAL16x9DVCProHD4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCProHD4barcode.Text)
            End If
            If txtPAL16x9DVCProHD5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCProHD5barcode.Text)
            End If
            If txtPAL16x9DVCProHD6barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCProHD6barcode.Text)
            End If
            If txtPAL16x9DVCProHD7barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCProHD7barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMDVCPROHD"
        ElseIf optSearchFilter(7).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " DVCPro HD NTSC 16x9"
            l_lngDuration = GetDurationOfTape(txtNTSC16x9DVCProHD1barcode.Text)
            If txtNTSC16x9DVCProHD2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCProHD2barcode.Text)
            End If
            If txtNTSC16x9DVCProHD3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCProHD3barcode.Text)
            End If
            If txtNTSC16x9DVCProHD4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCProHD4barcode.Text)
            End If
            If txtNTSC16x9DVCProHD5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCProHD5barcode.Text)
            End If
            If txtNTSC16x9DVCProHD6barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCProHD6barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMDVCPROHDXC"
        ElseIf optSearchFilter(23).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " DVCAM HD PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9DVCamHD1barcode.Text)
            If txtPAL16x9DVCamHD2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCamHD2barcode.Text)
            End If
            If txtPAL16x9DVCamHD3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCamHD3barcode.Text)
            End If
            If txtPAL16x9DVCamHD4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCamHD4barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMDVCAMHD"
        ElseIf optSearchFilter(24).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " XDCAM HD PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9XDCAMHD1barcode.Text)
            If txtPAL16x9XDCAMHD2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD2barcode.Text)
            End If
            If txtPAL16x9XDCAMHD3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD3barcode.Text)
            End If
            If txtPAL16x9XDCAMHD4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD4barcode.Text)
            End If
            If txtPAL16x9XDCAMHD5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD5barcode.Text)
            End If
            If txtPAL16x9XDCAMHD6barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD6barcode.Text)
            End If
            If txtPAL16x9XDCAMHD7barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD7barcode.Text)
            End If
            If txtPAL16x9XDCAMHD8barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD8barcode.Text)
            End If
            If txtPAL16x9XDCAMHD9barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9XDCAMHD9barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMXDCAMHD"
        ElseIf optSearchFilter(25).Value <> 0 Then
            l_blnHDflag = True
            l_strJobLine = l_strJobLine & " XDCAM HD NTSC 16x9"
            l_lngDuration = GetDurationOfTape(txtntsc16x9XDCAMHD1barcode.Text)
            If txtntsc16x9XDCAMHD2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtntsc16x9XDCAMHD2barcode.Text)
            End If
            If txtntsc16x9XDCAMHD3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtntsc16x9XDCAMHD3barcode.Text)
            End If
            If txtntsc16x9XDCAMHD4barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtntsc16x9XDCAMHD4barcode.Text)
            End If
            If txtntsc16x9XDCAMHD5barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtntsc16x9XDCAMHD5barcode.Text)
            End If
            If txtntsc16x9XDCAMHD6barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtntsc16x9XDCAMHD6barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKHDCAMXDCAMHDXC"
'        ElseIf optSearchFilter(26).Value <> 0 Then
'            l_strJobLine = l_strJobLine & " XDCAM NTSC 16x9"
'            l_lngDuration = GetDurationOfTape(txtNTSC16x9XDCAM1barcode.Text)
'            If txtNTSC16x9XDCAM2barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9XDCAM2barcode.Text)
'            End If
'            If txtNTSC16x9XDCAM3barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9XDCAM3barcode.Text)
'            End If
'            If txtNTSC16x9XDCAM4barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9XDCAM4barcode.Text)
'            End If
'            If txtNTSC16x9XDCAM5barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9XDCAM5barcode.Text)
'            End If
'            If txtNTSC16x9XDCAM6barcode.Text <> "" Then
'                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9XDCAM6barcode.Text)
'            End If
'            l_lngQuantity = adoItems.Recordset.RecordCount
'            l_strChargeCode = "SKDBXDCAM"
        ElseIf optSearchFilter(27).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCPro50 PAL 16x9"
            l_lngDuration = GetDurationOfTape(txtPAL16x9DVCPro501barcode.Text)
            If txtPAL16x9DVCPro502barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro502barcode.Text)
            End If
            If txtPAL16x9DVCPro503barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro503barcode.Text)
            End If
            If txtPAL16x9DVCPro504barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro504barcode.Text)
            End If
            If txtPAL16x9DVCPro505barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro505barcode.Text)
            End If
            If txtPAL16x9DVCPro506barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro506barcode.Text)
            End If
            If txtPAL16x9DVCPro507barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro507barcode.Text)
            End If
            If txtPAL16x9DVCPro508barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro508barcode.Text)
            End If
            If txtPAL16x9DVCPro509barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtPAL16x9DVCPro509barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCPRO50"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        ElseIf optSearchFilter(28).Value <> 0 Then
            l_strJobLine = l_strJobLine & " DVCAM NTSC 16x9"
            l_lngDuration = GetDurationOfTape(txtNTSC16x9DVCam1barcode.Text)
            If txtNTSC16x9DVCam2barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCam2barcode.Text)
            End If
            If txtNTSC16x9DVCam3barcode.Text <> "" Then
                l_lngDuration = l_lngDuration + GetDurationOfTape(txtNTSC16x9DVCam3barcode.Text)
            End If
            l_lngQuantity = adoItems.Recordset.RecordCount
            l_strChargeCode = "SKDBDVCAMXC"
            If l_lngQuantity >= 11 Then
                l_strChargeCode = l_strChargeCode & "11"
            ElseIf l_lngQuantity >= 7 Then
                l_strChargeCode = l_strChargeCode & "10"
            ElseIf l_lngQuantity >= 2 Then
                l_strChargeCode = l_strChargeCode & "06"
            Else
                l_strChargeCode = l_strChargeCode & Format(l_lngQuantity, "00")
            End If
        End If
        
        MakeJobDetailLine l_lngJobID, "I", l_strJobLine, l_lngQuantity, l_strChargeCode, l_lngDuration, Now, g_strUserInitials, 0, 0
        
        adoItems.Recordset.MoveFirst
        Do While Not adoItems.Recordset.EOF
            adoItems.Recordset("contractbilled") = 1
            adoItems.Recordset.Update
            adoItems.Recordset.MoveNext
        Loop
        adoItems.Recordset.MoveLast
        RefreshWithBookmark
        l_blnHDflag = False
    Else
        MsgBox "You need to have a valid Confirmed job loaded to be able to Bill", vbCritical
    End If
Else
    MsgBox "You need to have filtered only the items ready to be billed.", vbCritical
End If

End Sub

Private Sub cmdDespatchBillAll_Click()

If Not adoItems.Recordset.EOF Then
    adoItems.Recordset.MoveFirst
    Do While Not adoItems.Recordset.EOF
            
        If cmdAdvanceProcess.Caption = "Bill on Despatch Job" Then
            cmdAdvanceProcess.Value = True
        End If
        adoItems.Recordset.MoveNext
    Loop
End If

End Sub

Private Sub cmdDuplicateGrid_Click()

Dim l_strSQL As String, l_rstTemp As ADODB.Recordset, l_strNewTitle As String

l_strNewTitle = InputBox("Please give the Title for the New set of Items", "Duplicating itmes for a New Run")

l_strSQL = adoItems.RecordSource
Set l_rstTemp = ExecuteSQL(l_strSQL, g_strExecuteError)
CheckForSQLError
If l_rstTemp.RecordCount > 0 Then

    l_rstTemp.MoveFirst
    Do While Not l_rstTemp.EOF
    
        l_strSQL = "INSERT into skibblytracker (skibblycustomerID, skibblycustomername, title, pal16x9hdcam1, pal16x9hdcam2, pal16x9hdcam3, pal16x9hdcam4, pal16x9hdcam5, ntsc16x9hdcam1, ntsc16x9hdcam2, ntsc16x9hdcam3, "
        l_strSQL = l_strSQL & "pal16x9digi1, pal16x9digi2, pal16x9digi3, pal16x9digi4, pal16x9digi5, ntsc16x9digi1, ntsc16x9digi2, ntsc16x9digi3, pal4x3digi1, pal4x3digi2, pal4x3digi3, pal4x3digi4, "
        l_strSQL = l_strSQL & "pal4x3dvcam1, pal4x3dvcam2, pal4x3dvcam3, pal4x3dvcam4, ntsc4x3dvcam1, ntsc4x3dvcam2, ntsc4x3dvcam3, "
        l_strSQL = l_strSQL & "pal16x9betasx1, pal16x9betasx2, pal16x9betasx3, pal16x9betasx4, pal16x9betasx5, pal4x3betasx1, pal4x3betasx2, pal4x3betasx3, pal4x3betasx4, pal4x3betasx5, ntsc4x3betasx1, ntsc4x3betasx2, ntsc4x3betasx3, "
        l_strSQL = l_strSQL & "pal4x3dvcpro1, pal4x3dvcpro2, pal4x3dvcpro3, pal4x3dvcpro4, ntsc4x3dvcpro1, ntsc4x3dvcpro2, ntsc4x3dvcpro3, ntsc4x3dvcpro4, "
        l_strSQL = l_strSQL & "pal16x9dvcpro1, pal16x9dvcpro2, pal16x9dvcpro3, pal16x9dvcpro501, pal16x9dvcpro502, pal16x9dvcpro503, pal16x9dvcpro504, pal16x9dvcpro505, pal16x9dvcpro506, pal16x9dvcpro507, pal16x9dvcpro508, pal16x9dvcpro509, "
        l_strSQL = l_strSQL & "pal16x9dvcproHD1, pal16x9dvcproHD2, pal16x9dvcproHD3, pal16x9dvcproHD4, pal16x9dvcproHD5, pal16x9dvcproHD6, pal16x9dvcproHD7, "
        l_strSQL = l_strSQL & "pal16x9xdcam1, pal16x9xdcam2, pal16x9xdcam3, pal16x9xdcam4, pal16x9xdcam5, pal16x9xdcam6, pal16x9xdcam7, "
        l_strSQL = l_strSQL & "pal16x9dvcam1, pal16x9dvcam2, pal16x9dvcam3, pal16x9dvcam4, pal16x9dvcam5, "
        l_strSQL = l_strSQL & "pal16x9dvcamHD1, pal16x9dvcamHD2, pal16x9dvcamHD3, pal16x9dvcamHD4, "
        l_strSQL = l_strSQL & "pal16x9hdcamSR1, pal16x9hdcamSR2, pal16x9hdcamSR3, "
        l_strSQL = l_strSQL & "pal16x9xdcamHD1, pal16x9xdcamHD2, pal16x9xdcamHD3, pal16x9xdcamHD4, pal16x9xdcamHD5, pal16x9xdcamHD6, pal16x9xdcamHD7, pal16x9xdcamHD8, pal16x9xdcamHD9, "
        l_strSQL = l_strSQL & "ntsc16x9xdcamHD1, ntsc16x9xdcamHD2, ntsc16x9xdcamHD3, ntsc16x9xdcamHD4, ntsc16x9xdcamHD5, ntsc16x9xdcamHD6"
        l_strSQL = l_strSQL & ") VALUES ("
        
        l_strSQL = l_strSQL & l_rstTemp("skibblycustomerID") & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstTemp("skibblycustomername")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewTitle) & "', "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcam1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcam2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcam3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcam4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcam5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9hdcam1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9hdcam2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9hdcam3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9digi1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9digi2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9digi3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9digi4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9digi5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9digi1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9digi2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9digi3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3digi1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3digi2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3digi3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3digi4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcam1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcam2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcam3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcam4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcam1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcam2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcam3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9betasx1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9betasx2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9betasx3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9betasx4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9betasx5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3betasx1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3betasx2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3betasx3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3betasx4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3betasx5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3betasx1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3betasx2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3betasx3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcpro1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcpro2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcpro3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal4x3dvcpro4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcpro1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcpro2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcpro3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc4x3dvcpro4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro501") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro502") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro503") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro504") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro505") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro506") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro507") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro508") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcpro509") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD6") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcproHD7") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam6") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcam7") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcam1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcam2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcam3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcam4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcam5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcamHD1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcamHD2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcamHD3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9dvcamHD4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcamSR1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcamSR2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9hdcamSR3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD6") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD7") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD8") & ", "
        l_strSQL = l_strSQL & l_rstTemp("pal16x9xdcamHD9") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9xdcamHD1") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9xdcamHD2") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9xdcamHD3") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9xdcamHD4") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9xdcamHD5") & ", "
        l_strSQL = l_strSQL & l_rstTemp("ntsc16x9xdcamHD6") & "); "
        
        Debug.Print l_strSQL
        ExecuteSQL l_strSQL, g_strExecuteError
        CheckForSQLError
        
        l_rstTemp.MoveNext
        
    Loop
    
End If

l_rstTemp.Close
Set l_rstTemp = Nothing
Beep

End Sub

Private Sub cmdInsertNew_Click()

ExecuteSQL "INSERT INTO skibblytracker (skibblycustomerID) VALUES (null);", g_strExecuteError
CheckForSQLError
RefreshWithBookmark

End Sub

Private Sub cmdReport_Click()

Dim l_strSelectionFormula As String
Dim l_strReportFile As String

l_strSelectionFormula = "{skibblytracker.title} = """ & txtTitleSearch.Text & """"

l_strReportFile = g_strLocationOfCrystalReportFiles & "Skibblydelivery.rpt"
PrintCrystalReport l_strReportFile, l_strSelectionFormula, True
    
End Sub

Private Sub cmdSearch_Click()

Dim l_strSQL As String, l_sql As String, l_rst As ADODB.Recordset, l_conSearch As ADODB.Connection, l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

l_strSQL = "SELECT DISTINCT title FROM skibblytracker WHERE 1=1 "

If optComplete(0).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled = 0 "
ElseIf optComplete(1).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled <> 0 "
ElseIf optComplete(3).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled = 0 and readytodespatch <> 0 and (despatchID is null or despatchID = 0) "
ElseIf optComplete(4).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled = 0 and readytodespatch <> 0 and despatchID is not null and despatchID <> 0 "
End If

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open l_strSQL, l_conSearch, adOpenDynamic
End With

l_rstSearch.ActiveConnection = Nothing

Set txtTitleSearch.DataSourceList = l_rstSearch

l_conSearch.Close
Set l_conSearch = Nothing

l_strSQL = "SELECT * FROM skibblytracker WHERE 1=1 "

If txtTitleSearch.Text <> "" Then
    l_strSQL = l_strSQL & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
End If

If optComplete(0).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled = 0 "
ElseIf optComplete(1).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled <> 0 "
ElseIf optComplete(3).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled = 0 and readytodespatch <> 0 and (despatchID is null or despatchID = 0) "
ElseIf optComplete(4).Value = True Then
    l_strSQL = l_strSQL & "AND deliverybilled = 0 AND contractbilled = 0 and readytodespatch <> 0 and despatchID is not null and despatchID <> 0 "
End If

If optSearchFilter(1).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9hdcam1 <> 0 OR pal16x9hdcam2 <> 0 OR pal16x9hdcam3 <> 0 OR pal16x9hdcam4 <> 0 OR pal16x9hdcam5 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcam1 <> 0 and (pal16x9hdcam1barcode IS NULL or pal16x9hdcam1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcam2 <> 0 and (pal16x9hdcam2barcode IS NULL or pal16x9hdcam2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcam3 <> 0 and (pal16x9hdcam3barcode IS NULL or pal16x9hdcam3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcam4 <> 0 and (pal16x9hdcam4barcode IS NULL or pal16x9hdcam4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcam5 <> 0 and (pal16x9hdcam5barcode IS NULL or pal16x9hdcam5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "pal16x9hdcam"
ElseIf optSearchFilter(2).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (ntsc16x9hdcam1 <> 0 OR ntsc16x9hdcam2 <> 0 OR ntsc16x9hdcam3 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9hdcam1 <> 0 and (ntsc16x9hdcam1barcode IS NULL or ntsc16x9hdcam1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9hdcam2 <> 0 and (ntsc16x9hdcam2barcode IS NULL or ntsc16x9hdcam2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9hdcam3 <> 0 and (ntsc16x9hdcam3barcode IS NULL or ntsc16x9hdcam3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(4).Caption = ""
    lblTapeRemaining(5).Caption = ""
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = False
    txtAllocateReel(5).Enabled = False
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "ntsc16x9hdcam"
ElseIf optSearchFilter(3).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9digi1 <> 0 OR pal16x9digi2 <> 0 OR pal16x9digi3 <> 0 OR pal16x9digi4 <> 0 OR pal16x9digi5 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9digi1 <> 0 and (pal16x9digi1barcode IS NULL or pal16x9digi1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9digi2 <> 0 and (pal16x9digi2barcode IS NULL or pal16x9digi2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9digi3 <> 0 and (pal16x9digi3barcode IS NULL or pal16x9digi3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9digi4 <> 0 and (pal16x9digi4barcode IS NULL or pal16x9digi4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9digi5 <> 0 and (pal16x9digi5barcode IS NULL or pal16x9digi5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "pal16x9digi"
'ElseIf optSearchFilter(4).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal4x3digi1 <> 0 OR pal4x3digi2 <> 0 OR pal4x3digi3 <> 0 OR pal4x3digi4 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3digi1 <> 0 and (pal4x3digi1barcode IS NULL or pal4x3digi1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3digi2 <> 0 and (pal4x3digi2barcode IS NULL or pal4x3digi2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3digi3 <> 0 and (pal4x3digi3barcode IS NULL or pal4x3digi3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3digi4 <> 0 and (pal4x3digi4barcode IS NULL or pal4x3digi4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal4x3digi"
'ElseIf optSearchFilter(5).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc16x9digi1 <> 0 OR ntsc16x9digi2 <> 0 OR ntsc16x9digi3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9digi1 <> 0 and (ntsc16x9digi1barcode IS NULL or ntsc16x9digi1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9digi2 <> 0 and (ntsc16x9digi2barcode IS NULL or ntsc16x9digi2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9digi3 <> 0 and (ntsc16x9digi3barcode IS NULL or ntsc16x9digi3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "ntsc16x9digi"
'ElseIf optSearchFilter(6).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc4x3digi1 <> 0 OR ntsc4x3digi2 <> 0 OR ntsc4x3digi3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3digi1 <> 0 and (ntsc4x3digi1barcode IS NULL or ntsc4x3digi1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3digi2 <> 0 and (ntsc4x3digi2barcode IS NULL or ntsc4x3digi2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3digi3 <> 0 and (ntsc4x3digi3barcode IS NULL or ntsc4x3digi3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "ntsc4x3digi"
'ElseIf optSearchFilter(10).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal4x3dvcam1 <> 0 OR pal4x3dvcam2 <> 0 OR pal4x3dvcam3 <> 0 OR pal4x3dvcam4 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcam1 <> 0 and (pal4x3dvcam1barcode IS NULL or pal4x3dvcam1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcam2 <> 0 and (pal4x3dvcam2barcode IS NULL or pal4x3dvcam2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcam3 <> 0 and (pal4x3dvcam3barcode IS NULL or pal4x3dvcam3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcam4 <> 0 and (pal4x3dvcam4barcode IS NULL or pal4x3dvcam4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal4x3dvcam"
'ElseIf optSearchFilter(11).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc4x3dvcam1 <> 0 OR ntsc4x3dvcam2 <> 0 OR ntsc4x3dvcam3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3dvcam1 <> 0 and (ntsc4x3dvcam1barcode IS NULL or ntsc4x3dvcam1barcode = '')) "
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3dvcam2 <> 0 and (ntsc4x3dvcam2barcode IS NULL or ntsc4x3dvcam2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3dvcam3 <> 0 and (ntsc4x3dvcam3barcode IS NULL or ntsc4x3dvcam3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "ntsc4x3dvcam"
ElseIf optSearchFilter(12).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9betasx1 <> 0 OR pal16x9betasx2 <> 0 OR pal16x9betasx3 <> 0 OR pal16x9betasx4 <> 0 OR pal16x9betasx5 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9betasx1 <> 0 and (pal16x9betasx1barcode IS NULL or pal16x9betasx1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9betasx2 <> 0 and (pal16x9betasx2barcode IS NULL or pal16x9betasx2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9betasx3 <> 0 and (pal16x9betasx3barcode IS NULL or pal16x9betasx3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9betasx4 <> 0 and (pal16x9betasx4barcode IS NULL or pal16x9betasx4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9betasx5 <> 0 and (pal16x9betasx5barcode IS NULL or pal16x9betasx5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "pal16x9betasx"
ElseIf optSearchFilter(13).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal4x3betasx1 <> 0 OR pal4x3betasx2 <> 0 OR pal4x3betasx3 <> 0 OR pal4x3betasx4 <> 0 OR pal4x3betasx5 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3betasx1 <> 0 and (pal4x3betasx1barcode IS NULL or pal4x3betasx1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3betasx2 <> 0 and (pal4x3betasx2barcode IS NULL or pal4x3betasx2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3betasx3 <> 0 and (pal4x3betasx3barcode IS NULL or pal4x3betasx3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3betasx4 <> 0 and (pal4x3betasx4barcode IS NULL or pal4x3betasx4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3betasx5 <> 0 and (pal4x3betasx5barcode IS NULL or pal4x3betasx5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "pal4x3betasx"
'ElseIf optSearchFilter(14).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc4x3betasx1 <> 0 OR ntsc4x3betasx2 <> 0 OR ntsc4x3betasx3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3betasx1 <> 0 and (ntsc4x3betasx1barcode IS NULL or ntsc4x3betasx1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3betasx2 <> 0 and (ntsc4x3betasx2barcode IS NULL or ntsc4x3betasx2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc4x3betasx3 <> 0 and (ntsc4x3betasx3barcode IS NULL or ntsc4x3betasx3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "ntsc4x3betasx"
'ElseIf optSearchFilter(15).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal4x3dvcpro1 <> 0 OR pal4x3dvcpro2 <> 0 OR pal4x3dvcpro3 <> 0 OR pal4x3dvcpro4 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcpro1 <> 0 and (pal4x3dvcpro1barcode IS NULL or pal4x3dvcpro1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcpro2 <> 0 and (pal4x3dvcpro2barcode IS NULL or pal4x3dvcpro2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcpro3 <> 0 and (pal4x3dvcpro3barcode IS NULL or pal4x3dvcpro3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal4x3dvcpro4 <> 0 and (pal4x3dvcpro4barcode IS NULL or pal4x3dvcpro4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal4x3dvcpro"
'ElseIf optSearchFilter(17).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal16x9XDCAM1 <> 0 OR pal16x9XDCAM2 <> 0 OR pal16x9XDCAM3 <> 0 OR pal16x9XDCAM4 <> 0 OR pal16x9XDCAM5 <> 0 OR pal16x9XDCAM6 <> 0 OR pal16x9XDCAM7 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM1 <> 0 and (pal16x9XDCAM1barcode IS NULL or pal16x9XDCAM1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM2 <> 0 and (pal16x9XDCAM2barcode IS NULL or pal16x9XDCAM2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM3 <> 0 and (pal16x9XDCAM3barcode IS NULL or pal16x9XDCAM3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM4 <> 0 and (pal16x9XDCAM4barcode IS NULL or pal16x9XDCAM4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM5 <> 0 and (pal16x9XDCAM5barcode IS NULL or pal16x9XDCAM5barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM6 <> 0 and (pal16x9XDCAM6barcode IS NULL or pal16x9XDCAM6barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAM7 <> 0 and (pal16x9XDCAM7barcode IS NULL or pal16x9XDCAM7barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(7).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = True
'    txtAllocateReel(6).Enabled = True
'    txtAllocateReel(7).Enabled = True
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal16x9XDCAM"
'ElseIf optSearchFilter(18).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc16x9betasx1 <> 0 OR ntsc16x9betasx2 <> 0 OR ntsc16x9betasx3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9betasx1 <> 0 and (ntsc16x9betasx1barcode IS NULL or ntsc16x9betasx1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9betasx2 <> 0 and (ntsc16x9betasx2barcode IS NULL or ntsc16x9betasx2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9betasx3 <> 0 and (ntsc16x9betasx3barcode IS NULL or ntsc16x9betasx3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    m_strFormat = "ntsc16x9betasx"
ElseIf optSearchFilter(19).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9dvcam1 <> 0 OR pal16x9dvcam2 <> 0 OR pal16x9dvcam3 <> 0 OR pal16x9dvcam4 <> 0 OR pal16x9dvcam5 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcam1 <> 0 and (pal16x9dvcam1barcode IS NULL or pal16x9dvcam1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcam2 <> 0 and (pal16x9dvcam2barcode IS NULL or pal16x9dvcam2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcam3 <> 0 and (pal16x9dvcam3barcode IS NULL or pal16x9dvcam3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcam4 <> 0 and (pal16x9dvcam4barcode IS NULL or pal16x9dvcam4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcam5 <> 0 and (pal16x9dvcam5barcode IS NULL or pal16x9dvcam5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "pal16x9dvcam"
'ElseIf optSearchFilter(20).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal16x9hdcamSR1 <> 0 OR pal16x9hdcamSR2 <> 0 OR pal16x9hdcamSR3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcamSR1 <> 0 and (pal16x9hdcamSR1barcode IS NULL or pal16x9hdcamSR1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcamSR2 <> 0 and (pal16x9hdcamSR2barcode IS NULL or pal16x9hdcamSR2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9hdcamSR3 <> 0 and (pal16x9hdcamSR3barcode IS NULL or pal16x9hdcamSR3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal16x9hdcamSR"
'ElseIf optSearchFilter(21).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal16x9dvcpro1 <> 0 OR pal16x9dvcpro2 <> 0 OR pal16x9dvcpro3 <> 0 OR pal16x9dvcpro4 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro1 <> 0 and (pal16x9dvcpro1barcode IS NULL or pal16x9dvcpro1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro2 <> 0 and (pal16x9dvcpro2barcode IS NULL or pal16x9dvcpro2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro3 <> 0 and (pal16x9dvcpro3barcode IS NULL or pal16x9dvcpro3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro4 <> 0 and (pal16x9dvcpro4barcode IS NULL or pal16x9dvcpro4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal16x9dvcpro"
ElseIf optSearchFilter(22).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9dvcproHD1 <> 0 OR pal16x9dvcproHD2 <> 0 OR pal16x9dvcproHD3 <> 0 OR pal16x9dvcproHD4 <> 0 OR pal16x9dvcproHD5 <> 0 OR pal16x9dvcproHD6 <> 0 OR pal16x9dvcproHD7 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD1 <> 0 and (pal16x9dvcproHD1barcode IS NULL or pal16x9dvcproHD1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD2 <> 0 and (pal16x9dvcproHD2barcode IS NULL or pal16x9dvcproHD2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD3 <> 0 and (pal16x9dvcproHD3barcode IS NULL or pal16x9dvcproHD3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD4 <> 0 and (pal16x9dvcproHD4barcode IS NULL or pal16x9dvcproHD4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD5 <> 0 and (pal16x9dvcproHD5barcode IS NULL or pal16x9dvcproHD5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD6 <> 0 and (pal16x9dvcproHD6barcode IS NULL or pal16x9dvcproHD6barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcproHD7 <> 0 and (pal16x9dvcproHD7barcode IS NULL or pal16x9dvcproHD7barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(7).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = True
    txtAllocateReel(7).Enabled = True
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "pal16x9dvcproHD"
ElseIf optSearchFilter(7).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (ntsc16x9dvcproHD1 <> 0 OR ntsc16x9dvcproHD2 <> 0 OR ntsc16x9dvcproHD3 <> 0 OR ntsc16x9dvcproHD4 <> 0 OR ntsc16x9dvcproHD5 <> 0 OR ntsc16x9dvcproHD6 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcproHD1 <> 0 and (ntsc16x9dvcproHD1barcode IS NULL or ntsc16x9dvcproHD1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcproHD2 <> 0 and (ntsc16x9dvcproHD2barcode IS NULL or ntsc16x9dvcproHD2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcproHD3 <> 0 and (ntsc16x9dvcproHD3barcode IS NULL or ntsc16x9dvcproHD3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcproHD4 <> 0 and (ntsc16x9dvcproHD4barcode IS NULL or ntsc16x9dvcproHD4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcproHD5 <> 0 and (ntsc16x9dvcproHD5barcode IS NULL or ntsc16x9dvcproHD5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcproHD6 <> 0 and (ntsc16x9dvcproHD6barcode IS NULL or ntsc16x9dvcproHD6barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = True
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "ntsc16x9dvcproHD"
'ElseIf optSearchFilter(23).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (pal16x9dvcamHD1 <> 0 OR pal16x9dvcamHD2 <> 0 OR pal16x9dvcamHD3 <> 0 OR pal16x9dvcamHD4 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcamHD1 <> 0 and (pal16x9dvcamHD1barcode IS NULL or pal16x9dvcamHD1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcamHD2 <> 0 and (pal16x9dvcamHD2barcode IS NULL or pal16x9dvcamHD2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcamHD3 <> 0 and (pal16x9dvcamHD3barcode IS NULL or pal16x9dvcamHD3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcamHD4 <> 0 and (pal16x9dvcamHD4barcode IS NULL or pal16x9dvcamHD4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "pal16x9dvcamHD"
ElseIf optSearchFilter(24).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9XDCAMHD1 <> 0 OR pal16x9XDCAMHD2 <> 0 OR pal16x9XDCAMHD3 <> 0 OR pal16x9XDCAMHD4 <> 0 OR pal16x9XDCAMHD5 <> 0 OR pal16x9XDCAMHD6 <> 0 OR pal16x9XDCAMHD7 <> 0 OR pal16x9XDCAMHD8 <> 0 OR pal16x9XDCAMHD9 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD1 <> 0 and (pal16x9XDCAMHD1barcode IS NULL or pal16x9XDCAMHD1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD2 <> 0 and (pal16x9XDCAMHD2barcode IS NULL or pal16x9XDCAMHD2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD3 <> 0 and (pal16x9XDCAMHD3barcode IS NULL or pal16x9XDCAMHD3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD4 <> 0 and (pal16x9XDCAMHD4barcode IS NULL or pal16x9XDCAMHD4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD5 <> 0 and (pal16x9XDCAMHD5barcode IS NULL or pal16x9XDCAMHD5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD6 <> 0 and (pal16x9XDCAMHD6barcode IS NULL or pal16x9XDCAMHD6barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD7 <> 0 and (pal16x9XDCAMHD7barcode IS NULL or pal16x9XDCAMHD7barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(7).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD8 <> 0 and (pal16x9XDCAMHD8barcode IS NULL or pal16x9XDCAMHD8barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(8).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9XDCAMHD9 <> 0 and (pal16x9XDCAMHD9barcode IS NULL or pal16x9XDCAMHD9barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(9).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = True
    txtAllocateReel(7).Enabled = True
    txtAllocateReel(8).Enabled = True
    txtAllocateReel(9).Enabled = True
    m_strFormat = "pal16x9XDCAMHD"
ElseIf optSearchFilter(25).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (ntsc16x9XDCAMHD1 <> 0 OR ntsc16x9XDCAMHD2 <> 0 OR ntsc16x9XDCAMHD3 <> 0 OR ntsc16x9XDCAMHD4 <> 0 OR ntsc16x9XDCAMHD5 <> 0 OR ntsc16x9XDCAMHD6 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAMHD1 <> 0 and (ntsc16x9XDCAMHD1barcode IS NULL or ntsc16x9XDCAMHD1barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAMHD2 <> 0 and (ntsc16x9XDCAMHD2barcode IS NULL or ntsc16x9XDCAMHD2barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAMHD3 <> 0 and (ntsc16x9XDCAMHD3barcode IS NULL or ntsc16x9XDCAMHD3barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAMHD4 <> 0 and (ntsc16x9XDCAMHD4barcode IS NULL or ntsc16x9XDCAMHD4barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAMHD5 <> 0 and (ntsc16x9XDCAMHD5barcode IS NULL or ntsc16x9XDCAMHD5barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAMHD6 <> 0 and (ntsc16x9XDCAMHD6barcode IS NULL or ntsc16x9XDCAMHD6barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = True
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = "ntsc16x9XDCAMHD"
'ElseIf optSearchFilter(26).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc16x9XDCAM1 <> 0 OR ntsc16x9XDCAM2 <> 0 OR ntsc16x9XDCAM3 <> 0 OR ntsc16x9XDCAM4 <> 0 OR ntsc16x9XDCAM5 <> 0 OR ntsc16x9XDCAM6 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAM1 <> 0 and (ntsc16x9XDCAM1barcode IS NULL or ntsc16x9XDCAM1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAM2 <> 0 and (ntsc16x9XDCAM2barcode IS NULL or ntsc16x9XDCAM2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAM3 <> 0 and (ntsc16x9XDCAM3barcode IS NULL or ntsc16x9XDCAM3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAM4 <> 0 and (ntsc16x9XDCAM4barcode IS NULL or ntsc16x9XDCAM4barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAM5 <> 0 and (ntsc16x9XDCAM5barcode IS NULL or ntsc16x9XDCAM5barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9XDCAM6 <> 0 and (ntsc16x9XDCAM6barcode IS NULL or ntsc16x9XDCAM6barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = True
'    txtAllocateReel(5).Enabled = True
'    txtAllocateReel(6).Enabled = True
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "ntsc16x9XDCAM"
ElseIf optSearchFilter(27).Value <> 0 Then
    l_strSQL = l_strSQL & "AND (pal16x9dvcpro501 <> 0 OR pal16x9dvcpro502 <> 0 OR pal16x9dvcpro503 <> 0 OR pal16x9dvcpro504 <> 0 OR pal16x9dvcpro505 <> 0 OR pal16x9dvcpro506 <> 0 OR pal16x9dvcpro507 <> 0 OR pal16x9dvcpro508 <> 0 OR pal16x9dvcpro509 <> 0) "
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro501 <> 0 and (pal16x9dvcpro501barcode IS NULL or pal16x9dvcpro501barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro502 <> 0 and (pal16x9dvcpro502barcode IS NULL or pal16x9dvcpro502barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro503 <> 0 and (pal16x9dvcpro503barcode IS NULL or pal16x9dvcpro503barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro504 <> 0 and (pal16x9dvcpro504barcode IS NULL or pal16x9dvcpro504barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(4).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro505 <> 0 and (pal16x9dvcpro505barcode IS NULL or pal16x9dvcpro505barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(5).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro506 <> 0 and (pal16x9dvcpro506barcode IS NULL or pal16x9dvcpro506barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(6).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro507 <> 0 and (pal16x9dvcpro507barcode IS NULL or pal16x9dvcpro507barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(7).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro508 <> 0 and (pal16x9dvcpro508barcode IS NULL or pal16x9dvcpro508barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(8).Caption = " " & l_rst(0)
    l_rst.Close
    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (pal16x9dvcpro509 <> 0 and (pal16x9dvcpro509barcode IS NULL or pal16x9dvcpro509barcode = '')) "
    If txtTitleSearch.Text <> "" Then
        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
    End If
    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
    If Not l_rst.EOF Then lblTapeRemaining(9).Caption = " " & l_rst(0)
    l_rst.Close
    Set l_rst = Nothing
    txtAllocateReel(1).Enabled = True
    txtAllocateReel(2).Enabled = True
    txtAllocateReel(3).Enabled = True
    txtAllocateReel(4).Enabled = True
    txtAllocateReel(5).Enabled = True
    txtAllocateReel(6).Enabled = True
    txtAllocateReel(7).Enabled = True
    txtAllocateReel(8).Enabled = True
    txtAllocateReel(9).Enabled = True
    m_strFormat = "pal16x9dvcpro50"
'ElseIf optSearchFilter(28).Value <> 0 Then
'    l_strSQL = l_strSQL & "AND (ntsc16x9dvcam1 <> 0 OR ntsc16x9dvcam2 <> 0 OR ntsc16x9dvcam3 <> 0) "
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcam1 <> 0 and (ntsc16x9dvcam1barcode IS NULL or ntsc16x9dvcam1barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(1).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcam2 <> 0 and (ntsc16x9dvcam2barcode IS NULL or ntsc16x9dvcam2barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(2).Caption = " " & l_rst(0)
'    l_rst.Close
'    l_sql = "SELECT Count(skibblytrackerID) from skibblytracker WHERE (ntsc16x9dvcam3 <> 0 and (ntsc16x9dvcam3barcode IS NULL or ntsc16x9dvcam3barcode = '')) "
'    If txtTitleSearch.Text <> "" Then
'        l_sql = l_sql & "AND title LIKE '" & QuoteSanitise(txtTitleSearch.Text) & "%' "
'    End If
'    Set l_rst = ExecuteSQL(l_sql, g_strExecuteError)
'    If Not l_rst.EOF Then lblTapeRemaining(3).Caption = " " & l_rst(0)
'    l_rst.Close
'    Set l_rst = Nothing
'    lblTapeRemaining(4).Caption = ""
'    lblTapeRemaining(5).Caption = ""
'    lblTapeRemaining(6).Caption = ""
'    txtAllocateReel(1).Enabled = True
'    txtAllocateReel(2).Enabled = True
'    txtAllocateReel(3).Enabled = True
'    txtAllocateReel(4).Enabled = False
'    txtAllocateReel(5).Enabled = False
'    txtAllocateReel(6).Enabled = False
'    txtAllocateReel(7).Enabled = False
'    txtAllocateReel(8).Enabled = False
'    m_strFormat = "ntsc16x9dvcam"
Else
    lblTapeRemaining(1).Caption = ""
    lblTapeRemaining(2).Caption = ""
    lblTapeRemaining(3).Caption = ""
    lblTapeRemaining(4).Caption = ""
    lblTapeRemaining(5).Caption = ""
    lblTapeRemaining(6).Caption = ""
    lblTapeRemaining(7).Caption = ""
    lblTapeRemaining(8).Caption = ""
    lblTapeRemaining(9).Caption = ""
    txtAllocateReel(1).Enabled = False
    txtAllocateReel(2).Enabled = False
    txtAllocateReel(3).Enabled = False
    txtAllocateReel(4).Enabled = False
    txtAllocateReel(5).Enabled = False
    txtAllocateReel(6).Enabled = False
    txtAllocateReel(7).Enabled = False
    txtAllocateReel(8).Enabled = False
    txtAllocateReel(9).Enabled = False
    m_strFormat = ""
End If

l_strSQL = l_strSQL & ";"

m_blnSkipMoveComplete = True

adoItems.RecordSource = l_strSQL
adoItems.Refresh

m_blnSkipMoveComplete = False

If adoItems.Recordset.RecordCount > 0 Then adoItems.Recordset.MoveFirst

End Sub

Private Sub cmdSearchForTape_Click(Index As Integer)

Dim l_lngTempID As Long

If optSearchFilter(0).Value = True Then
    MsgBox "Please select a Tape Type filter, before attempting to locate a tape.", vbCritical, "Cannot Find Tape."
    Exit Sub
End If

If txtSearchForTape(Index).Text = "" Then
    MsgBox "No barcode has been scanned.", vbCritical, "Cannot Find Tape."
    Exit Sub
End If

Dim l_strFieldName As String

If adoItems.Recordset.RecordCount > 0 Then

    If optSearchFilter(1).Value = True Then
        l_strFieldName = "pal16x9hdcam"
        If Index > 4 Then
            MsgBox "Only 5 tapes for HDCAM 16x9 PAL", vbCritical, "Cannot Find Tape."
            Exit Sub
        End If
    ElseIf optSearchFilter(2).Value = True Then
        l_strFieldName = "ntsc16x9hdcam"
        If Index > 2 Then
            MsgBox "Only 3 tapes for HDCAM 16x9 NTSC", vbCritical, "Cannot Find Tape."
            Exit Sub
        End If
    ElseIf optSearchFilter(3).Value = True Then
        l_strFieldName = "pal16x9digi"
        If Index > 4 Then
            MsgBox "Only 5 tapes for DIGI 16x9 PAL", vbCritical, "Cannot Find Tape."
            Exit Sub
        End If
'    ElseIf optSearchFilter(4).Value = True Then
'        l_strFieldName = "pal4x3digi"
'        If Index > 3 Then
'            MsgBox "Only 4 tapes for DIGI 16x9 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(5).Value = True Then
'        l_strFieldName = "ntsc16x9digi"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for DIGI 4x3 PAL", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(6).Value = True Then
'        l_strFieldName = "ntsc4x3digi"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for DIGI 4x3 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(10).Value = True Then
'        l_strFieldName = "pal4x3dvcam"
'        If Index > 3 Then
'            MsgBox "Only 4 tapes for DVCAM 4x3 PAL", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(11).Value = True Then
'        l_strFieldName = "ntsc4x3dvcam"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for DVCAM 4x3 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
    ElseIf optSearchFilter(12).Value = True Then
        l_strFieldName = "pal16x9betasx"
        If Index > 4 Then
            MsgBox "Only 5 tapes for BetaSX 16x9 PAL", vbCritical, "Cannot Find Tape."
            Exit Sub
        End If
    ElseIf optSearchFilter(13).Value = True Then
        l_strFieldName = "pal4x3betasx"
        If Index > 4 Then
            MsgBox "Only 5 tapes for BetaSX 4x3 PAL", vbCritical, "Cannot Find Tape."
            Exit Sub
        End If
'    ElseIf optSearchFilter(14).Value = True Then
'        l_strFieldName = "ntsc4x3betasx"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for betaSX 4x3 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(15).Value = True Then
'        l_strFieldName = "pal4x3dvcpro"
'        If Index > 3 Then
'            MsgBox "Only 4 tapes for DVCPro 4x3 PAL", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(16).Value = True Then
'        l_strFieldName = "ntsc4x3dvcpro"
'        If Index > 3 Then
'            MsgBox "Only 4 tapes for DVCPro 4x3 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(17).Value = True Then
'        l_strFieldName = "pal16x9XDCAM"
'    ElseIf optSearchFilter(18).Value = True Then
'        l_strFieldName = "ntsc16x9betasx"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for BetaSX 16x9 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
    ElseIf optSearchFilter(19).Value = True Then
        l_strFieldName = "pal16x9dvcam"
        If Index > 4 Then
            MsgBox "Only 5 tapes for DVCAM 16x9 PAL", vbCritical, "Cannot Find Tape."
            Exit Sub
        End If
'    ElseIf optSearchFilter(20).Value = True Then
'        l_strFieldName = "pal16x9hdcamSR"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for HDCAM SR 16x9 PAL", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
'    ElseIf optSearchFilter(21).Value = True Then
'        l_strFieldName = "pal16x9dvcpro"
'        If Index > 3 Then
'            MsgBox "Only 4 tapes for DVCPro 16x9 PAL", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
    ElseIf optSearchFilter(22).Value = True Then
        l_strFieldName = "pal16x9dvcproHD"
    ElseIf optSearchFilter(7).Value = True Then
        l_strFieldName = "ntsc16x9dvcproHD"
'    ElseIf optSearchFilter(23).Value = True Then
'        l_strFieldName = "pal16x9dvcamHD"
'        If Index > 3 Then
'            MsgBox "Only 4 tapes for DVCAM HD 16x9 PAL", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
    ElseIf optSearchFilter(24).Value = True Then
        l_strFieldName = "pal16x9XDCAMHD"
    ElseIf optSearchFilter(25).Value = True Then
        l_strFieldName = "ntsc16x9XDCAMHD"
'    ElseIf optSearchFilter(26).Value = True Then
'        l_strFieldName = "ntsc16x9XDCAM"
    ElseIf optSearchFilter(27).Value = True Then
        l_strFieldName = "pal16x9dvcpro50"
'    ElseIf optSearchFilter(28).Value = True Then
'        l_strFieldName = "ntsc16x9dvcam"
'        If Index > 2 Then
'            MsgBox "Only 3 tapes for DVCAM 4x3 NTSC", vbCritical, "Cannot Find Tape."
'            Exit Sub
'        End If
    End If
    
    l_strFieldName = l_strFieldName & Index + 1 & "barcode"
    
    l_lngTempID = GetData("skibblytracker", "skibblytrackerID", l_strFieldName, txtSearchForTape(Index).Text)
    
    adoItems.Recordset.MoveFirst
    
    Do While Not adoItems.Recordset.EOF
        If adoItems.Recordset("skibblytrackerID") = l_lngTempID Then
            MsgBox "Found it", vbInformation, "Tape Found"
            txtSearchForTape(Index).Text = ""
            Exit Sub
        End If
        adoItems.Recordset.MoveNext
    Loop

End If

MsgBox "That tape is not assigned to anything currently showing in the grid.", vbInformation, "Tape Not Found."

End Sub

Private Sub cmdUnAdvanceProcess_Click()

Select Case cmdUnAdvanceProcess.Caption

    Case "Un-Advance Process"
        'Nothing
        
    Case "Unbill Despatch"
        grdItems.Columns("deliverybilled").Text = 0
        grdItems.Update
        RefreshWithBookmark
    
    Case "Unbill Contract"
        grdItems.Columns("contractbilled").Text = 0
        grdItems.Update
        RefreshWithBookmark
    
    Case "Unmark Despatch"
        grdItems.Columns("despatchID").Text = ""
        txtDespatchID.Text = ""
        grdItems.Columns("readytobillcontract").Text = 0
        grdItems.Update
        RefreshWithBookmark

End Select

End Sub

Private Sub ddnSkibblyCustomers_CloseUp()

If ddnSkibblyCustomers.Columns("skibblycustomerID").Text <> "" Then
    grdItems.Columns("skibblycustomerID").Text = ddnSkibblyCustomers.Columns("skibblycustomerID").Text
    grdItems.Columns("skibblycustomer").Text = ddnSkibblyCustomers.Columns("customername").Text
End If

End Sub

Private Sub Form_Load()

Dim l_strSQL As String, Count As Integer

grdItems.StyleSets("headerfield").BackColor = &HE7FFE7
grdItems.StyleSets("stagefield").BackColor = &HE7FFFF
grdItems.StyleSets("conclusionfield").BackColor = &HFFFFE7

grdItems.StyleSets.Add "Error"
grdItems.StyleSets("Error").BackColor = &HA0A0FF

adoItems.ConnectionString = g_strConnection

cmdSearch.Value = True

adoCustomers.ConnectionString = g_strConnection
adoCustomers.RecordSource = "SELECT * FROM skibblycustomer ORDER BY customername;"
adoCustomers.Refresh

grdItems.Columns("skibblycustomer").DropDownHwnd = ddnSkibblyCustomers.hWnd

If Not CheckAccess("/skibblyrequirements", True) Then
    'Make all the selectors and individual allocation controls disabled.
    For Count = 0 To 26
        On Error Resume Next
        Frame3(Count).Enabled = False
        On Error GoTo 0
    Next
'    Frame4.Visible = True
Else
    For Count = 0 To 26
        On Error Resume Next
        Frame3(Count).Enabled = True
        On Error GoTo 0
    Next
'    Frame4.Visible = False
End If

End Sub

Private Sub Form_Resize()

On Error Resume Next

'grdItems.Width = Me.ScaleWidth - grdItems.Left - 120
grdItems.Height = (Me.ScaleHeight - grdItems.Top - frmButtons.Height) - 240
frmButtons.Top = Me.ScaleHeight - frmButtons.Height - 120
frmButtons.Left = Me.ScaleWidth - frmButtons.Width - 120

End Sub

Private Sub grdItems_AfterDelete(RtnDispErrMsg As Integer)

adoItems.Refresh

End Sub

Private Sub grdItems_BeforeUpdate(Cancel As Integer)

If grdItems.Columns("despatchID").Text <> "" And grdItems.Columns("despatchcost").Text <> "" Then
    grdItems.Columns("readytobilldelivery").Value = -1
Else
    grdItems.Columns("readytobilldelivery").Value = 0
End If

If StatusCheck(Val(lblTrackerID.Caption)) = False Then
    grdItems.Columns("readytodespatch").Text = 0
Else
    grdItems.Columns("readytodespatch").Text = 1
End If

End Sub

Private Sub grdItems_RowLoaded(ByVal Bookmark As Variant)

If grdItems.Columns("readytodespatch").Text = 0 Then grdItems.Columns("readytodespatch").CellStyleSet "Error"
If grdItems.Columns("readytobillcontract").Text = 0 Then grdItems.Columns("readytobillcontract").CellStyleSet "Error"
If grdItems.Columns("contractbilled").Text = 0 Then grdItems.Columns("contractbilled").CellStyleSet "Error"
If grdItems.Columns("readytobilldelivery").Text = 0 Then grdItems.Columns("readytobilldelivery").CellStyleSet "Error"
If grdItems.Columns("deliverybilled").Text = 0 Then grdItems.Columns("deliverybilled").CellStyleSet "Error"

If Trim(" " & grdItems.Columns("despatchID").Text) <> "" Then
    txtDespatchCost.Enabled = True
Else
    txtDespatchCost.Enabled = False
End If

End Sub

Private Sub optComplete_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Private Sub optSearchFilter_Click(Index As Integer)

cmdSearch.Value = True

End Sub

Private Sub txtAllocateReel_KeyPress(Index As Integer, KeyAscii As Integer)

Dim l_blnAllocated As Boolean, l_rsTemp As ADODB.Recordset

If KeyAscii = 13 Then
    If txtAllocateReel(Index).Text <> "" Then
        If Val(lblTapeRemaining(Index).Caption) > 0 Then
            If CheckSkibblyAllocation(txtAllocateReel(Index).Text) Then
                Set l_rsTemp = ExecuteSQL(adoItems.RecordSource, g_strExecuteError)
                CheckForSQLError
                l_rsTemp.MoveFirst
                Do While Not l_rsTemp.EOF
                    If Val(l_rsTemp(m_strFormat & Index)) <> 0 And Trim(" " & l_rsTemp(m_strFormat & Index & "barcode")) = "" Then
                        txtAllocateReel(Index).Text = UCase(txtAllocateReel(Index).Text)
                        If MsgBox("About to allocate '" & l_rsTemp("skibblycustomername") & "' Reel " & Index & vbCrLf & "Are You Sure?", vbYesNo, "Allocating Tape") = vbYes Then
                            SetData "skibblytracker", m_strFormat & Index & "barcode", "skibblytrackerID", l_rsTemp("skibblytrackerID"), txtAllocateReel(Index).Text
                            AllocateSkibblyTape txtAllocateReel(Index).Text
                            If StatusCheck(l_rsTemp("skibblytrackerID")) = True Then
                                SetData "skibblytracker", "readytodespatch", "skibblytrackerID", l_rsTemp("skibblytrackerID"), 1
                            Else
                                SetData "skibblytracker", "readytodespatch", "skibblytrackerID", l_rsTemp("skibblytrackerID"), 0
                            End If
                            RefreshWithBookmark
                            txtAllocateReel(Index).Text = ""
                            lblTapeRemaining(Index) = Val(lblTapeRemaining(Index)) - 1
                            Exit Do
                        End If
                    End If
                    l_rsTemp.MoveNext
                Loop
                l_rsTemp.Close
                Set l_rsTemp = Nothing
            Else
                MsgBox "Tape Has Already Been Allocated.", vbCritical
                txtAllocateReel(Index).Text = ""
            End If
        Else
            MsgBox "All " & m_strFormat & " reel " & Index & " are already allocated.", vbCritical
            txtAllocateReel(Index).Text = ""
        End If
    End If
End If

End Sub

Private Sub txtDespatchCost_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    If IsNumeric(txtDespatchCost.Text) Then
        grdItems.Columns("despatchcost").Text = txtDespatchCost.Text
        grdItems.Columns("readytobilldelivery").Text = 1
    Else
        grdItems.Columns("despatchcost").Text = ""
        grdItems.Columns("readytobilldelivery").Text = 0
    End If
    grdItems.Update
    RefreshWithBookmark
End If

End Sub

Private Sub txtDespatchID_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    If txtDespatchID.Text <> "" Then
        grdItems.Columns("despatchID").Text = txtDespatchID.Text
        grdItems.Columns("readytobillcontract").Text = 1
        grdItems.Update
    Else
        grdItems.Columns("despatchID").Text = txtDespatchID.Text
        grdItems.Columns("readytobillcontract").Text = 0
        grdItems.Update
    End If
End If

End Sub

'Private Sub txtNTSC16x9BetaSX1barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9BetaSX1barcode", txtNTSC16x9BetaSX1barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9BetaSX2barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9BetaSX2barcode", txtNTSC16x9BetaSX2barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9BetaSX3barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9BetaSX3barcode", txtNTSC16x9BetaSX3barcode
'End If
'
'End Sub

Private Sub txtNTSC16x9Digi1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9Digi1barcode", txtNTSC16x9Digi1barcode
End If

End Sub

Private Sub txtNTSC16x9Digi2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9Digi2barcode", txtNTSC16x9Digi2barcode
End If

End Sub

Private Sub txtNTSC16x9Digi3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9Digi3barcode", txtNTSC16x9Digi3barcode
End If

End Sub

Private Sub txtNTSC16x9DVCam1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCam1barcode", txtNTSC16x9DVCam1barcode
End If

End Sub

Private Sub txtNTSC16x9DVCam2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCam2barcode", txtNTSC16x9DVCam2barcode
End If

End Sub

Private Sub txtNTSC16x9DVCam3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCam3barcode", txtNTSC16x9DVCam3barcode
End If

End Sub

Private Sub txtNTSC16x9DVCProHD1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCPROHD1barcode", txtNTSC16x9DVCProHD1barcode
End If

End Sub

Private Sub txtNTSC16x9DVCProHD2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCPROHD2barcode", txtNTSC16x9DVCProHD2barcode
End If

End Sub

Private Sub txtNTSC16x9DVCProHD3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCPROHD3barcode", txtNTSC16x9DVCProHD3barcode
End If

End Sub

Private Sub txtNTSC16x9DVCProHD4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCPROHD4barcode", txtNTSC16x9DVCProHD4barcode
End If

End Sub

Private Sub txtNTSC16x9DVCProHD5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCPROHD5barcode", txtNTSC16x9DVCProHD5barcode
End If

End Sub

Private Sub txtNTSC16x9DVCProHD6barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9DVCPROHD6barcode", txtNTSC16x9DVCProHD6barcode
End If

End Sub

Private Sub txtNTSC16x9hdcam1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9hdcam1barcode", txtNTSC16x9hdcam1barcode
End If

End Sub

Private Sub txtNTSC16x9hdcam2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9hdcam2barcode", txtNTSC16x9hdcam2barcode
End If

End Sub

Private Sub txtNTSC16x9hdcam3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC16x9hdcam3barcode", txtNTSC16x9hdcam3barcode
End If

End Sub

'Private Sub txtNTSC16x9XDCAM1barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9XDCAM1barcode", txtNTSC16x9XDCAM1barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9XDCAM2barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9XDCAM2barcode", txtNTSC16x9XDCAM2barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9XDCAM3barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9XDCAM3barcode", txtNTSC16x9XDCAM3barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9XDCAM4barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9XDCAM4barcode", txtNTSC16x9XDCAM4barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9XDCAM5barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9XDCAM5barcode", txtNTSC16x9XDCAM5barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC16x9XDCAM6barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC16x9XDCAM6barcode", txtNTSC16x9XDCAM6barcode
'End If
'
'End Sub

Private Sub txtntsc16x9XDCAMHD1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "ntsc16x9XDCAMHD1barcode", txtntsc16x9XDCAMHD1barcode
End If

End Sub

Private Sub txtntsc16x9XDCAMHD3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "ntsc16x9XDCAMHD3barcode", txtntsc16x9XDCAMHD3barcode
End If

End Sub

Private Sub txtntsc16x9XDCAMHD4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "ntsc16x9XDCAMHD4barcode", txtntsc16x9XDCAMHD4barcode
End If

End Sub

Private Sub txtntsc16x9XDCAMHD5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "ntsc16x9XDCAMHD5barcode", txtntsc16x9XDCAMHD5barcode
End If

End Sub

Private Sub txtntsc16x9XDCAMHD6barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "ntsc16x9XDCAMHD6barcode", txtntsc16x9XDCAMHD6barcode
End If

End Sub

Private Sub txtNTSC4x3BetaSX1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC4x3BetaSX1barcode", txtNTSC4x3BetaSX1barcode
End If

End Sub

Private Sub txtNTSC4x3BetaSX2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC4x3BetaSX2barcode", txtNTSC4x3BetaSX2barcode
End If

End Sub

Private Sub txtNTSC4x3BetaSX3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC4x3BetaSX3barcode", txtNTSC4x3BetaSX3barcode
End If

End Sub

'Private Sub txtNTSC4x3Digi1barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC4x3Digi1barcode", txtNTSC4x3Digi1barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC4x3Digi2barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC4x3Digi2barcode", txtNTSC4x3Digi2barcode
'End If
'
'End Sub
'
'Private Sub txtNTSC4x3Digi3barcode_KeyPress(KeyAscii As Integer)
'
'If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
'    BarcodeTextbox "NTSC4x3Digi3barcode", txtNTSC4x3Digi3barcode
'End If
'
'End Sub

Private Sub txtNTSC4x3DVCam1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC4x3DVCam1barcode", txtNTSC4x3DVCam1barcode
End If

End Sub

Private Sub txtNTSC4x3DVCam2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC4x3DVCam2barcode", txtNTSC4x3DVCam2barcode
End If

End Sub

Private Sub txtNTSC4x3DVCam3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "NTSC4x3DVCam3barcode", txtNTSC4x3DVCam3barcode
End If

End Sub

Private Sub txtPAL16x9BetaSX1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9BetaSX1barcode", txtPAL16x9BetaSX1barcode
End If

End Sub

Private Sub txtPAL16x9BetaSX2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9BetaSX2barcode", txtPAL16x9BetaSX2barcode
End If

End Sub

Private Sub txtPAL16x9BetaSX3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9BetaSX3barcode", txtPAL16x9BetaSX3barcode
End If

End Sub

Private Sub txtPAL16x9BetaSX4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9BetaSX4barcode", txtPAL16x9BetaSX4barcode
End If

End Sub

Private Sub txtPAL16x9BetaSX5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9BetaSX5barcode", txtPAL16x9BetaSX5barcode
End If

End Sub

Private Sub txtPAL16x9Digi1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9Digi1barcode", txtPAL16x9Digi1barcode
End If

End Sub

Private Sub txtPAL16x9Digi2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9Digi2barcode", txtPAL16x9Digi2barcode
End If

End Sub

Private Sub txtPAL16x9Digi3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9Digi3barcode", txtPAL16x9Digi3barcode
End If

End Sub

Private Sub txtPAL16x9Digi4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9Digi4barcode", txtPAL16x9Digi4barcode
End If

End Sub

Private Sub txtPAL16x9Digi5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9Digi5barcode", txtPAL16x9Digi5barcode
End If

End Sub

Private Sub txtPAL16x9DVCam4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCam4barcode", txtPAL16x9DVCam4barcode
End If

End Sub

Private Sub txtPAL16x9DVCam5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCam5barcode", txtPAL16x9DVCam5barcode
End If

End Sub

Private Sub txtPAL16x9DVCamHD1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCamHD1barcode", txtPAL16x9DVCamHD1barcode
End If

End Sub

Private Sub txtPAL16x9DVCamHD2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCamHD2barcode", txtPAL16x9DVCamHD2barcode
End If

End Sub

Private Sub txtPAL16x9DVCamHD3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCamHD3barcode", txtPAL16x9DVCamHD3barcode
End If

End Sub

Private Sub txtPAL16x9DVCamHD4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCamHD4barcode", txtPAL16x9DVCamHD4barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO1barcode", txtPAL16x9DVCPro1barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO2barcode", txtPAL16x9DVCPro2barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO3barcode", txtPAL16x9DVCPro3barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO4barcode", txtPAL16x9DVCPro4barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro501barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO501barcode", txtPAL16x9DVCPro501barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro502barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO502barcode", txtPAL16x9DVCPro502barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro503barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO503barcode", txtPAL16x9DVCPro503barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro504barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO504barcode", txtPAL16x9DVCPro504barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro505barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO505barcode", txtPAL16x9DVCPro505barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro506barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO506barcode", txtPAL16x9DVCPro506barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro507barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO507barcode", txtPAL16x9DVCPro507barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro508barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO508barcode", txtPAL16x9DVCPro508barcode
End If

End Sub

Private Sub txtPAL16x9DVCPro509barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPRO509barcode", txtPAL16x9DVCPro509barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD1barcode", txtPAL16x9DVCProHD1barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD2barcode", txtPAL16x9DVCProHD2barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD3barcode", txtPAL16x9DVCProHD3barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD4barcode", txtPAL16x9DVCProHD4barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD5barcode", txtPAL16x9DVCProHD5barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD6barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD6barcode", txtPAL16x9DVCProHD6barcode
End If

End Sub

Private Sub txtPAL16x9DVCProHD7barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCPROHD7barcode", txtPAL16x9DVCProHD7barcode
End If

End Sub

Private Sub txtPal16x9hdcam1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcam1barcode", txtPal16x9hdcam1barcode
End If

End Sub

Private Sub txtPal16x9hdcam2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcam2barcode", txtPal16x9hdcam2barcode
End If

End Sub

Private Sub txtPal16x9hdcam3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcam3barcode", txtPal16x9hdcam3barcode
End If

End Sub

Private Sub txtPal16x9hdcam4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcam4barcode", txtPal16x9hdcam4barcode
End If

End Sub

Private Sub txtPal16x9hdcam5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcam5barcode", txtPal16x9hdcam5barcode
End If

End Sub

Private Sub txtPal16x9hdcamSR1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcamsr1barcode", txtPal16x9hdcamSR1barcode
End If

End Sub

Private Sub txtPal16x9hdcamSR2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcamsr2barcode", txtPal16x9hdcamSR2barcode
End If

End Sub

Private Sub txtPal16x9hdcamSR3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "Pal16x9hdcamsr3barcode", txtPal16x9hdcamSR3barcode
End If

End Sub

Private Sub txtPAL16x9XDCAM4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM4barcode", txtPAL16x9XDCAM4barcode
End If

End Sub

Private Sub txtPAL16x9XDCAM5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM5barcode", txtPAL16x9XDCAM5barcode
End If

End Sub

Private Sub txtPAL16x9XDCAM6barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM6barcode", txtPAL16x9XDCAM6barcode
End If

End Sub

Private Sub txtPAL16x9XDCAM7barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM7barcode", txtPAL16x9XDCAM7barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD1barcode", txtPAL16x9XDCAMHD1barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD2barcode", txtPAL16x9XDCAMHD2barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD3barcode", txtPAL16x9XDCAMHD3barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD4barcode", txtPAL16x9XDCAMHD4barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD5barcode", txtPAL16x9XDCAMHD5barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD6barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD6barcode", txtPAL16x9XDCAMHD6barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD7barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD7barcode", txtPAL16x9XDCAMHD7barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD8barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD8barcode", txtPAL16x9XDCAMHD8barcode
End If

End Sub

Private Sub txtPAL16x9XDCAMHD9barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAMHD9barcode", txtPAL16x9XDCAMHD9barcode
End If

End Sub

Private Sub txtPAL4x3BetaSX1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3BetaSX1barcode", txtPAL4x3BetaSX1barcode
End If

End Sub

Private Sub txtPAL4x3BetaSX2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3BetaSX2barcode", txtPAL4x3BetaSX2barcode
End If

End Sub

Private Sub txtPAL4x3BetaSX3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3BetaSX3barcode", txtPAL4x3BetaSX3barcode
End If

End Sub

Private Sub txtPAL4x3BetaSX4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3BetaSX4barcode", txtPAL4x3BetaSX4barcode
End If

End Sub

Private Sub txtPAL4x3BetaSX5barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3BetaSX5barcode", txtPAL4x3BetaSX5barcode
End If

End Sub

Private Sub txtPAL4x3Digi1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3Digi1barcode", txtPAL4x3Digi1barcode
End If

End Sub

Private Sub txtPAL4x3Digi2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3Digi2barcode", txtPAL4x3Digi2barcode
End If

End Sub

Private Sub txtPAL4x3Digi3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3Digi3barcode", txtPAL4x3Digi3barcode
End If

End Sub

Private Sub txtPAL16x9DVCam1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCam1barcode", txtPAL16x9DVCam1barcode
End If

End Sub

Private Sub txtPAL16x9DVCam2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCam2barcode", txtPAL16x9DVCam2barcode
End If

End Sub

Private Sub txtPAL16x9DVCam3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9DVCam3barcode", txtPAL16x9DVCam3barcode
End If

End Sub

Private Sub txtPAL4x3Digi4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3Digi4barcode", txtPAL4x3Digi4barcode
End If

End Sub

Private Sub txtPAL4x3DVCam1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCam1barcode", txtPAL4x3DVCam1barcode
End If

End Sub

Private Sub txtPAL4x3DVCam2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCam2barcode", txtPAL4x3DVCam2barcode
End If

End Sub

Private Sub txtPAL4x3DVCam3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCam3barcode", txtPAL4x3DVCam3barcode
End If

End Sub

Private Sub txtPAL4x3DVCam4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCam4barcode", txtPAL4x3DVCam4barcode
End If

End Sub

Private Sub txtPAL4x3DVCPro1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCPRO1barcode", txtPAL4x3DVCPro1barcode
End If

End Sub

Private Sub txtPAL4x3DVCPro2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCPRO2barcode", txtPAL4x3DVCPro2barcode
End If

End Sub

Private Sub txtPAL4x3DVCPro3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCPRO3barcode", txtPAL4x3DVCPro3barcode
End If

End Sub

Private Sub txtPAL16x9XDCAM1barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM1barcode", txtPAL16x9XDCAM1barcode
End If

End Sub
Private Sub txtPAL16x9XDCAM2barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM2barcode", txtPAL16x9XDCAM2barcode
End If

End Sub
Private Sub txtPAL16x9XDCAM3barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL16x9XDCAM3barcode", txtPAL16x9XDCAM3barcode
End If

End Sub

Private Sub txtPAL4x3DVCPro4barcode_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 And lblTrackerID.Caption <> "" Then
    BarcodeTextbox "PAL4x3DVCPRO4barcode", txtPAL4x3DVCPro4barcode
End If

End Sub

Function CheckSkibblyAllocation(lp_strBarcode As String) As Boolean

Dim l_lngLibraryID As Long, rs As ADODB.Recordset

If lp_strBarcode <> "" Then
    l_lngLibraryID = GetData("library", "libraryID", "barcode", lp_strBarcode)
    If l_lngLibraryID <> 0 Then
        Set rs = ExecuteSQL("SELECT * FROM skibblyallocatedtapes WHERE libraryID = " & l_lngLibraryID, g_strExecuteError)
        CheckForSQLError
        If rs.RecordCount > 0 Then
            CheckSkibblyAllocation = False
        Else
            CheckSkibblyAllocation = True
        End If
        rs.Close
        Set rs = Nothing
    Else
        MsgBox "Tape Does not exist", vbCritical
        CheckSkibblyAllocation = False
    End If
Else
    CheckSkibblyAllocation = True
End If

End Function

Sub AllocateSkibblyTape(lp_strBarcode As String)

Dim l_strSQL As String

l_strSQL = "INSERT INTO skibblyallocatedtapes (libraryID, barcode) VALUES (" & GetData("library", "libraryID", "barcode", lp_strBarcode) & ", '" & UCase(lp_strBarcode) & "');"
Debug.Print l_strSQL
ExecuteSQL l_strSQL, g_strExecuteError
CheckForSQLError

End Sub

Sub ClearSkibblyAllocation(lp_strBarcode As String)

ExecuteSQL "DELETE FROM skibblyallocatedtapes WHERE barcode = '" & lp_strBarcode & "';", g_strExecuteError
CheckForSQLError

End Sub

Sub RefreshWithBookmark()

Dim Bookmark As Variant
On Error Resume Next
Bookmark = adoItems.Recordset.Bookmark
adoItems.Refresh
On Error Resume Next
If adoItems.Recordset.RecordCount > 0 Then
    adoItems.Recordset.Bookmark = Bookmark
    RefreshWithBookmark2
End If

End Sub

Sub RefreshWithBookmark2()

If adoItems.Recordset("deliverybilled") <> 0 Then
    cmdUnAdvanceProcess.Caption = "Unbill Despatch"
    cmdUnAdvanceProcess.Visible = True
ElseIf adoItems.Recordset("contractbilled") <> 0 Then
    cmdUnAdvanceProcess.Caption = "Unbill Contract"
    cmdUnAdvanceProcess.Visible = True
ElseIf Trim(" " & adoItems.Recordset("despatchID")) <> "" Then
    cmdUnAdvanceProcess.Caption = "Unmark Despatch"
    cmdUnAdvanceProcess.Visible = True
Else
    cmdUnAdvanceProcess.Caption = "Un-Advance Process"
    cmdUnAdvanceProcess.Visible = False
End If
    
If adoItems.Recordset("deliverybilled") = 0 And adoItems.Recordset("readytobilldelivery") <> 0 Then
    cmdAdvanceProcess.Caption = "Bill on Despatch Job"
    cmdAdvanceProcess.Visible = True
    cmdContractBilling.Visible = False
ElseIf adoItems.Recordset("deliverybilled") = 0 And adoItems.Recordset("contractbilled") = 0 And adoItems.Recordset("readytobillcontract") <> 0 Then
    cmdAdvanceProcess.Caption = "Advance Process"
    cmdAdvanceProcess.Visible = False
    cmdContractBilling.Visible = True
ElseIf adoItems.Recordset("deliverybilled") = 0 And adoItems.Recordset("contractbilled") = 0 And adoItems.Recordset("readytobillcontract") = 0 And adoItems.Recordset("readytodespatch") <> 0 Then
    cmdAdvanceProcess.Caption = "Despatch Item"
    cmdAdvanceProcess.Visible = True
    cmdContractBilling.Visible = False
Else
    cmdAdvanceProcess.Caption = "Advance Process"
    cmdAdvanceProcess.Visible = False
    cmdContractBilling = False
End If

If Trim(" " & adoItems.Recordset("despatchID")) <> "" Then
    txtDespatchCost.Enabled = True
Else
    txtDespatchCost.Enabled = False
End If

End Sub

Function GetDurationOfTape(lp_strBarcode As String) As Long

Dim l_strDuration As String, l_lngRunningTime As Long

l_strDuration = GetData("library", "totalduration", "barcode", lp_strBarcode)
If l_strDuration <> "" Then
    l_lngRunningTime = 60 * Val(Mid(l_strDuration, 1, 2))
    l_lngRunningTime = l_lngRunningTime + Val(Mid(l_strDuration, 4, 2))
    If Val(Mid(l_strDuration, 7, 2)) > 30 Then
        l_lngRunningTime = l_lngRunningTime + 1
    End If
End If

GetDurationOfTape = l_lngRunningTime

End Function

Private Sub txtSearchForTape_KeyPress(Index As Integer, KeyAscii As Integer)

If KeyAscii = 13 Then
    cmdSearchForTape_Click Index
End If

End Sub

Private Sub txtWaybillNumber_KeyPress(KeyAscii As Integer)

If KeyAscii = 13 Then
    If txtWaybillNumber.Text <> "" Then
        grdItems.Columns("waybillnumber").Text = txtWaybillNumber.Text
        grdItems.Update
    Else
        grdItems.Columns("waybillnumber").Text = txtWaybillNumber.Text
        grdItems.Update
    End If
End If

End Sub

Function CheckboxValidate(lp_strFieldName As String, CheckboxName As Control) As Boolean

'Basic functionality of the checkbox - checks permissions, then checks if the database agrees with the control, and sets the database if it doesn't, and does a refresh

If lblTrackerID.Caption <> "" Then
    
    If Not CheckAccess("/skibblyrequirements") And CheckboxName.Value <> GetData("skibblytracker", lp_strFieldName, "skibblytrackerID", Val(lblTrackerID.Caption)) Then
        CheckboxName.Value = GetData("skibblytracker", lp_strFieldName, "skibblytrackerID", Val(lblTrackerID.Caption))
        CheckboxValidate = False
        Exit Function
    End If

    SetData "skibblytracker", lp_strFieldName, "skibblytrackerID", Val(lblTrackerID.Caption), CheckboxName.Value
    If StatusCheck(Val(lblTrackerID.Caption)) = True Then
        SetData "skibblytracker", "readytodespatch", "skibblytrackerID", Val(lblTrackerID.Caption), 1
    Else
        SetData "skibblytracker", "readytodespatch", "skibblytrackerID", Val(lblTrackerID.Caption), 0
    End If
    RefreshWithBookmark

End If

CheckboxValidate = True

End Function

Sub BarcodeTextbox(lp_strFieldName As String, TextBoxName As Control)

'Basic functionality of the individual allocations text boxes
'Checks whether the tape is already allocated, then allocates and updates the tracker table, and does a refresh.
If TextBoxName.Text <> "" Then
    If CheckSkibblyAllocation(TextBoxName.Text) Then
        TextBoxName.Text = UCase(TextBoxName.Text)
        SetData "skibblytracker", lp_strFieldName, "skibblytrackerID", Val(lblTrackerID.Caption), TextBoxName.Text
        AllocateSkibblyTape TextBoxName.Text
        If StatusCheck(Val(lblTrackerID.Caption)) = True Then
            SetData "skibblytracker", "readytodespatch", "skibblytrackerID", Val(lblTrackerID.Caption), 1
        Else
            SetData "skibblytracker", "readytodespatch", "skibblytrackerID", Val(lblTrackerID.Caption), 0
        End If
        RefreshWithBookmark
    Else
        MsgBox "Tape Cannot Be Allocated.", vbCritical
        TextBoxName.Text = ""
    End If
Else
    ClearSkibblyAllocation GetData("skibblytracker", lp_strFieldName, "skibblytrackerID", Val(lblTrackerID.Caption))
    SetData "skibblytracker", lp_strFieldName, "skibblytrackerID", Val(lblTrackerID.Caption), ""
    If StatusCheck(Val(lblTrackerID.Caption)) = True Then
        SetData "skibblytracker", "readytodespatch", "skibblytrackerID", Val(lblTrackerID.Caption), 1
    Else
        SetData "skibblytracker", "readytodespatch", "skibblytrackerID", Val(lblTrackerID.Caption), 0
    End If
    RefreshWithBookmark
End If

End Sub

Function StatusCheck(lp_lngTrackerID As Long) As Boolean

Dim l_blnReady As Boolean, l_rsTemp As ADODB.Recordset

Set l_rsTemp = ExecuteSQL("SELECT * FROM skibblytracker WHERE skibblytrackerID = " & lp_lngTrackerID & ";", g_strExecuteError)
CheckForSQLError

l_blnReady = True

If l_rsTemp("pal16x9hdcam1") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcam1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9hdcam2") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcam2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9hdcam3") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcam3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9hdcam4") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcam4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9hdcam5") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcam5barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9hdcam1") <> 0 And Trim(" " & l_rsTemp("ntsc16x9hdcam1barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9hdcam2") <> 0 And Trim(" " & l_rsTemp("ntsc16x9hdcam2barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9hdcam3") <> 0 And Trim(" " & l_rsTemp("ntsc16x9hdcam3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9digi1") <> 0 And Trim(" " & l_rsTemp("pal16x9digi1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9digi2") <> 0 And Trim(" " & l_rsTemp("pal16x9digi2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9digi3") <> 0 And Trim(" " & l_rsTemp("pal16x9digi3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9digi4") <> 0 And Trim(" " & l_rsTemp("pal16x9digi4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9digi5") <> 0 And Trim(" " & l_rsTemp("pal16x9digi5barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9digi1") <> 0 And Trim(" " & l_rsTemp("ntsc16x9digi1barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9digi2") <> 0 And Trim(" " & l_rsTemp("ntsc16x9digi2barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9digi3") <> 0 And Trim(" " & l_rsTemp("ntsc16x9digi3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3digi1") <> 0 And Trim(" " & l_rsTemp("pal4x3digi1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3digi2") <> 0 And Trim(" " & l_rsTemp("pal4x3digi2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3digi3") <> 0 And Trim(" " & l_rsTemp("pal4x3digi3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3digi4") <> 0 And Trim(" " & l_rsTemp("pal4x3digi4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3digi1") <> 0 And Trim(" " & l_rsTemp("ntsc4x3digi1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3digi2") <> 0 And Trim(" " & l_rsTemp("ntsc4x3digi2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3digi3") <> 0 And Trim(" " & l_rsTemp("ntsc4x3digi3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9betasp1") <> 0 And Trim(" " & l_rsTemp("pal16x9betasp1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9betasp2") <> 0 And Trim(" " & l_rsTemp("pal16x9betasp2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9betasp3") <> 0 And Trim(" " & l_rsTemp("pal16x9betasp3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9betasp4") <> 0 And Trim(" " & l_rsTemp("pal16x9betasp4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9betasp5") <> 0 And Trim(" " & l_rsTemp("pal16x9betasp5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9betasp6") <> 0 And Trim(" " & l_rsTemp("pal16x9betasp6barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3betasp1") <> 0 And Trim(" " & l_rsTemp("pal4x3betasp1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3betasp2") <> 0 And Trim(" " & l_rsTemp("pal4x3betasp2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3betasp3") <> 0 And Trim(" " & l_rsTemp("pal4x3betasp3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3betasp4") <> 0 And Trim(" " & l_rsTemp("pal4x3betasp4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3betasp5") <> 0 And Trim(" " & l_rsTemp("pal4x3betasp5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3betasp6") <> 0 And Trim(" " & l_rsTemp("pal4x3betasp6barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3betasp1") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasp1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3betasp2") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasp2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3betasp3") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasp3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3betasp4") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasp4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3betasp5") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasp5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3betasp6") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasp6barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcam1") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcam1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcam2") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcam2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcam3") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcam3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcam4") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcam4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcam5") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcam5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcam1") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcam1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcam2") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcam2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcam3") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcam3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcam4") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcam4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcam1") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcam1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcam2") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcam2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcam3") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcam3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9BetaSX1") <> 0 And Trim(" " & l_rsTemp("pal16x9betasx1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9BetaSX2") <> 0 And Trim(" " & l_rsTemp("pal16x9betasx2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9BetaSX3") <> 0 And Trim(" " & l_rsTemp("pal16x9betasx3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9BetaSX4") <> 0 And Trim(" " & l_rsTemp("pal16x9betasx4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9BetaSX5") <> 0 And Trim(" " & l_rsTemp("pal16x9betasx5barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3BetaSX1") <> 0 And Trim(" " & l_rsTemp("pal4x3betasx1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3BetaSX2") <> 0 And Trim(" " & l_rsTemp("pal4x3betasx2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3BetaSX3") <> 0 And Trim(" " & l_rsTemp("pal4x3betasx3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3BetaSX4") <> 0 And Trim(" " & l_rsTemp("pal4x3betasx4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal4x3BetaSX5") <> 0 And Trim(" " & l_rsTemp("pal4x3betasx5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9BetaSX1") <> 0 And Trim(" " & l_rsTemp("ntsc16x9BetaSX1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9BetaSX2") <> 0 And Trim(" " & l_rsTemp("ntsc16x9BetaSX2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9BetaSX3") <> 0 And Trim(" " & l_rsTemp("ntsc16x9BetaSX3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3BetaSX1") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasx1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3BetaSX2") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasx2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3BetaSX3") <> 0 And Trim(" " & l_rsTemp("ntsc4x3betasx3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcpro1") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcpro1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcpro2") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcpro2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcpro3") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcpro3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal4x3dvcpro4") <> 0 And Trim(" " & l_rsTemp("pal4x3dvcpro4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcpro1") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcpro1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcpro2") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcpro2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcpro3") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcpro3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc4x3dvcpro4") <> 0 And Trim(" " & l_rsTemp("ntsc4x3dvcpro4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM1") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM2") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM3") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM4") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM5") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM6") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM6barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9XDCAM7") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAM7barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9hdcamSR1") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcamSR1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9hdcamSR2") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcamSR2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9hdcamSR3") <> 0 And Trim(" " & l_rsTemp("pal16x9hdcamSR3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcpro1") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcpro2") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcpro3") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcpro4") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro501") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro501barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro502") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro502barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro503") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro503barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro504") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro504barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro505") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro505barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro506") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro506barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro507") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro507barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro508") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro508barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcpro509") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcpro509barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD1") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD2") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD3") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD4") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD5") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD5barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD6") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD6barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9dvcproHD7") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcproHD7barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9dvcproHD1") <> 0 And Trim(" " & l_rsTemp("ntsc16x9dvcproHD1barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9dvcproHD2") <> 0 And Trim(" " & l_rsTemp("ntsc16x9dvcproHD2barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9dvcproHD3") <> 0 And Trim(" " & l_rsTemp("ntsc16x9dvcproHD3barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9dvcproHD4") <> 0 And Trim(" " & l_rsTemp("ntsc16x9dvcproHD4barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9dvcproHD5") <> 0 And Trim(" " & l_rsTemp("ntsc16x9dvcproHD5barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9dvcproHD6") <> 0 And Trim(" " & l_rsTemp("ntsc16x9dvcproHD6barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcamHD1") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcamHD1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcamHD2") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcamHD2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcamHD3") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcamHD3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("pal16x9dvcamHD4") <> 0 And Trim(" " & l_rsTemp("pal16x9dvcamHD4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD1") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD1barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD2") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD2barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD3") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD3barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD4") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD4barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD5") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD5barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD6") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD6barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD7") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD7barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD8") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD8barcode")) = "" Then l_blnReady = False
If l_rsTemp("pal16x9XDCAMHD9") <> 0 And Trim(" " & l_rsTemp("pal16x9XDCAMHD9barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9XDCAMHD1") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAMHD1barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9XDCAMHD2") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAMHD2barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9XDCAMHD3") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAMHD3barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9XDCAMHD4") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAMHD4barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9XDCAMHD5") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAMHD5barcode")) = "" Then l_blnReady = False
If l_rsTemp("ntsc16x9XDCAMHD6") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAMHD6barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9XDCAM1") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAM1barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9XDCAM2") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAM2barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9XDCAM3") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAM3barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9XDCAM4") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAM4barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9XDCAM5") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAM5barcode")) = "" Then l_blnReady = False
'If l_rsTemp("ntsc16x9XDCAM6") <> 0 And Trim(" " & l_rsTemp("ntsc16x9XDCAM6barcode")) = "" Then l_blnReady = False

StatusCheck = l_blnReady

End Function
